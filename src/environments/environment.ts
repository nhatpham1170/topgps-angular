// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: true,
	isMockEnabled: true, // You have to switch this, when your real back-end is done
	authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
	sentry: {
		dsn: "https://6513c83b18b14f23b4d70aa6ce97fecc@sentry.vnetgps.com:9000/27",
	},
	api: {
		// host: 'https://dev.vnetgps.com/api',
		// host: 'http://192.168.1.67:8080/api',
		// host: 'http://118.70.181.186:8181/api',
		// host: 'http://125.212.203.173:9191/api',
		// host: 'https://demo.vnetgps.com/api',//DEMO
		// host: 'https://dev.vnetgps.com/api',//DEV
		host: 'https://www.gpsvnet.com/api',//PROD
		// host: 'http://125.212.203.173:9090/api',
		//apic cloud/api',
		// host:'http://localhost:9191/api',
		AppID: 'Mr2n4FaCO8XdS7K6x4sHbIT6L+Gumltq7dy/EW0eIXQ=',
	},
	googleMapKey: 'AIzaSyDZbYx6ZOJH2e7uAbVwlC0dwDrsnq_NdTs',
	osrm: { 
		host: 'http://125.212.203.173:5000'
	},
	hostAllow: [
		'https://free.currconv.com',
		'https://free.currconv.com/api/v7/convert',
		// 'http://admin.haduwaco.com',
		'http://192.168.1.67:8080',
		'http://118.70.181.186:8181',
		'https://www.gpsvnet.com/api',
		'http://125.212.203.173:9191/api',
		'https://dev.vnetgps.com/api',
		'https://demo.vnetgps.com/api',
		'https://dev.vnetgps.com/api',	
	],
	
	googleAPI:{
		keyCheck:"loadedGoogleAPIKey",
		keys:[
			"AIzaSyDzRqfRfTAXkMUMSEddnXLFlOTDcjV9KqA",
			"AIzaSyBCVf9X1wGKDSd4xJ3QuyDCar4t2v6ekHk"
		],
	},
	// hostImage:'http://admin.haduwaco.com',
	version: "v 1.3.8",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
