import { Subscription, Observable, Observer } from 'rxjs';
// Angular
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ChangeDetectorRef,Inject  } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
// Layout
import { LayoutConfigService, SplashScreenService, TranslationService,LayoutConfigModel } from './core/_base/layout';
// language list
import { locale as enLang } from './core/_config/i18n/en';
import { locale as chLang } from './core/_config/i18n/ch';
import { locale as esLang } from './core/_config/i18n/es';
import { locale as jpLang } from './core/_config/i18n/jp';
import { locale as deLang } from './core/_config/i18n/de';
import { locale as frLang } from './core/_config/i18n/fr';
import { locale as vnLang } from './core/_config/i18n/vn';
import { LoginPageService } from '@core/admin';
import { tap, finalize } from 'rxjs/operators';

const LAYOUT_LOGINS = ['login-v1','login-v2','login-v3'];
const LOGIN_DEFAULT:string =  "login-v1";
@Component({
	// tslint:disable-next-line:component-selector
	selector: 'body[kt-root]',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit, OnDestroy {
	// Public properties
	title = 'Metronic';
	loader: boolean;
	private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/
	private configLayout: LayoutConfigModel;
	private loginDefault:string =  "login-v1";
	/**
	 * Component constructor
	 *
	 * @param translationService: TranslationService
	 * @param router: Router
	 * @param layoutConfigService: LayoutCongifService
	 * @param splashScreenService: SplashScreenService
	 */
	constructor(private translationService: TranslationService,
				private router: Router,
				private layoutConfigService: LayoutConfigService,
				private splashScreenService: SplashScreenService,
				private loginPageService: LoginPageService,
				private cdr: ChangeDetectorRef,
				@Inject(DOCUMENT) private _document: HTMLDocument) {

		// register translations
		// console.log("Pham Duy Nhat");
		this.translationService.loadTranslations(enLang, chLang, esLang, jpLang, deLang, frLang,vnLang);
		this.configLayout = this.layoutConfigService.getConfig();
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		// enable/disable loader
		this.loader = this.layoutConfigService.getConfig('loader.enabled');

		const routerSubscription = this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				// hide splash screen
				// this.splashScreenService.hide();

				// scroll to top on every route change
				window.scrollTo(0, 0);

				// to display back the body content
				setTimeout(() => {
					document.body.classList.add('kt-page--loaded');
				}, 500);
			}
		});
		this.unsubscribe.push(routerSubscription);

		let currentHost = location.hostname;
		this.loginPageService.host(currentHost).pipe(
			tap(result => {
				if (result.result) {
					this.configLayout.self.logo.dark = result.result.logoUrl;
					this.configLayout.self.logo.light = result.result.logoUrl;
					let layout = LAYOUT_LOGINS.find(x=>x ==  result.result.layout);

					// set layout login 
					if(!layout) layout = LOGIN_DEFAULT;
					this.configLayout.self.login =  layout;//layout;
					this.configLayout.self["loginpage"] = result.result;

					// set title
					this._document.getElementById('appTitle').innerText = result.result.title || "TopGPS";
					
					
					// set icon
					if(result.result.iconUrl){
						this.configLayout.self.icon = result.result.iconUrl;					
						this.getBase64ImageFromURL(this.configLayout.self.icon).subscribe(base64data=>{							
							this._document.getElementById('appFavicon').setAttribute('href', base64data);
						})
						
					}
					this.layoutConfigService.setConfig(this.configLayout, true);
				}
				else {
					this.configLayout.self.login = "not-support";
					this.layoutConfigService.setConfig(this.configLayout, true);
				}
			},
				error => {
					this.configLayout.self.login = "error";
					this.layoutConfigService.setConfig(this.configLayout, true);
				}
			),
			finalize(() => {
				this.splashScreenService.hide();
				this.cdr.markForCheck();
				this.cdr.detectChanges();
			})
		).subscribe();
	}

	/**
	 * On Destroy
	 */
	ngOnDestroy() {
		this.unsubscribe.forEach(sb => sb.unsubscribe());
	}
	getBase64ImageFromURL(url: string) {
		return Observable.create((observer: Observer<string>) => {
		  // create an image object
		  let img = new Image();
		  img.crossOrigin = 'Anonymous';
		  img.src = url;
		  if (!img.complete) {
			  // This will call another method that will create image from url
			  img.onload = () => {
			  observer.next(this.getBase64Image(img));
			  observer.complete();
			};
			img.onerror = (err) => {
			   observer.error(err);
			};
		  } else {
			  observer.next(this.getBase64Image(img));
			  observer.complete();
		  }
		});
	 }
	 getBase64Image(img: HTMLImageElement) {
		// We create a HTML canvas object that will create a 2d image
		var canvas = document.createElement("canvas");
		canvas.width = img.width;
		canvas.height = img.height;
		var ctx = canvas.getContext("2d");
		// This will draw image    
		ctx.drawImage(img, 0, 0);
		// Convert the drawn image to Data URL
		var dataURL = canvas.toDataURL("image/png");
	 	return dataURL;
	 }
}
