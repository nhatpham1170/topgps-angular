import { Component, OnInit, ChangeDetectorRef, NgModule, Input, Output, EventEmitter, ComponentFactoryResolver, Injector, ElementRef, ViewChild, OnDestroy, HostBinding } from '@angular/core';
import * as L from 'leaflet';
import { MapConfigModel, MapConfig, MapUtil, MapService, Item, LayoutMapConfigModel, MapConfigService } from '@core/utils/map';
import { TrackingService, TrackingUtil } from '@core/map';
import { tap, finalize } from 'rxjs/operators';
import { DeviceMap } from '@core/map/_models/device-map';
import { NgbModal, NgbModalRef, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import objectPath from 'object-path';
import { landmarkIconClass } from '@core/map/consts/landmark/landmark-icon-class';

declare var $;
@Component({
  selector: 'kt-tracking-geofence',
  templateUrl: './tracking-geofence.component.html',
  styleUrls: ['./tracking-geofence.component.scss']
})
export class TrackingGeofenceComponent implements OnInit, OnDestroy {

  // @Input() eventEmitterAlert?: EventEmitter<any>;
  @Input() item: any = [];
  // @Input() userId: string;

  public map: L.Map;

  public optionsLayer: {
    color: string,
    fill: boolean,
    fillColor: string,
    fillOpacity: number,
    opacity: number,
    stroke: boolean
    weight: number,
  }
  public options;
  public layers = {};
  public typeAction: string;
  public currentPopup: L.Layer;
  public showListDevice: boolean = false;
  public layerGroup: L.FeatureGroup;
  public isLoading: boolean;
  public mapService: MapService;
  public mapConfig: MapConfigModel;
  public closeResult: string;
  private modalReference: NgbModalRef;
  public intervalTracking; // 5s
  public timeRefresh: number = 5 * 1000; // 5s
  public data: DeviceMap;
  public isShowInfo: boolean;
  public isMouseEnter: boolean = false;
  private layoutMapConfigModel: LayoutMapConfigModel
  public trackingObj: any;
  private trackingObjOnMap: any;
  public carsInFence;
  public carsInMap;
  private bound: L.LatLngBounds
  public landmarkIconClass = landmarkIconClass;

  @ViewChild('dataContainer', { static: true }) dataContainer: ElementRef;
  @HostBinding('style.flex')
  flex = 'scroll';
  constructor(
    private cdr: ChangeDetectorRef,
    private mapUtil: MapUtil,
    private resolver: ComponentFactoryResolver, private injector: Injector,
    private trackingService: TrackingService,
    private trackingUtil: TrackingUtil,
    private modalService: NgbModal,
    private mapConfigService: MapConfigService
  ) {
    this.mapService = new MapService(this.resolver, this.injector, "MAP_CONFIG_CONTROLS_UTILS");
    this.options = this.mapService.init();
    this.mapConfig = this.mapService.getConfigs();
    this.mapService.config.features.cluster.show = false;
    this.mapService.config.features.fitbound.show = false;
    // this.mapService.config.features.markerStyle.show = false;
    this.mapService.config.features.help = false;
    this.mapService.config.features.geofence.show = false;
    this.mapService.config.features.landmark.show = false;
    this.mapService.config.features.createTool.show = false;
    this.mapService.config.features.markerPopup.value = false;
    this.mapService.config.features.follow.maxTail = 50;// 50 point  
    this.mapService.config.features.markerStyle.options.showSpeed = true;
    this.isLoading = true;
    this.isShowInfo = true;
    this.layoutMapConfigModel = this.mapConfigService.getLayoutConfig();
    this.mapConfigService.layoutConfigUpdate$.subscribe((layoutMapConfig: LayoutMapConfigModel) => {
      if (this.layoutMapConfigModel.infoDeviceMulti.baseData.showFull != layoutMapConfig.infoDeviceMulti.baseData.showFull) {
        this.isShowInfo = layoutMapConfig.infoDeviceMulti.baseData.showFull;
      }
      this.layoutMapConfigModel = layoutMapConfig;

      this.cdr.detectChanges();
    });
  }

  ngOnInit() {
    this.isShowInfo = this.checkShow('infoDeviceMulti.baseData.showFull');

  }

  ngOnDestroy() {
    if (this.intervalTracking) clearInterval(this.intervalTracking);
  }

  createTrackingObj(trackingObj) {
    this.trackingObj = trackingObj
    if (trackingObj.type == 'geofence') {
      this.trackingObjOnMap = this.mapService.addGeofences([trackingObj.data], true);
      this.mapService.map.fitBounds(this.trackingObjOnMap.getBounds())
      this.bound = this.mapService.map.getBounds();
      this.mapService.map.setMaxBounds(this.bound);
    } else {
      this.trackingObjOnMap = this.mapService.createLandmarkTracking([trackingObj.data]);
      this.mapService.panToLandmark([this.trackingObj.data.latitude, this.trackingObj.data.longitude])
      this.bound = this.mapService.map.getBounds();
    }
  }

  addItem(item) {
    this.item = item;
    this.mapService.addMarkers(item);
    this.cdr.detectChanges();
  }

  updateItem(item) {
    this.carsInFence = [];
    this.carsInMap = [];
    this.item = item;
    this.mapService.addMarkers(item);
    if (this.trackingObjOnMap) {
      this.item.forEach(i => {
        let lat = i.lat;
        let lng = i.lng;
        if (this.trackingObj.type == 'geofence' && lat && lng && this.trackingObjOnMap.getBounds().contains([lat, lng])) {
          this.carsInFence.push(i);
        }
        if (lat && lng && this.mapService.map.getBounds().contains([lat, lng])) {
          this.carsInMap.push(i);
        }
      })
    }
    this.cdr.detectChanges();
  }

  setStyle(width: number) {
    this.flex = `1 1 ${width}px`;
  }

  onMapReady(map) {
    this.map = map;
    this.mapService.setMap(this.map);
    this.mapService.map.on('zoomend', () => {
      this.updateItem(this.item)
    })
  }
  onResize($elm) {
    this.mapService.resize();
    let clientRect = document.getElementById('geofenceConfig').getBoundingClientRect();
    let maxHeightWapper = clientRect.height;
    this.cdr.detectChanges();
  }

  getMapService() {
    return this.mapService;
  }
  fitBound() {
    if (this.layerGroup) this.mapUtil.fitBound(this.layerGroup, this.map);
  }

  toggleInfo() {
    this.isShowInfo = !this.isShowInfo;
    if (this.isShowInfo == false) {
      this.isMouseEnter = false;
    }
    this.cdr.detectChanges();
  }
  onMouseEnter() {
    this.isMouseEnter = true;
    this.cdr.detectChanges();
  }
  onMouseLeave() {
    this.isMouseEnter = false;
    this.cdr.detectChanges();
  }
  checkShow(path: string, deep?: boolean) {
    let obj = objectPath.get(this.layoutMapConfigModel, path);
    if (deep && obj) {
      if (Object.values(obj).some(x => x === true)) return true;
      else return false;
    }
    return objectPath.get(this.layoutMapConfigModel, path);
  }

  close(item) {

  }

  refesh() {
    if (this.trackingObj.type == 'geofence') {
      this.mapService.map.fitBounds(this.bound)
    } else {
      this.mapService.panToLandmark([this.trackingObj.data.latitude, this.trackingObj.data.longitude])
    }

  }

}

