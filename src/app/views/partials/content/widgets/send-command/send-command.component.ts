import { Component, OnInit, EventEmitter, Input, ViewChild, ElementRef, Output, ChangeDetectorRef } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DeviceService, Device, User } from '@core/manage';
import { tap, count, takeUntil, finalize, timeout } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DeviceConfigService } from '@core/common/_services/device-config.service';
import { logWarnings } from 'protractor/built/driverProviders';
import { TranslateService } from '@ngx-translate/core';

declare var $: any;
@Component({
  selector: 'kt-send-command',
  templateUrl: './send-command.component.html',
  styleUrls: ['./send-command.component.scss']
})
export class SendCommandComponent implements OnInit {

  @Input() sendCommandOpen: EventEmitter<Device[]>;
  @Input() deviceTypes: any[];
  @Output() sendCommandResult: EventEmitter<{ status: string, message: string, data: any }>;
  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  public devices: Device[];
  public closeResult: string;
  private unsubscribe: Subject<any>;
  private currentReason: any;
  public types: any[];
  public commands: any[];
  public commandStr: string;
  public commandName: string;
  public isSend:boolean = false;
  @ViewChild('content', { static: true }) content: ElementRef;
  constructor(private deviceService: DeviceService,
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef,
    private deviceConfig: DeviceConfigService,
    private translete: TranslateService) {
    this.devices = [];
    this.sendCommandResult = new EventEmitter();
    this.unsubscribe = new Subject();
    this.currentReason = {};
    this.commandStr = "";
    this.commandName = "";
  }
  ngOnInit() {
    if (this.sendCommandOpen != undefined) {
      this.sendCommandOpen.pipe(
        tap(devices => {
          this.devices = devices;
          this.open(this.content);
        })
      ).subscribe();
    }

  }
  open(content) {
    let _this=this;
    if (this.deviceTypes == undefined) {
      this.deviceConfig.get().then(x => {
        this.deviceTypes = x.deviceTypes; 
        // this.types = this.deviceTypes.map(x => x.name);

        setTimeout(() => {
          if(_this.devices.length>0){
            $('#frmSendCommand .selectpicker-type').val(_this.devices[0].typeDevice).selectpicker("refresh");
        
            _this.selecteDeviceType(_this.devices[0].typeDevice);
          }
          else{
            $('#frmSendCommand .selectpicker-type').selectpicker("refresh");
            $('#frmSendCommand .selectpicker-command').selectpicker("refresh");
          }
        });
      });
    }
    else {
      this.types = this.deviceTypes.map(x => x.name);
    }
    this.resetForm();

    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    setTimeout(() => {
      $('#frmSendCommand .kt-selectpicker-send-command').selectpicker();
      if(_this.devices.length>0){
        
        $('#frmSendCommand .selectpicker-type').val(_this.devices[0].typeDevice).selectpicker("refresh");
        _this.selecteDeviceType(_this.devices[0].typeDevice);
      }
      else{
        $('#frmSendCommand .selectpicker-type').selectpicker("refresh");
        $('#frmSendCommand .selectpicker-command').selectpicker("refresh");
      }
    });
  }
  resetForm() {
    this.commandStr = "";
    this.commandName = "";
    this.commands = [];
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  onSubmit() {
    this.isSend = true;
    let deviceIds = this.devices.map(x => x.id);
    this.deviceService.sendCommand({ ids: deviceIds, commandStr: this.commandStr,commandName:this.commandName }, { notifyGlobal: true }).pipe(
      tap(result => {
        if (result.status == 201) {
          this.modalService.dismissAll(this.currentReason);
          this.sendCommandResult.emit({ status: 'success', message: "", data: this.devices });
        }
        else {
          this.sendCommandResult.emit({ status: 'error', message: result.message, data: this.devices });
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.isSend = false;
        this.cdr.markForCheck();
      })
    ).subscribe();
  }
  selecteDeviceType(value) {
    if (value > 0) {
      let type = this.deviceTypes.find(x => x.id == value);
      if (type) {
        if(Array.isArray(type.command)){
          this.commands = type.command;
        }
        // this.commands = Object.keys(type.command).map(x => {
        //   return { name: x, command: type.command[x] };
        // });
      }
      else this.commands = [];
    }
    else this.commands = [];
    setTimeout(() => {
      if (this.devices[0]['commandName']){
        let selectedCommand = this.commands.find(command => command.name == this.devices[0]['commandName'] );
        $('#frmSendCommand .selectpicker-command').val(selectedCommand.commandStr).selectpicker("refresh");
        this.commandStr = selectedCommand.commandStr;
        this.commandName = this.translete.instant('COMMON.COMMAND.' + selectedCommand.name);
      } else {
         $('#frmSendCommand .selectpicker-command').val('default').selectpicker('refresh');
      }
    });
  }

  selectCommandType(value) {
    this.commandStr = value.target.selectedOptions[0].value;
    this.commandName = value.target.selectedOptions[0].text;
  }

  removeDevice(id) {
    this.devices = this.devices.filter(x => x.id != id);
  }
  commandChange(value) {
    this.commandStr = value;
  }
  commandNameChange(value) {
    this.commandName = value;
  }
}
