import { Component, OnInit, ChangeDetectorRef, NgModule, Input, Output, EventEmitter, ComponentFactoryResolver, Injector, OnDestroy } from '@angular/core';
import * as L from 'leaflet';
import { MapConfigModel, MapConfig, MapUtil, MapService, Item } from '@core/utils/map';
import { AlertPopup, AlertService } from '@core/manage';

declare var $;
@Component({
  selector: 'kt-alert-popup',
  templateUrl: './alert-popup.component.html',
  styleUrls: ['./alert-popup.component.scss']
})
export class AlertPopupComponent implements OnInit, OnDestroy {
  public mapConfig: MapConfigModel;

  @Input() updateMap?: EventEmitter<AlertPopup>; // event for update and refesh by parrent
  @Input() alertPopup?: AlertPopup;
  // @Input() eventEmitterAlert?: EventEmitter<any>;
  public map: L.Map;

  public optionsLayer: {
    color: string,
    fill: boolean,
    fillColor: string,
    fillOpacity: number,
    opacity: number,
    stroke: boolean
    weight: number,
  }
  public options;
  public layers = {};
  public typeAction: string;
  public currentPopup: L.Layer;
  public showListDevice: boolean = false;
  public layerGroup: L.FeatureGroup;
  public isLoading: boolean;
  private mapService: MapService;

  constructor(private cdr: ChangeDetectorRef,
    private mapUtil: MapUtil,
    private resolver: ComponentFactoryResolver, private injector: Injector) {
    this.mapService = new MapService(this.resolver, this.injector,"MAP_CONFIG_CONTROLS_UTILS");
    this.options = this.mapService.init();
    this.mapConfig = this.mapService.getConfigs();
    this.mapService.config.features.cluster.show = false;
    this.mapService.config.features.fitbound.show = false;
    this.mapService.config.features.markerStyle.show = false;
    this.mapService.config.features.help = false;
    this.mapService.config.features.geofence.show = false;
    this.isLoading = true;
  };

  ngOnInit() {
    if(this.updateMap)
    {
      this.updateMap.subscribe(params => {   
        this.alertPopup = params;
        this.showAlertOnMap();

      });
    }
    let clientRect = document.getElementById('geofenceConfig').getBoundingClientRect();
    let maxHeightWapper = clientRect.height;
    // $("#geofenceBody").css("max-height", maxHeightWapper);
    this.cdr.detectChanges();
  }
  ngOnDestroy() {
  }

  onMapReady(map) {
    this.map = map;
    this.mapService.setMap(this.map);

    this.showAlertOnMap();
  }
  //
  showAlertOnMap() {

    this.isLoading = false;

    this.layerGroup = new L.FeatureGroup();
    if (this.alertPopup.lat && this.alertPopup.lat != 0 && this.alertPopup.lng && this.alertPopup.lng != 0) {
      // create marker
      let markerResult = this.mapUtil.createMarker({ lat: this.alertPopup.lat, lng: this.alertPopup.lng });
      if (markerResult) {
        this.layerGroup.addLayer(markerResult);
      }
    }
    if (this.alertPopup.geofence) {
      if (this.alertPopup.geofence.points) {
        // create geofence
        let geofenceResult = this.mapUtil.createGeofence(this.alertPopup.geofence);
        if (geofenceResult) {
          this.layerGroup.addLayer(geofenceResult);
        }
      }
    }

    if(this.alertPopup.points)
    {

      let optionsPolyline: any = {
        weight: 5,
        color: "#65b201",
      };

      let polyline = this.mapUtil.createPolyline(this.alertPopup.points,optionsPolyline, {
        isShow: true, options: {
          offset: 3,
        }
      });
      if (polyline) {
        this.layerGroup.addLayer(polyline);
      }

      let optionCTDefault = {
        width: 32,
        heigth: 32,
        color: "#1abb0a",//hex  
        mapIconUrl:  `<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
        width="{width}px" height="{height}px"
        viewBox="0 0 48 48"
        style=" fill:{color};"><path stroke="#fff" stroke-width="1"  d="M24,1C15.2,1,6.015,7.988,6,18C5.982,29.981,24,48,24,48s18.019-17.994,18-30 C41.984,8.003,32.8,1,24,1z M24,26c-4.418,0-8-3.582-8-8s3.582-8,8-8s8,3.582,8,8S28.418,26,24,26z"></path></svg>`,
    } 
      let markerBegin = this.mapUtil.createMarker(this.alertPopup.points[0],{},optionCTDefault);
      if (markerBegin) {
        this.layerGroup.addLayer(markerBegin);
      }

      let markerEnd = this.mapUtil.createMarker(this.alertPopup.points[this.alertPopup.points.length - 1]);
      if (markerEnd) {
        this.layerGroup.addLayer(markerEnd);
      }
    }


    
    if (this.layerGroup.getLayers().length > 0) {
      this.layerGroup.addTo(this.map);
      let _this=this;
      setTimeout(()=>{

        _this.mapUtil.fitBound(_this.layerGroup, _this.map,{ padding: [50, 50], maxZoom: 19 });
      });
    }
  }
  onResize($elm) {
    this.mapService.resize();
    let clientRect = document.getElementById('geofenceConfig').getBoundingClientRect();
    let maxHeightWapper = clientRect.height;

    setTimeout(() => {
      // $("#geofenceBody").css("max-height", maxHeightWapper);
    });

    this.cdr.detectChanges();
  }
  onSetting($elm) {
    let clientRect = document.getElementById('geofenceConfig').getBoundingClientRect();
    let maxHeightWapper = clientRect.height;
    // $("#geofenceBody").css("max-height", maxHeightWapper);
    this.cdr.detectChanges();
  }
  getMapService() {
    return this.mapService;
  }
  fitBound() {
    if (this.layerGroup) this.mapUtil.fitBound(this.layerGroup, this.map);
  }


}
