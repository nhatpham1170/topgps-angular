import { MapConfigModel, MapService } from '@core/utils/map';
import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'kt-color-hint',
  templateUrl: './color-hint.component.html',
  styleUrls: ['./color-hint.component.scss']
})
export class ColorHintComponent implements OnInit, AfterViewInit {

  @Input() mapConfig?: MapConfigModel;
  @Input() mapService?: MapService;
  @ViewChild('container', { static: true }) container: ElementRef;
  @ViewChild('content', { static: true }) content: ElementRef;
  @ViewChild('btn', { static: true }) button: ElementRef;
  public listStatus: any[] = [];
  public isShow: boolean = false;
  constructor() { }

  ngOnInit() {
    let item = ['run', 'stop', 'stopIgnition', 'lostGPS', 'lostGPRS', 'historyTransfer', 'expired','inactive' ];
    item.forEach(status => {
      this.listStatus.push({
        title: this.mapConfig.status[status].text,
        style: { 'background-color': this.mapConfig.status[status].colorCode}
      })
    })
   
  }
  

  ngAfterViewInit() {
    if (this.mapConfig.features.colorHint.value) {
      setTimeout(()=>{
        this.container.nativeElement.style.width = '100%';
        this.button.nativeElement.classList.add('no-border');
      },1000) 
    } 
  }

  toggle() {
    if (this.mapConfig.features.colorHint.value) {
      this.container.nativeElement.style.width = '0px';
      this.button.nativeElement.classList.remove('no-border');
    } else {
      this.container.nativeElement.style.width = '100%';
      this.button.nativeElement.classList.add('no-border');

      this.isShow = true;
    }
      this.mapConfig.features.colorHint.value = !this.mapConfig.features.colorHint.value;
      this.mapService.saveConfigControls();
  }
}
