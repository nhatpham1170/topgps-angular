import { Component, OnInit, ChangeDetectorRef, NgModule, Input, Output, EventEmitter, ComponentFactoryResolver, Injector } from '@angular/core';
import * as L from 'leaflet';
import { MapConfigModel, MapUtil, MapService, Item } from '@core/utils/map';
import { PoiModel, PoiService } from '@core/manage';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { ValidatorCustomService, ToastService } from '@core/_base/layout';
import { TranslateService } from '@ngx-translate/core';
import { tap, takeUntil, finalize } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import "leaflet/dist/images/marker-shadow.png";
import { PoiTypeService,ListIconPoiType } from '@core/admin'; 

declare var $;
@Component({
  selector: 'kt-create-tool',
  templateUrl: './create-tool.component.html',
  styleUrls: ['./create-tool.component.scss'],
  providers: [ListIconPoiType]

})
export class CreateToolComponent implements OnInit {
  public mapConfig: MapConfigModel;

  @Input() listDevices: Array<any>;
  @Input() listTypePoi: Array<any>;
  @Input() poiInput?: PoiModel;
  @Input() currentUser: any;
  @Output() poiResult: EventEmitter<{ status: string, message: string, data: any }>;
  @Input() centerMap?: any;
  public poiInfo: PoiModel;
  public poiMap: PoiModel;
  public poiForm: FormGroup;
  public currentLayer: any;
  public map: L.Map;
  public latlngArr: FormArray;
  public isCricle: boolean;
  public optionsLayer: {
    color: string,
    fill: boolean,
    fillColor: string,
    fillOpacity: number,
    opacity: number,
    stroke: boolean
    weight: number,
  }
  public editableLayers = new L.FeatureGroup();
  public options;
  public drawOptions = {
    position: 'topright',
    draw: {
      polyline: false,
      polygon: false,
      circle: false,
      circlemarker: false,
      rectangle: false,
      marker: false,
    },
    edit: {
      // featureGroup: this.editableLayers,
      edit: false,
      remove: false,
    },
  };

  public layers = {};
  public typeAction: string;
  public listData: Array<any>;
  public layerDevice: L.FeatureGroup;
  public currentPopup: L.Layer;
  public showListDevice: boolean = false;
  public mapService: MapService;
  public icon: any;

  constructor(
    private listIconPoiType: ListIconPoiType,
    private cdr: ChangeDetectorRef,
    // private poiService: poiService,
    private mapUtil: MapUtil,
    private fb: FormBuilder,
    private validatorCT: ValidatorCustomService,
    private translate: TranslateService,
    private toast: ToastService,
    private resolver: ComponentFactoryResolver,
    private injector: Injector,
    private sanitizer: DomSanitizer,
    private poi: PoiService
  ) {
    this.typeAction = "add";
    this.mapService = new MapService(this.resolver, this.injector);
    this.options = this.mapService.init();
    this.isCricle = false;
    this.mapConfig = this.mapService.getConfigs();
    this.listData = [];
    this.poiResult = new EventEmitter();
    let roadmap = L.tileLayer(this.mapConfig.mapType.roadmap.layer['link'], this.mapConfig.mapType.roadmap.layer['option']);
    let satellite = L.tileLayer(this.mapConfig.mapType.satellite.layer['link'], this.mapConfig.mapType.satellite.layer['option']);
    this.layers[this.translate.instant(this.mapConfig.mapType.roadmap.translate)] = roadmap;
    this.layers[this.translate.instant(this.mapConfig.mapType.satellite.translate)] = satellite;
  }

  ngOnInit() {
    this.listData = this.processData(this.listDevices);
    this.mapService.config.options.elementProcessText = "mapProcessText";
    if (this.poiInput) {
      this.typeAction = "edit";
      this.poiInfo = new PoiModel(this.poiInput);
      let points = this.mapUtil.decode(this.poiInfo.encodedPoints);
    }
    else {
      this.poiInfo = new PoiModel();
    }
    this.optionsLayer = {
      color: this.poiInfo.color,
      fill: this.poiInfo.fill,
      fillColor: this.poiInfo.fillColor,
      fillOpacity: this.poiInfo.fillOpacity,
      opacity: this.poiInfo.opacity,
      stroke: this.poiInfo.stroke,
      weight: this.poiInfo.weight,
    };
    if (this.centerMap) {
      this.poiInfo.poiType = '19'
      this.poiInfo.typePoi = '19';
      this.getTypePoiIcon('19')
    }
    this.createForm(this.poiInfo);
    this.poiInfo.userId = this.currentUser.id;
    this.cdr.detectChanges();
  }

  onDrawReady(drawControl) {
    // Do stuff with map     
  }

  onMapReady(map) {
    this.map = map;
    L.control.layers(this.layers).addTo(this.map);
    this.mapService.setMap(this.map);
    if (this.centerMap) {
      this.mapService.map.setView(this.centerMap.latlng, this.centerMap.zoom)
    }
    this.renderpoi(this.poiInput, true);
    if (this.centerMap) this.renderLatlngs([this.mapService.map.getCenter()], 0, 'mark');

  }

  createForm(poi: PoiModel) {
    // covert points
    poi.latlngs.map(x => {
      x.lat = parseFloat(parseFloat(x.lat.toString()).toFixed(5));
      x.lng = parseFloat(parseFloat(x.lng.toString()).toFixed(5));
      return x;
    });
    poi.points = poi.latlngs.map(x => {
      return [x.lat, x.lng]
    });
    if (poi.radius) poi.radius = parseFloat(parseFloat(poi.radius.toString()).toFixed(5));
    this.poiForm = this.fb.group({
      name: new FormControl({ value: poi.name, disabled: false }, [Validators.required, this.validatorCT.maxLength(64)]),
      typePoi: new FormControl({ value: poi.typePoi, disabled: false }, []),
      description: new FormControl({ value: poi.description, disabled: false }, [this.validatorCT.maxLength(256)]),
      latlngArr: this.fb.array([])
    });

    setTimeout(() => {
      $('#listPoiTypeTool').val(poi.poiType).selectpicker('refesh');
    });
    if (poi.latlngs.length > 0) {
      this.latlngArr = this.poiForm.get('latlngArr') as FormArray;
      this.latlngArr.clear();

      poi.latlngs.map(x => {
        this.latlngArr.push(this.createLatlng(x));
      });
    }
    this.poiInfo = Object.assign(this.poiInfo, poi);
    this.updateLayout();

  }
  getOptions(poi: PoiModel) {
    return {
      color: poi.color,
      fill: poi.fill,
      fillColor: poi.fillColor,
      fillOpacity: poi.fillOpacity,
      opacity: poi.opacity,
      stroke: poi.stroke,
      weight: poi.weight,
    };
  }

  renderpoi(poi: PoiModel, fitBound?: boolean) {
    let _this = this;
    if (poi) {
      this.isCricle = false;
      if (this.currentLayer) this.currentLayer.remove();
      switch (poi.type) {

        case "marker":
          let marker: any = L.marker(poi.points[0]).addTo(this.map);
          this.currentLayer = marker;
          this.currentLayer.layerType = "marker";
          this.currentLayer.editing.enable();

          this.currentLayer.on('moveend', function (e) {
            _this.layerEdited(e.target);
          });
          let latlngs = this.currentLayer.getLatLng();

          poi.latlngs = poi.points.map(x => {
            return { lat: x[0], lng: x[1] };
          })
          this.createForm(poi);
          if (fitBound) {
            let _this = this;
            setTimeout(() => {
              _this.map.fitBounds(poi.points, { padding: [50, 50], maxZoom: 15 });
            })
          }
          break;
      }
    }
  }

  onReset() {
    this.poiForm.reset();
    this.renderpoi(this.poiInput, true);
    setTimeout(() => {
      $('.kt_selectpicker').val('').selectpicker('refresh');
    });
  }

  renderLatlngs(latlngs, radius, type: string) {
    this.poiInfo.radius = radius || 0;
    this.poiInfo.type = type || "";
    this.poiInfo.points = latlngs.map(x => {
      return [x.lat, x.lng];
    })
    this.poiInfo.latlngs = latlngs;
    this.createForm(this.poiInfo);
    this.cdr.detectChanges();
  }

  setLatlngForm(latlngs: Array<{ lat: number, lng: number }>) {
    this.latlngArr = this.poiForm.get('latlngArr') as FormArray;
    this.latlngArr.clear();
    latlngs.map(x => {
      this.latlngArr.push(this.createLatlng(x));
    });
  }

  onCreated(layer) {
    layer.layer.editing.enable();
    this.editableLayers.addLayer(layer.layer);
    let _this = this;
    if (this.currentLayer) {
      this.currentLayer.remove();
    }
    this.currentLayer = layer.layer;
    let latlngs;
    let radius;
    let type;
    this.isCricle = false;
    switch (layer.layerType) {
      case "marker":
        let marker: L.Marker = layer.layer;
        latlngs = marker.getLatLng();
        latlngs = [latlngs];
        type = "marker";
        break;
    }
    this.currentLayer['layerType'] = layer.layerType;
    // set options
    this.currentLayer.options = this.optionsLayer;
    this.currentLayer.on('edit', function (e) {
      _this.layerEdited(e.target);
    });
    if (this.currentLayer['layerType'] == 'marker') {
      this.currentLayer.on('moveend', function (e) {
        _this.layerEdited(e.target);
      });
    }
    this.renderLatlngs(latlngs, radius, type);
  }

  layerEdited(layer) {
    let latlngs;
    let radius;
    this.isCricle = false;
    let type;
    switch (layer.layerType) {
      case "marker":
        let marker: L.Marker = layer;
        latlngs = marker.getLatLng();
        latlngs = [latlngs];
        type = "marker";
        break;
    }
    this.renderLatlngs(latlngs, radius, type);
  }

  onResize() {
    this.mapService.resize();
    this.cdr.detectChanges();
  }

  updateLayout() {
    this.mapService.resize();
    this.cdr.detectChanges();
  }

  getCenterLatlngs(){
    if(this.centerMap){
      this.renderLatlngs([this.mapService.map.getCenter()], 0, 'mark')
    }
  }
  onSubmit() {
    this.poiForm.markAllAsTouched();
    setTimeout(() => {
      const firstElementWithError = document.querySelector('#formpoiTool .invalid-feedback');
      if (firstElementWithError) {
        firstElementWithError.scrollIntoView({ block: 'center' });
      }
    });
    if (this.poiInfo.type.length == 0) this.toast.show({ message: this.translate.instant('MANAGE.POI.MESSAGE.NOT_MARKET'), type: 'error' });
    if (!this.poiForm.invalid && this.poiInfo.type.length > 0) {
      this.poiInfo.encodedPoints = this.mapUtil.encode(this.poiInfo.points);
      this.poiInfo.poiType = this.poiForm.value.typePoi;
      this.poiInfo.latitude = this.poiInfo.points[0][0];
      this.poiInfo.longitude = this.poiInfo.points[0][1];
      if (this.typeAction == "edit") {
        this.poi.update(this.poiInfo, { notifyGlobal: true }).pipe(
          tap(data => {
            if (data.status == 200) {
              this.poiResult.emit({ status: "success", message: "", data: data.result });
            }
          }),
          finalize(() => {
            this.cdr.markForCheck();
          })
        ).subscribe();
      }
      else {
        this.poi.create(this.poiInfo, { notifyGlobal: true }).pipe(
          tap(data => {
            if (data.status == 201) {
              this.poiResult.emit({ status: "success", message: "", data: data.result });
            }
          }),
          finalize(() => {
            this.cdr.markForCheck();
          })
        ).subscribe();
      }
    }
    else {
    }
  }

  getTypePoiIcon(id) {
    let icon = this.listTypePoi.find(poi => {
      return poi.id == id
    })
    this.icon = icon

  }

  formChange(value) {
    // this.poiForm.markAllAsTouched();
    this.poiInfo.name = this.poiForm.get('name').value;
    this.poiInfo.typePoi = this.poiForm.get('typePoi').value;
    this.poiInfo.description = this.poiForm.get('description').value;
    this.getTypePoiIcon(this.poiInfo.typePoi);
    // set point
    if (this.formLatLngs.value.length > 0) {
      this.poiInfo.points = this.formLatLngs.value.map(x => {
        return [x.lat, x.lng];
      });
      this.mapService.map.panTo(this.poiInfo.points[0])
    }
    // set options
    this.optionsLayer = {
      color: this.poiInfo.color,
      fill: this.poiInfo.fill,
      fillColor: this.poiInfo.fillColor,
      fillOpacity: this.poiInfo.fillOpacity,
      opacity: this.poiInfo.opacity,
      stroke: this.poiInfo.stroke,
      weight: this.poiInfo.weight,
    };
    this.renderpoi(this.poiInfo, false);
  }

  createLatlng(latlng: { lat: number, lng: number }): FormGroup {
    return this.fb.group({
      lat: [latlng.lat, this.validatorCT.float],
      lng: [latlng.lng, this.validatorCT.float]
    });
  }
  get formLatLngs(): FormArray {
    if (this.poiForm)
      return this.poiForm.get('latlngArr') as FormArray;
  }

  get f() {
    if (this.poiForm) return this.poiForm.controls;
  }

  onSetting($elm) {
    this.cdr.detectChanges();
  }

  processData(data) {
    let _this = this;
    const icons = _this.mapConfig.icons;
    const status = this.mapConfig.status;
    let dataNew = data.map(x => {
      let indexIcon = 0;
      if (x['icon']['name']) {
        let indexIconSearch = _this.mapConfig.icons.findIndex(i => i.name == x['icon']['name']);
        if (indexIconSearch >= 0) {
          indexIcon = indexIconSearch;
        }
      }
      x.iconType = Object.assign({}, icons[indexIcon]);
      x.iconTypeProcess = Object.assign({}, icons[indexIcon]);

      if (x['lat'] == "null") x['lat'] = null;
      if (x['lng'] == "null") x['lng'] = null;

      x.iconTypeProcess.icon = x.iconTypeProcess.icon
        .replace('viewBox="0 0 34 34"', 'viewBox="0 0 24 24"')
        .replace('viewBox="0 0 48 48"', 'viewBox="0 0 38 38"')
        .replace('transform="rotate({iconRotate})"', "")
        .replace('{iconFill}', "#fff");
      x.iconTypeProcess.icon =
        _this.sanitizer.bypassSecurityTrustHtml(x.iconTypeProcess.icon);

      switch (x.status) {
        case "lost_gps":
          x.statusType = status.lostGPS;
          break;
        case "lost_gprs":
          x.statusType = status.lostGPRS; break;
        case "history_transfer":
          x.statusType = status.historyTransfer;
          break;
        case "expired":
          x.statusType = status.expired;
          break;
        case "stop":
          x.statusType = status.stop;
          break;
        case "run":
          x.statusType = status.run;
          break;
        case "inactive":
          x.statusType = status.inactive;
          break;
        case "nodata":
          x.statusType = status.nodata;
          break;
        default:
          x.statusType = status.lostSignal;
          break;
      }
      let item = new Item(x);
      return item;
    });
    return dataNew;
  }

  onWidgetCheckChange(data) {
    if (this.layerDevice) this.layerDevice.remove();
    if (data.listChecked.length > 0) {
      this.layerDevice = new L.FeatureGroup(); data.listChecked.map(x => {
        let marker = this.mapService.createDevice(x, this.currentPopup, this.map, "style-one");
        if (marker) this.layerDevice.addLayer(marker);
      });
      this.layerDevice.addTo(this.map);
      this.map.fitBounds(this.layerDevice.getBounds(), { padding: [50, 50] });
    }
  }

  changeShowListDevice() {
    this.showListDevice = !this.showListDevice;
    this.cdr.detectChanges();
  }

  close(event){
    this.showListDevice = false
  }
}
