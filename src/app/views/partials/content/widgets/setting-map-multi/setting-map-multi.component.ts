import { Component, OnInit, EventEmitter, Input, ViewChild, ElementRef, Output, ChangeDetectorRef } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DeviceGroup, DeviceGroupService } from '@core/manage';
import { tap, count, takeUntil, finalize, timeout } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MapConfigService } from '@core/utils';
import { LayoutMapConfigModel } from '@core/utils/map';


declare var $;
@Component({
  selector: 'kt-setting-map-multi',
  templateUrl: './setting-map-multi.component.html',
  styleUrls: ['./setting-map-multi.component.scss']
})
export class SettingMapMultiComponent implements OnInit {

  @Input() eventInput: EventEmitter<string>;
  @Output() result: EventEmitter<{ status: string, message: string, data: LayoutMapConfigModel }>;
  public closeResult: string;
  private unsubscribe: Subject<any>;
  private userId: string;
  public form: FormGroup;
  private modalReference: NgbModalRef;
  public isSend: boolean = false;
  public layoutMapConfigModel: LayoutMapConfigModel;
  public actionKey: string;
  @ViewChild('content', { static: true }) content: ElementRef;
  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef,
    private mapConfigService: MapConfigService) {
    this.result = new EventEmitter();
    this.unsubscribe = new Subject();
    this.layoutMapConfigModel = this.mapConfigService.getLayoutConfig();
    // console.log(this.layoutMapConfigModel);
  }
  ngOnInit() {
    // this.layoutMapConfigModel = this.mapConfigService.layoutConfigModel;
    if (this.eventInput != undefined) {
      this.eventInput.pipe(
        tap(actionKey => {
          switch (actionKey) {
            case "list_device_base":
              this.actionKey = "list_device_base";
              break;
            case "info_device_multi":
              this.actionKey = "info_device_multi";
              break;
          }          
          this.cdr.detectChanges();
          this.open(this.content);
        })
      ).subscribe();
    }
  }
  open(content) {
    // this.createForm();
    this.modalReference = this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal-tb', backdrop: 'static' });
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    setTimeout(() => {
      $('.kt-selectpicker-send-command').selectpicker();
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  createForm() {
    this.form = this.fb.group({
      groupName: ["", Validators.required],
    })
  }
  onSubmit() {
    this.mapConfigService.setLayoutConfig(JSON.parse(JSON.stringify(this.layoutMapConfigModel)));
    this.modalReference.close();
    this.result.emit({ status: "success", message: "", data: JSON.parse(JSON.stringify(this.layoutMapConfigModel)) });
  }
}
