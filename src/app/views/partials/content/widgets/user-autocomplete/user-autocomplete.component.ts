import { Component, OnInit, ViewChild, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
import { debounceTime, map, filter, distinctUntilChanged, finalize, tap, switchMap } from 'rxjs/operators';
import { Observable, merge, Subject, of } from 'rxjs';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { AsciiPipe } from '@core/_base/layout';
import { UserManageService } from '@core/manage/_service/user-manage.service';

@Component({
  selector: 'kt-user-autocomplete',
  templateUrl: './user-autocomplete.component.html',
  styleUrls: ['./user-autocomplete.component.scss'],
  providers: [AsciiPipe],
})
export class UserAutocompleteComponent implements OnInit {
  @Input('placeholder') placeholder?: string;
  @Output('eventChange') eventChange: EventEmitter<{ text: string, obj: any }> = new EventEmitter();
  public modelSearch;
  public textValue: string;
  public result: Array<any> = [];
  public icons = ['icon-tag', 'icon-thermometer-half', 'icon-gas-station', 'icon-tank', 'icon-tachometer-alt', "icon-manometer", "icon-odometer", "icon-route"];
  public focus$ = new Subject<string>();
  public click$ = new Subject<string>();
  public searching = false;
  public searchFailed = false;
  @ViewChild('instance', { static: true }) instance: NgbTypeahead;
  constructor(private cdr: ChangeDetectorRef, private userManageService: UserManageService, private ascii: AsciiPipe) { }

  ngOnInit() {
  }
  search = (text$: Observable<any>) => {
    return text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => {
        this.eventChange.emit({ text: this.modelSearch, obj: null });
        this.searching = true;
        this.textValue = this.modelSearch;
      }),
      switchMap(term => term.trim() === "" ? of([]) :

        this.userManageService.searchChildUsers({
          params: {
            "searchText": term.trim()
          }, sessionStore: true, module: "user-auto-complete"
        }).pipe(
          map(result => result.result),
          tap(result => {
            this.searchFailed = false;
          },
            error => {
              this.searchFailed = true;
            }),
          finalize(() => {
            this.searching = false;
            this.cdr.markForCheck();
          })
        ))
    );
  }
  selectItem(item) {

    // console.log( this.modelSearch );
    this.textValue = item.item.username;
    this.eventChange.emit({ text: item.item.username, obj: item });
    this.cdr.detectChanges();
    this.instance.dismissPopup();
    this.modelSearch = item.item.username;
  }

  public formatter = (x) => x.username;
  clear() {
    this.modelSearch = "";
    this.textValue = "";
    this.eventChange.emit({ text: "", obj: null });
    this.cdr.detectChanges();
  }
}
