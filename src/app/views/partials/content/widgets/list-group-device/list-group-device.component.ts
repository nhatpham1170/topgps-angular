import { Component, Output, Input, OnInit, ChangeDetectorRef,EventEmitter} from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { AsciiPipe } from '@core/_base/layout';
import moment from 'moment';
import { finalize, takeUntil, tap ,debounceTime} from 'rxjs/operators';
import { DeviceService, DeviceGroupService} from '@core/manage';
import { array } from '@amcharts/amcharts4/core';
declare var $: any;

@Component({
    selector: 'list-group-device',
    templateUrl: './list-group-device.component.html',
    styleUrls: ['./list-group-device.component.scss'],
    providers: [AsciiPipe]

  })
  export class ListGroupDeviceComponent implements OnInit {
    public listDevices : any = [];
    public listGroupDevice : any = [];
    public dataEmiter:EventEmitter<any>;
    public dataEmiterDevice:EventEmitter<any>;
    public isshowListDevice:boolean = false;
    public countCheckedGroup:number = 0;
    public countCheckedDevices:number = 0;
    public listIdDeviced:Array<any> = [];
    public listIdGrouped:Array<any> = [];
    public paramsDefault:paramsDefault = {
        listDeviced:[],
        listGrouped:[]
    };
    private subscription;
    public userId:number;
    public keySearchFilter:string = "name";
    @Output() checkedChange = new EventEmitter();
    @Input() userIdChange?: EventEmitter<parameterEmit>;

    constructor(
        private cdr: ChangeDetectorRef,
        private deviceService: DeviceService,
        private deviceGroup: DeviceGroupService, 

      ) 
      {
          this.dataEmiter = new EventEmitter();
          this.dataEmiterDevice = new EventEmitter();
      }

   async ngOnInit(){
        if (this.userIdChange) {
            this.subscription = this.userIdChange.subscribe(params=>{
              this.userId = params.userId;
              if(params.dataDefault) this.paramsDefault = params.dataDefault;
              if(params.dataDefault && params.dataDefault.listDeviced && params.dataDefault.listDeviced.length > 0) this.isshowListDevice = true;
              this.loadListData(params);

            });
        }
    
      }



      ngOnDestroy(){
        this.subscription.unsubscribe();

      }

     async loadListData(params)
      {
        if(params.listGroups)
        {
          this.listGroupDevice = params.listGroups;
        }else{
          await this.getListDeviceGroup();
        }

        if(params.listDevices)
        {
          this.listDevices = params.listDevices;
        }else{
         await this.getListDevices();
        }
        if(this.paramsDefault)
        {

          let listGroupDeviced  = [];
          if(this.paramsDefault.listGrouped) listGroupDeviced = this.paramsDefault.listGrouped; 
          this.listGroupDevice = this.setCheckParams(listGroupDeviced,this.listGroupDevice);
          
          let lisDeviced = [];
          if(this.paramsDefault.listDeviced) lisDeviced = this.paramsDefault.listDeviced; 
          this.listDevices = this.setCheckParams(lisDeviced,this.listDevices);

        }
        setTimeout(() => {
          this.dataEmiterDevice.emit(this.listDevices);
          this.dataEmiter.emit(this.listGroupDevice);
          this.cdr.detectChanges();

        });
      
      }

    async getListDevices(){
    let option : any = [];
    option['pageNo'] = -1;
    if(this.userId)    option['userId'] = this.userId;
    await  this.deviceService.list({ params: option }).pipe(
        finalize(() => {
            this.cdr.markForCheck();
            }),
            tap((data: any) => {
                if(data.status == 200)
                {
                    this.listDevices = data.result;
                    if(this.paramsDefault)
                    {
                        let listGrouped  = [];
                        if(this.paramsDefault.listDeviced) listGrouped = this.paramsDefault.listDeviced; 
                        this.listDevices = this.setCheckParams(listGrouped,this.listDevices);
                    }
                    this.dataEmiterDevice.emit(this.listDevices);
                    this.cdr.detectChanges();
                }
            })
    ).toPromise();
    }   
    
    async getListDeviceGroup(){
        let option : any = [];
        option['pageNo'] = -1;
        if(this.userId)
        {
            let params = {
              pageNo : -1,
              pageSize : 10,
              user_id :this.userId
            };
            await  this.deviceGroup.listDeviceGroup(params).pipe(
                tap((data: any) => {
                    this.listGroupDevice = data.result;
                    if(this.paramsDefault)
                    {
                        let listGrouped = [];
                        if(this.paramsDefault.listGrouped) listGrouped = this.paramsDefault.listGrouped; 
                        this.listGroupDevice = this.setCheckParams(listGrouped,this.listGroupDevice);
                    }
                    this.dataEmiter.emit(this.listGroupDevice);
                    this.cdr.detectChanges();

                  })
            ).toPromise();
        }
  
      } 

      setCheckParams(dataDefault,listData)
      {
        listData.map(data=>{
          data.checked = false;
          return data;
        });

        for(let i=0;i<dataDefault.length;i++)
        {
           if(listData.find(group=>group.id == dataDefault[i])) listData.find(group=>group.id == dataDefault[i]).checked = true;
        }
        return listData;
      }

      showListDevice(e)
      {
          this.isshowListDevice = !e;
          setTimeout(() => {
           this.dataEmiterDevice.emit(this.listDevices);
          },);
      } 

    changeCheckedDevices(event)
    {
        this.listIdDeviced = event.listChecked;
        this.countCheckedDevices = event.countChecked;
        this.checkedChange.emit({
            listGroup:this.listIdGrouped,
            listDevice:this.listIdDeviced
        });

    }

    changeCheckedGroup(event)
    {
        this.listIdGrouped = event.listChecked;
        this.countCheckedGroup = event.countChecked;
        this.checkedChange.emit({
            listGroup:this.listIdGrouped,
            listDevice:this.listIdDeviced
        });
    }

    emitData(){
        
    }

    reset()
    {
        return 1;
    }

    setValue(paramsDefault:paramsDefault)
    {
        this.paramsDefault = paramsDefault;
    }
  }

  export class paramsDefault
  {
    listDeviced?:Array<number>;
    listGrouped?:Array<number>;
  }

  export class parameterEmit
  {
    userId:number;
    dataDefault?:paramsDefault;
    listGroups?:any;
    listDevices?:any;
  }
    