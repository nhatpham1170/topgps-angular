import { Component, OnInit, Input,ViewChild,TemplateRef } from '@angular/core';
import { User } from '@core/manage';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { CurrentUserService } from '@core/auth'
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Content } from '@angular/compiler/src/render3/r3_ast';


@Component({
  selector: 'kt-user-popup',
  templateUrl: './user-popup.component.html',
  styleUrls: ['./user-popup.component.scss']
})
export class UserPopupComponent implements OnInit {
  
  @Input() data:any =[];
  @ViewChild('content', { static: false })


  saveFormUsers: FormGroup;
  public dataDefault: any = [{}]; //
  public idParentUserEdit: any;
  public userIdSelected:number;
  public closeResult: string;

  public listTimeZone :any = [];
  public languageList :any = [];
  public weekFirstDay :any = [];
  public decimalSeprerator:any= [];
  public unitVolume :any = [];
  public unitWeight :any = [];
  public unitTemperature :any = [];
  public unitDistance :any = [];
  public dateFormat :any = [];
  public timeFormat :any = [];
  public listRoles :any = [];
  public listUserType :any = [];
  constructor(
    private formBuilder: FormBuilder,
    private CurrentUserService : CurrentUserService,
    private modalService: NgbModal,
  ){
      
  }

  ngOnInit() {
    this.buildForm();
    this.dataDefault = [this.data];
  }

  loadParamsetUser(){
    this.listTimeZone = this.CurrentUserService.listTimeZone;
    this.languageList = this.CurrentUserService.listLanguage;
    this.weekFirstDay = this.CurrentUserService.listWeekFirst;
    this.decimalSeprerator = this.CurrentUserService.listDecimal;
    this.unitVolume = this.CurrentUserService.listVolume;
    this.unitWeight = this.CurrentUserService.listWeight;
    this.unitDistance = this.CurrentUserService.listDistance;
    this.unitTemperature = this.CurrentUserService.listTemperature;
    this.timeFormat = this.CurrentUserService.listTimeFormat;
    this.dateFormat = this.CurrentUserService.listDateFormat;
    this.listRoles = this.CurrentUserService.listRoles;
    this.listUserType = this.CurrentUserService.listUserType;
  }

  private buildForm(): void {
    this.saveFormUsers = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(6)]],
      name: ['', Validators.required],
      phone: [''],
      email: ['', [Validators.required, Validators.email]],
      role: [''],
      timezone: [''],
      language: [''],
      type: ['', Validators.required],
      description: [''],
      address: [''],
      distance: [''],
      volume: [''],
      weight: [''],
      date: [''],
      time: [''],
      temperature: [''],
      fisrtday: [''],
      seprerator: ['']
    });
}

get f() {
    if (this.saveFormUsers) return this.saveFormUsers.controls;
  }

getParrentUser(value)
{
  if (value.id > 0) {
    if(this.idParentUserEdit != value.id) 
    {
      this.idParentUserEdit = value.id;
    //   this.buildDataDefault();

    }
  }
  if(this.idParentUserEdit == undefined) this.idParentUserEdit = this.userIdSelected;
}

open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder',size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  
  onSubmit(form: any) {
    if (this.saveFormUsers.invalid) {
      return;
    }
    let params = {
      "parentId": this.idParentUserEdit,
      "aliasId": 0,
      "path": "",
      "username": form.value.username,
      "type": form.value.type,
      "name": form.value.name,
      "surname": "",
      "email": form.value.email,
      "phone": form.value.phone,
      "description": form.value.description,
      "status": 1,
      "active": 1,
      "createdAt": "",
      "stockDevice": 0,
      "totalDevice": 0,
      "totalPoint": 0,
      "isRenewed": 1,
      "timezone": form.value.timezone,
      "language": form.value.language,
      "unitDistance": form.value.distance,
      "unitVolume": form.value.volume,
      "unitTemperature": form.value.temperature,
      "unitWeight": form.value.weight,
      "dateFormat": form.value.date,
      "timeFormat": form.value.time,
      "weekFirstDay": form.value.fisrtday,
      "markerStyle": "html",
      "carSorting": "plate",
      "decimalSeprerator": form.value.seprerator,
      "roleId": form.value.role
    } as User;

    // if (this.isEdit) {
    //   params.id = this.idUserEdit;
    //   this.manageUser.put(params,{ notifyGlobal: true }).subscribe((result: any) => {
    //     if (result.status == 200) {
    //       let params = {
    //         id: this.idParentUserEdit,
    //         path : result.result.path
    //       }
    //       this.modalService.dismissAll();
    //     }
    //   });
     
    //   return;
    // };
   
  }


}
