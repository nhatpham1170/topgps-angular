import { Component, OnInit, Input, Output, ElementRef, ViewChild, EventEmitter } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'kt-confirm-delete',
  templateUrl: './confirm-delete.component.html',
  styleUrls: ['./confirm-delete.component.scss']
})
export class ConfirmDeleteComponent implements OnInit {

  @Input() openDelete:EventEmitter<any>;
  @Input() title:string;
  @Input() template?:any;
  @Input() message?:string;
  @Input() class?:string;
  @Output() result:EventEmitter<boolean>;
  public closeResult:any;
  public currentReason:any;
  public modal:NgbModalRef;
  @ViewChild('deleteTmp', { static: true }) deleteTmp: ElementRef;
  constructor(private modalService:NgbModal) {
    this.result = new EventEmitter();   
   }

  ngOnInit() {
    if(this.openDelete){
      this.openDelete.pipe(
        tap(()=>{
          this.open(this.deleteTmp);
        })
      ).subscribe();
    }
  }
  open(tmp){
    this.modal = this.modalService.open(tmp, { windowClass: 'kt-mt-50 modal-dialog-scrollable ' + this.class , size: 'lg', backdrop: 'static' });
    this.modal.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        this.currentReason = reason;      
      });
  }
  onSubmit(){
    this.result.emit(true);
    this.modal.close();
  }
  onCancel(){
    this.result.emit(false);
    this.modal.close();
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
