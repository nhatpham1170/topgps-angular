import { Component, OnInit, ChangeDetectorRef, NgModule, Input, Output, EventEmitter, ComponentFactoryResolver, Injector, ElementRef, ViewChild, OnDestroy, HostBinding } from '@angular/core';
import * as L from 'leaflet';
import { MapConfigModel, MapConfig, MapUtil, MapService, Item, LayoutMapConfigModel, MapConfigService } from '@core/utils/map';
import { TrackingService, TrackingUtil } from '@core/map';
import { tap, finalize } from 'rxjs/operators';
import { DeviceMap } from '@core/map/_models/device-map';
import { NgbModal, NgbModalRef, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import objectPath from 'object-path';

declare var $;
@Component({
  selector: 'kt-tracking-item',
  templateUrl: './tracking-item.component.html',
  styleUrls: ['./tracking-item.component.scss']
})
export class TrackingItemComponent implements OnInit,OnDestroy {

  // @Input() eventEmitterAlert?: EventEmitter<any>;
  @Input() item: any = {};
  // @Input() userId: string;

  public map: L.Map;

  public optionsLayer: {
    color: string,
    fill: boolean,
    fillColor: string,
    fillOpacity: number,
    opacity: number,
    stroke: boolean
    weight: number,
  }
  public options;
  public layers = {};
  public typeAction: string;
  public currentPopup: L.Layer;
  public showListDevice: boolean = false;
  public layerGroup: L.FeatureGroup;
  public isLoading: boolean;
  public mapService: MapService;
  public mapConfig: MapConfigModel;
  public closeResult: string;
  private modalReference: NgbModalRef;
  public intervalTracking; // 5s
  public timeRefresh: number = 5 * 1000; // 5s
  public data: DeviceMap;
  public isShowInfo:boolean;
  public isMouseEnter:boolean = false;
  private layoutMapConfigModel:LayoutMapConfigModel

  
  @ViewChild('dataContainer', { static: true }) dataContainer: ElementRef;
  @HostBinding('style.flex') 
     flex = 'scroll';
  constructor(
    private cdr: ChangeDetectorRef,
    private mapUtil: MapUtil,
    private resolver: ComponentFactoryResolver, private injector: Injector,
    private trackingService: TrackingService,
    private trackingUtil: TrackingUtil,
    private modalService: NgbModal,
    private mapConfigService: MapConfigService 
  ) {
    this.mapService = new MapService(this.resolver, this.injector,"MAP_CONFIG_CONTROLS_UTILS");
    this.options = this.mapService.init();    
    this.mapConfig = this.mapService.getConfigs();
    this.mapService.config.features.cluster.show = false;
    this.mapService.config.features.fitbound.show = false;
    // this.mapService.config.features.markerStyle.show = false;
    this.mapService.config.features.help = false;
    this.mapService.config.features.geofence.show = false;
    this.mapService.config.features.landmark.show = false;
    this.mapService.config.features.createTool.show = false;
    this.mapService.config.features.markerPopup.value = false;
    this.mapService.config.features.follow.maxTail = 50;// 50 point  
    this.mapService.config.features.markerStyle.options.showSpeed = true;
    this.isLoading = true;
    this.isShowInfo = true;
    this.layoutMapConfigModel = this.mapConfigService.getLayoutConfig();
    this.mapConfigService.layoutConfigUpdate$.subscribe((layouMapConfig:LayoutMapConfigModel)=>{
      if(this.layoutMapConfigModel.infoDeviceMulti.baseData.showFull != layouMapConfig.infoDeviceMulti.baseData.showFull){
        this.isShowInfo = layouMapConfig.infoDeviceMulti.baseData.showFull;
      }
      this.layoutMapConfigModel = layouMapConfig;
      
      this.cdr.detectChanges();
    });
  }

  ngOnInit() {
    this.isShowInfo = this.checkShow('infoDeviceMulti.baseData.showFull');
  }
  ngOnDestroy(){
    if(this.intervalTracking) clearInterval(this.intervalTracking);
  }
  addItem(item){
    this.item = item;
    this.mapService.addMarkers([item]);
    this.mapService.follow(item);
    this.mapService.fitbound([item],{maxZoom:18});
    this.dataContainer.nativeElement.innerHTML = this.item.iconType.icon
    .replace('viewBox="0 0 34 34"', 'viewBox="0 0 24 24"')
    .replace('viewBox="0 0 48 48"', 'viewBox="0 0 38 38"')
    .replace('transform="rotate({iconRotate})"', "")
    .replace('fill="{iconFill}"', "");
    this.cdr.detectChanges();
  }
  updateItem(item){
    this.item = item;
    this.mapService.updateMarkers([item],true);
    this.dataContainer.nativeElement.innerHTML = this.item.iconType.icon
    .replace('viewBox="0 0 34 34"', 'viewBox="0 0 24 24"')
    .replace('viewBox="0 0 48 48"', 'viewBox="0 0 38 38"')
    .replace('transform="rotate({iconRotate})"', "")
    .replace('fill="{iconFill}"', "");
    this.cdr.detectChanges();
  }
  setStyle(width:number){
    this.flex = `1 1 ${width}px`;
  }

  onMapReady(map) {
    this.map = map;
    this.mapService.setMap(this.map);
  }
  onResize($elm) {
    this.mapService.resize();
    let clientRect = document.getElementById('geofenceConfig').getBoundingClientRect();
    let maxHeightWapper = clientRect.height;
    
    this.cdr.detectChanges();
  }

  getMapService() {
    return this.mapService;
  }
  fitBound() {
    if (this.layerGroup) this.mapUtil.fitBound(this.layerGroup, this.map);
  }

  toggleInfo(){
    this.isShowInfo = !this.isShowInfo;
    if(this.isShowInfo == false){
      this.isMouseEnter = false;
    }
    this.cdr.detectChanges();
  }
  onMouseEnter(){
    this.isMouseEnter = true;
    this.cdr.detectChanges();
  }
  onMouseLeave(){
    this.isMouseEnter = false;
    this.cdr.detectChanges();
  }
  checkShow(path:string,deep?:boolean){
    let obj =  objectPath.get(this.layoutMapConfigModel,path);
    if(deep && obj){
      if(Object.values(obj).some(x=>x===true)) return true;
      else return false;
    }
    return objectPath.get(this.layoutMapConfigModel,path);
  }
  close(item){

  }

}

