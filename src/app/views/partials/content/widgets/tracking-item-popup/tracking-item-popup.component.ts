import { Component, OnInit, ChangeDetectorRef, NgModule, Input, Output, EventEmitter, ComponentFactoryResolver, Injector, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import * as L from 'leaflet';
import { MapConfigModel, MapConfig, MapUtil, MapService, Item } from '@core/utils/map';
import { TrackingService, TrackingUtil } from '@core/map';
import { tap, finalize } from 'rxjs/operators';
import { DeviceMap } from '@core/map/_models/device-map';
import { NgbModal, NgbModalRef, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

declare var $;
@Component({
  selector: 'kt-tracking-item-popup',
  templateUrl: './tracking-item-popup.component.html',
  styleUrls: ['./tracking-item-popup.component.scss']
})
export class TrackingItemPopupComponent implements OnInit,OnDestroy {

  // @Input() eventEmitterAlert?: EventEmitter<any>;
  @Input() imei: string;
  @Input() userId: string;
  public map: L.Map;

  public optionsLayer: {
    color: string,
    fill: boolean,
    fillColor: string,
    fillOpacity: number,
    opacity: number,
    stroke: boolean
    weight: number,
  }
  public options;
  public layers = {};
  public typeAction: string;
  public currentPopup: L.Layer;
  public showListDevice: boolean = false;
  public layerGroup: L.FeatureGroup;
  public isLoading: boolean;
  private mapService: MapService;
  public mapConfig: MapConfigModel;
  public closeResult: string;
  private modalReference: NgbModalRef;
  public intervalTracking; // 5s
  public timeRefresh: number = 5 * 1000; // 5s
  public data: DeviceMap;
  public isShowInfo:boolean;

  
  @ViewChild('dataContainer', { static: true }) dataContainer: ElementRef;
  constructor(
    private cdr: ChangeDetectorRef,
    private mapUtil: MapUtil,
    private resolver: ComponentFactoryResolver, private injector: Injector,
    private trackingService: TrackingService,
    private trackingUtil: TrackingUtil,
    private modalService: NgbModal
  ) {
    this.mapService = new MapService(this.resolver, this.injector,"MAP_CONFIG_CONTROLS_UTILS");
    this.options = this.mapService.init();    
    this.mapConfig = this.mapService.getConfigs();
    this.mapService.config.features.cluster.show = false;
    this.mapService.config.features.fitbound.show = false;
    // this.mapService.config.features.markerStyle.show = false;
    this.mapService.config.features.help = false;
    this.mapService.config.features.geofence.show = false;
    this.mapService.config.features.landmark.show = false;
    this.mapService.config.features.createTool.show = false;
    this.mapService.config.features.markerPopup.value = false;
    this.mapService.config.features.follow.maxTail = 50;// 50 point  
    this.mapService.config.features.markerStyle.options.showSpeed = true;
    this.isLoading = true;
    this.isShowInfo = true;
   
    
  }

  ngOnInit() {

  }
  ngOnDestroy(){
    if(this.intervalTracking) clearInterval(this.intervalTracking);
  }

  onMapReady(map) {
    this.map = map;
    this.mapService.setMap(this.map);
    this.getDevice();
  }
  onResize($elm) {
    this.mapService.resize();
    let clientRect = document.getElementById('geofenceConfig').getBoundingClientRect();
    let maxHeightWapper = clientRect.height;


    this.cdr.detectChanges();
  }

  getMapService() {
    return this.mapService;
  }
  fitBound() {
    if (this.layerGroup) this.mapUtil.fitBound(this.layerGroup, this.map);
  }

  getDevice() {
    let _this = this;
    this.isLoading = true;
    if (this.imei) {
      if (this.intervalTracking) clearInterval(this.intervalTracking);
      this.intervalTracking = setInterval(() => {
        _this.refreshDevice();
      }, _this.timeRefresh);

      this.trackingService.list({ params: { groupId: -1, userId: this.userId, imei: this.imei } }).pipe(
        tap(data => {
          let dateTimeNow = data.datetime;

          let dataNew = this.trackingUtil.processItems(data.result, dateTimeNow);
          if (dataNew.length > 0) {
            this.data = dataNew[0];
            this.dataContainer.nativeElement.innerHTML = this.data.iconType.icon
            .replace('viewBox="0 0 34 34"', 'viewBox="0 0 24 24"')
            .replace('viewBox="0 0 48 48"', 'viewBox="0 0 38 38"')
            .replace('transform="rotate({iconRotate})"', "")
            .replace('fill="{iconFill}"', "");

            let dataMap = [];
            dataNew.forEach(d => {
              let item = new Item(d);
              dataMap.push(item);
            });      
            this.mapService.addMarkers(dataMap, true);
            this.mapService.follow(dataMap[0]);

          }

        }),
        finalize(() => {
          this.isLoading = false;
          this.cdr.markForCheck();
          this.cdr.detectChanges();
        })
      ).subscribe();
    }
  }
  refreshDevice() {    
    this.isLoading = true;
    this.trackingService.listRefresh({ params: { groupId: -1, userId: this.userId, imei: this.imei } }).pipe(
      tap(data => {
        let dateTimeNow = data.datetime;
        let dataNew = this.trackingUtil.processItems(data.result, dateTimeNow);
        if (dataNew.length > 0) {
          this.data = dataNew[0];
          let dataMap = [];
          dataNew.forEach(d => {
            let item = new Item(d);
            dataMap.push(item);
          });
          // set status         
          this.mapService.updateMarkers(dataMap, true);
        }
      }),
      finalize(() => {
        this.isLoading = false;
        this.cdr.markForCheck();
      })
    ).subscribe();
  }
  toggleInfo(){
    this.isShowInfo = !this.isShowInfo;
    this.cdr.detectChanges();
  }

}

