import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'kt-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
  encapsulation:ViewEncapsulation.Emulated,
})
export class NotFoundComponent implements OnInit {

  @Input() message: string;
  @Input() icon: string;
  @Input() type:string;
  @Input() size:string;
  public styleIcon:object;
  constructor() {
    if(!this.icon) this.icon = "icon-search";
    if (!this.message) this.message = "COMMON.MESSAGE.NOT_FOUND_DATA";
    if(!this.size)this.size = "md";
    this.styleIcon = {'font-size':'2.4rem'};
  }

  ngOnInit() {
    switch(this.size.toLocaleLowerCase()){
      case "xs":
        this.styleIcon = {'font-size':'1.2rem'};
      break;     
      case "sm":
        this.styleIcon = {'font-size':'1.6rem'};
      break;
      case "md":
        this.styleIcon = {'font-size':'2.4rem'};
      break;
      case "lg":
        this.styleIcon = {'font-size':'2.8rem'};
      break;
      case "xl":
        this.styleIcon = {'font-size':'3rem'};
      break;        
    }
  }

}
