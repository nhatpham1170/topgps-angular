import { PartialsModule } from './../../partials.module';
import { CreateToolComponent } from './create-tool/create-tool.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatIconModule, MatPaginatorModule, MatProgressSpinnerModule, MatSortModule, MatTableModule, } from '@angular/material';
import { CoreModule } from '../../../../core/core.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { SharedModule } from '@app/core/common';
import { RouterModule } from '@angular/router';

// import { AutocompleteLibModule } from 'angular-ng-autocomplete';// Angular Material
// import { TreeModule } from 'angular-tree-component';

// Datatable
import { DataTableComponent } from './general/data-table/data-table.component';
// General widgets
import { Widget1Component } from './widget1/widget1.component';
import { Widget4Component } from './widget4/widget4.component';
import { Widget5Component } from './widget5/widget5.component';
// import { Widget12Component } from './widget12/widget12.component';
import { Widget14Component } from './widget14/widget14.component';
import { Widget26Component } from './widget26/widget26.component';
import { Timeline2Component } from './timeline2/timeline2.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { TreeModule } from 'angular-tree-component';
import { UserTreeNewComponent } from './user-tree-new/user-tree-new.component';
import { TranslateModule } from '@ngx-translate/core';
import { NotFoundComponent } from './not-found/not-found.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ClipboardModule } from 'ngx-clipboard';
import { ToastrModule } from 'ngx-toastr';
import { UsersTreeComponent } from './user-tree/users-tree.component';

import { UserInfoComponent } from './user-info/user-info.component';
import { ConfirmDeleteComponent } from './confirm-delete/confirm-delete.component';
import { MapControlComponent } from './map-control/map-control.component';
import { ListDeviceComponent } from './list-device/list-device.component';
import { BoxCheckListComponent } from './box-check-list/box-check-list.component';
import { SelectPickerComponent } from './select-picker/select-picker.component';
import { SelectPickerGroupComponent } from './select-picker/select-picker-group/select-picker-group.component';
import { SelectPickerNoGroupComponent } from './select-picker/select-picker-no-group/select-picker-no-group.component';

import { BreadCrumbComponent } from './breadcrumb/breadcrumb.component';
import { ChartGlobalComponent } from './chart-global/chart-global.component';
import { FuelChartGlobalComponent } from './fuel-chart-global/fuel-chart-global.component';
import { TemperatureComponent } from './temperature-chart/temperature-chart.component';

import { UserPopupComponent } from './user-popup/user-popup.component';
import { WidgetCheckListComponent } from './widget-check-list/widget-check-list.component';
import { AlertPopupComponent } from './alert-popup/alert-popup.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletDrawModule } from '@asymmetrik/ngx-leaflet-draw';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TrackingItemPopupComponent } from './tracking-item-popup/tracking-item-popup.component';
import { UserDatePipe } from '@core/_base/layout';
import { NgxPermissionsModule } from 'ngx-permissions';
import { SettingMapComponent } from './setting-map/setting-map.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { UserAutocompleteComponent } from './user-autocomplete/user-autocomplete.component';
import { LightboxModule } from 'ngx-lightbox';
import { TrackingItemComponent } from './tracking-item/tracking-item.component';
import { ListDeviceBaseComponent } from './list-device-base/list-device-base.component';
import { SettingMapMultiComponent } from './setting-map-multi/setting-map-multi.component';
import { SendCommandComponent } from './send-command/send-command.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ChartsModule } from 'ng2-charts';
import { GeofenceCreatorComponent } from './geofence-creator/geofence-creator.component';
import { MessageErrorCopyComponent } from './message-error/message-error.component';
import { CenterPointComponent } from './center-point/center-point.component';
import { ColorHintComponent } from './color-hint/color-hint.component';
import { ListDeviceLiteComponent } from './list-device-lite/list-device-lite.component';
import { SearchComponent } from './search-place/search-place.component';
import { SearchDirectionComponent } from './search-direction/search-direction.component';
import { DistanceComponent } from './distance/distance.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ListGroupDeviceComponent } from './list-group-device/list-group-device.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { TrackingGeofenceComponent } from './tracking-geofence/tracking-geofence.component';
@NgModule({
	declarations: [
		TemperatureComponent,
		DataTableComponent,
		// Widgets
		Widget1Component,
		Widget4Component,
		Widget5Component,
		// Widget12Component,
		Widget14Component,
		Widget26Component,
		Timeline2Component,
		SidebarComponent,
		UserTreeNewComponent,
		NotFoundComponent,
		UsersTreeComponent,
		UserInfoComponent,
		ConfirmDeleteComponent,
		MapControlComponent,
		ListDeviceComponent,
		BoxCheckListComponent,
		BreadCrumbComponent,
		UserPopupComponent,
		WidgetCheckListComponent,
		AlertPopupComponent,		
		TrackingItemPopupComponent, 
		SettingMapComponent, 
		UploadFileComponent, 
		UserAutocompleteComponent, 
		TrackingItemComponent, 
		ListDeviceBaseComponent, 
		SettingMapMultiComponent, 
		SendCommandComponent,
		TrackingItemPopupComponent,
		ChartGlobalComponent,
		FuelChartGlobalComponent,
		SelectPickerComponent,
		CreateToolComponent,
		GeofenceCreatorComponent,
		MessageErrorCopyComponent,
		CenterPointComponent,
		ColorHintComponent,
		SearchComponent,
		SelectPickerGroupComponent,
		SelectPickerNoGroupComponent,
		ListDeviceLiteComponent,
		SearchDirectionComponent,
		DistanceComponent,
		ListGroupDeviceComponent,
		TrackingGeofenceComponent
		// AsciiPipe
	],
	exports: [
		TemperatureComponent,
		DataTableComponent,
		// Widgets
		Widget1Component,
		Widget4Component,
		Widget5Component,
		// Widget12Component,
		Widget14Component,
		Widget26Component,
		Timeline2Component,
		SidebarComponent,
		UserTreeNewComponent,
		NotFoundComponent,
		UserInfoComponent,
		ConfirmDeleteComponent,
		MapControlComponent,
		ListDeviceComponent,
		BoxCheckListComponent,
		BreadCrumbComponent,
		UserPopupComponent,		
		WidgetCheckListComponent,
		AlertPopupComponent,
		TrackingItemPopupComponent,
		SettingMapComponent,
		UserAutocompleteComponent,
		TrackingItemComponent,
		ListDeviceBaseComponent,
		ListDeviceLiteComponent,
		SettingMapMultiComponent,
		SendCommandComponent,
		UploadFileComponent,
		ChartGlobalComponent,
		FuelChartGlobalComponent,
		CreateToolComponent,
		GeofenceCreatorComponent,
		MessageErrorCopyComponent,
		CenterPointComponent,
		ColorHintComponent,
		SearchComponent,
		SelectPickerComponent,
		SelectPickerGroupComponent,
		SelectPickerNoGroupComponent,
		SearchDirectionComponent,
		DistanceComponent,
		ListGroupDeviceComponent
	],
	imports: [			
		RouterModule,
		AutocompleteLibModule,
		TreeModule.forRoot(),
		CommonModule,
		PerfectScrollbarModule,
		MatTableModule,
		CoreModule,
		MatIconModule,
		MatButtonModule,
		MatProgressSpinnerModule,
		MatPaginatorModule,
		MatSortModule,
		SharedModule,
		TranslateModule,
		NgbModule,		
		ClipboardModule,
		ToastrModule.forRoot(),
		LeafletModule.forRoot(),
		LeafletDrawModule.forRoot(),
		ReactiveFormsModule,
		FormsModule,
		NgxPermissionsModule.forChild(),
		NgSelectModule,
		LightboxModule,
		ImageCropperModule,
		ChartsModule,
		NgxPermissionsModule.forChild(),
		DragDropModule,
		MatExpansionModule,
		MatChipsModule,
		MatFormFieldModule,
		MatSelectModule
		// PartialsModule,
	],
	providers:[
		UserDatePipe
	],
	entryComponents: [TrackingItemComponent, TrackingGeofenceComponent],
})
export class WidgetModule {
}
