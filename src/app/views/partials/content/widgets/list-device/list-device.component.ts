import { Poi } from './../../../../../core/map/_models/poi.model';
import { PoiService } from './../../../../../core/manage/_services/poi.service';
import { Component, OnInit, ChangeDetectorRef, Input, EventEmitter, OnDestroy, ViewChild, ElementRef, Output } from '@angular/core';
import { User, Logout } from '@core/auth';
import { TrackingService, TrackingUtil } from '@core/map';
import { tap, finalize, takeUntil } from 'rxjs/operators';
import { DeviceMap } from '@core/map/_models/device-map';
import { MapService, MapConfigModel, Item, SVGUtil, MapChangeEvent, MapUtilityService, LayoutMapConfigModel, MapConfigService } from '@core/utils/map';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastService, AsciiPipe, UserDatePipe } from '@core/_base/layout';
import { DeviceGroupService, GeofenceService, GeofenceModel, DeviceService } from '@core/manage';
import { DeviceConfigService } from '@core/common/_services/device-config.service';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Subscription } from 'rxjs';
import objectPath from 'object-path';
import { ListDeviceService } from '@core/common';
import { DataTable } from '@core/_base/layout/models/datatable.model';

declare var $;
@Component({
  selector: 'kt-list-device',
  templateUrl: './list-device.component.html',
  styleUrls: ['./list-device.component.scss'],
  providers: [AsciiPipe]
})
export class ListDeviceComponent implements OnInit, OnDestroy {
  @Input() eventEmiter: EventEmitter<{ type: string, data: any }>;
  @Input() timeRefesh: number = 15 * 1000;
  @Input() mapService: MapService;
  @Output('dataChange') eventChange: EventEmitter<{ type: string, data: any, options?: any }>;
  public intervalRefesh;
  public maxHeightWapper: number = 650;
  public currentUser: User;
  public listView: Array<DeviceMap> = [];
  public listData: Array<DeviceMap> = [];
  public listDataOld: Array<DeviceMap> = [];
  public configMap: MapConfigModel;
  public deviceType: Array<any> = [];
  public deviceGroup: Array<any> = [];
  public showFilter: boolean = false;
  public isResetFilter: boolean = false;
  public listGeofence: Array<GeofenceModel>;
  public checkAll: boolean;
  public favoriteMode: boolean;
  public dataFilter: { sort: string, status: string, keySearch: string, deviceGroup: Array<number>, deviceType: Array<number> }
    = { sort: 'off', status: 'all', keySearch: "", deviceGroup: [], deviceType: [] };
  public isLoading: boolean = false;
  public status: { all: number, stop: number, running: number, lostGPS: number, lostSignal: number };
  private isShow: boolean = true;
  private mapEventChange: Subscription;
  public eventMapSetting: EventEmitter<any>;
  public layoutMapConfigModel: LayoutMapConfigModel;
  public unsubscribe: Subscription[] = [];
  public isShowMobile: boolean;
  public lastDeviceIdSelected: number;
  private poiData: Poi;
  // feature emitter
  public sendCommandEventEmitter:EventEmitter<any> = new EventEmitter();
  public dataTable: DataTable = new DataTable();
  constructor(private cdr: ChangeDetectorRef,
    private trackingService: TrackingService,
    private deviceService: DeviceService,
    private sanitizer: DomSanitizer,
    private toastService: ToastService,
    private deviceGroupService: DeviceGroupService,
    private deviceConfigService: DeviceConfigService,
    private geofenceService: GeofenceService,
    private trackingUtilUtil: TrackingUtil,
    private mapUtilService: MapUtilityService,
    private acsii: AsciiPipe,
    private userDate: UserDatePipe,
    private mapConfigService: MapConfigService,
    private listDeviceService: ListDeviceService,
    ) {
    this.status = { all: 0, stop: 0, running: 0, lostGPS: 0, lostSignal: 0 };
    this.checkAll = true;
    this.eventChange = new EventEmitter();
    this.favoriteMode = false;
    this.eventMapSetting = new EventEmitter();
    this.layoutMapConfigModel = this.mapConfigService.getLayoutConfig();
    const subscr = this.mapConfigService.layoutConfigUpdate$.subscribe(layoutMapConfig => {
      this.layoutMapConfigModel = layoutMapConfig;
      this.cdr.detectChanges();
    });
    this.unsubscribe.push(subscr);
    this.isShowMobile = false;
  }

  ngOnInit() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
;
      })
    ).subscribe();
    this.configMap = this.mapService.getConfigs();
    let clientRect = document.getElementsByClassName('mp-map-sidebar')[0].getBoundingClientRect();
    // this.maxHeightWapper = clientRect.height - 202;
    let clientRectHeader = document.getElementsByClassName('mp-map-sidebar-header')[0].getBoundingClientRect();
    this.maxHeightWapper = clientRect.height - (clientRectHeader.height + 45);
    this.cdr.detectChanges();
    if (this.eventEmiter) {
      this.eventEmiter.pipe(
        tap(data => {
          switch (data.type) {
            case "user":
              this.currentUser = data.data;
              this.loadDevice();
              this.getDeviceGroups();
              this.getDeviceType();
              break;
            case "geofence":
              this.listGeofence = data.data;
              break;
            case "tracking":
              if (data.data) {
                this.isShow = false;
                if (this.intervalRefesh) {
                  clearInterval(this.intervalRefesh);
                }
                if (this.mapEventChange) this.mapEventChange.unsubscribe();
              }
              else {
                this.isShow = true;
                this.loadDevice();
                this.mapEventChange = this.mapService.eventChange.pipe(
                  tap(data => {
                    this.mapServiceEventChange(data);
                  }),
                  takeUntil(new Subject())
                ).subscribe();
              }
              break;
          }
        }),
        takeUntil(new Subject())
      ).subscribe();
    }
    $(function () {
      $('.selectpicker-device-type').selectpicker();
    });
    this.mapEventChange = this.mapService.eventChange.pipe(
      tap(data => {
        this.mapServiceEventChange(data);
      }),
      takeUntil(new Subject())
    ).subscribe();
    let subscribeListDevice = this.listDeviceService.eventEmitterAction.subscribe((action: string) => {
      switch (action) {
        case "open":
          this.isShowMobile = true;
          break;
        case "close":
          this.isShowMobile = false;
          break;
      }
    });
    this.unsubscribe.push(subscribeListDevice);
    this.listDeviceService.showOnMobile = true;
  }

  ngOnDestroy() {
    if (this.intervalRefesh) {
      clearInterval(this.intervalRefesh);
    }
    this.mapService.addMarkers([]);
    this.unsubscribe.forEach(sb => sb.unsubscribe());
  }

  mapServiceEventChange(data: MapChangeEvent) {
    switch (data.type) {
      case "follow":
        this.listData = this.listData.map(x => {
          if (x.id == data.data.id) x.feature.follow = true;
          return x;
        });
        this.searchDevice();
        break;
      case "unfollow":
        this.listData = this.listData.map(x => {
          if (x.id == data.data.id) x.feature.follow = false;
          return x;
        });
        this.searchDevice();
        break;
      case "action__set_view_list_device":
        this.setViewItemSelected(data.data);
        break;
      case "action__require_favourite":
        if (this.listData.find(x => x.id == data.data.id))
          this.favoriteItem(data.data);
        break;
    }
    this.listDataOld = [...this.listData];
    this.cdr.detectChanges();


  }
  onResize($contentRect) {
    let clientRect = document.getElementsByClassName('mp-map-sidebar')[0].getBoundingClientRect();
    let clientRectHeader = document.getElementsByClassName('mp-map-sidebar-header')[0].getBoundingClientRect();
    // this.maxHeightWapper = clientRect.height - 202;
    let _this = this;
    setTimeout(() => {
      _this.maxHeightWapper = clientRect.height - (clientRectHeader.height + 45);
      $("#listDeviceContainer").css("max-height", _this.maxHeightWapper);
      $("#listDeviceContainer").css("min-height", _this.maxHeightWapper);
      _this.cdr.detectChanges();
    })
  }
  loadDevice() {
    let _this = this;
    this.listView = [];
    this.listData = [];
    this.listDataOld = [];
    this.cdr.detectChanges();
    if (this.intervalRefesh) clearInterval(this.intervalRefesh);
    this.intervalRefesh = setInterval(() => {
      _this.refreshDevice();
    }, _this.timeRefesh);


    this.isLoading = true;
    if (this.favoriteMode) {
      this.deviceService.favoriteList({ params: { groupId: -1, userId: this.currentUser.id, favorite: 1 } }).pipe(
        tap(data => {
          this.loadDeviceProcess(data);
        }),
        finalize(() => {
          this.isLoading = false;
          this.cdr.markForCheck();
        })
      ).subscribe();
    }
    else {
      this.trackingService.list({ params: { groupId: -1, userId: this.currentUser.id, favorite: 1 } }).pipe(
        tap(data => {
          this.loadDeviceProcess(data);
        }),
        finalize(() => {
          this.isLoading = false;
          this.cdr.markForCheck();
        })
      ).subscribe();
    }

  }
  loadDeviceProcess(data) {
    let dateTimeNow = data.datetime;
    let dataNew = this.trackingUtilUtil.processItems(data.result, dateTimeNow);
    this.processStatus(dataNew);
    let dataMap = [];
    dataNew.forEach(d => {
      let item = new Item(d);
      dataMap.push(item);
    });
    // set status
    this.mapService.addMarkers(dataMap, true); 
   
    // 
    dataNew.map(d => {
      d.feature.hideOnMap = false;
      return d;
    });
    // if (this.listData) {
    //   dataNew.map(d => {
    //     let itemOld = this.listData.find(x => x.id == d.id);
    //     if (itemOld) {
    //       d.feature = itemOld.feature;
    //       if (d.feature.follow) this.mapService.follow(d);
    //       if (d.feature.pin) this.mapService.pinMarker(d);
    //     }else {
    //       d.feature.hideOnMap = false;
    //     }
    //     return d;
    //   });
    // }
    this.listData = dataNew;
    this.listView = dataNew;
    this.listDataOld = [...dataNew];
    this.eventChange.emit({ type: 'list_data', data: this.listData });

    this.searchDevice();
    this.cdr.detectChanges();
  }
  refreshDeviceProcess(data) {
    let _this = this;
    let dateTimeNow = data.datetime;
    let dataNew = this.trackingUtilUtil.processItems(data.result, dateTimeNow);
    dataNew.forEach(d => {
      let index = _this.listData.findIndex(x => x.id == d.id);
      if (index >= 0) {
        _this.listData[index] = d;
        let itemOld = this.listDataOld.find(x => x.id == d.id);

        if (itemOld) {
          _this.listData[index].feature = itemOld.feature;
          if ((!_this.listData[index].address || _this.listData[index].address.length == 0) && _this.listData[index].lat == itemOld.lat
            && _this.listData[index].lng == itemOld.lng) {
            _this.listData[index].address = itemOld.address;
          }
        }
      }
      else {
        _this.listData.push(d);
      }
    });
    this.eventChange.emit({ type: 'list_data', data: this.listData });
    this.processStatus(this.listData);

    this.searchDevice(true);
    this.sortDevice();

    let dataMap = [];
    dataNew.forEach(d => {
      let item = new Item(d);
      dataMap.push(item);
    });
    
    this.mapService.updateMarkers(dataMap);
    this.cdr.detectChanges();

  }
  refreshDevice() {
    this.isLoading = true;
    if (this.favoriteMode) {
      this.deviceService.favoriteList({ params: { groupId: -1, userId: this.currentUser.id, favorite: 1, isChange: 1 } }).pipe(
        tap(data => {
          this.refreshDeviceProcess(data);
        }),
        finalize(() => {
          this.isLoading = false;
          this.cdr.markForCheck();
        })
      ).subscribe();
    }
    else {
      this.trackingService.listRefresh({ params: { groupId: -1, userId: this.currentUser.id, favorite: 1 } }).pipe(
        tap(data => {
          this.refreshDeviceProcess(data);
        }),
        finalize(() => {
          this.isLoading = false;
          this.cdr.markForCheck();

        })
      ).subscribe();
    }

  }
  processStatus(data?) {
    let _this = this;
    let dataTemp = this.listData;
    if (data) dataTemp = data;
    let status: { all: number, stop: number, running: number, expired: number, lostSignal: number,lostGPS:number }
      = { all: 0, stop: 0, running: 0, expired: 0, lostSignal: 0,lostGPS:0 };
    status.all = dataTemp.length;
    dataTemp.map(x => {
      switch (x.statusType.name) {
        case "lostGPS":
          // x.statusType = _this.configMap.status.lostGPS;
          status.lostGPS++;
          break;
        case "lostGPRS":
          // x.statusType = _this.configMap.status.lostGPRS;
          status.lostSignal++;
          break;
        case "historyTransfer":
          // x.statusType = _this.configMap.status.historyTransfer;
          status.lostSignal++;
          break;
        case "expired":
          // x.statusType = _this.configMap.status.expired;
          status.lostSignal++;
          break;
        case "stop":
          // x.statusType = _this.configMap.status.stop;
          status.stop++;
          break;
        case "run":
          // x.statusType = _this.configMap.status.run;
          status.running++;
          break;
        case "inactive":
          // x.statusType = _this.configMap.status.inactive;
          status.lostSignal++;
          break;
        case "nodata":
          // x.statusType = _this.configMap.status.nodata;
          status.lostSignal++;
          break;

        default:
          // x.statusType = _this.configMap.status.lostSignal;
          status.lostSignal++;
          break;
      }
    });
    _this.status = status;
  }

  selectCar(item) {
    //
    let itemTemp = JSON.parse(JSON.stringify(item));
    if (!item.address || item.address.length == 0) {

      if (item.lat != null && !item.lng != null) {
        let address = item.lat.toFixed(5) + ", " + item.lng.toFixed(5);
        item.addressLoading = true;
        this.cdr.detectChanges();
        this.mapUtilService.geocode({ params: { lat: item.lat, lng: item.lng } }).pipe(
          tap(
            data => {
              address = data.result.address;
            }
          ),
          finalize(() => {
            item.addressLoading = false;
            if (item.lat == itemTemp.lat && item.lng == itemTemp.lng) {
              item.address = address;
              itemTemp.address = address;
              this.mapService.updateMarkers([itemTemp]);
            }
            this.cdr.detectChanges();
            this.cdr.markForCheck();
          })
        ).subscribe();
      }

    }
    if (this.validateItem(item)) {
      // this.mapService.selectItem(item);
      // this.mapService.fitboundByIds([item.id]);
    }
    this.lastDeviceIdSelected = item.id;
    this.mapService.selectItem(item,18);
  }
  validateItem(item, allowFollow?: boolean): boolean {
    if (item.statusType.name == this.configMap.status.inactive.name) {
      this.toastService.show({ translate: "COMMON.LIST_DEVICE.DEVICE_INACTIVE", type: "info" });
      return false;
    }
    if (item.timestampUTC == null) {
      this.toastService.show({ translate: "COMMON.LIST_DEVICE.NOT_UPDATE_DATA", type: "info" });
      return false;
    }
    if (item.lat == null || item.lng == null || (item.lat == 0 && item.lng == 0)) {
      this.toastService.show({ translate: "COMMON.LIST_DEVICE.DEVICE_NOT_SET_COORDINATES", type: "info" });
      return false;
    }
    if (item.statusType.name == this.configMap.status.expired.name) {
      this.toastService.show({ translate: "COMMON.LIST_DEVICE.DEVICE_EXPIRED", type: "warning" });
      return false;
    }

    return true;
  }
  getDeviceGroups() {
    this.deviceGroupService.list({ params: { user_id: this.currentUser.id } }).pipe(
      tap(x => {
        this.deviceGroup = x.result;
        setTimeout(function () {
          $('.selectpicker-device-group').selectpicker("refresh");
        }, 100);
      }),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  getDeviceType() {
    this.deviceConfigService.get().then(data => {
      this.deviceType = data.deviceTypes;
      setTimeout(function () {
        $('.selectpicker-device-type').selectpicker("refresh");
      }, 100);
      this.cdr.markForCheck();
    });
  }

  offFilter() {
    this.showFilter = false;
  }

  onFilter() {
    this.showFilter = true;
  }

  filter() {
    this.offFilter();
    this.dataFilter.deviceType = ($('.selectpicker-device-type').val());
    this.dataFilter.deviceGroup = $('.selectpicker-device-group').val();
    if (this.dataFilter.deviceType.length > 0 || this.dataFilter.deviceGroup.length > 0)
      this.isResetFilter = true;
    this.searchDevice();
  }

  changeSort() {
    switch (this.dataFilter.sort) {
      case 'off':
        this.dataFilter.sort = "asc";
        break;
      case 'asc':
        this.dataFilter.sort = "desc";
        break;
      case 'desc':
        this.dataFilter.sort = "off";
        break;
    }
    this.sortDevice();
  }
  changeStatus(value) {
    this.dataFilter.status = value;
    this.searchDevice();
    
  }

  resetFilter() {
    $('.selectpicker-device-type').val('default').selectpicker("refresh");
    $('.selectpicker-device-group').val('default').selectpicker("refresh");
    this.dataFilter.deviceType = [];
    this.dataFilter.deviceGroup = [];
    this.isResetFilter = false;
    this.searchDevice();
  }

  changeKeySearch(value) {
    if (this.dataFilter.keySearch != this.acsii.transform(value).toLowerCase()) {
      this.dataFilter.keySearch = this.acsii.transform(value).toLowerCase();
      this.searchDevice();
    }
  }

  searchDevice(refresh?:boolean) {
    let _this = this;
    let data = [...this.listData];
    let dataSearch = this.dataFilter;
    if (dataSearch.keySearch.length > 0) {
      data = data.filter(x => {
        return _this.acsii.transform(x.imei).toLowerCase().includes(dataSearch.keySearch)
          || _this.acsii.transform(x.name).toLowerCase().includes(dataSearch.keySearch);

      });
    }
    // search status
    switch (dataSearch.status) {

      case 'driving':
        data = data.filter(x => x.statusType.name == this.configMap.status.run.name);
        break;
      case 'stop':
        data = data.filter(x => x.statusType.name == this.configMap.status.stop.name);
        break;
      case 'lost_gps':
        data = data.filter(x => x.statusType.name == this.configMap.status.lostGPS.name);
        break;
      case 'no_data':
        data = data.filter(x => {
          if (x.statusType.name == this.configMap.status.lostSignal.name
            || x.statusType.name == this.configMap.status.lostGPRS.name
            || x.statusType.name == this.configMap.status.expired.name
            || x.statusType.name == this.configMap.status.nodata.name
            || x.statusType.name == this.configMap.status.inactive.name
            || x.statusType.name == this.configMap.status.historyTransfer.name
          )
            return true;
        });
        break;
      default:
        break;
    }

    // search groups
    if (dataSearch.deviceGroup.length > 0) {
      data = data.filter(x => {
        let dGroup = dataSearch.deviceGroup.find(d => {
          return x.groups.some(g => g.id == d);
        });

        if (dGroup) return true;
      });
    }

    // search types    
    if (dataSearch.deviceType.length > 0) {
      data = data.filter(x => {
        return dataSearch.deviceType.some(d => d == x.type);
      })
    }

    this.listView = data;

    // update and remove  map;
    let dataUpdate = [];
    
    if(refresh){
      dataUpdate =  this.listData.map(x => {
        if(!this.listView.find(d=>d.id === x.id))
        {
          x.feature.hideOnMap = true; 
        }
        return new Item(x);
      });
    }
    else{
      dataUpdate =  this.listData.map(x => {
        if(!this.listView.find(d=>d.id === x.id))
        {
          x.feature.hideOnMap = true; 
        }
        else  x.feature.hideOnMap = false;
        return new Item(x);
      });
    }

    this.mapService.updateMarkers(dataUpdate);
    // this.mapService.removeItems(dataRemove);
    this.cdr.detectChanges();

    // update check 
    this.checkAll = this.checkIsCheckAll();
  }

  // sort by name
  sortDevice() {
    let data = [...this.listView];
    switch (this.dataFilter.sort) {
      case 'asc':
        data.sort(function (a, b) {
          var x = a.name.toLowerCase();
          var y = b.name.toLowerCase();
          if (x < y) { return -1; }
          if (x > y) { return 1; }
          return 0;
        });
        break;
      case 'desc':
        data.sort(function (a, b) {
          var x = a.name.toLowerCase();
          var y = b.name.toLowerCase();
          if (x < y) { return 1; }
          if (x > y) { return -1; }
          return 0;
        });
        break;
      case 'off':
        data = this.listData.filter(x => {
          return data.find(d => d.id == x.id);
        })
        break;
    }
    this.listView = data;
  }
  follow(item) {
    item = new Item(item);
    this.mapService.follow(item);
  }

  followPopup(item) {
    if (this.validateItem(item)) {
      this.mapService.eventChange.emit({ type: "followPopup", data: item });
    }
  }
  checkShowItem(item) {
    let indexSearch = this.listData.findIndex(x => x.id == item.id);
    if (indexSearch >= 0) {
      if (item.feature.hideOnMap === true) {
        this.listData[indexSearch].feature.hideOnMap = false;
      }
      else {
        this.listData[indexSearch].feature.hideOnMap = true;
      }
      this.updateCheckBoxItem(item);
    }
  }

  checkAllItem() {
    if (this.checkAll) {
      this.listData = this.listData.map(x => { 
        x.feature.hideOnMap = true;
        return x;
      });
    }
    else {
      this.listData = this.listData.map(x => {
        if (this.listView.findIndex(v => v.id == x.id) >= 0) {
          x.feature.hideOnMap = false;
        }
        else {
          x.feature.hideOnMap = true;
        }
        return x;
      })
    } 
    this.updateCheckBoxItem();
  }
  checkIsCheckAll() {
    // this.dataFilter.status 
    return this.listView.filter(x => x.feature.hideOnMap).length > 0 ? false : true;
  }
  updateCheckBoxItem(item?) {
    this.listDataOld = [...this.listData];
    
    this.checkAll = this.checkIsCheckAll();
    let dataMap = [];
    if (item) {
      let temp = new Item(item);
      dataMap.push(temp);
    }
    else {
      this.listData.forEach(d => {
        let temp = new Item(d);
        dataMap.push(temp);
      });
    }
    
    this.mapService.updateMarkers(dataMap);
    // update device.
    this.cdr.detectChanges();
  }
  trackingOneDevice(item, action?) {
    this.eventChange.emit({
      type: 'tracking', data: item, options: {
        tab: action
      }
    });
  }
  onFavoriteMode() {
    this.favoriteMode = !this.favoriteMode;
    // this.cdr.detectChanges();
    this.loadDevice();
    // this.searchDevice();
  }
  favoriteItem(item) {
    if (item.favorite === 1) {
      this.deviceService.favoriteDeleteByDeviceId({ params: { deviceId: item.id }, notifyGlobal: true }).pipe(
        tap(event => {
          this.listData = this.listData.map((x: any) => {
            if (x.id == item.id) {
              x.favorite = 0;
              this.mapService.updateMarkers([x]);
            }

            return x;
          });
          // remove item in list and maps
          if (this.favoriteMode) {
            this.listData = this.listData.filter(x => x.id != item.id);
            let itemMap = new Item(item);
            // this.mapService.updateMarkers([itemMap]);
            this.mapService.removeMarkers([itemMap]);

            this.searchDevice();
            this.cdr.detectChanges();
          }
          else {

            this.searchDevice();
            this.cdr.detectChanges();
          }
        }),
        finalize(this.cdr.markForCheck)
      ).subscribe();
    }
    else {
      this.deviceService.favoriteCreate({ deviceId: item.id }, { notifyGlobal: true }).pipe(
        tap(
          event => {
            this.listData = this.listData.map((x: any) => {
              if (x.id == item.id) {
                x.favorite = 1;

                this.mapService.updateMarkers([x]);
              }
              return x;
            });

            this.searchDevice();
          }
        ),
        finalize(this.cdr.markForCheck)
      ).subscribe();
    }
  }
  
  sendCommand(item){
    
    item.typeDevice = item.type;
    console.log(item);
    this.sendCommandEventEmitter.emit([item]);
  }

  mapSettingResult(result) {
    // if(result.status=="success"){
    //   this.layoutMapConfigModel = result.data;
    //   this.cdr.detectChanges();
    // }
  }
  openSettingMap(key) {
    this.eventMapSetting.emit(key);
  }

  checkShow(path: string, deep?: boolean) {
    let obj = objectPath.get(this.layoutMapConfigModel, path);
    if (deep && obj) {
      if (Object.values(obj).some(x => x === true)) return true;
      else return false
    }
    return objectPath.get(this.layoutMapConfigModel, path);
  }
  close() {
    this.listDeviceService.close();
  }
  setViewItemSelected(item: Item) {
    this.lastDeviceIdSelected = item.id;
    let element = document.getElementById(`carId_${item.id}`);
    if (element) {
      element.scrollIntoView({ behavior: "smooth", block: "center", inline: "nearest" });
    }
  }
}
