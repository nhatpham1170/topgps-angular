import { Component, OnInit, ChangeDetectorRef, Sanitizer, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Lightbox } from 'ngx-lightbox';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { NgxImageCompressService } from 'ngx-image-compress';
import { NgbModalRef, ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';

const minSize: number = 1;
const maxSize: number = 20;

export enum UploadFileStatus { ADD = 'add', REMOVE = 'remove', OLD = 'old' };

@Component({
  selector: 'kt-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss'],
  providers: [NgxImageCompressService]
})


export class UploadFileComponent implements OnInit {
  @Input('url') url?: any;
  @Input('buttonName') buttonName?: string;
  @Output('eventChange') eventChange: EventEmitter<any> = new EventEmitter();
  @Input('key') key?: string;
  @Input('size') size?: number = 1;
  @Input('urls') urls?: Array<any>;
  @Input('options') options?: UploadFileOptionModel;
  @ViewChild('fileUpload', { static: true }) fileUpload: ElementRef;
  public imageCropper: any;
  public images: Array<ImageUploadMode>;
  public currentFile: any;
  private optionDefault: UploadFileOptionModel;
  private modalReference: NgbModalRef;
  public closeResult: string;
  public croppedImage: any = '';
  public cropImageLoading: boolean = false;
  public imageEdit: ImageUploadMode;
  public validators: Array<string> = [];

  constructor(private cdr: ChangeDetectorRef, private sanitizer: DomSanitizer,
    private lightBox: Lightbox,
    private modalService: NgbModal,
    private imageCompressService: NgxImageCompressService,
    private translate: TranslateService) {
    if (this.key == undefined) {
      this.key = "uploadFile_" + Date.now();
    }
    if (this.size < minSize || this.size > maxSize) {
      this.size = minSize;
    }
    this.images = [];
    this.optionDefault = new UploadFileOptionModel();
    this.optionDefault.compress = true;
    this.optionDefault.maxLengthFile = 5;
    this.optionDefault.upload = true;
    this.optionDefault.close = true;
    this.optionDefault.zoom = true;
    this.optionDefault.crop = true;
  }

  ngOnInit() {
    if (this.options)
      this.options = Object.assign(this.optionDefault, this.options);
    else this.options = this.optionDefault;
    this.cdr.detectChanges();
    if (this.url != undefined) {
      const image = {
        url: this.url,
        name: "",
        id: this.randomId(),
        file: null,
        status: UploadFileStatus.OLD,
      }
      this.images.push(image);
    }
    if (this.urls != undefined) {
      this.urls.forEach(url => {
        const image = {
          url: url,
          name: "",
          id: this.randomId(),
          file: null,
          status: UploadFileStatus.OLD,
        }
        this.images.push(image);
      })
    }
  }

  async onFileChange(event, name: string = '') {
    this.validators = [];
    let checkSize = true;
    let checkLengthFile = true;
    if (event.target.files.length > 0) {
      for (let i = 0; i < event.target.files.length; i++) {
        const file = event.target.files[i];
        if (event.target.files[i].size / 1024 / 1024 >= this.options.maxLengthFile) {
          checkLengthFile = false;
        }
        else {
          // send file change
          await this.convertFileToBase64(file).then((image: any) => {
            if (image !== null) {
              if (this.size === 1) {
                this.images = [image];
              }
              else {
                if (this.images.length < this.size) {
                  this.images.push(image);
                }
                else {
                  checkSize = false;
                }
              }
            }
          });
        }
      }

      if (this.options.compress) {
        await this.compressFiles(this.images);
      }
      this.cdr.markForCheck();
      this.cdr.detectChanges();

      this.eventChange.emit(this.images);
    }
    this.fileUpload.nativeElement.value = "";
    // set validate
    if (!checkSize)
      this.validators.push(this.translate.instant('COMMON.UPLOAD_FILE.VALIDATION.MAX_COUNT', { maxCount: this.size }));
    else if (!checkLengthFile)
      this.validators.push(this.translate.instant('COMMON.UPLOAD_FILE.VALIDATION.LENGTH_FILE', { maxLength: this.options.maxLengthFile }));
  }

  /**
   * Zoom image
   * @param url 
   */
  zoomImage(url) {
    if (url && url.length > 0 && this.options.zoom) {
      const album: any = {
        src: this.sanitizer.bypassSecurityTrustUrl(url),
        caption: "",
        thumb: url
      };
      this.lightBox.open([album], 0);
    }

  }

  /**
   * Close and remove image
   * @param image 
   */
  onCloseImage(image) {
    let _this = this;
    let index = this.images.findIndex(x => x.id == image.id);
    if (index >= 0) {
      if (this.images[index].file === null) {
        this.images[index].status = UploadFileStatus.REMOVE;
      }
      else this.images.splice(index, 1);
    }
    this.eventChange.emit(this.images);
    this.cdr.detectChanges();
  }

  /**
   * Crop image
   * @param image 
   */
  onpenCropImage(content, image: ImageUploadMode) {
    let _this = this;
    if (image.status === UploadFileStatus.OLD) {
      fetch(image.url).then(res => res.blob().then(bold => {
        this.convertFileToBase64(bold).then((imgUrl: ImageUploadMode) => {
          _this.imageCropper = imgUrl.url;
        });
      }));
    }
    else {
      this.imageCropper = image.url;
    }
    this.croppedImage = null;
    this.cropImageLoading = true;
    this.imageEdit = image;

    this.modalReference = this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', backdrop: 'static' });
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    this.cdr.detectChanges();
  }

  /**
   * Save image cropped
   */
  onSaveCrop() {
    if (this.imageEdit.status === UploadFileStatus.OLD) {
      this.imageEdit.status = UploadFileStatus.REMOVE
      this.images.push(this.imageEdit);
    }
    this.images.map(x => {
      if (x.id == this.imageEdit.id && x.status !== UploadFileStatus.REMOVE) {
        if (this.imageEdit.status)
          x.url = this.croppedImage;
        x.file = this.dataURItoBlob(this.croppedImage);
      }
      return x;
    });
    this.eventChange.emit([...this.images]);
    this.modalReference.close();
    this.cdr.detectChanges();
  }

  /* ==== crop image ==== */
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    this.cdr.detectChanges();
  }

  imageLoaded() {
    // show cropper
    this.cropImageLoading = false;
    this.cdr.detectChanges();
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  /**
   * Get list images
   */
  public getImages() {
    return [...this.images];
  }

  /**
  * compress image 
  * @param images list images
  */
  async compressFiles(images: Array<ImageUploadMode> = []) {
    let configSize = [
      {
        quality: 10,
        sizeMin: 1000000
      },
      {
        quality: 30,
        sizeMin: 300000
      },
    ]

    if (images.length > 0) {
      for (let i = 0; i < images.length; i++) {
        if (images[i].status === UploadFileStatus.ADD && images[i].fileCompress == undefined) {
          let quality = 100;

          // check size 
          configSize.forEach(x => {
            if (this.imageCompressService.byteCount(images[i].url) >= x.sizeMin && quality === 100)
              quality = x.quality;
          });
          // console.log(this.imageCompressService.byteCount(images[i].url));
          // console.log(quality);
          
          // if (this.imageCompressService.byteCount(images[i].url) > 300000) {
            
          //   await this.imageCompressService.compressFile(images[i].url, 0, 100, 100).then(
          //     result => {
          //       images[i].url = result;

          //       images[i].file = this.dataURItoBlob(result);
          //     });
          // }
        }
      }
    }
    return images;
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url) || "";
  }
  private convertFileToBase64(file) {
    let _this = this;
    return new Promise((resolve, reject) => {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function () {
        reader.result;
        let mimeType = file.type;

        if (mimeType.match(/image\/*/) != null) {
          const image = {
            url: reader.result,
            name: file.name,
            id: _this.randomId(),
            file: file,
            status: UploadFileStatus.ADD,
          } as ImageUploadMode
          resolve(image);
        }
        else {
          resolve(null);
        }
      };
    })
  }

  private dataURItoBlob(dataURI) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    let byteString = atob(dataURI.split(',')[1]);

    // write the bytes of the string to an ArrayBuffer
    let ab = new ArrayBuffer(byteString.length);
    let ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    // write the ArrayBuffer to a blob, and you're done
    let bb = new Blob([ab]);
    return bb;
  }

  private randomId() {
    return `${Date.now()}${(Math.random() * 1000).toFixed(0)}`;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
export class ImageUploadMode {
  url: string;
  name: string;
  id: string;
  file: any;
  status: string;
  // compress?:boolean;
  fileCompress?: any;
}
export class UploadFileOptionModel {
  maxLengthFile?: number;//mb 
  compress?: boolean;
  maxCount?: number;
  upload?:boolean;
  close?:boolean;
  zoom?:boolean;
  crop?:boolean;
}
