import { ListIconPoiType, PoiTypeService } from './../../../../../core/admin/_services/poi-type.service';
import { landmarkIconClass } from './../../../../../core/map/consts/landmark/landmark-icon-class';
import { Component, Output, EventEmitter, Input, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl } from "@angular/forms";
import { tap } from 'rxjs/operators';
import { AsciiPipe } from '@core/_base/layout';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'kt-widget-check-list',
  templateUrl: './widget-check-list.component.html',
  styleUrls: ['./widget-check-list.component.scss'],
  providers: [AsciiPipe, ListIconPoiType]
})
export class WidgetCheckListComponent implements OnInit,OnDestroy {

  @Input() dataChangeEmiter?: EventEmitter<any>;
  @Input() data: any = [];
  @Input() filter?: string = 'name';
  @Input('placeholder') placeholder?: string = 'Search ...';
  @Input('size') size?: string = 'col-lg-4';
  @Input('name') name?: string = 'boxList';
  @Input('max-heigth') maxHeight?: number = 115;
  @Input('options') options?: { type?: string };
  @Input() listTypePoi: any[];
  @Input() currentUser: any;
  @Input() isClose?: boolean = false;
  @Input() isSwitch?: boolean = true;

  @Output() checkedChange = new EventEmitter(); 
  @Output() edit = new EventEmitter();
  @Output() delete = new EventEmitter();
  @Output() closeWidget = new EventEmitter();

  public formCheckList: FormGroup;
  public checkallboxed: boolean;
  public searchText: string = "";
  public landmarkIconClass = landmarkIconClass;
  public unsubscribe: Subscription[] = [];
  private savedData;

  constructor(private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private ascii: AsciiPipe) {
    if (this.options == undefined) {
      this.options = {};
    }
    this.buildForm();        
  }

  ngOnInit() {      
    if (this.dataChangeEmiter) {          
      const sub = this.dataChangeEmiter.pipe(
        tap(data => {                    
          data = data.map(x => {
            if (x[this.filter]) {
              x['textSearch'] = this.ascii.transform(x[this.filter]);
            }
            return x;
          });
          this.updateForm(data);
          this.cdr.detectChanges();
        })
      ).subscribe();
      this.unsubscribe.push(sub);
    }
    this.addIdToFormArray();
  }
  ngOnDestroy(){
    
    this.unsubscribe.forEach(x=>x.unsubscribe());
  }

  updateForm(data) {
    this.data = data;
    this.savedData = data;
    this.buildForm();
    if (this.options.type == 'landmark') {
      if (localStorage.getItem(`checkedLandmark_${this.currentUser.id}`)) {
        let savedLandmark = JSON.parse(localStorage.getItem(`checkedLandmark_${this.currentUser.id}`));
        this.data.forEach(landmark => {
          savedLandmark.forEach(saved => {
            if (landmark.id == saved.id) {
              landmark.checked = saved.checked;
            }
          })
        });
        this.getListIdSelected();
      }
    }
    if (this.options.type == 'geofence') {
      if (localStorage.getItem(`checkedGeofence_${this.currentUser.id}`)) {
        let savedGeofence = JSON.parse(localStorage.getItem(`checkedGeofence_${this.currentUser.id}`));
        this.data.forEach(geofence => {
          savedGeofence.forEach(saved => {
            if (geofence.id == saved.id) {
              geofence.checked = saved.checked;
            }
          })
        });
        this.getListIdSelected();
      }
    }
    this.addIdToFormArray();
  }

  private buildForm(): void {
    this.formCheckList = this.formBuilder.group({
      checkallboxed: [],
      searchText: [],
    });
  }

  addIdToFormArray() {
    this.formCheckList.controls[this.name] = new FormArray([]);
    for (let i = 0; i < this.data.length; i++) {
      let id = this.name + '_id_' + this.data[i].id;
      this.formCheckList.controls[this.name]['controls'][id] = new FormControl();
    }
    this.isCheckSelected();
  }

  isCheckSelected(id?: number) {
    if (this.data.length == 0) {
      this.checkallboxed = false;
    }
    else {
      this.checkallboxed = this.data.every(function (item: any) {
        return item.checked == true;
      });
    }

    this.getListIdSelected(id);
  }

  checkUncheckAll(sub?:boolean) {
    
    this.data = this.data.map(x => {
      x.checked = this.checkallboxed;
      return x;
    });
    this.getListIdSelected();
    // if(sub===true) {
    //   this.checkallboxed = !this.checkallboxed;
    //   this.cdr.detectChanges();
    // }
  }

  getListIdSelected(idChanged?: number) {
    let type = 'check'
    if (this.options.type === 'landmark') { type = 'check-landmark' };
    let dataSelected = this.data.filter(x => { return x.checked });
    let listNotSelected = this.data.filter(x => { return !x.checked });

    let listChanged = this.data.filter(x => { return x.id == idChanged });
    if (listChanged.length == 0 && this.data.length > 0) {
      listChanged = [...this.data];
    }
    let params = {
      listChecked: dataSelected,
      listUnChecked: listNotSelected,
      listChanged: listChanged,
      countChecked: dataSelected.length,
      type: type
    }
    if (this.options.type == 'landmark') {
      localStorage.setItem(`checkedLandmark_${this.currentUser.id}`, JSON.stringify(this.savedData));
    }

    if (this.options.type == 'geofence') {
      localStorage.setItem(`checkedGeofence_${this.currentUser.id}`, JSON.stringify(this.data));
    }
    this.checkedChange.emit(params);
  }

  searchItem(val) {
    this.searchText = val;
  }

  clearFilter() {
    this.searchText = "";
  }

  onSelecteMarker(item) {
    this.checkedChange.emit({
      type: "selecte-marker",
      items: [item]
    });
  }

  onSelecteLandmark(item) {
    this.checkedChange.emit({
      type: "selecte-landmark",
      items: [item]
    });
  }

  showEdit(item) {
    this.edit.emit({ type: this.options.type, data: item })
  }

  deleteItem(item) {
    this.delete.emit({ type: this.options.type, data: item });
  }

  selectType(select) {
    const selected = document.querySelectorAll('#select-type option:checked');
    const values = Array.from(selected).map(el => el['value']);
    if (select.value == '0') {
      this.data = this.savedData;
      this.getListIdSelected();
      return;
    }
    this.data = this.savedData;
    this.data = [];
    this.savedData.forEach(poi => {
      values.forEach(value => {
        if (poi.poiType.toString() == value) {
          this.data.push(poi)
        }
      })
    })
    this.getListIdSelected();
  }
}