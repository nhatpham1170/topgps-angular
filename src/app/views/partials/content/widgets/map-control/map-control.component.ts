import { Component, OnInit, Input, EventEmitter, ChangeDetectorRef, OnChanges, SimpleChanges, Output } from '@angular/core';
import { MapConfigModel, MapService, MapType } from '@core/utils/map';
import * as L from 'leaflet';
import { tap } from 'rxjs/operators';
import { environment } from '@env/environment.prod';

const GOOGLE_API_KEYS: Array<any> = environment.googleAPI.keys;
const GOOGLE_API_KEY_CHECK: string = environment.googleAPI.keyCheck;
declare var $;
declare var noUiSlider;
@Component({
  selector: 'kt-map-control',
  templateUrl: './map-control.component.html',
  styleUrls: ['./map-control.component.scss']
})
export class MapControlComponent implements OnInit, OnChanges {
  public mapConfig: MapConfigModel;
  public mapConfigOld: MapConfigModel;
  public mapType: Array<MapType>;
  @Input() mapService: MapService;
  @Input() style?: {};
  @Input() fixed?:boolean = false;
  @Input() isShowTag?: boolean= true;

  private scripts: any = {};
  public googleApiKey: string;
  public keyName: string;
  public form;
  public isClosed:boolean = false;
  @Output() popup = new EventEmitter();
  @Output() point = new EventEmitter();

  constructor(private cdr: ChangeDetectorRef) {
    this.keyName = new Date().getMilliseconds().toString();
  }

  ngOnInit() {
    let _this = this;
    this.mapConfig = this.mapService.getConfigs();

    this.mapConfigOld = JSON.parse(JSON.stringify(this.mapConfig));

    this.mapType = [];
    Object.keys(this.mapConfig.mapType).forEach(x => {
      _this.mapType.push(_this.mapConfig.mapType[x]);
    });
   
    if (this.mapService.eventChangeConfig) {
      this.mapService.eventChangeConfig.pipe(
        tap(data => {
          this.mapConfig = data.config;
          this.cdr.detectChanges();
        })
      ).subscribe()
    };
    this.isClosed = this.mapConfig.infoBox.collapse;
    setTimeout(() => {
      $(".selectpicker").selectpicker();
      $(`#mapType${_this.keyName}`).val(_this.mapConfig.features.mapType.value).selectpicker('refresh');
      let e:any = document.getElementById("kt_nouislider_brightness_"+_this.keyName);
      noUiSlider.create(e, {
          start: [_this.mapConfig.features.brightness.value],
          connect: [!0, !1],
          step: 1,
          range: {
              min: [25],
              max: [100]
          },
        //   pips: {
        //     mode: 'values',
        //     values: [50, 80],
        //     density: 4
        // }
      });
      e.noUiSlider.on('update', function (e, t) { 
        _this.mapConfig.features.brightness.value = Number.parseInt(e[t]);
        _this.brightness();
        _this.cdr.detectChanges();
      });     
    });
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  loadScript(key: string) {
    return new Promise((resolve, reject) => {
      let script: any = document.createElement('script');
      script.type = 'text/javascript';
      script.src = "https://maps.googleapis.com/maps/api/js?key=" + key;
      if (script.readyState) {  //IE
        script.onreadystatechange = () => {
          if (script.readyState === "loaded" || script.readyState === "complete") {
            script.onreadystatechange = null;
            // this.scripts[name].loaded = true;
            resolve({ script: name, loaded: true, status: 'Loaded' });
          }
        };
      }
      // else {  //Others
      //   script.onload = () => {
      //     this.scripts[name].loaded = true;
      //     resolve({ script: name, loaded: true, status: 'Loaded' });
      //   };
      // }
      script.onerror = (error: any) => resolve({ script: name, loaded: false, status: 'Loaded' });
      document.getElementsByTagName('head')[0].appendChild(script);
      resolve({ script: name, loaded: true, status: 'Loaded' });
    });
  }

  zoomIn() {
    this.mapService.zoomIn();
  }

  zoomOut() {
    this.mapService.zoomOut();
  }

  changeMapType(value) {
    if (value.includes("google")) {
      let random = Math.floor(Math.random() * GOOGLE_API_KEYS.length)
      this.googleApiKey = GOOGLE_API_KEYS[random];
      let loadedGoogleAPIKey = sessionStorage.getItem(GOOGLE_API_KEY_CHECK);
      if (loadedGoogleAPIKey != '1') {
        this.loadScript(this.googleApiKey).then((event: any) => {
          if (event.loaded) {
            sessionStorage.setItem(GOOGLE_API_KEY_CHECK, '1');
            let _this = this;
            setTimeout(() => {
              _this.mapService.changeMapType(value);
              _this.cdr.detectChanges();
            }, 300);
          }
          else sessionStorage.setItem(GOOGLE_API_KEY_CHECK, '0');

        });
      }
      else {
        this.mapService.changeMapType(value);
      }
    }
    else {
      this.mapService.changeMapType(value);
    
    }
    
  }

  fitbound() {
    this.mapService.fitbound();
  }
  location() {
    this.mapService.location();
  }
  traffic(value) {
    this.mapService.traffic(value);
  }

  /**
   * User cluster
   */
  groupChange() {
    // this.mapConfig.features.cluster.value = !this.mapConfig.features.cluster.value;    
    this.mapService.groupChange(this.mapConfig.features.cluster.value);
    this.mapService.saveConfigControls();
  }

  changStyleMarker(value) {
    // console.log(this.mapConfig.features);
    if (this.mapConfig.features.markerStyle.value != value) {
      this.mapConfig.features.markerStyle.value = value;
      this.mapService.changStyleMarker(value);
      this.mapService.saveConfigControls();
    }
  }

  geofenceChange() {
    this.mapConfig.features.geofence.value = !this.mapConfig.features.geofence.value;
    this.mapConfig.features.landmark.value = false;
  }

  landmarkChange() {
    this.mapConfig.features.landmark.value = !this.mapConfig.features.landmark.value;
    this.mapConfig.features.geofence.value = false
  }

  followChange() {
    this.mapService.followChange(this.mapConfig.features.follow.value);
  }

  centerPointChange() {
    this.mapConfig.features.centerPoint.value = !this.mapConfig.features.centerPoint.value
    this.mapService.centerPointChange();
    this.mapService.saveConfigControls();
  }

  markerPopupChange() {
    this.mapService.markerPopupChange(this.mapConfig.features.markerPopup.value);
    this.mapService.saveConfigControls();
  }

  fullScreen() {
    this.mapConfig.features.fullScreen.value = !this.mapConfig.features.fullScreen.value;
    this.mapService.fullScreen(this.mapConfig.features.fullScreen.value);
  }

  brightness() {
    this.mapService.setBrightness(this.mapConfig.features.brightness.value);
    this.mapService.saveConfigControls();
  }

  checkConfigInfoBox() {
    if (JSON.stringify(this.mapConfig.infoBox) !== JSON.stringify(this.mapConfigOld.infoBox)) {
      this.mapConfigOld.infoBox = JSON.parse(JSON.stringify(this.mapConfig.infoBox));
      this.mapService.saveConfigControls();      
      this.mapService.refresh();
    }
  }
  onToggle(){
    this.isClosed = !this.isClosed;
    if(this.isClosed)
    this.mapConfig.infoBox.collapse = this.isClosed;
    this.mapService.saveConfigControls();
  }

  showLandmarkPopup(type){
    this.popup.emit(type);
  }
  
}
