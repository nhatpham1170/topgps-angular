import { Component, Output, EventEmitter, Input, ViewChild ,ChangeDetectorRef,ElementRef} from '@angular/core';
import { ITreeOptions, TreeNode, ITreeState, TreeModel, TreeComponent } from 'angular-tree-component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitterService } from '@app/core/common/_service/eventEmitter.service';
import { UserTreeService } from '@app/core/common/_service/user-tree.service';
import { finalize, takeUntil, tap ,debounceTime} from 'rxjs/operators';
import { Subject } from 'rxjs';
import * as $ from 'jquery';
declare var $: any;

@Component({
  selector: 'kt-users-tree',
  templateUrl: './users-tree.component.html',
  styleUrls: ['./users-tree.component.scss']

})
export class UsersTreeComponent {
  @Input() nameTree: string;
  @Input() isLocalStore: string;
  @Output() messageEvent = new EventEmitter<string>();
  public nodes: any = [];// form users tree
  public idRoot: any; // id user login
  public nameRoot: any; // name user login
  public paramsRoot; // params user login
  public newNodes; // data child when click user parent
  public pageNo = 0;
  public pageSize = 10;
  public keyword = 'username'; // keyword search user
  public dataSearch: any[] = []; // data search
  public nameLocalStorage: string = 'treeState';
  private unsubscribe: Subject<any>;
  public isLoading = false;
  @ViewChild(TreeComponent, {static: false})
//   @ViewChild(TreeComponent)
  private nametree: TreeComponent;

  options: ITreeOptions = { // option users tree
    scrollOnActivate: true,
    getChildren: this.getChildren.bind(this),
    animateExpand: true,
    scrollContainer: <HTMLElement>document.body
  };

  constructor(
    private http: HttpClient,
    private tree: UserTreeService,
    private eventEmitterService: EventEmitterService,
    private cdr: ChangeDetectorRef

  ) {}

  ngOnInit(){
    this.getNameLocalStorage();
    this.buildUserTree();
    this.resetLocalStorage();
    this.updateTree();
  }

  updateTree(){
    this.eventEmitterService.loadUpdateTree.subscribe((params) => {
       let item = params.item;
       let parentId = params.parentId;
       let action = params.action;
       let node =  this.nametree.treeModel.getNodeById(parentId);
       if (node)
       {
        if(action == 'delete')
        {
          let position = node.data.children.findIndex(x=> x.id === item.id);
          node.data.children.splice(position, 1);
        }else{
          item.hasChildren = true
          node.data.children.unshift(item);
        }
        this.nametree.treeModel.update();
       }
    });
  }

  getNameLocalStorage() {
    this.nameLocalStorage = this.nameTree;
  }

  get state(): ITreeState {
    return localStorage.getItem(this.nameLocalStorage) && JSON.parse(localStorage.getItem(this.nameLocalStorage));
  }
  set state(state: ITreeState) {
    localStorage.setItem(this.nameLocalStorage, JSON.stringify(state));
  }
  // 
  getChildren(node: any) {
    const params = {
      'parentId': node.data.id,
      'pageNo': this.pageNo,
      'pageSize': this.pageSize
    };
    
    let promise = new Promise((resolve, reject) => {
      this.tree.getChildUsers(params).subscribe((data: any) => {
        
        if (data.status == 200) {
          this.newNodes = data.result;
          for (var i in this.newNodes) {
            this.newNodes[i].hasChildren = true;
          }
          resolve(this.newNodes);
        }
      });
    });
    return promise;
  }
  // get info users login (id,name)
  buildUserTree() {
    this.idRoot = 234;
    this.nameRoot = 'admin';
    this.paramsRoot = {
      'parentId': this.idRoot,
      'pageNo': this.pageNo,
      'pageSize': this.pageSize
    }
    this.buildRootTree();
  }
  // build users tree for users login
  buildRootTree() {
    this.tree.getChildUsers(this.paramsRoot)
    .pipe(
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((data: any) => {
      if (data.status == 200) {
        let childNodes = data.result.map((c) => Object.assign({}, c));
        // this.nodes = data.result.map((c) => Object.assign({}, c));
        
        for (var i in childNodes) {
          childNodes[i].hasChildren = true;
        }
        this.nodes = [
          {
            id: this.idRoot,
            name: this.nameRoot,
            children: childNodes,
            root:true,
            hasChildren: true,
          }
        ];
        let expandedNodeIds = {};
        let activeNodeIds = {};
        expandedNodeIds[this.idRoot] = true;
        activeNodeIds[this.idRoot] = true;
        this.setTreeState(expandedNodeIds, activeNodeIds);// set display id tree
      }
    });
  }

  setTreeState(expandedNodeIds, activeNodeIds) {
    let treeState = {
      activeNodeIds: {},
      expandedNodeIds: {},
      focusedNodeId: ''
    };
    treeState.expandedNodeIds = expandedNodeIds;
    treeState.activeNodeIds = activeNodeIds;

    if (!localStorage.getItem(this.nameLocalStorage)) {
      localStorage.setItem(this.nameLocalStorage, JSON.stringify(treeState));
    }
  }

  // load users when click item user tree
  loadUsers(id) {
    if(this.isLocalStore){
      this.eventEmitterService.userTreeIdSelect(id);
    }else{
      this.eventEmitterService.loadDataDefault(id);
    }
  }

  //  select item load form user tree
  selectEvent(item, string) {
    this.tree.setNodeActive(item,this.nameLocalStorage,false);
    this.loadUsers(item.id);// load data user
    this.scrollToEl(item.id);
  }

  scrollToEl(id){
    setTimeout(() => {
      document.getElementById(this.nameTree+'_'+id).scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' });
     },800);
  }

  onChangeSearch(val: string) {
    let params = {
      "searchText": val
    }
    this.isLoading = true;
    setTimeout(() => {
      this.tree.searchChildUsers(params)
      .pipe(
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((data: any) => {
        this.dataSearch = data.result;
        this.isLoading = false;
      });
     }, 500);
  }

  resetLocalStorage() {
    this.eventEmitterService.resetLocalStorage.subscribe(() => {
      if (!this.isLocalStore) {
        localStorage.removeItem(this.nameLocalStorage);
      }
    });

  }

}