// Angular
import {
  EventEmitter,
  Component,
  ElementRef,
  OnInit,
  Input,
  ChangeDetectorRef
} from '@angular/core';
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import { ReportFuelService } from '@core/report/_services/report-fuel.service';
am4core.useTheme(am4themes_animated);
import { finalize, takeUntil, tap, debounceTime,delay } from 'rxjs/operators';
import { UserDateAdvPipe } from '@core/_base/layout/pipes/user-date-adv.pipe';
import { CurrentUserService } from '@core/auth';
import { TranslateService } from '@ngx-translate/core';
import { PdfmakeService,} from '@core/utils';
import { DatePipe } from '@angular/common';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
@Component({
selector: 'kt-fuel-chart-global',
templateUrl: './fuel-chart-global.component.html',
styleUrls: ['./fuel-chart-global.component.scss'],
providers:[UserDateAdvPipe]
})
export class FuelChartGlobalComponent implements OnInit {
@Input() eventUpdateChart: EventEmitter<any>; // event for update and refesh by parrent
@Input() show:boolean = true; // event for update and refesh by parrent
@Input() showChangeFuel:boolean = false; // event for update and refesh by parrent
@Input() isDownload?: EventEmitter<any>;
  public timeStart:string;
  public timeEnd:string;
  public sensorIds:string;
  public deviceIds:string;
  public listChart:any = [];
  public chartData : any = [];
  public isLoading : boolean = true;
  public chart:any;
  public timeZone:string;
  public deviceName: string;
  public dateFormat: string;
  public timeFormat:string;
  public listChangeFuel:any = [
  
  ];
  public listSummary:any = [];
constructor(
      private pdf : PdfmakeService,
      private Fuel: ReportFuelService,
      // private unsubscribe: Subject<any>,
      private cdr: ChangeDetectorRef,
      private userDateAdv : UserDateAdvPipe,
      private currentUserService: CurrentUserService,
      private translate: TranslateService,
      private currentUser: CurrentUserService,
      public datepipe: DatePipe,
) {
      // this.unsubscribe = new Subject();
      this.eventUpdateChart = new EventEmitter();
      this.isDownload = new EventEmitter();
      this.timeZone = this.currentUser.currentUser.timezone;
      this.dateFormat = this.currentUser.dateFormatPipe;
      this.timeFormat = this.currentUser.timeFormat;

  }

ngOnInit(): void {
      if(this.isDownload)
      {
        this.isDownload.subscribe(params => {
          if(params.download) this.download();
        });
      }
      if(this.eventUpdateChart)
      {
        // console.log(1);
        am4core.disposeAllCharts();
        if(this.chart) this.chart.dispose();
        this.showChangeFuel = false;
        this.eventUpdateChart.subscribe(params => {
          // console.log(params);

          this.timeStart = params.dateStartChart;
          this.timeEnd = params.dateEndChart;
          this.sensorIds = params.sensorIdSearhChart;
          this.deviceIds = params.deviceIdSearhChart;
          this.deviceName = params.deviceName;

          am4core.disposeAllCharts();
          if(this.chart) this.chart.dispose();
          this.drawChart();
      });
      }        
  }

      
  ngOnDestroy():void {
    am4core.disposeAllCharts(); 
}

download()
{
  if(!this.chart.exporting) this.drawChart;
    this.chart.exporting.getImageAdvanced( "png" ).then( ( data ) => {
      this.exportFilePDF(data);
   } );

}

renderFuelSummary(data){
  let result = [];
  data.forEach(element => {
    let rows = [];
    rows.push({ text: element.fuelStart,alignment:"center",fillColor:"#fff",bold:false});
    rows.push({ text: element.totalAdd,alignment:"center",fillColor:"#fff",bold:false});
    rows.push({ text: element.totalRemove,alignment:"center",fillColor:"#fff",bold:false});
    rows.push({ text: element.consume,alignment:"center",fillColor:"#fff",bold:false});
    rows.push({ text: element.fuelEnd,alignment:"center",fillColor:"#fff",bold:false});
    result.push(rows);
  });
  return result;

}

renderFuelChange(data)
{

 let result = [];

 let count = 1;
 data.forEach(element => {
   let rows = [];
  let add = 0;
  let remove = 0;
  if(element.add) add = element.totalOffset;
  if(!element.add) remove = element.totalOffset;

   let timeStart =  this.userDateAdv.transform(element.startTime,'datetime');
   let timeEnd =  this.userDateAdv.transform(element.endTime,'datetime');

   let itemtimeStart = this.renderText(timeStart);
   rows.push({ text: count,alignment:"center",fillColor:"#fff",bold:false});
   rows.push({ text: timeStart,alignment:"center",fillColor:"#fff",bold:false});
   rows.push(element.address);
   rows.push({ text: element.startVolume,alignment:"center",fillColor:"#fff",bold:false});

   rows.push({ text: timeEnd,alignment:"center",fillColor:"#fff",bold:false});
   rows.push(element.address);
   rows.push({ text: element.endVolume,alignment:"center",fillColor:"#fff",bold:false});
   rows.push({ text: add,alignment:"center",fillColor:"#fff",bold:true});
   rows.push({ text: remove,alignment:"center",fillColor:"#fff",bold:true});
   count++;
   result.push(rows);
 });
return result;
}

renderText(data){
  let item = { text: data,alignment:"left",fillColor:"#fff",bold:false};
  return item;
}
  
exportFilePDF(base64){
  let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
  let _this = this;
  let obj;
  let header;
  header = {
        title: this.translate.instant('REPORT.FUEL.COLUMN.FUEL_GRAPH'),
        params:{
          timeStart:": "+this.timeStart,
          timeEnd:": "+this.timeEnd,
          deviceName:": "+this.deviceName,
          timeZone:": "+this.timeZone,
        },
   };
  let content_header = this.pdf.getHeader(header,{info:{}});
  let content_chart = [];
  this.listChart.forEach(chart => {
    let tableFuelChange = {};
    let tableFuelSummary = {};

    if(chart.fuelChange.length>0)
    {
      let fuelChange = this.renderFuelChange(chart.fuelChange);
      let body = [
        [ 
          { text: '#',rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},

          { text: this.translate.instant('REPORT.FUEL.CHART.START'),colSpan:3,alignment:"center",fillColor:"#EEEEEE",bold:true},
          { text:''},
          { text:''},

          { text: this.translate.instant('REPORT.FUEL.CHART.END'),colSpan:3,alignment:"center",fillColor:"#EEEEEE",bold:true},
          { text:''},
          { text:''},

          { text: this.translate.instant('REPORT.FUEL.CHART.CHANGE'),colSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true},
          '',
        
        ],
        [
          '',
         
          { text: this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
          { text: this.translate.instant('COMMON.COLUMN.ADDRESS'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
          { text: this.translate.instant('REPORT.FUEL.GENERAL.REPORT_FUEL'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},

          { text: this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
          { text: this.translate.instant('COMMON.COLUMN.ADDRESS'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},  
          { text: this.translate.instant('REPORT.FUEL.GENERAL.REPORT_FUEL'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},

          { text: this.translate.instant('REPORT.FUEL.GENERAL.ADD'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
          { text: this.translate.instant('REPORT.FUEL.GENERAL.REMOVE'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},

        ]
      ];
      body = body.concat(fuelChange);
      tableFuelChange =  {
        table: {
          headerRows: 2,
          widths: ['4%','8%','24%','8%','8%','24%','8%','8%','8%'],
          body: body
        }
      };
    }

    if(chart.listSummary.length>0)
    {
      let fuelsummary = this.renderFuelSummary(chart.listSummary);
      let bodySummay = [
        [ 
          { text: this.translate.instant('REPORT.FUEL.CHART.FUEL_START'),alignment:"center",fillColor:"#EEEEEE",bold:true},
          { text: this.translate.instant('REPORT.FUEL.GENERAL.ADD'),alignment:"center",fillColor:"#EEEEEE",bold:true},
          { text: this.translate.instant('REPORT.FUEL.GENERAL.REMOVE'),alignment:"center",fillColor:"#EEEEEE",bold:true},
          { text: this.translate.instant('REPORT.FUEL.CHART.CONSUME'),alignment:"center",fillColor:"#EEEEEE",bold:true},
          { text: this.translate.instant('REPORT.FUEL.CHART.FUEL_END'),alignment:"center",fillColor:"#EEEEEE",bold:true},   
        ]
     
      ];
      bodySummay = bodySummay.concat(fuelsummary);
      tableFuelSummary =  {
        table: {
          headerRows: 1,
          widths: ['10%','10%','10%','10%','10%'],
          body: bodySummay
        }
      };
    }


    let nameChart = chart.name;
    let unit = chart.parameters.unit;
    let sensor = nameChart+" ("+unit+")";
    content_chart =  [
      // {
      //   text:nameChart,
      //   alignment: 'center',
      //   fontSize: 14,
      //   bold: true,
      //   color:"#CF0000"
      // },
      {
        text:sensor,
        fontSize: 11,
        bold: true,
        margin: [10, 0]
      },
      {
        image:base64,
        width: 750,
        margin: [0,0,0, 10]
      },
      {
        text:''
      },
      {
        text:this.translate.instant('REPORT.FUEL.CHART.SUMMARY'),
        fontSize: 11,
        bold: true,
        margin: [0, 8]
      },
      tableFuelSummary,
      {
        text:''
      },
      {
        text:this.translate.instant('REPORT.FUEL.CHART.HISTORY'),    
        fontSize: 11,
        bold: true,
        margin: [0, 15, 0, 8]
      },
      tableFuelChange
    ];
  });
  obj = {
    pageOrientation: 'landscape',
    content: [  
      content_header,
      content_chart
    ],
    footer:function(currentPage, pageCount) { 
            return {
              columns: [
                {
                  text: _this.datepipe.transform(_this.timeStart, dateTimeFormat)+ ' - ' + _this.datepipe.transform(_this.timeEnd, dateTimeFormat)+', '+_this.deviceName,
                  alignment : 'left',
                  margin:[35,10,20,0],
                },
                {
                  text:'Page '+currentPage.toString() + '/' + pageCount,
                  alignment : 'right',
                  margin:[0,10,30,0],
                }
              ]
            }           
            },
    defaultStyle: {
      fontSize: 8,  
    }
  };
let file_name = this.pdf.getFileName(header,'FuelChart');
pdfMake.createPdf(obj).download(file_name);
  // this.xlsx.exportFileTest([data], config, {});

}

  renderChart()
  {
      this.listChart.forEach(chartItem => {
          let min = chartItem.parameters.min;
          let max = parseInt(chartItem.parameters.max);
          let unit = chartItem.parameters.unit;
          let totalAdd = 0;
          let totalRemove = 0;
          let fuelStart = 0;
          let fuelEnd = 0;
          max = max + (max*10)/100;
       
          this.chart = am4core.create("chartdiv"+chartItem.id, am4charts.XYChart);
          // point.date = new Date(this.userDateAdv.transform(point.time, "YYYY-MM-DD HH:mm:ss"));
          chartItem.details = chartItem.details.map(data=>{
       
                data.date = new Date(data.date);
                data.color = am4core.color(data.indexColor);
                  return data;
              }
          );

          if(chartItem.fuelChange.length > 0)
          {
            chartItem.fuelChange.map(fuel =>{
              fuel.totalOffset = Math.round(fuel.totalOffset * 100) / 100 ;
              if(fuel.add) totalAdd = totalAdd + fuel.totalOffset;
              if(!fuel.add) totalRemove = totalRemove + fuel.totalOffset;
              return fuel;
            })
            this.listChangeFuel = chartItem.fuelChange;
          }
          if(chartItem.details.length > 0)
          {
            fuelStart = chartItem.details[0].volume;
            fuelEnd  = chartItem.details[chartItem.details.length-1].volume;
          }
          let consume = totalAdd + fuelStart - fuelEnd - totalRemove;
          consume = Math.round(consume * 100) / 100 ;

          if (consume < 0) consume = 0;

          this.listSummary = [
            {
              fuelStart : fuelStart,
              fuelEnd: fuelEnd,
              totalAdd: totalAdd,
              totalRemove:totalRemove,
              consume: consume
            }
          ];
          chartItem.listSummary = this.listSummary;
          this.chart.leftAxesContainer.layout = "vertical";
          this.chart.paddingTop = 30;
          this.chart.cursor = new am4charts.XYCursor();
          this.chart.data = chartItem.details;
          this.chart.dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss";
          this.chart.paddingRight = 10;
          // set xAxes

          let dateAxis = this.chart.xAxes.push(new am4charts.DateAxis());
          dateAxis.renderer.grid.template.location = 0;
          dateAxis.renderer.minGridDistance = 50;
          dateAxis.renderer.line.strokeWidth  = 2;
          dateAxis.renderer.line.strokeOpacity = 1;
          dateAxis.renderer.line.stroke = am4core.color("#e2e5ec");

          let date1 = new Date(this.timeEnd);
          let date2 = new Date(this.timeStart);
          let  diffTime = Math.abs(date2.getTime() - date1.getTime());
   
          if(diffTime > 1728000000)
           {
            dateAxis.gridIntervals.setAll([
              { timeUnit: "hour", count: 1 },
              { timeUnit: "hour", count: 6 },
              { timeUnit: "hour", count: 12 },
              { timeUnit: "hour", count: 18 },
   

            ]);
          }

          if(diffTime > 2160000000)
          {
            dateAxis.gridIntervals.setAll([
              { timeUnit: "hour", count: 1 },
              { timeUnit: "hour", count: 12 },
              { timeUnit: "hour", count: 20 },
             
            ]);
          }
     
          // dateAxis.dateFormatter.dateFormat = "yyyy-MM-dd HH:mm";
          dateAxis.dateFormats.setKey("day", "yyyy-MM-dd HH:mm:ss");
          dateAxis.periodChangeDateFormats.setKey("day", "[bold]d[/]");
   
          // set yAxes
          let valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());
          valueAxis.min = 0;
          valueAxis.max = max;
          valueAxis.strictMinMax = true; 
          valueAxis.renderer.line.strokeOpacity = 1;
          valueAxis.renderer.line.strokeWidth  = 2;
          valueAxis.renderer.line.stroke = am4core.color("#e2e5ec");

          if (max != undefined && this.chart.data.length > 0) {
            let range = valueAxis.axisRanges.create();
            range.value = parseInt(chartItem.parameters.max);
            range.grid.stroke = am4core.color("#A96478");
            range.grid.strokeWidth = 1;
            range.grid.strokeOpacity = 1;
            range.label.inside = true;
            range.label.text = this.translate.instant('REPORT.FUEL.GENERAL.MAX_FUEL')+' ('+parseInt(chartItem.parameters.max)+')';
            range.label.fill = range.grid.stroke;
            range.label.align = "right";
            range.label.verticalCenter = "bottom";
            range.label.fontWeight = '600';
            range.label.fontSize = 13;
            range.label.paddingTop = 20;
          }
          // valueAxis.strictMinMax = true;
          valueAxis.renderer.minGridDistance = 50;
          //style chart
          // Create series
          let series = this.chart.series.push(new am4charts.LineSeries());
          series.dataFields.valueY = "volume"; 
          // series.dataFields.valueY = "volume";
          series.propertyFields.stroke = "color";
          series.propertyFields.fill = "color";
          series.propertyFields.opacity = "0.5";
          series.dataFields.dateX = "date";
          series.fillOpacity = 0.5;
          series.strokeWidth = 2;
          series.minBulletDistance = 10;
          series.tooltipText = "{valueY}";
          series.tooltip.pointerOrientation = "vertical";
          series.tooltip.background.cornerRadius = 20;
          series.tooltip.background.fillOpacity = 0.5;
          series.tooltip.label.padding(12,12,12,12)
          series.tooltipText = "{dateX}: [bold]{valueY.formatNumber('#.00')}[/] Lít";
          // series.tooltipText = "{dateX.formatDate('yyyy-mm')}: {valueY.formatNumber('#.00')}";
          // axisTitle
          // let axisTitle = series.createChild(am4core.Label);
          // axisTitle.layout = "absolute";
          // axisTitle.html = this.translate.instant('REPORT.FUEL.GENERAL.REPORT_FUEL')+" ("+unit+")";
          // axisTitle.fontWeight = '600';
          // axisTitle.fontSize = 13;
          // axisTitle.paddingTop = -25;
  
          // no data
          if (!this.chart.data || this.chart.data.length == 0) {
              this.listChangeFuel = [];
              let axisNodata = this.chart.createChild(am4core.Label);
              axisNodata.fill = am4core.color("rgb(87, 87, 87)");
              axisNodata.html = "<i class='icon-database'></i> " + this.translate.instant("MAP.TRACKING.CHART.NO_DATA");
              axisNodata.fontWeight = '600';
              axisNodata.fontSize = 13;
              axisNodata.isMeasured = false;
              axisNodata.x = am4core.percent(50);
              axisNodata.horizontalCenter = "middle";
              axisNodata.verticalCenter = "middle";
              valueAxis.renderer.grid.template.disabled = true; 
              axisNodata.paddingTop = 200;
              axisNodata.y = 50;
            }
          
              this.isLoading = false;             
              this.showChangeFuel = true;
          // chart.scrollbarX = new am4charts.XYChartScrollbar();
      });
    
  }

async  drawChart()
  {
      this.isLoading = true;
      let params = {
          timeTo:this.timeEnd,
          timeFrom:this.timeStart,
          deviceId:this.deviceIds,
          sensorIds:this.sensorIds
        }  
     await  this.Fuel.detail(params,{notifyGlobal:true})
      .pipe(
    
       tap((result: any) => {
              if (result.status == 200) {
                let _this = this;
                let i = 0;
                let indexColor;

                result.result =  result.result.map(item => {
                  item.sensorTemplate = {
                    parameters:item.parameters
                  };
                  item.details = item.details.map(detail =>{
                      let timestampUtc = _this.userDateAdv.transform(detail.timestampUtc,'YYYY-MM-DD HH:mm:ss');
                  
                      // add color to previous data item depending on whether current value is less or more than previous value
                      if(detail.add){
                          indexColor = '#348ae2';//'#0abb87'
                      } 
                      if(detail.remove){
                          indexColor = '#348ae2'; //'#fd397a'
                      }
                      if(!detail.add && ! detail.remove){
                          indexColor = '#348ae2';
                      }
                      detail.date = timestampUtc;
                      detail.indexColor = indexColor;
                      return detail;
                  })
                    i++;
                    return item;
                   
                });
               
                this.listChart = result.result;
                setTimeout(()=>{   

                  this.renderChart();
                  this.cdr.detectChanges();
                  this.cdr.markForCheck();

                  // this.isLoading = false;             
                }, 500);
              }
        }),
        finalize(() => {
          this.cdr.markForCheck();
          
        })
      ).toPromise();   
  
  }
  


}
