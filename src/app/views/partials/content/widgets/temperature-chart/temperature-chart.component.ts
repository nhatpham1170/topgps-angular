// Angular
import {
    EventEmitter,
    Component,
    ElementRef,
    OnInit,
    Output,
    Input,
    ChangeDetectorRef
  } from '@angular/core';
  import am4themes_animated from "@amcharts/amcharts4/themes/animated";
  import * as am4core from "@amcharts/amcharts4/core";
  import * as am4charts from "@amcharts/amcharts4/charts";
  import { ReportTemperatureService } from '@core/report';
  am4core.useTheme(am4themes_animated);
  import { finalize, takeUntil, tap, debounceTime,delay } from 'rxjs/operators';
  import { UserDateAdvPipe } from '@core/_base/layout/pipes/user-date-adv.pipe';
  import { CurrentUserService } from '@core/auth';
  import { TranslateService } from '@ngx-translate/core';
  import { PdfModel } from '@core/utils/pdfmake/pdfmake.service';
  import { PdfmakeService,} from '@core/utils';
  import { DatePipe } from '@angular/common';
  import pdfMake from 'pdfmake/build/pdfmake';
  import pdfFonts from 'pdfmake/build/vfs_fonts';
  pdfMake.vfs = pdfFonts.pdfMake.vfs;

  @Component({
  selector: 'kt-temperature-chart',
  templateUrl: './temperature-chart.component.html',
  styleUrls: ['./temperature-chart.component.scss'],
  providers:[UserDateAdvPipe,DatePipe]
  })
  export class TemperatureComponent implements OnInit {
  @Input() eventUpdateChart: EventEmitter<any>; // event for update and refesh by parrent
  @Input() show:boolean = true; // event for update and refesh by parrent
  @Input() showChangeTemperature:boolean = false; // event for update and refesh by parrent
  @Input() isDownload: EventEmitter<any>;

    public timeStart:string;
    public timeEnd:string;
    public sensorIds:string;
    public deviceIds:string;
    public listChart:any = [];
    public chartData : any = [];
    public isLoading : boolean = true;
    public chart:any;
    public timeZone:string;
    public deviceName: string;
    public dateFormat: string;
    public timeFormat:string;
    public unit:string;
    public listChangeTemperature:any = [
    
    ];
    public showPoints:boolean = false;
    constructor(
        private pdf : PdfmakeService,
        private Temperature: ReportTemperatureService,
        // private unsubscribe: Subject<any>,
        private cdr: ChangeDetectorRef,
        private userDateAdv : UserDateAdvPipe,
        private currentUserService: CurrentUserService,
        private translate: TranslateService,
        private currentUser: CurrentUserService,
        public datepipe: DatePipe,

  ) {
        // this.unsubscribe = new Subject();
        this.eventUpdateChart = new EventEmitter();
        this.timeZone = this.currentUser.currentUser.timezone;
        this.dateFormat = this.currentUser.dateFormatPipe;
        this.timeFormat = this.currentUser.timeFormat;

    }
  
  ngOnInit(): void {
        if(this.isDownload)
        {
          this.isDownload.subscribe(params => {
            if(params.download) this.download();
          });
        }


        if(this.eventUpdateChart)
        {
          // console.log(1);
          am4core.disposeAllCharts();
          if(this.chart) this.chart.dispose();
          this.showChangeTemperature = false;
          this.eventUpdateChart.subscribe(params => {
            // console.log(params);
            this.timeStart = params.dateStartChart;
            this.timeEnd = params.dateEndChart;
            this.sensorIds = params.sensorIdSearhChart;
            this.deviceIds = params.deviceIdSearhChart;
            // this.showPoints = params.showPoints;
            this.deviceName = params.deviceName
            am4core.disposeAllCharts();
            if(this.chart) this.chart.dispose();
            this.drawChart();
        });
        }        
    }
  
    ngOnDestroy():void {
      am4core.disposeAllCharts(); 
  }  
  changeShowPoints(check)
  {
    this.showPoints = !check;
  }
  download()
  {
    if(!this.chart.exporting) this.drawChart;
      this.chart.exporting.getImageAdvanced( "png" ).then( ( data ) => {
        this.exportFilePDF(data);
     } );
 
  }

  exportFilePDF(base64){
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let _this = this;
    let obj;
    let header;
    header = {
          title: this.translate.instant('REPORT.TEMPERATURE.GENERAL.REPORT_TEMPERATURE'),
          params:{
            timeStart:": "+this.timeStart,
            timeEnd:": "+this.timeEnd,
            deviceName:": "+this.deviceName,
            timeZone:": "+this.timeZone,
          },
     };
    let content_header = this.pdf.getHeader(header,{info:{}});
    let content_chart = [];
    this.listChart.forEach(chart => {
      let nameChart = chart.name;
      let unit = chart.parameters.unit;
      this.unit = unit;
      let sensor = nameChart+" ("+unit+")";
      content_chart =  [
        // {
        //   text:nameChart,
        //   alignment: 'center',
        //   fontSize: 14,
        //   bold: true,
        //   color:"#CF0000"
        // },
        {
          text:sensor,
          fontSize: 11,
          bold: true,
          // color:"#CF0000",
          margin: [10, 0]
        },
        {
          image:base64,
          width: 750,
          margin: [0,0,0, 10]
        },
        {
          text:''
        }
      ];
    });
    obj = {
      pageOrientation: 'landscape',
      content: [  
        content_header,
        content_chart
      ],
      footer:function(currentPage, pageCount) { 
              return {
                columns: [
                  {
                    text: _this.datepipe.transform(_this.timeStart, dateTimeFormat)+ ' - ' + _this.datepipe.transform(_this.timeEnd, dateTimeFormat)+', '+_this.deviceName,
                    alignment : 'left',
                    margin:[35,10,20,0],
                  },
                  {
                    text:'Page '+currentPage.toString() + '/' + pageCount,
                    alignment : 'right',
                    margin:[0,10,30,0],
                  }
                ]
              }           
              },
      defaultStyle: {
        fontSize: 8,  
      }
    };
  let file_name = this.pdf.getFileName(header,'ReportTemperature');
  pdfMake.createPdf(obj).download(file_name);
    // this.xlsx.exportFileTest([data], config, {});
 
  }
    
    
    renderChart()
    {
        
        this.listChart.forEach((chartItem,i) => {
            let min = parseInt(chartItem.parameters.min);
            let max = parseInt(chartItem.parameters.max);
            let unit = chartItem.parameters.unit;
  
            max = max + (max*20)/100;
         
            this.chart = am4core.create("chartdiv"+i, am4charts.XYChart);
            // point.date = new Date(this.userDateAdv.transform(point.time, "YYYY-MM-DD HH:mm:ss"));
            chartItem.details = chartItem.details.map(data=>{
         
                  data.date = new Date(data.date);
                  data.color = am4core.color(data.indexColor);
                    return data;
                }
            );
            
            this.chart.leftAxesContainer.layout = "vertical";
            this.chart.paddingTop = 30;
            this.chart.cursor = new am4charts.XYCursor();
            this.chart.data = chartItem.details;
            this.chart.dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss";
            this.chart.paddingRight = 10;
          
            let dateAxis = this.chart.xAxes.push(new am4charts.DateAxis());
            dateAxis.renderer.grid.template.location = 0;
            dateAxis.renderer.minGridDistance = 50;
            dateAxis.renderer.line.strokeWidth  = 2;
            dateAxis.renderer.line.strokeOpacity = 1;
            dateAxis.renderer.line.stroke = am4core.color("#e2e5ec");
            
            dateAxis.keepSelection = true;
            let date1 = new Date(this.timeEnd);
            let date2 = new Date(this.timeStart);
            let  diffTime = Math.abs(date2.getTime() - date1.getTime());
     
            if(diffTime > 1728000000)
             {
              dateAxis.gridIntervals.setAll([
                { timeUnit: "hour", count: 1 },
                { timeUnit: "hour", count: 6 },
                { timeUnit: "hour", count: 12 },
                { timeUnit: "hour", count: 18 },
              ]);
            }
  
            if(diffTime > 2160000000)
            {
              dateAxis.gridIntervals.setAll([
                { timeUnit: "hour", count: 1 },
                { timeUnit: "hour", count: 12 },
                { timeUnit: "hour", count: 20 },
               
              ]);
            }
       
            // dateAxis.dateFormatter.dateFormat = "yyyy-MM-dd HH:mm";
            dateAxis.dateFormats.setKey("day", "yyyy-MM-dd HH:mm:ss");
            dateAxis.periodChangeDateFormats.setKey("day", "[bold]d[/]");
     
            // set yAxes
            let valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.min = min;
            valueAxis.max = max;
            valueAxis.renderer.line.strokeOpacity = 1;
            valueAxis.renderer.line.strokeWidth  = 2;
            valueAxis.renderer.line.stroke = am4core.color("#e2e5ec");

            if (min != undefined && this.chart.data.length > 0) {
              let range = valueAxis.axisRanges.create();
              range.value = min;
              range.grid.stroke = am4core.color("#396478");
              range.grid.strokeWidth = 1;
              range.grid.strokeOpacity = 1;
              range.label.inside = true;
              range.label.fill = range.grid.stroke;
              range.label.text = this.translate.instant('REPORT.FUEL.GENERAL.MIN')+' ('+min+')';
              range.label.align = "right";
              range.label.verticalCenter = "top";
              range.label.fontWeight = '600';
              range.label.fontSize = 13;
              range.label.paddingBottom = 20;
            }
            if (max != undefined && this.chart.data.length > 0) {
              let range = valueAxis.axisRanges.create();
              range.value = parseInt(chartItem.parameters.max);
              range.grid.stroke = am4core.color("#A96478");
              range.grid.strokeWidth = 1;
              range.grid.strokeOpacity = 1;
              range.label.inside = true;
              range.label.text = this.translate.instant('REPORT.FUEL.GENERAL.MAX')+' ('+parseInt(chartItem.parameters.max)+')';
              range.label.fill = range.grid.stroke;
              range.label.align = "right";
              range.label.verticalCenter = "bottom";
              range.label.fontWeight = '600';
              range.label.fontSize = 13;
              range.label.paddingTop = 20;
            }
            //style chart
            // Create series
            let series = this.chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = "temperature";
            series.strokeWidth = 3;
            series.stroke = am4core.color("#CF0000");

            series.propertyFields.opacity = "1";
            series.dataFields.dateX = "date";

            series.minBulletDistance = 10;
            series.tooltipText = "{valueY}";
            series.tooltip.pointerOrientation = "vertical";
            series.tooltip.background.cornerRadius = 20;
            series.tooltip.background.fillOpacity = 0.5;
            series.tooltip.label.padding(12,12,12,12)
            series.tooltipText = "{dateX}: [bold]{valueY}[/] "+unit;

            // Make a panning cursor
            if(this.showPoints)
            {
              let  bullet = series.bullets.push(new am4charts.CircleBullet());
              bullet.circle.strokeWidth = 2;
              bullet.circle.fill = am4core.color("#fff");
              var bullethover = bullet.states.create("hover");
              bullethover.properties.scale = 1.3;
            }

           // Positive  temperature
            let borderRange = 0;
            if(unit == '°F') borderRange = 32;
            let rangePositive  = valueAxis.createSeriesRange(series);
            rangePositive.value = 32;
            rangePositive.endValue = 10000;
            rangePositive.contents.strokeWidth = 3;
            rangePositive.contents.stroke = am4core.color("#CF0000");
            rangePositive.contents.fill = rangePositive.contents.stroke;
            // Negative  temperature

            // axisTitle
            // let axisTitle = series.createChild(am4core.Label);
            // axisTitle.layout = "absolute";
            // axisTitle.html = this.translate.instant('MENU.REPORT_TEMPERATURE')+" ("+unit+")";
            // axisTitle.fontWeight = '600';
            // axisTitle.fontSize = 13;
            // axisTitle.paddingTop = -25;

            // no data
            if (!this.chart.data || this.chart.data.length == 0) {
                this.listChangeTemperature = [];
                let axisNodata = this.chart.createChild(am4core.Label);
                axisNodata.fill = am4core.color("rgb(87, 87, 87)");
                axisNodata.html = "<i class='icon-database'></i> " + this.translate.instant("MAP.TRACKING.CHART.NO_DATA");
                axisNodata.fontWeight = '600';
                axisNodata.fontSize = 13;
                axisNodata.isMeasured = false;
                axisNodata.x = am4core.percent(50);
                axisNodata.horizontalCenter = "middle";
                axisNodata.verticalCenter = "middle";
                valueAxis.renderer.grid.template.disabled = true; 
                
                axisNodata.paddingTop = 200;
                valueAxis.max = max;
                valueAxis.min = 0;
                axisNodata.y = 50;
              }
            
                this.isLoading = false;             
                this.showChangeTemperature = true;
            // chart.scrollbarX = new am4charts.XYChartScrollbar();
        });
      
    }
  
  async  drawChart()
    {
        this.isLoading = true;
        let params = {
            timeTo:this.timeEnd,
            timeFrom:this.timeStart,
            deviceId:this.deviceIds,
            sensorIds:this.sensorIds
          }  
       await  this.Temperature.detail(params,{notifyGlobal:true})
        .pipe(
      
         tap((result: any) => {
                if (result.status == 200) {
                  let _this = this;
                  let i = 0;
                  let indexColor;
  
                  result.result =  result.result.map(item => {
                    item.sensorTemplate = {
                      parameters:item.parameters
                    };
                    item.details = item.details.map(detail =>{
                        let timestampUtc = _this.userDateAdv.transform(detail.timestampUtc,'YYYY-MM-DD HH:mm:ss');
                    
                        // add color to previous data item depending on whether current value is less or more than previous value
                        if(detail.add){
                            indexColor = '#0abb87';
                        } 
                        if(detail.remove){
                            indexColor = '#fd397a';
                        }
                        if(!detail.add && ! detail.remove){
                            indexColor = '#348ae2';
                        }
                        detail.date = timestampUtc;
                        detail.indexColor = indexColor;
                        return detail;
                    })
                      i++;
                      return item;
                     
                  });
                 
                  this.listChart = result.result;
                  
                  setTimeout(()=>{   
  
                    this.renderChart();
                    this.cdr.detectChanges();
                    this.cdr.markForCheck();
  
                    // this.isLoading = false;             
                  }, 500);
                }
          }),
          finalize(() => {
            this.cdr.markForCheck();
            
          })
        ).toPromise();   
    
    }
     
  
  }
  