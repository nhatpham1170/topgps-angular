import { Component, Output, EventEmitter, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { AsciiPipe } from '@core/_base/layout';
import moment from 'moment';
import { group } from '@angular/animations';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
declare var $: any;
import { TranslateService } from '@ngx-translate/core';
import { array } from '@amcharts/amcharts4/core';

@Component({
    selector: 'kt-select-picker',
    templateUrl: './select-picker.component.html',
    styleUrls: ['./select-picker.component.scss'],
    providers: [AsciiPipe]

  })
export class SelectPickerComponent implements OnInit {
    @Input() eventUpdateData: EventEmitter<any>; // event for update and refesh by parrent
    @Input() filter:string = 'name';
    @Input('placeholder') placeholder: string = 'Search...';
    @Input('size') size: string = 'col-lg-12';
    @Input('name') name: string = 'box-list';
    @Input('options') options?: {
         type?: string,
         textSelected?:string,
         fieldGroup?:string
     };
    @Input('showGroup') showGroup?:boolean;
    @Input() isSwitch: boolean = true;
    @Output() checkedChange = new EventEmitter();
    formCheckList: FormGroup;
    public checkallboxed:boolean;
    public searchText: string = "";
    public hasData:boolean = false;
    public textSelected:string = "Tất cả thiết bị";
    public dataByGroup:any = [];
    public paramsEmiter: EventEmitter<any>;
    public resetData:EventEmitter<boolean>;
    public dataDefault:any;
    private isShowGroup:boolean = false;
    public dataLength:number = 0;
    public keySearchFilter:string = "name"; 
    constructor(
      private translate: TranslateService,
      private formBuilder: FormBuilder,
      private asciiPipe:AsciiPipe)
      {
       this.paramsEmiter = new EventEmitter();
       this.resetData  = new EventEmitter();
       if (this.options == undefined) {
        this.options = {};
      }
    }

    ngOnInit(){
        if(this.eventUpdateData)
        {            
            this.eventUpdateData.subscribe(data=>{                        
                this.dataLength = data.length;
                if(this.showGroup)
                {
                    let listDeviceOrther = [];
                    this.dataByGroup = [];
                      data.forEach(device => {
                        if(device.groupDevices.length > 0)
                        {
                            device.groupDevices.forEach(group => {
                                let itemGroup = {
                                    name:'',
                                    id:'',
                                    info:any,
                                    listDevice:[]
                                };
                                if( this.dataByGroup.find(x=>x.info.id == group.id))
                                {
                                    this.dataByGroup.find(x=>x.info.id == group.id).listDevice.push({
                                        name:device.name,
                                        id: device.id
                                    })
                                }else
                                {
                                    itemGroup.info = group;
                                    itemGroup.name = group.name;
                                    itemGroup.id   = group.id;
                                    itemGroup.listDevice.push({
                                        name:device.name,
                                        id: device.id
                                    });
                                    this.dataByGroup.push(itemGroup);
                                }
                               
                            });
                        }else{
                            listDeviceOrther.push({
                                name:device.name,
                                id: device.id
                            });
                        }
                 
                    });       
                    if(listDeviceOrther.length>0)
                    {
                        this.dataByGroup.push({
                            info:{
                                name:this.translate.instant('ADMIN.POI_TYPE.LANGUAGE.OTHER'),
                                id:0,
                            },
                            listDevice:listDeviceOrther
                        });
                    }         
                 if(this.dataDefault)
                 {
                    for(let i=0;i<this.dataDefault.length;i++)
                    {
          
                          this.dataByGroup.map(item=>{
                            let  itemCheck = item.listDevice.find(device=>device.id == this.dataDefault[i]);
                              if(itemCheck) itemCheck.checked = true;
                              item = itemCheck;
                              return item;
                          })
          
                        
                    }
                 }
                }

                if(!this.showGroup)  this.dataByGroup = data;
                
                this.paramsEmiter.emit(
                    {
                        data:this.dataByGroup,
                        count:this.dataLength,
                        showGroup:this.showGroup,
                    }
                );
            })
        }
      }

      getListItem(event)
      {
          this.checkedChange.emit(event);
      }

      reset()
      {
          this.resetData.emit(true);
      }

      setValue(arrValue)
      {
          this.dataDefault = arrValue;
      }

}
