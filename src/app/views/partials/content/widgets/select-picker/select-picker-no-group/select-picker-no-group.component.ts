import { Component, Output, EventEmitter, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { AsciiPipe } from '@core/_base/layout';
import moment from 'moment';
import { DeviceGroup,DeviceGroupService,DeviceService} from '@core/manage';
declare var $: any;

@Component({
    selector: 'kt-select-picker-no-group',
    templateUrl: './select-picker-no-group.component.html',
    styleUrls: ['./select-picker-no-group.component.scss'],
    providers: [AsciiPipe]

  })
export class SelectPickerNoGroupComponent implements OnInit {
    @Input() eventUpdateData: EventEmitter<any>; // event for update and refesh by parrent
    @Input() data:any =[];
    @Input() filter:string = 'name';
    @Input('placeholder') placeholder: string = 'Search...';
    @Input('size') size: string = 'col-lg-12';
    @Input('name') name: string = 'box-list';
    @Input('options') options?: { type?: string };
    @Input() resetData: EventEmitter<boolean>; // event for update and refesh by parrent

    @Input() isSwitch: boolean = true;
    @Output() checkedChange = new EventEmitter();
    formCheckList: FormGroup;
    public checkallboxed:boolean;
    public searchText: string = "";
    public hasData:boolean = false;
    public textSelected:string = "Tất cả thiết bị";
    
    constructor(
      private formBuilder: FormBuilder,
      private asciiPipe:AsciiPipe)
    {
       if (this.options == undefined) {
        this.options = {};
      }
      this.buildForm();

    }

    ngOnInit(){
        if(this.eventUpdateData)
        {
            this.eventUpdateData.subscribe(data=>{
                this.data = data;
                // this.data = this.data.map(
                //     (x)=>{
                //       x['textSearch'] = this.asciiPipe.transform(x[this.filter] || "");
                //       return x;
                //     }
                //   )
                this.addIdToFormArray();
            })   

        }
        if(this.resetData)
        {
          this.resetData.subscribe(reset=>{
            if(reset) this.reset();
          })
        }
      }

  
      private buildForm(): void {
          this.formCheckList = this.formBuilder.group({
          checkallboxed:[],
          searchText:[]
          });
          }
  
     
      addIdToFormArray()
      {            
          this.formCheckList.controls[this.name] = new FormArray([]);
          for(var i = 0; i < this.data.length ; i++)
          {
              let id = this.name+'_id_'+ this.data[i].id;
              this.formCheckList.controls[this.name]['controls'][id] =  new FormControl();
          }
          this.isCheckSelected();
      }
  
      isCheckSelected()
      {
          this.checkallboxed = this.data.every(function(item:any) {
            return item.checked == true;
          });
          this.getListIdSelected();
      }
      
      checkUncheckAll()
      {
          for (var i = 0; i < this.data.length; i++) {  
            this.data[i].checked = this.checkallboxed;
          };
          this.getListIdSelected();
      }
      
      getListIdSelected()
      {
          let listIdSelected : any = [];
          let listSelected : any = [];
          for (var i = 0; i < this.data.length; i++) 
          {
              
            if(this.data[i].checked)
            {        
              listIdSelected.push(this.data[i].id);
              listSelected.push(this.data[i]);
            }
          }
  
          let params = {
              listChecked:listIdSelected,
              countChecked: listIdSelected.length
          }
          this.renderShowTextSelect(listSelected);       
          this.checkedChange.emit(params);
      }

      renderShowTextSelect(listSelectd)
      {
            
            if(listSelectd.length == this.data.length || listSelectd.length == 0) this.textSelected = 'Tất cả thiết bị'; 
            if(listSelectd.length != this.data.length && listSelectd.length > 0)
            {
                this.textSelected = '';
                listSelectd.map(list=>{
                    let name = list.name
                    this.textSelected = this.textSelected+ name +',';
                }) 
            }  
      }
  
      searchItem(val)
      {
          this.searchText = val;
      }
      
      reset()
      {
        this.textSelected = 'Tất cả thiết bị'; 
        this.checkallboxed = false;
        this.data.map(item=>{
            item.checked = false;
            return item;
        });
        let params = {
            listChecked:[],
            countChecked: 0
           };
          this.checkedChange.emit(params);
      }

      clearFilter()
      {
          this.searchText = "";
      }
}
