import { map } from 'rxjs/operators';
import { Component, Output, EventEmitter, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { AsciiPipe } from '@core/_base/layout';
import moment from 'moment';
import { DeviceGroup, DeviceGroupService, DeviceService } from '@core/manage';
import { group } from '@angular/animations';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
declare var $: any;
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'kt-select-picker-group',
  templateUrl: './select-picker-group.component.html',
  styleUrls: ['./select-picker-group.component.scss'],
  providers: [AsciiPipe]

})
export class SelectPickerGroupComponent implements OnInit {
  @Input() eventUpdateData: EventEmitter<any>; // event for update and refesh by parrent
  @Input() resetData: EventEmitter<boolean>; // event for update and refesh by parrent

  @Input() filter: string = 'name';
  @Input('placeholder') placeholder: string = 'Search...';
  @Input('size') size: string = 'col-lg-12';
  @Input('name') name: string = 'box-list';
  @Input('options') options?: { type?: string };
  @Input('dataLength') dataLength: number;
  @Input() isSwitch: boolean = true;
  @Output() checkedChange = new EventEmitter();
  formCheckList: FormGroup;
  public checkallboxed: boolean;
  public searchText: string = "";
  public hasData: boolean = false;
  public textSelected: string = '';
  public dataByGroup: any = [];
  public showGroup: boolean = false;
  public lenghtData: number = 0;
  public dataFull: any = [];
  public test: any;
  public textSearch: string = 'textSearch';
  public dataOrigin = [];
  public dataSearch = [];

  constructor(
    private cdr: ChangeDetectorRef,

    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private asciiPipe: AsciiPipe) {
    this.textSelected = this.translate.instant('REPORT.GEOFENCE.SUMMARY.ALL_DEVICE');
    if (this.options == undefined) {
      this.options = {};
    }
    this.buildForm();

  }

  ngOnInit() {

    if (this.eventUpdateData) {
      this.eventUpdateData.subscribe(data => {
        data.data = data.data.map(item => {
          item.listDevice = item.listDevice.map(device => {
            device.textSearch = this.asciiPipe.transform(device.name || "");
            return device;
          })
          return item;
        })
        this.dataFull = JSON.parse(JSON.stringify(data.data));
        this.dataByGroup = JSON.parse(JSON.stringify(data.data));
        this.dataOrigin =  JSON.parse(JSON.stringify(data.data));
        if (data.data.length > 1) this.showGroup = true;
        if (data.data.length == 1) this.showGroup = false;

        this.addIdToFormArray();
      })
    }

    if (this.resetData) {
      this.resetData.subscribe(reset => {
        if (reset) this.reset();
      })
    }
  }

  private buildForm(): void {
    this.formCheckList = this.formBuilder.group({
      checkallboxed: [],
      searchText: []
    });
  }

  addIdToFormArray() {
    this.formCheckList.controls[this.name] = new FormArray([]);
    for (var i = 0; i < this.dataByGroup.length; i++) {
      let group = this.dataByGroup[i].info.id;
      group = "group" + group;
      let dataGroup = this.dataByGroup[i].listDevice;
      this.formCheckList['controls'][group] = new FormControl();
      for (let index = 0; index < dataGroup.length; index++) {
        let id = this.name + '_id_' + dataGroup[index].id;
        // let id =  this.name+'_id_'+ dataGroup[index].id+'_group_'+this.dataByGroup[i].info.id;

        this.formCheckList.controls[this.name]['controls'][id] = new FormControl();
        this.lenghtData++;
      }
    }
  }

  isCheckSelected(device,group ) {
    this.dataByGroup =  this.dataByGroup.map(g=>{
        g.listDevice = g.listDevice.map(i=>{
          if(i.id == device.id) i.checked = device.checked;
          return i;
        })
      return g;
    });
    this.resetChecked();    
    this.getListIdSelected();
  }

  // Tích chọn tất cả theo nhóm 
  checkUncheckAllGroup(groupId) {
    // set all item group active;
    let groupChange = this.dataByGroup.find(x => x.info.id == groupId);
    this.dataByGroup = this.dataByGroup.map(g=>{
      g.listDevice =  g.listDevice.map(x=>{
        if(groupChange.listDevice.some(i=>i.id ==x.id)){
          x.checked = groupChange.info.checked;
        }
        return x;
      });
      return g;
    });

    this.resetChecked();
  }

  //Tich chon tat ca
  checkUncheckAll() {
    this.dataByGroup.map(group => {
      group.info.checked = this.checkallboxed;
      group.listDevice.map(device => {
        device.checked = this.checkallboxed;
      })
      return group;
    });
    this.getListIdSelected();
  }

  getListIdSelected() {
    let listIdSelected = [];
    let listSelected: any = [];

    for (var i = 0; i < this.dataByGroup.length; i++) {
      let listDevice = this.dataByGroup[i].listDevice;
      for (var index = 0; index < listDevice.length; index++) {
        if (listDevice[index].checked) {
          if ($.inArray(listDevice[index].id, listIdSelected) == -1) {
            listIdSelected.push(listDevice[index].id);
            listSelected.push(listDevice[index]);
          }
        }
      }
    }
    let params = {
      listChecked: listIdSelected,
      countChecked: listIdSelected.length
    }
    this.renderShowTextSelect(listSelected);
    this.checkedChange.emit(params);
  }

  renderShowTextSelect(listSelectd) {
    if (this.checkallboxed || listSelectd.length == 0) this.textSelected = this.translate.instant('REPORT.GEOFENCE.SUMMARY.ALL_DEVICE');
    if (listSelectd.length != this.lenghtData && listSelectd.length > 0) {
      this.textSelected = '';
      listSelectd.map(list => {
        let name = list.name
        this.textSelected = this.textSelected + name + ',';
      })
    }
  }

  reset() {
    this.textSelected = this.translate.instant('REPORT.GEOFENCE.SUMMARY.ALL_DEVICE');
    this.checkallboxed = false;
    this.dataByGroup.map(group => {
      group.info.checked = false;
      group.listDevice.map(device => {
        device.checked = false;
      })
      return group;
    });
    let params = {
      listChecked: [],
      countChecked: 0
    };
    this.checkedChange.emit(params);
  }

  searchItem(val) {
    val = this.asciiPipe.transform(val);
    this.dataByGroup = JSON.parse(JSON.stringify(this.dataFull));

    this.dataByGroup = this.dataOrigin.filter(it => {
      let found = false;
      it.listDevice = it.listDevice.filter(device => {
        if (device[this.textSearch].toLowerCase().includes(val)) found = true;
        return device[this.textSearch].toLowerCase().includes(val);
      });
      return found;
    });
  }

  resetChecked() {
    let a = [{name: "Mazda CX5", id: 386, textSearch: "Mazda CX5", checked: true}];   
     this.dataByGroup = this.dataByGroup.map(g => {
      g.info.checked = g.listDevice.every(x=>{
        return x.checked === true
      });    
      return g;  
    });

    this.checkallboxed = this.dataByGroup.every(x=>x.checked === true);
    this.cdr.detectChanges();
  }

  clearFilter() {
    let dataFilter = this.dataFull;
    dataFilter = dataFilter.map(itemTest => {
      if (this.dataByGroup.find(x => x.id == itemTest.id)) {
        itemTest.info = this.dataByGroup.find(x => x.id == itemTest.id).info;
        let listDevice = this.dataByGroup.find(x => x.id == itemTest.id).listDevice;
        itemTest.listDevice = itemTest.listDevice.map(device => {
          if (listDevice.find(x => x.id == device.id)) {
            device.checked = listDevice.find(x => x.id == device.id).checked;
          }
          return device;
        })
      }
      return itemTest;
    })

    this.dataByGroup = dataFilter;
    this.resetChecked();
    this.searchText = "";
  }
}
