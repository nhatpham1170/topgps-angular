import { Component, Output, EventEmitter, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";

@Component({
    selector: 'kt-breadcrumb',
    templateUrl: './breadcrumb.component.html',
    styleUrls: ['./breadcrumb.component.scss'],

  })
export class BreadCrumbComponent implements OnInit {

    @Input() data:any =[];
    @Input('title') title: string = '';

    constructor()
    {
    }

    ngOnInit(){

    }

    
}
