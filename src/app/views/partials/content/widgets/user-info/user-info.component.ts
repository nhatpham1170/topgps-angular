import { Component, OnInit, ChangeDetectorRef, EventEmitter, Input,Output } from '@angular/core';
import { User } from '@core/manage';
import { ToastService } from '@core/_base/layout';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { CurrentUserService,currentUser } from '@core/auth';
import { UserManageService } from '@app/core/manage/_service/user-manage.service';
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { AppState } from '@core/reducers';
import { EventEmitterService } from '@core/common/_service/eventEmitter.service';

import * as $ from 'jquery';
import { NgxPermissionsService } from 'ngx-permissions';
declare var $: any;

@Component({
  selector: 'kt-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  @Input() user: User;
  @Input() type: string;
  @Output()updateUserTree : EventEmitter<any>; // event change user send to parrent

  saveFormUsers: FormGroup;
  public dataDefault: any = [{}]; //
  public idParentUserEdit: any;
  public userIdSelected: number;
  public closeResult: string;
  public isEdit: boolean = true;
  public idUserEdit: any; // id user edit

  public listTimeZone: any = [];
  public languageList: any = [];
  public weekFirstDay: any = [];
  public decimalSeprerator: any = [];
  public unitVolume: any = [];
  public unitWeight: any = [];
  public unitTemperature: any = [];
  public unitDistance: any = [];
  public dateFormat: any = [];
  public timeFormat: any = [];
  public listRoles: any = [];
  public listUserType: any = [];
  public typeShow: string; 
  public idLogin:number;  
  public parrentEmiterUpdate: EventEmitter<boolean | { id: number, path: string }>;

  public userModel:any = {
    id: 0
  }; 
  private firstLoad : boolean = true;
  constructor(
    private formBuilder: FormBuilder,
    private CurrentUserService: CurrentUserService,
    private modalService: NgbModal,
    private toast: ToastService,
    private manageUser: UserManageService,
    private cdr: ChangeDetectorRef,
    private permissionService: NgxPermissionsService,    
    private store: Store<AppState>,
    private eventEmitterService:EventEmitterService
  ) {
    this.parrentEmiterUpdate = new EventEmitter();
    this.updateUserTree = new EventEmitter();

    
  }

  ngOnInit() {
    if (this.user == undefined) {
      this.user = new User();
      this.user.clear();
    }
    if (this.type == undefined) this.type = "user";
    switch (this.type) {
      case "user":
        if(this.permissionService.getPermission("ROLE_manage.device"))
          this.typeShow = "device";
        break;
      case "device":
          if(this.permissionService.getPermission("ROLE_manage.user"))
            this.typeShow = "user";
        break;
      default:
          if(this.permissionService.getPermission("ROLE_manage.device"))
            this.typeShow = "device";
        break;
    }

    this.buildForm();
    let userModel = {
      id: 0,
    };
    let user$:any = this.store.pipe(select(currentUser));
    if (user$) {
      select(user$.subscribe(function (data) {
        Object.assign(userModel, data);
      }
      ).unsubscribe());
    }
    this.idLogin =  userModel.id;
  }

  copied($event) {
    this.toast.copied($event);
  }

  openModal(userEdit, content) {
    this.firstLoad = true;
    // this.parrentEmiterUpdate.emit({id:userEdit.id,path:userEdit.path});
    let id = userEdit.id;
    this.idParentUserEdit = userEdit.parentId;
    this.idUserEdit = id;
    this.manageUser.get(id)
    .pipe(
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 200) {
        let data = result.result;

        this.dataDefault = [data];
        setTimeout(function () {
          $('.kt_selectpicker').selectpicker();
        }, 110);
      }
    });
    this.loadParamsetUser();
    setTimeout(function () {
      $('.kt_selectpicker').selectpicker();
    }, 110);
    this.open(content);
  }
  loadParamsetUser() {
    this.listTimeZone = this.CurrentUserService.listTimeZone;
    this.languageList = this.CurrentUserService.listLanguage;
    this.weekFirstDay = this.CurrentUserService.listWeekFirst;
    this.decimalSeprerator = this.CurrentUserService.listDecimal;
    this.unitVolume = this.CurrentUserService.listVolume;
    this.unitWeight = this.CurrentUserService.listWeight;
    this.unitDistance = this.CurrentUserService.listDistance;
    this.unitTemperature = this.CurrentUserService.listTemperature;
    this.timeFormat = this.CurrentUserService.listTimeFormat;
    this.dateFormat = this.CurrentUserService.listDateFormat;
    this.listRoles = this.CurrentUserService.listRoles;
    this.listUserType = this.CurrentUserService.listUserType;
  }

  private buildForm(): void {
    this.saveFormUsers = this.formBuilder.group({
      username: ['', Validators.required],
      name: ['', Validators.required],
      phone: [''],
      email: ['', [Validators.required, Validators.email]],
      role: [''],
      timezone: [''],
      language: [''],
      type: ['', Validators.required],
      description: [''],
      address: [''],
      distance: [''],
      volume: [''],
      weight: [''],
      date: [''],
      time: [''],
      temperature: [''],
      fisrtday: [''],
      seprerator: ['']
    });
  }

  get f() {
    if (this.saveFormUsers) return this.saveFormUsers.controls;
  }

  getParrentUser(value) {
    if(!this.firstLoad)
    {
      this.idParentUserEdit = value.id;
    }else{
      this.firstLoad = false;
    }

    this.buildDataDefault();

    // if (this.idParentUserEdit == undefined) this.idParentUserEdit = this.user.parentId;
  }

  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder', size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  onSubmit(form: any) {

    if (this.saveFormUsers.invalid) {
      return;
    }
    let params = {
      "parentId": this.idParentUserEdit,
      "aliasId": 0,
      "path": "",
      "username": form.value.username,
      "type": form.value.type,
      "name": form.value.name,
      "surname": "",
      "email": form.value.email,
      "phone": form.value.phone,
      "description": form.value.description,
      "status": 1,
      "active": 1,
      "createdAt": "",
      "stockDevice": 0,
      "totalDevice": 0,
      "totalPoint": 0,
      "isRenewed": 1,
      "timezone": form.value.timezone,
      "language": form.value.language,
      "unitDistance": form.value.distance,
      "unitVolume": form.value.volume,
      "unitTemperature": form.value.temperature,
      "unitWeight": form.value.weight,
      "dateFormat": form.value.date,
      "timeFormat": form.value.time,
      "weekFirstDay": form.value.fisrtday,
      "markerStyle": "html",
      "carSorting": "plate",
      "decimalSeprerator": form.value.seprerator,
      "roleId": form.value.role
    } as User;
    // profile
    if(this.idLogin == this.idUserEdit)
    {
       params = {
        "name": form.value.name,
        "surname": "",
        "email": form.value.email,
        "phone": form.value.phone,
        "description": form.value.description,
        "timezone": form.value.timezone,
        "language": form.value.language,
        "unitDistance": form.value.distance,
        "unitVolume": form.value.volume,
        "unitTemperature": form.value.temperature,
        "unitWeight": form.value.weight,
        "dateFormat": form.value.date,
        "timeFormat": form.value.time,
        "weekFirstDay": form.value.fisrtday,
        "decimalSeprerator": form.value.seprerator,
      } as User;
      this.manageUser.updateProfile(params,{ notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          this.modalService.dismissAll();
          let params = {
            id: this.idParentUserEdit,
            path : result.result.path
          };
          
          this.eventEmitterService.updateTree(params);
        }
      });
      return;
    }
    // end profile
    if (this.isEdit) {
      params.id = this.idUserEdit;
      this.manageUser.put(params, { notifyGlobal: true }).pipe(
        finalize(() => {
          this.cdr.markForCheck()
        })
      ).subscribe((result: any) => {
        if (result.status == 200) {
          if(this.idUserEdit == this.CurrentUserService.currentUser.id)
          {
            window.location.reload();
          }
          let params = {
            id: this.idParentUserEdit,
            path : result.result.path
          };
          this.user.username = result.result.username;
          this.user.name = result.result.name;

          this.user.email = result.result.email;
          this.updateUserTree.emit(result.result);
          // this.eventEmitterService.loadUpdateTree.emit(params);
          // console.log(this.user);
          this.modalService.dismissAll();
        }
      });

      return;
    };

  }

  buildDataDefault() {
    // this.getInfoByIdUser(id);
    this.manageUser.get(this.idParentUserEdit)
      .pipe(
        finalize(() => {
          this.cdr.markForCheck()
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          let data = result.result;
          this.dataDefault[0].timezone = data.timezone;
          this.dataDefault[0].language = data.language;
          this.dataDefault[0].unitDistance = data.unitDistance;
          this.dataDefault[0].unitVolume = data.unitVolume;
          this.dataDefault[0].unitTemperature = data.unitTemperature;
          this.dataDefault[0].unitWeight = data.unitWeight;
          this.dataDefault[0].dateFormat = data.dateFormat;
          this.dataDefault[0].timeFormat = data.timeFormat;
          this.dataDefault[0].weekFirstDay = data.weekFirstDay;
          this.dataDefault[0].decimalSeprerator = data.decimalSeprerator;
          setTimeout(() => {
            $('.kt_selectpicker').selectpicker('refresh');
          }, 200);
        }
      });
  }
}
