import { Component,OnInit,Output, EventEmitter, Input, ViewChild ,ChangeDetectorRef} from '@angular/core';
import { MultiDataSet, Label} from 'ng2-charts';
import { ChartType,ChartOptions,ChartDataSets } from 'chart.js';
import { UserManageService } from '@app/core/manage/_service/user-manage.service';
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';

// Translate
import { TranslateService } from '@ngx-translate/core';
declare var $;
import * as moment from 'moment';

@Component({
  selector: 'kt-chart-global',
  templateUrl: './chart-global.component.html',
  styleUrls: ['./chart-global.component.scss'],
})
export class ChartGlobalComponent implements OnInit{
  public barChartLabels: Label[] = [];
  public barChartOptions: ChartOptions;
  public barChartData: ChartDataSets[];
  public data:any;
  public loading:boolean = false;
  @Input() ConfigChart:any;
  @Output() onChangeDate = new EventEmitter<string>();
  @ViewChild('datePicker', { static: true }) datePicker: DateRangePickerComponent;

  public datePickerOptions: any = {
    startDate: moment().subtract(1, 'days'),
    style: true
  };
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public pieChartColors = [
		{
		  backgroundColor: [],
		},];
  public customDate:any = [];
  public labelActive:string = "Today";
  private unsubscribe: Subject<any>;
  public label_y:string = '';
  private fieldConfig = [
    {
      type:'stop',
      label : "DASHBOARD.STATIC.TOP_10_STOP",
      textUnit :this.translate.instant('DASHBOARD.STATIC.DURATION'),
      label_y:this.translate.instant('DASHBOARD.STATIC.DURATION_LABEL'),
      field:'top_10_stops',
      column:"duration"
    },
    {
      type:'idling',
      label :"DASHBOARD.STATIC.TOP_10_IDLING",
      textUnit :this.translate.instant('DASHBOARD.STATIC.DURATION'),
      label_y:this.translate.instant('DASHBOARD.STATIC.DURATION_LABEL'),

      field:'top_10_idlings',
      column:"stopEngineOnTime"
    },
    {
      type:'distance',
      label :"DASHBOARD.STATIC.TOP_10_DISTANCE",
      textUnit :this.translate.instant('DASHBOARD.STATIC.DISTANCE'),
      label_y:this.translate.instant('DASHBOARD.STATIC.DISTANCE_LABEL'),

      field:'top_10_distances',
      column:"distance"
    }
  ];
  public chartCurrent;
  public count : number = 0;
  public allowNext: boolean = true;
  public allowPrevious: boolean = true;
  public showNumber : boolean = false;
  constructor(
    private manageUser: UserManageService,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,


  ) {
              // this.renderConfigChartFunction();

}
  ngOnInit(){
    this.renderChartCurrent();
    this.renderConfigChartFunction();
  }

  renderBackgroundColor()
  {
    let bg;
    if(this.ConfigChart.background) bg = this.ConfigChart.background; 
    bg.forEach(element => {
      
    });
  }

  renderChartCurrent()
  {
    this.chartCurrent = this.fieldConfig.find(x => x.type === this.ConfigChart.type);
    this.datePickerOptions.key = this.chartCurrent.type;
  }

 async dateSelectChange(data1) {
   this.loading = true;
   this.onChangeDate.emit(data1.startDate.format("YYYY-MM-DD"));
   let params = {
    type:this.chartCurrent.type,
    date: data1.startDate.format("YYYY-MM-DD")
   } ;
    if(this.count == 1)
    {
          await this.manageUser.getInfoParent(params)
          .pipe(
            debounceTime(2000),
            tap((data: any) => {
              if (data.status == 200) {
              //render chart
              let field = this.chartCurrent.field;
              this.ConfigChart.data = data.result[field];
              this.ConfigChart.data = this.sortObjectByValue(this.ConfigChart.data,field);
              this.loading = false;
              }
            }),
            finalize(() => {
              this.cdr.markForCheck();
            })
            ).toPromise();
          this.renderConfigChartFunction();
    }

    if(this.count == 0)
    {
      this.count = 1;
      this.loading = false;
    }

   }

  loadCustomDate()
	{

		this.customDate = [
			{
				id:1,
				label:'Today',
				startDate : moment().startOf('day').format("YYYY-MM-DD HH:mm:ss"),
				endDate : moment().endOf('day').format("YYYY-MM-DD HH:mm:ss")
			},
			{
				id:2,
				label:'Yesterday',
				startDate : moment().subtract(1, 'days').startOf('day').format("YYYY-MM-DD HH:mm:ss"),
				endDate : moment().subtract(1, 'days').endOf('day').format("YYYY-MM-DD HH:mm:ss")
			},
			{
				id:3,
				label:'Last 7 Days',
				startDate : moment().subtract(6, 'days').startOf('day').format("YYYY-MM-DD HH:mm:ss"),
				endDate : moment().endOf('day').format("YYYY-MM-DD HH:mm:ss")
			},
		];
	}

	setDate(id)
	{
		let obj = this.customDate.find(x => x.id === id);
    this.labelActive = obj.label;
    this.onChangeDate.emit(obj);
  }


  previousDate() {
    this.datePicker.previousDay();
  }

  nextDate() {
    this.datePicker.nextDay();
  }

  cropString(text)
  {
    let textLaster = text;
    if(text.length >= 15)
    {
      textLaster = text.substring(0, 15) + "...";
    }
    return textLaster;
  }

  sortObjectByValue(obj,key)
  {
    obj.sort((a,b) => (a[key] > b[key]) ? 1 : ((b[key] > a[key]) ? -1 : 0)); 
    obj = obj.reverse();
    return obj;
  }

  renderConfigChartFunction(){
    console.log(this.ConfigChart);
  let dataConfig = this.ConfigChart.data;
  if(this.ConfigChart.showNumber)
  {
    this.showNumber = this.ConfigChart.showNumber;
  }
  let data  =  [];
  let dataStep = 0;
  let totalData = 0;
	let label = this.chartCurrent.label;
  let textUnit = this.chartCurrent.textUnit;
  this.label_y = this.chartCurrent.label_y;
	this.barChartLabels =[];
	let listDevices = [];
	let index = 0;
	dataConfig.forEach(element => {
		index++;
    let count1 = index.toString();
    // set number
    if(this.showNumber)
    {
      this.barChartLabels.push(count1);
    }
    else
    {
      this.barChartLabels.push(this.cropString(element.name));
    }
    let duration = element[this.chartCurrent.column];
    totalData = totalData+duration;
		data.push(duration);
    listDevices.push(element.name);
    // set background
    if(this.ConfigChart.background)
    {
      this.pieChartColors[0].backgroundColor.push(this.ConfigChart.background);
    }
   
  });
  dataStep = Math.round(totalData/3);
	//options
	this.barChartOptions = {
		legend: {
			position: 'left',
			display: false,
			reverse:false
		 },
		title: {
			display: true,
			position : 'left'
		},
		responsive: true,
		scales: {
     xAxes: [{
      gridLines:{
        drawTicks:false
      },
      ticks:{
        padding:4,

      }
     }],
     yAxes: [{
      gridLines:{
        drawTicks:false
      },
			ticks: {
				lineHeight:2,
				padding:10,
        min : 0,
        max : 86400,
        stepSize : 21600,
				callback: function(label, index, labels,) {
            var hours = Math.floor(Number(label) / 3600).toString();
            var minutes = (Math.floor(Number(label) / 60) % 60).toString();
            var output  = hours ;
            // var output  = hours + ' h ' + minutes + ' min';

			  	  return output;
				}
			}

		}] },
		tooltips:{
			displayColors:false,
			xPadding:15,
			yPadding:15,
			titleFontSize:14,
			titleFontColor:"#48465b",
			backgroundColor: '#fff',
			bodyFontColor : "#48465b",
			borderColor:"#48465b",
			borderWidth:2,
			callbacks: {
				title: function(tooltipItem, data) {
					  return listDevices[tooltipItem[0].index];
				  },
				label:function(tooltipItem, data) {
					let value = tooltipItem.value;    						
					// let output = this.convertNumberToTime(value);
					var hours = Math.floor(parseInt(value) / 3600);
					var minutes = Math.floor(parseInt(value)  / 60) % 60
					var output = hours + ' h ' + minutes + ' min';
					return textUnit+": "+output;
				}
			},
			titleAlign: 'center',
		},
    };	
    // type distance
    if(this.chartCurrent.type == 'distance')
    {

      this.barChartOptions.scales.yAxes[0].ticks = {
        padding:10,
        callback: function(label, index, labels,) {
          return label;
         }
      };
      this.barChartOptions.scales.yAxes[0].ticks.stepSize = dataStep;
      this.barChartOptions.tooltips = {
			displayColors:false,
			xPadding:15,
			yPadding:15,
			titleFontSize:14,
			titleFontColor:"#48465b",
			backgroundColor: '#fff',
			bodyFontColor : "#48465b",
			borderColor:"#48465b",
			borderWidth:2,
			callbacks: {
				title: function(tooltipItem, data) {
					  return listDevices[tooltipItem[0].index];
				  },
				label:function(tooltipItem, data) {
          let value = parseInt(tooltipItem.value); 
          value = Math.round(value);   						
					var output = value + ' km';
					return textUnit+": "+output;
				}
			},
			titleAlign: 'center',
    };
  }
    // view defaut yAxes (data empty)
    if(dataConfig.length == 0)
    {
      let min = 0;
      let max = 0;
      let stepSize = 0;
      if(this.chartCurrent.type == 'stop' || this.chartCurrent.type == 'idling' )
      {
        min = 0;
        max = 86400;
        stepSize = 21600;
      }
      if(this.chartCurrent.type == 'distance')
      {
        min = 0;
        max = 20;
        stepSize = 5;
      }
      this.barChartOptions.scales.yAxes[0].ticks.min = min;
      this.barChartOptions.scales.yAxes[0].ticks.max = max;
      this.barChartOptions.scales.yAxes[0].ticks.padding = 10;

      this.barChartOptions.scales.yAxes[0].ticks.stepSize = stepSize;
      this.barChartOptions.scales.yAxes[0].gridLines.drawOnChartArea = false;
    };
    // data	  
   this.data = data; 
	this.barChartData =
	[
		{
		 data: data, 
		 label: label 
		},
	];
}

	
}