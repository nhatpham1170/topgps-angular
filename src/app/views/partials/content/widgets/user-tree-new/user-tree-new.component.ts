import { Component, Output, EventEmitter, Input, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { ITreeOptions, TreeNode, ITreeState, TreeModel, TreeComponent, TREE_ACTIONS,IActionMapping } from 'angular-tree-component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitterService } from '@app/core/common/_service/eventEmitter.service';
import { UserTreeService } from '@app/core/common/_service/user-tree.service';
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject, Observable, pipe } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { AppState } from '@core/reducers';
import { currentUser, User, AuthService } from '@core/auth';
import { AsciiPipe } from '@core/_base/layout';
import { User as UseModel } from '@core/manage';

declare var $: any;

const prefix: string = "kt_tree_";
const timeDelaySearch: number = 100; // time delay for search ms

@Component({
  selector: 'kt-user-tree-new',
  templateUrl: './user-tree-new.component.html',
  styleUrls: ['./user-tree-new.component.scss'],
  providers: [AsciiPipe,TreeModel],
})
export class UserTreeNewComponent implements OnInit {
  @Input() cacheSearch: boolean; // save history to variable
  @Input() userTreeName: string; //
  @Input() eventUpdate: EventEmitter<any>; // event for update and refesh by parrent
  @Input() eventUpdateCheckbox: EventEmitter<any>; // event for update and refesh by parrent

  @Input() storeLocal: boolean; // allow save localStorge
  @Input() showCountDevice: boolean; // allow show count device (stock/total)
  @Input() showPoint: boolean; // allow show point (point)
  @Output() changeUser: EventEmitter<UseModel>; // event change user send to parrent
  @Input() btnClose: boolean;
  @Input() allowCollapse: boolean;
  @Input() parentId: number;
  @Input() style?: any;
  @Input() userSelected?:UserModel;
  @Input() checkBox:boolean;
  @Input() userRoot?:any;
  @Input() disableRoot?:boolean = true;
  // @Input() currentUser: 
  
  @Input() listCheckbox:any;
  @Output() clickCheckbox: EventEmitter<UseModel>; // event change user send to parrent

  public nodes: any = [];// form users tree
  public idRoot: any; // id user login
  public nameRoot: any; // name user login
  public paramsRoot; // params user login
  public newNodes; // data child when click user parent
  public showResultSearch: boolean;
  public searchNotFound: boolean;
  public isLoading = false;
  public treeState: ITreeState;
  public keyword = 'keyDisplay'; // keyword search user
  public dataSearch: any[] = []; // data search  
  private searchHistory: Map<string, any> = new Map<string, any>();
  private pageNo = 0;
  private pageSize = 0;
  private unsubscribe: Subject<any>;
  private lastNodeSelected: { id: number, path: string };
  private lastKeySearch: string = "";
  private nodeRoot: any;
  public collapsed: boolean = false;
  public isShow: boolean = false;
  public mouseEnter:boolean = true;
  private keySaveCacheLayout:string;
  private configLayoutMode:{collapse:boolean} = {
    collapse:false
  }


  @ViewChild(TreeComponent, { static: false })
  //   @ViewChild(TreeComponent)
  private nametree: TreeComponent;

  options: ITreeOptions = { // option users tree
    scrollOnActivate: true,
    getChildren: this.loadChildUser.bind(this),
    animateExpand: true,
    useVirtualScroll: true,
    animateSpeed: 30,

    // allowDrag: (node) => {
    //   return true;
    // },
    // allowDrop: (node) => {
    //   return true;
    // },
    actionMapping: {
      mouse: {
        // drop: (tree: TreeModel, node: TreeNode, $event: any, { from, to }) => {
        //   // use from to get the dragged node.
        //   // use to.parent and to.index to get the drop location
        //   // use TREE_ACTIONS.MOVE_NODE to invoke the original action
        // },
        // click:(tree, node, $event) => {
        //   if (node.hasChildren) TREE_ACTIONS.TOGGLE_EXPANDED(tree, node, $event);

        // },
        dblClick: (tree, node, $event) => {
          if (node.hasChildren) TREE_ACTIONS.TOGGLE_EXPANDED(tree, node, $event);
        }

      }
    },
    levelPadding: 20,
    animateAcceleration: 1.2,

    scrollContainer: <HTMLElement>document.body
  };

  constructor(
    private http: HttpClient,
    private treeModel : TreeModel,
    private tree: UserTreeService,
    private cdr: ChangeDetectorRef,
    private store: Store<AppState>,
    private ascii: AsciiPipe,
    private authService: AuthService,  

  ) {
    this.changeUser = new EventEmitter();
    this.clickCheckbox = new EventEmitter();

    if (this.storeLocal == undefined) this.storeLocal = false;
    if (this.showCountDevice == undefined) this.showCountDevice = false;
    if (this.showPoint == undefined) this.showPoint = false;
    if (this.userTreeName == undefined) this.userTreeName = "user_tree";
    // show checkbox
    if (this.checkBox == undefined) this.checkBox = false;
    // this.options.useCheckbox = this.checkBox;
    // end show checkbox
    if (this.cacheSearch == undefined || typeof this.cacheSearch != "boolean") {
      this.cacheSearch = false;
    }
    if (this.allowCollapse == undefined) this.allowCollapse = false;

    this.lastNodeSelected = null;
    this.treeState = {
      activeNodeIds: {},
      expandedNodeIds: {},
      focusedNodeId: ''
    };
    this.showResultSearch = false;
    this.searchNotFound = false;
    this.isShow = false;   
    this.keySaveCacheLayout = `${prefix}${this.userTreeName}__layout_config`; 
   
  }
  ngOnInit() {
    // if(this.rootId){
    //   this.idRoot = this.rootId;
    //   console.log("Root Id: " + this.rootId);
    // }
    
    
    // fix default temp
    this.showCountDevice = false;
    this.tree.userTreeResponsive = false;

    this.loadCacheLayout();

    //update checkbox
    if(this.eventUpdateCheckbox)
    {
      this.eventUpdateCheckbox.pipe(
        tap(option => {
          this.updateChildNodeCheckbox(option.id,false);
        })
      ).subscribe();
    }
    // subscribe eventupdate send by parrent
    if (this.eventUpdate) {
      this.eventUpdate.pipe(
        tap(option => {
          this.refreshTree(option);
        })
      ).subscribe();
    }
    // init after currentUser loaded
    setTimeout(() => {
      this.init();
      

    });
    if (this.btnClose) {
      this.tree.userTreeMainDisplay = true;
    }
  }
  /**
   * Init tree
   */
  async init() {
    // get current user login
    let userModel = {
      id: 0, 
      username: "",
      name: "",
      path: "",
      stockDevice: 0,
      totalDevice: 0,
      tree_root: true,
      type: 1
    };
    // check showPoint
    // if (this.showPoint === true) {
    //   await this.authService.getUserByToken({}).toPromise().then(
    //     (data) => {
    //       Object.assign(userModel, data.result);
    //     }
    //   );
    // }
    // else {
    //   // get info node root load with current login
    //   let user$ = this.store.pipe(select(currentUser));
    //   if (user$) {
    //     select(user$.subscribe(function (data) {
    //       Object.assign(userModel, data);
    //     }
    //     ).unsubscribe());
    //   }
    // }
    let user$:any = this.store.pipe(select(currentUser));
    if (user$) {
      select(user$.subscribe(function (data) {
        Object.assign(userModel, data);
      }
      ).unsubscribe());
    }
    if(this.userRoot){
      Object.assign(userModel, this.userRoot);   
    }
    this.idRoot = userModel.id;
    this.nameRoot = userModel.name;
    if (userModel.type <= 1) this.isShow = true;
    else this.isShow = false;

    this.paramsRoot = {
      'parentId': this.idRoot,
      'pageNo': this.pageNo,
      'pageSize': this.pageSize
    };
    this.nodeRoot = Object.assign({}, userModel);
    if (!this.lastNodeSelected) this.lastNodeSelected = Object.assign({}, userModel);

    // console.log(this.nodeRoot);
    
    // check localstore 
    if (this.storeLocal) {
      let nodeLocal = localStorage.getItem(prefix + this.nodeRoot.id);
      if (nodeLocal) this.lastNodeSelected = Object.assign({}, JSON.parse(nodeLocal));
    }
    let keySearchGlobal = sessionStorage.getItem("search_global");
      if(keySearchGlobal){
        sessionStorage.removeItem("search_global");
        const userSearch:any = JSON.parse(keySearchGlobal);
        this.lastNodeSelected = Object.assign({},userModel, userSearch);
      }
    if(this.userSelected){
      this.lastNodeSelected = Object.assign({}, this.userSelected);
    }
    if (this.isShow) {
      this.loadUserRoot();
    }
    else {
      this.changeUser.emit(this.nodeRoot);
    }
    let _this=this;
    setTimeout(()=>{       
      _this.tree.event.emit('close');
      
    });
    setTimeout(()=>{
      $(".tree-mobile .user-tree").css({"opacity": "1"});
    },1000);

    
    // this.tree.event.emit('close');
    // checkd edit
    setTimeout(function(){
      _this.listChecked();
    },300);
  }
  async reload() {
    // get current user login
    let userModel = {
      id: 0,
      username: "",
      name: "",
      path: "",
      stockDevice: 0,
      totalDevice: 0,
      tree_root: true,
    };
    // check showPoint
    await this.authService.getUserByToken({}).toPromise().then(
      (data) => {
        Object.assign(userModel, data.result);
      }
    );

    this.idRoot = userModel.id;
    this.nameRoot = userModel.name;

    this.paramsRoot = {
      'parentId': this.idRoot,
      'pageNo': this.pageNo,
      'pageSize': this.pageSize
    };
    this.nodeRoot = Object.assign({}, userModel);
    if (!this.lastNodeSelected) this.lastNodeSelected = Object.assign({}, userModel);
    // check localstore 
    if (this.storeLocal) {
      let nodeLocal = localStorage.getItem(prefix + this.nodeRoot.id);
      if (nodeLocal) this.lastNodeSelected = Object.assign({}, JSON.parse(nodeLocal));
    }
    this.loadUserRoot();
  }
  openSearch($event) {
    // console.log($event);
  }
  transformDataSearh(data) {

  }
  collapse() {
    this.collapsed = !this.collapsed;
    if(this.collapse){
      this.mouseEnter = false;
    }
    this.saveCacheLayout();
    this.cdr.detectChanges();
  }

  //#region  search
  /**
   * Search key by user name and name
   * @param val 
   */
  searchUser(val: string) {
    this.showResultSearch = false;
    this.searchNotFound = false;
    val = val.trim();
    val = this.ascii.transform(val);
    if (val.length > 0 && val != this.lastKeySearch) {
      this.isLoading = true;
      this.lastKeySearch = val;

      if (this.cacheSearch === true) {
        let dataStore = this.searchHistory.get(prefix + val);
        if (dataStore) {
          setTimeout(() => {
            this.dataSearch = dataStore;
            if (this.dataSearch.length > 0) {
              this.showResultSearch = true;
            }
            else {
              this.searchNotFound = true;
            }
            this.isLoading = false;
            this.cdr.markForCheck();
          }, timeDelaySearch);
        }
        else {
          setTimeout(() => {
            this.tree.searchChildUsers({
              "searchText": val
            })
              .pipe(
                tap((data: any) => {
                  this.dataSearch = data.result.map(x => {
                    x['keyDisplay'] = (x.name + " (" + x.username + ")");
                    x['keySearch'] = this.ascii.transform(x['keyDisplay']);
                    return x;
                  });
                  // save session store
                  this.searchHistory.set(prefix + val, this.dataSearch);
                  if (this.dataSearch.length > 0) {
                    this.showResultSearch = true;
                  }
                  else {
                    this.searchNotFound = true;
                  }
                  this.isLoading = false;
                  this.cdr.markForCheck();
                }),
                finalize(() => {
                  // this.cdr.markForCheck();
                })
              )
              .subscribe();
          }, timeDelaySearch);
        }

      }
      else {
        setTimeout(() => {
          this.tree.searchChildUsers({
            "searchText": val
          })
            .pipe(
              finalize(() => {
                this.cdr.markForCheck();
              })
            )
            .subscribe((data: any) => {
              this.dataSearch = data.result.map(x => {
                x['keyDisplay'] = (x.name + " (" + x.username + ")");
                x['keySearch'] = this.ascii.transform(x['keyDisplay']);
                return x;
              });
              if (this.dataSearch.length > 0) {
                this.showResultSearch = true;
              }
              else {
                this.searchNotFound = true;
              }
              this.isLoading = false;
              this.cdr.markForCheck();
            });
        }, timeDelaySearch);
      }

    }
    else if (val.length > 0 && this.dataSearch.length > 0) {
      this.showResultSearch = true;
    }
    else if (val.length > 0 && this.dataSearch.length == 0) {
      this.searchNotFound = true;
    }

  }
  onSearchFocused(val: any) {
    // console.log(val.target.value);
    if (this.dataSearch.length > 0 && val.target.value.length > 0) {
      this.showResultSearch = true;
    }
    if (this.dataSearch.length == 0 && val.target.value.length > 0) {
      this.searchNotFound = true;
    }
  }
  closedSearch(val: string) {
    this.showResultSearch = false;
    this.searchNotFound = false;
  }
  clearInputSearch(val: string) {
    this.searchUser("");
  }
  /**
   * Select user
   * @param item 
   */
  selecteSearchUser(item) {
    this.showResultSearch = false;
    this.setNodeActive(item);
    this.scrollToEl(item.id);

  }
  //checked box
   listChecked()
   {
     let _this = this;
    // console.log(this.listCheckbox);
     if(this.checkBox && this.listCheckbox.length > 0)
     {
      this.listCheckbox.forEach(element => {
      
        //  //expand
         let path = element.path;
         let expandedNodeIds = {};
         let activeNodeIds = {};
         path = path.split(",");
         for (var i = 0; i < path.length; i++) {
           expandedNodeIds[path[i]] = true;
         }
         activeNodeIds[element.id] = true;
         _this.setTreeState(expandedNodeIds, activeNodeIds);
         setTimeout(function(){
          let node1 = _this.nametree.treeModel.getNodeById(element.id);
          if(node1) node1.data.checked = true;
        },300);

       });
     }
   }
   check(node, checked) {
    this.clickCheckbox.emit(node);
    this.updateChildNodeCheckbox(node,true);
  
    if(this.disableRoot)  this.updateParentNodeCheckbox(node.realParent); // no update parent node when disableRoot == false

  }

   updateChildNodeCheckbox(node, checked) {
    node.data.checked = checked;

    // node = this.treeModel.getNodeById('5732');
    // console.log(this.treeModel.getNodeById(5732));
    if (node.children) {
      node.children.forEach((child) => function(){
        this.updateChildNodeCheckbox(child, checked);
        this.clickCheckbox.emit(child);

      });
    }
  }

   updateParentNodeCheckbox(node) { 
    if (!node) {
      return;
    }

    let allChildrenChecked = true;
    let noChildChecked = true;

    for (const child of node.children) {
      if (!child.data.checked || child.data.indeterminate) {
        allChildrenChecked = false;
      }
      if (child.data.checked) {
        noChildChecked = false;
      }
    }

    if (allChildrenChecked) {
      node.data.checked = true;
      node.data.indeterminate = false;
    } else if (noChildChecked) {
      node.data.checked = false;
      node.data.indeterminate = false;
    } else {
      node.data.checked = true;
      node.data.indeterminate = true;
    }
    this.updateParentNodeCheckbox(node.parent);
  }
  //#endregion

  /**
   * Load tree width current login
   */
  loadUserRoot() {
    this.tree.getChildUsers(this.paramsRoot)
      .pipe(
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((data: any) => {
        if (data.status == 200) {

          let childNodes = data.result;
          for (var i in childNodes) {
            childNodes[i].hasChildren = true;
            // childNodes[i].checked = true;

          }
          let treeRoot = {
            id: this.idRoot,
            name: this.nameRoot,
            children: childNodes,
            root: true,
            hasChildren: true,
            isLeaf:true

          };
          treeRoot = Object.assign({}, treeRoot, this.nodeRoot);
          this.nodes = [
            treeRoot
          ];

          let expandedNodeIds = {};
          let activeNodeIds = {};
          expandedNodeIds[this.idRoot] = true;
          if (this.parentId) {
            activeNodeIds[this.parentId] = true;
            expandedNodeIds[this.parentId] = true;

          } else {
            activeNodeIds[this.idRoot] = true;
          }
          // set node active
          if (this.lastNodeSelected.id != this.nodeRoot.id) {
            this.setNodeActive(this.lastNodeSelected);
          }
          else {
            this.setTreeState(expandedNodeIds, activeNodeIds);// set display id tree          
            this.changeUser.emit(this.nodeRoot);
          }
        }
      });
  }

  /**
   * Load child user when click expanded
   * @param node 
   */
  loadChildUser(node: any) {
    const params = {
      'parentId': node.data.id,
      'pageNo': this.pageNo,
      'pageSize': this.pageSize
    };

    let promise = new Promise((resolve, reject) => {
      this.tree.getChildUsers(params).subscribe((data: any) => {
        if (data.status == 200) {

          this.newNodes = data.result;
          for (var i in this.newNodes) {
            switch (this.newNodes[i].type) {
              case "0":
                this.newNodes[i].hasChildren = true;
                break;
              case "1":
                this.newNodes[i].hasChildren = true;
                break;
              default:
                this.newNodes[i].hasChildren = false;
                break;
            }
          }
          resolve(this.newNodes);
        }
      });
    });
    return promise;
  }

  /**
   * select node by click treeview
   * @param node 
   */
  selecteNode(node) {
    this.lastNodeSelected = node.data;
    this.changeUser.emit((node.data as UseModel));
    if (this.storeLocal === true) {
      localStorage.setItem(prefix + this.nodeRoot.id, JSON.stringify(this.lastNodeSelected));
    }

  }
  /**
   * refresh Tree
   * @param option 
   */
  refreshTree(option: { id: number, path: string } | boolean) {
    if (option !== true) {
      Object.assign(this.lastNodeSelected, option);
    }
    // clear cache search
    this.searchHistory.clear();
    this.reload();
  }

  /**
   * Set node active by id,path
   * @param node
   */
  setNodeActive(node) {
    let path = node.path;
    let id = node.id;
    let expandedNodeIds = {};
    let activeNodeIds = {};
    path = path.split(",");
    for (var i = 0; i < path.length; i++) {
      expandedNodeIds[path[i]] = true;
    }
    activeNodeIds[id] = true;
    this.setTreeState(expandedNodeIds, activeNodeIds);
    this.changeUser.emit((node as UseModel));
    this.lastNodeSelected = node;
    if (this.storeLocal === true) {
      localStorage.setItem(prefix + this.nodeRoot.id, JSON.stringify(this.lastNodeSelected));
    }
  }

  /**
   * Set state of tree
   * @param expandedNodeIds 
   * @param activeNodeIds 
   */
  setTreeState(expandedNodeIds, activeNodeIds) {
    let tree = {
      expandedNodeIds: {},
      activeNodeIds: {}
    }
    tree.activeNodeIds = activeNodeIds;
    tree.expandedNodeIds = expandedNodeIds;
    this.treeState = tree;
  }

  close() {
    this.tree.close();
    this.tree.userTreeResponsive = false;
    this.cdr.detectChanges();
   
  }
  onMouseEnter(){
    // if(!this.tree.userTreeResponsive){
    //   this.mouseEnter = true;
    // }
    
  }
  onMouseLeave(){
    // if(!this.tree.userTreeResponsive){
    //   this.mouseEnter = false;
    // }
    
  }
  saveCacheLayout(){
    if(this.allowCollapse){
      this.configLayoutMode.collapse = this.collapsed;
    }
    localStorage.setItem(this.keySaveCacheLayout,JSON.stringify(this.configLayoutMode));
  }
  loadCacheLayout(){
    const cacheStr = localStorage.getItem(this.keySaveCacheLayout);
    if(cacheStr){
      this.configLayoutMode = Object.assign(this.configLayoutMode,JSON.parse(cacheStr));
      if(this.allowCollapse){
        this.collapsed = this.configLayoutMode.collapse;
        if(this.collapsed){
          this.mouseEnter = false;
        }
        this.cdr.detectChanges();
      }
    }
  }
  

  /**
   * Scroll to node selected
   * @param id 
   */
  scrollToEl(id) {
    let _this = this;
    setTimeout(() => {
      let node = document.getElementById(_this.userTreeName + '_' + id);
      if (node) {
        node.scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' });
      }
    }, 800);
  }

  get state(): ITreeState {
    return this.treeState;
  }
  set state(state: ITreeState) {
    // this.treeState = state;
  }
}
export class UserModel{
  id: number;
  username: string;
  name: string;
  path: string;
  stockDevice: number;
  totalDevice: number;
  tree_root: boolean;
  type: number;
}