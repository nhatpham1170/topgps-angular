import { Component, Output, EventEmitter, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { AsciiPipe } from '@core/_base/layout';
import moment from 'moment';
import { tap } from 'rxjs/operators';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';

@Component({
    selector: 'kt-box-check-list',
    templateUrl: './box-check-list.component.html',
    styleUrls: ['./box-check-list.component.scss'],
    providers: [AsciiPipe]

  })
export class BoxCheckListComponent implements OnInit {
   @Input() dataChangeEmiter?: EventEmitter<any>;
    @Input() data?:any =[];
    @Input() filter?:string = 'name';
    @Input('placeholder') placeholder?: string = 'Search...';
    @Input('size') size?: string = 'col-lg-4';
    @Input('name') name?: string = 'box-list';
    @Input('maxHeight') maxHeight?: string = 'auto';
    @Input('options') options?: { type?: string };

    @Input() isSwitch: boolean = true;
    @Output() checkedChange = new EventEmitter();
    formCheckList: FormGroup;
    public checkallboxed:boolean;
    public searchText: string = "";
    public action:any;
    public isLoading:boolean = true;
    private subscription;

    constructor(
      private cdr: ChangeDetectorRef,
      private formBuilder: FormBuilder,
      private asciiPipe:AsciiPipe)
    {

      if (this.options == undefined) {
        this.options = {};
      }
      this.buildForm();
    }

    ngOnInit(){
      if (this.dataChangeEmiter) {
      this.subscription = this.dataChangeEmiter.subscribe(dataItem=>{
           this.data = dataItem;
           this.handlingData();
           this.cdr.detectChanges();
           this.isLoading = false;
        });
  
      }
      if (!this.dataChangeEmiter && this.data)  
      {
        this.handlingData();
        this.isLoading = false;

      }
   
      // this.addIdToFormArray();
    }

    ngOnDestroy(){
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
    }

    handlingData()
    {
      this.data = this.data.map(x => {
        if (x[this.filter]) {
          x['textSearch'] = this.asciiPipe.transform(x[this.filter] || "");
        }
        return x;
      });
    // this.data = dataItem;
     this.addIdToFormArray();
    }

    private buildForm(): void {
        this.formCheckList = this.formBuilder.group({
        checkallboxed:[],
        searchText:[]
        });
        }

   
    addIdToFormArray()
    {
        this.formCheckList.controls[this.name] = new FormArray([]);
        for(var i = 0; i < this.data.length ; i++)
        {
        
            let id = this.name+'_id_'+ this.data[i].id;
            this.formCheckList.controls[this.name]['controls'][id] =  new FormControl();
          
        }
        this.isCheckSelected();
    }

    isCheckSelectedTest(checked,id)
    {
      this.action = {
        checked:checked,
        id:id,
        name:this.data.find(x=>x.id == id).name
      };
      this.isCheckSelected();
    }

    isCheckSelected()
    {
        this.checkallboxed = this.data.every(function(item:any) {
          return item.checked == true;
        });
        this.getListIdSelected();
    }
    
    checkUncheckAll()
    {
        for (var i = 0; i < this.data.length; i++) {  
          this.data[i].checked = this.checkallboxed;
        };
        this.getListIdSelected();
    }
    
    getListIdSelected()
    {
        let listIdSelected : any = [];
        let listFullSelected : any = [];

        for (var i = 0; i < this.data.length; i++) 
        {
          if(this.data[i].checked)
          {
            listIdSelected.push(this.data[i].id);
            listFullSelected.push({
              id:this.data[i].id,
              name : this.data[i].name,
            });
          }
        }
        let params:any = {
            listChecked:listIdSelected,
            countChecked: listIdSelected.length,
            listFullSelected:listFullSelected,
            action:this.action
        }
        if(this.checkallboxed) params.checkedAll = true;
        if(listIdSelected.length == 0) params.checkedAll = false;

        this.checkedChange.emit(params);
    }

    searchItem(val)
    {
        this.searchText = val;
    }
    
    clearFilter()
    {
        this.searchText = "";
    }
    
}
