import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'kt-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  // @Input()emiterParrent:EventEmitter<string>;
  @Input()testInput:string;
  @Output()testOutput = new EventEmitter();
  constructor() {
    // this.testOutput = new EventEmitter();
    // this.testOutput ="testOutput";
   }

  ngOnInit() {
    this.testOutput.emit("Hello sidebar");
    // this.emiterParrent.subscribe(x=>console.log(x));
  }

}
