import { TollFeeDetail } from './../../../../../core/map/consts/map-tools/toll-fee.model';
import { environment } from '@env/environment.prod';
import { HttpClient } from '@angular/common/http';
import { DeviceMap } from './../../../../../core/map/_models/device-map';
import { FormControl, NgForm } from '@angular/forms';
import { Component, OnInit, Input, ChangeDetectorRef, ViewChild, ElementRef, OnDestroy, OnChanges } from '@angular/core';
import { MapService, MapConfigModel, MapUtil } from '@core/utils/map';
import { MapToolsService } from '@core/utils/map/_services/map-tools.service';
import { Subject, Subscription, of } from 'rxjs';
import { debounceTime, switchMap, catchError, map } from 'rxjs/operators';
import { DirectionInputControl } from '@core/map/consts/map-tools/direction-input-control';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { SearchResults } from '@core/utils/map/_model/search-address';
import * as L from 'leaflet';
import { getDistancePoint } from '@core/map/consts/map-tools/distance-point';
import { TollFee } from '@core/map/consts/map-tools/toll-fee.model';
declare var $;

const pointIcon = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="8px" height="8px"><path fill="#00acc1" d="M24 4A20 20 0 1 0 24 44A20 20 0 1 0 24 4Z"/><path fill="#e9f6f7" d="M24 8A16 16 0 1 0 24 40A16 16 0 1 0 24 8Z"/></svg>';
@Component({
  selector: 'kt-search-direction',
  templateUrl: './search-direction.component.html',
  styleUrls: ['./search-direction.component.scss']
})
export class SearchDirectionComponent implements OnInit, OnDestroy {
  public isShow = false;
  private isFullSize = true;
  private observable = new Subject();
  private subcription: Subscription;
  public inputControl: DirectionInputControl[] = [];
  private selectedId: number;
  private valueOnInput: string;
  private inputControlIndex = 3;
  private mapUtil = new MapUtil();
  private routeLayer: L.FeatureGroup = new L.FeatureGroup();
  private supportLayer: L.FeatureGroup = new L.FeatureGroup();
  private pointLayer: L.FeatureGroup = new L.FeatureGroup();
  private dragingLayer: L.FeatureGroup = new L.FeatureGroup();
  private tollLayer: L.FeatureGroup = new L.FeatureGroup();
  private markerLayer: L.FeatureGroup = new L.FeatureGroup();
  public isShowAddBtn: boolean = true;
  public isShowRemoveBtn: boolean = false;
  public isSearching: boolean = false;
  public startSearch: boolean;
  public isNoRoute: boolean = false
  public isFindingRoute: boolean = false;
  public selectedRoute: any;
  private savedPoints = {};
  @Input() mapService: MapService;
  @Input() listDevices: any[];
  @Input() type?: string;
  @Input() transportType?: any;
  public mapConfig: MapConfigModel;
  selectControl = new FormControl()
  public selectedType: number = 2;
  public routes: any;
  private savedPolylines: L.Polyline[] = [];
  private activeColor = '#1A73E8';
  private inactiveColor = '#b5bdc9';
  public step: number;
  public tollFee: { id?: TollFeeDetail[] };
  public isFindingToll: boolean;
  public showInputResult: boolean = false;
  public selectedDevice = {};
  public isPickingFromMap: boolean = false;
  isSelectType = true;
  public keyName: string;
  private minFee = 100000000000;
  public isAvoidTolls: boolean = false;
  @ViewChild('form', { static: true }) form: NgForm;
  isShowResult: boolean;
  constructor(public maptoolsService: MapToolsService, public cdr: ChangeDetectorRef, private http: HttpClient) {
    this.keyName = new Date().getMilliseconds().toString();
  }

  ngOnInit() {
    this.mapConfig = this.mapService.getConfigs();
    this.subscribeSearch();
    let startingPoint = new DirectionInputControl(this.inputControl.length, 'point0');
    this.inputControl.push(startingPoint);
    let endingPoint = new DirectionInputControl(this.inputControl.length, 'point1');
    this.inputControl.push(endingPoint);
    if (this.type == 'toll') {
      this.mapService.map.on('dragend', () => {
        this.showPoints();
      })
      this.mapService.map.on('zoomend', () => {
        this.showPoints();
      })
    }
    this.getPlaceFromMap();
    this.pointLayer.addTo(this.mapService.map);
  }

  getPlaceFromMap() {
    this.mapService.map.on('click', ($event) => {
      for (let i = 0; i < this.form['_directives'].length; i++) {
        if (this.form['_directives'][i].value == '') {
          let marker = this.mapUtil.createMarker($event['latlng'], { zIndexOffset: -9999 }, {
            width: 14,
            heigth: 10,
            mapIconUrl: getDistancePoint()
          });
          this.markerLayer.addLayer(marker);
          this.markerLayer.addTo(this.mapService.map)
          this.setPoint(this.form, this.inputControl[i], undefined, $event['latlng'], true);
          this.findPlaceInMap($event, this.form, i, $event);
          return
        }
      }
    })
  }

  pickFromMap(form) {
    this.form = form;
    this.isPickingFromMap = !this.isPickingFromMap
  }

  changeSelect(value) {
    this.isSelectType = value;
    this.selectControl.setValue(this.selectedType)
  }

  subscribeSearch() {
    this.subcription = this.observable.pipe(debounceTime(100), switchMap((address: string) => {
      return (this.maptoolsService.searchAddress(address).pipe(
        catchError(() => {
          return of({ status: 400 });
        })))
    }
    )).subscribe((data: SearchResults) => {
      if (data.status == 200) {
        let selectedInput = this.inputControl.find(input => {
          return input.id == this.selectedId
        });
        this.isSearching = false;
        selectedInput.data = data;
        selectedInput.active = 0;
        if (data.result, length == 0) {
          selectedInput.value = this.valueOnInput
        }
        this.cdr.detectChanges();
      }
    })
  }

  fullWidth() {
    if (this.inputControl.length > 2) return;
    this.isFullSize = !this.isFullSize;
    let container = document.querySelector('.search-direction');
    let arrow = document.querySelector('.arrow');
    if (container instanceof HTMLElement && arrow instanceof HTMLElement) {
      if (this.isFullSize) {
        container.style.height = "100%";
        arrow.style.transform = "rotate(00deg)"
      } else {
        if (this.type == 'toll') return;
        container.style.height = "500px";
        arrow.style.transform = "rotate(180deg)"
      }
    }
  }

  search(id: number, address: string, event: KeyboardEvent, form: NgForm) {
    this.selectedId = id;
    let selectedInput = this.inputControl.find(input => {
      return input.id == this.selectedId
    });
    if (address == '') {
      selectedInput.data = undefined;
      selectedInput.type = '';
      return;
    };
    if (event.keyCode == 32) return;
    selectedInput.value = address;
    if (selectedInput.data) {
      switch (event.keyCode) {
        case (40): {
          if (selectedInput.active == selectedInput.data.result.length - 1) return;
          selectedInput.active++;
          return;
        }
        case (38): {
          if (selectedInput.active == 0) return;
          selectedInput.active--;
          return;
        }
        case (13): {
          this.setPoint(form, selectedInput);
          return;
        }
        case (32): {
          return;
        }
      }
    }
    if (this.valueOnInput == address) return;
    if (address.startsWith('@')) {
      selectedInput.type = 'device';
      selectedInput.data = { result: this.listDevices }
      selectedInput.active = 0;

      if (address.length > 1) {
        let keyWord = address.slice(1, address.length);
        selectedInput.data.result = this.listDevices.filter(device => {
          return device.name.includes(keyWord)
        });
      }
      return;
    }
    if (selectedInput.type !== 'device') {
      this.observable.next(address);
      selectedInput.type = 'address';
      this.isSearching = true;
      this.startSearch = true;
    }
    this.valueOnInput = address;
  }

  setPoint(form: NgForm, input: DirectionInputControl, index?: number, value?: string, isNoSubmit?: boolean) {
    if (value) {
      form.controls[input.name].setValue(value);
      input.value = value;
      if (!isNoSubmit) this.onSubmit(this.form);
      this.cdr.detectChanges();
      return;
    }
    if (index) input.active = index;
    if (input.data.result.length == 0) return;
    let selectedInput = input.data.result[input.active];
    if (input.type == 'address') {
      form.controls[input.name].setValue(selectedInput['address']);
      input.value = selectedInput['address']
    } else {
      this.selectedDevice[this.inputControl.indexOf(input)] = selectedInput['name'];
      form.controls[input.name].setValue('')
      form.controls[input.name].disable();
      input.value = selectedInput['lat'] + ',' + selectedInput['lng'];
    }
    input.data = undefined;
    if (this.type) {
      this.onSubmit(this.form);
    }
  }

  clearInput(form: NgForm, input: DirectionInputControl,option?) {
    form.controls[input.name].enable();
    this.selectedDevice[this.inputControl.indexOf(input)] = undefined;
    input.type = '';
    if(option){
      form.controls[input.name].setValue('');
      input.type = '';
      input.value = '';
    }
    
  }

  drop(event: CdkDragDrop<any>) {
    moveItemInArray(this.inputControl, event.previousIndex, event.currentIndex);
    this.onSubmit(this.form);
  }

  addMoreDestionation() {
    if (!this.isFullSize) { this.fullWidth() };
    this.isShowRemoveBtn = true;
    this.inputControlIndex++;
    let newDestiation = new DirectionInputControl(this.inputControlIndex, 'point' + this.inputControlIndex);
    this.inputControl.push(newDestiation);
    if (this.inputControl.length == 6) { this.isShowAddBtn = false; return };
  }

  clearValue(form: NgForm, name: string) {
    form.controls[name].setValue('');
  }

  removeDestination(id: number) {
    this.isShowAddBtn = true;
    this.inputControl.splice(id, 1);
    this.onSubmit(this.form);
    if (this.inputControl.length == 2) { this.isShowRemoveBtn = false; return }
  }

  onSubmit(form: NgForm) {
    if (form.invalid) return;
    let listPointValue = this.inputControl.map(input => input.value);
    if (listPointValue.length < this.inputControl.length) {
      listPointValue = Object.values(form.value);
    }
    let url = '';
    listPointValue.forEach(value => {
      url = url + value + '/';
    })
    if (this.type == 'toll') {
      this.isNoRoute = false;
      this.getDirection(listPointValue[0], listPointValue[listPointValue.length - 1], listPointValue.slice(1, listPointValue.length - 1), form);
    } else {
      window.open(`https://www.google.com/maps/dir/${url}`, '_black')
    }
  }

  onCheckbox(checkbox) {
    this.isAvoidTolls = checkbox.checked
    this.onSubmit(this.form)
  }

  changeSubmit(form) {
    if (this.type) {
      this.onSubmit(form);
    }
  }

  getDirection(origin, destination, waypoint?, form?: NgForm, option?) {
    // Check waypoint and option
    this.isShowResult == false
    this.isFindingRoute = true;
    this.minFee = 100000000000;
    let points = waypoint.map(place => {
      return { location: place }
    });
    
    if (option) {
      waypoint.push(option);
      points.push({ location: option })
    }

    return new google.maps.DirectionsService()
      .route({ origin: origin, destination: destination, travelMode: google.maps.TravelMode.DRIVING, waypoints: points, optimizeWaypoints: true, provideRouteAlternatives: true, avoidTolls: this.isAvoidTolls },
        (data) => {
          if (data) this.routes = data.routes;
          this.savedPolylines = [];
          this.routeLayer.clearLayers();
          this.tollLayer.clearLayers();
          this.tollFee = {};
          this.cdr.detectChanges();
          if (data.routes.length == 0) {
            this.isNoRoute = true;
            return;
          }
          /* Loop routes to create line on map*/
          this.routes.forEach((route, routeIndex) => {
            if (!option) {
              this.markerLayer.clearLayers();
            }
            let poly;
            let isPolyDragging = false;
            let isStartPointDragging = false;
            let isEndPointDragging = false;
            let isWaypointDragging = false;
            let checkClicking = false;
            let selectedPoint;
            let polyPoint = [];
            let duration = 0;
            let distance = 0;
            this.step = undefined;
            
            if (option) {
              route.waypoint_order.forEach((point, i) => {
                if (point == waypoint.length - 1) {
                  route.waypoint_order.splice(i, 1);
                  waypoint.splice(waypoint.length - 1, 1);
                }
              })
            }

            /*Start-end points start*/
            route.legs.forEach((leg, i) => {
              leg.steps.forEach(step => {
                polyPoint = polyPoint.concat(step.path);
              })
              poly = polyPoint;
              duration += leg.duration.value;
              distance += leg.distance.value;
              if (!option) {
                
                
                let endAddress = {
                  lat: leg.end_location.lat(),
                  lng: leg.end_location.lng(),
                }
                let startAddress = {
                  lat: leg.start_location.lat(),
                  lng: leg.start_location.lng(),
                }
                if (i === 0) {
                  let start = this.mapUtil.createMarker(startAddress, { zIndexOffset: -9999 }, {
                    width: 14,
                    heigth: 10,
                    mapIconUrl: getDistancePoint()
                  })

                  start.on('mousedown', () => {
                    this.mapService.map.dragging.disable();
                    isStartPointDragging = true;
                  })
                  this.markerLayer.addLayer(start);
                } else {
                  let point = this.mapUtil.createMarker(startAddress, { zIndexOffset: -9999 }, {
                    width: 14,
                    heigth: 10,
                    mapIconUrl: getDistancePoint()
                  })

                  point.on('mousedown', () => {
                    this.mapService.map.dragging.disable();
                    isWaypointDragging = true;
                    selectedPoint = i - 1;
                  })
                  this.markerLayer.addLayer(point);
                }

                if (i == data.routes[0].legs.length - 1) {
                  let end = this.mapUtil.createMarker(endAddress, { zIndexOffset: -9999 }, {
                    width: 14,
                    heigth: 10,
                    mapIconUrl: getDistancePoint()
                  });
                  end.on('mousedown', () => {
                    this.mapService.map.dragging.disable();
                    isEndPointDragging = true;
                  })
                  this.markerLayer.addLayer(end);
                }
              }
            })
            /*Start-end points end*/

            /* Add map's eventListener */
            this.mapService.map.on('mousemove', ($event) => {
              if (isStartPointDragging || isPolyDragging || isEndPointDragging || isWaypointDragging) {
                this.dragingLayer.clearLayers();
                let marker = this.mapUtil.createMarker($event['latlng'], { zIndexOffset: -9999 }, {
                  width: 14,
                  heigth: 10,
                  mapIconUrl: getDistancePoint()
                });
                checkClicking = true;
                this.dragingLayer.addLayer(marker);
                
              }
            })

            this.mapService.map.on('mouseup', ($event) => {
              this.mapService.map.dragging.enable();
              this.dragingLayer.clearLayers();
              // On polyline Dragging
              if (isPolyDragging && checkClicking) {
                this.getDirection(origin, destination, waypoint, form, $event['latlng']);
                checkClicking = false;
                isPolyDragging = false;
              }
              // On startpoint dragging
              else if (isStartPointDragging && checkClicking) {
                this.findPlaceInMap($event, form, 0,undefined, true);
                this.getDirection($event['latlng'], destination, waypoint, form);
                isStartPointDragging = false;
                checkClicking = false;
              }
              // On endpoint dragging
              else if (isEndPointDragging && checkClicking) {
                this.findPlaceInMap($event, form, this.inputControl.length - 1, undefined,true);
                this.getDirection(origin, $event['latlng'], waypoint, form);
                isEndPointDragging = false;
                checkClicking = false;
              }
              // On waypoint dragging
              else if (isWaypointDragging && checkClicking) {
                this.findPlaceInMap($event, form, route.waypoint_order[selectedPoint] + 1);
                waypoint.splice(route.waypoint_order[selectedPoint], 1, $event['latlng']);
                this.getDirection(origin, destination, waypoint, form, true);
                isWaypointDragging = false;
                checkClicking = false;
              }
            });

            /* Polyline start */
            let polylinePoint = poly.map(latlng => {
              return { lat: latlng.lat(), lng: latlng.lng() }
            })

            //Create route's polyline
            this.savedPoints = [];
            let polyline = this.mapService.createPolyline(polylinePoint, {
              color: this.inactiveColor,
              weight: 6,
              opacity: 1,
            },
              {
                isShow: false,
                options: {
                  offset: 3,
                },
              })

            // Get polyline bound
            let neLat = polyline.getBounds().getNorthEast().lat;
            let neLng = polyline.getBounds().getNorthEast().lng;
            let swLat = polyline.getBounds().getSouthWest().lat;
            let swLng = polyline.getBounds().getSouthWest().lng;
            // Add route's property
            this.calculateFee(polylinePoint, routeIndex, neLat, neLng, swLat, swLng);
            route.polylinePoint = polylinePoint;
            route.duration = duration;
            route.distance = distance;

            // Add polyline's eventListener
            polyline.on('mousedown', ($event) => {
              this.mapService.map.dragging.disable();
              isPolyDragging = true;
            });

            polyline.on('click', () => {
              polyline.bringToFront();
              this.tollLayer.bringToFront();
              this.step = routeIndex;
              isPolyDragging = false;
              this.selectedRoute = polylinePoint;
              this.savedPolylines.forEach((line: L.Polyline) => {
                line.setStyle({
                  color: this.inactiveColor
                })
              })
              polyline.setStyle({
                color: this.activeColor
              })
              this.cdr.detectChanges();
            })

            polyline.on('mousemove', ($event) => {
              this.supportLayer.clearLayers();
              let point2 = this.maptoolsService.createTollFeePopup(
                $event['latlng'], { zIndexOffset: 9999 },
                { width: 32, heigth: 150 },
                { type: 'polyline', title: 'MAP.FEATURES.DIRECTION_CHANGE', fee: route.totalFee, distance: route.distance, time: route.duration }
              );
              this.supportLayer.addLayer(point2);
              polyline.on('mouseout', () => {
                this.supportLayer.clearLayers();
              })
            })
            this.savedPolylines.push(polyline);
            /* Polyline end */

            /* Add layer to map */
            this.routeLayer.addLayer(polyline);
            this.routeLayer.addLayer(this.supportLayer)
            this.routeLayer.addLayer(this.markerLayer);
            this.routeLayer.addLayer(this.dragingLayer)
            this.routeLayer.addTo(this.mapService.map);
            this.mapService.map.fitBounds(polyline.getBounds())
          })
        })
  }

  findPlaceInMap(event, form, i, onMapEvent?, noSubmit?: boolean) {
    this.maptoolsService.searchAddress(`${event['latlng'].lat + ',' + event['latlng'].lng}`).subscribe(data => {
      if (data.status == 200) {
        if (data.result.length > 0) {
          this.setPoint(form, this.inputControl[i], undefined, data.result[0].address, noSubmit);
          if (onMapEvent) {
            let marker = this.mapUtil.createMarker(onMapEvent['latlng'], { zIndexOffset: -9999 }, {
              width: 14,
              heigth: 10,
              mapIconUrl: getDistancePoint()
            });
            this.markerLayer.addLayer(marker);
          }
        } 
      }
    })
  }

  showPoints() {
    this.pointLayer.clearLayers();
    if (!this.selectedRoute) return;
    if (this.mapService.map.getZoom() < 16) return;
    let currentBounds = this.mapService.getBounds();
    this.selectedRoute.forEach((point, i) => {
      let p = L.point(point.lat, point.lng);
      if (currentBounds.contains(p)) {
        if (this.savedPoints[i] == undefined) {
          let pointMarker = this.mapUtil.createMarker(
            point,
            { zIndexOffset: 999 },
            {
              width: 8,
              heigth: 11,
              mapIconUrl: pointIcon
            }
          )
          this.savedPoints[i] = pointMarker;
          this.pointLayer.addLayer(pointMarker);
        } else {
          let pointMarker = this.savedPoints[i];
          this.pointLayer.addLayer(pointMarker);
        }
      }
    })
    this.cdr.detectChanges();
  }

  calculateFee(polylinePoint, i, neLat, neLng, swLat, swLng) {
    this.tollLayer.clearLayers();
    this.isFindingToll = true;
    let encodedPoint = this.mapUtil.encode(polylinePoint);
    this.maptoolsService.calculateTollFee(encodedPoint, this.selectedType, neLat, neLng, swLat, swLng).subscribe((data: TollFee) => {
      if (data.status !== 200) return;
      this.isFindingRoute = false;
      this.routes[i].fee = data.result;
      this.routes[i].totalFee = this.calculateTotalFee(data.result);
      if (this.routes[i].totalFee < this.minFee) {
        this.savedPolylines[i].fire('click');
        this.minFee = this.routes[i].totalFee;
      }
      this.routes[i].fee.forEach(route => {
        route.tollStations.forEach(toll => {
          let tollLatlng = this.mapUtil.decode(toll.encodedPoints);
          let tollMarker = L.circle(tollLatlng[0], { radius: toll.radius, color: 'red', fillOpacity: 0.2 });
          this.tollLayer.addLayer(tollMarker);
          this.tollLayer.addTo(this.mapService.map);
          this.cdr.detectChanges();
        })
      })
      this.isFindingToll = false;
      this.cdr.detectChanges();
      this.isShowResult = true;
    })
  }

  calculateTotalFee(tolls: TollFeeDetail[]) {
    let totalFee = 0;
    tolls.forEach(toll => {
      totalFee += toll.amount
    })
    return totalFee
  }

  panToToll(station) {
    let latlng = this.mapUtil.decode(station.encodedPoints);
    this.mapService.map.setView(latlng[0], 15);
  }

  getYourLocation(form, input) {
    let latlng;
    this.mapService.map.locate({
      setView: false,
    });
    this.mapService.map.on('locationfound', (e: any) => {
      this.mapConfig.features.location.value = !this.mapConfig.features.location.value;
      latlng = ([e.latlng.lat, e.latlng.lng]);
      this.setPoint(form, input, undefined, latlng.toString());
      this.mapService.map.removeEventListener('locationfound');
    });
    this.mapService.map.on('locationerror', function (e: any) {
      alert(e.message);
    });
  }

  onChangeSelect() {
    this.selectedType = this.selectControl.value
    this.onSubmit(this.form)
  }

  selectRoute(index: number) {
    this.savedPolylines[index].fire('click')
  }

  close(){
    this.mapConfig.features.direction.value = !this.mapConfig.features.direction.value;
    this.markerLayer.clearLayers();
  }

  ngOnDestroy() {
    this.subcription.unsubscribe();
  }

}
