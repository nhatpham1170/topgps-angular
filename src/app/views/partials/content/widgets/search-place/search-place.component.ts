import { environment } from '../../../../../../environments/environment.prod';
import { FormControl } from '@angular/forms';
import { SearchResults } from '../../../../../core/utils/map/_model/search-address';
import { MapToolsService } from '../../../../../core/utils/map/_services/map-tools.service';
import { switchMap, debounceTime, catchError } from 'rxjs/operators';
import { Subject, of, Subscription } from 'rxjs';
import { Component, OnInit, Input, ChangeDetectorRef, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MapService } from '@core/utils/map/_services/map.service';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

const API_URL = 'https://maps.googleapis.com/maps/api/place/autocomplete/json';
const api = environment.api.host + '/utility/maps/places';
@Component({
  selector: 'kt-search',
  templateUrl: './search-place.component.html',
  styleUrls: ['./search-place.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  public data: SearchResults;
  public isShow = false;
  public isSearching: boolean = false;
  public startSearch: boolean;
  private subcription: Subscription;
  @Input() mapService: MapService;
  @ViewChild('searchField', { static: true }) searchField: ElementRef;
  @ViewChild('searchResult', { static: true }) searchResult: ElementRef;
  @ViewChild('btn', { static: true }) btn: ElementRef;
  @ViewChild('input', { static: true }) input: ElementRef;
  private observable = new Subject();
  constructor(public maptoolsService: MapToolsService, public cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.subscribeSearch();
  }

  subscribeSearch() {
    this.subcription = this.observable.pipe(debounceTime(100), switchMap((address: string) => {
      return (this.maptoolsService.searchAddress(address).pipe(
        catchError(() => {
          return of({ status: 400 });
        })))
    }
    )).subscribe(data => {
      if (data.status == 200) {
        this.data = data;
        this.data.active=0;
        this.data.result.forEach(place=>{
          place.isActive = false;
        })
        this.isSearching = false;
        this.cdr.detectChanges();
      }
    })
  }

  search(address: string,event:KeyboardEvent) {
    if (address == '') {
      this.data = undefined;
      this.searchResult.nativeElement.classList.add('no-box-shadow');
      this.input.nativeElement.classList.remove('no-border-bot');
      return
    };
    
    switch (event.keyCode) {
    case (40): {
      if(this.data.active==this.data.result.length-1) return;
      this.data.active++;
      return;
    }
    case ( 38):{
      if (this.data.active==0) return;
      this.data.active--;
      return;
    }
    case(13):{
      this.panToPlace(this.data.result[this.data.active]);
      return;
    }
    case(32):{
      return;
    }
  }
    this.searchResult.nativeElement.classList.remove('no-box-shadow');
    this.input.nativeElement.classList.add('no-border-bot');
    this.observable.next(address);
    this.isSearching = true;
    this.startSearch = true;
  }

  toggle() {
    this.isShow = !this.isShow
    if (this.isShow) {
      this.searchField.nativeElement.style.width = "300px";
      this.searchResult.nativeElement.style.width = "298px";
      this.btn.nativeElement.classList.add('no-border');
    } else {
      this.searchField.nativeElement.style.width = "0%";
      this.searchResult.nativeElement.style.width = "0";
      this.btn.nativeElement.classList.remove('no-border');
      this.searchResult.nativeElement.classList.add('no-box-shadow');
    }
  }

  panToPlace(place,index?) {
    if(this.data.result.length==0) return;
    let { lat, lng } = place;
    this.mapService.map.panTo([lat, lng]);
    if(index) this.data.active = index;
  }

  ngOnDestroy() {
    this.subcription.unsubscribe();
  }
}

