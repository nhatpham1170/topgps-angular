import { Subscription } from 'rxjs';
import { MapToolsService } from './../../../../../core/utils/map/_services/map-tools.service';
import { MapService } from '@core/utils/map/_services/map.service';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'kt-distance',
  templateUrl: './distance.component.html',
  styleUrls: ['./distance.component.scss'],
  providers: [MapToolsService]
})
export class DistanceComponent implements OnInit, OnDestroy {

  @Input() mapService: MapService;
  public distance: string;
  public isOnRunning: boolean = false;
  private subcrition: Subscription;
  constructor(private mapToolsService: MapToolsService) { }

  ngOnInit() {
    this.mapToolsService.setMapService(this.mapService);
    this.subcrition = this.mapToolsService.distanceChange.subscribe(data=>{
        this.distance = data
    })
  }

  calculate() {
    this.isOnRunning = !this.isOnRunning;
    this.mapToolsService.calculateDistance(this.isOnRunning)
  }

  ngOnDestroy(){
    this.subcrition.unsubscribe();
  }
}
