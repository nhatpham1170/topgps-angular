// Angular
import { NgModule } from '@angular/core';
import { DateRangePickerComponent } from './date-range-picker/date-range-picker.component';
import { NgbModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { CoreModule } from '@core/core.module';
import { CommonModule } from '@angular/common';

@NgModule({
	imports: [
		CoreModule,
		NgbModule,
		NgbModalModule,
		CommonModule
	],
	declarations: [DateRangePickerComponent],
	exports: [DateRangePickerComponent]
})
export class DatepickerModule {
	
}
