import { Component, OnInit, Input, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CurrentUserService } from '@core/auth';
import * as moment from 'moment';
import { isArray } from 'util';
import { TranslationService } from '@core/_base/layout';
import { tap } from 'rxjs/operators';

declare var $;
@Component({
  selector: 'kt-date-range-picker',
  templateUrl: './date-range-picker.component.html',
  styleUrls: ['./date-range-picker.component.scss']
})
export class DateRangePickerComponent implements OnInit {
  @Input('options') options: {
    size?: string,//"sm" | "lg" | "md"
    key?: string, // id datepicker 
    placeholder?: string,
    tooltip?: string,
    singleDatePicker?: boolean, //show single date picker or show date range picker
    select?: "from" | "to" // from: select date <= now || to: select datetime >= now
    timePicker?: boolean, // allow show timepicker
    optionDatePicker?: any, // set option date picker !important
    ranges?: any, // "form" || "to" || custom by key setup: ['TO_DAY','YESTERDAY',..]
    selectRange?: string,
    format?: string,// date | datetime | format custom 'YYYY-MM-DD HH:mm:ss'
    translate?: boolean, // allow translate ** default:true,
    startDate?: any, // set start date ** default: now
    endDate?: any, // set end date ** default: now
    minDateOffset?: { days?: number }, // set minDate example: {days:1,months:2}
    maxDateOffset?: { days?: number }, // set maxDate example: {days:1,months:2}
    autoSelect?: boolean,// auto select time if startDate is null &&  endDate is null ** default:false
    btnClear?: boolean,
    disabled?:boolean
    // switchRange?:boolean, // next and previous date range
  }
  @Output('dateChange') dateChange: EventEmitter<{ startDate: any, endDate: any, data?: any }>; // event when select date
  private startDate: any;
  private endDate: any;
  public size: string;
  private optionsDefault: any;

  public disabled:boolean = true;
  constructor(private translateService: TranslateService,
    private currentUserService: CurrentUserService,
    private cdr: ChangeDetectorRef,
    private translationService: TranslationService) {
    this.size = "";
    this.dateChange = new EventEmitter();
    this.translationService.eventChange.pipe(
      tap(lang => {
        this.updateLanguage();
      })
    ).subscribe();

    this.optionsDefault = {
      "singleDatePicker": false,
      "showDropdowns": true,
      // "minYear": 1999,
      // "maxYear": 2020,
      "showWeekNumbers": false,
      "showISOWeekNumbers": false,
      "timePicker": true,
      "timePicker24Hour": true,
      "timePickerIncrement": 1,
      "timePickerSeconds": false,
      "autoApply": false,
      "locale": {
        "format": this.currentUserService.dateFormat,
        "separator": " - ",
        "applyLabel": "Apply",
        "cancelLabel": "Cancel",
        "fromLabel": "From",
        "toLabel": "To",
        "customRangeLabel": "Custom",
        "weekLabel": "W",
        "daysOfWeek": [
          "Su",
          "Mo",
          "Tu",
          "We",
          "Th",
          "Fr",
          "Sa"
        ],
        "monthNames": [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December"
        ],
        "firstDay": 3
      },
      "alwaysShowCalendars": false,
      "showCustomRangeLabel": true,
      "startDate": this.now().startOf('day'),
      "endDate": this.now().endOf('day'),
      "opens": "right",
      "drops": "down",
      autoUpdateInput: false,
      todayHighlight: true,
      orientation: "bottom left",
      clearBtn: true,
      // autoclose: true,
      templates: {
        // leftArrow: '<i class="icon-arrow-alt-circle-left"></i>',
        // rightArrow: '<i class="icon-arrow-alt-circle-right"></i>'
      },
    }, function (start, end, label) {
      // console.log('New date range selectd: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
    }

  }

  ngOnInit() {
    let options = { singleDatePicker: true, format: "date", startDate: this.now().startOf('day'), endDate: this.now().endOf('day') };
    if (this.options == undefined) this.options = options;

    else {
      // convert start date, end 
      if (this.options.startDate != undefined) this.options.startDate = moment(this.options.startDate, "YYYY/MM/DD HH:mm:ss");
      if (this.options.endDate != undefined) this.options.endDate = moment(this.options.endDate, "YYYY/MM/DD HH:mm:ss");
      if (this.options.selectRange) {
        let range = {};
        this.addRange(this.options.selectRange, null, range);
        let rangeKey = Object.keys(range);
        if (rangeKey.length > 0) {
          this.options.startDate = range[rangeKey[0]][0];
          this.options.endDate = range[rangeKey[0]][1];
        }
      }
      Object.keys(options).forEach((k) => {
        if (this.options[k] == undefined) this.options[k] = options[k];
      });
    }
    if (this.options.key == undefined) this.options.key = "datePickerCT_" + moment().unix();

    this.init();
  }
  private init(options?: any) {
    let _this = this;
    // process size
    this.processSize(this.options);
    this.processDatePicker(this.options);
    this.translate(this.options);
    this.cdr.detectChanges();

    if (this.options.startDate != undefined) this.optionsDefault.startDate = this.options.startDate;
    if (this.options.endDate != undefined) this.optionsDefault.endDate = this.options.endDate;
    if (options != undefined) Object.assign(this.optionsDefault, options);
    
    $("#" + this.options.key).daterangepicker(this.optionsDefault);
    $("#" + this.options.key).on('apply.daterangepicker', function (ev, picker) {
    
      
      //do something, like clearing an input
      _this.startDate = picker.startDate;
      _this.endDate = picker.endDate;
      // set value
      let str = picker.startDate.format(_this.optionsDefault.locale.format);
      if (picker.startDate.format(_this.optionsDefault.locale.format) != picker.endDate.format(_this.optionsDefault.locale.format)
        && _this.optionsDefault.singleDatePicker === false)
        str += " - " + picker.endDate.format(_this.optionsDefault.locale.format);
      $("#" + _this.options.key).val(str);
      
      _this.dateChange.emit({ startDate: picker.startDate, endDate: picker.endDate, data: picker });
    });

    if (!this.options.autoSelect) {
      this.clear();
    }
    else {
      
      if (this.options.autoSelect === true || this.options.startDate != undefined
        || (options != undefined && options.startDate != undefined)) {
        let data = $("#" + this.options.key).data('daterangepicker');
     
        _this.startDate = data.startDate;
        _this.endDate = data.endDate;
        let str = _this.startDate.format(_this.optionsDefault.locale.format);
        if (_this.startDate.format(_this.optionsDefault.locale.format) != _this.endDate.format(_this.optionsDefault.locale.format)
          && _this.optionsDefault.singleDatePicker === false) {
          str += " - " + _this.endDate.format(_this.optionsDefault.locale.format);
        }
        $("#" + _this.options.key).val(str);
        
        _this.dateChange.emit({ startDate: _this.startDate, endDate: _this.endDate, data: data });
      }
    }
  }

  private processSize(options) {
    if (options.size != undefined) {
      switch (options.size) {
        case "sm":
          this.size = "form-control-sm";
          break;
        case "lg":
          this.size = "form-control-lg";
          break;
        case "md":
          this.size = "form-control-md";
          break;
        default:
          this.size = "form-control-md";
          break;
      }
    }
  }

  private processDatePicker(options) {
    // format
    if (!options.format) options.format = "date";
    switch (options.format) {
      case "date":
        this.optionsDefault.locale.format = this.currentUserService.dateFormat;
        break;
      case "datetime":
        this.optionsDefault.locale.format = this.currentUserService.dateFormat + " " + this.currentUserService.timeFormat;
        break;
      default:
        this.optionsDefault.locale.format = options.format;
        break;
    }
    // singleDatePicker
    if (options.singleDatePicker) this.optionsDefault.singleDatePicker = true;
    else this.optionsDefault.singleDatePicker = false;

    // firstDay
    this.optionsDefault.locale.firstDay = this.currentUserService.firstDay;
    let option: any = {
      week: {
        dow: this.currentUserService.firstDay,
      },
    }
    moment.updateLocale('en', option);

    //timePicker
    if (options.timePicker != undefined) this.optionsDefault.timePicker = options.timePicker;
    else this.optionsDefault.timePicker = false;

    // date form/to
    switch (options.select) {
      case "from":
        this.optionsDefault.maxDate = this.now();
        break;
      case "to":
        this.optionsDefault.minDate = this.now();
        break;
    }

    // ranges
    if (!options.ranges) delete this.optionsDefault.ranges;
    else {
      let ranges = {};

      if (isArray(options.ranges)) {
        options.ranges.forEach(x => {
          ranges = this.addRange(x, this.now(), ranges);
        });
      }
      else {
        switch (options.ranges) {
          case "from":
            ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.TO_DAY")]
              = [this.now().startOf('day'), this.now().endOf('day')];
            ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.YESTERDAY")]
              = [this.now().subtract(1, 'days').startOf('day'), this.now().subtract(1, 'days').endOf('day')];
            ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.THIS_WEEK")]
              = [this.now().startOf('week').startOf('day'), this.now().endOf('day')];
            ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.LAST_WEEK")]
              = [this.now().subtract(1, 'week').startOf('week').startOf('day'), this.now().subtract(1, 'week').endOf('week').endOf('day')];
            ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.THIS_MONTH")]
              = [this.now().startOf('month').startOf('day'), this.now().endOf('day')];
            // ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.LAST_30_DAY")]
            //   = [this.now().subtract(29, 'days').startOf('day'), this.now().endOf('day')];
            ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.LAST_MONTH")]
              = [this.now().subtract(1, 'months').startOf('month').startOf('day'), this.now().subtract(1, 'months').endOf('month').endOf('day')];
            break;
          case "to":
            ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.TO_DAY")] = [this.now().startOf('day'), this.now().endOf('day')];
            ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.ADD_2_WEEK")] = [this.now().startOf('day'), this.now().add(2, 'week').endOf('day')];
            ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.ADD_1_MONTH")] = [this.now().startOf('day'), this.now().add(1, 'months').endOf('day')];
            ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.ADD_3_MONTH")] = [this.now().startOf('day'), this.now().add(3, 'months').endOf('day')];
            ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.ADD_6_MONTH")] = [this.now().startOf('day'), this.now().add(6, 'months').endOf('day')];
            ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.ADD_1_YEAR")] = [this.now().startOf('day'), this.now().add(1, 'years').endOf('day')];
            break;
        }
      }
      if (Object.keys(ranges).length > 0) {
        this.optionsDefault.ranges = ranges;
      }
    }

    // from day, to days 
    if (options.minDateOffset != undefined) {
      let minDate = this.now();
      Object.keys(options.minDateOffset).forEach(x => {
        minDate.subtract(x, options.minDateOffset[x]);
      });
      this.optionsDefault.minDate = minDate;
    }
    if (options.maxDateOffset != undefined) {
      let maxDate = this.now();
      Object.keys(options.maxDateOffset).forEach(x => {
        maxDate.subtract(x, options.maxDateOffset[x]);
      });
      this.optionsDefault.maxDate = maxDate;
    }

    // optionsDatePicker
    if (options.optionDatePicker != undefined) Object.assign(this.optionsDefault, options.optionDatePicker);
  }
  private updateLanguage() {
    let _this = this;
    if(!this.options) return;
    this.processDatePicker(this.options);
    this.translate(this.options);
    if (this.startDate) this.optionsDefault.startDate = this.startDate;
    if (this.endDate) this.optionsDefault.endDate = this.endDate;
    $("#" + this.options.key).daterangepicker(this.optionsDefault);
    $("#" + this.options.key).on('apply.daterangepicker', function (ev, picker) {
      //do something, like clearing an input
      _this.startDate = picker.startDate;
      _this.endDate = picker.endDate;
      let str = _this.startDate.format(_this.optionsDefault.locale.format);
        if (_this.startDate.format(_this.optionsDefault.locale.format) != _this.endDate.format(_this.optionsDefault.locale.format)
          && _this.optionsDefault.singleDatePicker === false) {
          str += " - " + _this.endDate.format(_this.optionsDefault.locale.format);
        }
        $("#" + _this.options.key).val(str);

      _this.dateChange.emit({ startDate: picker.startDate, endDate: picker.endDate, data: picker });
    });

  }

  public refresh() {
    this.init();
  }

  public clear() {
    this.startDate = null;
    this.endDate = null;
    $("#" + this.options.key).val('');
  
    this.dateChange.emit({ startDate: null, endDate: null, data: undefined });
  }

  public setDate(startDate, endDate) {
    this.options.startDate = startDate;
    this.options.endDate = endDate;
    this.refresh();
  }
  public nextDay() {
    let data = $("#" + this.options.key).data("daterangepicker");
    let daySpan = moment(data.endDate.format("YYYY/MM/DD HH:mm:ss"), "YYYY/MM/DD HH:mm:ss").diff(data.startDate, "days") + 1;
    let startDate = moment(data.startDate.format("YYYY/MM/DD HH:mm:ss"), "YYYY/MM/DD HH:mm:ss").add(daySpan, "days");
    let endDate = moment(data.endDate.format("YYYY/MM/DD HH:mm:ss"), "YYYY/MM/DD HH:mm:ss").add(daySpan, "days").endOf("day");
    if (data.maxDate === false || (startDate <= data.maxDate.startOf("day") && endDate <= data.maxDate.endOf("day"))) {
      this.init({
        startDate: startDate, endDate: endDate
      })
    }
  }
  public previousDay() {
    let data = $("#" + this.options.key).data("daterangepicker");
    let daySpan = moment(data.endDate.format("YYYY/MM/DD HH:mm:ss"), "YYYY/MM/DD HH:mm:ss").diff(data.startDate, "days") + 1;
    let startDate = moment(data.startDate.format("YYYY/MM/DD HH:mm:ss"), "YYYY/MM/DD HH:mm:ss").subtract(daySpan, "days").startOf("day");
    let endDate = moment(data.endDate.format("YYYY/MM/DD HH:mm:ss"), "YYYY/MM/DD HH:mm:ss").subtract(daySpan, "days").endOf("day");

    if (data.minDate === false || (startDate >= data.minDate.startOf("day") && endDate >= data.minDate.startOf("day"))) {
      this.init({
        startDate: startDate, endDate: endDate
      })
    }
  }


  get data() {
    // let data = $("#" + this.options.key).data('daterangepicker');
    return { startDate: this.startDate, endDate: this.startDate };
  }

  private translate(options) {
    if (options.translate === false) return;

    //daysOfWeek
    let daysOfWeek = [];
    daysOfWeek.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.DAY_OF_WEEK.SU"));
    daysOfWeek.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.DAY_OF_WEEK.MO"));
    daysOfWeek.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.DAY_OF_WEEK.TU"));
    daysOfWeek.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.DAY_OF_WEEK.WE"));
    daysOfWeek.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.DAY_OF_WEEK.TH"));
    daysOfWeek.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.DAY_OF_WEEK.FR"));
    daysOfWeek.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.DAY_OF_WEEK.SA"));
    this.optionsDefault.locale.daysOfWeek = daysOfWeek;

    // monthNames
    let monthNames = [];
    monthNames.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.MONTH_NAME.JANUARY"));
    monthNames.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.MONTH_NAME.FEBRUARY"));
    monthNames.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.MONTH_NAME.MARCH"));
    monthNames.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.MONTH_NAME.APRIL"));
    monthNames.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.MONTH_NAME.MAY"));
    monthNames.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.MONTH_NAME.JUNE"));
    monthNames.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.MONTH_NAME.JULY"));
    monthNames.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.MONTH_NAME.AUGUST"));
    monthNames.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.MONTH_NAME.SEPTEMBER"));
    monthNames.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.MONTH_NAME.OCTOBER"));
    monthNames.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.MONTH_NAME.NOVEMBER"));
    monthNames.push(this.translateService.instant("LIBARY.DATE_RANGE_PICKER.MONTH_NAME.DECEMBER"));
    this.optionsDefault.locale.monthNames = monthNames;

    // common
    this.optionsDefault.locale['customRangeLabel'] = this.translateService.instant("LIBARY.DATE_RANGE_PICKER.CUSTOM_RANGE_LABLE");
    this.optionsDefault.locale['applyLabel'] = this.translateService.instant("LIBARY.DATE_RANGE_PICKER.APPLY_LABLE");
    this.optionsDefault.locale['cancelLabel'] = this.translateService.instant("LIBARY.DATE_RANGE_PICKER.CANCEL_LABLE");
    this.optionsDefault.locale['fromLabel'] = this.translateService.instant("LIBARY.DATE_RANGE_PICKER.FROM_LABLE");
    this.optionsDefault.locale['toLabel'] = this.translateService.instant("LIBARY.DATE_RANGE_PICKER.TO_LABLE");

  }

  private now() {
    let now = moment(moment().utc().format("YYYY/MM/DD HH:mm:ss"), "YYYY/MM/DD HH:mm:ss");
    now.add(this.currentUserService.timeZone, "minutes");
    return now;
  }
  private addRange(key, now, ranges) {
    switch (key) {
      case "TO_DAY":
        ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.TO_DAY")] = [this.now().startOf('day'), this.now().endOf('day')];
        break;
      case "ADD_1_WEEK":
        ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.ADD_2_WEEK")] = [this.now().startOf('day'), this.now().add(7, 'days').endOf('day')];
        break;
      case "ADD_2_WEEK":
        ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.ADD_2_WEEK")] = [this.now().startOf('day'), this.now().add(14, 'days').endOf('day')];
        break;
      case "ADD_1_MONTH":
        ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.ADD_1_MONTH")] = [this.now().startOf('day'), this.now().add(1, 'months').endOf('day')];
        break;
      case "ADD_3_MONTH":
        ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.ADD_3_MONTH")] = [this.now().startOf('day'), this.now().add(3, 'months').endOf('day')];
        break;
      case "ADD_6_MONTH":
        ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.ADD_6_MONTH")] = [this.now().startOf('day'), this.now().add(6, 'months').endOf('day')];
        break;
      case "ADD_1_YEAR":
        ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.ADD_1_YEAR")] = [this.now().startOf('day'), this.now().add(1, 'years').endOf('day')];
        break;
      case "ADD_2_YEAR":
        ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.ADD_2_YEAR")] = [this.now().startOf('day'), this.now().add(2, 'years').endOf('day')];
        break;
      case "YESTERDAY":
        ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.YESTERDAY")]
          = [this.now().subtract(1, 'days').startOf('day'), this.now().subtract(1, 'days').endOf('day')];
        break;
      case "LAST_WEEK":
        ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.LAST_WEEK")]
          = [this.now().subtract(1, 'week').startOf('week').startOf('day'), this.now().subtract(1, 'week').endOf('week').endOf('day')];
        break;
      case "LAST_7_DAYS":
        ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.THIS_WEEK")]
          = [this.now().subtract(7, 'days').startOf('day'), this.now().endOf('day')];
        break;
      case "THIS_WEEK":
        ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.THIS_WEEK")]
          = [this.now().startOf('week').startOf('day'), this.now().endOf('day')];
        break;
      case "THIS_MONTH":
        ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.THIS_MONTH")]
          = [this.now().startOf('month').startOf('day'), this.now().endOf('day')];
        break;
      case "LAST_30_DAYS":
        ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.LAST_30_DAYS")]
          = [this.now().subtract(29, 'days').startOf('day'), this.now().endOf('day')];
        break;
      case "LAST_MONTH":
        ranges[this.translateService.instant("LIBARY.DATE_RANGE_PICKER.LAST_MONTH")]
          = [this.now().subtract(1, 'months').startOf('month').startOf('day'), this.now().subtract(1, 'months').endOf('month').endOf('day')];
        break;
    }
    return ranges;
  }
}
