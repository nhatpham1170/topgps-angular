import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ValidationService } from '@core/common/_service/validation.service';
import { MessageError } from '@core/_base/layout';
import { TranslateService } from '@ngx-translate/core';
import { retry } from 'rxjs/operators';
import { BrowserBackend } from '@sentry/browser/dist/backend';

@Component({
  selector: 'kt-message-error',
  templateUrl: './message-error.component.html',
  styleUrls: ['./message-error.component.scss']
})
export class MessageErrorComponent implements OnInit {
  @Input() errorFormGroup: FormGroup;
  @Input() errorFormControlName: string;
  @Input() errorMessages?: MessageError[];

  public message: string = "";
  constructor(
    public validate: ValidationService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
  }

  get isError(): boolean {
    let _this = this;
    let controlName = this.errorFormControlName;
    let control = this.errorFormGroup[this.errorFormControlName] as FormControl;
    let messageStrCheck = "";
    if (control) {
      if ((control.touched || control.dirty) && control.invalid) {
        if (this.errorMessages) {
          let field = this.errorMessages.find(x => x.identifier == controlName);

          if (field != undefined) {
            let rule = field.rules.find(x => {
              if (control.hasError(x.type) != undefined) {
                _this.message = x.prompt;
                messageStrCheck = x.prompt;
                return true;
              }
            });

            if (rule != undefined) return true;
          }
        }
        if (messageStrCheck.length == 0) {
          let keys = Object.keys(control.errors);
          let data = control.errors[keys[0]];
          this.message = this.messageDefault(keys[0], data);

        }
        return true;

      }
    }
    return false;
  }
  private messageDefault(type, data) {

    let message = "";
    switch (type) {
      case "required":
        message = this.translate.instant('COMMON.VALIDATION.REQUIRED_FIELD_NAME');
        break;
      case "select":
        message = this.translate.instant('COMMON.VALIDATION.PLEASE_CHOOSE_TYPE');
        break;
      case "number":
        message = this.translate.instant('COMMON.VALIDATION.PLEASE_ENTER_NUMBER');
        break;
      case "formula":
        message = this.translate.instant('COMMON.VALIDATION.PLEASE_ENTER_CORRECT_FORM');
        break;
      case "calibration":
        message = this.translate.instant('COMMON.VALIDATION.PLEASE_ENTER_CORRECT_FORM');
        break;
      case "minLength":
        message = this.translate.instant('COMMON.VALIDATION.PLEASE_ENTER_MIN_LENGTH', data);
        break;
      case "maxLength":
        message = this.translate.instant('COMMON.VALIDATION.PLEASE_ENTER_MAX_LENGTH', data);
        break;
      case "rangeLength":
        message = this.translate.instant('COMMON.VALIDATION.PLEASE_ENTER_RANGE_LENGTH', data);
        break;
      case "min":
        message = this.translate.instant('COMMON.VALIDATION.PLEASE_ENTER_MIN', data);
        break;
      case "max":
        message = this.translate.instant('COMMON.VALIDATION.PLEASE_ENTER_MAX', data);
        break;
      case "range":
        message = this.translate.instant('COMMON.VALIDATION.PLEASE_ENTER_RANGE', data);
        break;
      case "phone":
        message = this.translate.instant('COMMON.VALIDATION.PLEASE_ENTER_PHONE');
        break;
      case "email":
        message = this.translate.instant('COMMON.VALIDATION.PLEASE_ENTER_EMAIL', data);
        break;
      default:
        message = this.translate.instant('COMMON.VALIDATION.INVALID');
        break;
    }
    return message;
  }

}