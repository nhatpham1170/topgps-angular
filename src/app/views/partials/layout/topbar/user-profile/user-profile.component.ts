import { Component, OnInit, ChangeDetectorRef, EventEmitter, Input, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { User } from '@core/manage';
import { ToastService, MenuAsideService } from '@core/_base/layout';
import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { CurrentUserService } from '@core/auth';
import { UserManageService } from '@app/core/manage/_service/user-manage.service';
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../../../../core/reducers';
import { currentUser, Logout } from '../../../../../core/auth';
import { Observable, Subscription, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { PageService } from '@core/_base/layout';

import * as $ from 'jquery';
import { Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { AuthService } from '@core/auth';
declare var $: any;

const TITLE_FORM_ADD: string = "COMMON.ACTIONS.ADD";
const TITLE_FORM_DELETE: string = "COMMON.ACTIONS.DELETE";

@Component({
  selector: 'kt-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],

})
export class UserProfileComponent implements OnInit,OnDestroy {

  @Input() user: User;
  @Input() avatar: boolean = true;
  @Input() greeting: boolean = true;
  @Input() badge: boolean;
  @Input() icon: boolean;
  user$: Observable<User>;

  saveFormUsers: FormGroup;
  formChangePassWord: FormGroup;
  public dataDefault: any = [{}]; //
  public idParentUserEdit: any;
  public userIdSelected: number;
  public closeResult: string;
  public isEdit: boolean = true;
  public idUserEdit: any; // id user edit

  public listTimeZone: any = [];
  public languageList: any = [];
  public weekFirstDay: any = [];
  public decimalSeprerator: any = [];
  public unitVolume: any = [];
  public unitWeight: any = [];
  public unitTemperature: any = [];
  public unitDistance: any = [];
  public dateFormat: any = [];
  public timeFormat: any = [];
  public checkPassword: boolean = false;
  public mnenuProfile: any = [];
  public listPages: any = [];
  private unsubscribe:Subscription;
  public listPageMain:any = [];
  public checkPasswordAgain:boolean = false;
  public fieldTextTypePassOld:boolean = false;
  public fieldTextTypePassNew:boolean = false;

  public fieldTextTypePassConfirm:boolean = false;
  constructor(
    private page: PageService,
    private formBuilder: FormBuilder,
    private CurrentUserService: CurrentUserService,
    private modalService: NgbModal,
    private manageUser: UserManageService,
    private cdr: ChangeDetectorRef,
    private store: Store<AppState>,
    private toast: ToastService,
    private translate: TranslateService,
    private router: Router,
    private permissionService: NgxPermissionsService,
    private authService:AuthService,
    private menuAsideService:MenuAsideService

  ) {
    this.listPages = this.page.getListPageProfile();
  }

  ngOnInit() {
    this.listPageMain = this.menuAsideService.getPageMain();
    // this.buildForm();
    this.user$ = this.store.pipe(select(currentUser));
    this.unsubscribe =  this.authService.profileLoaded.pipe(
      tap((user:any)=>{
        this.updateProfile(user);
      })
    ).subscribe();
  }
  ngOnDestroy(){
    if(this.unsubscribe)this.unsubscribe.unsubscribe();
  }
  updateProfile(user){
    this.user$ = new Observable((observer)=>{
      observer.next(user);
    });
  }
  loadParamsetUser() {
    this.listTimeZone = this.CurrentUserService.listTimeZone;
    this.languageList = this.CurrentUserService.listLanguage;
    this.weekFirstDay = this.CurrentUserService.listWeekFirst;
    this.decimalSeprerator = this.CurrentUserService.listDecimal;
    this.unitVolume = this.CurrentUserService.listVolume;
    this.unitWeight = this.CurrentUserService.listWeight;
    this.unitDistance = this.CurrentUserService.listDistance;
    this.unitTemperature = this.CurrentUserService.listTemperature;
    this.timeFormat = this.CurrentUserService.listTimeFormat;
    this.dateFormat = this.CurrentUserService.listDateFormat;
   
  }

  private buildForm(): void {
    
    let pageMain = this.CurrentUserService.currentUser['pageMain'] || this.menuAsideService.getPageMainDefault().page;

    this.saveFormUsers = this.formBuilder.group({
      username: ['', Validators.required],
      name: ['', Validators.required],
      passCurrent: [''],
      passNew: [''],
      passAgain: [''],
      phone: [''],
      email: ['', [Validators.required, Validators.email]],
      timezone: [''],
      language: [''],
      description: [''],
      address: [''],
      distance: [''],
      volume: [''],
      weight: [''],
      date: [''],
      time: [''],
      temperature: [''],
      fisrtday: [''],
      seprerator: [''],
      pageMain:[pageMain]
    });

    this.formChangePassWord = this.formBuilder.group({
      passOld:['',Validators.required],
      passNew: ['',Validators.required],
      passAgain: ['',Validators.required],
    });
    setTimeout(()=>{
      $("#pageMain").selectpicker();
      $("#pageMain").val(pageMain).selectpicker('refresh');
    });
  }

  get f() {
    if (this.saveFormUsers) return this.saveFormUsers.controls;
  }

  showPass(key,value)
  {
    switch (key) {
      case 'old': 
        this.fieldTextTypePassOld = !value;
        break;
     case 'new': 
        this.fieldTextTypePassNew = !value;
        break;  
        case 'confirm': 
        this.fieldTextTypePassConfirm = !value;
        break;    
        
      default:
        break;
    }
    console.log(event);
  }

  open(content, id) {
    if(this.saveFormUsers){
      this.saveFormUsers.reset();
    }
    
    this.buildForm();
    this.idUserEdit = id;
    this.loadParamsetUser();
    setTimeout(function () {
      $('.kt_selectpicker').selectpicker();
    });
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-800', size: 'lg', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  resetInput(id){
    if(id){
      $(id).val('');
      $(id).focus();
    }
  }

  onChangePassWord(form: any){
    if (form.invalid) {
      return;
    }
    let pass_old = form.value.passOld;
    let pass_new = form.value.passNew;
    let pass_again = form.value.passAgain;
  
    if (pass_new != '' && pass_new != null && pass_new != undefined) {
      this.checkPassword = false;
      this.checkPasswordAgain = false;
      if (pass_new.length < 6) {
        this.checkPassword = true;
        this.resetInput('#passNew');
        return;
      }
      if (pass_new != pass_again) {
        this.resetInput('#passAgain');
        this.checkPasswordAgain = true;
        return;
      }
      let params = {
        passwordOld : pass_old,
        passwordNew: pass_new
      };
      this.manageUser.changePassWord(params,this.idUserEdit, { notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          // location.reload();
          this.modalService.dismissAll();
        }
      },(error=>{
        this.resetInput('#passOld');
      }));
    }
  }

  onSubmit(form: any) {
    if (this.saveFormUsers.invalid) {
      return;
    }
    let pass_current = form.value.passCurrent;
    let pass_new = form.value.passNew;
    let pass_again = form.value.passAgain;

    let params = {
      "name": form.value.name,
      "surname": "",
      "email": form.value.email,
      "phone": form.value.phone,
      "description": form.value.description,
      "timezone": form.value.timezone,
      "language": form.value.language,
      "unitDistance": form.value.distance,
      "unitVolume": form.value.volume,
      "unitTemperature": form.value.temperature,
      "unitWeight": form.value.weight,
      "dateFormat": form.value.date,
      "timeFormat": form.value.time,
      "weekFirstDay": form.value.fisrtday,
      "decimalSeprerator": form.value.seprerator,
      "pageMain": form.value.pageMain
    } as User;

    // if (pass_new != '' && pass_new != null && pass_new != undefined) {
    //   this.checkPassword = false;
    //   if (pass_new.length < 6) {
    //     this.checkPassword = true;
    //     return;
    //   }
    //   if (pass_new != pass_again) {
    //     this.toast.show(
    //       {
    //         message: this.translate.instant('MANAGE.USER.MESSAGE.PASSWORD_INCORRECT'),
    //         type: 'error',
    //       })
    //     return;
    //   }
    //   params.password = pass_new;
    // }

    this.manageUser.updateProfile(params, { notifyGlobal: true }).subscribe((result: any) => {
      if (result.status == 200) {
        location.reload();
        this.modalService.dismissAll();
      }
    });
  }
  onClickPoint() {
    if (this.permissionService.getPermission('ROLE_point.manage') != undefined)
      this.router.navigateByUrl("/points/manage");
  }

  logout() {
    this.store.dispatch(new Logout());
    if (localStorage.getItem("favorite-report") != null) {
      localStorage.removeItem('favorite-report');
    }
    if (localStorage.getItem("permissions") != null) {
      localStorage.removeItem('permissions');
    }
    if (localStorage.getItem("eventType") != null) {
      localStorage.removeItem('eventType');
    }
  }

  // openTabExtension(refresh?: Boolean) {
  //   if(this.listMapKeys.length == 0 || refresh == true) {
  //     this.getListMapKey();
  //   }
  // }
}
