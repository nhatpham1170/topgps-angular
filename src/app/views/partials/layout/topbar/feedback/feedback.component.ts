import { Component, OnInit, EventEmitter, Input, ViewChild, ElementRef, Output, ChangeDetectorRef, Renderer2 } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { tap, count, takeUntil, finalize, timeout } from 'rxjs/operators';
import { Subject, from } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Lightbox } from 'ngx-lightbox';
import { DomSanitizer } from '@angular/platform-browser';
import domtoimage from 'dom-to-image';
import { FeedbackService, FeedbackModel } from '@core/common';
import { UploadFileComponent, ImageUploadMode, UploadFileStatus } from '@app/views/partials/content/widgets/upload-file/upload-file.component';
import { StatusCode } from '@core/_base/crud';
import { ValidatorCustomService } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
// import { StatusCode } from '@core/_base/crud';


declare var $: any;


@Component({
  selector: 'kt-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {

  @Input() eventOpen: EventEmitter<any>;
  @Output() result: EventEmitter<{ status: string, message: string, data: any }>;
  public url: any;
  public shotscreenImg: any;
  public closeResult: string;
  private unsubscribe: Subject<any>;
  private userId: number;
  public form: FormGroup;
  private modalReference: NgbModalRef;
  public isSend: boolean = false;
  public img: any;
  private canvasCaupture: any;
  // private isShootscreen:boolean = false;
  private shotscreenProcess: string = '';
  private imgsUpload: Array<ImageUploadMode> = [];
  @ViewChild('content', { static: true }) content: ElementRef;
  constructor(
    private fb: FormBuilder,
    private feedbackService: FeedbackService,
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef,
    private renderer2: Renderer2,
    private el: ElementRef,
    private sanitizer: DomSanitizer,
    private lightBox: Lightbox,
    private validatorCT:ValidatorCustomService,
    private currentUserService:CurrentUserService) {
    this.result = new EventEmitter();
    this.unsubscribe = new Subject();
  }
  ngOnInit() {
    if (this.eventOpen != undefined) {
      this.eventOpen.pipe(
        tap(() => {
          this.open(this.content);
        })
      ).subscribe();
    }
    this.isSend = false;
  }
  createForm() {
    this.form = this.fb.group({
      name: [this.currentUserService.currentUser.fullname || ""],
      phone: [this.currentUserService.currentUser.phone || ""],
      email: [this.currentUserService.currentUser.email || "",this.validatorCT.email],
      content: ["", Validators.required],
      shotscreen: [""],
      isShotscreen: [false],
      img: [""],
    });
   
    this.url = undefined;
    this.shotscreenImg = undefined;
    setTimeout(()=>{
      $('#fbContent').focus();
    });

  }

  open(content) {
    this.createForm();
    this.modalReference = this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', backdrop: 'static' });
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  onSubmit() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.isSend = true;
      let model = new FeedbackModel();
      model.name = this.form.get('name').value;
      model.phone = this.form.get('phone').value;
      model.email = this.form.get('email').value;
      model.content = this.form.get('content').value;
      model.images = [];
      if (this.imgsUpload.length > 0) {
        model.images = this.imgsUpload.filter(x => x.status === UploadFileStatus.ADD).map(x => x.file);
      }
      if (this.form.get('isShotscreen').value === true && this.shotscreenImg != undefined) {
        model.images.splice(0, 0, this.shotscreenImg);
      }
      this.feedbackService.create(model,{notifyGlobal:true}).pipe(
        tap(result=>{
          if(result.status == StatusCode.CREATED){
            this.modalReference.close();
          }
        }),
        finalize(()=>{
          this.isSend = false;
          this.cdr.markForCheck();
          this.cdr.detectChanges();
        })
      ).subscribe();
      setTimeout(()=>{
        this.isSend = false;
        this.cdr.detectChanges();
      },10000);
      
    }
  }

  onChangeShotscreen(value) {
    let _this = this;
    let containerCapture: any = document.getElementById('ktBase');
    // shotscreen 
    if (!this.form.get('isShotscreen').value) {

      this.shotscreenProcess = 'processing';
      _this.cdr.detectChanges();
      if (this.url != undefined) {
        setTimeout(() => {
          _this.shotscreenProcess = 'complete';
          _this.cdr.detectChanges();
        })

        return;
      }
      // export use domtoimage
      setTimeout(() => {
        domtoimage.toPng(containerCapture).then(function (dataUrl) {
          _this.url = dataUrl;
          _this.shotscreenProcess = 'complete';
          fetch(_this.url).then(res => res.blob().then(bold => {
            _this.shotscreenImg = bold;
          }));
          _this.cdr.detectChanges();
        }).catch(
          (error => {
            _this.shotscreenProcess = 'error';
            _this.url = undefined;
            _this.shotscreenProcess = undefined;
          })
        );
      })
    }
    else {
      // remove show image 
      this.shotscreenProcess = 'hide';
      // this.url = undefined;
      // this.canvasCaupture = undefined;
    }
    this.cdr.detectChanges();
  }
  checkBrowser() {
    // const agent = window.navigator.userAgent.toLowerCase();
    // return !(agent.indexOf('safari')>-1)
    return true;
  }
  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url) || "";
  }
  zoomIn(url) {
    if (url && url.length > 0) {
      const album: any = {
        src: this.sanitizer.bypassSecurityTrustUrl(url),
        caption: "",
        thumb: url
      };
      this.lightBox.open([album], 0);
    }
  }
  // upload file change 
  imgChange(value) {
    this.imgsUpload = value;
  }

  // popup
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}

