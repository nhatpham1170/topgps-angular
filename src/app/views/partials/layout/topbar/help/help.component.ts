import { LayoutConfigService } from '@core/_base/layout';
import { LayoutRefService } from './../../../../../core/_base/layout/services/layout-ref.service';
// Angular
import { ChangeDetectorRef, Component, ElementRef, Input, OnInit, EventEmitter } from '@angular/core';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { HelpService } from '@core/manage'
import { finalize, takeUntil, tap, debounceTime,delay} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SystemChangeLogService } from '@core/admin';
import { TranslateService } from '@ngx-translate/core';
import { DomSanitizer } from '@angular/platform-browser';
import html2canvas from 'html2canvas';

@Component({
	selector: 'kt-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']

})


export class HelpComponent implements OnInit {
	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */
    public closeResult: string;
    private unsubscribe: Subject<any>;
    public loadMore:boolean = false;
    private urlApi = 'http://docs.vnetgps.com/wp-json/wp/v2/posts';
    private filter;
    posts: any;
    public eventOpenFeedback:EventEmitter<any>;
    public loginPage;
    public linkDocument:string = "";
	constructor(
        private changeLog: SystemChangeLogService,
        private modalService: NgbModal,
        private helpService : HelpService,
        private translate: TranslateService,
        private sanitized: DomSanitizer,
        private layoutConfigService:LayoutConfigService
        )
     {
      this.eventOpenFeedback = new EventEmitter();
     }

    ngOnInit(){  
      this.loginPage = this.layoutConfigService.getLoginPage();
      if(this.loginPage.config  )
      {
        this.linkDocument = this.loginPage.config.linkDocument || "";
      }
    }
    onFeedback(){
      // html2canvas(document.body).then(function(canvas){
      //   document.body.appendChild(canvas);
      // })
      this.eventOpenFeedback.emit();
    }
    resultFeedback(result){
      
    }
    buildFilter()
    {
        this.filter = {
          pageNo:1,
          pageSize:10,
          type:'web',
          orderType:'DESC',
          orderBy:"releasedAt"

      };
    }
    systemChangeLog(content)
    {
        this.buildFilter();
        this.getData();
        this.open(content);
    }
    getData(scroll:boolean = false){
      let delayNumber = 100;
      if(scroll) delayNumber = 300;
      
      let langDefault = this.translate.getDefaultLang();
      this.changeLog.list(this.filter,{localStore:true,sessionStore:true}).pipe(
        delay(delayNumber)

      )
      .subscribe(data => {
        data.result.content.map(data=>{
          let content = '';
          let title = '';
          if(langDefault == 'en')
          {
            content = data.description;
            title   =  'Version '+data.version;
          }else{
            content = data.descriptionVi;
            title   =  'Phiên bản '+data.version;
          }
          // data.content = content;
          // data.title = title;
          data.content = content;
          data.title = 'v'+data.version;
          return data;
        });
        if(scroll) 
        {
          this.posts = this.posts.concat(data.result.content);
          if(data.result.content.length == 0)
          {
            this.filter.pageNo = this.filter.pageNo-1;
          }
          this.loadMore = false;
    
        }
        if(!scroll)this.posts = data.result.content;
        
        });
    }

    onScrollDown() {
      this.loadMore = true;
      this.filter.pageNo = this.filter.pageNo+1;
      this.getData(true);

  
    }

    renderUrl(params){
      let stringUrl = '';
      let i = 0;
      Object.keys(params).forEach(x => {
        i++;
        if( i == 1)
        {
          stringUrl += '?'+x+'='+params[x];
        }else{
          stringUrl += '&'+x+'='+params[x];
        }
       
      });
      return this.urlApi+stringUrl;
    }
    
    open(content) {
        this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder',size:'lg'}).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
      }
    
      private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
          return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
          return 'by clicking on a backdrop';
        } else {
          return  `with: ${reason}`;
        }
      }


}
