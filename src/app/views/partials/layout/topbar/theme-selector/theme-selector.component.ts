// Angular
import { Component, HostBinding, OnInit, Input } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
// RxJS
import { filter } from 'rxjs/operators';
// Translate
import { TranslationService, LayoutConfigService, LayoutConfigModel } from '../../../../../core/_base/layout';

interface LanguageFlag {
  lang: string;
  name: string;
  flag: string;
  active?: boolean;
}
interface Theme {
  name: string;
  color: string;
  translate:string;
  active?: boolean;
}

@Component({
  selector: 'kt-theme-selector',
  templateUrl: './theme-selector.component.html',
  styleUrls: ['./theme-selector.component.scss']
})
export class ThemeSelectorComponent implements OnInit {
  // Public properties
  @HostBinding('class') classes = '';
  @Input() iconType: '' | 'brand';
  model: LayoutConfigModel;
  theme: Theme;
  themes: Theme[] = [
    {
      name: 'light',
      color: "#fff",
      translate:"THEME.LIGHT",
    },
    {
      name: 'dark',
      color: "#1e1e2d",
      translate:"THEME.DARK",
    }
  ]; 

	/**
	 * Component constructor
	 *
	 * @param translationService: TranslationService
	 * @param router: Router
	 */
  constructor(private translationService: TranslationService, private router: Router,
    private layoutConfigService:LayoutConfigService) {
  }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
  ngOnInit() {
    // this.theme = this.themes[0];
    this.model = this.layoutConfigService.getConfig();
    let skin =  this.model.header.self.skin  || 'ligth';
    // this.theme = this.themes.find(x=>x.name == skin);
    this.themes.forEach((t=>{
      if(t.name == skin){
        t.active = true;
        this.theme = t;
        this.model.header.self.skin =  this.theme.name;
        this.model.brand.self.skin =  this.theme.name;
        this.model.aside.self.skin =  this.theme.name;      
      } 
      else t.active = false;
    }));
  }

  setTheme(theme:Theme){    
    this.themes.forEach((t=>{
      if(t.name == theme.name){
        t.active = true;
        this.theme = t;
        this.model.header.self.skin = theme.name;
        this.model.brand.self.skin = theme.name;
        this.model.aside.self.skin = theme.name;
        // this.model.header.self.skin = theme.name;
        this.layoutConfigService.setConfig(this.model, true);
        location.reload();
      } 
      else t.active = false;
    }));
  }
}
