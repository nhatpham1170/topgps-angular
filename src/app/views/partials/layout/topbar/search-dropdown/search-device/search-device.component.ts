// Angular
import { ChangeDetectorRef, Component, ElementRef, Input, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { DataTable } from '@app/core/_base/layout/models/datatable.model';
import { UserManageService } from '@app/core/manage/_service/user-manage.service';
import { Subject } from 'rxjs';
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Device, DeviceService, User } from '@core/manage';
import { ToastService } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth'
import { DeviceConfigService } from '@core/common/_services/device-config.service';
import { DeviceType, SimType, Deparment, Transport, DeviceTypeService } from '@core/admin';


declare var $: any;

@Component({
  selector: 'kt-search-device',
  templateUrl: './search-device.component.html',
})
export class SearchDeviceComponent implements OnInit {
  // Public properties

  searchFormDevice: FormGroup;
  data: any[];
  result: any[];
  loading: boolean;
  public closeResult: string;
  public columns: any = [];
  public filter: any;
  public paginationSelect: any = [];
  public dataTable: DataTable = new DataTable();
  public totalRecod: number = 0;
  private unsubscribe: Subject<any>;
  public isShowSearchAdvanced: boolean;
  public dataDefault: any = [{}]; // 
  public userEdit: any;
  public idLogin: any;
  public frmDeviceEditBase: FormGroup;
  public deviceTypes: DeviceType[];
  public simTypes: SimType[];
  public iconTypes: any[];
  public showLoaddingSearch: boolean = false;
  public sendCommandEmitter: EventEmitter<Device[]>;
  // public sendCommand:SendCommandComponent

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

  constructor(
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private manageUser: UserManageService,
    private deviceService: DeviceService,
    private toast: ToastService,
    private CurrentUserService: CurrentUserService,
    private deviceConfig: DeviceConfigService,


  ) {
    this.unsubscribe = new Subject();
    this.sendCommandEmitter = new EventEmitter();
  }

	/**
	 * On init
	 */
  ngOnInit(): void {

    this.updateDataTable();
    this.buildForm(); // build value form
    this.setColumns();
    this.buildPagination(); // build config pagination
    this.setPaginationSelect();
    this.setDataTable();
    this.idLogin = this.CurrentUserService.Id;
    // simulate result from API
    // type 0|1 as separator or item

  }

  setDataTable() {
    this.dataTable.init({
      data: [],
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
      }
    });
  }



  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;

        //this.filter = option;
        this.getData();
      }),

    ).subscribe();
  }

  private getData() {
    this.showLoaddingSearch = true;

    this.deviceService.searchGlobal({ params: this.filter }).pipe(
      tap((data: any) => {
        this.dataTable.update({
          data: data.result.content,
          totalRecod: data.result.totalRecord
        });
        this.showLoaddingSearch = false;

      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();

      })
    ).subscribe();
  }

  searchDevice(form: any) {

    if (form.value.plate == null || form.value.plate == undefined) {
      form.value.plate = '';
    }

    if (form.value.device == null || form.value.device == undefined) {
      form.value.device = '';
    }

    if (form.value.imei == null || form.value.imei == undefined) {
      form.value.imei = '';
    }

    if (form.value.simno == null || form.value.simno == undefined) {
      form.value.simno = '';
    }

    if (form.value.subAccount == '' || form.value.subAccount == null || form.value.subAccount == undefined) {
      form.value.subAcount = false;
    }
    this.filter.numberPlate = form.value.plate;
    this.filter.imei = form.value.imei;
    this.filter.name = form.value.device;
    this.filter.simno = form.value.simno;
    this.filter.subAccount = form.value.subAccount;
    this.getData();
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();

  }

  editDevice(item, content) {
    this.open(content);
    this.dataDefault = [item];

    setTimeout(function () {

      $('.kt_selectpicker').selectpicker('refresh');

    }, 110);
  }

  routerLinkUser(parentId, path) {
    let removeString = parentId + ',';
    path = path.replace(removeString, "");
    let lastNodeSelected = {
      id: parentId,
      path: path
    };
    localStorage.setItem('kt_tree_' + this.idLogin, JSON.stringify(lastNodeSelected));
  }

  routerLinkDevice(userId) {
    this.editUser(userId);
    // let addString = parentId+',';
    // path = path+addString;
    // console.log(path);
    // let lastNodeSelected = {
    //     id: parentId,
    //     path:path
    // };
    // localStorage.setItem('kt_tree_234', JSON.stringify(lastNodeSelected));
  }

  async editUser(id) {
    await this.manageUser.get(id)
      .pipe(
        tap((result: any) => {
          if (result.status == 200) {
            let data = result.result;
            let addString = data.id + ',';
            let path = data.path + addString;
            let lastNodeSelected = {
              id: data.id,
              path: path
            };
            localStorage.setItem('kt_tree_' + this.idLogin, JSON.stringify(lastNodeSelected));
          }
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .toPromise();
  }

  copied(val) {
    this.toast.copied(val);
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10
    };
  }

  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'id',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        width: 20,
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
      },
      {
        title: 'Tài khoản',
        field: 'userName',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        width: 100,
        class: '',
        translate: 'COMMON.COLUMN.USERNAME',
        autoHide: false,
      },
      {
        title: 'Biển số',
        field: 'plate',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '85px' },
        width: 85,
        class: '',
        translate: 'COMMON.LIST_DEVICE.NUMBER_PLATE',
        autoHide: false,
      },
      {
        title: 'IMEI',
        field: 'imei',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '125px' },
        width: 125,
        class: '',
        translate: 'COMMON.COLUMN.IMEI',
        autoHide: true,
      },
      {
        title: 'Name',
        field: 'name',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '120px' },
        width: 120,
        class: '',
        translate: 'COMMON.COLUMN.NAME',
        autoHide: true,
      },

      {
        title: 'Type',
        field: 'type',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '90px' },
        class: '',
        width: 90,
        translate: 'COMMON.COLUMN.TYPE',
        autoHide: true,
      },
      {
        title: 'Sim',
        field: 'simno',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        width: 110,
        translate: 'COMMON.COLUMN.SIM',
        autoHide: true,
      },
      {
        title: 'Trạng thái',
        field: 'status',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '70px' },
        class: '',
        width: 70,
        translate: 'COMMON.COLUMN.STATUS',
        autoHide: true,
      },

      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        width: 117,
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
      },
    ]
  }

  resetFormSearch() {
    this.filter.pageNo = 1;
    this.searchFormDevice.reset();
  }

  private buildForm(): void {
    this.searchFormDevice = this.formBuilder.group({
      imei: [''],
      device: [''],
      simno: [''],
      plate: [''],
      phone: [''],
      subAccount: [true]
    });
    this.frmDeviceEditBase = this.formBuilder.group({
      imei: [''],
      name: [''],
      icon: [''],
      groupId: [''],
      simno: [''],
      simType: [''],
      typeDevice: [''],
      activeWarranty: [''],
      active: [''],
      description: [''],
      descAdmin: [''],
      isSell: [''],
      sellAt: [''],
      userName: [''],
      warrantyExpiredDate: [''],
      activeWarrantyDate: [''],
      serviceExpire: [''],
      status: [''],
      desc: [''],
      sellDate: [''],
      sellUpdatedDate: [''],
      vehicelType: [''],
      transportType: [''],
      driver: [''],
      coefficient: [''],
      sendToGovernment: [''],
      deparmentManage: [''],
      governmentManage: [''],
      numberPlate: [''],
      frameNumber: [''],
      vinNumber: [''],
      engineNumber: ['']

    });
  }

	/**
	 * Search
	 * @param e: Event
	 */
  showSearch(content) {
    this.open(content);
  }

  toggleSearchAdvanced() {
    this.isShowSearchAdvanced = !this.isShowSearchAdvanced;
  }

  onDirectionLink(action: string, value: any) {
    const userModel = { id: value.userId, path: value.userPath };
    sessionStorage.setItem("search_global", JSON.stringify(userModel));
  }
  sendCommand(item) {
    let data = [item];
    if (data.length > 0) {
      this.sendCommandEmitter.emit(data);
    }
  }
  sendCommandResult(result) {
  }
  open(content) {
    this.deviceConfig.get().then(x => {
      this.simTypes = x.simTypes;
      this.deviceTypes = x.deviceTypes;
      this.iconTypes = x.deviceIcons;

    });
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-900', size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
