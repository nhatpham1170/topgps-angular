// Angular
import { ChangeDetectorRef, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { DataTable } from '@app/core/_base/layout/models/datatable.model';
import { UserManageService } from '@app/core/manage/_service/user-manage.service';
import { Subject } from 'rxjs';
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { CurrentUserService } from '@core/auth'
import { ToastService } from '@core/_base/layout';
import { TranslateService } from '@ngx-translate/core';

declare var $: any;

@Component({
	selector: 'kt-search-user',
	templateUrl: './search-user.component.html',
})
export class SearchUserComponent implements OnInit {
	// Public properties
	saveFormUsers: FormGroup;
	searchFormUsers: FormGroup;
	data: any[];
	result: any[];
	public isEdit: boolean = true;
	loading: boolean;
	public closeResult: string;
	public columns: any = [];
	public filter: any;
	public paginationSelect: any = [];
	public dataTable: DataTable = new DataTable();
	public totalRecod: number = 0;
	private unsubscribe: Subject<any>;
	public dataDefault: any = [{}]; // 
	public listTimeZone: any = [];
	public languageList: any = [];
	public weekFirstDay: any = [];
	public decimalSeprerator: any = [];
	public unitVolume: any = [];
	public unitWeight: any = [];
	public unitTemperature: any = [];
	public unitDistance: any = [];
	public dateFormat: any = [];
	public timeFormat: any = [];
	public listRoles: any = [];
	public listUserType: any = [];
	public idLogin: any;
	public showLoaddingSearch: boolean = false;

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	constructor(
		private cdr: ChangeDetectorRef,
		private modalService: NgbModal,
		private formBuilder: FormBuilder,
		private manageUser: UserManageService,
		private CurrentUserService: CurrentUserService,
		private toast: ToastService,
		private translate: TranslateService,

	) {
		this.unsubscribe = new Subject();
	}

	/**
	 * On init
	 */
	ngOnInit(): void {
		// this.getData();
		this.updateDataTable();
		this.buildForm(); // build value form
		this.setColumns();
		this.buildPagination(); // build config pagination
		this.setPaginationSelect();
		this.setDataTable();
		this.loadParamsetUser();
		// simulate result from API
		// type 0|1 as separator or item
	}

	loadParamsetUser() {
		this.listTimeZone = this.CurrentUserService.listTimeZone;
		this.languageList = this.CurrentUserService.listLanguage;
		this.weekFirstDay = this.CurrentUserService.listWeekFirst;
		this.decimalSeprerator = this.CurrentUserService.listDecimal;
		this.unitVolume = this.CurrentUserService.listVolume;
		this.unitWeight = this.CurrentUserService.listWeight;
		this.unitDistance = this.CurrentUserService.listDistance;
		this.unitTemperature = this.CurrentUserService.listTemperature;
		this.timeFormat = this.CurrentUserService.listTimeFormat;
		this.dateFormat = this.CurrentUserService.listDateFormat;
		this.listUserType = this.CurrentUserService.listUserType;
		this.listRoles = this.CurrentUserService.listRoles;
		this.idLogin = this.CurrentUserService.Id;
	}


	editUser(id, content) {
		this.open(content);
		this.manageUser.get(id)
			.pipe(
				takeUntil(this.unsubscribe),
				finalize(() => {
					this.cdr.markForCheck();
				})
			)
			.subscribe((result: any) => {
				if (result.status == 200) {
					let data = result.result;
					this.dataDefault = [data];
					setTimeout(function () {
						$('.kt_selectpicker').selectpicker();
					}, 110);
				}
			});
	}

	setDataTable() {
		this.dataTable.init({
			data: [],
			totalRecod: this.totalRecod,
			paginationSelect: this.paginationSelect,
			columns: this.columns,
			layout: {
				body: {
					scrollable: false,
					maxHeight: 600,
				},
				selecter: false,
			}
		});
	}

	updateDataTable() {
		this.dataTable.eventUpdate.pipe(
			tap(option => {
				this.filter.pageNo = option.pageNo;
				this.filter.pageSize = option.pageSize;
				//this.filter = option;
				this.getData();
			}),

		).subscribe();
	}

	getData() {
		this.showLoaddingSearch = true;

		this.manageUser.search(this.filter)
			.pipe(
				takeUntil(this.unsubscribe),
				finalize(() => {
					this.cdr.markForCheck();
				})
			)
			.subscribe((result: any) => {
				if (result.status == 200) {
					this.data = result.result.content;
					this.totalRecod = result.result.totalRecord;
					this.dataTable.update({
						data: this.data,
						totalRecod: this.totalRecod
					});
					this.showLoaddingSearch = false;

				}
			})
	}

	searchUser(form: any) {
		if (form.value.username == null || form.value.username == undefined) {
			form.value.username = '';
		}
		if (form.value.email == null || form.value.email == undefined) {
			form.value.email = '';
		}
		if (form.value.phone == null || form.value.phone == undefined) {
			form.value.phone = '';
		}
		if (form.value.subAccount == '' || form.value.subAccount == null || form.value.subAccount == undefined) {
			form.value.subAcount = false;
		}
		this.filter.name = form.value.username;
		this.filter.email = form.value.email;
		this.filter.phone = form.value.phone;
		this.filter.includeSubAccount = form.value.subAccount;
		this.showLoaddingSearch = true;

		this.manageUser.search(this.filter)
			.pipe(
				takeUntil(this.unsubscribe),
				finalize(() => {
					this.cdr.markForCheck();
				})
			)
			.subscribe(
			  (data: any) => {
				if (data.status == 200) {
					this.data = data.result.content;
					this.totalRecod = data.result.totalRecord;
					this.dataTable.update({
						data: this.data,
						totalRecod: this.totalRecod
					});
					this.showLoaddingSearch = false;
				}
			},
			(error:any) => {
				if(error.error)
				{
					let mess = this.translate.instant(error.error.message);
					this.toast.show({message:mess,type:'error'});

				}
				setTimeout(()=>{
					this.showLoaddingSearch = false;
				},1000)
			}
			);
	}

	routerLinkUser(parentId, path) {

		let removeString = parentId + ',';
		path = path.replace(removeString, "");
		let lastNodeSelected = {
			id: parentId,
			path: path
		};
		localStorage.setItem('kt_tree_' + this.idLogin, JSON.stringify(lastNodeSelected));
	}

	routerLinkDevice(parentId, path) {
		let addString = parentId + ',';
		path = path + addString;
		console.log(path);
		let lastNodeSelected = {
			id: parentId,
			path: path
		};
		localStorage.setItem('kt_tree_' + this.idLogin, JSON.stringify(lastNodeSelected));
	}

	onResize($elm) {
		this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
		this.cdr.detectChanges();

	}

	setPaginationSelect() {
		this.paginationSelect = [10, 20, 30, 40, 50];
	}

	buildPagination() {
		this.filter = {
			pageNo: 1,
			pageSize: 10
		};
	}

	setColumns() {
		this.columns = [
			{
				title: '#',
				field: 'no',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { 'width': '20px' },
				width: 20,
				class: '',
				translate: '#',
				autoHide: false,
			},
			{
				title: 'Username',
				field: 'username',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { 'width': '110px', 'margin': '0px' },
				width: 110,
				class: '',
				translate: 'COMMON.COLUMN.USERNAME',
				autoHide: false,
			},
			{
				title: 'Full name',
				field: 'groupName',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { 'width': '130px' },
				width: 110,
				class: '',
				translate: 'MANAGE.USER.GENERAL.FULL_NAME',
				autoHide: true,
			},
			{
				title: 'Phone',
				field: 'groupName',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { 'width': '110px' },
				width: 110,
				class: '',
				translate: 'MANAGE.USER.GENERAL.PHONE',
				autoHide: false,
			},
			{
				title: 'Email',
				field: 'groupName',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { 'width': '130px' },
				width: 130,
				class: '',
				translate: 'MANAGE.USER.GENERAL.EMAIL',
				autoHide: true,
			},
			{
				title: 'Status',
				field: 'groupName',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { 'width': '104px', 'text-align': 'center' },
				width: 104,
				class: '',
				translate: 'COMMON.COLUMN.STATUS',
				autoHide: false,
			},
			{
				title: 'Actions',
				field: 'action',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { 'width': '150px' },
				width: 130,
				class: '',
				translate: 'COMMON.COLUMN.ACTIONS',
				autoHide: false,
			},
		]
	}

	private buildForm(): void {
		this.searchFormUsers = this.formBuilder.group({
			username: [''],
			email: [''],
			phone: [''],
			subAccount: [true]
		});

		this.saveFormUsers = this.formBuilder.group({
			username: ['', [Validators.required, Validators.minLength(6)]],
			name: ['', Validators.required],
			phone: [''],
			email: ['', [Validators.email]],
			role: [''],
			timezone: [''],
			language: [''],
			type: ['', Validators.required],
			description: [''],
			address: [''],
			distance: [''],
			volume: [''],
			weight: [''],
			date: [''],
			time: [''],
			temperature: [''],
			fisrtday: [''],
			seprerator: ['']
		});
	}

	resetFormSearch() {
		this.filter.pageNo = 1;
		this.searchFormUsers.reset();
	}

	/**
	 * Search
	 * @param e: Event
	 */

	copied(val) {
		this.toast.copied(val);
	}

	open(content) {
		this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-900', size: 'lg' }).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});
	}
	onDirectionLink(action:string,value:any){
		sessionStorage.setItem("search_global",JSON.stringify(value));
	}

	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}
}
