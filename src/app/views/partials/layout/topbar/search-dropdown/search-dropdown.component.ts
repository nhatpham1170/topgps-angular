// Angular
import { ChangeDetectorRef, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { DataTable } from '@app/core/_base/layout/models/datatable.model';

@Component({
	selector: 'kt-search-dropdown',
	templateUrl: './search-dropdown.component.html',
})
export class SearchDropdownComponent implements OnInit {
	// Public properties

	// Set icon class name
	@Input() icon: string = 'flaticon2-search-1';

	// Set true to icon as SVG or false as icon class
	@Input() useSVG: boolean;

	@Input() type: 'brand' | 'success' | 'warning' = 'success';

	@ViewChild('searchInput', {static: true}) searchInput: ElementRef;
	searchFormUsers: FormGroup;
	data: any[];
	result: any[];
	loading: boolean;
	public closeResult: string;
	public columns: any = [];
	public filter: any;
	public paginationSelect: any = [];
	public dataTable: DataTable = new DataTable();
	public totalRecod: number = 0;

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	constructor(private modalService: NgbModal) {}

	/**
	 * On init
	 */
	ngOnInit(): void {

		// simulate result from API
		// type 0|1 as separator or item
		this.result = [
			{
				icon: '',
				text: 'Documents',
				type: 0
			}, {
				icon: '<i class="flaticon-interface-3 kt-font-warning">',
				text: 'Annual finance report',
				type: 1
			}, {
				icon: '<i class="flaticon-share kt-font-success"></i>',
				text: 'Company meeting schedule',
				type: 1
			}, {
				icon: '<i class="flaticon-paper-plane kt-font-info"></i>',
				text: 'Project quotations',
				type: 1
			}, {
				icon: '',
				text: 'Customers',
				type: 0
			}, {
				icon: '<img src="assets/media/users/user1.jpg" alt="">',
				text: 'Amanda Anderson',
				type: 1
			}, {
				icon: '<img src="assets/media/users/user2.jpg" alt="">',
				text: 'Kennedy Lloyd',
				type: 1
			}, {
				icon: '<img src="assets/media/users/user3.jpg" alt="">',
				text: 'Megan Weldon',
				type: 1
			}, {
				icon: '<img src="assets/media/users/user4.jpg" alt="">',
				text: 'Marc-André ter Stegen',
				type: 1
			}, {
				icon: '',
				text: 'Files',
				type: 0
			}, {
				icon: '<i class="flaticon-lifebuoy kt-font-warning"></i>',
				text: 'Revenue report',
				type: 1
			}, {
				icon: '<i class="flaticon-coins kt-font-primary"></i>',
				text: 'Anual finance report',
				type: 1
			}, {
				icon: '<i class="flaticon-calendar kt-font-danger"></i>',
				text: 'Tax calculations',
				type: 1
			}
		];
	}


	/**
	 * Search
	 * @param e: Event
	 */
	showSearch(content)
	{
		this.open(content);
	}
	
	open(content) {
		this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-1100',size: 'lg' }).result.then((result) => {
		  this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
		  this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});
	  }
	
	  private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
		  return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
		  return 'by clicking on a backdrop';
		} else {
		  return  `with: ${reason}`;
		}
	  }
}
