// Angular
import { Component, OnInit, Input, ChangeDetectorRef, EventEmitter, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NotificationService, ToastService, UserDateAdvPipe } from '@core/_base/layout';
import { finalize, takeUntil, tap, debounceTime, delay } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { AlertService, AlertPopup, AlertRulesService } from '@core/manage';

import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EventEmitterService } from '@core/common/_service/eventEmitter.service';
import { number } from '@amcharts/amcharts4/core';
// import { AlertService } from '@core/manage/_services/alert.service';
import * as FileSaver from 'file-saver';

@Component({
	selector: 'kt-notification',
	templateUrl: './notification.component.html',
	styleUrls: ['notification.component.scss'],
	providers: [UserDateAdvPipe]
})
export class NotificationComponent implements OnInit {

	// Show pulse on icon
	@Input() pulse: boolean;
	@Input() pulseLight: boolean;
	// Set icon class name
	@Input() icon: string = 'flaticon2-bell-alarm-symbol';
	@Input() iconType: '' | 'success';
	// Set true to icon as SVG or false as icon class
	@Input() useSVG: boolean;
	// Set bg image path
	@Input() bgImage: string;
	// Set skin color, default to light
	@Input() skin: 'light' | 'dark' = 'light';
	@Input() type: 'brand' | 'success' = 'success';

	/**
	 * Component constructor
	 *
	 * @param sanitizer: DomSanitizer
	 */

	// Show dot on top of the icon
	private unsubscribe: Subject<any>;
	public dot: boolean = false;
	public countNew: number = 0;
	public listAlerts: Array<any> = [];
	public loadCount;
	public notFound: boolean = false;
	public isCross: boolean = false;
	public closeResult: string;
	public data: any = [];
	public checkOpen: boolean = false;
	public eventEmitterAlert: EventEmitter<any>;
	private subscribeAlertEvent: Subscription;
	public loadNew: boolean;
	public loadingList: boolean;
	public loadMore: boolean;
	private startTime: string;
	private endTime: string;
	public allowLoadMore: boolean;


	constructor(
		private sanitizer: DomSanitizer,
		private cdr: ChangeDetectorRef,
		private notification: NotificationService,
		private alertService: AlertService,
		private alertRuleService: AlertRulesService,
		private modalService: NgbModal,
		private toastService: ToastService,
		private eventEmitter: EventEmitterService,
		private userDateAdv: UserDateAdvPipe

	) {
		this.loadingList = false;
		this.loadMore = false;
		this.loadNew = false;
		this.allowLoadMore = false;
		this.listAlerts = [];
		this.eventEmitterAlert = new EventEmitter();
		this.startTime = "";
		this.endTime = "";

	}

	ngOnInit() {
		// this.getCountNew();
		this.eventEmitter.alertChanged.subscribe(number=>{
			this.dot = false;
			if (this.countNew != parseInt(number)) {
				this.countNew = parseInt(number);
				// this.eventEmitter.getAlertChanged(this.countNew);
			}
			if (this.countNew > 99) {
				this.countNew = 99;
				this.isCross = true;
			}
			if (this.countNew > 0) this.dot = true;
		});
		// this.loadCount = setInterval(() => {
		// 	this.getCountNew();
		// }, 10000);
		this.subscribeAlertEvent = this.alertService.eventChange.pipe(
			tap(event => {
				this.listenEventAlert(event);
			})
		).subscribe();
	}

	ngOnDestroy() {
		if (this.loadCount) {
			clearInterval(this.loadCount);
		}
		this.subscribeAlertEvent.unsubscribe();
	}
	listenEventAlert(event) {
		switch (event.type) {
			case "seen_all":
				this.getCountNew();
				break;
			case "seen_device":
				this.getCountNew();
				break;
			case "mark_read":
				this.listAlerts = this.listAlerts.map(x => {
					if (x.id == event.data.id) x['isRead'] = event.data.isRead;
					return x;
				});
				this.cdr.detectChanges();
				break;
			case "mark_read_all":
				if (event.data.deviceId > 0) {
					this.listAlerts = this.listAlerts.map(x => {
						if (x.deviceId == event.data.deviceId) x['isRead'] = event.data.isRead;
						return x;
					});
					this.cdr.detectChanges();
				}
				else {
					this.listAlerts = this.listAlerts.map(x => {
						x['isRead'] = event.data.isRead;
						return x;
					});
					this.cdr.detectChanges();
				}
				break;
		}
	}
	showNotification() {		
		if (!this.checkOpen && this.listAlerts.length == 0 || this.countNew > 0) this.getNews();
	}

	openChange(event) {
		this.checkOpen = event;
	}

	getCountNew() {
		this.alertService.countNew({})
			.pipe(				
				finalize(() => {
					this.cdr.markForCheck();
					this.cdr.detectChanges();
				})
			)
			.subscribe((result: any) => {
				if (result.status == 200) {
					this.dot = false;
					if (this.countNew != parseInt(result.result.total)) {
						this.countNew = parseInt(result.result.total);
						this.eventEmitter.getAlertChanged(this.countNew);
					}
					if (this.countNew > 99) {
						this.countNew = 99;
						this.isCross = true;
					}
					if (this.countNew > 0) this.dot = true;
				}
			})
	}
	getAlerts() {
		this.startTime = "";
		this.endTime = "";
		this.loadingList = true;
		this.allowLoadMore = true;
		this.listAlerts = [];		
		this.cdr.detectChanges();
		this.alertService.getListAlert({ pageNo: 1, pageSize: 10 }).pipe(
			delay(300),
			tap(data => {
				this.listAlerts = data.result.content;
				
				if (this.listAlerts.length > 0) {
					this.startTime = this.userDateAdv.transform(this.listAlerts[0].createdAt, "YYYY-MM-DD HH:mm:ss");
					this.endTime = this.userDateAdv.transform(this.listAlerts[this.listAlerts.length - 1].createdAt, "YYYY-MM-DD HH:mm:ss");
					this.alertService.eventChange.emit({ type: "seen_all", data: {} });
				}
				if (data.result.content.length < 10) {
					this.allowLoadMore = false;
				}
				if (this.listAlerts.length > 0) {
					this.notFound = false;
				}
				else this.notFound = true;
			}),
			finalize(() => {
				this.loadingList = false;
				this.cdr.markForCheck();
				this.cdr.detectChanges();
			})
		).subscribe();		
	}
	getMore() {
		if (this.startTime.length == 0) {
			this.getAlerts();
			return;
		}
		this.loadMore = true;
		this.alertService.getListAlert({ pageNo: 1, pageSize: 5, timeTo: this.endTime, timeEqual: 0 }).pipe(
			delay(300),
			tap(data => {
				this.listAlerts = this.listAlerts.concat(data.result.content);

				this.startTime = this.userDateAdv.transform(this.listAlerts[0].createdAt, "YYYY-MM-DD HH:mm:ss");
				this.endTime = this.userDateAdv.transform(this.listAlerts[this.listAlerts.length - 1].createdAt, "YYYY-MM-DD HH:mm:ss");
				if (data.result.content.length < 5) {
					this.allowLoadMore = false;
				}
			}),
			finalize(() => {
				this.loadMore = false;
				this.cdr.markForCheck();
				this.cdr.detectChanges();
			})
		).subscribe();
		this.cdr.detectChanges();
	}

	getNews() {

		if (this.startTime.length == 0) {
			this.getAlerts();
			return;
		}
		this.loadNew = true;
		this.alertService.getListAlert({ pageNo: 1, pageSize: 1000, timeFrom: this.startTime, timeEqual: 0 }).pipe(
			delay(300),
			tap(data => {
				this.listAlerts = data.result.content.concat(this.listAlerts);
				this.startTime = this.userDateAdv.transform(this.listAlerts[0].createdAt, "YYYY-MM-DD HH:mm:ss");
				this.endTime = this.userDateAdv.transform(this.listAlerts[this.listAlerts.length - 1].createdAt, "YYYY-MM-DD HH:mm:ss");
				this.alertService.eventChange.emit({ type: "seen_all", data: {} });
			}),
			finalize(() => {
				this.loadNew = false;
				this.cdr.markForCheck();
				this.cdr.detectChanges();
			})
		).subscribe();
		this.cdr.detectChanges();
	}	

	downloadReport(alert)
	{		
		let url = alert.reportUrl;
		FileSaver.saveAs(url);
		this.markAsRead(alert.id, 0);

	}

	markAllRead() {
		this.alertService.markAllRead({})
			.pipe(
				finalize(this.cdr.markForCheck)
			).subscribe();
	}

	markAsRead(id: number, isRead: number) {
		let isReadUpdate = 1;
		if (isRead == 1) {
			isReadUpdate = 0;
		}
		let params = {
			id: id,
			isRead: isReadUpdate
		};

		this.alertService.markAsRead(params).pipe(
			tap(data => {
				// console.log(id);
				// console.log(isReadUpdate);
				this.listAlerts = this.listAlerts.map(x => {
					if (x.id == data.result.id) x.isRead = data.result.isRead;
					return x;
				});
				this.cdr.detectChanges();
			}),
			finalize(this.cdr.markForCheck)
		).subscribe();
	}

	backGroundStyle(): string {
		if (!this.bgImage) {
			return 'none';
		}

		return 'url(' + this.bgImage + ')';
	}

	open(content, id) {
		this.getInfoAlert(id);
		this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder' }).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});
	}

	getInfoAlert(id) {
		this.alertRuleService.markAsRead({ id: id })
			.pipe(
				tap(result => {
					this.data = [result.result];
					this.cdr.detectChanges();
				}),
				finalize(() => {
					this.cdr.markForCheck();
				})
			)
			.subscribe();
	}

	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}
	public alertPopup: AlertPopup;
	async showAlert(content, alert) {
		await this.alertService.alertDetail(alert.id).pipe(
			tap(data => {
				if (data.status == 200) {
					this.alertPopup = data.result;
				}
				this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal--fit kt-modal-clear', size: 'lg', backdrop: 'static' })
					.result.then((result) => {
						this.closeResult = `Closed with: ${result}`;

					}, (reason) => {
						this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
					});
			},
				error => {
					this.toastService.show({ translate: error.messageCode, type: "warning" });
				}),

			finalize(() => this.cdr.markForCheck())
		).toPromise();
		// update is read
		this.markAsRead(alert.id, 0);
	}
	// load alert history
	onScrollDown() {
		// console.log("Scroll down");
		if (!this.loadMore && this.allowLoadMore) {
			this.getMore();
		}

	}
	// load alert new
	onScrollUp() {
		// console.log("Scroll up");
		if (!this.loadNew) {
			this.getNews();
		}
	}

}
