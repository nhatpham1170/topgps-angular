// Angular
import { Component, OnInit, Input, ChangeDetectorRef, EventEmitter, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NotificationService, ToastService, UserDateAdvPipe } from '@core/_base/layout';
import { finalize, takeUntil, tap, debounceTime, delay } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { AlertService, AlertPopup, AlertRulesService } from '@core/manage';

import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EventEmitterService } from '@core/common/_service/eventEmitter.service';
import { MessageService,message} from '@core/manage';

// import { AlertService } from '@core/manage/_services/alert.service';
declare var $: any;

@Component({
	selector: 'kt-topbar-message',
	templateUrl: './message.component.html',
	styleUrls: ['message.component.scss'],
	providers: [UserDateAdvPipe]
})
export class messageTopBarComponent implements OnInit {

	// Show pulse on icon
	@Input() pulse: boolean;
	@Input() pulseLight: boolean;
	// Set icon class name
	@Input() icon: string = 'flaticon2-bell-alarm-symbol';
	@Input() iconType: '' | 'success';
	// Set true to icon as SVG or false as icon class
	@Input() useSVG: boolean;
	// Set bg image path
	@Input() bgImage: string;
	// Set skin color, default to light
	@Input() skin: 'light' | 'dark' = 'light';
	@Input() type: 'brand' | 'success' = 'success';

	/**
	 * Component constructor
	 *
	 * @param sanitizer: DomSanitizer
	 */

	// Show dot on top of the icon
	private unsubscribe: Subject<any>;
	public dot: boolean = false;
	public countNew: number = 0;
	public listAlerts: Array<any> = [];
	public loadCount;
	public notFound: boolean = false;
	public isCross: boolean = false;
	public closeResult: string;
	public data: any = [];
	public checkOpen: boolean = false;
	public eventEmitterAlert: EventEmitter<any>;
	private subscribeAlertEvent: Subscription;
	public loadNew: boolean;
	public loadingList: boolean;
	public loadMore: boolean;
	private startTime: string;
	public allowLoadMore: boolean;
	public filter:any;
	public listMessage:any = [];
	private idDeleteMessage:number;
	// public changeCount:EventEmitter<any>;
	constructor(
		private cdr: ChangeDetectorRef,
		private alertService: AlertService,
		private modalService: NgbModal,
		private Message: MessageService,
        private changeCount : EventEmitterService

	) {
		this.loadingList = false;
		this.loadMore = false;
		this.loadNew = false;
		this.allowLoadMore = false;
		this.listAlerts = [];
		this.eventEmitterAlert = new EventEmitter();
		this.startTime = "";
		// this.changeCount = new EventEmitter();

	}

	ngOnInit() {
		this.buildPagination();
		this.getCountNew();
		this.loadCount = setInterval(() => {
			this.getCountNew();
		}, 10000);
	}

	ngOnDestroy() {
		if (this.loadCount) {
			clearInterval(this.loadCount);
		}
		if(this.subscribeAlertEvent)
			this.subscribeAlertEvent.unsubscribe();
	}

	buildPagination() {
		this.filter = {
		  pageNo: 1,
		  pageSize: 10,
		  orderBy:'createdAt',
		  orderType:'DESC'
		};
	  }

	getCountNew() {
		this.Message.getCount({})
			.pipe(				
				finalize(() => {
					this.cdr.markForCheck();
					this.cdr.detectChanges();
				})
			)
			.subscribe((result: any) => {
				if (result.status == 200) {
					this.changeCount.getAlertChanged(parseInt(result.result.total.notification));
					this.dot = false;
					if (this.countNew != parseInt(result.result.total.message)) {
						this.countNew = parseInt(result.result.total.message);
					}
					if (this.countNew > 99) {
						this.countNew = 99;
						this.isCross = true;
					}
					if (this.countNew > 0) this.dot = true;
				}
			})
	}

	markAllReadConfirm(modal) {
		this.Message.markAllRead({},{notifyGlobal:true})
			.pipe(
				finalize(this.cdr.markForCheck)
			).subscribe((result: any) => {
				if (result.status == 200) {
					this.listMessage = this.listMessage.map(x => {
						x.status = 1;
						return x;
					});
					modal.dismiss('Cross click');
				}
			});
	}

	backGroundStyle(): string {
		if (!this.bgImage) {
			return 'none';
		}
		return 'url(' + this.bgImage + ')';
	}

	showPopupMessage(content)
	{
		this.buildPagination();
		this.getData();
		this.open(content,'message');
	}

	open(content, type:string = 'delete') {
		// this.getInfoAlert(id);
		let obj;
		if(type == 'delete')
		{obj = { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-delete'};}
		else{obj = { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-900',backdrop:'static'};}
		
		this.modalService.open(content, obj).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});
	}


	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}

	// load alert history
	onScrollDown() {
		this.loadMore = true;
		this.filter.pageNo = this.filter.pageNo+1;
		this.getData(true);
     }
	// load alert new


	asRead(id){
		let params = {
			id:id,
			status:1
		};
		this.Message.markAsRead(params)
		.pipe(				
			finalize(() => {
				this.cdr.markForCheck();
				this.cdr.detectChanges();
			})
		)
		.subscribe((result: any) => {
			if (result.status == 200) {
				
				this.listMessage = this.listMessage.map(x => {
					if (x.id == result.result.id) x.status = 1;
					return x;
				});
			//   this.listMessage = result.result.content;
			}
		})
	}

	
	getData(scroll:boolean = false) {
		let delayNumber = 100;
		if(scroll) delayNumber = 300;
		  this.Message.listReceive(this.filter)
		  .pipe(		
			delay(delayNumber),		
			  finalize(() => {
				  this.cdr.markForCheck();
				  this.cdr.detectChanges();
			  })
		  )
		  .subscribe((result: any) => {
			  if (result.status == 200) {
				this.countNew = 0;
				if(scroll) 
				{
				  this.listMessage = this.listMessage.concat(result.result.content);
				  if(result.result.content.length == 0)
				  {
					this.filter.pageNo = this.filter.pageNo-1;
				  }
				  this.loadMore = false;
			
				}
				if(!scroll)this.listMessage = result.result.content;
			  }
		  })
	  }

	markAllReadPopup(content){
	this.open(content,'delete');
	}

	deleteAll(content){
		this.open(content);
	}

	confirmDeleteMessageAll(modal)
	{
		this.Message.deleteReceiveAll({notifyGlobal:true})
		.pipe(		
		
			finalize(() => {
				this.cdr.markForCheck();
				this.cdr.detectChanges();
			})
		)
		.subscribe((result: any) => {
			
			if (result.status == 200) {
				modal.dismiss('Cross click');
				this.listMessage = [];
			}
		})
	}

	deleteMessage(content,id)
	{
		this.idDeleteMessage = id;
		this.open(content,'delete');
	}

	confirmDeleteMessage(modal){
		let _this = this;
		this.Message.deleteReceive(this.idDeleteMessage,{notifyGlobal:true})
		.pipe(		
		
			finalize(() => {
				this.cdr.markForCheck();
				this.cdr.detectChanges();
			})
		)
		.subscribe((result: any) => {
			
			if (result.status == 200) {
				modal.dismiss('Cross click');

				this.listMessage = this.listMessage.filter(function( obj ) {
					return obj.id !== _this.idDeleteMessage;
				});
			}
		})
		// this.modalService.dismissAll();
	}

}
