import { Component, OnInit } from '@angular/core';
import { environment } from 'environments/environment'
import { ToastService, TranslationService } from '@core/_base/layout';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'kt-version',
  templateUrl: './version.component.html',
  styleUrls: ['./version.component.scss']
})
export class VersionComponent implements OnInit {

  constructor(private toastService: ToastService, private translate: TranslateService,private translation:TranslationService) { }

  ngOnInit() {
    let version;
    // console.log(this.translation.getSelectedLanguage());
    this.translate.use(this.translation.getSelectedLanguage());
    if (localStorage.getItem("version") && localStorage.getItem("version") != environment.version) {
      localStorage.setItem("version", environment.version);
      localStorage.setItem("version_message", this.translate.instant("SYSTEM.UPDATED_VERSION") + ": " + environment.version);
      if (localStorage.getItem("version")) location.reload(true);

    }
    else {
      version = environment.version;
      if (localStorage.getItem("version_message")) {
        let config: any = {
          autoDismiss: true,
          closeButton: true,
          timeOut: 0
        }
        this.toastService.show({ message: localStorage.getItem("version_message"), type: "info", config: config });
        localStorage.removeItem("version_message");
      }
    }


    console.log("version", version);

  }

}
