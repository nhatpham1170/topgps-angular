// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Components
import { BaseComponent } from './base/base.component';
import { ErrorPageComponent } from './content/error-page/error-page.component';
// Auth
import { AuthGuard, ModuleGuard } from '../../../core/auth';

const routes: Routes = [
	{
		path: '',
		component: BaseComponent,		
		canActivate: [ModuleGuard],
		children: [
			{
				path: 'dashboard',
				canActivate: [AuthGuard],			
				loadChildren: () => import('app/views/pages/dashboard/dashboard.module').then(m => m.DashboardModule)
			},
			{
				path: 'admin',
				canActivate: [AuthGuard],
				loadChildren: () => import('app/views/pages/admin/admin.module').then(m => m.AdminModule)
			},
			{
				path: 'manage',		
				canActivate: [AuthGuard],		
				loadChildren: () => import('app/views/pages/manage/manage.module').then(m => m.ManageModule)
			},
			{
				path: 'fuel',		
				canActivate: [AuthGuard],		
				loadChildren: () => import('app/views/pages/fuel/fuel.module').then(m => m.FuelModule)
			},
			{
				path: 'reports',
				canActivate: [AuthGuard],
				loadChildren: () => import('app/views/pages/report/report.module').then(m => m.ReportModule)
			},
			{
				path: 'map',
				canActivate: [AuthGuard],
				loadChildren: () => import('app/views/pages/map/map.module').then(m => m.MapModule)
			},
			{
				path: 'points',
				canActivate: [AuthGuard],
				loadChildren: () => import('app/views/pages/points/points.module').then(m => m.PointsModule)
			},
			{
				path: 'builder',
				loadChildren: () => import('app/views/themes/default/content/builder/builder.module').then(m => m.BuilderModule)
			},
			{
				path: 'utilities',
				loadChildren: () => import('app/views/pages/utilities/utilities.module').then(m => m.UtilitiesModule)
			},
			{
				path: 'support',
				loadChildren: () => import('app/views/pages/supports/supports.module').then(m => m.SupportsModule)
			},
			{path: 'error/:type', component: ErrorPageComponent},
			{path: '', redirectTo: 'dashboard', pathMatch: 'full'},
			{path: '**', redirectTo: '/error/404', pathMatch: 'full'}
		]
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	providers:[ModuleGuard],
	exports: [RouterModule]
})
export class PagesRoutingModule {
}
