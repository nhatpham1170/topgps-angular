import { Component, OnInit, ViewChild, ElementRef, EventEmitter, ChangeDetectorRef } from '@angular/core';
import * as L from 'leaflet';
import 'leaflet.markercluster';
import { MapConfigService } from '@core/utils';
import { MapConfigModel } from '@core/utils/map/_model/map-config';
import { MapService } from '@core/utils/map/_services/map.service';
import { Item } from '@core/utils/map';
import { ResizeEvent } from 'angular-resizable-element';
import { TrackingService } from '@core/map';
import { tap } from 'rxjs/operators';
import { User } from '@core/auth';

declare var $;
@Component({
  selector: 'kt-map-test',
  templateUrl: './map-test.component.html',
  styleUrls: ['./map-test.component.scss']
})
export class MapTestComponent implements OnInit {
  public options: any;
  public currentUserSelect: any;
  public changeUserEmiter:EventEmitter<User>;
  data = [
    {
      "id": "2388",
      "imei": "864811036964364",
      "name": "Quỳnh Ngọc",
      "active": "1",
      "status": 2,
      "lat": "20.658936",
      "lng": "106.2652",
      "address": "106.267378",
      "gsm": "20",
      "powerVolt": "12",
      "time": "2019-10-05 09:42:00",
      "timestamp": "1570243320",
      "groupId": "16",
      "groupName": "unknown",
      "sensors": [
        {
          "name": "GSM",
          "type": "gsm",
          "text": "65 %",
          "value": 65,
          "scaleValue": 4
        },
        {
          "text": 1.7,
          "name": "Áp lực",
          "type": "pressure",
          "value": "781",
          "unit": "kg/cm2",
          "scaleValue": ""
        },
        {
          "text": "22h 15m 59s",
          "name": "Uptime",
          "type": "time",
          "value": "80159",
          "unit": "",
          "scaleValue": ""
        }
      ]
    },
    {
      "id": "2388",
      "imei": "864811036964364",
      "name": "Quỳnh Ngọc 2",
      "active": "1",
      "status": 2,
      "lat": "20.658736",
      "lng": "106.2652",
      "address": "106.267378",
      "gsm": "20",
      "powerVolt": "12",
      "time": "2019-10-05 09:42:00",
      "timestamp": "1570243320",
      "groupId": "16",
      "groupName": "unknown",
      "sensors": [
        {
          "name": "GSM",
          "type": "gsm",
          "text": "65 %",
          "value": 65,
          "scaleValue": 4
        },
        {
          "text": 1.7,
          "name": "Áp lực",
          "type": "pressure",
          "value": "781",
          "unit": "kg/cm2",
          "scaleValue": ""
        },
        {
          "text": "22h 15m 59s",
          "name": "Uptime",
          "type": "time",
          "value": "80159",
          "unit": "",
          "scaleValue": ""
        }
      ]
    },
    {
      "id": "2387",
      "imei": "864811036958630",
      "name": "Quỳnh Trang",
      "active": "1",
      "status": 2,
      "lat": "20.602258",
      "lng": "106.344364",
      "address": "106.344364",
      "gsm": "31",
      "powerVolt": "12",
      "time": "2019-10-05 09:42:00",
      "timestamp": "1570243320",
      "groupId": "16",
      "groupName": "unknown",
      "sensors": [
        {
          "name": "GSM",
          "type": "gsm",
          "text": "100 %",
          "value": 100,
          "scaleValue": 5
        },
        {
          "text": 1.3,
          "name": "Áp Lực",
          "type": "pressure",
          "value": "584",
          "unit": "Kg/cm2",
          "scaleValue": ""
        }
      ]
    },
    {
      "id": "2386",
      "imei": "864811036964257",
      "name": "Nhà máy nước Quỳnh Giao",
      "active": "1",
      "status": 1,
      "lat": "20.679656",
      "lng": "106.309173",
      "address": "106.309173",
      "gsm": "22",
      "powerVolt": "12",
      "time": "2019-10-05 09:42:00",
      "timestamp": "1570243320",
      "groupId": "16",
      "groupName": "unknown",
      "sensors": [
        {
          "name": "GSM",
          "type": "gsm",
          "text": "71 %",
          "value": 71,
          "scaleValue": 4
        },
        {
          "text": 2.9,
          "name": "Áp lực",
          "type": "pressure",
          "value": "1045",
          "unit": "Kg/Cm2",
          "scaleValue": ""
        }
      ]
    },
    {
      "id": "2385",
      "imei": "864811036976863",
      "name": "Quỳnh Bảo",
      "active": "1",
      "status": 1,
      "lat": "20.618107",
      "lng": "106.330302",
      "address": "106.330302",
      "gsm": "26",
      "powerVolt": "12",
      "time": "2019-10-05 09:42:00",
      "timestamp": "1570243320",
      "groupId": "16",
      "groupName": "unknown",
      "sensors": [
        {
          "name": "GSM",
          "type": "gsm",
          "text": "84 %",
          "value": 84,
          "scaleValue": 5
        },
        {
          "text": 1,
          "name": "Áp Lực",
          "type": "pressure",
          "value": "633",
          "unit": "kg/cm2",
          "scaleValue": ""
        },
        {
          "text": "19h 48m 13s",
          "name": "Uptime",
          "type": "time",
          "value": "67693",
          "unit": "",
          "scaleValue": ""
        }
      ]
    },
    {
      "id": "2384",
      "imei": "864811036956139",
      "name": "Quỳnh Hưng",
      "active": "1",
      "status": 2,
      "lat": "20.624587",
      "lng": "106.338587",
      "address": "106.338587",
      "gsm": "25",
      "powerVolt": "12",
      "time": "2019-10-05 09:42:00",
      "timestamp": "1570243320",
      "groupId": "16",
      "groupName": "unknown",
      "sensors": [
        {
          "name": "GSM",
          "type": "gsm",
          "text": "81 %",
          "value": 81,
          "scaleValue": 5
        },
        {
          "text": 1.1,
          "name": "Áp lực",
          "type": "pressure",
          "value": "638",
          "unit": "Kg/cm2",
          "scaleValue": ""
        },
        {
          "text": "22h 40m 50s",
          "name": "Uptime",
          "type": "time",
          "value": "78050",
          "unit": "",
          "scaleValue": ""
        }
      ]
    },
    {
      "id": "2382",
      "imei": "864811036960578",
      "name": "NMN Thanh Sơn",
      "active": "1",
      "status": 1,
      "lat": "20.870293",
      "lng": "106.446231",
      "address": "106.446231",
      "gsm": "31",
      "powerVolt": "12",
      "time": "2019-10-05 09:42:20",
      "timestamp": "1570243340",
      "groupId": "17",
      "groupName": "unknown",
      "sensors": [
        {
          "name": "GSM",
          "type": "gsm",
          "text": "100 %",
          "value": 100,
          "scaleValue": 5
        },
        {
          "text": 3.1,
          "name": "Áp lực 1",
          "type": "pressure",
          "value": "1069",
          "unit": "Kg/cm2",
          "scaleValue": ""
        },
        {
          "text": "127h 38m 22s",
          "name": "Uptime",
          "type": "time",
          "value": "455902",
          "unit": "",
          "scaleValue": ""
        }
      ]
    },
    {
      "id": "2381",
      "imei": "864811036960511",
      "name": "Huyện đội Thanh Hà",
      "active": "1",
      "status": 1,
      "lat": "20.896922",
      "lng": "106.416924",
      "address": "106.416924",
      "gsm": "20",
      "powerVolt": "12",
      "time": "2019-10-05 09:42:00",
      "timestamp": "1570243320",
      "groupId": "17",
      "groupName": "unknown",
      "sensors": [
        {
          "name": "GSM",
          "type": "gsm",
          "text": "65 %",
          "value": 65,
          "scaleValue": 4
        },
        {
          "text": 0.69,
          "name": "Áp lực 1",
          "type": "pressure",
          "value": "462",
          "unit": "Kg/cm2",
          "scaleValue": ""
        },
        {
          "text": "1h 45m 52s",
          "name": "Uptime",
          "type": "time",
          "value": "2752",
          "unit": "",
          "scaleValue": ""
        }
      ]
    },
    {
      "id": "2389",
      "imei": "864811036962129",
      "name": "Văn Xá - Ái Quốc",
      "active": "1",
      "status": 1,
      "lat": "20.962307",
      "lng": "106.378276",
      "address": "106.378276",
      "gsm": "22",
      "powerVolt": "12",
      "time": "2019-10-05 09:42:00",
      "timestamp": "1570243320",
      "groupId": "19",
      "groupName": "unknown",
      "sensors": [
        {
          "name": "GSM",
          "type": "gsm",
          "text": "71 %",
          "value": 71,
          "scaleValue": 4
        },
        {
          "text": 0.79,
          "name": "Áp lực",
          "type": "pressure",
          "value": "527",
          "unit": "Kg/cm2",
          "scaleValue": ""
        },
        {
          "text": "49h 28m 39s",
          "name": "Uptime",
          "type": "time",
          "value": "178119",
          "unit": "",
          "scaleValue": ""
        }
      ]
    },
    {
      "id": "2377",
      "imei": "869696043528749",
      "name": "Khánh Hội - Nam Đồng",
      "active": "1",
      "status": 1,
      "lat": "20.930362",
      "lng": "106.341778",
      "address": "106.341778",
      "gsm": "26",
      "powerVolt": "12",
      "time": "2019-10-05 09:42:00",
      "timestamp": "1570243320",
      "groupId": "19",
      "groupName": "unknown",
      "sensors": [
        {
          "name": "GSM",
          "type": "gsm",
          "text": "84 %",
          "value": 84,
          "scaleValue": 5
        },
        {
          "text": 0.54,
          "name": "Áp Lực",
          "type": "pressure",
          "value": "446",
          "unit": "kg/cm2",
          "scaleValue": ""
        },
        {
          "text": "46h 59m 17s",
          "name": "Uptime",
          "type": "time",
          "value": "165557",
          "unit": "",
          "scaleValue": ""
        }
      ]
    },
    {
      "id": "2375",
      "imei": "869696043503312",
      "name": "Nhà máy nước Nam Đồng",
      "active": "1",
      "status": 1,
      "lat": "20.952807",
      "lng": "106.336409",
      "address": "106.336409",
      "gsm": "31",
      "powerVolt": "11",
      "time": "2019-10-05 09:42:00",
      "timestamp": "1570243320",
      "groupId": "19",
      "groupName": "unknown",
      "sensors": [
        {
          "name": "GSM",
          "type": "gsm",
          "text": "100 %",
          "value": 100,
          "scaleValue": 5
        },
        {
          "text": 3.4,
          "name": "Áp lực",
          "type": "pressure",
          "value": "1046",
          "unit": "Kg/cm2",
          "scaleValue": ""
        },
        {
          "text": "23h 4m 27s",
          "name": "Uptime",
          "type": "time",
          "value": "83067",
          "unit": "",
          "scaleValue": ""
        }
      ]
    }
  ];
  map;
  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  constructor(private mapService: MapService, private cdr:ChangeDetectorRef,
    private trackingService:TrackingService) {
    this.options = this.mapService.init();
    this.parrentEmiter = new EventEmitter();
    this.changeUserEmiter = new EventEmitter();
    this.currentUserSelect = {};
  }

  ngOnInit() {
    this.test();
  } 
  onMapReady(map) {
    this.mapService.setMap(map);
    let _this = this; 
    // setTimeout(() => {     
    //   let items: Array<Item> = [];
    //   _this.data.forEach((x) => {
    //     let item = new Item(x);
    //     Object.assign(item, x);
    //     item.direction = Math.random() * 360;
    //     items.push(item);
    //     return item;
    //   });
    //   _this.mapService.addMarkers(items, true);

    // });   
  }
  test() {
    // this.trackingService.list({params:{groupId:-1}}).pipe(
    //   tap(x=>{
    //     console.log(x);
    //   })
    // ).subscribe();
  }
  onResize($elm) {
    this.mapService.resize();
    this.cdr.detectChanges();
  }
  getMapService() {
    return this.mapService;
  }
  changeUser(value){    
    this.currentUserSelect = value;
    this.changeUserEmiter.emit(value);
  }
}
