import { PoiToolComponent } from './../manage/poi-manage/poi-tool/poi-tool.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MapComponent } from './map.component';
import { AuthGuard, ModuleGuard } from '../../../../../src/app/core/auth';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { LeafletModule } from "@asymmetrik/ngx-leaflet";
import { DataServiceService } from './data-service.service';
import { MapTestComponent } from './map-test/map-test.component';
import { PartialsModule } from '@app/views/partials/partials.module';
import { ResizeObserverDirective } from '@core/_base/layout';
import { CoreModule } from '@core/core.module';
import { NgbModalModule, NgbModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { DeviceComponent } from './device/device.component';
import { TrackingComponent } from './tracking/tracking.component';
import { InfoComponent } from './tracking/info/info.component';
import { RouteComponent } from './tracking/route/route.component';
import { NotificationsComponent } from './tracking/notifications/notifications.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ChartComponent } from './tracking/chart/chart.component';
import { DeviceInfoBoxComponent } from './device-info-box/device-info-box.component';
import { TrackingMultiComponent } from './tracking-multi/tracking-multi.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPermissionsModule } from 'ngx-permissions';
import { TollFeeCalculationComponent } from './toll-fee-calculation/toll-fee-calculation.component';
import { TrackingGeosfencesComponent } from './tracking-geofences/tracking-geofences.component';
import { TrackingLandmarkComponent } from './tracking-landmark/tracking-landmark.component';
import { MatExpansionModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: MapComponent,
    canActivate: [ AuthGuard],
    // data: {
    //   permisison: 'ROLE_map.map',
    // },
  },
  {
    path: 'trackings',
    component: TrackingMultiComponent,
    canActivate: [ModuleGuard, AuthGuard],
    data: {
      permisison: "Role_map.trackings"
    }
  },
  {
    path: 'toll-fee',
    component: TollFeeCalculationComponent,
    canActivate: [ModuleGuard, AuthGuard],
    // data: {
    //   permisison: "Role_map.trackings"
    // }
  },
  {
    path: 'tracking-geofences',
    component: TrackingGeosfencesComponent,
    canActivate: [ModuleGuard, AuthGuard],
    // data: {
    //   permisison: "Role_map.trackings"
    // }
  },
  {
    path: 'tracking-landmarks',
    component: TrackingLandmarkComponent,
    canActivate: [ModuleGuard, AuthGuard],
    // data: {
    //   permisison: "Role_map.trackings"
    // }
  },
]

@NgModule({

  imports: [
    CommonModule,
    CoreModule,
    RouterModule.forChild(routes),
    DragDropModule,
    LeafletModule,
    PartialsModule,
    NgbModule,
    NgbModalModule,
    ScrollingModule,
    InfiniteScrollModule,
    NgbPopoverModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPermissionsModule.forChild(),
    MatExpansionModule,

  ],
  providers: [AuthGuard, ModuleGuard],
  declarations: [MapComponent,
    MapTestComponent,
    DeviceComponent,
    TrackingComponent,
    InfoComponent,
    RouteComponent,
    NotificationsComponent,
    ChartComponent,
    DeviceInfoBoxComponent,
    TrackingMultiComponent,
    TollFeeCalculationComponent,
    TrackingGeosfencesComponent,
    TrackingLandmarkComponent],
})
export class MapModule { }
