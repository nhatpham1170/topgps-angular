import { Component, OnInit, ViewChild, ElementRef, EventEmitter, ChangeDetectorRef, ComponentFactoryResolver, Injector, OnDestroy, OnChanges, SimpleChanges, ViewContainerRef, ComponentRef } from '@angular/core';
import * as L from 'leaflet';
import 'leaflet.markercluster';
import { MapConfigService } from '@core/utils';
import { MapConfigModel } from '@core/utils/map/_model/map-config';
import { MapService } from '@core/utils/map/_services/map.service';
import { MapService as MapServiceAPI } from '@core/map';
import { Item, MapUtil, MapChangeEvent } from '@core/utils/map';
import { ResizeEvent } from 'angular-resizable-element';
import { TrackingService } from '@core/map';
import { tap, finalize } from 'rxjs/operators';
import { User, CurrentUserService } from '@core/auth';
import { GeofenceService, GeofenceModel } from '@core/manage';
import { NgbModal, NgbModalRef, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { DeviceMap } from '@core/map/_models/device-map';
import { TrackingItemComponent } from '@app/views/partials/content/widgets/tracking-item/tracking-item.component';

declare var $;
@Component({
  selector: 'kt-map',
  templateUrl: './tracking-multi.component.html',
  styleUrls: ['./tracking-multi.component.scss']
})
export class TrackingMultiComponent implements OnInit, OnDestroy,OnChanges {
  public options: any;
  public currentUserSelect: any;
  public listDeviceEventEmiter: EventEmitter<{ type: string, data: any }>;
  public map;
  public listGeofence: Array<GeofenceModel>;
  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  public geofenceDataChangeEmiter: EventEmitter<any>;
  public mapConfig: MapConfigModel;
  public optionWideCheckList: any = { type: "geofence" };
  private mapService: MapService;
  public closeResult: string;
  private modalReference: NgbModalRef;
  public dataTrackingItem: any;
  public ktMapFit: boolean;

  public isTracking: boolean;
  public currentListDevices: Array<any>;
  public deivceSelectedTracking: any;
  public isUserLogin: boolean = true;
  public trackingActionDefault: string = "now";
  public chartShow: boolean;
  public chartHide: boolean;
  private subscribeMapServiceAPI: Subscription;
  public showUserTree:boolean;
  public optionGrid:{
    columns:number,
    rows:number,
    arrColumns:Array<number>,
    arrRows:Array<number>,
  };
  public listDevice:Array<Item> = [];
  public withMapItem:number;
  public widthMapTrackingMulti:number;
  public viewMapItems:Array<{component:ComponentRef<TrackingItemComponent>,id:number,subscribe:Subscription}> = [];
  @ViewChild('mapItemsRef',{static:true,read:ViewContainerRef}) mapItemsRef:ViewContainerRef;
  @ViewChild('contentTrackingItem', { static: true }) contentTrackingItem: ElementRef;
  constructor(private cdr: ChangeDetectorRef,
    private trackingService: TrackingService,
    private geofenceService: GeofenceService,
    private resolver: ComponentFactoryResolver, private injector: Injector,
    private modalService: NgbModal,
    private currentUserService: CurrentUserService,
    private mapServiceAPI: MapServiceAPI,
    private userTreeService:UserTreeService) {
    this.mapService = new MapService(this.resolver, this.injector,"tracking-multi");
    this.options = this.mapService.init();
    this.parrentEmiter = new EventEmitter();
    this.listDeviceEventEmiter = new EventEmitter();
    this.geofenceDataChangeEmiter = new EventEmitter();
    this.currentUserSelect = {};
    this.listGeofence = [];
    this.mapConfig = this.mapService.config;
    this.mapService.config.features.follow.show = true;
    this.mapService.config.features.markerPopup.show = true;
    this.mapService.config.features.markerStyle.options.showSpeed = true;
    // this.mapService.config.features.markerStyle.options.showDuration = true;
    this.mapService.config.features.cluster.options.showSpeed = true;
    this.mapService.config.features.cluster.options.showDuration = true;
    this.mapService.config.features.fullScreen.show = true;
    this.mapService.config.features.brightness.show = true;
    this.mapService.config.features.geofence.show = false;
    this.mapService.config.features.landmark.show = false;
    this.mapService.config.features.createTool.show = false;
    this.mapService.config.features.location.show = false;
    this.mapService.config.features.cluster.show = false;
    this.mapService.config.features.cluster.value = false;
    this.mapService.config.features.follow.show = false;
    if(!this.mapService.getCacheConfigs()){
      this.mapService.config.features.markerPopup.value = false;
      Object.assign(this.mapService.config.infoBox.advance,{
        icon:true,
        duration:false,
        speed:false,
        battery:false,
        gsm:false,
        numberPlate:true,
        imei:false,
        simno:true,
        vin:false,
        powerVoltage:false,
        distanceToday:false,
        driver:false,
        drivingToday:false,
        updateTime:true,
        group:false,
        geofence:false,
        address:true,
      });
    }
    delete this.mapService.config.mapType.googleRoadmap;
    delete this.mapService.config.mapType.googleSatellite;
    this.mapService.renderMarker = false;
    
    this.ktMapFit = false;
    this.isTracking = false;
    this.currentUserService.init();
    this.chartShow = false;
    this.showUserTree = true;
    this.optionGrid = {columns:3,rows:4,arrColumns:Array(4).fill(1).map((x,i)=>i+1),arrRows:Array(3).fill(1).map((x,i)=>i+1)};
  }

  ngOnInit() {
    this.mapService.eventChange.pipe(
      tap(data => {
        this.mapServiceEventChange(data);
      })
    ).subscribe();
    this.subscribeMapServiceAPI = this.mapServiceAPI.eventChange.pipe(
      tap(event => {
        this.listenEventMapService(event);
      })
    ).subscribe();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();

    let clientRect = document.getElementById('mapTrackingMulti').getBoundingClientRect();
    this.widthMapTrackingMulti = clientRect.height;
  }
  ngOnChanges(changes:SimpleChanges){    
  }
  ngOnDestroy() {
    if (this.subscribeMapServiceAPI) this.subscribeMapServiceAPI.unsubscribe();
  }
  mapServiceEventChange(data: MapChangeEvent) {
    const countOld = this.viewMapItems.length;
    switch (data.type) {
      case "feature__zoom_in":
        this.viewMapItems.forEach(x=>{
          x.component.instance.getMapService().zoomIn();
        });
        break;
      case "feature__change_map_type":
        this.viewMapItems.forEach(x=>{
          x.component.instance.mapService.changeMapType(data.data);
        });
        break;
      case "feature__change_style_marker":
        this.viewMapItems.forEach(x=>{
          x.component.instance.mapService.changStyleMarker(data.data);
        });
        break;
      case "feature__zoom_out":
        this.viewMapItems.forEach(x=>{
          x.component.instance.getMapService().zoomOut();
        });
        break;
      case "feature__brightness":
        this.viewMapItems.forEach(x=>{
          x.component.instance.getMapService().setBrightness(data.data);
        });
        break;
      case "feature__change_config_controls":
        this.viewMapItems.forEach(x=>{
          x.component.instance.getMapService().refresh();
        });
        break;
      case "feature__fitbound":
        this.viewMapItems.forEach(x=>{
          x.component.instance.getMapService().fitbound(data.data,{maxZoom:18});
        });
        break;
      case "followPopup":
        this.followPopup(data.data);
        break;
      case "fullScreen":
        this.fullScreen(data.data);
        break;
      case "action__update_items":
        data.data.forEach((x:Item)=>{
          const indexItem = this.viewMapItems.findIndex((i:Item)=>i.id == x.id);
          if(indexItem>=0){
            if(x.feature['hideOnMap']){
              this.viewMapItems[indexItem].subscribe.unsubscribe();
              this.viewMapItems[indexItem].component.destroy();
              this.viewMapItems.splice(indexItem,1);
            }else{
              this.viewMapItems[indexItem].component.instance.updateItem(x);
              this.viewMapItems[indexItem].component.changeDetectorRef.detectChanges();
            }
          }
          else if(!x.feature['hideOnMap']){
            const factory = this.resolver.resolveComponentFactory(TrackingItemComponent);
            const component = this.mapItemsRef.createComponent(factory);
            component.changeDetectorRef.detectChanges();
            component.instance.addItem(x);
            
            component.instance.mapService.config = this.mapService.config;
            component.instance.mapService.setBrightness(this.mapService.config.features.brightness.value);
            component.instance.updateItem(x);
            component.changeDetectorRef.detectChanges();
            const subscribe =  component.instance.getMapService().eventChange.subscribe((data)=>{
               if(data.type == "action__select_device")
               {
                this.mapService.sendEmitter({ type: "action__set_view_list_device", data: data.data });
               } 
            });
            component.instance.close =  (item)=>{
              // console.log(item);
              
              this.mapService.sendEmitter({ type: "action__uncheck_list_device", data: item });
            }
            this.viewMapItems.push({component:component,id:x.id,subscribe:subscribe});
            
          }
          // close 
       
          this.listDevice.push(x);
        });
        this.updateLayout();
        break;
      case "action__add_items":
        this.viewMapItems.forEach(x=>{
          x.subscribe.unsubscribe();
          x.component.destroy();
        })
        this.viewMapItems = [];
        // this.updateLayout();
        
        break;
      case "action__remove_items":
        data.data.forEach((x:Item)=>{
          const indexItem = this.viewMapItems.findIndex((i:Item)=>i.id == x.id);
          if(indexItem>=0){
            if(x.feature['hideOnMap']){
              this.viewMapItems[indexItem].subscribe.unsubscribe();
              this.viewMapItems[indexItem].component.destroy();
              this.viewMapItems.splice(indexItem,1);
            }
          }
        });
        this.updateLayout();
        break;
    }
    if(countOld!= this.viewMapItems.length){
      this.updateLayout();
    }
  }
  sendActionFeature(action:string){
   
  }
  
  updateListDevice(){
    // this.listDevice = this.mapService.getItems().filter(x=>{
    //   return !x.feature.hideOnMap;
    // });
    // this.updateLayout();
  }
  followPopup(item) {
    this.dataTrackingItem = item;
    this.open(this.contentTrackingItem);
    this.cdr.detectChanges();
  }
  fullScreen(data) {
    if (data) {
      this.ktMapFit = true;
    }
    else {
      this.ktMapFit = false;
    }
  }
  onMapReady(map) {
    
    // this.options.layer.updateFilter(['brightness:110%', 'hue:90deg', 'saturate:120%']);
    this.mapService.setMap(map);
    // this.mapService.config.features.cluster.value = true;
  }

  onResize() {
    this.mapService.resize();
    this.cdr.detectChanges();
  }

  onResizeMapMulti($elm)
  {
    this.widthMapTrackingMulti = $elm.width; 
    this.updateLayout();
  }
  updateLayout(){
    const  rows = Math.ceil(Math.sqrt(this.viewMapItems.length));
    this.withMapItem = this.widthMapTrackingMulti/rows;
    this.viewMapItems.forEach(x=>{
      x.component.instance.setStyle(this.withMapItem);
    })
    this.cdr.detectChanges();
  }

  getMapService() {
    return this.mapService;
  }

  changeUser(value) {
    this.currentUserSelect = value;
    this.listDeviceEventEmiter.emit({ type: "user", data: value });
    this.loadGeofence(this.currentUserSelect);
    if (value.id != this.currentUserService.currentUser.id) {
      this.isUserLogin = false;
    }
    else this.isUserLogin = true;
    this.cdr.detectChanges();
  }

  // get list geofence
  loadGeofence(user: any) {
    this.geofenceService.list({ params: { userId: user.id, pageNo: -1 } }).pipe(
      tap((data) => {
        data.result = data.result.filter(x => x.active == 1);
        this.geofenceDataChangeEmiter.emit(data.result);
        this.listDeviceEventEmiter.emit({ type: "geofence", data: data.result });
      }),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
    // this.open(this.contentTrackingItem);
  }

  onWidgetCheckChange(data) {
    if (data.type) {
      switch (data.type) {
        case "check":
          this.mapService.clearGeofences();
          this.mapService.addGeofences(data.listChecked, false);
          break;
        case "selecte-marker":
          this.mapService.fitBoundGeofences(data.items);
          break;
        case "selecte-Landmark":
          this.mapService.fitBoundGeofences(data.items);
          break;
      }
    }
  }

  ktListDeviceEventChange(data) {
    if (data.type) {
      switch (data.type) {
        case "tracking":
          if (data.options && data.options.tab) {
            this.trackingActionDefault = data.options.tab;
          }
          this.isTracking = true;
          this.deivceSelectedTracking = data.data;
          this.listDeviceEventEmiter.emit({ type: "tracking", data: true });

          this.cdr.detectChanges();
          break;
        case "list_data":
          this.currentListDevices = data.data;
          break;
      }
    }
  }

  eventTracking(event) {
    if (event.type == "close_tracking") {
      this.chartShow = false;
      this.isTracking = false;
      this.listDeviceEventEmiter.emit({ type: "tracking", data: false });
      this.cdr.detectChanges();
    }
  }

  listenEventMapService(event) {
    switch (event.type) {
      case "tracking_route":
        this.chartShow = true;
        this.chartHide = false;
        this.cdr.detectChanges();
        break;
      case "tracking_route_hide":
        this.chartHide = true;
        this.cdr.detectChanges();
        break;
    }
  }


  open(content) {
    this.modalReference = this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal--fit kt-modal-clear', size: "lg", backdrop: 'static' });
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
