import { DeviceService } from './../../../../core/manage/_services/device.service';
import { Component, OnInit, ComponentFactoryResolver, Injector, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { MapService } from '@core/utils/map';
import { TransportTypeService } from '@core/admin';
import { debounceTime, takeUntil, finalize, tap } from 'rxjs/operators';
import { CurrentUserService } from '@core/auth';

@Component({
  selector: 'kt-toll-fee-calculation',
  templateUrl: './toll-fee-calculation.component.html',
  styleUrls: ['./toll-fee-calculation.component.scss']
})
export class TollFeeCalculationComponent implements OnInit {
  public ktMapFit: boolean;
  private mapService: MapService;
  public options: any;
  private filter: any;
  public transportType: any;
  public listDeivce: any;
  public isShowCalculate = false;
  private isShow = true
  @ViewChild('calculator',{static: true}) calculator: ElementRef;
  @ViewChild('btn', { static: true }) btn: ElementRef;
  @ViewChild('icon', { static: true }) icon: ElementRef;

  constructor(private resolver: ComponentFactoryResolver, 
    private injector: Injector, 
    private cdr: ChangeDetectorRef, 
    private transportService: TransportTypeService,
    private deviceService: DeviceService,
    private currentUserService: CurrentUserService,
    ) {
    this.mapService = new MapService(this.resolver, this.injector, "toll-fee");
    this.options = this.mapService.init();
    this.currentUserService.init();
  }

  ngOnInit() {
    this.mapService.config.features.geofence.show = false;
    this.mapService.config.features.landmark.show = false;
    this.mapService.config.features.createTool.show = false;
    this.mapService.config.features.centerPoint.show = false;
    this.mapService.config.features.cluster.show = false;
    this.mapService.config.features.follow.show = false;
    this.mapService.config.features.markerPopup.show = false;
    this.mapService.config.features.markerStyle.show = false;
    this.mapService.config.features.markerStyle.options.showSpeed = false;
    this.mapService.config.features.cluster.value = false;
    this.mapService.config.features.fitbound.show = false;
    this.mapService.config.features.location.show = false;
    this.filter = {
      pageNo: 1,
      pageSize: 10,
      orderBy: 'sortOrder',
      orderType: 'ASC'
    };
    this.getTransportType();
    this.getDevice();
  }

  onResize() {
    this.mapService.resize();
    this.cdr.detectChanges();
  }

  onMapReady(map) {
    this.mapService.setMap(map);
    this.isShowCalculate = true
  }

  getMapService() {
    return this.mapService;
  }

  showPopup() {

  }

  getTransportType() {
    this.transportService.list(this.filter)
      .pipe(
        debounceTime(1000),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.transportType = result.result.content;
        }
      })
  }

  async getDevice() {
    let params = {
      userId: this.currentUserService.currentUser.id,
      pageNo: -1
    };
    await this.deviceService.list({ params: params })
    .pipe(
      tap((data: any) => {
        this.listDeivce = data.result.filter(device=>{
          return device.transportType != 0
        });
      }),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).toPromise();
  }

  toggle(){
    if(this.isShow){
      this.calculator.nativeElement.style.width = '0px';
      this.btn.nativeElement.style.right= '-20px';
      this.btn.nativeElement.style.width = '20px';
      this.icon.nativeElement.style.transform = 'rotate(180deg)'
      this.isShow = !this.isShow;

    } else {
      this.isShow = !this.isShow
      this.calculator.nativeElement.style.width = '400px';
      this.btn.nativeElement.style.right = '0px';
      this.btn.nativeElement.style.width = '15px';

      this.icon.nativeElement.style.transform = 'rotate(0deg)'
    }
    
    
  }
}
