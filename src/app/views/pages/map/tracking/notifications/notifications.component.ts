import { Component, OnInit, Input, EventEmitter, ChangeDetectorRef, ViewChild, OnDestroy } from '@angular/core';
import { MapService, MapUtil } from '@core/utils/map';
import { Alert, AlertPopup, AlertService, AlertRulesService } from '@core/manage';
import { tap, finalize, delay } from 'rxjs/operators';
import * as L from 'leaflet';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastService, UserDateAdvPipe } from '@core/_base/layout';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { BehaviorSubject, Subscription } from 'rxjs';

@Component({
  selector: 'kt-tracking-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
  providers: [UserDateAdvPipe]
})
export class NotificationsComponent implements OnInit, OnDestroy {

  @Input('mapService') mapService: MapService;
  @Input('item') item: any;
  @Input("isUserLogin") isUserLogin: boolean;
  @Input('itemChange') itemChange: EventEmitter<any>;
  @ViewChild(CdkVirtualScrollViewport, { static: true }) viewPort: CdkVirtualScrollViewport;
  public infinite = new BehaviorSubject<any[]>([]);
  public notifications: Array<Alert>;
  private featureLayer: L.FeatureGroup;
  public alertPopup: AlertPopup;
  public closeResult: string;
  public loadingList: boolean;
  public loadMore: boolean;
  public allowLoadMore: boolean;
  public loadNew: boolean;
  public isClosed: boolean;
  public itemSize = 20;
  private pageNo = 1;
  private pageSize = 20;
  private startTime: string;
  private endTime: string;
  private subscribeAlert: Subscription;
  private subscribeItemChange: Subscription;

  constructor(
    private alertService: AlertService,
    private alertRuleService: AlertRulesService,
    private mapUtil: MapUtil,
    private modalService: NgbModal,
    private toastService: ToastService,
    private cdr: ChangeDetectorRef,
    private userDateAdv: UserDateAdvPipe) {
    this.notifications = [];
    this.featureLayer = new L.FeatureGroup();
    this.loadingList = false;
    this.loadMore = false;
    this.loadNew = false;
    this.isClosed = false;
    this.allowLoadMore = false;
  }

  ngOnInit() {
    this.getAlerts();
    this.mapService.map.addLayer(this.featureLayer);
    this.subscribeAlert = this.alertService.eventChange.pipe(
      tap(event => {
        this.listenAlertEvent(event);
      })
    ).subscribe();
    this.subscribeItemChange = this.itemChange.pipe(
      tap(event => {
        this.processEvent(event);
      })
    ).subscribe();
  }
  ngOnDestroy() {
    this.isClosed = true;
    this.subscribeAlert.unsubscribe();
    this.subscribeItemChange.unsubscribe();
  }
  getAlerts() {
    this.startTime = "";
    this.endTime = "";
    this.loadingList = true;
    this.allowLoadMore = true;
    this.notifications = [];
    this.cdr.detectChanges();
    this.alertService.getListAlert({ pageNo: 1, pageSize: 20, deviceId: this.item.id }).pipe(
      delay(300),
      tap(data => {
        this.notifications = data.result.content;
        if (this.notifications.length > 0) {
          this.startTime = this.userDateAdv.transform(this.notifications[0].createdAt, "YYYY-MM-DD HH:mm:ss");
          this.endTime = this.userDateAdv.transform(this.notifications[this.notifications.length - 1].createdAt, "YYYY-MM-DD HH:mm:ss");
          this.alertService.eventChange.emit({ type: "seen_device", data: this.item });
        }
        if (data.result.content.length < 20) {
          this.allowLoadMore = false;
        }
      }),
      finalize(() => {
        this.loadingList = false;
        this.cdr.markForCheck();
        this.cdr.detectChanges();
      })
    ).subscribe();
    // this.cdr.detectChanges();
  }
  getMore() {
    if (this.startTime.length == 0) {
      this.getAlerts();
      return;
    }
    this.loadMore = true;
    this.alertService.getListAlert({ pageNo: 1, pageSize: 5, timeTo: this.endTime, deviceId: this.item.id, timeEqual: 0 }).pipe(
      delay(300),
      tap(data => {
        this.notifications = this.notifications.concat(data.result.content);

        this.startTime = this.userDateAdv.transform(this.notifications[0].createdAt, "YYYY-MM-DD HH:mm:ss");
        this.endTime = this.userDateAdv.transform(this.notifications[this.notifications.length - 1].createdAt, "YYYY-MM-DD HH:mm:ss");
        if (data.result.content.length < 5) {
          this.allowLoadMore = false;
        }
      }),
      finalize(() => {
        this.loadMore = false;
        this.cdr.markForCheck();
        this.cdr.detectChanges();
      })
    ).subscribe();
    this.cdr.detectChanges();
  }
  getNews() {

    if (this.startTime.length == 0) {
      this.getAlerts();
      return;
    }
    this.loadNew = true;
    this.alertService.getListAlert({ pageNo: 1, pageSize: 1000, timeFrom: this.startTime, deviceId: this.item.id, timeEqual: 0 }).pipe(
      delay(300),
      tap(data => {
        this.notifications = data.result.content.concat(this.notifications);
        this.startTime = this.userDateAdv.transform(this.notifications[0].createdAt, "YYYY-MM-DD HH:mm:ss");
        this.endTime = this.userDateAdv.transform(this.notifications[this.notifications.length - 1].createdAt, "YYYY-MM-DD HH:mm:ss");
        this.alertService.eventChange.emit({ type: "seen_device", data: this.item });
      }),
      finalize(() => {
        this.loadNew = false;
        this.cdr.markForCheck();
        this.cdr.detectChanges();
      })
    ).subscribe();
    this.cdr.detectChanges();
  }
  readMarkAll() {
    if (this.isUserLogin)
      this.alertService.markAllRead({ deviceId: this.item.id, isRead: 1 }).subscribe();
  }
  refresh() {
    this.getNews();
  }
  showMarker(item) {
    this.featureLayer.clearLayers();
    let marker = this.mapUtil.createMarker([item.latitude, item.longitude]);
    if (marker) {
      this.featureLayer.addLayer(marker);
      this.mapService.map.setView(marker.getLatLng(), this.mapService.map.getZoom());
    }
    if (item.isRead != 1 && this.isUserLogin) {
      item.isRead = 1;
      this.cdr.detectChanges();
      this.markAsRead(item.id, 1);
    }
  }

  async showInfo(content, alert) {
    await this.alertService.alertDetail(alert.id).pipe(
      tap(data => {
        if (data.status == 200) {
          this.alertPopup = data.result;
        }
        this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal--fit kt-modal-clear', size: 'lg', backdrop: 'static' })
          .result.then((result) => {
            this.closeResult = `Closed with: ${result}`;

          }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          });
      },
        error => {
          this.toastService.show({ translate: error.messageCode, type: "warning" });
        }),

      finalize(() => this.cdr.markForCheck())
    ).toPromise();

    // update is read
    if (alert.isRead != 1 && this.isUserLogin) {
      alert.isRead = 1;
      this.cdr.detectChanges();
      this.markAsRead(alert.id, 1);
    }
  }
  markAsRead(id: number, isRead: number) {
    let params = {
      id: id,
      isRead: isRead
    }
    this.alertService.markAsRead(params).subscribe();
  }
  readChange(item) {
    if (this.isUserLogin) {
      item.isRead = (item.isRead == 0 ? 1 : 0);
      // this.
      this.cdr.detectChanges();
      this.markAsRead(item.id, item.isRead);
    }

  }
  listenAlertEvent(event) {
    switch (event.type) {
      case "mark_read":
        this.notifications = this.notifications.map(x => {
          if (x.id == event.data.id) x['isRead'] = event.data.isRead;
          return x;
        });
        break;
      case "mark_read_all":
        if (event.data.deviceId) {
          if (this.item.id == event.data.deviceId) {
            this.notifications = this.notifications.map(x => {
              x['isRead'] = event.data.isRead;
              return x;
            });
          }

        }
        else {
          this.notifications = this.notifications.map(x => {
            x['isRead'] = event.data.isRead;
            return x;
          });
        }
        break;
    }
  }
  onScrollDown() {
    // console.log('scrolled down!!');    
    if (!this.loadMore && this.allowLoadMore) {
      this.getMore();
    }
  }

  onScrollUp() {
    // console.log('scrolled up!!');
    if (!this.loadNew) {
      this.getNews();
    }
  }
  processEvent(data) {
    switch (data.type) {
      case "change_item_tracking":
        if (!this.isClosed) {
          this.item = data.data;
          this.getAlerts();
        }
        break;
      case "change_tab":
        if (data.data == "notification") {
          this.isClosed = false;
        }
        else {
          if (this.featureLayer) this.featureLayer.clearLayers();
          this.isClosed = true;
        }
        break;
    }

  }
  checkIsCurrentUser() {
    sessionStorage.get("");
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
