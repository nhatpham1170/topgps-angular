import {
	Component,
	OnInit,
	ViewChild,
	ChangeDetectorRef,
	Input,
	OnDestroy,
	EventEmitter,
} from '@angular/core';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { MapService } from '@core/map';
import { tap, finalize, takeUntil } from 'rxjs/operators';
import { Subject, from, Subscription } from 'rxjs';
import { MapConfigService, XlsxService } from '@core/utils';
import {
	MapService as MapServiceUtil,
	MapUtil,
	MapUtilityService,
	LayoutMapConfigModel,
} from '@core/utils/map';
import * as L from 'leaflet';
import { UserDateAdvPipe } from '@core/_base/layout';
import { options } from '@amcharts/amcharts4/core';
import objectPath from 'object-path';
import { XLSXModelTest, ExcelService } from '@core/utils/xlsx/excel.service';
import { TranslateService } from '@ngx-translate/core';
import { CurrentUserService } from '@core/auth';
import { PdfModel, PdfmakeService } from '@core/utils/pdfmake/pdfmake.service';
// import { objectPath } from 'object-path'

declare var $;
const PLAYBACK_STOP: string = 'PLAYBACK_STOP';
const KEY_CACHE_CHART: string = 'tracking_chart_show';
@Component({
	selector: 'kt-tracking-route',
	templateUrl: './route.component.html',
	styleUrls: ['./route.component.scss'],
	providers: [UserDateAdvPipe],
})
export class RouteComponent implements OnInit, OnDestroy {
	// public datePicker;
	@Input('mapService') mapServiceUtil: MapServiceUtil;
	@Input('item') item: any;
	@Input('itemChange') itemChange: EventEmitter<any>;
	@ViewChild('datePicker', { static: true })
	datePicker: DateRangePickerComponent;

	public routeData: any = [];
	public datePickerOptions: any = {
		size: 'md',
		select: 'from',
		ranges: 'from',
		optionDatePicker: {
			maxSpan: {
				days: 31,
			},
		},
		autoSelect: true,
		singleDatePicker: false,
		timePicker: true,
	};
	private startDate: string;
	private endDate: string;
	public routeLoading: boolean;
	private searchOld: {
		startDate?: string;
		endDate?: string;
	};
	public timeItemSelect: number;
	public currentPopup: any;
	public summary: {
		distance: number;
		driving: number;
		stop: number;
	};
	public allowShowLostSignal: boolean = true;
	public allowNext: boolean = true;
	public allowPrevious: boolean = true;
	public lastData: any;
	private featureLayer: L.FeatureGroup;
	private isClosed: boolean = false;
	private destroy: boolean = false;
	private subscription: Subscription;
	private subscriptionMapServiceUtil: Subscription;
	private subscriptionMapService: Subscription;
	private unsubscribe: Subscription[] = [];
	public layoutMapConfig: LayoutMapConfigModel;

	constructor(
		private mapService: MapService,
		private mapUtil: MapUtil,
		private cdr: ChangeDetectorRef,
		private userDateAdvPipe: UserDateAdvPipe,
		private mapUtilService: MapUtilityService,
		private mapConfigService: MapConfigService,
		private translate: TranslateService,
		private datepipe: UserDateAdvPipe,
		private currentUserService: CurrentUserService,
		private xlsxService: ExcelService,
		private pdf: PdfmakeService,
	) {
		this.routeData = this.routeData.map((day) => {
			let countStop = 1;
			day.data = day.data.map((item) => {
				if (item.type == 'stop') {
					item.index = countStop;
					countStop++;
				}
				return item;
			});
			return day;
		});
		this.routeData = [];
		this.routeLoading = false;
		this.searchOld = {};

		this.summary = {
			distance: 0,
			driving: 0,
			stop: 0,
		};
		this.layoutMapConfig = this.mapConfigService.getLayoutConfig();
		const sub = this.mapConfigService.layoutConfigUpdate$.subscribe(
			(layoutConfig) => {
				this.layoutMapConfig = layoutConfig;
			}
		);
		this.unsubscribe.push(sub);
	}

	ngOnInit() {

		if (this.item.timestampUTC) {
			let date = this.userDateAdvPipe.transform(
				this.item.timestampUTC,
				'YYYY/MM/DD'
			);
			this.datePickerOptions.startDate = date + ' 00:00:00';
			this.datePickerOptions.endDate = date + ' 23:59:59';
			this.cdr.detectChanges();
		}

		if (this.itemChange) {
			this.subscription = this.itemChange
				.pipe(
					tap((data) => {
						this.changeItem(data);
					})
				)
				.subscribe();
		}
		// check event map service
		this.subscribeEventMapService();
		this.subscriptionMapService = this.mapService.eventChange
			.pipe(
				tap((event) => {
					this.listenMapServiceChange(event);
				})
			)
			.subscribe();
		this.mapService.eventChange.emit({
			type: 'tracking_route',
			options: { startTime: this.startDate, endTime: this.endDate },
		});
	}

	ngOnDestroy() {
		this.isClosed = true;
		this.destroy = true;
		if (this.featureLayer) this.featureLayer.remove();
		this.lastData = undefined;
		this.mapService.eventChange.emit({
			type: 'tracking_route_hide',
			options: { startTime: this.startDate, endTime: this.endDate },
		});
		if (this.subscription) this.subscription.unsubscribe();
		if (this.subscriptionMapServiceUtil)
			this.subscriptionMapServiceUtil.unsubscribe();
		if (this.subscriptionMapService)
			this.subscriptionMapService.unsubscribe();
		this.unsubscribe.forEach((s) => s.unsubscribe());
	}

	// Select Timeline
	selectTime(time) {
		this.mapService.eventChange.emit({
			type: 'select_route',
			data: time,
		});
	}

	subscribeEventMapService() {
		if (this.mapServiceUtil.eventChange) {
			this.subscriptionMapServiceUtil = this.mapServiceUtil.eventChange
				.pipe(
					tap((data) => {
						this.mapServiceUtilChange(data);
					}),
					takeUntil(new Subject())
				)
				.subscribe();
		}
	}

	mapServiceUtilChange(event) {
		if (event.type == 'feature__fitbound') {
			if (this.featureLayer.getLayers().length > 0) {
				this.featureLayer.addTo(this.mapServiceUtil.map);
				this.mapUtil.fitBound(
					this.featureLayer,
					this.mapServiceUtil.map,
					this.getOptionFitBound()
				);
				// this.mapUtil.fitBound(this.featureLayer, this.mapServiceUtil.map);
			}
		}
	}

	listenMapServiceChange(event) {
		switch (event.type) {
			case 'chart_selecte_point':
				let item;
				this.routeData.forEach((day) => {
					if (item == undefined) {
						item = day.routes.find((x) => x.start.time <= event.data.time && event.data.time <= x.end.time);
						if (item) {
							// if(this.currentPopup)this.currentPopup.remove();
							this.timeItemSelect = item.start.time;
							this.cdr.detectChanges();
							let element = document.getElementById(this.timeItemSelect.toString());
							if (element) {
								element.scrollIntoView({
									behavior: 'smooth',
									block: 'center',
									inline: 'nearest',
								});
							}
						}
					}
				});
				break;
		}
	}

	dateRouteChange(data) {
		if (this.startDate == data.startDate.format('YYYY-MM-DD HH:mm:ss') &&
		this.endDate == data.endDate.format('YYYY-MM-DD HH:mm:ss')) return;
		this.startDate = data.startDate.format('YYYY-MM-DD HH:mm:ss');
		this.endDate = data.endDate.format('YYYY-MM-DD HH:mm:ss');
		this.search();
		if (data.data.maxDate.format('YYYY-MM-DD') == data.data.endDate.format('YYYY-MM-DD')) {
			this.allowNext = false;
		} else this.allowNext = true;
		if (data.data.minDate !== false && data.data.minDate.format('YYYY-MM-DD') == data.data.startDate.format('YYYY-MM-DD')) {
			this.allowPrevious = false;
		} else this.allowPrevious = true;
	}

	changeItem(data) {
		switch (data.type) {
			case 'change_item_tracking':
				this.item = data.data;
				if (!this.isClosed && !this.destroy) {
					this.search(true);
					this.mapService.eventChange.emit({
						type: 'tracking_route',
						options: {
							startTime: this.startDate,
							endTime: this.endDate,
						},
					});
				}

				break;
			case 'change_tab':
				this.mapService.eventChange.emit({ type: 'change_tab', data: null });
				if (data.data == 'route') {
					if (this.featureLayer && !this.destroy && this.isClosed) {
						if (this.featureLayer.getLayers().length > 0) {
							this.featureLayer.addTo(this.mapServiceUtil.map);
							// this.mapUtil.fitBound(this.featureLayer, this.mapServiceUtil.map);
						}
					}
					this.isClosed = false;
					this.subscribeEventMapService();
					this.mapService.eventChange.emit({
						type: 'tracking_route',
						options: {
							startTime: this.startDate,
							endTime: this.endDate,
						},
					});

					// set view center
					this.mapUtil.fitBound(
						this.featureLayer,
						this.mapServiceUtil.map,
						this.getOptionFitBound()
					);
					this.timeItemSelect = undefined;
					if (this.currentPopup) this.currentPopup.remove();
				} else {
					this.isClosed = true;
					this.mapService.eventChange.emit({
						type: 'tracking_route_hide',
						options: {
							startTime: this.startDate,
							endTime: this.endDate,
						},
					});
					if (this.featureLayer) this.featureLayer.remove();
					if (this.subscriptionMapServiceUtil)
						this.subscriptionMapServiceUtil.unsubscribe();
				}
				break;
		}
	}

	search(important?: boolean) {
		let _this = this;
		if (this.item == undefined) return;
		if (this.featureLayer) this.featureLayer.remove();
		this.featureLayer = new L.FeatureGroup();
		this.summary = {
			distance: 0,
			driving: 0,
			stop: 0,
		};

		if (
			this.startDate != undefined &&
			this.endDate != undefined &&
			(this.startDate != this.searchOld.startDate ||
				this.endDate != this.searchOld.endDate ||
				important)
		) {
			this.searchOld.startDate = this.startDate;
			this.searchOld.endDate = this.endDate;
			this.routeLoading = true;
			this.routeData = [];
			let params = {
				timeFrom: this.startDate,
				timeTo: this.endDate,
				deviceId: this.item.id,
				polyline: 1,
				fullPoint: 1,
			};
			this.mapService.eventChange.emit({
				type: 'route_loading',
				data: true,
			});
			this.mapService
				.playback({ params: params })
				.pipe(
					tap((data) => {
						this.lastData = data.result.routes;
						data.result.device.deviceName = this.item.name;
						data.result['params'] = params;
						this.renderRoute(this.lastData);
						this.mapService.eventChange.emit({
							type: 'device_info',
							data: data.result.device,
						});
						this.mapService.eventChange.emit({
							type: 'playback',
							data: data.result,
						});
					}),
					// takeUntil(new Subject()),
					finalize(() => {
						this.routeLoading = false;
						this.mapService.eventChange.emit({
							type: 'route_loading',
							data: false,
						});
						this.cdr.markForCheck();
						this.cdr.detectChanges();
					})
				)
				.subscribe();
		}
	}

	renderRoute(data) {
		let _this = this;
		let countStop = 1;
		let startPoint;
		let endPoint;
		let stopFirst;
		let stopLast;

		// set startPoint and endPoint
		this.routeData = data.map((day) => {
			let distanceDay = 0;
			day.routes = day.routes.map((item) => {
				// this.mapUtil.createMarker()
				if (startPoint == undefined) {
					startPoint = item;
				}
				endPoint = item;

				// caculator summary
				if (item.type == 'stop') {
					item.index = countStop;
					this.summary.stop =
						this.summary.stop + (item.end.time - item.start.time);
					let markerStop = this.mapUtil.createMarker(
						[item.start.lat, item.start.lng],
						{},
						{
							width: 46,
							heigth: 30,
							color: '#e74c3c',
							mapIconUrl:
								`<svg xmlns="http://www.w3.org/2000/svg" width="44" height="32" viewBox="0 0 42 36" style="fill:{color}">
              <g>
                  <rect x="1" stroke="#fff" stroke-width="1" y="1" width="40" height="28" fill="#e74c3c" rx="5" ry="5">
                  </rect>
                  <text fill="#fff" x="21" y="14" text-anchor="middle" alignment-baseline="central" font-size="16" font-weight="bold">` +
								countStop +
								`</text>        
                  <svg width="10" x="17" y="27" height="10" viewBox="0 0 10 8">
                      <polyline points="0,0 5,7 10,0" stroke="#fff" stroke-width="1"/>
                      <polygon points="0,0 5,7 10,0" fill="{color}" stroke="{color}" stroke-width="0.5"/>
                  </svg>
              </g>
            </svg>`,
						},
						item
					);

					countStop++;

					if (markerStop) {
						if (stopFirst == undefined) stopFirst = markerStop;
						if (stopFirst != undefined) stopLast = markerStop;
						markerStop.on('click', function (p) {
							_this.selectMarker(p);
						});
						this.featureLayer.addLayer(markerStop);
					}
				}
				if (item.type == 'route') {
					distanceDay += parseFloat(item.distance.toFixed(2));
					this.summary.driving =
						this.summary.driving +
						(item.end.time - item.start.time);
					let optionsPolyline: any = {
						data: item,
						weight: 5,
						color: '#65b201',
					};
					let polyline = this.mapServiceUtil.createPolyline(
						item.points,
						optionsPolyline,
						{
							isShow: true,
							options: {
								offset: 3,
							},
						}
					);
					if (polyline) {
						polyline.on('click', function (p) {
							_this.selectPolyline(p);
						});
						this.featureLayer.addLayer(polyline);
					}
				}
				if (item.type == 'lost' && this.allowShowLostSignal) {
					let optionsPolyline: any = {
						data: item,
						weight: 3,
						color: '#1f292f',
						dashArray: [25, 15],
						opacity: 0.6,
					};
					item.points = [item.start, item.end];
					let polyline = this.mapServiceUtil.createPolyline(
						item.points,
						optionsPolyline,
						{
							isShow: true,
							options: {
								offset: 3,
							},
						}
					);
					if (polyline) {
						polyline.on('click', function (p) {
							_this.selectPolylineSignal(p);
						});
						this.featureLayer.addLayer(polyline);
					}
				}
				return item;
			});
			this.summary.distance =
				this.summary.distance + parseFloat(distanceDay.toFixed(2));
			return day;
		});

		this.mapServiceUtil.addMarkers([]);
		if (startPoint && startPoint != endPoint) {
			let markerStart = this.mapUtil.createMarker(
				[startPoint.start.lat, startPoint.start.lng],
				{},
				{
					width: 28,
					heigth: 32,
					color: '#1abb0a',
					// mapIconUrl: `<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
					//   width="{width}px" height="{height}px"
					//   viewBox="0 0 48 48"
					//   style=" fill:{color};"><path stroke="#fff" stroke-width="1"  d="M24,1C15.2,1,6.015,7.988,6,18C5.982,29.981,24,48,24,48s18.019-17.994,18-30 C41.984,8.003,32.8,1,24,1z M24,26c-4.418,0-8-3.582-8-8s3.582-8,8-8s8,3.582,8,8S28.418,26,24,26z"></path></svg>`
					// }
					mapIconUrl: `<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
        height="32px" width="32px"
        viewBox="0 0 560 560"
        style=" fill:{color};">
        <path d="m221.496094 59.035156c-88.855469 0-160.882813 72.027344-160.882813 160.875 0 88.851563 72.027344 160.878906 160.882813 160.878906 88.84375 0 160.871094-72.027343 160.871094-160.878906-.101563-88.808594-72.070313-160.773437-160.871094-160.875zm79.652344 256.167969c-3.390626 2.714844-8.148438 2.929687-11.765626.53125l-67.886718-44.980469-67.898438 44.980469c-3.617187 2.398437-8.378906 2.183594-11.765625-.53125-3.386719-2.722656-4.632812-7.3125-3.070312-11.371094l73.402343-191.171875c1.480469-3.863281 5.1875-6.40625 9.320313-6.40625s7.84375 2.542969 9.328125 6.40625l73.402344 191.171875c1.558594 4.050781.316406 8.648438-3.066406 11.371094zm0 0"/><path d="m168.597656 281.84375 47.378906-31.390625c3.34375-2.214844 7.6875-2.214844 11.03125 0l47.378907 31.390625-52.890625-137.753906zm0 0"/><path d="m221.496094 0c-123.542969 0-224.046875 95.886719-224.046875 213.746094 0 37.792968 11.828125 77.707031 35.152343 118.632812 18.933594 33.21875 45.414063 67.132813 78.703126 100.800782 33.816406 33.9375 70.703124 64.675781 110.191406 91.820312 39.476562-27.144531 76.363281-57.882812 110.183594-91.820312 33.289062-33.667969 59.765624-67.582032 78.703124-100.800782 23.324219-40.925781 35.152344-80.839844 35.152344-118.632812 0-117.863282-100.507812-213.746094-224.039062-213.746094zm0 400.757812c-99.726563 0-180.851563-81.125-180.851563-180.847656 0-99.71875 81.125-180.84375 180.851563-180.84375 99.71875 0 180.84375 81.125 180.84375 180.84375 0 99.722656-81.125 180.847656-180.84375 180.847656zm0 0"/>
        </svg>`,
				},
				startPoint
			);
			if (markerStart) this.featureLayer.addLayer(markerStart);
			if (startPoint.type == 'stop') {
				this.featureLayer.removeLayer(stopFirst);
				markerStart.on('click', function (p) {
					_this.selectMarker(p);
				});
			}
		}
		if (
			endPoint &&
			(startPoint != endPoint ||
				(startPoint == endPoint && startPoint.type != 'stop'))
		) {
			let latLng: any = [endPoint.end.lat, endPoint.end.lng];
			if (endPoint.type == 'stop') {
				latLng = [endPoint.start.lat, endPoint.start.lng];
			}

			let markerEnd = this.mapUtil.createMarker(
				latLng,
				{},
				{
					width: 24,
					heigth: 32,
					color: '#000000',
					mapIconUrl: ` <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
        width="32px" height="{height}px"
        viewBox="0 0 24 24"
        style=" fill:{color};"> 
      <path stroke="#fff" stroke-width="1" d="M 23 0.09375 L 21.5 1.40625 C 21.5 1.40625 19.292969 3.1875 17.09375 3.1875 C 16.09375 3.1875 15.3125 2.804688 14.3125 2.40625 C 13.210938 1.90625 11.898438 1.1875 10 1.1875 C 4.601563 1.1875 1.511719 4.488281 1.3125 4.6875 L 1 5 L 6.1875 19.59375 L 7.65625 23.71875 L 9.53125 23.0625 L 7.78125 18.09375 C 8.589844 17.410156 10.324219 16.09375 12.09375 16.09375 C 13.59375 16.09375 14.511719 16.394531 15.3125 16.59375 C 16.113281 16.792969 16.894531 17.09375 18.09375 17.09375 C 20.492188 17.09375 22.492188 16 22.59375 16 L 23.09375 15.6875 Z M 10.09375 3 C 10.355469 3 10.605469 3.03125 10.84375 3.0625 L 11.6875 6.3125 C 12.585938 6.3125 15.09375 7.5 15.09375 7.5 L 14.375 4.375 C 15.199219 4.730469 16.050781 5 17.1875 5 C 17.652344 5 18.113281 4.941406 18.5625 4.84375 L 18.8125 8 C 18.8125 8 19.851563 7.722656 21.1875 6.875 L 21.1875 11.09375 C 20.078125 11.585938 19.09375 11.8125 19.09375 11.8125 L 19.34375 15.0625 C 18.917969 15.144531 18.476563 15.1875 18 15.1875 C 17.5625 15.1875 17.195313 15.132813 16.84375 15.0625 L 16.09375 11.8125 C 15.09375 11.511719 13.605469 11 12.90625 11 L 13.8125 14.34375 C 13.273438 14.257813 12.675781 14.1875 11.90625 14.1875 C 11.273438 14.1875 10.652344 14.34375 10.0625 14.53125 L 8.8125 11.3125 C 7.789063 11.734375 6.742188 12.507813 6.03125 13.09375 L 4.40625 8.53125 C 5.875 7.410156 7.3125 6.90625 7.3125 6.90625 L 6.03125 3.78125 C 7.132813 3.335938 8.488281 3 10.09375 3 Z M 19.09375 11.8125 L 18.8125 8 C 18.8125 8 18.207031 8.1875 17.40625 8.1875 C 16.304688 8.1875 15.09375 7.59375 15.09375 7.59375 L 16 11.6875 C 16 11.6875 16.894531 11.90625 17.59375 11.90625 C 18.292969 11.90625 19.09375 11.8125 19.09375 11.8125 Z M 11.6875 6.3125 L 10.90625 6.3125 C 8.90625 6.3125 7.1875 7 7.1875 7 L 8.90625 11.3125 C 8.90625 11.3125 10.195313 10.8125 11.09375 10.8125 C 11.992188 10.8125 12.90625 10.90625 12.90625 10.90625 Z"></path></svg>`,
				},
				endPoint
			);

			if (markerEnd) this.featureLayer.addLayer(markerEnd);
			if (endPoint.type == 'stop') {
				if (stopLast) this.featureLayer.removeLayer(stopLast);
				markerEnd.on('click', function (p) {
					_this.selectMarker(p);
				});
			}
		}

		if (this.featureLayer.getLayers().length > 0) {
			this.featureLayer.addTo(this.mapServiceUtil.map);
			this.mapUtil.fitBound(
				this.featureLayer,
				this.mapServiceUtil.map,
				this.getOptionFitBound()
			);
		}
	}

	selectItem(item) {
		switch (item.type) {
			case 'stop':
				// panto point and show popup
				this.mapUtil.setView(
					[item.start.lat, item.start.lng],
					this.mapServiceUtil.map
				);
				if (!item.start.address || item.start.address.length == 0) {
					item.start.addressLoading = true;
					this.cdr.detectChanges();
					this.mapUtilService
						.geocode({
							params: {
								lat: item.start.lat,
								lng: item.start.lng,
							},
						})
						.pipe(
							tap((data) => {
								item.start.address = data.result.address;
								item.start.addressLoading = false;
								if (this.timeItemSelect == item.start.time) {
									this.showPopupMarker(item);
								}
								this.cdr.detectChanges();
							}),
							finalize(this.cdr.markForCheck)
						)
						.subscribe();
				}

				this.showPopupMarker(item);

				break;
			case 'route':
				this.showPolyline(item, true);
				break;
			case 'lost':
				this.showPolylineSignal(item, true);
				break;
		}
	}

	selectMarker(marker) {
		this.selectItem(marker.target.options.data);
		// this.showPopupMarker(marker.target.options.data);
	}

	showPopupMarker(item) {
		this.timeItemSelect = item.start.time;
		if (this.currentPopup) this.featureLayer.removeLayer(this.currentPopup);
		// this.currentPopup.remove();

		let dataTem = {
			type: PLAYBACK_STOP,
			data: {
				index: item.index,
				date: this.userDateAdvPipe.transform(item.start.time),
				startTime: this.userDateAdvPipe.transform(
					item.start.time,
					'HH:mm'
				),
				endTime: this.userDateAdvPipe.transform(item.end.time, 'HH:mm'),
				duration: this.userDateAdvPipe.transform(
					item.start.time,
					'fromNowDateTimeShort',
					{ valueOffset: 0, now: item.end.time, nowOffset: 0 }
				),
			},
		};
		Object.assign(dataTem.data, item.start);

		let popup = this.mapServiceUtil.createPopupCustom(
			[item.start.lat, item.start.lng],
			{},
			{
				width: 32,
				heigth: 38,
			},
			dataTem
		);
		if (popup) {
			this.currentPopup = popup;
			this.featureLayer.addLayer(this.currentPopup);
		}
		this.cdr.detectChanges();
		let element = document.getElementById(this.timeItemSelect.toString());
		if (element) {
			element.scrollIntoView({
				behavior: 'smooth',
				block: 'center',
				inline: 'nearest',
			});
		}
	}

	selectPolyline(polyline) {
		this.selectItem(polyline.target.options.data);
		// this.showPolyline(polyline.target.options.data);
	}

	showPolyline(item, fitBound?: boolean) {
		let _this = this;
		this.timeItemSelect = item.start.time;
		if (this.currentPopup) this.featureLayer.removeLayer(this.currentPopup);
		let polyline = this.mapServiceUtil.createPolyline(
			item.points,
			{
				// color: "#65b201",
				color: '#0095F6',
				weight: 5,
				opacity: 1,
				// dashArray: [12,12],
			},
			{
				isShow: true,
				options: {
					offset: 3,
				},
			}
		);
		if (polyline) {
			this.currentPopup = polyline;
			this.featureLayer.addLayer(polyline);

			if (fitBound === true) {
				this.mapUtil.fitBounds(
					polyline.getBounds(),
					this.mapServiceUtil.map,
					this.getOptionFitBound({
						maxZoom: this.mapServiceUtil.map.getMaxZoom(),
					})
				);
			}
		}
		this.cdr.detectChanges();
		let element = document.getElementById(this.timeItemSelect.toString());
		if (element) {
			element.scrollIntoView({
				behavior: 'smooth',
				block: 'center',
				inline: 'nearest',
			});
		}
	}

	selectPolylineSignal(polyline) {
		this.showPolylineSignal(polyline.target.options.data);
	}

	showPolylineSignal(item, fitBound?: boolean) {
		let _this = this;
		this.timeItemSelect = item.start.time;
		if (this.currentPopup) this.featureLayer.removeLayer(this.currentPopup);
		//  this.currentPopup.remove();

		let polyline = this.mapServiceUtil.createPolyline(
			item.points,
			{
				color: '#1f292f',
				weight: 3,
				opacity: 0.5,
			},
			{
				isShow: true,
				options: {
					offset: 3,
				},
			}
		);
		if (polyline) {
			this.currentPopup = polyline;
			this.featureLayer.addLayer(polyline);
			if (fitBound === true) {
				this.mapUtil.fitBounds(
					polyline.getBounds(),
					this.mapServiceUtil.map,
					this.getOptionFitBound({
						maxZoom: this.mapServiceUtil.map.getMaxZoom(),
					})
				);
			}
		}
		this.cdr.detectChanges();
		let element = document.getElementById(this.timeItemSelect.toString());
		if (element) {
			element.scrollIntoView({
				behavior: 'smooth',
				block: 'center',
				inline: 'nearest',
			});
		}
	}
	getOptionFitBound(options?: any) {
		let option: any = {};
		let cacheIsShow = localStorage.getItem(KEY_CACHE_CHART);
		if (cacheIsShow == '1') option['paddingBottomRight'] = [50, 290];
		if (options != undefined) option = Object.assign(option, options);
		return option;
	}

	previousDate() {
		this.datePicker.previousDay();
	}

	nextDate() {
		this.datePicker.nextDay();
	}

	onResize(element) { }
	isShow(path: string, deep?: boolean) {
		let obj = objectPath.get(this.layoutMapConfig, path);
		if (deep && obj) {
			if (Object.values(obj).some((x) => x === true)) return true;
			else return false;
		}
		return objectPath.get(this.layoutMapConfig, path);
	}
	exportFile(type: string) {
		let data = this.processDataExport();
		switch (type) {
			case 'xlsx':
				this.exportFileXLSX(
					data.data,
					data.timeStart,
					data.timeEnd,
					data.deviceName,
					data.totalAll
				);
				break;
			case 'pdf':
				this.exportFilePDF(
					data.data,
					data.timeStart,
					data.timeEnd,
					data.deviceName,
					data.totalAll
				);
				break;
		}
	}
	processDataExport() {
		let totalAll = {
			count: 0,
			distance: 0,
			duration: 0,
			avgspeed: 0,
			maxSpeed: 0,
		};
		let dataProcess = [...this.routeData];
		let totalAllAvgSpeedRoute = 0;
		let totalAllAvgSpeedCount = 0;
		// process data
		dataProcess = dataProcess.map((day) => {
			let dayResult = {
				timeFrom: day.timeFrom,
				timeEnd: day.timeEnd,
				data: [],
				timeFromUtc: this.userDateAdvPipe.transform(
					day.timeEnd,
					'date'
				), //"09/03/2020",
				countRowData: 0,
				totalDuration: 0,
				maxSpeed: 0,
				totalDistance: 0,
				totalavgspeed: 0,
				totalDurationInt: 0,
			};
			let count = 0;
			// let totalSpeedDay = 0;
			dayResult.data = day.routes.map((route) => {
				let pointStart = route.points[0];
				let pointEnd = route.points[route.points.length - 1];

				let routeResult = {
					timeFromUtc: this.userDateAdvPipe.transform(
						pointStart.time,
						'YYYY-MM-DD HH:mm:ss'
					),
					timeEndUtc: this.userDateAdvPipe.transform(
						pointEnd.time,
						'YYYY-MM-DD HH:mm:ss'
					),
					deviceId: 1,
					type: route.type,
					from: pointStart.time,
					fromUtc: this.userDateAdvPipe.transform(
						pointStart.time,
						'YYYY-MM-DD HH:mm:ss'
					),
					to: pointEnd.time,
					toUtc: this.userDateAdvPipe.transform(
						pointEnd.time,
						'YYYY-MM-DD HH:mm:ss'
					),
					status: 'Xe chạy 0.5084441860016056 KM ',
					startAddress: pointStart.address,
					stopAddress: pointEnd.address,
					startlatitude: pointStart.lat,
					stoplatitude: pointStart.lng,
					startlongitude: pointEnd.lat,
					stoplongitude: pointEnd.lng,
					duration: pointEnd.time - pointStart.time,
					distance: route.distance
						? parseFloat(route.distance.toFixed(2))
						: 0,
					avgspeed: 0,
					maxspeed: 0,
					startAddressStr: this.userDateAdvPipe.transform(
						pointStart.time,
						'HH:mm:ss'
					),
					stopAddressStr: this.userDateAdvPipe.transform(
						pointEnd.time,
						'HH:mm:ss'
					),
					//'fromNowDateTimeShort':{valueOffset:0,now:item.end.time, nowOffset:0}
					//{{item.start.time | userDateAdv:'fromNowDateTimeShort':{valueOffset:0,now:item.end.time, nowOffset:0} }}
					durationTime: this.userDateAdvPipe.transform(
						'2000-01-01 00:00:00',
						'HH:mm:ss',
						{
							valueOffset: pointEnd.time - pointStart.time,
						}
					),
					count: count++,
				};
				let totalSpeed = 0;
				let avgSpeed = 0;
				let maxSpeed = 0;
				let time = pointEnd.time - pointStart.time;

				if (route.type == 'route') {
					route.points.forEach((p) => {
						// totalSpeed += p.speed;
						if (p.speed > maxSpeed) maxSpeed = p.speed;
					});
					avgSpeed = parseFloat(
						((route.distance * 3600) / time).toFixed(2)
					);
					// avgSpeed = parseFloat((totalSpeed / route.points.length).toFixed(2));
					// console.log( `date: ${pointStart.time} - ${pointEnd.time} _ totalSpeed: ${totalSpeed}, _ length: ${route.points.length} _ avgSpeed: ${avgSpeed}`);
					// totalSpeedDay += avgSpeed;
					dayResult.countRowData++;
					dayResult.totalDurationInt += routeResult.duration;
					totalAllAvgSpeedRoute += avgSpeed;
					totalAllAvgSpeedCount++;
				}
				routeResult.avgspeed = avgSpeed;
				routeResult.maxspeed = maxSpeed;
				dayResult.totalDistance += routeResult.distance;

				if (routeResult.maxspeed > dayResult.maxSpeed)
					dayResult.maxSpeed = routeResult.maxspeed;
				return routeResult;
			});
			dayResult.totalDuration = this.userDateAdvPipe.transform(
				'2000-01-01 00:00:00',
				'HH:mm:ss',
				{
					valueOffset: dayResult.totalDurationInt,
				}
			);
			dayResult.totalavgspeed =
				dayResult.totalDurationInt == 0
					? 0
					: parseFloat(
						(
							(dayResult.totalDistance * 3600) /
							dayResult.totalDurationInt
						).toFixed(2)
					);
			totalAll.count += dayResult.countRowData;
			totalAll.distance += dayResult.totalDistance;
			totalAll.duration += dayResult.totalDurationInt;
			if (dayResult.maxSpeed > totalAll.maxSpeed)
				totalAll.maxSpeed = dayResult.maxSpeed;
			dayResult.totalDistance = parseFloat(
				dayResult.totalDistance.toFixed(2)
			);
			return dayResult;
		});
		totalAll.avgspeed =
			totalAll.duration == 0
				? 0
				: parseFloat(
					(
						(totalAll.distance * 3600) /
						totalAll.duration
					).toFixed(2)
				);
		totalAll.duration = this.userDateAdvPipe.transform(
			'2000-01-01 00:00:00',
			'HH:mm:ss',
			{
				valueOffset: totalAll.duration,
			}
		);
		let dataResult = {
			data: dataProcess,
			timeStart: '',
			timeEnd: '',
			deviceName: this.item.name,
			totalAll: {},
		};
		dataResult.totalAll = totalAll;
		if (dataProcess.length > 0) {
			dataResult.timeStart = this.userDateAdvPipe.transform(
				dataProcess[0].timeFrom,
				'datetime'
			);
			dataResult.timeEnd = this.userDateAdvPipe.transform(
				dataProcess[dataProcess.length - 1].timeEnd,
				'datetime'
			);
		}
		return dataResult;
	}

	exportFilePDF(data, dateStart, dateEnd, deviceName, totalAllRowData: any) {
		let config: PdfModel = {
			info: {
				title: 'ReportRoute_export',
			},
			content: {
				header: {
					title: this.translate.instant(
						'REPORT.ROUTE.EXCEL.REPORT_ROUTE'
					),
					params: {
						timeZone:
							': ' +
							this.currentUserService.currentTimeZone.timeZone +
							' ' +
							this.currentUserService.currentTimeZone.GMTOffset,
						timeStart: ': ' + dateStart,
						timeEnd: ': ' + dateEnd,
						deviceName: ': ' + deviceName,
					},
				},
				table: [
					{
						woorksheet: {
							name: deviceName,
						},
						total: {
							group: [{ columnData: 'timeFromUtc' }],
							totalCol: [
								{
									text: this.translate.instant(
										'REPORT.ROUTE.COLUMN.TOTAL_MOVE'
									),
									style: {
										colSpan: 5,
										alignment: 'right',
										bold: true,
										fillColor: '#fff',
									},
								},
								{
									text: 'Tổng',
									colSpan: 5,
									alignment: 'right',
									bold: true,
									fillColor: '#fff',
								},
								{
									text: 'Tổng',
									colSpan: 5,
									alignment: 'right',
									bold: true,
									fillColor: '#fff',
								},
								{
									text: 'Tổng',
									colSpan: 5,
									alignment: 'right',
									bold: true,
									fillColor: '#fff',
								},
								{
									text: 'Tổng',
									colSpan: 5,
									alignment: 'right',
									bold: true,
									fillColor: '#fff',
								},
								{
									columnData: 'totalDistance',
									style: {
										alignment: 'right',
										bold: true,
										fillColor: '#fff',
									},
								},
								{
									columnData: 'totalDuration',
									style: {
										alignment: 'right',
										bold: true,
										fillColor: '#fff',
									},
								},
								{
									columnData: 'totalavgspeed',
									style: {
										alignment: 'right',
										bold: true,
										fillColor: '#fff',
									},
								},
								{
									columnData: 'maxSpeed',
									style: {
										alignment: 'right',
										bold: true,
										fillColor: '#fff',
									},
								},
							],
							totalAll: {
								title: [
									{
										text: this.translate.instant(
											'MAP.TRACKING.ROUTE.SUMMARY'
										),
										alignment: 'left',
										bold: true,
										fillColor: '#fff',
										fontSize: 20,
										margin: [0, 20, 0, 5],
									},
								],
								content: [
									[
										{
											text: this.translate.instant(
												'REPORT.ROUTE.COLUMN.TRIPS'
											),
											fillColor: '#EEEEEE',
										},
										{ text: totalAllRowData.count },
									],
									[
										{
											text: this.translate.instant(
												'REPORT.ROUTE.COLUMN.TOTAL_DISTANCE'
											),
											fillColor: '#EEEEEE',
										},
										{ text: totalAllRowData.distance },
									],
									[
										{
											text: this.translate.instant(
												'REPORT.ROUTE.COLUMN.TIME_DURATION'
											),
											fillColor: '#EEEEEE',
										},
										{ text: totalAllRowData.duration },
									],
									[
										{
											text: this.translate.instant(
												'REPORT.ROUTE.COLUMN.AVERAGE_SPEED'
											),
											fillColor: '#EEEEEE',
										},
										{ text: totalAllRowData.avgspeed },
									],
									[
										{
											text: this.translate.instant(
												'REPORT.ROUTE.COLUMN.MAX_SPEED'
											),
											fillColor: '#EEEEEE',
										},
										{ text: totalAllRowData.maxSpeed },
									],
								],
							},
						},
						headerRows: 2,
						widths: [5, 35, 100, 35, 100, 42, 42, 42, 42],
						header: [
							[
								{
									text: '#',
									rowSpan: 2,
									alignment: 'center',
									fillColor: '#EEEEEE',
									bold: true,
									margin: [0, 10, 0, 0],
								},
								{
									text: this.translate.instant(
										'REPORT.ROUTE.COLUMN.MOVE_START'
									),
									colSpan: 2,
									alignment: 'center',
									fillColor: '#EEEEEE',
									bold: true,
								},
								{
									text: this.translate.instant(
										'REPORT.ROUTE.COLUMN.MOVE_START'
									),
									colSpan: 2,
									alignment: 'center',
									fillColor: '#EEEEEE',
									bold: true,
								},
								{
									text: this.translate.instant(
										'REPORT.ROUTE.COLUMN.MOVE_END'
									),
									colSpan: 2,
									alignment: 'center',
									fillColor: '#EEEEEE',
									bold: true,
								},
								{
									text: this.translate.instant(
										'REPORT.ROUTE.COLUMN.MOVE_END'
									),
									colSpan: 2,
									alignment: 'center',
									fillColor: '#EEEEEE',
									bold: true,
								},
								{
									text: this.translate.instant(
										'REPORT.ROUTE.COLUMN.TOTAL_DISTANCE'
									),
									rowSpan: 2,
									alignment: 'center',
									fillColor: '#EEEEEE',
									bold: true,
									margin: [0, 6, 0, 0],
								},
								{
									text: this.translate.instant(
										'REPORT.ROUTE.COLUMN.TIME_DURATION'
									),
									rowSpan: 2,
									alignment: 'center',
									fillColor: '#EEEEEE',
									margin: [0, 6, 0, 0],
									bold: true,
								},
								{
									text: this.translate.instant(
										'REPORT.ROUTE.COLUMN.AVERAGE_SPEED'
									),
									rowSpan: 2,
									alignment: 'center',
									fillColor: '#EEEEEE',
									margin: [0, 6, 0, 0],
									bold: true,
								},
								{
									text: this.translate.instant(
										'REPORT.ROUTE.COLUMN.MAX_SPEED'
									),
									rowSpan: 2,
									alignment: 'center',
									fillColor: '#EEEEEE',
									margin: [0, 6, 0, 0],
									bold: true,
								},
							],
							[
								'',
								{
									text: this.translate.instant(
										'MANAGE.ALERT_RULE.NOTIFICATION.TIME'
									),
									alignment: 'center',
									fillColor: '#EEEEEE',
									bold: true,
									margin: [0, 4, 0, 0],
								},
								{
									text: this.translate.instant(
										'COMMON.COLUMN.ADDRESS'
									),
									alignment: 'center',
									fillColor: '#EEEEEE',
									bold: true,
									margin: [0, 4, 0, 0],
								},
								{
									text: this.translate.instant(
										'MANAGE.ALERT_RULE.NOTIFICATION.TIME'
									),
									alignment: 'center',
									fillColor: '#EEEEEE',
									bold: true,
									margin: [0, 4, 0, 0],
								},
								{
									text: this.translate.instant(
										'COMMON.COLUMN.ADDRESS'
									),
									alignment: 'center',
									fillColor: '#EEEEEE',
									bold: true,
									margin: [0, 4, 0, 0],
								},
								'',
								'',
								'',
								'',
							],
						],
						body: [
							{ auto: true },
							{
								columnData: 'startAddressStr',
								style: {
									alignment: 'center',
									fillColor: '#fff',
									bold: false,
								},
							},
							{
								columnData: 'startAddress',
								style: {
									alignment: 'left',
									fillColor: '#fff',
									bold: false,
								},
							},
							{
								columnData: 'stopAddressStr',
								style: {
									alignment: 'center',
									fillColor: '#fff',
									bold: false,
								},
							},
							{
								columnData: 'stopAddress',
								style: {
									alignment: 'left',
									fillColor: '#fff',
									bold: false,
								},
							},
							{
								columnData: 'distance',
								style: {
									alignment: 'right',
									fillColor: '#fff',
									bold: false,
								},
							},
							{
								columnData: 'durationTime',
								style: {
									alignment: 'right',
									fillColor: '#fff',
									bold: false,
								},
							},
							{
								columnData: 'avgspeed',
								style: {
									alignment: 'right',
									fillColor: '#fff',
									bold: false,
								},
							},
							{
								columnData: 'maxspeed',
								style: {
									alignment: 'right',
									fillColor: '#fff',
									bold: false,
								},
							},
						],
					},
				],
			},
			footer: {
				title: dateStart + ' - ' + dateEnd + ', ' + deviceName,
			},
		};
		this.pdf.ExportPdf([data], config);
		// this.xlsx.exportFileTest([data], config, {});
	}

	private exportFileXLSX(
		data,
		dateStart,
		dateEnd,
		deviceName,
		totalAllRowData: {}
	) {
		let dateTimeFormat =
			this.currentUserService.dateFormat +
			' ' +
			this.currentUserService.timeFormat;
		let config: XLSXModelTest = {
			file: {
				title: this.translate.instant(
					'REPORT.ROUTE.GENERAL.REPORT_DEVICE'
				),
				prefixFileName: 'ReportRoute',
			},
			template: [
				{
					header: [
						{
							text: this.translate.instant(
								'REPORT.ROUTE.EXCEL.REPORT_ROUTE'
							),
							type: 'header',
						},
						{
							text: '',
						},
						{
							text: '!timezone',
						},

						{
							text: '',
						},

						{
							text:
								this.translate.instant(
									'REPORT.ROUTE.GENERAL.DATE_START'
								) +
								': ' +
								this.datepipe.transform(
									dateStart,
									dateTimeFormat
								),
						},
						{
							text:
								this.translate.instant(
									'REPORT.ROUTE.GENERAL.DATE_END'
								) +
								': ' +
								this.datepipe.transform(
									dateEnd,
									dateTimeFormat
								),
						},
						{
							text:
								this.translate.instant(
									'MANAGE.DEVICE.GENERAL.DEVICE_NAME'
								) +
								': ' +
								deviceName,
						},

						{
							text: '',
						},
					],
					columns: [
						{
							name: '#',
							columnData: 'auto',
							wch: 5,
							style: {
								alignment: {
									vertical: 'center',
									horizontal: 'center',
									indent: 0,
									wrapText: true,
								},
							},
						},
						{
							name: this.translate.instant(
								'REPORT.ROUTE.COLUMN.MOVE_START'
							),
							columnData: 'startAddressStr',
							style: {
								alignment: {
									vertical: 'center',
									horizontal: 'center',
									indent: 0,
									wrapText: true,
								},
							},
							type: 'mergeTime',
							wch: 10,
						},
						{
							name: this.translate.instant(
								'REPORT.ROUTE.COLUMN.MOVE_START'
							),
							columnData: 'startAddress',
							style: {
								alignment: {
									vertical: 'center',
									horizontal: 'left',
									indent: 0,
									wrapText: true,
								},
							},
							type: 'mergeTime',
							wch: 40,
						},
						{
							name: this.translate.instant(
								'REPORT.ROUTE.COLUMN.MOVE_END'
							),
							columnData: 'stopAddressStr',
							style: {
								alignment: {
									vertical: 'center',
									horizontal: 'center',
									indent: 0,
									wrapText: true,
								},
							},
							type: 'mergeTime',
							wch: 10,
						},
						{
							name: this.translate.instant(
								'REPORT.ROUTE.COLUMN.MOVE_END'
							),
							columnData: 'stopAddress',
							type: 'mergeTime',
							style: {
								alignment: {
									vertical: 'center',
									horizontal: 'left',
									indent: 0,
									wrapText: true,
								},
							},

							wch: 40,
						},
						{
							name: this.translate.instant(
								'REPORT.ROUTE.COLUMN.TIME_DURATION'
							),
							columnData: 'duration',
							type: 'time',
							style: {
								alignment: {
									vertical: 'center',
									horizontal: 'right',
									indent: 0,
									wrapText: true,
								},
							},
							wch: 20,
						},
						{
							name: this.translate.instant(
								'REPORT.ROUTE.COLUMN.AVERAGE_SPEED'
							),
							columnData: 'avgspeed',
							style: {
								alignment: {
									vertical: 'center',
									horizontal: 'right',
									indent: 0,
									wrapText: true,
								},
							},
							wch: 20,
						},
						{
							name: this.translate.instant(
								'REPORT.ROUTE.COLUMN.MAX_SPEED'
							),
							columnData: 'maxspeed',
							style: {
								alignment: {
									vertical: 'center',
									horizontal: 'right',
									indent: 0,
									wrapText: true,
								},
							},

							wch: 20,
						},
						{
							name: this.translate.instant(
								'REPORT.ROUTE.COLUMN.TOTAL_DISTANCE'
							),
							columnData: 'distance',
							style: {
								alignment: {
									vertical: 'center',
									horizontal: 'right',
									indent: 0,
									wrapText: true,
								},
							},
							wch: 20,
						},
					],
					columnsMerge: {
						data: [
							'',
							this.translate.instant(
								'MANAGE.ALERT_RULE.NOTIFICATION.TIME'
							),
							this.translate.instant('COMMON.COLUMN.ADDRESS'),
							this.translate.instant(
								'MANAGE.ALERT_RULE.NOTIFICATION.TIME'
							),
							this.translate.instant('COMMON.COLUMN.ADDRESS'),
							'',
							'',
							'',
							'',
						],
						position: [
							'A9:A10',
							'B9:C9',
							'D9:E9',
							'F9:F10',
							'G9:G10',
							'H9:H10',
							'I9:I10',
						],
					},
					woorksheet: {
						name: deviceName,
					},
					total: {
						group: 'timeFromUtc',
						totalCol: {
							field: [
								{
									text: this.translate.instant(
										'REPORT.ROUTE.COLUMN.TOTAL_MOVE'
									),
									merge: {
										full: false,
										range: '4',
									},
								},
								{
									text: ' ',
								},
								{
									text: ' ',
								},
								{
									text: ' ',
								},
								{
									text: ' ',
								},
								{
									columnData: 'totalDuration',
									style: {
										font: {
											name: 'Times New Roman',
											sz: 14,
											bold: true,
										},
										alignment: {
											vertical: 'center',
											horizontal: 'right',
											indent: 0,
											wrapText: true,
										},
									},
								},
								{
									columnData: 'totalavgspeed',
									style: {
										font: {
											name: 'Times New Roman',
											sz: 14,
											bold: true,
										},
										alignment: {
											vertical: 'center',
											horizontal: 'right',
											indent: 0,
											wrapText: true,
										},
									},
								},
								{
									columnData: 'maxSpeed',
									style: {
										font: {
											name: 'Times New Roman',
											sz: 14,
											bold: true,
										},
										alignment: {
											vertical: 'center',
											horizontal: 'right',
											indent: 0,
											wrapText: true,
										},
									},
								},
								{
									columnData: 'totalDistance',
									style: {
										font: {
											name: 'Times New Roman',
											sz: 14,
											bold: true,
										},
										alignment: {
											vertical: 'center',
											horizontal: 'right',
											indent: 0,
											wrapText: true,
										},
									},
								},
								// {
								//   columnData: "totalDuration",
								//   style: {
								//     font: { name: 'Times New Roman', sz: 14, bold: true },
								//     alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
								//   }
								// },
								// {
								//   columnData: "totalavgspeed",
								//   style: {
								//     font: { name: 'Times New Roman', sz: 14, bold: true },
								//     alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
								//   }
								// },
								// {
								//   columnData: "maxSpeed",
								//   style: {
								//     font: { name: 'Times New Roman', sz: 14, bold: true },
								//     alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
								//   }
								// }
							],
						},
						totalAll: {
							position: {
								top: 2,
								left: 7,
							},
							data: [
								{
									key: this.translate.instant(
										'REPORT.ROUTE.COLUMN.TRIPS'
									),
									value: totalAllRowData['count'],
									style: {
										alignment: {
											vertical: 'center',
											horizontal: 'left',
											wrapText: true,
										},
									},
								},
								{
									key: this.translate.instant(
										'REPORT.ROUTE.COLUMN.TOTAL_DISTANCE'
									),
									value: totalAllRowData['distance'],
									style: {
										alignment: {
											vertical: 'center',
											horizontal: 'left',
											wrapText: true,
										},
									},
								},
								{
									key: this.translate.instant(
										'REPORT.ROUTE.COLUMN.TIME_DURATION'
									),
									value: totalAllRowData['duration'],
									style: {
										alignment: {
											vertical: 'center',
											horizontal: 'left',
											wrapText: true,
										},
									},
								},
								{
									key: this.translate.instant(
										'REPORT.ROUTE.COLUMN.AVERAGE_SPEED'
									),
									value: totalAllRowData['avgspeed'],
									style: {
										alignment: {
											vertical: 'center',
											horizontal: 'left',
											wrapText: true,
										},
									},
								},
								{
									key: this.translate.instant(
										'REPORT.ROUTE.COLUMN.MAX_SPEED'
									),
									value: totalAllRowData['maxSpeed'],
									style: {
										alignment: {
											vertical: 'center',
											horizontal: 'left',
											wrapText: true,
										},
									},
								},
							],
						},
					},
				},
			],
		};
		this.xlsxService.exportFileTest([data], config, {});
	}
}
