import { CurrentUserService } from './../../../../../core/auth/_services/current-user.service';
import { TranslateService } from '@ngx-translate/core';
import { DeviceConfigService } from './../../../../../core/common/_services/device-config.service';
import { DeviceService } from './../../../../../core/manage/_services/device.service';
import { Component, OnInit, ChangeDetectorRef, Input, EventEmitter, OnDestroy, Output, ViewChild, ElementRef } from '@angular/core';
import { TrackingUtil, TrackingService } from '@core/map';
import { tap, finalize, takeUntil, map } from 'rxjs/operators';
import { MapService as MapServiceUtil, MapService, MapUtilityService, MapConfigService, LayoutMapConfigModel } from '@core/utils/map';
import { interval, Subject, Subscription } from 'rxjs';
import { isNull } from 'util';
import { UserDateAdvPipe, LayoutConfigModel } from '@core/_base/layout';
import objectPath from 'object-path';
import { TypeSensor, SensorService, CameraService } from '@core/manage';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';


@Component({
  selector: 'kt-tracking-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
  providers: [UserDateAdvPipe]
})
export class InfoComponent implements OnInit, OnDestroy {
  @Input('mapService') mapService: MapService;
  @Input('item') item: any;
  @Input('isUserLogin') isUserLogin: boolean;
  @Input('itemChange') itemChange: EventEmitter<any>;
  @Output('eventChange') eventChange: EventEmitter<{ type: string, data: any, options?: any }>;
  private isClosed: boolean;
  private destroy: boolean;
  private intervalRefresh;
  private timeRefresh: number; // second
  private subscription: Subscription;
  private unsubscribe: Subscription[] = [];
  private layoutMapConfig: LayoutMapConfigModel;
  public typeSensors: Array<TypeSensor>;
  public sendCommandEmitter: EventEmitter<any>;
  public isShowCommand: boolean;
  public currentUser: any;
  private turnOnCmd: any;
  private turnOffCmd: any;
  public turnOn = 'TURN_ON_ENGINE';
  public turnOff = 'TURN_OFF_ENGINE';
  @ViewChild('content', { static: true }) content: ElementRef;
  closeResult: string;
  command: any;
  constructor(private trackingUtil: TrackingUtil,
    private trackingService: TrackingService,
    private mapUtilService: MapUtilityService,
    private cdr: ChangeDetectorRef,
    private userDatePipeAdv: UserDateAdvPipe,
    private mapConfigService: MapConfigService,
    private sensorService: SensorService,
    private deviceService: DeviceService,
    private deviceConfigService: DeviceConfigService,
    private modalService: NgbModal,
    private translete: TranslateService,
    private cameraService: CameraService,
    public sanitizer: DomSanitizer) {
    this.isClosed = false;
    this.timeRefresh = 10 * 1000;
    this.eventChange = new EventEmitter();
    this.layoutMapConfig = this.mapConfigService.getLayoutConfig();
    const subscri = this.mapConfigService.layoutConfigUpdate$.subscribe(layoutConfig => {
      this.layoutMapConfig = layoutConfig;
    });
    this.unsubscribe.push(subscri);
    this.typeSensors = this.sensorService.getAllTypeSensor();
  }

  ngOnInit() {
    this.getCamera();
    this.sendCommandEmitter = new EventEmitter();    
    this.deviceConfigService.get().then(data => {
      let deviceType = data.deviceTypes.find(type => { return type.id == this.item.type });
      this.turnOffCmd = deviceType.command.find(command => {
        return command.name == 'TURN_OFF_ENGINE'
      })
      this.turnOnCmd = deviceType.command.find(command => {
        return command.name == 'TURN_ON_ENGINE'
      })
      if (this.turnOffCmd && this.turnOnCmd) {
        this.isShowCommand = true;

      } else {
        this.isShowCommand = false;
      }
    })
    if (this.itemChange != undefined) {
      this.subscription = this.itemChange.pipe(
        tap(data => {
          this.processEvent(data);
        })
      ).subscribe();
    }
    this.loadInfo(this.item);
    if (this.intervalRefresh) clearInterval(this.intervalRefresh);
    let _this = this;
    this.intervalRefresh = setInterval(() => {
      _this.refresh(_this.item)
    }, _this.timeRefresh);    
  }
  ngOnDestroy() {
    this.isClosed = true;
    this.destroy = true;
    if (this.intervalRefresh) clearInterval(this.intervalRefresh);
    if (this.subscription) this.subscription.unsubscribe();
    this.mapService.clearMap();
    this.unsubscribe.forEach(s => s.unsubscribe());
  }
  
  processEvent(data) {
    switch (data.type) {
      case "change_item_tracking":
        if (!this.isClosed) {
          this.loadInfo(data.data);
          if (this.intervalRefresh) clearInterval(this.intervalRefresh);
          let _this = this;
          this.intervalRefresh = setInterval(() => {
            _this.refresh(data.data)
          }, _this.timeRefresh);
        }
        break;
      case "change_tab":
        if (data.data == "now") {
          this.isClosed = false;
          if (this.intervalRefresh) clearInterval(this.intervalRefresh);
          let _this = this;
          this.loadInfo(this.item);
          this.intervalRefresh = setInterval(() => {
            _this.refresh(_this.item);
          }, _this.timeRefresh);
        }
        else {
          this.isClosed = true;
          if (this.intervalRefresh) clearInterval(this.intervalRefresh);
          this.mapService.clearMap();
        }
        break;
    }

  }
  loadInfo(item) {
    if (item.id > 0) {
      this.trackingService.detail({ params: { deviceId: item.id } }).pipe(
        tap(data => {

          let items = this.trackingUtil.processItems([data.result], data.datetime);
          let item = JSON.parse(JSON.stringify(items[0]));
          if ((!item.address || item.address.length == 0) && this.item.address && this.item.address.length > 0
            && item.lat == this.item.lat && item.lng == this.item.lng) {
            items[0].address = this.item.address;
            item.address = this.item.address;
          }
          this.item = items[0];
          this.mapService.addMarkers([item], true);
          this.mapService.follow(item);
          this.getGeocode(this.item);
          // process  icon 
          if (this.item && this.item.sensors) {
            this.item.sensors = this.item.sensors.map(x => {
              let type = this.typeSensors.find(sensor => sensor.type == x.type);
              if (type) x.icon = type.icon;
              return x;
            });
          }
          this.cdr.detectChanges();
        }),
        finalize(this.cdr.markForCheck)
      ).subscribe();
    }
  }
  refresh(item) {
    if (item.id > 0) {
      this.trackingService.detail({ params: { deviceId: item.id } }).pipe(
        tap(data => {
          let items = this.trackingUtil.processItems([data.result], data.datetime);

          let item = JSON.parse(JSON.stringify(items[0]));

          if ((!item.address || item.address.length == 0) && this.item.address.length > 0 && item.lat == this.item.lat && item.lng == this.item.lng) {
            items[0].address = this.item.address;
            item.address = this.item.address;
          }
          let checkChangeAddress = false;
          if (this.item.lat != items[0].lat || this.item.lng != items[0].lng) {
            checkChangeAddress = true;
          }
          this.item = items[0];
          this.mapService.updateMarkers([item]);
          if (!isNull(item.lat) && !isNull(item.lng)) {
            let isView = this.mapService.map.latLngToContainerPoint([item.lat, item.lng]);
            if (isView.x < 0 || isView.y < 0) {
              this.mapService.map.panTo([item.lat, item.lng]);
            }
          }

          if (checkChangeAddress) this.getGeocode(this.item);
          // process  icon 
          if (this.item && this.item.sensors) {
            this.item.sensors = this.item.sensors.map(x => {
              let type = this.typeSensors.find(sensor => sensor.type == x.type);
              if (type) x.icon = type.icon;
              return x;
            });
          }
          this.cdr.detectChanges();
        }),
        finalize(this.cdr.markForCheck)
      ).subscribe();
    }
  }

  selectItem(item) {
    this.getGeocode(item);
    this.mapService.fitbound([item]);
  }

  //rtsp://27.72.29.182:8999/{name}
  public link:string ="https://wcs5-eu.flashphoner.com:8888/embed_player?urlServer=wss://wcs5-eu.flashphoner.com:8443&streamName={link}&mediaProviders=WebRTC,Flash,MSE,WSPlayer";
  public dataCamera:Array<any> = [];
  private getCamera(){
    // https://demo.vnetgps.com/api/settings/cameras?pageNo=1&pageSize=10&deviceId=842
    let params = {
      pageNo: 1,
      pageSize: 10,
      deviceId: this.item.id
    };

    this.cameraService.list({ params: params }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.dataCamera = data.result.content.map(i=>{
            i['url'] = this.sanitizer.bypassSecurityTrustResourceUrl(this.link.replace('{link}',i.description));
            return i;
          });   

        }

      }),
      // takeUntil(this.unsubscribe),
      finalize(this.cdr.markForCheck)
    ).subscribe();
  }
  getGeocode(item) {
    // get address if isEmpty
    if (!item.address || item.address.length == 0) {
      if (isNull(item.lat) || isNull(item.lng)) return;
      let address = (item.lat || 0).toFixed(5) + ", " + (item.lng || 0).toFixed(5);
      item.addressLoading = true;
      this.cdr.detectChanges();
      this.mapUtilService.geocode({ params: { lat: item.lat, lng: item.lng } }).pipe(
        tap(
          data => {
            address = data.result.address;
          }
        ),
        finalize(() => {
          item.addressLoading = false;
          item.address = address;
          if (this.item.lat == item.lat && this.item.lng == item.lng) {
            this.mapService.updateMarkers([item]);
          }
          this.cdr.detectChanges();
          this.cdr.markForCheck();
        })
      ).subscribe();
    }
  }

  changeTab(value, date) {
    date = this.userDatePipeAdv.transform(date, "YYYY/MM/DD HH:mm:ss");
    this.eventChange.emit({ type: 'sub_change_tab', data: value, options: { date: date } });
  }
  isShow(path: string, deep?: boolean) {
    let obj = objectPath.get(this.layoutMapConfig, path);
    if (deep && obj) {
      if (Object.values(obj).some(x => x === true)) return true;
      else return false
    }
    return objectPath.get(this.layoutMapConfig, path);
  }

  openConfirm(command) {
    this.command = command;
    this.item.commandName = command;
    this.item.typeDevice = this.item.type;
    if (this.command == this.turnOn) {
      if (this.turnOnCmd.password) {
        this.sendCommandEmitter.emit([this.item]);
        return;
      }
    } else {
      if (this.turnOffCmd.password) {
        this.sendCommandEmitter.emit([this.item]);
        return;
      }
    }

    Swal.fire({
      title: this.translete.instant('MAP.TRACKING.INFO.TURN_ON_OFF'),
      text: this.translete.instant('MAP.TRACKING.INFO.CONFIRM'),
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translete.instant('MAP.TRACKING.INFO.YES'),
      cancelButtonText: this.translete.instant('MAP.TRACKING.INFO.NO'),
    }).then((result) => {
      if (result.value) {
        this.sendCommand();
      }
    })
  }

  sendCommand() {
    let commandStr;
    let commandName;
    if (this.command == this.turnOn) {
      commandStr = this.turnOnCmd.commandStr;
      commandName = this.translete.instant('COMMON.COMMAND.' + this.turnOnCmd.name)
    } else {
      commandStr = this.turnOffCmd.commandStr;
      commandName = this.translete.instant('COMMON.COMMAND.' + this.turnOffCmd.name)
    }
    this.deviceService.sendCommand({ ids: [this.item.id], commandStr: commandStr, commandName: commandName }, { notifyGlobal: true }).subscribe((data => {
    }));
    this.modalService.dismissAll(this.closeResult)
  }

  noSendCmd() {
    this.modalService.dismissAll(this.closeResult)
  }
}
