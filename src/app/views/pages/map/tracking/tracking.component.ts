import { Component, OnInit, ChangeDetectorRef, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { MapService } from '@core/utils/map';
import { MapService as MapServiceAPI } from '@core/map'
import { tap, takeUntil, finalize } from 'rxjs/operators';
import { Subject, interval, Subscription } from 'rxjs';
import { AlertService } from '@core/manage';
import { TrackingNowService } from '@core/common';

declare var $;
@Component({
  selector: 'kt-tracking',
  templateUrl: './tracking.component.html',
  styleUrls: ['./tracking.component.scss']
})
export class TrackingComponent implements OnInit, OnDestroy {

  @Input("listDevices") listDevices: Array<any>;
  @Input("itemTracking") itemTracking: any;
  @Input("isUserLogin") isUserLogin: boolean;
  @Input("mapService") mapService: MapService;
  @Input("tabAction") tabAction?: string;
  @Output("eventChange") eventChange: EventEmitter<{ type: string, data: any }>;
  public tabActive: string;
  public maxHeightWapper: number;
  public tabInfoLoaded: boolean;
  public tabRouteLoaded: boolean;
  public tabNotificationLoaded: boolean;
  public countNew: number;
  private intervalNotification;
  private durationGetCountNotification: number = 10 * 1000 // 10s
  private subscribeEventChange: Subscription;
  private unsubscribes: Array<Subscription> = [];
  public eventMapSetting: EventEmitter<string> = new EventEmitter();
  public isShowMobile: boolean;
  constructor(private cdr: ChangeDetectorRef,
    private alertService: AlertService,
    private mapServiceAPI: MapServiceAPI,
    private trackingNowService: TrackingNowService) {

    this.eventChange = new EventEmitter();
    this.countNew = 0;
    this.isShowMobile = true;
  }

  ngOnInit() {
    switch (this.tabAction) {
      case "now":
        this.tabActive = "now";
        this.tabInfoLoaded = true;
        break;
      case "route":
        this.tabActive = "route";
        this.tabRouteLoaded = true;
        break;
      case "notification":
        this.tabActive = "notification";
        this.tabNotificationLoaded = true;
        break;
      default:
        this.tabActive = "now";
        this.tabInfoLoaded = true;
        break;
    }
    let _this = this;
    setTimeout(() => {
      $(".kt_selectpicker").selectpicker();
    });

    this.mapService.clearMap();
    if (this.isUserLogin) {
      this.getCountNotification();
      this.intervalNotification = setInterval(() => {
        _this.alertService.countNew({ deviceId: _this.itemTracking.id }).pipe(
          tap(
            data => {
              _this.countNew = data.result.total;
              _this.cdr.detectChanges();
            }
          ),
          finalize(_this.cdr.markForCheck)
        ).subscribe();
      }, _this.durationGetCountNotification);
    }

    this.subscribeEventChange = this.alertService.eventChange.pipe(
      tap(event => {
        if (this.isUserLogin) {
          _this.listenEvenAlertChange(event);
        }
      })
    ).subscribe();
    let unsubscribeTrackingEmitter = this.trackingNowService.eventEmitterAction.subscribe((action: string) => {
      switch (action) {
        case "open":
          this.isShowMobile = true;
          break;
        case "close":
          this.isShowMobile = false;
          break;
      }
    });
    this.unsubscribes.push(unsubscribeTrackingEmitter);
  }
  ngOnDestroy() {
    this.mapService.clearMap();
    clearInterval(this.intervalNotification);
    if (this.subscribeEventChange) this.subscribeEventChange.unsubscribe();
    this.unsubscribes.forEach((sub) => {
      if (sub) sub.unsubscribe();
    });
  }

  changeTab(value) {
    this.tabActive = value;
    switch (value) {
      case 'route':
        this.tabRouteLoaded = true;
        break;
      case 'notification':
        this.tabNotificationLoaded = true;
        break;
      case 'now':
        this.tabInfoLoaded = true;
        break;
    }
    this.eventChange.emit({ type: 'change_tab', data: value });
  }

  changeKeySearch(value) {

  }

  onResize(element) {
    let clientRect = document.getElementById('ktTracking').getBoundingClientRect();
    let clientRectHeader = document.getElementById('headerListDeviceTracking').getBoundingClientRect();
    // this.maxHeightWapper = clientRect.height - 202;
    let _this = this;
    setTimeout(() => {
      _this.maxHeightWapper = clientRect.height - (clientRectHeader.height + 5);
      $("#listDeviceContainerTracking").css("height", _this.maxHeightWapper);
      $("#listDeviceContainerTracking").css("max-height", _this.maxHeightWapper);

      _this.cdr.detectChanges();
    });
  }

  changeItemTracking(data) {
    let item = this.listDevices.find(x => x.id == data.target.value);
    if (item) {
      this.itemTracking = item;
      this.eventChange.emit({ type: 'change_item_tracking', data: item });
      this.cdr.detectChanges();
    }
    if (this.tabActive != "route") {
      this.tabRouteLoaded = false;
    }
    if (this.tabActive != "now") {
      this.tabInfoLoaded = false;
    }
    if (this.tabActive != "notification") {
      this.tabNotificationLoaded = false;
      this.countNew = 0;
      this.cdr.detectChanges();
    }
  }

  closeTracking() {
    this.eventChange.emit({ type: 'close_tracking', data: this.itemTracking });
  }

  outputInfo(data) {
    switch (data.type) {
      case "sub_change_tab":
        this.tabRouteLoaded = false;
        this.cdr.detectChanges();
        this.changeTab(data.data);
        break;
    }
  }

  listenEvenAlertChange(event) {
    switch (event.type) {
      case "seen_device":
        this.getCountNotification();
        break;
      case "seen_all":
        this.countNew = 0;
        this.cdr.detectChanges();
        break;
    }
  }

  getCountNotification() {
    this.alertService.countNew({ deviceId: this.itemTracking.id }).pipe(
      tap(
        data => {
          this.countNew = data.result.total;
          this.cdr.detectChanges();
        }
      ),
      finalize(this.cdr.markForCheck)
    ).subscribe();
  }
  onConfigMap(actionKey) {
    this.eventMapSetting.emit(actionKey + "_" + this.tabActive);
  }
  close() {
    this.trackingNowService.close();
  }
}
