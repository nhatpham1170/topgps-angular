import { Component, OnInit, NgZone, OnDestroy, ChangeDetectorRef, Input, NgModule, HostListener, AfterViewInit } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { MapService as MapServiceAPI } from '@core/map';
import { MapService, MapUtilityService, MapUtil } from '@core/utils/map';
import { Subscription, Subscriber, interval } from 'rxjs';
import { tap, filter, debounceTime, takeUntil, finalize, delay, } from 'rxjs/operators';
import { UserDateAdvPipe, UserDateService, ToastService, } from '@core/_base/layout';
import { point } from 'leaflet';
import { CurrentUserService } from '@core/auth';
import * as L from 'leaflet';
import { TranslateService } from '@ngx-translate/core';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { XLSXModel, XlsxService, ExcelService } from '@core/utils';
import { DecimalPipe } from '@angular/common';
import { SensorService } from '@core/manage';
import { ReportsService } from '@core/report';
import { getSpeedColor } from '@core/map/consts/polyline-color/polyline-color';

am4core.useTheme(am4themes_animated);
declare var $;
const KEY_CACHE_CHART: string = 'tracking_chart_show';
const sensorDefault: any = { type: 'speed' };
export enum KEY_CODE {
	RIGHT_ARROW = 39,
	LEFT_ARROW = 37,
	UP_ARROW = 38,
	DOWN_ARROW = 40,
}

@Component({
	selector: 'kt-chart',
	templateUrl: './chart.component.html',
	styleUrls: ['./chart.component.scss'],
	providers: [UserDateAdvPipe, DecimalPipe],
})
export class ChartComponent implements OnInit, OnDestroy, AfterViewInit {
	@Input('mapService') mapService: MapService;

	private chart: am4charts.XYChart;
	public chartNumber: number = 1;
	public isShow: boolean = false;
	public dataPoints: any;
	public dataPointsByDay: any;
	public dataPlayback: any;
	public deviceTemplate: any;
	private subscribeMapService: Subscription;
	private groupLayer: L.FeatureGroup;
	private polylineLayer: L.FeatureGroup = new L.FeatureGroup();
	private lastPointCenter: any;
	public isLoading: boolean;
	public sensorActive: any;
	public dataTable: DataTable;
	public currentEvent: any = {};
	public styleDay = {
		'line-height': '10px',
		width: '100px',
		background: '',
	};
	public downloading: boolean = false;
	private isDataLogShowed: boolean = false;
	public itemActive: any = {};
	private listSensorsLoaded = {};
	private $destroySubscriber: Subscription[] = [];
	private filter: any = {};
	private autoPlay: any;
	public speed: number = 1;
	public isPlay: boolean = false;
	private progress: any;
	public pageOnHover = 0;
	private isMouseDown = false;
	private checkpoint: any;
	public isShowPlayback: boolean = false;
	private checkPlaying = false;
	private progressWidth = 100;
	private currentItemInTotal = 0;
	private isSelectRoute = false;
	private timeStamp: any;
	private isOnProgress = false;
	private offsetX: number;
	private polylinePoint: any[] = [];
	private colorfulPolyline: boolean = false;
	private savedPolylinePoint: any[] = [];
	// const prefixKeyCache:string = "name"
	constructor(
		private zone: NgZone,
		private cdr: ChangeDetectorRef,
		private mapServiceAPI: MapServiceAPI,
		private userDateAdv: UserDateAdvPipe,
		private currentUserService: CurrentUserService,
		private mapUtil: MapUtil,
		private translateService: TranslateService,
		private translate: TranslateService,
		private decimalPipe: DecimalPipe,
		private xlsx: ExcelService,
		private toast: ToastService,
		private sensorService: SensorService,
		private reportsService: ReportsService,
	) {
		this.isLoading = true;
		this.sensorActive = { type: 'speed' };
		this.initDataTable();
		this.itemActive = {};
	}

	@HostListener('window:keydown', ['$event'])
	keyEvent(event: KeyboardEvent) {
		switch (event.keyCode) {
			case KEY_CODE.DOWN_ARROW:
				this.changeSelectDataLog('increment');
				break;
			case KEY_CODE.UP_ARROW:
				this.changeSelectDataLog('decrement');
				break;
		}
	}

	// PLAYBACK start
	controlPlayback() {
		this.isPlay = !this.isPlay;
		if (this.isPlay) {
			this.playback();
		} else {
			this.pause();
		}
	}

	playback() {
		this.pause();
		this.isPlay = true;
		this.autoPlay = setInterval(() => {
			this.changeSelectDataLog('increment');
			if (this.currentItemInTotal == this.dataTable.getPaginations().total) this.pause();
			this.cdr.detectChanges();
		}, 1000 / this.speed);
	}

	pause() {
		this.isPlay = false;
		if (this.autoPlay) clearInterval(this.autoPlay);
	}

	changeSpeed() {
		this.speed++;
		if (this.speed > 4) this.speed = 1;
		if (!this.isPlay) return;
		this.playback();
	}

	getProgress(point = undefined) {
		if (point) {
			this.progress.style.width = `${point}px`;
			this.checkpoint.style.left = `${point - 5}px`;
			return;
		}
		this.currentItemInTotal = this.itemActive.id + this.dataTable.getPaginations().form - 1;
		let progress = (this.currentItemInTotal / this.dataTable.getPaginations().total) * this.progressWidth;
		this.progress.style.width = `${progress}px`;
		this.checkpoint.style.left = `${progress - 5}px`;
		if (this.itemActive.id > 0) {
			let chartDataLog = document.getElementById(
				`chartDataLog_${this.itemActive.id - 1}`
			);
			if (chartDataLog)
				chartDataLog.scrollIntoView({
					behavior: 'auto',
					block: 'nearest',
					inline: 'center',
				});
		}
		this.cdr.detectChanges();
	}

	getRealTimeProgress() {
		if (this.itemActive.id == undefined) {
			this.itemActive.id = 0;
		}
		if (this.progress instanceof HTMLElement && this.checkpoint instanceof HTMLElement) {
			this.getProgress();
		}
	}

	// Function to control playback progress bar
	jumpToActiveItem(offsetX: number, data: any[]) {
		let pageSize = this.dataTable.getPaginations().pageSize;
		let currentPage = this.dataTable.getCurrentPage();
		let currentIDTotal = Math.ceil(offsetX / this.progressWidth * this.dataTable.getPaginations().total);
		let currentID = currentIDTotal - (currentPage - 1) * pageSize;
		if (currentID < 1) currentID = 1;
		if (currentID > 9) currentID = 9;
		this.addPoint(data[currentID - 1], true);
		this.getProgress();
	}

	progressOnClick(event: MouseEvent) {
		let page = Math.ceil((event.offsetX / this.progressWidth) * this.dataTable.getLastPage());
		this.offsetX = event.offsetX;
		this.isOnProgress = true;
		this.dataTable.gotoPage(page);
	}

	progressOnHover(event: MouseEvent) {
		if (this.isMouseDown) {
			if (this.progress instanceof HTMLElement && this.checkpoint instanceof HTMLElement) {
				if (event.offsetY < 1 || event.offsetY > 35)
					this.isMouseDown = false;
				let point = event.offsetX;
				if (point > 99) {
					this.isMouseDown = false;
					return;
				}
				if (point < 0) {
					this.isMouseDown = false;
					this.dataTable.gotoPage(1);
					point = 0;
					return;
				}
				this.getProgress(point);
			}
		}
	}

	progressOnMousedown(event: MouseEvent) {
		this.isMouseDown = true;
		this.offsetX = event.offsetX;
		this.isOnProgress = true;
		if (this.isPlay) {
			this.pause();
			this.checkPlaying = true;
		} else {
			this.checkPlaying = false;
		}
		if (this.progress instanceof HTMLElement && this.checkpoint instanceof HTMLElement) {
			this.getProgress(event.offsetX);
		}
	}

	progressOnMouseup(event: MouseEvent) {
		if (this.isMouseDown) {
			let page = Math.ceil((event.offsetX / this.progressWidth) * this.dataTable.getLastPage());
			this.dataTable.gotoPage(page);
			this.isOnProgress = true;
			this.offsetX = event.offsetX;
			this.isMouseDown = false;
			let point = event.offsetX;
			if (point > 99) {
				this.isMouseDown = false;
				point = this.progressWidth;
				return;
			}
			if (point < 0) {
				this.isMouseDown = false;
				this.dataTable.gotoPage(1);
				point = 0;
				return;
			}
			this.getProgress(point);
			if (this.checkPlaying) this.playback();
		}
	}

	private async changeSelectDataLog(type: string) {
		// find item
		let flag: boolean = false;
		let finish: boolean = false;
		let pointIncrement = null;
		let pointDecrement = null;
		if (type == 'increment' || type == 'decrement') {
			for (let index = 0; index < this.dataTable.data.length; index++) {
				const day = this.dataTable.data[index].points;
				for (let indexPoint = 0; indexPoint < day.length; indexPoint++) {
					const point = day[indexPoint];
					if (this.itemActive.id == undefined && type == 'increment') {
						pointIncrement = point;
						pointDecrement = point;
						flag = false;
						finish = true;
						break;
					}
					if (point.id == this.itemActive.id) {
						flag = true;
						continue;
					} else {
						if (flag !== true) pointDecrement = point;
						else pointIncrement = point;
					}
					if (flag === true) {
						flag = false;
						finish = true;
						break;
					}
				}
				if (finish === true) break;
			}
		}
		switch (type) {
			case 'decrement':
				if (pointDecrement != null) {
					this.itemActive = pointDecrement;
					this.addPoint(this.itemActive);
					this.getRealTimeProgress();
				} else {
					//  check previous
					if (this.dataTable.getCurrentPage() > this.dataTable.getFirstPage()) {
						this.dataTable.gotoPage(this.dataTable.getCurrentPage() - 1);
						this.itemActive = {};
					}
				}

				break;
			case 'increment':
				if (pointIncrement != null) {
					this.itemActive = pointIncrement;
					this.addPoint(this.itemActive);
					this.getRealTimeProgress();
					this.showPolyline(this.itemActive);
				} else {
					// check next page
					if (this.dataTable.getCurrentPage() < this.dataTable.getLastPage()) {
						this.dataTable.gotoPage(this.dataTable.getCurrentPage() + 1);
						this.itemActive = {};
					}
				}
				break;
		}
		if (this.itemActive.id > 0) {
			let chartDataLog = document.getElementById(
				`chartDataLog_${this.itemActive.id - 1}`
			);
			if (chartDataLog)
				chartDataLog.scrollIntoView({
					behavior: 'auto',
					block: 'nearest',
					inline: 'center',
				});
		}
	}
	// PLAYBACK end

	ngAfterViewInit() {
		this.progress = document.querySelector('.progress');
		this.checkpoint = document.querySelector('.checkpoint');
	}

	ngOnInit() {
		let cacheIsShow = localStorage.getItem(KEY_CACHE_CHART);
		if (cacheIsShow == '1') {
			this.isShow = true;
		}
		this.subscribeMapService = this.mapServiceAPI.eventChange
			.pipe(
				tap((event) => {
					this.listenMapService(event);
				})
			)
			.subscribe();
		this.groupLayer = new L.FeatureGroup();
		this.mapService.map.addLayer(this.groupLayer);

	}

	ngOnDestroy() {
		this.pause();
		this.zone.runOutsideAngular(() => {
			if (this.chart) {
				this.chart.dispose();
			}
		});
		if (this.chart) {
			this.chart.dispose();
		}
		if (this.subscribeMapService) this.subscribeMapService.unsubscribe();
		if (this.$destroySubscriber)
			this.$destroySubscriber.forEach((x) => {
				if (x) x.unsubscribe();
			});
		this.mapService.map.removeLayer(this.groupLayer);
	}

	toggleChart() {
		this.pause();
		this.isShow = !this.isShow;
		localStorage.setItem(KEY_CACHE_CHART, this.isShow ? '1' : '0');
		if (this.isShow) {
			let _this = this;
			this.listSensorsLoaded = {};
			this.updateData();
		} else {
			if (this.chart) {
				this.chart.dispose();
			}
		}
		this.mapService.eventChange.emit({
			type: 'feature__fitbound',
			data: [],
		});
		this.cdr.detectChanges();

	}

	showChart() {
		this.isLoading = true;
		this.updateChart(this.sensorActive);
		this.isLoading = false;
		this.cdr.detectChanges();
	}
	updateDataLog() {
		let _this = this;
		if (_this.dataPointsByDay.length == 0)
			_this.dataPointsByDay = _this.processDataByDay(
				_this.currentEvent.data.routes
			);
		this.isLoading = true;
		setTimeout(() => {
			_this.dataTable.update({ data: _this.dataPointsByDay });
			let chartDataLog = document.getElementById('chartDataLog_0');
			if (chartDataLog)
				chartDataLog.scrollIntoView({
					behavior: 'smooth',
					block: 'start',
					inline: 'start',
				});
			_this.isLoading = false;
			_this.cdr.detectChanges();
		});
		let width = 10;
		this.dataTable.getColumns().forEach((x) => {
			width += x.width + 15;
		});

		if (this.deviceTemplate) {
			if (this.deviceTemplate.basicTemplate) {
				width += this.deviceTemplate.basicTemplate.length * 115;
			}
			width += this.deviceTemplate.sensorTemplate.length * 115;
		}
		this.styleDay['width'] = width - 25 + 'px';
		$(document).ready(function () {
			$('#dataTableDataLog').on('scroll', function () {
				$('#dataTableDataLogFixed').width(
					$('#dataTableDataLog').clientWidth - 6
				);
				$('#dataTableDataLogFixed').scrollLeft($(this).scrollLeft());
			});
		});
		if ($('#chartDataLog').height() > $('#dataTableDataLog').height())
			$('#dataTableDataLogFixed').width(
				$('#dataTableDataLog').clientWidth - 6
			);
		this.cdr.detectChanges();
	}
	private calculatorDayStyle() {
		let width = 10;
		this.dataTable.getColumns().forEach((x) => {
			width += x.width + 15;
		});

		if (this.deviceTemplate) {
			if (this.deviceTemplate.basicTemplate) {
				width += this.deviceTemplate.basicTemplate.length * 115;
			}
			width += this.deviceTemplate.sensorTemplate.length * 115;
		}
		this.styleDay['width'] = width - 25 + 'px';

		$(document).ready(function () {
			$('#dataTableDataLog').on('scroll', function () {
				$('#dataTableDataLogFixed').scrollLeft($(this).scrollLeft());
			});
		});

		this.cdr.detectChanges();
	}

	getSelectedDataTable(timeTo: string, timeStamp: number) {
		let filter = {
			timeFrom: this.currentEvent['data']['params']['timeFrom'],
			timeTo: timeTo,
			deviceIds: this.currentEvent['data']['params']['deviceId'],
		};
		this.timeStamp = timeStamp;

		this.reportsService
			.dataLogs({ params: filter, sessionStore: true })
			.subscribe((data) => {
				if (this.isShow && this.sensorActive.type == 'dataLog') {
					let checkpoint: number;
					checkpoint = Math.ceil(
						(data.result.totalRecord / this.dataTable.getPaginations().total) * this.dataTable.getPaginations().lastPage
					);
					if (this.dataTable.getCurrentPage() == checkpoint) {
						for (let i = 0; i < this.dataTable.data[0].points.length; i++) {
							if (this.dataTable.data[0].points[i].time === this.timeStamp) {
								if (i == this.dataTable.data[0].points.length - 1) { this.addPointWithoutFit(this.dataTable.data[0].points[i]) }
								else if (i == 0 && checkpoint == 1) {
									this.addPointWithoutFit(this.dataTable.data[0].points[i])
								}
								else {
									this.addPointWithoutFit(this.dataTable.data[0].points[i + 1]);
								}
							}
							this.cdr.detectChanges();
						}
						this.getProgress();
						this.isSelectRoute = false;
					} else {
						this.dataTable.gotoPage(checkpoint);
					}
				}
			});
	}

	private async getDataLog(filter) {
		filter['timeFrom'] = this.currentEvent['data']['params']['timeFrom'];
		filter['timeTo'] = this.currentEvent['data']['params']['timeTo'];
		filter['deviceIds'] = this.currentEvent['data']['params']['deviceId'];
		this.filter = filter;
		this.itemActive = {};
		await this.reportsService
			.dataLogs({ params: filter, sessionStore: true })
			.pipe(
				tap((result) => {
					this.dataPointsByDay = this.processDataPointsByDay(
						result.result.content
					);
					this.dataTable.update({
						data: this.dataPointsByDay,
						totalRecod: result.result.totalRecord,
					});
					this.calculatorDayStyle();
					if (this.isSelectRoute) {
						for (let i = 0; i < this.dataTable.data[0].points.length; i++) {
							if (this.dataTable.data[0].points[i].time === this.timeStamp) {
								if (i == this.dataTable.data[0].points.length - 1) { this.addPointWithoutFit(this.dataTable.data[0].points[i]) }
								else if (i == 0 && this.dataTable.getCurrentPage() == 1) {
									this.addPointWithoutFit(this.dataTable.data[0].points[i])
								}
								else {
									this.addPointWithoutFit(this.dataTable.data[0].points[i + 1]);
								}
							}
							this.cdr.detectChanges();
						}
						this.getProgress();
						this.isSelectRoute = false;
					}
					if (this.isOnProgress) {
						this.jumpToActiveItem(this.offsetX, this.dataTable.data[0].points);
						this.isOnProgress = false;
					}
				}),
				debounceTime(1000),
				finalize(() => {
					this.cdr.markForCheck();
				})
			)
			.subscribe();
	}

	processDataPointsByDay(data) {
		let dataResult = [];
		let dataObj = {};
		let index = 0;
		data.map((x) => {
			index++;
			x['id'] = index;
			x['base'] = x['base'].map((b) => {
				let id = `base_${b['key']}`;
				x[id] = b['value'];
				b['id'] = id;
				return b;
			});
			if (this.deviceTemplate && !this.deviceTemplate.basicTemplate) {
				this.deviceTemplate['basicTemplate'] = x['base'];
			}
			x.dateTime = this.userDateAdv.transform(
				x.time,
				'YYYY/MM/DD HH:mm:ss'
			);
			x.latLng = `${this.decimalPipe.transform(
				x.lat,
				'1.0-6'
			)}, ${this.decimalPipe.transform(x.lng, '1.0-6')}`;
			// process battery
			if (x['battery']['isAvailable']) {
				// có pin
				if (x['battery']['isCharged']) {
					x['batteryStr'] =
						this.translate.instant('COMMON.GENERAL.CHANGED') +
						' - ' +
						x['battery']['voltage'] +
						'V - ' +
						(x['battery']['percent'] || 0) +
						'%';
				} else {
					x['batteryStr'] =
						x['battery']['voltage'] +
						'V - ' +
						(x['battery']['percent'] || 0) +
						'%';
					x['power'] = this.translate.instant(
						'COMMON.GENERAL.BATTERY'
					);
				}
			} else {
				// ko có pin
				x['batteryStr'] = this.translate.instant(
					'COMMON.GENERAL.NO_BATTERY'
				);
			}

			x.sensors.forEach((s) => {
				x[`sensor${s.id}`] = s.value;
			});
			if (
				dataObj[this.userDateAdv.transform(x.time, 'YYYY/MM/DD')] ==
				undefined
			) {
				dataObj[this.userDateAdv.transform(x.time, 'YYYY/MM/DD')] = [];
			}
			dataObj[this.userDateAdv.transform(x.time, 'YYYY/MM/DD')].push(x);
			return x;
		});
		// process dataObj
		dataResult = Object.keys(dataObj).map((k) => {
			return {
				timeFrom: k,
				points: dataObj[k],
			};
		});

		return dataResult;
	}

	private updateData() {
		let _this = this;
		if (this.isShow) {
			if (this.sensorActive.type == 'dataLog') {
				this.isShowPlayback = true;
				if (this.listSensorsLoaded[`data_${this.sensorActive.type}`] !== true) {
					this.progress = document.querySelector('.progress');
					this.checkpoint = document.querySelector('.checkpoint');
					this.isLoading = false;
					let point = this.dataTable.getCurrentPage() / this.dataTable.getLastPage() * this.progressWidth;
					this.getProgress(point)
					this.dataTable.reload({});
				}
			} else {
				this.isShowPlayback = false;
				if (this.dataPoints.length == 0)
					this.dataPoints = this.processData([
						...this.currentEvent.data.routes,
					]);
				this.isLoading = true;
				this.cdr.detectChanges();
				this.updateChart(this.sensorActive);
				this.isLoading = false;
				this.cdr.detectChanges();
			}
			this.listSensorsLoaded[`data_${this.sensorActive.type}`] = true;
		}
	}

	listenMapService(event) {
		switch (event.type) {
			case 'playback':
				this.listSensorsLoaded = {};
				this.dataPointsByDay = [];
				this.dataPoints = [];
				this.currentEvent = event;
				this.dataPlayback = event.data;
				this.itemActive = {};
				if (
					this.deviceTemplate &&
					this.deviceTemplate.imei != event.data.device.imei
				) {
					this.sensorActive = sensorDefault;
				}
				this.deviceTemplate = event.data.device;
				this.deviceTemplate.sensorTemplate = this.processIcon(
					this.deviceTemplate.sensorTemplate
				);
				this.updateData();
				this.dataTable.gotoPage(1);
				this.pause();
				if (this.progress instanceof HTMLElement && this.checkpoint instanceof HTMLElement) {
					this.progress.style.width = '1px';
					this.checkpoint.style.left = '-3.5px';
				}
				break;

			case 'route_loading':
				if (this.groupLayer) this.groupLayer.clearLayers();
				if (event.data === true) {
					this.isLoading = event.data;
				}
				this.cdr.detectChanges();
				break;
			case 'device_info':
				this.itemActive = {};
				if (
					this.deviceTemplate &&
					this.deviceTemplate.id != event.data.id
				) {
					this.sensorActive = sensorDefault;
				}
				this.deviceTemplate = event.data;
				this.deviceTemplate.sensorTemplate = this.processIcon(
					this.deviceTemplate.sensorTemplate
				);
				this.cdr.detectChanges();
				break;
			case 'tracking_route':
				break;
			case 'tracking_route_hide':
				if (this.groupLayer) this.groupLayer.clearLayers();
				this.cdr.detectChanges();
				break;
			case 'select_route':
				if (this.sensorActive.type != 'dataLog') return;
				let timeTo = this.userDateAdv.transform(
					event.data.start.time,
					'YYYY-MM-DD HH:mm:ss'
				);
				this.isSelectRoute = true;
				this.getSelectedDataTable(timeTo, event.data.start.time);
				break;
			case 'change_tab':
				this.pause();
				break;
			default:
				break;

		}
	}

	processIcon(sensors) {
		return sensors.map((x) => {
			let type = this.sensorService
				.getAllTypeSensor()
				.find((sensor) => sensor.type == x.parameters.typeSensor);
			x.parameters.icon = type ? type.icon : 'icon-tag';
			return x;
		});
	}

	processData(dataRoute) {
		let points = [];
		dataRoute.forEach((day) => {
			let countCheck = true;
			day.routes.forEach((routes) => {
				if (routes.points) {
					if (countCheck) points = points.concat(routes.points);
					else points = points.concat(routes.points.slice(1));
				}
				countCheck = false;
			});
		});
		// set date
		points = points.map((point) => {
			point.date = new Date(
				this.userDateAdv.transform(point.time, 'YYYY-MM-DD HH:mm:ss')
			);
			return point;
		});
		return points;
	}

	/**
	 * Process data points by day
	 * @param dataRoute
	 */
	processDataByDay(dataRoute) {
		let indexDay = 0;
		let dataResult = dataRoute.map((day) => {
			indexDay++;
			let countCheck = true;
			let points = [];
			day.routes.forEach((routes) => {
				if (points.length < 50) {
					if (routes.points) {
						if (countCheck) points = points.concat(routes.points);
						else points = points.concat(routes.points.slice(1));
					}
				}
				countCheck = false;
			});
			let index = 0;
			points = points.map((x) => {
				x.id = `${indexDay}_${index}`;
				index++;
				x['base'] = x['base'].map((b) => {
					let id = `base_${b['key']}`;
					x[id] = b['value'];
					b['id'] = id;
					return b;
				});
				if (this.deviceTemplate && !this.deviceTemplate.basicTemplate) {
					this.deviceTemplate['basicTemplate'] = x['base'];
				}
				// process battery
				if (x['battery']['isAvailable']) {
					// có pin
					if (x['battery']['isCharged']) {
						x['batteryStr'] =
							this.translate.instant('COMMON.GENERAL.CHANGED') +
							' - ' +
							x['battery']['voltage'] +
							'V - ' +
							(x['battery']['percent'] || 0) +
							'%';
					} else {
						x['batteryStr'] =
							x['battery']['voltage'] +
							'V - ' +
							(x['battery']['percent'] || 0) +
							'%';
						x['power'] = this.translate.instant(
							'COMMON.GENERAL.BATTERY'
						);
					}
				} else {
					// ko có pin
					x['batteryStr'] = this.translate.instant(
						'COMMON.GENERAL.NO_BATTERY'
					);
				}
				x.dateTime = this.userDateAdv.transform(
					x.time,
					'YYYY/MM/DD HH:mm:ss'
				);
				x.latLng = `${this.decimalPipe.transform(
					x.lat,
					'1.0-6'
				)}, ${this.decimalPipe.transform(x.lng, '1.0-6')}`;
				x.sensors.forEach((s) => {
					x[`sensor${s.id}`] = s.value;
				});
				return x;
			});
			return {
				timeFrom: day.timeFrom,
				timeEnd: day.timeEnd,
				points: points,
			};
		});
		return dataResult;
	}

	/**
	 * TODO
	 * add point to map
	 * send event to tracking route select route
	 * @param item as point
	 */
	addPoint(item, onTableClick?: boolean) {
		if (onTableClick) {
			this.hidePolyline();
		}
		this.itemActive = item;
		if (this.isShow) this.getProgress();
		this.mapServiceAPI.eventChange.emit({
			type: 'chart_selecte_point',
			data: item,
		});
		if (this.groupLayer) this.groupLayer.clearLayers();

		if (item.lat != 0 && item.lng != 0) {
			let marker = this.mapUtil.createMarker(
				[item.lat, item.lng],
				{},
				{
					color: '#2394f5',
				}
			);
			if (marker) {
				this.groupLayer.addLayer(marker);
				this.groupLayer.setZIndex(1);
				// Calculate the offset
				let isInMap = this.mapService.checkBound(marker.getLatLng())
				if (!isInMap) {
					this.mapUtil.fitBound(this.groupLayer, this.mapService.map, {
						paddingBottomRight: [0, this.mapService.map.getSize().y * 0.6],
						maxZoom: this.mapService.map.getZoom(),
					});
				}
			}
		}
	}

	/**
 * TODO
 * add point to map
 * send event to tracking route select route
 * @param item as point
 */
	addPointWithoutFit(item) {
		this.itemActive = item;
		this.hidePolyline();
		if (this.isShow) this.getProgress();
		this.mapServiceAPI.eventChange.emit({
			type: 'chart_selecte_point',
			data: item,
		});

		if (this.groupLayer) this.groupLayer.clearLayers();
		if (item.lat != 0 && item.lng != 0) {
			let marker = this.mapUtil.createMarker(
				[item.lat, item.lng],
				{},
				{
					color: '#2394f5',
				}
			);
			if (marker) {
				this.groupLayer.addLayer(marker);
				this.groupLayer.setZIndex(1);
			}
		}
	}

	showPolyline(item) {
		this.polylinePoint.push(item);
		let color = '#e36912';
		if (this.colorfulPolyline) {
			color = getSpeedColor(item.speed);
		}
		let polyline = this.mapService.createPolyline(
			this.polylinePoint,
			{
				color: color,
				weight: 5,
				opacity: 1,
			},
			{
				isShow: false,
				options: {
					offset: 3,
				},
			}
		);
		if (polyline) {
			this.polylineLayer.addLayer(polyline);
			this.polylineLayer.addTo(this.mapService.map);
			this.polylinePoint = [this.polylinePoint[this.polylinePoint.length - 1]];
		}
		this.cdr.detectChanges();
	}

	hidePolyline() {
		this.polylineLayer.clearLayers();
		this.polylinePoint = [];
	}

	updateChart(sensor: any) {
		let data;
		if (this.chart) {
			this.chart.dispose();
		}
		if (!this.dataPoints) this.dataPoints = [];
		switch (sensor.type) {
			case 'speed':
				data = this.dataPoints.map((x) => {
					x.valueChart = x.speed;
					return x;
				});
				this.renderOneChart(data, {
					min: 0,
					max: 200,
					tooltipText: '{dateX}: [bold]{valueY}[/] km/h',
					title:
						'<span>' +
						this.translateService.instant(
							'MAP.TRACKING.CHART.SPEED'
						) +
						' (km/h)</span>',
				});
				break;
			default:
				if (sensor.id) {
					data = this.dataPoints.map((x) => {
						let sensorTmp = x.sensors.find(
							(s) => s.id == sensor.id
						);
						if (sensorTmp) x.valueChart = sensorTmp.value;
						else x.valueChart = 0;
						return x;
					});
					this.renderOneChart(data, {
						min: Number.parseFloat(sensor.parameters.min),
						max: Number.parseFloat(sensor.parameters.max),
						tooltipText:
							'{dateX}: [bold]{valueY}[/] ' +
							sensor.parameters.unit,
						title:
							'<span>' +
							sensor.name +
							' (' +
							sensor.parameters.unit +
							')</span>',
						fill: true,
					});
				}
				break;
		}
	}

	onChangeTabs(sensor: any) {
		if (
			sensor.id != this.sensorActive.id ||
			sensor.type != this.sensorActive.type
		) {
			this.sensorActive = sensor;
			this.pause();
			this.updateData();
		}
	}

	private dataPointsByDayExport: any = [];
	exportFile(options: { type?: string }) {
		this.downloading = true;
		this.cdr.detectChanges();
		this.filter['pageNo'] = -1;
		this.reportsService
			.dataLogs({ params: this.filter })
			.pipe(
				tap((result) => {
					if (
						result.result.length == 0 ||
						(result.result.length > 0 &&
							result.result[0].points.length == 0)
					) {
						this.toast.show({
							type: 'error',
							translate: 'COMMON.MESSAGE.NOT_FOUND_DATA',
						});
					} else {
						this.dataPointsByDayExport = this.processDataPointsByDay(
							result.result[0].points
						);
						let summary = {
							startTime: this.userDateAdv.transform(
								this.filter['timeFrom'],
								'datetime',
								{ valueOffset: 0 }
							),
							endTime: this.userDateAdv.transform(
								this.filter['timeTo'],
								'datetime',
								{ valueOffset: 0 }
							),
							sensorTemplate: this.currentEvent.data.device
								.sensorTemplate,
							basicTemplate: this.deviceTemplate.basicTemplate,
							deviceName: this.currentEvent.data.device
								.deviceName,
						};
						switch (options.type) {
							case 'xlsx':
								this.exportFileXLSX(
									this.dataPointsByDayExport,
									summary
								);
								break;
						}
					}
				}),
				debounceTime(1000),
				finalize(() => {
					this.downloading = false;
					this.cdr.detectChanges();
					this.cdr.markForCheck();
				})
			)
			.subscribe();


		setTimeout(() => {
			this.downloading = false;
			this.cdr.detectChanges();
		}, 10000);
	}

	private exportFileXLSX(data, summary) {
		let sheets = [];
		data.forEach((v, i) => {
			let day = this.userDateAdv.transform(v.timeFrom, 'YYYY/MM/DD');
			let config: XLSXModel;
			if (i == 0) {
				config = {
					file: {
						title: this.translate.instant(
							'MAP.TRACKING.CHART.DATA_LOG'
						),
						prefixFileName: `${
							summary['deviceName']
							}___${this.userDateAdv.transform(
								summary.startTime,
								'YYYYMMDD_HHmmss',
								{ valueOffset: 0 }
							)}__${this.userDateAdv.transform(
								summary.endTime,
								'YYYYMMDD_HHmmss',
								{ valueOffset: 0 }
							)}`,
						typeName: 'DataHistory',
						objName: summary['deviceName'],
						timeStart: this.userDateAdv.transform(
							summary.startTime,
							'YYYYMMDD_HHmmss',
							{ valueOffset: 0 }
						),
						timeEnd: this.userDateAdv.transform(
							summary.endTime,
							'YYYYMMDD_HHmmss',
							{ valueOffset: 0 }
						),
					},
					header: [
						{
							text: this.translate.instant(
								'MAP.TRACKING.CHART.DATA_LOG'
							),
							type: 'header',
							style: {
								fill: {
									patternType: 'solid', // none / solid
									fgColor: { rgb: '01B050' },
								},
							},
							hpx: 32,
						},
						{
							text: '',
						},
						{
							text: '!timezone',
						},
						{
							text: '',
						},
						{
							text: [
								this.translate.instant(
									'COMMON.EXCEL.START_TIME'
								) + ': ',
								,
								summary['startTime'],
							],
						},
						{
							text: [
								this.translate.instant(
									'COMMON.EXCEL.END_TIME'
								) + ': ',
								,
								summary['endTime'],
							],
							// text: [,,],
						},
						{
							text: '',
						},
						{
							text: [
								this.translate.instant(
									'COMMON.COLUMN.DEVICE_NAME'
								) + ': ',
								,
								summary['deviceName'],
							],
							style: {
								font: { bold: true },
							},
						},
						{
							text: '',
						},
					],
					columns: [
						{
							name: '#',
							columnData: 'auto',
							wch: 5,
						},
						{
							name: this.translate.instant(
								'COMMON.COLUMN.TIME_DEVICE'
							),
							columnData: 'time',
							wch: 20,
							type: 'datetime',
							format: 'YYYY/MM/DD HH:mm:ss',
						},
						{
							name: this.translate.instant(
								'COMMON.COLUMN.UPDATE_TIME'
							),
							columnData: 'updatetimeUTC',
							wch: 20,
							type: 'datetime',
							format: 'YYYY/MM/DD HH:mm:ss',
						},
						{
							name: this.translate.instant(
								'COMMON.COLUMN.LAT_LNG'
							),
							columnData: 'latLng',
							wch: 20,
						},
						{
							name: this.translate.instant('COMMON.COLUMN.GSM'),
							columnData: 'gsm',
							wch: 20,
							type: 'number',
						},
						{
							name: this.translate.instant('COMMON.COLUMN.GPS'),
							columnData: 'gps',
							wch: 20,
							type: 'number',
						},
						{
							name:
								this.translate.instant('COMMON.COLUMN.POWER') +
								'(V)',
							columnData: 'powerVoltage',
							wch: 20,
							type: 'number',
						},
						{
							name: this.translate.instant(
								'COMMON.COLUMN.BATTERY'
							),
							columnData: 'batteryStr',
							wch: 30,
						},
						{
							name: `${this.translate.instant(
								'COMMON.COLUMN.SPEED'
							)}(km/h)`,
							columnData: 'speed',
							wch: 20,
							type: 'number',
						},
					],
					subColumn: [
						{
							text: day,
							style: {
								font: { bold: true },
								fill: {
									patternType: 'solid', // none / solid
									fgColor: { rgb: '989898' },
								},
							},
							merge: {
								full: true,
							},
						},
					],
					styleColumn: {
						fill: {
							patternType: 'solid', // none / solid
							fgColor: { rgb: '92D050' },
						},
						font: { bold: true },
						alignment: {
							vertical: 'center',
							horizontal: 'center',
							wrapText: true,
						},
					},
					showColumn: true,
					woorksheet: {
						name: summary['deviceName'],
					},
				};
			} else {
				config = {
					file: {
						title: this.translate.instant(
							'MAP.TRACKING.CHART.DATA_LOG'
						),
						prefixFileName: `${
							summary['deviceName']
							}___${this.userDateAdv.transform(
								summary.startTime,
								'YYYYMMDD_HHmmss',
								{ valueOffset: 0 }
							)}__${this.userDateAdv.transform(
								summary.endTime,
								'YYYYMMDD_HHmmss',
								{ valueOffset: 0 }
							)}`,
					},
					header: [],
					columns: [
						{
							name: '#',
							columnData: 'auto',
							wch: 5,
						},
						{
							name: this.translate.instant(
								'COMMON.COLUMN.TIME_DEVICE'
							),
							columnData: 'time',
							wch: 20,
							type: 'datetime',
							format: 'YYYY/MM/DD HH:mm:ss',
						},
						{
							name: this.translate.instant(
								'COMMON.COLUMN.UPDATE_TIME'
							),
							columnData: 'updatetimeUTC',
							wch: 20,
							type: 'datetime',
							format: 'YYYY/MM/DD HH:mm:ss',
						},
						{
							name: this.translate.instant(
								'COMMON.COLUMN.LAT_LNG'
							),
							columnData: 'latLng',
							wch: 20,
						},
						{
							name: this.translate.instant('COMMON.COLUMN.GSM'),
							columnData: 'gsm',
							wch: 20,
							type: 'number',
						},
						{
							name: this.translate.instant('COMMON.COLUMN.GPS'),
							columnData: 'gps',
							wch: 20,
							type: 'number',
						},
						{
							name:
								this.translate.instant('COMMON.COLUMN.POWER') +
								'(V)',
							columnData: 'powerVoltage',
							wch: 20,
							type: 'number',
						},
						{
							name: this.translate.instant(
								'COMMON.COLUMN.BATTERY'
							),
							columnData: 'batteryStr',
							wch: 30,
						},
						{
							name: `${this.translate.instant(
								'COMMON.COLUMN.SPEED'
							)}(km/h)`,
							columnData: 'speed',
							wch: 20,
							type: 'number',
						},
					],
					subColumn: [
						{
							text: day,
							style: {
								font: { bold: true },
								fill: {
									patternType: 'solid', // none / solid
									fgColor: { rgb: '989898' },
								},
							},
							merge: {
								full: true,
							},
						},
					],
					styleColumn: {
						fill: {
							patternType: 'solid', // none / solid
							fgColor: { rgb: '92D050' },
						},
						font: { bold: true },
						alignment: {
							vertical: 'center',
							horizontal: 'center',
							wrapText: true,
						},
					},
					showColumn: false,
					woorksheet: {
						name: summary['deviceName'],
					},
				};
			}
			// add basic
			summary.basicTemplate.forEach((s) => {
				config.columns.push({
					name: `${s.name}`,
					columnData: `${s.id}`,
					wch: 20,
					type: 'number',
				});
			});
			// add sensor
			summary.sensorTemplate.forEach((s) => {
				config.columns.push({
					name: `${s.name}(${s.unit || '-'})`,
					columnData: `sensor${s.id}`,
					wch: 20,
					type: 'number',
				});
			});
			sheets.push({ data: v.points, config: config, option: {} });
		});

		// let config2: any = JSON.parse(JSON.stringify(config));
		// config2.woorksheet.name = "test";
		this.xlsx.exportFileAdvanced({ sheets: sheets, mergeSheet: true });
	}
	private processDataExport(data) {
		let dataResult = [];
		let dataTemp = [];
		dataTemp = data.map((x) => {
			x.dateTime = this.userDateAdv.transform(
				x.time,
				'YYYY/MM/DD HH:mm:ss'
			);
			x.latLng = `${this.decimalPipe.transform(
				x.lat,
				'1.0-6'
			)}, ${this.decimalPipe.transform(x.lng, '1.0-6')}`;
			// x.speed =  this.decimalPipe.transform(x.speed,"1.0-0") || 0;
			x.sensors.forEach((s) => {
				x[`sensor${s.id}`] = s.value;
			});
			return x;
		});
		// process by day
		dataTemp.forEach((x) => {
			if (
				dataResult[this.userDateAdv.transform(x.time, 'YYYY/MM/DD')] ==
				undefined
			)
				dataResult[
					this.userDateAdv.transform(x.time, 'YYYY/MM/DD')
				] = [];
			dataResult[this.userDateAdv.transform(x.time, 'YYYY/MM/DD')] = x;
		});
		return dataResult;
	}
	onKeypress($value) {
	}
	// data-log
	private initDataTable() {
		let tableConfig = {
			pagination: [10, 20, 30, 50],
			columns: [
				{
					title: '#',
					field: 'id',
					allowSort: false,
					isSort: false,
					dataSort: '',
					style: { width: '40px' },
					width: 40,
					class: '',
					translate: '#',
					autoHide: false,
				},
				{
					title: 'time device',
					field: 'timeDevice',
					allowSort: false,
					isSort: false,
					dataSort: '',
					style: { width: '130px' },
					width: 130,
					class: '',
					translate: 'COMMON.COLUMN.TIME_DEVICE',
					autoHide: false,
				},
				{
					title: 'update time ',
					field: 'updateTime',
					allowSort: false,
					isSort: false,
					dataSort: '',
					style: { width: '135px' },
					width: 135,
					class: '',
					translate: 'COMMON.COLUMN.UPDATE_TIME',
					autoHide: false,
				},

				{
					title: 'lat lng',
					field: 'latLng',
					allowSort: false,
					isSort: false,
					dataSort: '',
					style: { width: '160px' },
					width: 160,
					class: '',
					translate: 'COMMON.COLUMN.LAT_LNG',
					autoHide: false,
				},
				{
					title: 'gsm',
					field: 'gsm',
					allowSort: false,
					isSort: false,
					dataSort: '',
					style: { width: '50px' },
					width: 50,
					class: '',
					translate: 'COMMON.COLUMN.GSM',
					autoHide: false,
				},
				{
					title: 'gps',
					field: 'gps',
					allowSort: false,
					isSort: false,
					dataSort: '',
					style: { width: '50px' },
					width: 50,
					class: '',
					translate: 'COMMON.COLUMN.GPS',
					autoHide: false,
				},
				{
					title: 'power vol',
					field: 'powerVoltage',
					allowSort: false,
					isSort: false,
					dataSort: '',
					style: { width: '50px' },
					width: 50,
					class: '',
					translate: 'COMMON.COLUMN.POWER',
					autoHide: false,
				},
				{
					title: 'battery',
					field: 'battery',
					allowSort: false,
					isSort: false,
					dataSort: '',
					style: { width: '150px' },
					width: 150,
					class: '',
					translate: 'COMMON.COLUMN.BATTERY',
					autoHide: false,
				},
				{
					title: 'speed',
					field: 'speed',
					allowSort: false,
					isSort: false,
					dataSort: '',
					style: { width: '100px' },
					width: 100,
					class: '',
					translate: 'COMMON.COLUMN.SPEED',
					autoHide: true,
				},
			],
		};
		this.dataTable = new DataTable();
		this.dataTable.init({
			data: [],
			totalRecod: 0,
			paginationSelect: [10, 20, 30, 50],
			columns: tableConfig.columns,
			formSearch: '#formSearch',
			pageSize: 10,
			// isDebug: true,
			layout: {
				body: {
					scrollable: false,
					maxHeight: 200,
				},
				responsive: false,
				// selecter: true,
			},
			showFull: true,
		});
		this.dataTable.eventUpdate
			.pipe(
				tap((option) => {
					this.filter.pageNo = option.pageNo;
					this.filter.pageSize = option.pageSize;
					this.getDataLog(this.filter);
				})
			)
			.subscribe();
	}
	showOnMap(item) { }
	onResize() { }
	// end data-log

	/**
	 *
	 * @param data dataPoint
	 * @param config config for chart
	 */
	renderOneChart(
		data: any,
		config: {
			min?: number;
			max?: number;
			tooltipText?: string;
			title?: string;
			fill?: boolean;
		}
	) {
		let chart = am4core.create('chartdiv', am4charts.XYChart);

		chart.paddingRight = 10;
		chart.leftAxesContainer.layout = 'vertical';
		//set chart data
		chart.data = data;

		// Set input format for the dates
		chart.dateFormatter.inputDateFormat = 'yyyy-MM-dd HH:mm:ss';

		// set xAxes
		let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
		dateAxis.dateFormats.setKey(
			'day',
			this.currentUserService.currentDate.amchart
		);
		dateAxis.periodChangeDateFormats.setKey('day', '[bold]d[/]');

		// set yAxes
		let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		// height of axis
		valueAxis.height = 150;
		valueAxis.marginTop = 30;
		valueAxis.renderer.fontSize = '0.8em';
		valueAxis.strictMinMax = true;
		if (config.min != undefined) {
			valueAxis.min = config.min;
			let range = valueAxis.axisRanges.create();
			range.value = config.min;
			range.grid.stroke = am4core.color('#396478');
			range.grid.strokeWidth = 1;
			range.grid.strokeOpacity = 1;
			range.label.inside = true;
			range.label.fill = range.grid.stroke;
			range.label.verticalCenter = 'bottom';
		}
		if (config.max != undefined) {
			valueAxis.max = config.max;
			let range = valueAxis.axisRanges.create();
			range.value = config.max;
			range.grid.stroke = am4core.color('#A96478');
			range.grid.strokeWidth = 1;
			range.grid.strokeOpacity = 1;
			range.label.inside = true;
			range.label.fill = range.grid.stroke;
			range.label.verticalCenter = 'bottom';
		}
		// set series
		let series = chart.series.push(new am4charts.LineSeries());
		series.dataFields.dateX = 'date';
		series.dataFields.valueY = 'valueChart';
		series.strokeWidth = 2;
		series.stroke = chart.colors.getIndex(0);
		if (config.fill == true) {
			series.fill = chart.colors.getIndex(0);
			series.fillOpacity = 0.2;
		}
		if (config.tooltipText) series.tooltipText = config.tooltipText;
		series.tooltipText = config.tooltipText || '{valueY}';

		// axisTitle
		let axisTitle = series.createChild(am4core.Label);
		axisTitle.layout = 'absolute';
		axisTitle.html = config.title || '';
		axisTitle.fontWeight = '600';
		axisTitle.fontSize = 13;
		axisTitle.paddingTop = -25;

		if (!data || data.length == 0) {
			let axisNodata = chart.createChild(am4core.Label);
			axisNodata.fill = am4core.color('rgb(87, 87, 87)');
			axisNodata.html =
				"<i class='icon-database'></i> " +
				this.translateService.instant('MAP.TRACKING.CHART.NO_DATA');
			axisNodata.fontWeight = '600';
			axisNodata.fontSize = 13;
			axisNodata.isMeasured = false;
			axisNodata.x = am4core.percent(50);
			axisNodata.horizontalCenter = 'middle';
			axisNodata.y = 50;
			valueAxis.renderer.grid.template.disabled = true;
		}
		chart.cursor = new am4charts.XYCursor();
		chart.cursor.interactionsEnabled = true;
		let _this = this;
		chart.events.on('hit', (ev) => {
			let dataItem = dateAxis.getSeriesDataItem(
				series,
				dateAxis.toAxisPosition(chart.cursor.xPosition),
				true
			);
			if (dataItem) {
				_this.addPoint(dataItem.dataContext);
			}
		});
		this.chart = chart;
	}

	differentChart() {
		var chart = am4core.create('chartdiv', am4charts.XYChart);
		chart.paddingRight = 20;
		var data = [];
		var visits = 10;
		var previousValue;
		for (var i = 0; i < 100; i++) {
			visits += Math.round(
				(Math.random() < 0.5 ? 1 : -1) * Math.random() * 10
			);
			if (i > 0) {
				// add color to previous data item depending on whether current value is less or more than previous value
				if (previousValue <= visits) {
					data[i - 1].color = chart.colors.getIndex(0);
				} else {
					data[i - 1].color = chart.colors.getIndex(5);
				}
			}
			data.push({ date: new Date(2018, 0, i + 1), value: visits });
			previousValue = visits;
		}
		chart.data = data;
		var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
		dateAxis.renderer.grid.template.location = 0;
		dateAxis.renderer.axisFills.template.disabled = true;
		dateAxis.renderer.ticks.template.disabled = true;
		var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.tooltip.disabled = true;
		valueAxis.renderer.minWidth = 35;
		valueAxis.min = 0;
		valueAxis.max = 100;
		var series = chart.series.push(new am4charts.LineSeries());
		series.dataFields.dateX = 'date';
		series.dataFields.valueY = 'value';
		series.strokeWidth = 2;
		series.tooltipText =
			'value: {valueY}, day change: {valueY.previousChange}';
		// set stroke property field
		series.propertyFields.stroke = 'color';
		chart.cursor = new am4charts.XYCursor();
		series.segments.template.interactionsEnabled = true;
		series.segments.template.events.on(
			'hit',
			(e) => {
			},
			this
		);
		dateAxis.keepSelection = true;
	}

	changePage(page) {
		this.dataTable.gotoPage(page);
		this.hidePolyline();
	}
}
