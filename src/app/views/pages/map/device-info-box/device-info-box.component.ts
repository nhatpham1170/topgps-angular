import { Component, OnInit, Input, EventEmitter, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MapService, MapUtilityService, MapConfigService, LayoutMapConfigModel } from '@core/utils/map';
import { TrackingUtil, TrackingService } from '@core/map';
import { UserDateAdvPipe } from '@core/_base/layout';
import { finalize, tap } from 'rxjs/operators';
import { isNull } from 'util';
import { Subscription } from 'rxjs';
import objectPath from 'object-path';
import { DeviceMap } from '@core/map/_models/device-map';
import { stringify } from 'querystring';
import { TypeSensor, SensorService } from '@core/manage';

const KEY_STORE: string = "DeviceInfoBox";
@Component({
  selector: 'kt-device-info-box',
  templateUrl: './device-info-box.component.html',
  styleUrls: ['./device-info-box.component.scss'],
  providers: [UserDateAdvPipe]
})
export class DeviceInfoBoxComponent implements OnInit, OnDestroy {

  @Input('keyName') keyName?: string;
  @Input('cache') cache?: boolean = true;
  @Input('mapService') mapService?: MapService;
  @Input('item') item?: any;
  @Input('itemChange') itemChange?: EventEmitter<any>;
  private keyStore: string;
  public isClosed: boolean;
  private destroy: boolean;
  private intervalRefresh;
  private timeRefresh: number; // second
  private subscription: Subscription;
  private unsubscribe: Subscription[] = [];
  private layoutMapConfig: LayoutMapConfigModel;
  public eventMapSetting: EventEmitter<string>;
  private objStore: any;
  public typeSensorSelectDefault: TypeSensor;
  public typeSensors: Array<TypeSensor>;
  constructor(private trackingUtil: TrackingUtil,
    private trackingService: TrackingService,
    private mapUtilService: MapUtilityService,
    private cdr: ChangeDetectorRef,
    private userDatePipeAdv: UserDateAdvPipe,
    private mapConfigService: MapConfigService,
    private sensorService: SensorService) {
    this.keyStore = `${KEY_STORE}${this.keyName || ""}`;
    this.objStore = {};

    this.eventMapSetting = new EventEmitter();
    this.item = new DeviceMap();
    this.timeRefresh = 10 * 1000;
    this.layoutMapConfig = this.mapConfigService.getLayoutConfig();
    const subscri = this.mapConfigService.layoutConfigUpdate$.subscribe(layoutConfig => {
      this.layoutMapConfig = layoutConfig;
    });

    this.unsubscribe.push(subscri);

    this.isClosed = false;
    this.typeSensors = this.sensorService.getAllTypeSensor();
    this.typeSensorSelectDefault = this.typeSensors.find(x => x.type == 'other');
  }

  ngOnInit() {
    const mapChangeSubcir = this.mapService.eventChange.subscribe((event) => {
      // process item 
      switch (event.type) {
        case "action__select_device":
          this.item = event.data;
          break;
        case "action__update_items":
          this.item = event.data.find((x) => x.id == this.item.id) || this.item;
          break;
        case "action__add_items":
          this.item = event.data.find((x) => x.id == this.item.id) || event.data[0];
          // this.mapService.focusItem(this.item);
          break;
        case "action__remove_items":
          if (event.data.find((x) => x.id == this.item.id)) {
            this.item = this.mapService.getItems().length > 0 ? this.mapService.getItems()[0] : new DeviceMap();
            // this.mapService.focusItem(this.item);
          }
          break;
      }
      if (!this.item) this.item = new DeviceMap();


      if (this.item && this.item.sensors) {
        this.item.sensors = this.item.sensors.map(x => {
          let type = this.typeSensors.find(sensor => sensor.type == x.type) || this.typeSensorSelectDefault;
          x.icon = type.icon;
          return x;
        });
      }
      this.setCache();
    });
    this.unsubscribe.push(mapChangeSubcir);
    if (this.cache) {
      if (localStorage.getItem(this.keyStore)) {

        this.objStore = JSON.parse(localStorage.getItem(this.keyStore));
        this.isClosed = this.objStore['isClosed'] || false;
        this.item.id = this.objStore['id'] || 0;
        // if(this.item.id > 0){
        //   this.onSelectDevice(this.item);
        // }
        this.cdr.detectChanges();
      }
    }
  }
  ngOnDestroy() {
    this.unsubscribe.forEach(sub => {
      if (sub) sub.unsubscribe();
    });
  }
  checkShow(path: string, deep?: boolean) {
    let obj = objectPath.get(this.layoutMapConfig, path);
    if (deep && obj) {
      if (Object.values(obj).some(x => x === true)) return true;
      else return false
    }
    return objectPath.get(this.layoutMapConfig, path);
  }
  openSettingLayout() {

    this.eventMapSetting.emit("info_device");
  }
  onToggle() {

    this.isClosed = !this.isClosed;
    this.setCache();

  }
  getGeocode(item) {
    // get address if isEmpty
    if (!item.address || item.address.length == 0) {
      if (isNull(item.lat) || isNull(item.lng)) return;
      let address = (item.lat || 0).toFixed(5) + ", " + (item.lng || 0).toFixed(5);
      item.addressLoading = true;
      this.cdr.detectChanges();
      this.mapUtilService.geocode({ params: { lat: item.lat, lng: item.lng } }).pipe(
        tap(
          data => {
            address = data.result.address;
          }
        ),
        finalize(() => {
          item.addressLoading = false;
          item.address = address;
          if (this.item.lat == item.lat && this.item.lng == item.lng) {
            this.mapService.updateMarkers([item]);
          }
          this.cdr.detectChanges();
          this.cdr.markForCheck();
        })
      ).subscribe();
    }
  }

  setCache() {
    if (this.cache) {
      this.objStore['isClosed'] = this.isClosed;
      this.objStore['id'] = this.item.id || 0;
      localStorage.setItem(this.keyStore, JSON.stringify(this.objStore));
    }

  }
  checkSensor() {
    return this.item.otherData
      && (this.item.otherData.air != '-' && this.checkShow('tracking.info.other.air')
        || (this.item.otherData.door != '-' && this.checkShow('tracking.info.other.door'))
        || (this.item.otherData.engine != '-' && this.checkShow('tracking.info.other.engine')))
      || (this.item.sensors && this.item.sensors.length > 0);
  }
  onSelectDevice(item) {
    this.mapService.selectItem(item);
    this.mapService.sendEmitter({ type: "action__set_view_list_device", data: item });
  }
  onFavorite(item) {
    this.mapService.sendEmitter({ type: "action__require_favourite", data: item });
  }

}
