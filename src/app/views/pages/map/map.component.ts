import { TollStationService } from './../../../core/admin/_services/toll-station.service';
import { GeofenceGroupService } from './../../../core/manage/_services/geofence-group.service';
import { PoiModel } from './../../../core/manage/_models/poi';
import { Component, OnInit, ViewChild, ElementRef, EventEmitter, ChangeDetectorRef, ComponentFactoryResolver, Injector, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import 'leaflet.markercluster';
import { MapConfigService } from '@core/utils';
import { MapConfigModel } from '@core/utils/map/_model/map-config';
import { MapService } from '@core/utils/map/_services/map.service';
import { MapService as MapServiceAPI } from '@core/map';
import { Item, MapChangeEvent, MapConfig } from '@core/utils/map';
import { ResizeEvent } from 'angular-resizable-element';
import { TrackingService } from '@core/map';
import { tap, finalize } from 'rxjs/operators';
import { User, CurrentUserService } from '@core/auth';
import { GeofenceService, GeofenceModel, PoiService, Device } from '@core/manage';
import { NgbModal, NgbModalRef, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { PoiTypeService, ListIconPoiType } from '@core/admin';
import { NgxPermissionsService } from 'ngx-permissions';
import { AsciiPipe } from '@core/_base/layout';

declare var $;
const TITLE_FORM_EDIT: string = "COMMON.ACTIONS.EDIT";
const TITLE_FORM_ADD: string = "COMMON.ACTIONS.ADD";
const TITLE_FORM_DELETE: string = "COMMON.ACTIONS.DELETE";
@Component({
  selector: 'kt-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  providers: [ListIconPoiType]
})
export class MapComponent implements OnInit, OnDestroy, OnChanges {
  public options: any;
  public currentUserSelect: any;
  public listDeviceEventEmiter: EventEmitter<{ type: string, data: any }>;
  public map;
  public listGeofence: Array<GeofenceModel>;
  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  public geofenceDataChangeEmiter: EventEmitter<any>;
  public landmarkDataChangeEmiter: EventEmitter<any>;
  public mapConfig: MapConfigModel;
  public optionWideCheckList: any = { type: "geofence" };
  public optionLandmarkCheckList: any = { type: "landmark" };
  private mapService: MapService;
  public closeResult: string;
  private modalReference: NgbModalRef;
  public dataTrackingItem: any;
  public ktMapFit: boolean;
  public isTracking: boolean;
  public currentListDevices: Array<any>;
  public deivceSelectedTracking: any;
  public isUserLogin: boolean = true;
  public trackingActionDefault: string = "now";
  public chartShow: boolean;
  public chartHide: boolean;
  private subscribePopup: Subscription;
  private subscribeMapServiceAPI: Subscription;
  public showUserTree: boolean;
  public listTypePoi = [];
  public listDevices: Array<Device> = [];
  public action: string = 'add';
  public geofenceAction: string = 'add';
  public poiEdit: PoiModel;
  private currentModel: PoiModel;
  private lastUserId: any;
  public listGeofenceGroup: any[];
  public geofenceEdit: GeofenceModel;
  public showMapFull: boolean = true;
  @ViewChild('contentTrackingItem', { static: true }) contentTrackingItem: ElementRef;
  @ViewChild('content', { static: true }) popup: ElementRef;
  @ViewChild('geofenceContent', { static: true }) geoPopup: ElementRef;

  public titlePopup: any;
  public centerMap: { latlng: { lat: number; lng: number; }, zoom: number };
  public landmark: PoiModel[];
  public showTools: boolean = false;
  private listTollStation: any[];
  constructor(private cdr: ChangeDetectorRef,
    private trackingService: TrackingService,
    private geofenceService: GeofenceService,
    private resolver: ComponentFactoryResolver, private injector: Injector,
    private modalService: NgbModal,
    private currentUserService: CurrentUserService,
    private mapServiceAPI: MapServiceAPI,
    private userTreeService: UserTreeService,
    private poiService: PoiService,
    private poiTypeService: PoiTypeService,
    private listIconPoiType: ListIconPoiType,
    private geofenceGroupService: GeofenceGroupService,
    private permissionsSevice: NgxPermissionsService,
    private tollStationService: TollStationService,
  ) {
    this.mapService = new MapService(this.resolver, this.injector);
    this.options = this.mapService.init();
    this.parrentEmiter = new EventEmitter();
    this.listDeviceEventEmiter = new EventEmitter();
    this.geofenceDataChangeEmiter = new EventEmitter();
    this.landmarkDataChangeEmiter = new EventEmitter();

    this.currentUserSelect = {};
    this.listGeofence = [];
    this.mapConfig = this.mapService.config;
    this.mapService.config.features.follow.show = true;
    this.mapService.config.features.markerPopup.show = true;
    this.mapService.config.features.markerStyle.options.showSpeed = true;
    // this.mapService.config.features.markerStyle.options.showDuration = true;
    this.mapService.config.features.cluster.options.showSpeed = true;
    this.mapService.config.features.cluster.options.showDuration = true;
    this.mapService.config.features.fullScreen.show = true;
    this.mapService.config.features.brightness.show = true;
    this.ktMapFit = false;
    this.isTracking = false;
    this.currentUserService.init();
    this.chartShow = false;
    this.showUserTree = true;

  }

  ngOnInit() {
    if (this.permissionsSevice.getPermission('ROLE_map.map_lite') != undefined
      && this.permissionsSevice.getPermission('ROLE_map.map_full') == undefined) {
      this.showMapFull = false;
    }
    this.mapService.eventChange.pipe(
      tap(data => {
        this.mapServiceEventChange(data);
      })
    ).subscribe();
    this.subscribeMapServiceAPI = this.mapServiceAPI.eventChange.pipe(
      tap(event => {
        this.listenEventMapService(event);
      })
    ).subscribe();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            this.cdr.detectChanges();
            break;
          case "open":
            this.showUserTree = true;
            this.cdr.detectChanges();
            break;
        }
      })
    ).subscribe();

    if (sessionStorage.getItem('listTypePoi')) {
      this.listTypePoi = JSON.parse(sessionStorage.getItem('listTypePoi'))
    } else {
      this.getListPoiType();
    }

    this.drawTolls()
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngOnDestroy() {
    if (this.subscribeMapServiceAPI) this.subscribeMapServiceAPI.unsubscribe();
    if (this.subscribePopup) this.subscribePopup.unsubscribe();

  }
  mapServiceEventChange(data: MapChangeEvent) {
    switch (data.type) {
      case "followPopup":
        this.followPopup(data.data);
        break;
      case "fullScreen":
        this.fullScreen(data.data);
        break;
    }
  }
  followPopup(item) {
    this.dataTrackingItem = item;
    this.open(this.contentTrackingItem);
    this.cdr.detectChanges();
  }
  fullScreen(data) {
    if (data) {
      this.ktMapFit = true;
    }
    else {
      this.ktMapFit = false;
    }
  }
  onMapReady(map) {
    // this.options.layer.updateFilter(['brightness:110%', 'hue:90deg', 'saturate:120%']);
    this.mapService.setMap(map);
    // this.mapService.config.features.cluster.value = true;
  }

  onResize() {
    this.mapService.resize();
    this.cdr.detectChanges();
  }


  getMapService() {
    return this.mapService;
  }

  changeUser(value) {
    this.currentUserSelect = value;
    this.listDeviceEventEmiter.emit({ type: "user", data: value });
    this.loadGeofence(this.currentUserSelect);
    this.loadLandmark(this.currentUserSelect);
    this.mapService.clearLandmark();
    if (value.id != this.currentUserService.currentUser.id) {
      this.isUserLogin = false;
    }
    else this.isUserLogin = true;
    this.cdr.detectChanges();
  }

  // get landmark 
  loadLandmark(user: any, data?: PoiModel) {
    this.poiService.list({ params: { userId: user.id } }).pipe(
      tap((data: any) => {
        data.result.content = data.result.content.filter(x => x.active == 1);
        this.mapService.setPoi(data.result.content);
        this.mapService.clearLandmark();
        this.landmark = data.result.content;
        this.landmarkDataChangeEmiter.emit(data.result.content);
      })
    ).subscribe();
  }

  // get list geofence
  loadGeofence(user: any) {
    this.geofenceService.list({ params: { userId: user.id, pageNo: -1 } }).pipe(
      tap((data) => {
        data.result = data.result.filter(x => x.active == 1);
        this.geofenceDataChangeEmiter.emit(data.result);
        this.listDeviceEventEmiter.emit({ type: "geofence", data: data.result });
      }),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
    // this.open(this.contentTrackingItem);
  }

  onWidgetCheckChange(data) {
    if (data.type) {
      switch (data.type) {
        case "check":
          this.mapService.clearGeofences();
          this.mapService.addGeofences(data.listChecked, false);
          break;
        case "check-landmark":
          this.mapService.addLandmark(data.listChecked);
          break;
        case "selecte-marker":
          this.mapService.fitBoundGeofences(data.items);
          break;
        case "selecte-landmark":
          this.mapService.fitBoundLandmark(data.items);
          break;
      }
    }
  }

  ktListDeviceEventChange(data) {
    if (data.type) {
      switch (data.type) {
        case "tracking":
          if (data.options && data.options.tab) {
            this.trackingActionDefault = data.options.tab;
          }
          this.isTracking = true;
          this.deivceSelectedTracking = data.data;
          this.listDeviceEventEmiter.emit({ type: "tracking", data: true });
          this.cdr.detectChanges();
          break;
        case "list_data":
          this.currentListDevices = data.data;
          break;
      }
    }
  }
  ktListDeviceLiteEventChange(data) {
    if (data.type) {
      let dataMap = [];
      switch (data.type) {
        case "load_list_data":

          data.data.forEach(d => {
            let item = new Item(d);
            dataMap.push(item);
          });

          this.mapService.addMarkers(dataMap, true);
          this.cdr.detectChanges();
          break;
        case "refresh_list_data":
          data.data.forEach(d => {
            let item = new Item(d);
            dataMap.push(item);
          });
          this.mapService.updateMarkers(dataMap);
          break;
        case "item_check_changes":
          data.data.forEach(d => {
            let item = new Item(d);
            dataMap.push(item);
          });
          this.mapService.updateMarkers(dataMap);

          break;
        case "item_check_change":
          this.mapService.updateMarker(new Item(data.data));
          break;
        case "item_select":
          if (data.data.feature.hideOnMap === false)
            this.mapService.selectItem(new Item(data.data));
          break;
      }
    }
  }

  eventTracking(event) {
    if (event.type == "close_tracking") {
      this.chartShow = false;
      this.isTracking = false;
      this.listDeviceEventEmiter.emit({ type: "tracking", data: false });
      this.cdr.detectChanges();
    }
  }

  listenEventMapService(event) {
    switch (event.type) {
      case "tracking_route":
        this.chartShow = true;
        this.chartHide = false;
        this.cdr.detectChanges();
        break;
      case "tracking_route_hide":
        this.chartHide = true;
        this.cdr.detectChanges();
        break;
    }
  }

  drawTolls() {
    this.tollStationService.list({ params: { pageNo: -1 } }).subscribe(data => {
      if (data.status == 200) {
        this.listTollStation = data.result;
        this.mapService.drawTollsOnMap(this.listTollStation)
      }
    })
  }

  open(content) {
    this.modalReference = this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal--fit kt-modal-clear', size: "lg", backdrop: 'static' });
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getListPoiType() {
    this.poiTypeService.list({ pageNo: -1 },).subscribe(data => {
      if (data.status == 200) {
        data.result.map(poiType => {
          let name = "ADMIN.POI_TYPE.LANGUAGE." + poiType.name;
          poiType.name = name;
          let iconClass = this.listIconPoiType.getIconByKey(poiType.type);
          poiType.iconClass = iconClass;
          return poiType;
        })
        this.listTypePoi = data.result;
        sessionStorage.setItem('listTypePoi', JSON.stringify(this.listTypePoi));
        setTimeout(() => {
          $('.kt_selectpicker').selectpicker();
        })
      }
    })
  }

  poiResult(data) {
    this.loadLandmark(this.currentUserSelect, data.data);
    this.modalService.dismissAll();
  }

  createLandmark(popup) {
    this.action = 'add';
    this.titlePopup = TITLE_FORM_ADD;
    this.centerMap = {
      latlng: {
        lat: this.mapService.map.getCenter().lat,
        lng: this.mapService.map.getCenter().lng
      },
      zoom: this.mapService.map.getZoom()
    };
    this.openPopup(popup)
  }

  async editItem(item) {
    switch (item.type) {
      case 'landmark':
        this.action = 'edit';
        this.titlePopup = TITLE_FORM_EDIT;
        item.data.latlngs = [
          {
            lat: item.data.latitude,
            lng: item.data.longitude
          }
        ];
        item.data.points = [
          [item.data.latitude, item.data.longitude]
        ];
        item.data.type = 'marker';
        this.poiEdit = new PoiModel(item.data);
        this.openPopup(this.popup);
        break;
      case 'geofence':
        this.geofenceAction = 'edit';
        this.titlePopup = TITLE_FORM_EDIT;
        await this.getListGeofenceGroup(this.currentUserSelect.id);
        this.geofenceService.detail(item.data.id).pipe(
          tap(body => {
            this.geofenceEdit = new GeofenceModel(body.result);
            this.modalService.open(this.geoPopup, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal-xl kt-modal--fit', backdrop: 'static' }).result.then((result) => {
              this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
              this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
          }),
          finalize(() => {
            this.cdr.markForCheck();
          })
        ).subscribe();
        break;
    }

  }

  openPopup(popup) {
    this.open(popup);
  }

  deleteItem(item) {
    switch (item.type) {
      case 'landmark':
        this.action = 'delete';
        this.titlePopup = TITLE_FORM_DELETE;
        this.currentModel = item.data;
        this.modalService.open(this.popup, { windowClass: 'kt-mt-50 modal-dialog-scrollable', size: 'sm', backdrop: 'static' });
        break;
      case 'geofence':
        this.geofenceAction = 'delete';
        this.titlePopup = TITLE_FORM_DELETE;
        this.currentModel = item.data;
        this.modalService.open(this.geoPopup, { windowClass: 'kt-mt-50 modal-dialog-scrollable', size: 'sm', backdrop: 'static' });
        break;
    }
  }

  deleteAction() {
    this.poiService.delete(this.currentModel.id, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.modalService.dismissAll();
        }
      }),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe(() => {
      this.loadLandmark(this.currentUserSelect);
    });

  }

  // Geofence Popup
  async createGeofence(popup) {
    this.geofenceAction = 'add';
    this.titlePopup = TITLE_FORM_ADD;
    await this.getListGeofenceGroup(this.currentUserSelect.id);
    this.centerMap = {
      latlng: {
        lat: this.mapService.map.getCenter().lat,
        lng: this.mapService.map.getCenter().lng
      },
      zoom: this.mapService.map.getZoom()
    };
    this.openPopup(popup);
  }


  async getListGeofenceGroup(userId: number) {
    if (userId != this.lastUserId) {
      this.lastUserId = userId;
      let params = {
        pageNo: -1,
        userId: userId
      }
      await this.geofenceGroupService.listGeofenceGroup(params)
        .pipe(
          tap((result: any) => {
            if (result.status == 200) {
              //example data
              this.listGeofenceGroup = result.result;

            }
          }),
          finalize(() => {
            this.cdr.markForCheck();
            this.cdr.detectChanges();
          })
        ).toPromise();
    }
    else {
      return this.listGeofenceGroup;
    }
  }

  deleteGeofence() {
    this.geofenceService.delete(this.currentModel.id, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.modalService.dismissAll();
        }
      }),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe(() => {
      this.loadGeofence(this.currentUserSelect);
    });
  }

  geofenceResult() {
    this.loadGeofence(this.currentUserSelect);
    this.modalService.dismissAll();
  }

  closeWidget(event) {
    this.mapConfig.features[event].value = false
  }

  showPopup(event) {
    switch (event) {
      case 'landmark':
        this.createLandmark(this.popup)
        break;
      case 'geofence':
        this.createGeofence(this.geoPopup)
    }
  }

  mapFullScreen() {
    this.mapConfig.features.fullScreen.value = !this.mapConfig.features.fullScreen.value;
    this.mapService.fullScreen(this.mapConfig.features.fullScreen.value);
  }
}
