import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup } from "@angular/forms";
import { ToastService, UserDateService  } from '@core/_base/layout';
import { ReportsService,Reports,FavoriteReport } from '@core/report';
import { TranslateService } from '@ngx-translate/core';
import { finalize, tap } from 'rxjs/operators';
import { CurrentUserService } from '@core/auth/_services';
import { Subject } from 'rxjs';
import { AppState } from '@core/reducers';
import { UserSetting,PageService } from '@core/_base/layout';
import { currentUser } from '@core/auth/_selectors/auth.selectors';

// Store
import { Store, select } from '@ngrx/store';
import { GeofenceService } from '@core/manage';
declare var $: any;

@Component({
  selector: 'kt-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  addReportForm: FormGroup;
  public listReportsNoFavorite:any = [];
  public listReportFavorite:any = [];
  public closeResult: string;
  public reportSelect:any;
  private keyDelete : string;
  private unsubscribe: Subject<any>;
  private listReports :any = [];
  private nameFavorite:string;
  private user:any 
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private toast: ToastService,
    private reportService:ReportsService,
    private translate:TranslateService,
    private cdr: ChangeDetectorRef,
    private store: Store<AppState>,
    private userSetting : UserSetting,
    private currentUserService : CurrentUserService,
    private page:PageService,


  ){
    let user:any = this.store.pipe(select(currentUser));
    if (user) {
      select(user.pipe(
        tap((data:any) => {
          if (data) {
            this.nameFavorite = "favorite"+data.id;           
          }
        }),
      ).subscribe().unsubscribe());
    }

    this.listReports = this.page.getListPageReports();
    
  }

  ngOnInit() {
    this.userSetting.nameFavorite = this.nameFavorite;
    this.buildForm();
    this.loadLocalStorageReport();
  }

  private buildForm(): void {
    this.addReportForm = this.formBuilder.group({
      itemReport: [''],
    });
  }

  loadLocalStorageReport()
  {
   
    
    if(this.userSetting.checkLocalStorageFavorite())
    {
      let favoriteReports = this.userSetting.getLocalStorageFavorite();      
      favoriteReports.forEach(v => {
        if(this.renderReportFavorite(v)) this.listReportFavorite.push(this.renderReportFavorite(v));
      });
      this.listReportFavorite =  this.sortOrder(this.listReportFavorite);
    }
    else
    {
      this.getListReportsFavorite();
    }

  }

  getListReportsNoUse(){
    this.reportSelect = [];
  }

  getListReportsFavorite()
  {
    let params = {
      id : this.currentUserService.Id,
    } as Reports;
    this.reportService.listReport(params).pipe(
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      if(data.result == null)
      {
        this.listReportFavorite = this.page.getListPageReports();
      }
      else
      {
        let favoriteReports = data.result.favoriteReports;
        this.userSetting.setLocalStorageFavorite(favoriteReports);
        favoriteReports.forEach(v => {
         if(this.renderReportFavorite(v)) this.listReportFavorite.push(this.renderReportFavorite(v));
        });
        this.listReportFavorite =  this.sortOrder(this.listReportFavorite);
      }
    });
  }

  renderReportFavorite(objIn){

    
   let key = objIn.key;
   let objOut =  this.listReports.find(x => x.translate == key);
   if(!objOut) return undefined;
   objOut.sortOrder = objIn.sortOrder;
   return objOut;
  }

  renderReportNoFavorite(){
    this.listReportsNoFavorite = this.listReports.filter(o=> !this.listReportFavorite.some(i=> i.translate === o.translate));
  }

  sortOrder(arr)
  {
    return arr.sort((a,b)=> parseInt(a.sortOrder) - parseInt(b.sortOrder));
  }

  addReport(content)
  {
    this.getListReportsNoUse();
    this.open(content);
    $(function () {
      $('.kt_selectpicker').selectpicker();
    });
  }

  deleteReport(){
    let key = this.keyDelete;
    this.listReportFavorite = this.listReportFavorite.filter(function( obj ) {
      return obj.translate !== key;
  });
    this.updateListFavorite();
    this.modalService.dismissAll();
  }

  onSubmitReport(form)
  {
    let reportKey = form.value.itemReport;
    if(reportKey.length == 0)
    {
        this.toast.show(
          {
            message: this.translate.instant('MANAGE.REPORT_SCHEDULE.MESSAGE.TYPE_REPORT'),
            type: 'error',
          })
        return false; 
    }
    reportKey.forEach(element => {
      let reportAdd = this.renderReportFavorite({key : element});
      if(reportAdd)
      {
        this.listReportFavorite.push(reportAdd);
      }
    });
    this.updateListFavorite();
    this.modalService.dismissAll();

  }

  openModalDelete(key,idModal) {
    this.keyDelete = key;
    this.modalService.open(idModal, { windowClass: 'kt-mt-50  modal-holder modal-delete'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });  }

  open(content) {
    this.renderReportNoFavorite();
    this.modalService.open(content, { windowClass: 'kt-mt-50  modal-holder'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  updateListFavorite()
  {
    let arrFavorite:any = [];
    this.listReportFavorite.forEach((v, i) => {
      let item = {
        key : v.translate,
        sortOrder : i,
      } as FavoriteReport;
      arrFavorite.push(item);
    });
    let params = {
      favoriteReport : arrFavorite,
      userId :  this.currentUserService.Id
    } as Reports;
    this.reportService.updateReport(params).pipe(
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      if(data.status == 200)
      {
        this.userSetting.setLocalStorageFavorite(arrFavorite);
        this.modalService.dismissAll();
      }
    });
  }

  drop(event: CdkDragDrop<string[]>) 
  {
    moveItemInArray(this.listReportFavorite, event.previousIndex, event.currentIndex);
    this.updateListFavorite();
    //code change position api
  }

}
