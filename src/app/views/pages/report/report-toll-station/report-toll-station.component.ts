import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef,EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { ReportTollStationService } from '@core/report';
import { DeviceService } from '@core/manage';
import { TranslateService } from '@ngx-translate/core';
import { ToastService, UserDatePipe,UserDateAdvPipe } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { ExcelService,PdfmakeService,ExcelTestService} from '@core/utils';
import { XLSXModel, XLSXModelTest } from '@core/utils/xlsx/excel.service';
import { PdfModel } from '@core/utils/pdfmake/pdfmake.service';

import { DatePipe } from '@angular/common';
import { UserDateService } from '@core/_base/layout';
import { DecimalPipe } from '@angular/common';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { waitDownloadModel } from '../wait-download/wait.download.service'
import { ExportService } from '@core/utils';
import { UserTreeService } from '@core/common/_service/user-tree.service';

// end chart
declare var $: any;

@Component({
  selector: 'report-toll-station',
  templateUrl: './report-toll-station.component.html',
  styleUrls: ['./report-toll-station.component.scss'],
  providers: [DatePipe, UserDatePipe, DecimalPipe,UserDateAdvPipe]

})
export class ReportTollStation implements OnInit {
  @ViewChild('listDevicePicker', { static: true }) 
  listDevicePicker:any;

  @ViewChild('datePicker', { static: true }) 
  datePicker: DateRangePickerComponent;
  @ViewChild('datePickerSearchChart', { static: true }) 

  datePickerSearchChart: DateRangePickerComponent;
  searchFormChart:FormGroup;
  searchFormSummary: FormGroup;
  selectdp: ElementRef;
  public datePickerOptions: any;
  public datePickerOptionsSearchChart:any;

  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  // show full data
  public showFullData:boolean = true;
  public dataPagination : any = [];
  public dataExport: any = null;
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataDefault: any = [{}];
  public filter: any;
  private unsubscribe: Subject<any>;
  public listDevices: any = [];
  public listSensors: any = [];
  public userIdSelected: number;
  public userIdSelectedEdit: number;
  public showUserTree: boolean = false;
  public dateStart: string;
  public dateEnd: string;
  public timeStart: string;
  public timeEnd: string;
  //time search chart
  public dateStartChart:string;
  public dateEndChart:string;
  public sensorIdSearhChart:any;
  public paramsEmiter: EventEmitter<waitDownloadModel>;
  public deviceId: string;
  public status: string;

  public deviceIdSearhChart:any;
  public deviceName: string;
  public dateFormat: string;
  public timeFormat: string;
  public timeZone:string;
  public totalAllRowData : any;
  public parentBread:any;
  public isdownLoad : boolean = false;
  public isdownLoadPDF:boolean = false;
  public closeResult: string;
  public checkbetweenChange:boolean = false;
  public listStatus:any;
  public listStatusObj:any;
  public listDeviced: any = [];

  // fuel
  public showFuel:boolean = true;

  // export
  public configExport:PdfModel;
 // configChart
  constructor(
    private userTreeService: UserTreeService,
    private tollStation: ReportTollStationService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private deviceService: DeviceService,
    private translate: TranslateService,
    private toast: ToastService,
    private currentUser: CurrentUserService,
    private xlsx: ExcelService,
    public datepipe: DatePipe,
    private userDate: UserDateService,
    private userDatePipe: UserDatePipe,
    private pdf : PdfmakeService,
    private userDateAdv : UserDateAdvPipe,
    private exportService:ExportService

  ) {

    this.paramsEmiter = new EventEmitter();
    
    this.dateFormat = this.currentUser.dateFormatPipe;
    this.timeFormat = this.currentUser.timeFormat;
    this.timeZone = this.currentUser.currentUser.timezone;
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.parentBread = [
      {
        name: this.translate.instant('REPORT.REPORTS.GENERAL.REPORTS'),
        translate: "REPORT.REPORTS.GENERAL.REPORTS",
        link: "/reports",
        icon: "flaticon2-graph"
      }
    ]
  }

  ngOnInit() {
    this.updateDataTable();
    this.refreshEventType();
    this.datePickerOptions = this.objOption();
    this.userTreeService.event.pipe(
        tap(action => {
          switch (action) {
            case "close":
              this.showUserTree = false;
              break;
            case "open":
              this.showUserTree = true;
              break;
          }
        })
      ).subscribe();
  }



  objOption()
  {
    let obj = {
      size: 'md',
      select: 'from',
      ranges: 'from',
      optionDatePicker: {
        maxSpan: {
          days: 31
        }

      },
      autoSelect: true,
      singleDatePicker: false,
      timePicker: true,
      startDate:moment().subtract(1, "days").startOf('day'),
      endDate:moment().subtract(1, "days").endOf('day'),
    }; 
    return obj;
  }

  renderConfig()
  {
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    this.configExport  = {
      info: {
        columnParent:'fuel',
        title: 'TollFee',
      },
      content:{
        header:{
          title:this.translate.instant('REPORT.TOLL_STATION.GENERAL.REPORT_NAME'),
          params:{
            timeStart:": "+this.dateStart,
            timeEnd:": "+this.dateEnd,
            timeZone:": "+this.timeZone,
          }
        },
        table:[
          {
              woorksheet: {
                name: this.deviceName,
              },
              total : {
                group : [
                  { columnData:'name'},
                ],
                totalCol:[
                    { text:this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL'),style:{colSpan:3,alignment:"right",bold:true,fillColor:"#fff"}},
                    { text:''},
                    { text:''},
                    { columnData:'totalAmount',style:{alignment:"left",bold:true,fillColor:"#fff"}},
                    { text:'',style:{colSpan:3,alignment:"center",bold:true,fillColor:"#fff"}},

                    { text:''},
                    { text:''},
      
                ],
                totalAll:{
                    title : [{ text:this.translate.instant('MAP.TRACKING.ROUTE.SUMMARY'),alignment:"left",bold:true,fillColor:"#fff",fontSize:20,margin: [0, 20, 0, 5]}],
                    content : [
                    [
                      { text: this.translate.instant('REPORT.TOLL_STATION.COLUMN.ALL_PRICE'),fillColor:"#EEEEEE"},
                      { text:this.totalAllRowData.amount}
                    ]
                   
                  ]
                }
              },
              headerRows: 2,
              widths: ['4%','14%','18%','12%','12%','20%','20%'],
              header :[           
                  [ 
                    
                    { text: '#',rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                    { text: this.translate.instant('ADMIN.TOLL_SEGMENT.COLUMN.ROAD'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                    { text: this.translate.instant('REPORT.TOLL_STATION.COLUMN.SEGMENT'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                    { text: this.translate.instant('ADMIN.TOLL_STATION.COLUMN.PRICE'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                    { text: this.translate.instant('REPORT.TOLL_STATION.COLUMN.TOLL'),colSpan:3,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text:''},
                    { text:''},
                  ],
                  [
                    '', '', '', '',
                    { text: this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('COMMON.COLUMN.NAME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('COMMON.COLUMN.ADDRESS'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                  ]
              ],
              body:[
                {columnData:'count',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'roadName',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'segmentName',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'amount',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnParent:'fuel' , columnData:'time',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnParent:'fuel' ,columnData:'name',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnParent:'fuel' ,columnData:'address',style:{alignment:"left",fillColor:"#fff",bold:false}},

       
              ],              
          },
          
        ]
       },
      footer:{
        title: this.datepipe.transform(this.dateStart, dateTimeFormat)+ ' - ' + this.datepipe.transform(this.dateEnd, dateTimeFormat)
      }    
    };
  }

  buildDateNow() {
    this.datePicker.refresh();
    this.deviceId = '';
    this.status = '';
    this.dataTable.update({
        data: [],
      });
    setTimeout( () => {
      $('.kt_selectpicker').selectpicker('refresh');
    });
    
  }

  private buildForm(): void {
    this.searchFormSummary = this.formBuilder.group({
      dateStart: [''],
      dateEnd: [''],
      deviceId: [''],
      status: ['']
    });

    
  }

  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
    // this.renderConfig();
   }


  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
    };
  }

  ChangeUserTree(value) {
    if (value.id > 0) {
      this.listDevicePicker.reset();
      this.userIdSelectedEdit = value.id;
      this.getDevices(value.id);
      this.buildDateNow();
      this.dataTable.update({
        data: [],
        totalRecod: 0
      });
    }
  }

  getNameDevice(device) {
    let deviceId = device.target.value;
    let obj = this.listDevices.find(x => x.id == deviceId);
    this.deviceName = obj.name;    
  }

  getNameStatus(status){
    let listStatus = '';
    if(this.searchFormSummary.value.status == '')
    {
       
        this.listStatus.map(status=>{
            listStatus = listStatus+','+status.name;
            return status;
        })
    }else{
        let listStatusId = this.searchFormSummary.value.status;
    }
  }

  getDevices(userId) {
    let params = {
      userId: userId, 
      pageNo: -1
    };
    // this.filter.pageNo = -1;
    this.deviceService.list({ params: params }).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      this.listDevices = data.result;
      this.paramsEmiter.emit(this.listDevices);

    });
  }

  changeChecked(event){
    this.listDeviced = event.listChecked;
  }


  checkSensorFuel(nameSensor)
  {
    let check = false;
    this.listDevices.find(device =>{
     let sensor =  device.sensors.find(o => o.name == nameSensor);
     if(sensor != undefined)
     {
      if(JSON.parse(sensor.parameters).typeSensor == 'fuel') check = true;
     }
      
    });
    
    return check;
  }
  

  private refreshEventType()
  {
   this.translate.onLangChange.subscribe((event) => {
     let titleDevice = this.translate.instant('REPORT.ROUTE.GENERAL.CHOISE_DEVICE');
     $(function () {
       $('.input-device').selectpicker({title: titleDevice}).selectpicker('refresh');
     });
   });
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  validateForm(deviceId: any) {
    if (deviceId == '' || deviceId == undefined || deviceId == null) {
      this.toast.show(
        {
          message: this.translate.instant('MESSEAGE_CODE.SENSOR.DEVICE_NOT_FOUND'),
          type: 'error',
        })
      return false;
    }
    return true;
  }


  searchSummary(form: any) {
    if(this.getListFilter()) 
    {
      this.showFullData = true;
      this.dataTable.reload({ currentPage: 1 });
    }
    // this.dataTable.reload({ currentPage: 1 });

  }

  checkbetween(value)
  {
    this.checkbetweenChange = value;
  }


  resetFormSearch() {
    this.listDevicePicker.reset();
    this.buildDateNow();
  }


  async getData(filter,type?:string) {
    if(type == 'pdf') this.isdownLoadPDF =true;
    if(type == 'excel') this.isdownLoad = true;

    let _this = this;
    // filter.timeFrom = '2020-06-11 00:00:00';
    // filter.timeTo = '2020-06-12 23:59:00';
    // filter.deviceId = 385;
  await this.tollStation.list(filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          if(type == 'pdf') _this.isdownLoadPDF = false;
          if(type == 'excel') _this.isdownLoad = false;
          let all_amount= [];
          let pageNo = this.filter.pageNo - 1;
          let pageSize = this.filter.pageSize;
          let j = 0;
          result.result = result.result.map(item=>{
               let toltalAmount = [];
               if(!item.tollFees) item.tollFees = [];
               item.data = item.tollFees.map(tollFee=>{
                   j++;
                    let count = pageNo * pageSize + j;
                 
                    tollFee.count = count;
                    //all one device
                    if(toltalAmount.find(x=>x.unit == tollFee.unit))
                    {
                        let existObj = toltalAmount.find(x=>x.unit == tollFee.unit);
                        existObj.amount = existObj.amount + tollFee.amount;
                    }else{
                        toltalAmount.push({
                            unit:tollFee.unit,
                            amount:tollFee.amount
                        });
                    }

                    // all device
                    if(all_amount.find(x=>x.unit == tollFee.unit))
                    {
                        let existObj = all_amount.find(x=>x.unit == tollFee.unit);
                        existObj.amount = existObj.amount + tollFee.amount;
                    }else{
                        all_amount.push({
                            unit:tollFee.unit,
                            amount:tollFee.amount
                        });
                    }
                
                    let amount = new Intl.NumberFormat().format(tollFee.amount)+ ' '+ tollFee.unit;
                    tollFee.amount = amount;
                    tollFee.fuel = tollFee.tollStations.map(toll=>{
                     toll.roadName = item.roadName;
                     toll.segmentName = item.segmentName;
                     toll.time = this.userDateAdv.transform(toll.timestamp,'datetime');
                    //  listToll.push(toll);
                     return toll;
                 });
                 
                 return tollFee;
              });

            //   item.listToll = listToll;
              let textTotalAmount = '';
              toltalAmount = toltalAmount.filter(itemTotalAmount=>{
                return itemTotalAmount.amount!= 0 && itemTotalAmount.unit != '';
              });
              toltalAmount.map((total,i)=>{
  
                  let itemText = new Intl.NumberFormat().format(total.amount)+ ' '+ total.unit;
                  if(i == toltalAmount.length - 1) textTotalAmount = textTotalAmount + itemText;
                  if(i != toltalAmount.length - 1) textTotalAmount = textTotalAmount + itemText+ ', ';

              });
              if(textTotalAmount == '') textTotalAmount = '0';
              item.totalAmount = textTotalAmount;
              return item;
          })

          // convert all amount
          let alltextTotalAmount = '';
          all_amount = all_amount.filter(itemTotalAmount=>{
            return itemTotalAmount.amount!= 0 && itemTotalAmount.unit != '';
          });
          all_amount.map((total,i)=>{
                let itemText = new Intl.NumberFormat().format(total.amount)+ ' '+ total.unit;
                if(i == all_amount.length - 1) alltextTotalAmount = alltextTotalAmount + itemText;
                if(i != all_amount.length - 1) alltextTotalAmount = alltextTotalAmount + itemText+ ', ';
            
            });
              
            if(alltextTotalAmount == '') alltextTotalAmount = '0';  
           this.totalAllRowData = {
                amount:alltextTotalAmount
            };

         this.dataPagination = result.result;
         this.dataExport = result.result;
    
         this.dataPagination =  this.dataPagination.filter(item=>{
          let data = item.data;
          if(data.length == 0) return false;
          return true;
        });
         this.getDataShowPage();
         if(j == 0) j = 1;
          if(!type)
          {
            this.dataTable.update({
              data:this.data,
              totalRecod: j
            });
          }
          if(type == 'pdf')     this.exportFilePDF(result.result);
          if(type == 'excel')    this.exportFileXLSX(this.data);
          this.showFullData = false;
        }
      })
  }

  getDataShowPage()
  {
    this.data = [];
    for(let i=0;i<this.dataPagination.length;i++)
    {
        
        this.data.push({
          name:this.dataPagination[i].name,
          totalAmount:this.dataPagination[i].totalAmount,
          data:[]
        }); 
        let data = this.dataPagination[i].data;
        for(let j=0;j<data.length;j++)
        {
          let count = this.dataPagination[i].data[j].count;
          if(count > (this.filter.pageNo-1)*this.filter.pageSize && count <= (this.filter.pageNo)*this.filter.pageSize)
          {
            this.data[i].data.push(this.dataPagination[i].data[j]);
          }
        }
    
    }

    this.data =  this.data.filter(item=>{
      let data = item.data;
      if(data.length == 0) return false;
      return true;
    });

  }


  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;
        if(this.showFullData)
        {
          this.getData(this.filter);
        }else{
          this.dataTable.isLoading = true;
          setTimeout(() => {
            this.getDataShowPage();
            this.dataTable.update({
              data: this.data
            });
            this.cdr.markForCheck();
            this.cdr.detectChanges();

            this.dataTable.isLoading = false;

          },300);
       
        }
      }),

    ).subscribe();
  }

  convertNumberToTime(number) {
    return this.userDate.convertNumberToTime(number);
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
        responsive: false,

      }
    });
  }

  dateSelectChangeSearchChart(data)
  {
    this.dateStartChart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEndChart = data.endDate.format("YYYY-MM-DD HH:mm:ss");
  }

  renderDatePickerChart(dateStartChart,endDate)
  {
    this.datePickerOptionsSearchChart = {
      size: 'md',
      select: 'from',
      ranges: 'from',
      optionDatePicker: {
        maxSpan: {
          days: 31
        }

      },
      autoSelect: true,
      singleDatePicker: false,
      timePicker: true,
      startDate:dateStartChart,
      endDate:endDate
    }
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  roundNumber(number) {
    return Math.round(number * 100) / 100;
  }

  async getdataExcel(file) {
    if(file =='pdf')
    {this.isdownLoadPDF = true;}else
    {this.isdownLoad = true; } 
    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;
    this.filter.deviceId = this.searchFormSummary.value.deviceId; 
    this.getData(this.filter,file);

  }

  //pdf
  async exportPDF(){
       // this.filter.deviceId = this.
      if(this.getListFilter()) this.getdataExcel('pdf');
      
     
  }
  
  getListFilter(){
    if(this.listDeviced.length == 0)
    {
      this.listDevices.forEach(device => {
        this.listDeviced.push(device.id);
      });
    }    // this.filter.format = 'excel';
    // this.filter.report = "status_history";
    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;   
    this.filter.deviceIds = this.listDeviced.toString();
    if(!this.validateForm(this.filter.deviceIds)) return false;
    return true;
  }

  exportFilePDF(data){
    this.renderConfig();
    this.pdf.ExportPdf([this.dataExport],this.configExport);
  }
  
  //export
  exportXlSX() {
  
     if(this.getListFilter())  this.getdataExcel('excel');
    

  }

  convertTextData(text)
  {    
    return this.exportService.convertTextData(text);
  }

  private exportFileXLSX(data) {

    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let config: XLSXModelTest = {
      file: {
        title: this.translate.instant('REPORT.TOLL_STATION.GENERAL.REPORT_NAME'),
        prefixFileName: "TollFee",
        timeStart:this.dateStart,
        timeEnd:this.dateEnd,
        columnParent:'fuel',

      },
      template: [{
        header: [
          {
            text: this.translate.instant('REPORT.TOLL_STATION.GENERAL.REPORT_NAME'),
            type: "header",
          },
          {
            text: ""
          },
          {
            text: '!timezone',
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START') + ": " + this.dateStart,
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_END') + ": " + this.dateEnd,
          },
          
          {
            text: ""
          },
        ],
        columns: [
          {
            name: "#",
            columnData: "count",
            wch: 5,
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            merge:{
              rowSpan:1
            }
          },
          {
            name: this.translate.instant('ADMIN.TOLL_SEGMENT.COLUMN.ROAD'),
            columnData: "roadName",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 30,
            merge:{
                rowSpan:1
              }
          },

          {
            name: this.translate.instant('REPORT.TOLL_STATION.COLUMN.SEGMENT'),
            columnData: "segmentName",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 30,
            merge:{
                rowSpan:1
              }
          },
     
          {
            name: this.translate.instant('ADMIN.TOLL_STATION.COLUMN.PRICE'),
            columnData: "amount",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "amount",
            wch: 15,
            merge:{
                rowSpan:1
              }
          },
          {
            name: this.translate.instant('REPORT.TOLL_STATION.COLUMN.TOLL'),
            columnData: "time",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 25,
            merge:{
                colSpan:2
              },
              columnParent:'fuel'
 
          },
          {
            name: this.translate.instant('REPORT.STATUS_HISTORY.COLUMN.DURATION'),
            columnData: "name",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 30,
            columnParent:'fuel'

          },
          {
            name:this.translate.instant('DASHBOARD.STATIC.DISTANCE_LABEL'),
            columnData: "address",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            columnParent:'fuel',

            wch:30,
          },

      
        ],
        columnsMerge:
        {
          data: [
            "","","","",
            this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),
            this.translate.instant('COMMON.COLUMN.NAME'),
            this.translate.instant('COMMON.COLUMN.ADDRESS'),
          ],
          // position: ["A8:A9","B8:B9","C8:E8","F8:H8","I8:J8"]
        },
        woorksheet: {
          name: this.deviceName,
        },
        total: {
            group: "name",
            totalCol:{
                field: [
                    {
                      text: this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL'),
                      style: {
                        font: { name: 'Times New Roman', sz: 14, bold: true },
                        alignment: { vertical: "right", horizontal: "right", indent: 0, wrapText: true }
                      },
                      merge: {
                        full: false,
                        range: "2"
                      }
                    },
                    {
                      text: " ",
                    },
                    {
                      text: " ",
                    },
                    
        
                    {
                      columnData: "totalAmount",
                      style: {
                        font: { name: 'Times New Roman', sz: 14, bold: true },
                        alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true }
                      }
                    },
               
                    {
                        text: " ",
                      },
                      {
                        text: " ",
                      },
                      {
                        text: " ",
                      },
                  ]
            },
            totalAll: {
                position: {
                  top: 2,
                  left:5
                },
                data: [
    
                  {
                    key: this.translate.instant('REPORT.TOLL_STATION.COLUMN.ALL_PRICE'),
                    value: this.totalAllRowData.amount,
                    style: { alignment: { vertical: "center", horizontal: "left", wrapText: true }, }
                  },
                
                ]
              }
        }
      }]

    };

    this.xlsx.exportFileTest([this.dataExport], config, {});
  }
  setColumns() {
    this.columns = [
        // {
        //     title: '#',
        //     field: 'no',
        //     allowSort: false,
        //     isSort: false,
        //     dataSort: '',
        //     style: { 'width': '20px' },
        //     class: 't-datatable__cell--center',
        //     translate: '#',
        //     autoHide: false,
        //     width: 20,
        //   },
        // {
        //     title: 'Road',
        //     field: 'road',
        //     allowSort: false,
        //     isSort: false,
        //     dataSort: '',
        //     style: { 'width': '200px'},
        //     class: '',
        //     translate: 'ADMIN.TOLL_SEGMENT.COLUMN.ROAD',
        //     autoHide: false,
        //     width: 200,
        //   },
        //   {
        //     title: 'Segment',
        //     field: 'segment',
        //     allowSort: false,
        //     isSort: false,
        //     dataSort: '',
        //     style: { 'width': '200px' },
        //     class: '',
        //     translate: 'REPORT.TOLL_STATION.COLUMN.SEGMENT',
        //     autoHide: false,
        //     width: 200,
        //   },
          {
            title: 'Time',
            field: 'time',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '200px','text-align':'center' },
            class: '',
            translate: 'COMMON.COLUMN.TIME',
            autoHide: false,
            width: 200,
          },
        //   {
        //     title: 'Amount',
        //     field: 'amount',
        //     allowSort: false,
        //     isSort: false,
        //     dataSort: '',
        //     style: { 'width': '100px'},
        //     class: '',
        //     translate: 'ADMIN.TOLL_STATION.COLUMN.PRICE',
        //     autoHide: false,
        //     width: 200,
        //   },
          {
            title: 'toll station',
            field: 'toll',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '300px' },
            class: '',
            translate: 'COMMON.COLUMN.NAME',
            autoHide: false,
            width: 200,
          },
          {
            title: 'Address',
            field: 'address',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '300px' },
            class: '',
            translate: 'COMMON.COLUMN.ADDRESS',
            autoHide: false,
            width: 200,
          },
          
    ]
  }


}




