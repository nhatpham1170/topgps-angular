import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef,EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { ReportSummaryDayService } from '@core/report';
import { DeviceService } from '@core/manage';
import { TranslateService } from '@ngx-translate/core';
import { ToastService, UserDatePipe,UserDateAdvPipe } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { ExcelService,PdfmakeService,ExcelTestService} from '@core/utils';
import { XLSXModel, XLSXModelTest } from '@core/utils/xlsx/excel.service';
import { PdfModel } from '@core/utils/pdfmake/pdfmake.service';

import { DatePipe } from '@angular/common';
import { UserDateService } from '@core/_base/layout';
import { DecimalPipe } from '@angular/common';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { waitDownloadModel } from '../wait-download/wait.download.service'
import { ExportService } from '@core/utils';

// end chart
declare var $: any;

@Component({
  selector: 'report-summary-day',
  templateUrl: './report-summary-day.component.html',
  styleUrls: ['./report-summary-day.component.scss'],
  providers: [DatePipe, UserDatePipe, DecimalPipe,UserDateAdvPipe]

})
export class ReportSummaryDay implements OnInit {
  @ViewChild('datePicker', { static: true }) 

  datePicker: DateRangePickerComponent;
  @ViewChild('datePickerSearchChart', { static: true }) 

  datePickerSearchChart: DateRangePickerComponent;
  searchFormChart:FormGroup;
  searchFormSummary: FormGroup;
  selectdp: ElementRef;
  public datePickerOptions: any;
  public datePickerOptionsSearchChart:any;

  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  // show full data
  public showFullData:boolean = true;
  public dataPagination : any = [];
  public dataExport: any = null;
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataDefault: any = [{}];
  public filter: any;
  private unsubscribe: Subject<any>;
  public listDevices: any = [];
  public listSensors: any = [];
  public userIdSelected: number;
  public userIdSelectedEdit: number;
  public showUserTree: boolean = false;
  public dateStart: string;
  public dateEnd: string;
  public timeStart: string;
  public timeEnd: string;
  //time search chart
  public dateStartChart:string;
  public dateEndChart:string;
  public sensorIdSearhChart:any;
  public paramsEmiter: EventEmitter<waitDownloadModel>;
  public deviceId: string;
  public deviceIdSearhChart:any;
  public deviceName: string;
  public dateFormat: string;
  public timeFormat: string;
  public timeZone:string;
  public totalAllRowData : any;
  public parentBread:any;
  public isdownLoad : boolean = false;
  public isdownLoadPDF:boolean = false;
  public closeResult: string;
  public checkbetweenChange:boolean = false;

  // fuel
  public showFuel:boolean = true;

  // export
  public configExport:PdfModel;
 // configChart
  constructor(
    private summary: ReportSummaryDayService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private deviceService: DeviceService,
    private translate: TranslateService,
    private toast: ToastService,
    private currentUser: CurrentUserService,
    private xlsx: ExcelService,
    public datepipe: DatePipe,
    private userDate: UserDateService,
    private userDatePipe: UserDatePipe,
    private pdf : PdfmakeService,
    private userDateAdv : UserDateAdvPipe,
    private exportService:ExportService

  ) {
    this.paramsEmiter = new EventEmitter();
    
    this.dateFormat = this.currentUser.dateFormatPipe;
    this.timeFormat = this.currentUser.timeFormat;
    this.timeZone = this.currentUser.currentUser.timezone;
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.parentBread = [
      {
        name: this.translate.instant('REPORT.REPORTS.GENERAL.REPORTS'),
        translate: "REPORT.REPORTS.GENERAL.REPORTS",
        link: "/reports",
        icon: "flaticon2-graph"
      }
    ]
  }

  ngOnInit() {
    this.updateDataTable();
    this.refreshEventType();
    this.datePickerOptions = this.objOption();
    // console.log(this.userDatePipe.transform('1574380755', "dateTime", null, "datetime"));
    let temp:any =  1574380755;
  }

  objOption()
  {
    let obj = {
      size: 'md',
      select: 'from',
      ranges: 'from',
      optionDatePicker: {
        maxSpan: {
          days: 31
        }

      },
      autoSelect: true,
      singleDatePicker: false,
      timePicker: true,
      startDate:moment().subtract(1, "days").startOf('day'),
      endDate:moment().subtract(1, "days").endOf('day'),
    }; 
    return obj;
  }

  renderConfig()
  {
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let totalFuel =
     [
        { text:''},
        { text:''},
        { text:''},
        { columnData:'totalConsume',style:{alignment:"left",bold:true,fillColor:"#fff"}},
        { columnData:'totalAdd',style:{alignment:"left",bold:true,fillColor:"#fff"}},
        { columnData:'totalRemove',style:{alignment:"left",bold:true,fillColor:"#fff"}},
    ];

    let arrHeader = [
      { text: this.translate.instant('MENU.FUEL'),colSpan:6,alignment:"center",fillColor:"#EEEEEE",bold:true},
      { text:''},{ text:''},{ text:''},{ text:''},{ text:''}
    ];

    let arrMergeFuel = [   
      { text: this.translate.instant('COMMON.COLUMN.SENSORS'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
      { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.START_DAY'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
      { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.END_DAY'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
      { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.CONSUME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
      { text: this.translate.instant('REPORT.FUEL.GENERAL.ADD'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
      { text: this.translate.instant('REPORT.FUEL.GENERAL.REMOVE'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]}
    ];

    let arrBody = [
      {columnParent:'fuel',columnData:'name',style:{alignment:"left",fillColor:"#fff",bold:false}},
      {columnParent:'fuel',columnData:'start',style:{alignment:"left",fillColor:"#fff",bold:false}},
      {columnParent:'fuel',columnData:'end',style:{alignment:"left",fillColor:"#fff",bold:false}},
      {columnParent:'fuel',columnData:'consume',style:{alignment:"left",fillColor:"#fff",bold:false}},
      {columnParent:'fuel',columnData:'add',style:{alignment:"left",fillColor:"#fff",bold:false}},
      {columnParent:'fuel',columnData:'remove',style:{alignment:"left",fillColor:"#fff",bold:false}},
    ];
    
    this.configExport = {
      info: {
        title: 'SummaryByDays',
        columnParent:'fuel',

      },
      content:{
        header:{
          title:this.translate.instant('MENU.REPORT_SUMMARY_DAY'),
          params:{
            deviceName:": "+this.deviceName,
            timeStart:": "+this.dateStart,
            timeEnd:": "+this.dateEnd,
            timeZone:": "+this.timeZone
          }
        },
        table:[
          {
              woorksheet: {
                name: this.deviceName,
              },
              total : {
                // group : [
                //   { columnData:'deviceName'},
                // ],
                totalCol:[
                    { text:this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL'),style:{colSpan:2,alignment:"center",bold:true,fillColor:"#fff"}},
                    { text:''},
                    { columnData:'totalDistance',style:{alignment:"left",bold:true,fillColor:"#fff"}},
                    { text:''},
                    { text:''},
                    { columnData:'totalEngine',style:{alignment:"center",bold:true,fillColor:"#fff"}},
                    { columnData:'totalStopEngine',style:{alignment:"center",bold:true,fillColor:"#fff"}},
                    { columnData:'totalMaxSpeed',style:{alignment:"left",bold:true,fillColor:"#fff"}},
                    { columnData:'totalAvgSpeed',style:{alignment:"left",bold:true,fillColor:"#fff"}}
      
                ]
              },
              headerRows: 2,
              widths: ['4%','12%','12%','12%','12%','12%','12%','12%','12%'],
              header :[           
                  [ 
                    { text: '#',rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                    { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.DATE'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                    { text: this.translate.instant('DASHBOARD.STATIC.DISTANCE_LABEL'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},

                    { text: this.translate.instant('DASHBOARD.STATIC.DURATION'),colSpan:4,alignment:"center",fillColor:"#EEEEEE",bold:true},
                    { text:''},
                    { text:''},
                    { text:''},
                    { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.SPEED'),colSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true},
                    { text:''}
                  ],
                  [
                    '',
                    '',
                    '',
                    { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.STARTED_WORK'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.FINISHED_WORK'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.ENGINE'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},

                    { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.STOP_ENGINE'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.MAX'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},  
                    { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.AVG'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]}
                  ]
              ],
              body:[
                {auto:true},
                {columnData:'date',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'distance',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'startTime',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'endTime',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'totalEngineOnTime',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'stopEngineOntime',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'maxspeed',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'avgspeed',style:{alignment:"left",fillColor:"#fff",bold:false}}
       

              ],              
          },
          
        ]
          },
      footer:{
        title: this.datepipe.transform(this.dateStart, dateTimeFormat)+ ' - ' + this.datepipe.transform(this.dateEnd, dateTimeFormat)+', '+this.deviceName
      }    
    };

    if(this.showFuel)
    {
      this.configExport.content.table[0].total.totalCol = this.configExport.content.table[0].total.totalCol.concat(totalFuel);
      this.configExport.content.table[0].header[0] = this.configExport.content.table[0].header[0].concat(arrHeader);
      this.configExport.content.table[0].header[1] = this.configExport.content.table[0].header[1].concat(arrMergeFuel);
      this.configExport.content.table[0].body = this.configExport.content.table[0].body.concat(arrBody);
      this.configExport.content.table[0].widths = ['2%','7%','7%','7%','7%','7%','7%','7%','7%','7%','7%','7%','7%','7%','7%'];
    }
  }

  buildDateNow() {
    this.datePicker.refresh();
    this.deviceId = '';
    setTimeout( () => {
      $('.kt_selectpicker').selectpicker('refresh');
    });
  }

  private buildForm(): void {
    this.searchFormSummary = this.formBuilder.group({
      dateStart: [''],
      dateEnd: [''],
      deviceId: ['']
    });
    this.searchFormChart = this.formBuilder.group({
      deviceIdSearhChart: ['',],
      sensorName:['']
    });
    
  }

  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
    this.renderConfig();
    // console.log(this.dateStart);
   }


  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
    };
  }

  ChangeUserTree(value) {
    if (value.id > 0) {
      this.userIdSelectedEdit = value.id;
      this.getDevices(value.id);
      this.buildDateNow();
      this.dataTable.update({
        data: [],
        totalRecod: 0
      });
    }
  }

  getNameDevice(device) {
    let deviceId = device.target.value;
    let obj = this.listDevices.find(x => x.id == deviceId);
    this.deviceName = obj.name;
    let sensors = obj.sensors;
    this.showFuel = false;
    if(sensors.length > 0 )
    {
      sensors = sensors.filter(sensor => {
        if(JSON.parse(sensor.parameters).typeSensor == 'fuel')
        {
          this.showFuel = true;
          return sensor;
        }
      })    
    }
    // console.log(this.deviceName);
    
  }

  getDevices(userId) {
    let params = {
      userId: userId, 
      pageNo: -1
    };
    // this.filter.pageNo = -1;
    this.deviceService.list({ params: params }).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      this.listDevices = data.result;
      setTimeout( () => {
        $('.kt_selectpicker').selectpicker('refresh');
      });
    });

  }

  checkSensorFuel(nameSensor)
  {
    let check = false;
    this.listDevices.find(device =>{
     let sensor =  device.sensors.find(o => o.name == nameSensor);
     if(sensor != undefined)
     {
      if(JSON.parse(sensor.parameters).typeSensor == 'fuel') check = true;
     }
      
    });
    
    return check;
  }
  

  private refreshEventType()
  {
   this.translate.onLangChange.subscribe((event) => {
     let titleDevice = this.translate.instant('REPORT.ROUTE.GENERAL.CHOISE_DEVICE');
     $(function () {
       $('.input-device').selectpicker({title: titleDevice}).selectpicker('refresh');
     });
   });
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  validateForm(deviceId: any) {
    if (deviceId == '' || deviceId == undefined || deviceId == null) {
      this.toast.show(
        {
          message: this.translate.instant('MESSEAGE_CODE.SENSOR.DEVICE_NOT_FOUND'),
          type: 'error',
        })
      return false;
    }
    return true;
  }


  searchSummary(form: any) {
    if (!this.validateForm(form.value.deviceId)) return;
    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;
    this.filter.deviceId = form.value.deviceId;
    this.filter.report = 'summary_by_day';
    this.showFullData = true;
    
    this.paramsEmiter.emit(this.filter);
    if(!this.checkbetweenChange) this.dataTable.reload({ currentPage: 1 });
    // this.dataTable.reload({ currentPage: 1 });

  }

  checkbetween(value)
  {
    this.checkbetweenChange = value;
  }


  resetFormSearch() {
    this.buildDateNow();
  }

  resetFormSearchChart(){
      $('#deviceChart').val('').selectpicker("refresh");
      $('#sensorChart').val(-1).selectpicker("refresh");
      this.searchFormChart.reset();
    // this.datePickerSearchChart.refresh();
  
  }

  async getData(filter,type?:string) {
    if(type == 'pdf') this.isdownLoadPDF =true;
    if(type == 'excel') this.isdownLoad = true;

    let _this = this;
    //test
    // filter.deviceId = '3';

    // filter.deviceIds = '3';
  await this.summary.list(filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          if(type == 'pdf') _this.isdownLoadPDF = false;
          if(type == 'excel') _this.isdownLoad = false;
          let pageNo = this.filter.pageNo - 1;
          let pageSize = this.filter.pageSize;
          let j = 1;
          let totalDistance = 0;
          let totalEngine  = 0;
          let totalStopEngine = 0;
          let totalMaxSpeed = 0;
          let totalAvgSpeed = 0;
          let totalConsume = 0;
          let totalAdd = 0;
          let totalRemove = 0;
          this.totalRecod = result.result.length;
          result.result = result.result.filter(item=>{
            if(j > pageNo*pageSize && j <= (pageNo+1)*pageSize -1)
            {
              let count = pageNo * pageSize + j;
              if(item.startTime == 0) 
              {
                item.startTime = '00:00:00';
              } else{
                item.startTime = this.userDateAdv.transform(item.startTime,'HH:mm:ss');
              }
              if(item.endTime == 0) 
              {
                item.endTime = '00:00:00';
              } else{
                item.endTime = this.userDateAdv.transform(item.endTime,'HH:mm:ss');
              }
              //total
              totalDistance = totalDistance + item.distance;
              totalEngine   = totalEngine + item.totalEngineOnTime;
              totalStopEngine = totalStopEngine + item.stopEngineOntime;
              if(totalMaxSpeed < item.maxspeed) totalMaxSpeed = item.maxspeed; 
              // end total
              item.distance = this.roundNumber(item.distance);
              item.avgspeed = this.roundNumber(item.avgspeed);
              item.maxspeed = this.roundNumber(item.maxspeed);
  
              item.totalEngineOnTime = this.convertNumberToTime(item.totalEngineOnTime);
              item.stopEngineOntime = this.convertNumberToTime(item.stopEngineOntime);
              item.duration = this.convertNumberToTime(item.duration);
              item.fuel = item.fuel.map(itemFuel => {
                itemFuel.nameSensor = itemFuel.name;
                itemFuel.name = itemFuel.name+" (lit)";
                itemFuel.add = this.roundNumber(itemFuel.add);
                itemFuel.remove = this.roundNumber(itemFuel.remove);
                itemFuel.consume = this.roundNumber(itemFuel.consume);
                itemFuel.consume = Math.abs(itemFuel.consume);
                // item excel
                item.name = itemFuel.name;
                item.add = itemFuel.add;
                item.remove = itemFuel.remove;
                item.consume = itemFuel.consume;
                item.start = itemFuel.start;
                item.end = itemFuel.end;
                // end item excel
                //total
                totalAdd = totalAdd + itemFuel.add;
                totalConsume = totalConsume + Math.abs(itemFuel.consume);
                totalRemove = totalRemove + itemFuel.remove;
                return  itemFuel;
              });
              if(item.fuel.length == 0) item.fuel.push({nametext:''});
              item.count = count;
             
              return item;
            }
            j++;
          });
          totalAvgSpeed = Math.round(totalDistance / (totalEngine/3600) * 100) / 100
          if(totalEngine == 0) totalAvgSpeed = 0;
          this.dataExport = [
            [
              {
              totalDistance : this.roundNumber(totalDistance),
              totalEngine : this.convertNumberToTime(totalEngine),
              totalStopEngine : this.convertNumberToTime(totalStopEngine),
              totalMaxSpeed : this.roundNumber(totalMaxSpeed),
              totalAvgSpeed : totalAvgSpeed,
              totalConsume : this.roundNumber(totalConsume),
              totalAdd : this.roundNumber(totalAdd),
              totalRemove : this.roundNumber(totalRemove),
              data: result.result
            }
          ]
        ];
     
          this.data = result.result;
          this.dataPagination = result.result;
          // console.log(result.result);
          if(!type)
          {
          
            this.dataTable.update({
              data: this.data,
              totalRecod: this.totalRecod
            });
          }
          if(type == 'pdf')     this.exportFilePDF(this.data);
          if(type == 'excel')    this.exportFileXLSX(this.data);
          this.showFullData = false;
          setTimeout( () => {
            $('.kt_selectpicker').selectpicker('refresh');
          });

        }
      })
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;
        // console.log(this.filter.pageNo );
        if(this.showFullData)
        {
          this.getData(this.filter);
        }
          else
        {
          let i = 0;
          this.data = this.dataPagination.filter(data =>{
            
            if(i >= (this.filter.pageNo-1)*this.filter.pageSize && i <= (this.filter.pageNo)*this.filter.pageSize )
            {
              return data;
            }
            i++;
           
          });
          this.dataTable.update({
            data: this.data
          });
        }
        
      }),

    ).subscribe();
  }

  convertNumberToTime(number) {
    return this.userDate.convertNumberToTime(number);
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
        responsive: false,

      }
    });
  }

  dateSelectChangeSearchChart(data)
  {
    this.dateStartChart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEndChart = data.endDate.format("YYYY-MM-DD HH:mm:ss");
  }

  renderDatePickerChart(dateStartChart,endDate)
  {
    this.datePickerOptionsSearchChart = {
      size: 'md',
      select: 'from',
      ranges: 'from',
      optionDatePicker: {
        maxSpan: {
          days: 31
        }

      },
      autoSelect: true,
      singleDatePicker: false,
      timePicker: true,
      startDate:dateStartChart,
      endDate:endDate
    }
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  roundNumber(number) {
    return Math.round(number * 100) / 100;
  }

  async getdataExcel(file) {
    if(file =='pdf')
    {this.isdownLoadPDF = true;}else
    {this.isdownLoad = true; } 
    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;
    this.filter.deviceId = this.searchFormSummary.value.deviceId; 
    this.getData(this.filter,file);

  }

  //pdf
  async exportPDF(){
    let validate = this.validateForm(this.searchFormSummary.value.deviceId);
    if (validate )
     {
      this.filter.format = 'pdf';
      // this.filter.deviceId = this.
      this.getListFilter();
      if(!this.checkbetweenChange) this.getdataExcel('pdf');
     }
  }
  
  getListFilter(){
    this.filter.deviceId = this.searchFormSummary.value.deviceId;
    this.filter.format = 'excel';
    this.filter.report = "summary_by_day";
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;    
    this.paramsEmiter.emit(this.filter);

  }

  exportFilePDF(data){
    this.renderConfig();
    this.pdf.ExportPdf(this.dataExport,this.configExport);
  }
  
  //export
  exportXlSX() {
    let validate = this.validateForm(this.searchFormSummary.value.deviceId);
    if (validate )
     {
      this.filter.format = 'excel';
      this.getListFilter();
      if(!this.checkbetweenChange) this.getdataExcel('excel');

     }
  }

  convertTextData(text)
  {    
    return this.exportService.convertTextData(text);
  }

  private exportFileXLSX(data) {

    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let totalFuel = [
      {text: " "},{text: " "},{text: " "},
      {
        columnData: "totalConsume",
        style: {
          font: { name: 'Times New Roman', sz: 14, bold: true },
          alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true }
        }
      },
      {
        columnData: "totalAdd",
        style: {
          font: { name: 'Times New Roman', sz: 14, bold: true },
          alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true }
        }
      },
      {
        columnData: "totalRemove",
        style: {
          font: { name: 'Times New Roman', sz: 14, bold: true },
          alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true }
        }
      },
    ];
    let arrFuel = [
      {
        name: this.translate.instant('MENU.FUEL'),
        columnData: "name",
        style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
        type: "mergeTime",
        wch: 15,
        merge:{
          colSpan:5
        },
        columnParent:'fuel'
      },
      {   
        name: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.SPEED'),
        wch: 15,
        columnData: "start",
        columnParent:'fuel',
        style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
      },
      {   
        name: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.SPEED'), 
        wch: 15,
        columnData: "end",
        columnParent:'fuel',
        style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
      },
      {  
        name: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.SPEED'), 
        wch: 15,
        columnData: "consume",
        columnParent:'fuel',
        style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
      },
      {   
        name: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.SPEED'),
        wch: 15,
        columnData: "add",
        columnParent:'fuel',
        style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
      },
      {   
        name: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.SPEED'),
        wch: 15,
        columnData: "remove",
        columnParent:'fuel',
        style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
      },
    ];

    let arrMergeFuel = [   
      this.translate.instant('COMMON.COLUMN.SENSORS'), 
      this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.START_DAY'),
      this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.END_DAY'),
      this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.CONSUME'),
      this.translate.instant('REPORT.FUEL.GENERAL.ADD'),
      this.translate.instant('REPORT.FUEL.GENERAL.REMOVE')
    ];

    let config: XLSXModelTest = {
      file: {
        title: this.translate.instant('MENU.REPORT_SUMMARY_DAY'),
        prefixFileName: "SummaryByDays",
        columnParent:'fuel',
        devices:this.deviceName,
        timeStart:this.dateStart,
        timeEnd:this.dateEnd,
      },
      template: [{
        header: [
          {
            text: this.translate.instant('MENU.REPORT_SUMMARY_DAY'),
            type: "header",
          },
          {
            text: ""
          },
          {
            text: '!timezone',
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START') + ": " + this.dateStart,
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_END') + ": " + this.dateEnd,
          },
          {
            text: this.translate.instant('MANAGE.DEVICE.GENERAL.DEVICE_NAME') + ": " + this.deviceName,
          },
          {
            text: ""
          },
        ],
        columns: [
          {
            name: "#",
            columnData: "auto",
            wch: 5,
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            merge:{
              rowSpan:1
            }
          },
          {
            name:this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.DATE'),
            columnData: "date",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            merge:{
              rowSpan:1
            },
            wch:20,
          },
          {
            name: this.translate.instant('DASHBOARD.STATIC.DISTANCE_LABEL'),
            columnData: "distance",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
            merge:{
              rowSpan:1
            },
          },
          {
            name: this.translate.instant('DASHBOARD.STATIC.DURATION'),
            columnData: "startTime",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
            merge:{
              colSpan:3
            },
          },
          {   
            name: this.translate.instant('DASHBOARD.STATIC.DURATION'),
            wch: 20,
            columnData: "endTime",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
          },
          {   
            name: this.translate.instant('DASHBOARD.STATIC.DURATION'),
            wch: 20,
            columnData: "totalEngineOnTime",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
          },
          {   
            name: this.translate.instant('DASHBOARD.STATIC.DURATION'),
            wch: 20,
            columnData: "stopEngineOntime",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
          },
       
          {
            name: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.SPEED'),
            columnData: "maxspeed",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
            merge:{
              colSpan:1
            },
          },
          {   
            name: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.SPEED'),
            wch: 20,
            columnData: "avgspeed",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
          },
      
        ],
        columnsMerge:
        {
          data: [
            "",
            "",
            "",
            this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.STARTED_WORK'),
            this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.FINISHED_WORK'),
            this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.ENGINE'),
            this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.STOP_ENGINE'),
            this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.MAX'),
            this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.AVG')
         

          ],
          // position: ["A8:A9","B8:B9","C8:E8","F8:H8","I8:J8"]
        },
        woorksheet: {
          name: this.deviceName,
        },
        total: {
          totalCol: {
            field: [
              {
                text: this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL'),
                style: {
                  font: { name: 'Times New Roman', sz: 14, bold: true },
                  alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true }
                },
                merge: {
                  full: false,
                  range: "1"
                },
                wch:5
              },
              {
               text: " "
              },
              {
                columnData: "totalDistance",
                style: {
                  font: { name: 'Times New Roman', sz: 14, bold: true },
                  alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true }
                }
              },
              {text: " "},{text: " "},

              {
                columnData: "totalEngine",
                style: {
                  font: { name: 'Times New Roman', sz: 14, bold: true },
                  alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true }
                }
              },
              {
                columnData: "totalStopEngine",
                style: {
                  font: { name: 'Times New Roman', sz: 14, bold: true },
                  alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true }
                }
              },
              {
                columnData: "totalMaxSpeed",
                style: {
                  font: { name: 'Times New Roman', sz: 14, bold: true },
                  alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true }
                }
              },
              {
                columnData: "totalAvgSpeed",
                style: {
                  font: { name: 'Times New Roman', sz: 14, bold: true },
                  alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true }
                }
              },
           
            ]
          }

        }
      }]

    };
    if(this.showFuel)
    {
      config.template[0].columns  = config.template[0].columns.concat(arrFuel);
      config.template[0].columnsMerge.data =   config.template[0].columnsMerge.data.concat(arrMergeFuel);
      config.template[0].total.totalCol.field = config.template[0].total.totalCol.field.concat(totalFuel);
    }

    this.xlsx.exportFileTest(this.dataExport, config, {});
  }
  setColumns() {
    this.columns = [

      {
        title: 'moveStart',
        field: 'moveStart',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px','text-align':'center'},
        styleNoFuel :{'width': '180px','text-align':'center'},
        class: '',
        translate: 'REPORT.SUMMARY.DAY.COLUMN.STARTED_WORK',
        autoHide: false,
        width: 200,
      },
      {
        title: 'moveEnd',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px','text-align':'center' },
        styleNoFuel :{'width': '180px','text-align':'center'},
        class: '',
        translate: 'REPORT.SUMMARY.DAY.COLUMN.FINISHED_WORK',
        autoHide: false,
        width: 200,
      },
      {
        title: 'Engine',
        field: 'Engine',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px','text-align':'center' },
        styleNoFuel :{'width': '180px','text-align':'center'},

        class: '',
        translate: 'REPORT.SUMMARY.DAY.COLUMN.ENGINE',     
        autoHide: false,
        width: 200,
      },
      {
        title: 'Stop Engine',
        field: 'moveStart',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px','text-align':'center' },
        styleNoFuel :{'width': '180px','text-align':'center'},

        class: '',
        translate: 'REPORT.SUMMARY.DAY.COLUMN.STOP_ENGINE',     
        autoHide: false,
        width: 200,
      },
      {
        title: 'Speed max',
        field: 'Speed max',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        styleNoFuel :{'width': '180px','text-align':'center'},

        class: '',
        translate: 'REPORT.SUMMARY.DAY.COLUMN.MAX',     

        autoHide: false,
        width: 200,
      },
      {
        title: 'Speed avg',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        styleNoFuel :{'width': '100%','text-align':'center'},

        class: '',
        translate: 'REPORT.SUMMARY.DAY.COLUMN.AVG',     
        autoHide: false,
        width: 200,
      },
      {
        title: 'Sensor',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'COMMON.COLUMN.SENSORS',     
        autoHide: false,
        width: 200,
        fuel:true

      },
      {
        title: 'Start day',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.SUMMARY.DAY.COLUMN.START_DAY',     
        autoHide: false,
        width: 200,
        fuel:true

      },
      {
        title: 'Speed avg',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.SUMMARY.DAY.COLUMN.END_DAY',     
        autoHide: false,
        width: 200,
        fuel:true

      },
      {
        title: 'Comsume',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.SUMMARY.DAY.COLUMN.CONSUME',     
        autoHide: false,
        width: 200,
        fuel:true

      },
      {
        title: 'Comsume',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '60px' },
        class: '',
        translate: 'REPORT.FUEL.GENERAL.ADD',     
        autoHide: false,
        width: 200,
        fuel:true

      },
      {
        title: 'Comsume',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '60px' },
        class: '',
        translate: 'REPORT.FUEL.GENERAL.REMOVE',     
        autoHide: false,
        width: 200,
        fuel:true
      },
    ]
  }


}




