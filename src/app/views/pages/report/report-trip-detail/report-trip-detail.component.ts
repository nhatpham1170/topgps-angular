import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef,EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { ReportGeofenceDetailService,ReportSummaryTripService } from '@core/report';
import { DeviceService,GeofenceService,GeofenceModel,TripService } from '@core/manage';
import { TranslateService } from '@ngx-translate/core';
import { ToastService, ValidatorCustomService, UserDatePipe, FunctionService } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { ExcelService,PdfmakeService,ExcelTestService} from '@core/utils';
import { XLSXModel, XLSXModelTest } from '@core/utils/xlsx/excel.service';
import { PdfModel } from '@core/utils/pdfmake/pdfmake.service';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { PageService,itemPage,UserDateAdvPipe} from '@core/_base/layout';

import { DatePipe } from '@angular/common';
import { UserDateService } from '@core/_base/layout';
import { DecimalPipe } from '@angular/common';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import * as moment from 'moment';
import { AlertPopup,AlertService } from '@core/manage';
import { EventEmitterService } from '@core/common/_service/eventEmitter.service'

declare var $: any;
@Component({
  selector: 'trip-detail',
  templateUrl: './report-trip-detail.component.html',
  styleUrls: ['./report-trip-detail.component.scss'],
  providers: [DatePipe, UserDatePipe, DecimalPipe,UserDateAdvPipe]

})
export class ReportTripDetailComponent implements OnInit {
    [x: string]: any;
  @ViewChild('listDevicePicker', { static: true }) 
  listDevicePicker:any;
  @ViewChild('datePicker', { static: true }) 
  datePicker: DateRangePickerComponent;
  timeStopInput:any;
  formRoute: FormGroup;
  searchFormGeofence: FormGroup;
  formCheckList: FormGroup;
  selectdp: ElementRef;
  public datePickerOptions: any = {
    size: 'md',
    select: 'from',
    ranges: 'from',
    optionDatePicker: {
      maxSpan: {
        days: 31
      }
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: true,
    startDate:moment().subtract(1, "days").startOf('day'),
    endDate:moment().subtract(1, "days").endOf('day'),
  };
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public dataExport: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataDefault: any ;
  public filter: any;
  private unsubscribe: Subject<any>;
  public listDevices: any = [];
  public listDeviced: any = [];

  public listTrip: any = [];
  public userIdSelected: number;
  public userIdSelectedEdit: number;
  public showUserTree: boolean = false;
  public dateStart: string;
  public dateEnd: string;
  public timeStart: string;
  public timeEnd: string;
  public timeStop:number;
  public deviceId: string;
  public deviceName: string;
  public dateFormat: string;
  public timeFormat: string;
  public timeZone:string;
  public totalAllRowData : any;
  public parentBread:any;
  public isdownLoad : boolean = false;
  public isdownLoadPDF:boolean = false;
  public closeResult: string;
  public geofenceId:any;
  public geofencePopup : any = {
    geofence:{points:[]}
  };
  public keySearchFilter:string = 'name';
  public objGeofenceSummary:itemPage;
  public DetailGeofenceEmiter: EventEmitter<any>;
  public paramsEmiter: EventEmitter<any>;

  constructor(
    private detail : ReportGeofenceDetailService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private deviceService: DeviceService,
    private translate: TranslateService,
    private toast: ToastService,
    private currentUser: CurrentUserService,
    private xlsx: ExcelService,
    public datepipe: DatePipe,
    private userDate: UserDateService,
    private userDatePipe: UserDatePipe,
    private pdf : PdfmakeService,
    private geofence: GeofenceService,
    private modalService: NgbModal,
    private page:PageService,
    private emiter:EventEmitterService,
    private alert: AlertService,
    private userDateAdv : UserDateAdvPipe,
    private detailTrip: ReportSummaryTripService,
    private trip : TripService,
    private functionService : FunctionService

  ) {    
    $(function () {
      $('.kt_selectpicker').selectpicker();
    });
    this.paramsEmiter = new EventEmitter();
    this.objGeofenceSummary = this.page.getPageById(122232);
    this.dateFormat = this.currentUser.dateFormatPipe;
    this.timeFormat = this.currentUser.timeFormat;
    this.timeZone = this.currentUser.currentUser.timezone;
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.parentBread = [
      {
        name: this.translate.instant('REPORT.REPORTS.GENERAL.REPORTS'),
        translate: "REPORT.REPORTS.GENERAL.REPORTS",
        link: "/reports",
        icon: "flaticon2-graph"
      }
    ];


  }

  ngOnInit() {
    this.refreshEventType();
    this.updateDataTable();
   
    if(this.emiter.geofenceDetail) 
    {

      console.log(this.emiter.geofenceDetail);
      return;
        this.datePickerOptions.startDate = this.emiter.geofenceDetail.timeStart;
        this.datePickerOptions.endDate = this.emiter.geofenceDetail.timeEnd;
        this.filter.deviceIds = this.emiter.geofenceDetail.deviceId.toString();
        this.filter.geofenceIds = this.emiter.geofenceDetail.geofenceId.toString();
        this.filter.timeFrom = this.emiter.geofenceDetail.timeStart;
        this.filter.timeTo = this.emiter.geofenceDetail.timeEnd;      
          console.log(this.filter);
      
       setTimeout(() => {
        this.dataTable.reload({ currentPage: 1 });
       });
        
       
    }
    
  }

  changeChecked(event){
    this.listDeviced = event.listChecked;
  }

  buildDateNow() {
    this.datePicker.refresh();
    this.deviceId = '';
    this.searchFormGeofence.reset();

  }

  private buildForm(): void {      
    this.searchFormGeofence = this.formBuilder.group({
      dateStart: [''],
      dateEnd: [''],
      deviceId: [''],
      tripId:[''],
    });
  }

  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
   }


  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
    };
  }

  ChangeUserTree(value) {
    if (value.id > 0) {
      this.userIdSelectedEdit = value.id;
      this.getDevices(value.id);
      this.getTrips(value.id);
      this.listDevicePicker.reset();

      // this.deviceId = '';
      this.buildDateNow();
      this.dataTable.update({
        data: [],
        totalRecod: 0
      });
    }
  }

  getNameDevice(text: string) {
    this.deviceName = text;
  }

 async getDevices(userId) {
    let params = {
      userId: userId,
      pageNo: -1
    };
    // this.filter.pageNo = -1;
  await  this.deviceService.list({ params: params }).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      this.listDevices = data.result;
      this.paramsEmiter.emit(this.listDevices);
      setTimeout(() => {
        $('#device').selectpicker('refresh');

        if(this.emiter.geofenceDetail)
        {
            $('#device').val(this.emiter.geofenceDetail.deviceId).selectpicker("refresh");     

            this.emiter.geofenceDetail = null;

        }
      });
    });

  }

  getTrips(userId) {
    let params = {
      userId: userId,
      pageNo: -1
    };
    // this.filter.pageNo = -1;
    this.trip.list(params).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      this.listTrips = data.result;
    // data test
   
      setTimeout(() => {
        $('#trip').selectpicker('refresh');
        if(this.emiter.geofenceDetail)
         $('#trip').val(this.emiter.geofenceDetail.geofenceId).selectpicker("refresh");     
      });
    });

  }

  

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }


  getListFilter(){

    let listDevices: any = [];
    let listTrip:any ;
    
    if(this.listDeviced.length == 0)
    {
      this.listDevices.forEach(device => {
        this.listDeviced.push(device.id);
      });
    }
    // if (this.searchFormGeofence.value.deviceId == '' || this.searchFormGeofence.value.deviceId == null || this.searchFormGeofence.value.deviceId == undefined) {
    //   this.listDevices.forEach(device => {
    //     listDevices.push(device.id);
    //   });
    // } else {
    //   listDevices = this.searchFormGeofence.value.deviceId;
    // }

    // if (this.searchFormGeofence.value.geofenceId == '' || this.searchFormGeofence.value.geofenceId == null || this.searchFormGeofence.value.geofenceId == undefined) {
    //     this.listTrip.forEach(geofence => {
    //         listTrip.push(geofence.id);
    //     });
    //   } else {
    //     listTrip = this.searchFormGeofence.value.geofenceId;
    //   }
    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;
    this.filter.deviceIds = this.listDeviced.toString();
    this.filter.tripId  = this.searchFormGeofence.value.tripId;

  }
  
  searchGeofence() {
    this.getListFilter();
    // console.log(this.filter);
    if(this.filter.tripId == '' || this.filter.tripId == undefined || this.filter.tripId == null) 
    {
       this.toast.show({message:this.translate.instant('REPORT.TRIP.DETAIL.NO'),type:'error'});
       return;
    } 
    // this.getData(this.filter);
    this.dataTable.reload({ currentPage: 1 });

  }

  resetFormSearch() {     
    this.listDevicePicker.reset();
    this.buildDateNow();
    $('#trip').val('').selectpicker("refresh");     
  }

  getColorGeofence(geofenceId)
  {
   
      let color = '#3388ff';   
      if(this.listTrip.find(x => x.id == geofenceId)) color = this.listTrip.find(x => x.id == geofenceId).color;
      return color;
  }

  getTypeGeofence(geofenceId)
  {   
      let type = 'circle';
      if(this.listTrip.find(x => x.id == geofenceId)) type = this.listTrip.find(x => x.id == geofenceId).type;
      return type;
  }

  
  getFillGeofence(geofenceId)
  {    

      let fill = '#3388ff';
      if(this.listTrip.find(x => x.id == geofenceId)) fill = this.listTrip.find(x => x.id == geofenceId).fillColor;
      return fill;
  }

  private refreshEventType()
  {
     this.translate.onLangChange.subscribe((event) => {
     let device = this.translate.instant('REPORT.GEOFENCE.SUMMARY.ALL_DEVICE');
     let geofence = this.translate.instant('REPORT.GEOFENCE.SUMMARY.ALL_GEOFENCE');
     
     $(function () {
       $('#device').selectpicker({title: device}).selectpicker('refresh');
       $('#geofence').selectpicker({title: geofence}).selectpicker('refresh');

     });
   });
  }

  showGeofence(geofenceId,item?:any)
  {
    
    this.geofencePopup = {
        points:item.points,
        // address : "Đường Đại Từ, Phường Đại Kim, Hoàng Mai, Hà Nội",
        geofence :  this.listTrip.find(x => x.id == geofenceId)
    } as AlertPopup;

   if(item)
   {
       this.geofencePopup.device = {
           name:item.deviceName
       };
       this.geofencePopup.timeIn = item.timeIn;
       this.geofencePopup.timeOut = item.timeOut;
       this.geofencePopup.totalTime = item.totalTime;
   }
//    this.DetailGeofenceEmiter.emit(this.geofencePopup);
   
  }

  getDataShowPage()
  {
    this.data = [];
    this.data = this.functionService.pagination(this.dataPagination,this.filter.pageNo,this.filter.pageSize,1);
  }
  
  open(content,geofenceId,item?:any) {

    this.showGeofence(geofenceId,item);
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal--fit kt-modal-clear', size: 'lg', backdrop: 'static'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

   getData(filter,type?:string) {
    if(type == 'pdf') this.isdownLoadPDF =true;
    if(type == 'excel') this.isdownLoad = true;

    let _this = this;
    this.detailTrip.listDetail(filter)
    .pipe(
      debounceTime(1000),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 200) {
        if(type == 'pdf') _this.isdownLoadPDF = false;
        if(type == 'excel') _this.isdownLoad = false;
        let pageNo = this.filter.pageNo - 1;
        let pageSize = this.filter.pageSize;
        let j = 1;
        let data = [];
        result.result = result.result.map(itemData=>{

          if(j > pageNo*pageSize && j <= (pageNo+1)*pageSize -1)
          {
            let count = pageNo * pageSize + j;

            let totalTime = 0;
            totalTime = itemData.timeEnd - itemData.timeFrom;
            itemData.totalTime = this.convertNumberToTime(totalTime);
            itemData.timeFrom = this.userDateAdv.transform(itemData.timeFrom,'YYYY-MM-DD HH:mm:ss');
            itemData.timeEnd = this.userDateAdv.transform(itemData.timeEnd,'YYYY-MM-DD HH:mm:ss');

            itemData.count = count;
            j++;
      
          }
          data.push(itemData);
          return itemData;
          });     

          this.dataExport = 
          [
            {
              data:data
            }
          ]; 
        this.totalRecod = j-1;        
        this.dataPagination = data;
        this.getDataShowPage();
        if(!type)
        {
        
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
        }
        if(type == 'pdf')     this.exportFilePDF(this.data);
        if(type == 'excel')    this.exportFileXLSX(this.data);
        
      }
    })

   
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;
        this.getData(this.filter);
      }),

    ).subscribe();
  }

  convertNumberToTime(number) {
    return this.userDate.convertNumberToTime(number);
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
        responsive:false
      }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  roundNumber(number) {
    return Math.round(number * 100) / 100;
  }

  //pdf
  async exportPDF(){
      this.getListFilter();
      if(this.filter.tripId == '' || this.filter.tripId == undefined || this.filter.tripId == null) 
      {
         this.toast.show({message:this.translate.instant('REPORT.TRIP.DETAIL.NO'),type:'error'});
         return;
      } 
      this.getData(this.filter,'pdf');

  }
  
  exportFilePDF(data){
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let config:PdfModel = {
      info: {
        title: 'DetailTrip',
      },
      content:{
        header:{
          title: this.translate.instant('REPORT.TRIP.DETAIL.NAME'),
          params:{
            timeStart:": "+this.dateStart,
            timeEnd:": "+this.dateEnd,
            timeZone:": "+this.timeZone,
          }
        },
        table:[
          {
              woorksheet: {
                name: this.deviceName,
              },
              total : {
             
            
              },
              headerRows: 1,
              widths: ['10%','20%','20%','20%','20%','10%'],
              header :[           
                  [ 
                    { text: '#',alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.TRIP.GENERAL.NAME_TRIP'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.GENERAL.COLUMN.DEVICE'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('COMMON.COLUMN.STARTTIME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('COMMON.COLUMN.ENDTIME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.GEOFENCE.SUMMARY.SUMMARY_TIME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},

                  ]
              ],
              body:[
                {columnData:'count',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'tripName',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'deviceName',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'timeFrom',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'timeEnd',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'totalTime',style:{alignment:"center",fillColor:"#fff",bold:false}},

              ],              
          },
          
        ]
          },
      footer:{
        title: this.datepipe.transform(this.dateStart, dateTimeFormat)+ ' - ' + this.datepipe.transform(this.dateEnd, dateTimeFormat)
      }    
    }

    this.pdf.ExportPdf([this.dataExport],config);
    // this.xlsx.exportFileTest([data], config, {});
 
  }
  
  //export
  exportXlSX() {
    this.getListFilter(); 
    if(this.filter.tripId == '' || this.filter.tripId == undefined || this.filter.tripId == null) 
    {
       this.toast.show({message:this.translate.instant('REPORT.TRIP.DETAIL.NO'),type:'error'});
       return;
    } 
    this.getData(this.filter,'excel');

  }
  private exportFileXLSX(data) {
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let config: XLSXModelTest = {
      file: {
        title: this.translate.instant('REPORT.TRIP.DETAIL.NAME'),
        prefixFileName: "DetailTrip",
        timeStart:this.dateStart,
        timeEnd:this.dateEnd,
      },
      template: [{
        header: [
          {
            text: this.translate.instant('REPORT.TRIP.DETAIL.NAME'),
            type: "header",
          },
          {
            text: ""
          },
          {
            text: '!timezone',
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START') + ": " + this.dateStart,
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_END') + ": " + this.dateEnd,
          },
          
          {
            text: ""
          },
        ],
        columns: [
          {
            name: "#",
            columnData: "count",
            wch: 5,
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
          },
          {
            name: this.translate.instant('REPORT.TRIP.GENERAL.NAME_TRIP'),
            columnData: "tripName",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 30,
          },
          {
            name: this.translate.instant('REPORT.GENERAL.COLUMN.DEVICE'),
            columnData: "deviceName",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 30,
          },
          {
            name: this.translate.instant('COMMON.COLUMN.STARTTIME'),
            columnData: "timeFrom",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 30,
          },
          {
            name: this.translate.instant('COMMON.COLUMN.ENDTIME'),
            columnData: "timeEnd",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 30,
          },
          {
            name: this.translate.instant('REPORT.GEOFENCE.SUMMARY.SUMMARY_TIME'),
            columnData: "totalTime",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 30,
          },


        ],
        woorksheet: {
          name: this.translate.instant('REPORT.TRIP.DETAIL.NAME'),
        },
        total: {
          
        }
      }]

    };
    this.xlsx.exportFileTest([this.dataExport], config, {});
  }
  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'name trip',
        field: 'Engine',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px'},
        class: '',
        translate: 'REPORT.TRIP.GENERAL.NAME_TRIP',     
        autoHide: false,
        width: 200,
      },
      {
        title: 'device',
        field: 'Engine',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px'},
        class: '',
        translate: 'REPORT.GENERAL.COLUMN.DEVICE',     
        autoHide: false,
        width: 200,
      },
      {
        title: 'Count in',
        field: 'Engine',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px','text-align':'center' },
        class: '',
        translate: 'COMMON.COLUMN.STARTTIME',     
        autoHide: false,
        width: 200,
      },
      {
        title: 'Count out',
        field: 'Engine',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px','text-align':'center'},
        class: '',
        translate: 'COMMON.COLUMN.ENDTIME',     
        autoHide: false,
        width: 200,
      },
      {
        title: 'Sensor',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px','text-align':'center'  },
        class: '',
        translate: 'REPORT.GEOFENCE.SUMMARY.SUMMARY_TIME',     
        autoHide: false,
        width: 200,
        fuel:true
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px','text-align':'center' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 100,
      },
    ]
  }


}




