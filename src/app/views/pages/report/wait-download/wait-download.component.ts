  import { Component, OnInit,Output,Input, ElementRef, ViewChild, ChangeDetectorRef,EventEmitter } from '@angular/core';
  import { finalize, takeUntil, tap, debounceTime,delay,take } from 'rxjs/operators';
  import { UserDateAdvPipe } from '@core/_base/layout/pipes/user-date-adv.pipe';
  import { CurrentUserService } from '@core/auth';
  import { TranslateService } from '@ngx-translate/core';
  import * as moment from 'moment';
  import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
  import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
  import { ReportSummaryDevicesService, ReportSummaryDayService } from '@core/report';
  import { waitDownloadModel } from './wait.download.service'
  import { Subject, from } from 'rxjs';
  declare var $: any;
  import { ToastrService } from 'ngx-toastr';

  @Component({
  selector: 'kt-wait-download',
  templateUrl: './wait-download.component.html',
  styleUrls: ['./wait-download.component.scss'],
  providers:[UserDateAdvPipe]
  })
  export class WaitDownloadComponent implements OnInit {
  @ViewChild('download', { static: true }) 
  download:any;

  @ViewChild('contentInfo', { static: true }) 
  contentInfo:any;
  
  @Input() paramsEmiter: EventEmitter<waitDownloadModel>; // event for update and refesh by parrent
  @Output() checkbetween:EventEmitter<boolean>;

  formExport:FormGroup;
  public timeStart:string;
  public timeEnd:string;
  public numberDevices:number = 0;
  public checkGetExport:boolean = false;
  public showPopupWaitDownload:boolean = false;
  public listSettingReport:settingReport[];
  public closeResult: string;
  public defaultExport:string = 'excel';
  public typeReport: string;
  public filter;
  public titleReport:string;
  private unsubscribe: Subject<any>;
  private urlFile:string;
  constructor(
        // private unsubscribe: Subject<any>,
        private cdr: ChangeDetectorRef,
        private userDateAdv : UserDateAdvPipe,
        private currentUserService: CurrentUserService,
        private modalService: NgbModal,
        private formBuilder: FormBuilder,
        private summaryDevices: ReportSummaryDevicesService,
        private toastr: ToastrService,
        private translate: TranslateService,
        private summaryByDay: ReportSummaryDayService,

  ) {
    this.buildForm();
    this.paramsEmiter = new EventEmitter();
    this.checkbetween = new EventEmitter();
    }
  
  ngOnInit(): void {    
      this.getListSettingReport();
      if(this.paramsEmiter)
      {     
            let countDevices = 0;     
            let maxTotal = 0; 
            this.paramsEmiter.subscribe(params => {
              
              this.checkGetExport = false;
              this.checkbetween.emit(false);
              if( params.format) this.defaultExport = params.format;
              this.filter = params;
              let firstDate = moment(params.timeFrom);
              let secondDate = moment(params.timeTo);
              let deviceIds = params.deviceIds;
              if(params.deviceId) deviceIds = params.deviceId;
              let diffInDays = Math.abs(firstDate.diff(secondDate, 'days')); 
              let report = params.report;
              this.typeReport = report;
              let objReport = this.listSettingReport.find(x => x.report === report);
              maxTotal = objReport.maxTotal;
              this.titleReport = objReport.translate;
              deviceIds = deviceIds.split(",");
              countDevices = deviceIds.length;  
   
              if(diffInDays*countDevices > maxTotal)
              {
                this.checkbetween.emit(true);
                this.open(this.download);
                setTimeout( () => {
                  $('.kt_selectpicker').selectpicker('refresh');
                });
                // this.showPopupWaitDownload = true;
                // this.checkGetExport = false;
                // result = true;
              } 
              // this.showSucssecDownload = result;
              
          });
      }  
    }

    private buildForm(): void {
      this.formExport = this.formBuilder.group({
        format : ['excel']
      });
      
    }
  
    ngOnDestroy():void 
    {

    }

    getListSettingReport()
    {
      this.listSettingReport = [
        {
          report:'summary_devices',
          translate:'MENU.REPORT_GENERAL',
          maxTotal:300,
        },
        {
          report:'summary_by_day',
          translate:'MENU.REPORT_SUMMARY_DAY',
          maxTotal:60,
        },
      ]
    }

    open(content) {
      this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder',backdrop:'static'}).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return  `with: ${reason}`;
      }
    }

    toasterClickedHandler() {

    }

    downloadSuccess()
    {
      let contentToast = this.translate.instant(this.titleReport)+this.translate.instant('REPORT.GENERAL.MESSAGE.TEXT_SUCSSEC')+'('+this.defaultExport+')';
      let titleToast = moment().format('DD/MM/YYYY HH:mm');
      this.toastr.success(
        contentToast ,titleToast, {
          closeButton: true,
          disableTimeOut: true,
          enableHtml: true,
          tapToDismiss: true,
          onActivateTick: true,
          positionClass:'toast-top-center',
          toastClass:'ngtoast-download'
      }).onTap
      .pipe(take(1))
      .subscribe(() => this.toasterClickedHandler());
    }

    onSubmitExport(form)
    {
      this.checkGetExport = true;
      let language = 0;
      let format = form.value.format;
      let typeReport = 0;
      if(format == 'pdf') typeReport = 1;
      this.defaultExport = format;
      if(this.translate.currentLang == 'vn') language =1;
      this.filter.typeExport = typeReport;
      this.filter.language = language;

      // summary_devices
      if(this.typeReport == 'summary_devices')
      {
        this.summaryDevices.list(this.filter)
        .pipe(
        debounceTime(1000),
        finalize(() => {
          this.cdr.markForCheck();
        }))
        .subscribe((result: any) => {
          this.downloadSuccess();          
        })
      }
      // summary_by_day
      if(this.typeReport == 'summary_by_day')
      {
        this.summaryByDay.list(this.filter)
        .pipe(debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        }))
        .subscribe((result: any) => {
          // this.urlFile = result.
          this.downloadSuccess();
        })
      }

      
    }
  }

  export class settingReport{
    report:string;
    maxTotal:number;
    translate:string;
  } 
