import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { ReportEngineService } from '@core/report';
import { DeviceService } from '@core/manage';
import { TranslateService } from '@ngx-translate/core';
import { ToastService, ValidatorCustomService, UserDatePipe } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { ExcelService,PdfmakeService } from '@core/utils';
import { XLSXModel,XLSXModelTest } from '@core/utils/xlsx/excel.service';
import { DatePipe } from '@angular/common';
import { UserDateService } from '@core/_base/layout';
import { DecimalPipe } from '@angular/common';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { PdfModel } from '@core/utils/pdfmake/pdfmake.service';
import { UserTreeService } from '@core/common';

declare var $: any;
@Component({
  selector: 'report-engine',
  templateUrl: './report-engine.component.html',
  styleUrls: ['./report-engine.component.scss'],
  providers: [DatePipe,UserDatePipe,DecimalPipe]

})
export class ReportEngineComponent implements OnInit {
  
  @ViewChild('datePicker', { static: true }) 

  datePicker: DateRangePickerComponent;
  formEngine: FormGroup;
  searchFormEngine: FormGroup;
  formCheckList: FormGroup;
  selectdp: ElementRef;
  public dataTable: DataTable = new DataTable();
  public datePickerOptions: any = {
    size: 'md',
    select: 'from',
    ranges: 'from',
    optionDatePicker: {
      maxSpan: {
        days: 31
      }
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: true,

  };
  public columns: any = [];
  public parentBread : any ;
  public data: any = [];
  public dataExport: any = [];
  public dataExportSummary: any = [
    {
      data:[]
    }
  ];
  public timeZone:string;
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataDefault: any = [{}];
  public filter: any;
  private unsubscribe: Subject<any>;
  public listDevices: any = [];
  public userIdSelected: number;
  public userIdSelectedEdit: number;
  public showUserTree: boolean = true;
  public dateStart: string;
  public dateEnd: string;
  public timeStart: string;
  public timeEnd: string;
  public deviceId: string;
  public deviceName: string;
  public dateFormat: string;
  public timeFormat: string;
  public timeFromdetail : string;
  public totalAllRowData : any;
  public closeResult: string;
  public listDataDetail : string;
  public isLoading : boolean = false;
  public isdownLoad : boolean = false;
  public isdownLoadPDF:boolean = false;
  constructor(
    private Engine: ReportEngineService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private deviceService: DeviceService,
    private translate: TranslateService,
    private toast: ToastService,
    private currentUser: CurrentUserService,
    private xlsx: ExcelService,
    public datepipe: DatePipe,
    private userDate: UserDateService,
    private userDatePipe : UserDatePipe,
    private modalService: NgbModal,
    private pdf : PdfmakeService,
    private userTreeService: UserTreeService,

  ) {
    this.timeZone = this.currentUser.currentUser.timezone;
    this.dateFormat = this.currentUser.dateFormatPipe;
    this.timeFormat = this.currentUser.timeFormat;
    // this.buildDateNow();
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.parentBread = [
        {
            name: this.translate.instant('REPORT.REPORTS.GENERAL.REPORTS'),
            translate: "REPORT.REPORTS.GENERAL.REPORTS",
            link:"/reports",
            icon:"flaticon2-graph"
        }
    ]
  }

  ngOnInit() {
    this.updateDataTable();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();
  }

  buildDateNow() {

    this.datePicker.refresh();
    this.deviceId = '';
    setTimeout(function () {
      $('.kt_selectpicker').selectpicker('refresh');
    }, 100);
  }

  private buildForm(): void {
    this.searchFormEngine = this.formBuilder.group({
      dateStart: [''],
      dateEnd: [''],
      deviceId: ['']
    });
  }


  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
   }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
    };
  }

  ChangeUserTree(value) {
    if (value.id > 0) {
      this.userIdSelectedEdit = value.id;
      this.getDevices(value.id);
      // this.deviceId = '';
      this.buildDateNow();
      this.dataTable.update({
        data: [],
        totalRecod: 0
      });
    }
  }

  getNameDevice(text: string) {
    this.deviceName = text;
  }

  getDevices(userId) {
    let params = {
      userId: userId,
      pageNo: -1
    };
    // this.filter.pageNo = -1;
    this.deviceService.list({ params: params }).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      this.listDevices = data.result;
      setTimeout(function () {
        $('.kt_selectpicker').selectpicker('refresh');
      }, 100);
    });

  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  validateForm(deviceId: string) {
    let timeStart = new Date(this.dateStart).getTime();
    let timeEnd = new Date(this.dateEnd).getTime();
    if (timeStart > timeEnd) {
      this.toast.show(
        {
          message: this.translate.instant('REPORT.ROUTE.MESSAGE.ERROR_TIME'),
          type: 'error',
        })
      return false;
    }
    if (deviceId == '' || deviceId == undefined || deviceId == null) {
      this.toast.show(
        {
          message: this.translate.instant('MESSEAGE_CODE.SENSOR.DEVICE_NOT_FOUND'),
          type: 'error',
        })
      return false;
    }
    return true;
  }

  searchEngine(form: any) {

    if(!this.validateForm(form.value.deviceId)) return;
    // this.dataTable.reload({ currentPage: 1 });
    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;
    this.filter.deviceId = this.deviceId = form.value.deviceId;
    this.filter.userId = this.userIdSelectedEdit;
    // this.getDataSummary(this.filter);
    this.dataTable.reload({currentPage: 1});

  }

  editEngine(timeFrom,timeEnd,content)
  {
    this.getData(timeFrom,timeEnd);
    this.timeFromdetail = timeFrom;
    this.open(content);

  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder kt-modal-xl'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  resetFormSearch() {
    this.buildDateNow();
  }

  getDataSummary(filter){
    this.Engine.list(filter)
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 200) {
        let j = 1;
        this.data = result.result.content.map(day => {
          let pageNo = this.filter.pageNo-1;
          let pageSize = this.filter.pageSize;
          if(day.data)
          {
            day.data = day.data.map(row =>{
              let count = pageNo*pageSize+j;
              row.count = count;
              j++;
              return row;
            });
          }
          return day;
        });
        this.totalRecod = j-1;
        this.dataTable.update({
          data: this.data,
          totalRecod: this.totalRecod
        });
        setTimeout(function () {
          $('.kt_selectpicker').selectpicker('refresh');
        }, 100);
      }
    })
  }

  getData(timeFrom,timeEnd) {
    this.isLoading =true;
    timeFrom =  this.userDatePipe.transform(timeFrom, "YYYY-MM-DD HH:mm:ss",null,"datetime") ;
    timeEnd =  this.userDatePipe.transform(timeEnd, "YYYY-MM-DD HH:mm:ss",null,"datetime") ;
    let params = {
      pageNo : -1,
      timeFrom : timeFrom,
      timeTo : timeEnd,
      deviceId : this.deviceId,
      userId : this.userIdSelectedEdit
    };
    this.Engine.detail(params)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.listDataDetail = result.result[0].data;
          this.isLoading = false;
        }
      })
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;
        this.getDataSummary(this.filter);
      }),

    ).subscribe();
  }

  convertNumberToTime(number) {
    return this.userDate.convertNumberToTime(number);
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
      }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  roundNumber(number)
  {
    return  Math.round(number*100)/100;    
  }

 async getdataExcel(file) {
  if(file =='pdf')
    {
      this.isdownLoadPDF = true;
    }else
    {
      this.isdownLoad = true;
    }
    let params = {
      timeFrom: this.dateStart,
      timeTo: this.dateEnd,
      pageNo: -1,
      userId: this.userIdSelectedEdit,
      deviceId: this.searchFormEngine.value.deviceId
    };
    
    await this.Engine.detail(params)
      .pipe(
        tap((result: any) => {
          if (result.status == 200) {
   
            result.result = result.result.map(day => {
              let countRowData = 0;
              let totalmoveDuration = 0;
              let totalengineonTime = 0;
  
              let dateGroup= this.userDatePipe.transform(day.timeFrom,"date", null, "datetime") ;  
              day.timeFromUtc = dateGroup;
               if (day.data) {
                day.data = day.data.map(row => {
                  countRowData++;
                  totalmoveDuration = totalmoveDuration + row.moveDuration;
                  totalengineonTime = totalengineonTime + row.engineonTime;
                  row.engineonTime  = this.convertNumberToTime(row.engineonTime);
                  row.moveDuration  = this.convertNumberToTime(row.moveDuration);
                  row.startTime     = this.userDatePipe.transform(row.startTime, "HH:mm:ss",null,"datetime") ;
                  row.stopTime     = this.userDatePipe.transform(row.stopTime, "HH:mm:ss",null,"datetime") ;
                  row.movePercent     = row.movePercent+'%' ;
  
                  return row;
                });
        
              }
                // add result
                day.countRowData = countRowData;
                day.totalmoveDuration = this.convertNumberToTime(totalmoveDuration);;
                day.totalengineonTime = this.convertNumberToTime(totalengineonTime);;
                day.totalmovePercent  = Math.round((totalmoveDuration/totalengineonTime)*100)+'%'
                if(totalengineonTime == 0)  day.totalmovePercent = '0%';
                
              return day;
            });
  
            // console.log(result.result);
            this.dataExport = result.result;    
            if(file =='pdf')
            {
              this.isdownLoadPDF = false;
            }else
            {
              this.isdownLoad = false;
            }
            this.isdownLoad = false;
            this.isdownLoadPDF = false;
          }
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      ).toPromise();
  // get data summary excel
    await this.Engine.list(params).
      pipe(
        tap((result: any) => {
          if (result.status == 200) {
                  let datatest = [];
      
                  let totalengineonTime = 0;
                  let totalmoveDuration = 0;
                  let totalmovePercent = 0;
      
                  let totalidlingDuration = 0;
                  let totalidlingPercent = 0;
                  let totalavgInterval = 0;
                  let totalmileage = 0;
                  let totalavgSpeed = 0;
                  let totalinterval = 0;
                  let count = 0;
                  result.result = result.result.map(day => {
                  let dateGroup= this.userDatePipe.transform(day.timeFrom,"date", null, "datetime") ;  
                
                  day.timeFromUtc = dateGroup;
      
                  if (day.data) {
                    count++;
                    day.data = day.data.map(row => {
                    
                      totalengineonTime = totalengineonTime + row.totalEngineOnTime;
                      totalmileage = totalmileage + row.mileage;
                      totalmoveDuration = totalmoveDuration + row.moveDuration;
                      totalmovePercent = totalmovePercent + row.movePercent;
                      totalidlingPercent = totalidlingPercent + row.idlingPercent;
                      totalidlingDuration = totalidlingDuration + row.stopEngineOnTime;
                      totalinterval = totalinterval + row.interval;
                      totalavgInterval = totalavgInterval + row.avgInterval;
                      totalavgSpeed = totalavgSpeed + row.avgSpeed;
      
                      row.engineonTime  = this.convertNumberToTime(row.totalEngineOnTime);
                      row.moveDuration  = this.convertNumberToTime(row.moveDuration);
                      row.movePercent     = row.movePercent+'%' ;
                      row.idlingDuration  = this.convertNumberToTime(row.stopEngineOnTime);
                      row.idlingPercent     = row.idlingPercent+'%' ;
                      row.startTime     = this.userDatePipe.transform(row.startTime, "HH:mm:ss",null,"datetime") ;
                      row.stopTime     = this.userDatePipe.transform(row.stopTime, "HH:mm:ss",null,"datetime") ;
                      row.avgInterval  = this.convertNumberToTime(row.avgInterval);
                      row.mileage  = this.roundNumber(row.mileage);
                      row.avgSpeed  = this.roundNumber(row.avgSpeed);
                      row.date = dateGroup;
                      
                      datatest.push(row);
                      return row;
                    });
      
                  }else{
                      datatest.push({
                        date:dateGroup,
                      });
                  }
                    // add result
                    
                  return day;
                });
                this.dataExportSummary[0].data = datatest;
                this.dataExportSummary[0].totalmovePercent = Math.round(totalmoveDuration/totalengineonTime*100)+'%';
                if(totalengineonTime == 0) this.dataExportSummary[0].totalmovePercent = '0%';
                this.dataExportSummary[0].totalidlingPercent = 100-Math.round(totalmoveDuration/totalengineonTime*100);
                this.dataExportSummary[0].totalidlingPercent = this.dataExportSummary[0].totalidlingPercent+'%'
                if(totalengineonTime == 0) this.dataExportSummary[0].totalidlingPercent = '0%';
      
                this.dataExportSummary[0].totalengineonTime =  this.convertNumberToTime(totalengineonTime);
                this.dataExportSummary[0].totalmileage = this.roundNumber(totalmileage);
                this.dataExportSummary[0].totalmoveDuration = this.convertNumberToTime(totalmoveDuration);
                this.dataExportSummary[0].totalidlingDuration = this.convertNumberToTime(totalidlingDuration);
                this.dataExportSummary[0].totalinterval = totalinterval;
                this.dataExportSummary[0].totalavgInterval = this.convertNumberToTime(totalavgInterval/count);
                this.dataExportSummary[0].totalavgSpeed = this.roundNumber(totalavgSpeed/count);
                if(count == 0) {
                  this.dataExportSummary[0].totalavgInterval = 0;
                  this.dataExportSummary[0].totalavgSpeed = 0;
                }
                this.isdownLoad = false;
                return;

          }
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
        this.cdr.markForCheck();
          })
        ).toPromise();
        if(file =='pdf')
        {
          // this.pdf.ExportPdf(this.dataExport,this.totalAllRowData,param_header,footer);
          this.exportFilePDF(this.dataExportSummary);
        }
        else
        {
          this.exportFileXLSX(this.dataExport,this.dataExportSummary);
        }  
  }

    //pdf
    async exportPDF(){

      let validate = this.validateForm(this.searchFormEngine.value.deviceId);
      if (validate )
       {
        this.getdataExcel('pdf')
       }
    }
    exportFilePDF(data){
      let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
      let config:PdfModel = {
        info: {
          title: 'ReportEngine',
          devices:this.deviceName
        },
        content:{
          header:{
            title:this.translate.instant('REPORT.ENGINE.EXCEL.REPORT_ENGINE'),
            params:{
              timeStart:": "+this.dateStart,
              timeEnd:": "+this.dateEnd,
              deviceName:": "+this.deviceName,
              timeZone:": "+this.timeZone
            }
          },
          table:[
            {
                woorksheet: {
                  name: this.deviceName,
                },
                total : {
                  totalCol:[
                      {text:''},
                      { text:this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL'),style:{alignment:"right",bold:true,fillColor:"#fff",fontSize:9}},
                      { columnData:'totalengineonTime' ,style:{alignment:"center",bold:true,fillColor:"#fff",fontSize:9}},
                      { columnData:'totalmoveDuration',style:{alignment:"center",bold:true,fillColor:"#fff",fontSize:9}},
                      { columnData:'totalmovePercent',style:{alignment:"right",bold:true,fillColor:"#fff",fontSize:9}},
                      { columnData:'totalidlingDuration',style:{alignment:"center",bold:true,fillColor:"#fff",fontSize:9}},
                      { columnData:'totalidlingPercent' ,style:{alignment:"right",bold:true,fillColor:"#fff",fontSize:9}},
                      { columnData:'totalavgInterval',style:{alignment:"right",bold:true,fillColor:"#fff",fontSize:9}},
                      { columnData:'totalmileage',style:{alignment:"right",bold:true,fillColor:"#fff",fontSize:9}},
                      { columnData:'totalavgSpeed',style:{alignment:"right",bold:true,fillColor:"#fff",fontSize:9}},
                      { columnData:'totalinterval',style:{alignment:"right",bold:true,fillColor:"#fff",fontSize:9}},
                  ],
            
                },
                headerRows: 2,
                widths: ['4%','12%','12%','9%','9%','9%','9%','9%','9%','9%','9%'],
                name: [
                  { text: this.translate.instant('REPORT.ENGINE.GENERAL.REPORT_ENGINE_DAILY'),alignment:"left",fillColor:"#FFF",fontSize:12,bold:true,margin:[0,0,0,3]}
                ],
                header :[           
                    [ 
                      { text: '#',rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                      { text: this.translate.instant('REPORT.ENGINE.COLUMN.DATE'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                      { text: this.translate.instant('REPORT.ENGINE.COLUMN.TIME_ENGINE'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                      { text: this.translate.instant('REPORT.ENGINE.GENERAL.MOVEMENT'),colSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true},
                      { text:""},
                      { text: this.translate.instant('REPORT.ENGINE.GENERAL.IDLING'),colSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true},
                      { text:""},
                      { text: this.translate.instant('REPORT.ENGINE.COLUMN.AVERAGE_TIME'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                      { text: this.translate.instant('REPORT.ENGINE.COLUMN.MILEAGE'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                      { text: this.translate.instant('REPORT.ENGINE.COLUMN.AVERAGE_SPEED'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                      { text: this.translate.instant('REPORT.ENGINE.COLUMN.INTERVAL'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},

                  
                    ],
                    [
                      '','','',
                      { text: this.translate.instant('REPORT.ENGINE.GENERAL.HOURS'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                      { text: '%',alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                      { text: this.translate.instant('REPORT.ENGINE.GENERAL.HOURS'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                      { text: '%',alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},        
                      '','','','',
                    ]
                ],
                body:[
                  {auto:true},
                  {columnData:'date',style:{alignment:"center",fillColor:"#fff",bold:false}},
                  {columnData:'engineonTime',style:{alignment:"center",fillColor:"#fff",bold:false}},
                  {columnData:'moveDuration',style:{alignment:"center",fillColor:"#fff",bold:false}},
                  {columnData:'movePercent',style:{alignment:"right",fillColor:"#fff",bold:false}},
                  {columnData:'idlingDuration',style:{alignment:"center",fillColor:"#fff",bold:false}},
                  {columnData:'idlingPercent',style:{alignment:"right",fillColor:"#fff",bold:false}},
                  {columnData:'avgInterval',style:{alignment:"right",fillColor:"#fff",bold:false}},
                  {columnData:'mileage',style:{alignment:"right",fillColor:"#fff",bold:false}},
                  {columnData:'avgSpeed',style:{alignment:"right",fillColor:"#fff",bold:false}},
                  {columnData:'interval',style:{alignment:"right",fillColor:"#fff",bold:false}},

                ],              
            },
            {
              woorksheet: {
                name: this.deviceName,
              },
              total : {
                group : [
                  { columnData:'timeFromUtc'},
                ],
                totalCol:[
                    {text:''},
                    { columnData:'totalengineonTime' ,style:{alignment:"center",bold:true,fillColor:"#fff",fontSize:9}},
                    { columnData:'totalmoveDuration',style:{alignment:"center",bold:true,fillColor:"#fff",fontSize:9}},
                    { columnData:'totalmovePercent',style:{alignment:"center",bold:true,fillColor:"#fff",fontSize:9}},
                    {text:''},{text:''},{text:''},{text:''},
                ],
          
              },
              headerRows: 2,
              widths: ['5%','12%','12%','9%','9%','22%','9%','22%'],
              name: [
                { text: this.translate.instant('REPORT.ENGINE.GENERAL.REPORT_ENGINE_DETAILED'),alignment:"left",fillColor:"#FFF",fontSize:12,bold:true,margin:[0,15,0,3]}
              ],
              header :[           
                  [ 
                    { text: '#',rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                    { text: this.translate.instant('REPORT.ENGINE.COLUMN.TIME_ENGINE'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                    { text: this.translate.instant('REPORT.ENGINE.GENERAL.MOVEMENT'),colSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true},
                    { text:""},
                    { text: this.translate.instant('REPORT.ENGINE.COLUMN.ENGINE_START'),colSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true},
                    { text:""},
                    { text: this.translate.instant('REPORT.ENGINE.COLUMN.ENGINE_END'),colSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true},
                    { text:""},
                  ],
                  [
                    '','',
                    { text: this.translate.instant('REPORT.ENGINE.GENERAL.HOURS'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: '%',alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.ENGINE.GENERAL.TIME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.ENGINE.GENERAL.PLACE'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},      
                    { text: this.translate.instant('REPORT.ENGINE.GENERAL.TIME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.ENGINE.GENERAL.PLACE'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},     
            
                  ]
              ],
              body:[
                {auto:true},
                {columnData:'engineonTime',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'moveDuration',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'movePercent',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'startTime',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'startAddress',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'stopTime',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'stopAddress',style:{alignment:"left",fillColor:"#fff",bold:false}},


              ],              
          }, 
          ]
            },
        footer:{
          title: this.datepipe.transform(this.dateStart, dateTimeFormat)+ ' - ' + this.datepipe.transform(this.dateEnd, dateTimeFormat)+', '+this.deviceName
        }    
      }
      this.pdf.ExportPdf([this.dataExportSummary,this.dataExport],config);
      // this.xlsx.exportFileTest([data], config, {});
   
    }

  //export
  exportXlSX() {
    let validate = this.validateForm(this.searchFormEngine.value.deviceId);
    if (validate) this.getdataExcel('excel');
  }
  private exportFileXLSX(data,dataSummary) {
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let dataExcel = [dataSummary,data];
    // return;
    let config: XLSXModelTest = {
      file: {
        title: this.translate.instant('REPORT.ENGINE.GENERAL.REPORT_ENGINE'),
        prefixFileName: "ReportEngine "
      },
      template : [
        {
          header: [
            {
              text:this.translate.instant('REPORT.ENGINE.EXCEL.REPORT_ENGINE'),
              type: "header",
            },
            {
              text: "",
            },
            {
              text: "!timezone",
            },
            {
              text: ""
            },
            {
              text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START') + ": " + this.datepipe.transform(this.dateStart, dateTimeFormat),
            },
            {
              text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_END') + ": " + this.datepipe.transform(this.dateEnd, dateTimeFormat)
            },
            {
              text: this.translate.instant('MANAGE.DEVICE.GENERAL.DEVICE_NAME') + ": " + this.deviceName
            },
            {
              text: ""
            },
            {
              text: this.translate.instant('REPORT.ENGINE.GENERAL.REPORT_ENGINE_DAILY'),
              style:{
                font: { name: 'Times New Roman', sz: 14, bold: true },
                alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true }
              }
            },
           
          ],
        
          columns: [
            {
              name: "#",
              columnData: "auto",
              wch: 5,
              style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            }, 
            {
              name: this.translate.instant('REPORT.ENGINE.COLUMN.DATE'),
              columnData: "date",
              style: {
                 alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true },},
              wch: 15,
              link: {
                target : this.deviceName + "-detailed!A1"
              }
            },
            {
              name: this.translate.instant('REPORT.ENGINE.COLUMN.TIME_ENGINE'),
              columnData: "engineonTime",
              style: { 
                alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true },
             },
              wch: 15,
            },
            {
              name: this.translate.instant('REPORT.ENGINE.GENERAL.MOVEMENT'),
              columnData: "moveDuration",
              style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },
              wch: 15,
            },
            {
              name: 'In movement',
              columnData: "movePercent",
              style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
            },

              wch: 10,
            },
            {
              name: this.translate.instant('REPORT.ENGINE.GENERAL.IDLING'),
              columnData: "idlingDuration",
              style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },
              wch: 15
            },
            
            {
              name: 'Idling',
              columnData: "idlingPercent",
              style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },
              wch: 10
            },
            {
              name: this.translate.instant('REPORT.ENGINE.COLUMN.AVERAGE_TIME'),
              columnData: "avgInterval",
              style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },
              wch: 15
            },
            {
              name: this.translate.instant('REPORT.ENGINE.COLUMN.MILEAGE'),
              columnData: "mileage",
              style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },
              wch: 15
            },
            {
              name: this.translate.instant('REPORT.ENGINE.COLUMN.AVERAGE_SPEED'),
              columnData: "avgSpeed",
              style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },
              wch: 15
            },
            {
              name: this.translate.instant('REPORT.ENGINE.COLUMN.INTERVAL'),
              columnData: "interval",
              style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },
              wch: 15
            },
          ],
          columnsMerge:
          {
            data:[
            "","","",this.translate.instant('REPORT.ENGINE.GENERAL.HOURS'),"%",this.translate.instant('REPORT.ENGINE.GENERAL.HOURS'),"%","","","",""],
              position : ["A9:K9","A10:A11","B10:B11","C10:C11","D10:E10","F10:G10","H10:H11","I10:I11","J10:J11","K10:K11"]
          },
          total:{
            totalCol : {
              field : [
                {
                  text:" ",
                },
                {
                  text:this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL')+':',
                  style: {
                    font: { name: 'Times New Roman', sz: 14, bold: true },
                    alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                  },
                },
                {
                  columnData:"totalengineonTime",
                  style:{
                    font: { name: 'Times New Roman', sz: 14, bold: true },
                    alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                  }
                },

                {
                  columnData:"totalmoveDuration",
                  style:{
                    font: { name: 'Times New Roman', sz: 14, bold: true },
                    alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                  }
                },

                {
                  columnData:"totalmovePercent",
                  style:{
                    font: { name: 'Times New Roman', sz: 14, bold: true },
                    alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                  }
                },

                {
                  columnData:"totalidlingDuration",
                  style:{
                    font: { name: 'Times New Roman', sz: 14, bold: true },
                    alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                  }
                },

                {
                  columnData:"totalidlingPercent",
                  style:{
                    font: { name: 'Times New Roman', sz: 14, bold: true },
                    alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                  }
                },

                {
                  columnData:"totalavgInterval",
                  style:{
                    font: { name: 'Times New Roman', sz: 14, bold: true },
                    alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                  }
                },

                {
                  columnData:"totalmileage",
                  style:{
                    font: { name: 'Times New Roman', sz: 14, bold: true },
                    alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                  }
                },

                {
                  columnData:"totalavgSpeed",
                  style:{
                    font: { name: 'Times New Roman', sz: 14, bold: true },
                    alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                  }
                },

                {
                  columnData:"totalinterval",
                  style:{
                    font: { name: 'Times New Roman', sz: 14, bold: true },
                    alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                  }
                }
              ]
            },
          },
          woorksheet: {
            name: this.deviceName + "-daily",
          }
        },
        {
          header: [
            {
              text: this.translate.instant('REPORT.ENGINE.GENERAL.REPORT_ENGINE'),
              type: "header",
            },
          
            {
              text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START') + ": " + this.datepipe.transform(this.dateStart, dateTimeFormat),
            },
            {
              text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_END') + ": " + this.datepipe.transform(this.dateEnd, dateTimeFormat),
            },
            {
              text: this.translate.instant('MANAGE.DEVICE.GENERAL.DEVICE_NAME') + ": " + this.deviceName,
            },
            {
              text: ""
            },
            {
              text: this.translate.instant('REPORT.ENGINE.GENERAL.REPORT_ENGINE_DETAILED'),
              style:{
                font: { name: 'Times New Roman', sz: 14, bold: true },
                alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true }
              }
            },
           
          ],
        
          columns: [
            {
              name: "#",
              columnData: "auto",
              wch: 5,
              style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            }, 
            {
              name: this.translate.instant('REPORT.ENGINE.COLUMN.TIME_ENGINE'),
              columnData: "engineonTime",
              style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
              wch: 20,
            },
            {
              name: this.translate.instant('REPORT.ENGINE.GENERAL.MOVEMENT'),
              columnData: "moveDuration",
              style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
              wch: 15,
            },
            {
              name: 'In movement',
              columnData: "movePercent",
              style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
              wch: 10,
            },
            {
              name: this.translate.instant('REPORT.ENGINE.COLUMN.ENGINE_START'),
              columnData: "startTime",
              style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
              wch: 15
            },
            
            {
              name: 'Engine start',
              columnData: "startAddress",
              style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
              wch: 40
            },
            {
              name: this.translate.instant('REPORT.ENGINE.COLUMN.ENGINE_END'),
              columnData: "stopTime",
              style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
              wch: 15
            },
            {
              name: 'Engine stop',
              columnData: "stopAddress",
              style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
              wch: 40
            }
          ],
          columnsMerge:
          {
            data:[
            "","",
            this.translate.instant('REPORT.ENGINE.GENERAL.HOURS'),
             "%",
             this.translate.instant('REPORT.ENGINE.GENERAL.TIME'),
             this.translate.instant('REPORT.ENGINE.GENERAL.PLACE'),
             this.translate.instant('REPORT.ENGINE.GENERAL.TIME'),
             this.translate.instant('REPORT.ENGINE.GENERAL.PLACE')
             ,"","","",""],
              position : ["A6:K6","A7:A8","B7:B8","C7:D7","E7:F7","G7:H7"]
          },
          total:{
            group:"timeFromUtc",
            totalCol : {
              field : [
                {
                  text:" ",
                },
                {
                  columnData:"totalengineonTime",
                  style:{
                    font: { name: 'Times New Roman', sz: 14, bold: true },
                    alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true }
                    
                  }
                },
                {
                  columnData:"totalmoveDuration",
                  style:{
                    font: { name: 'Times New Roman', sz: 14, bold: true },
                    alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true }
                  }
                },
                {
                  columnData:"totalmovePercent",
                  style:{
                    font: { name: 'Times New Roman', sz: 14, bold: true },
                    alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true }
                  }
                },
                {
                  text:" ",
                },
                {
                  text:" ",
                },
                {
                  text:" ",
                },
                {
                  text:" ",
                }
              ]
            },
          },
          woorksheet: {
            name: this.deviceName + "-detailed",
          }
        }
      ]
    };
    this.xlsx.exportFileTest(dataExcel, config, {});
  }
  // setcolumns for detail
  setColumns() {

    this.columns= [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Date',
        field: 'date',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.ENGINE.COLUMN.DATE',
        autoHide: false,
        width: 100,
      },
      {
        title: 'TIME_ENGINE',
        field: 'TIME_ENGINE',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.ENGINE.COLUMN.TIME_ENGINE',
        autoHide: false,
        width:100,
      },
      {
        title: 'TIME_MOVE',
        field: 'TIME_ENGINE',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.ENGINE.COLUMN.TIME_MOVE',
        autoHide: false,
        width:100,
      },
      {
        title: 'Move start',
        field: 'moveStart',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.ENGINE.COLUMN.RATIO',
        autoHide: false,
        width: 100,
      },
      {
        title: 'Engine',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.ENGINE.COLUMN.TIME_IDLING',
        autoHide: true,
        width: 100,
      },
      {
        title: 'Created ',
        field: 'distance',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.ENGINE.COLUMN.RATIO_IDLING',
        autoHide: true,
        width: 100,
      },
      {
        title: 'Sended',
        field: 'duration',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.ENGINE.COLUMN.AVERAGE_TIME',
        autoHide: true,
        width: 100,
      },
      {
        title: 'Time send',
        field: 'averageSpeed',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px', 'padding': '6px 0px' },
        class: '',
        translate: 'REPORT.ENGINE.COLUMN.MILEAGE',
        autoHide: true,
        width: 100,
      },
      {
        title: 'Time send',
        field: 'averageSpeed',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px', 'padding': '6px 0px' },
        class: '',
        translate: 'REPORT.ENGINE.COLUMN.AVERAGE_SPEED',
        autoHide: true,
        width: 100,
      },
      {
        title: 'Time send',
        field: 'averageSpeed',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px', 'padding': '6px 0px' },
        class: '',
        translate: 'REPORT.ENGINE.COLUMN.COUNT',
        autoHide: true,
        width: 100,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px','font-size':"14px" },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 117,
      },

    ];
    
  }


}




