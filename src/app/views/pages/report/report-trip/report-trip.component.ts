import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef,EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { ReportRouteService, ReportSummaryTripService } from '@core/report';
import { DeviceService,TripService } from '@core/manage';


import { TranslateService } from '@ngx-translate/core';
import { ToastService, ValidatorCustomService, UserDatePipe, FunctionService } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { ExcelService,PdfmakeService,ExcelTestService} from '@core/utils';
import { XLSXModel, XLSXModelTest } from '@core/utils/xlsx/excel.service';
import { PdfModel } from '@core/utils/pdfmake/pdfmake.service';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { PageService,itemPage} from '@core/_base/layout';

import { DatePipe } from '@angular/common';
import { UserDateService } from '@core/_base/layout';
import { DecimalPipe } from '@angular/common';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import * as moment from 'moment';
import { EventEmitterService } from '@core/common/_service/eventEmitter.service'
declare var $: any;
@Component({
  selector: 'report-trip',
  templateUrl: './report-trip.component.html',
  styleUrls: ['./report-trip.component.scss'],
  providers: [DatePipe, UserDatePipe, DecimalPipe]

})
export class ReportTripComponent implements OnInit {
  [x: string]: any;

  @ViewChild('listDevicePicker', { static: true }) 
  listDevicePicker:any;
  
  @ViewChild('datePicker', { static: true }) 
  datePicker: DateRangePickerComponent;
  formRoute: FormGroup;
  searchFormGeofence: FormGroup;
  formCheckList: FormGroup;
  selectdp: ElementRef;
  public datePickerOptions: any = {
    size: 'md',
    select: 'from',
    ranges: 'from',
    optionDatePicker: {
      maxSpan: {
        days: 31
      }
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: true,
    startDate:moment().subtract(1, "days").startOf('day'),
    endDate:moment().subtract(1, "days").endOf('day'),
  };
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public dataExport: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataDefault: any = [{}];
  public filter: any;
  private unsubscribe: Subject<any>;
  public listDevices: any = [];
  public listDeviced: any = [];

  public listTrips: any = [];
  public userIdSelected: number;
  public userIdSelectedEdit: number;
  public showUserTree: boolean = false;
  public dateStart: string;
  public dateEnd: string;
  public timeStart: string;
  public timeEnd: string;
  public timeStop:number;
  public deviceId: string;
  public geofenceId: string;

  public deviceName: string;
  public dateFormat: string;
  public timeFormat: string;
  public timeZone:string;
  public totalAllRowData : any;
  public parentBread:any;
  public isdownLoad : boolean = false;
  public isdownLoadPDF:boolean = false;
  public closeResult: string;
  public geofencePopup : any;
  public objGeofenceSummary:itemPage;
  public paramsEmiter: EventEmitter<any>;
  public dataPagination:any = [];
  constructor(
    private summary:ReportSummaryTripService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private deviceService: DeviceService,
    private translate: TranslateService,
    private toast: ToastService,
    private currentUser: CurrentUserService,
    private xlsx: ExcelService,
    public datepipe: DatePipe,
    private userDate: UserDateService,
    private userDatePipe: UserDatePipe,
    private pdf : PdfmakeService,
    private trip: TripService,
    private modalService: NgbModal,
    private page:PageService,
    private emiter:EventEmitterService,
    private functionService : FunctionService

  ) {
    $(function () {
      $('.kt_selectpicker').selectpicker();
    });
    this.paramsEmiter = new EventEmitter();

    this.objGeofenceSummary = this.page.getPageById(1222312);
    this.dateFormat = this.currentUser.dateFormatPipe;
    this.timeFormat = this.currentUser.timeFormat;
    this.timeZone = this.currentUser.currentUser.timezone;
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.parentBread = [
      {
        name: this.translate.instant('REPORT.REPORTS.GENERAL.REPORTS'),
        translate: "REPORT.REPORTS.GENERAL.REPORTS",
        link: "/reports",
        icon: "flaticon2-graph"
      }
    ]
  }

  ngOnInit() {
    this.refreshEventType();
    this.updateDataTable();
  }

  buildDateNow() {
    this.datePicker.refresh();
    this.deviceId = '';
    this.geofenceId = '';
    setTimeout(() =>{
      $('.kt_selectpicker').selectpicker('refresh');
    });
  }

  private buildForm(): void {

    this.searchFormGeofence = this.formBuilder.group({
      dateStart: [''],
      dateEnd: [''],
      deviceId: [''],
      tripId:[''],
    });
  }

  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
   }


  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
    };
  }

  ChangeUserTree(value) {
    if (value.id > 0) {
      this.userIdSelectedEdit = value.id;
     
      this.getDevices(value.id);
      this.getTrips(value.id);
      // this.listDevicePicker.reset();

      // this.deviceId = '';
      this.buildDateNow();
      this.dataTable.update({
        data: [],
        totalRecod: 0
      });
    }
  }

  getNameDevice(text: string) {
    this.deviceName = text;
  }

  getDevices(userId) {
    let params = {
      userId: userId,
      pageNo: -1
    };
    // this.filter.pageNo = -1;
    this.deviceService.list({ params: params }).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      this.listDevices = data.result;
      this.paramsEmiter.emit(this.listDevices);

      setTimeout(() => {
        $('#device').selectpicker('refresh');
      });
    });

  }

  changeChecked(event){
    this.listDeviced = event.listChecked;
  }

  getTrips(userId) {
    let params = {
      userId: userId, 
      pageNo: -1
    };
    // this.filter.pageNo = -1;
    this.trip.list(params).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      this.listTrips = data.result;
      // data test
      setTimeout(() => {
        $('#tripId').selectpicker('refresh');
      });
    });

  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  validateForm(deviceId: string) {
    let timeStart = new Date(this.dateStart).getTime();
    let timeEnd = new Date(this.dateEnd).getTime();
    if (timeStart > timeEnd) {
      this.toast.show(
        {
          message: this.translate.instant('REPORT.ROUTE.MESSAGE.ERROR_TIME'),
          type: 'error',
        })
      return false;
    }
    if (deviceId == '' || deviceId == undefined || deviceId == null) {
      this.toast.show(
        {
          message: this.translate.instant('MESSEAGE_CODE.SENSOR.DEVICE_NOT_FOUND'),
          type: 'error',
        })
      return false;
    }
    return true;
  }

  getListFilter(){

    let listDevices: any = [];
    let listTrips:any = [];
    
    
    // if(this.listDeviced.length == 0)
    // {
    //   this.listDevices.forEach(device => {
    //     this.listDeviced.push(device.id);
    //   });
    // }

    if (this.searchFormGeofence.value.geofenceId == '' || this.searchFormGeofence.value.geofenceId == null || this.searchFormGeofence.value.geofenceId == undefined) {
        this.listTrips.forEach(geofence => {
            listTrips.push(geofence.id);
        }); 
      } else {
        listTrips = this.searchFormGeofence.value.geofenceId;
      }

    // if(this.searchFormGeofence.value.timeStop < 1 || this.searchFormGeofence.value.timeStop == null || this.searchFormGeofence.value.timeStop == '') this.searchFormGeofence.value.timeStop = 0;
    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;
    // this.filter.deviceIds = 130,
    // this.filter.deviceIds = this.listDeviced.toString();
    // this.filter.deviceIds = '510';
    // this.filter.timeFrom = "2020-07-01 00:00:00";
    // this.filter.timeTo = "2020-07-02 00:00:00";
    this.filter.userId = this.userIdSelectedEdit;
    this.filter.tripIds  = listTrips.toString();

  }
  
  searchGeofence(form: any) {
    this.getListFilter();
    this.dataTable.reload({ currentPage: 1 });

  }

  resetFormSearch() {
    this.buildDateNow();
    $('#geofence').val('').selectpicker("refresh");     
    // $('#device').val('').selectpicker("refresh");   
    // this.listDevicePicker.reset();

  }

  getColorGeofence(geofenceId)
  {
      let color = '';
      color = this.listTrips.find(x => x.id == geofenceId).color;
      return color;
  }

  getTypeGeofence(geofenceId)
  {
      let type = '';
      type = this.listTrips.find(x => x.id == geofenceId).type;
      return type;
  }

  
  getFillGeofence(geofenceId)
  {
      let fill = '';
      fill = this.listTrips.find(x => x.id == geofenceId).fillColor;
      return fill;
  }

  RoutedetailGeofence(geofenceId,deviceId)
  {
    let params = {
      tripId:geofenceId,
      deviceId:deviceId,
      timeStart:this.dateStart,
      timeEnd :this.dateEnd,
    };
this.emiter.geofenceDetail = params;
    this.emiter.detailGeofence(params);
    // const url = this.router.serializeUrl(
    //   this.router.createUrlTree([`/custompage/${city.id}`])
    // );
  
    // window.open(url, '_blank');
  }

  private refreshEventType()
  {
     this.translate.onLangChange.subscribe((event) => {
     let device = this.translate.instant('REPORT.GEOFENCE.SUMMARY.ALL_DEVICE');
     let geofence = this.translate.instant('REPORT.GEOFENCE.SUMMARY.ALL_GEOFENCE');
     
     $(function () {
       $('#device').selectpicker({title: device}).selectpicker('refresh');
       $('#geofence').selectpicker({title: geofence}).selectpicker('refresh');

     });
   });
  }

  open(content,geofenceId) {

    this.geofencePopup = {
        address: "Đường Đại Từ, Phường Đại Kim, Hoàng Mai, Hà Nội",

    };
    this.geofencePopup.geofence =  this.listTrips.find(x => x.id == geofenceId);
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal--fit kt-modal-clear', size: 'lg', backdrop: 'static'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  async getData(filter,type?:string) {
    if(type == 'pdf') this.isdownLoadPDF =true;
    if(type == 'excel') this.isdownLoad = true;

    let _this = this;
    
    
  await this.summary.list(filter)
      .pipe(
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          if(type == 'pdf') _this.isdownLoadPDF = false;
          if(type == 'excel') _this.isdownLoad = false;
          let pageNo = this.filter.pageNo - 1;
          let pageSize = this.filter.pageSize;
          let j = 1;
          this.totalRecod = result.result.length;
          let data:any = [];
          result.result = result.result.map(itemData=>{

              if(j > pageNo*pageSize && j <= (pageNo+1)*pageSize -1)
              {
                let count = pageNo * pageSize + j;

                let totalDevice = '';
                totalDevice = itemData.deviceSuccess+'/'+itemData.deviceSuccess+itemData.deviceFail;
                itemData.totalDevice = totalDevice;
                itemData.totalTime = this.convertNumberToTime(itemData.timeSuccess);

                itemData.count = count;
                j++;
          
              }
              data.push(itemData);
              return itemData;
              });    
            this.dataExport = 
            [
              {
                data:data
              }
            ];      
                
          this.data = result.result;
      
          this.totalRecod = j-1;
          this.dataPagination = data;
          this.getDataShowPage();
          // console.log(result.result);
          if(!type)
          {
          
            this.dataTable.update({
              data: this.data,
              totalRecod: this.totalRecod
            });
          }
          if(type == 'pdf')     this.exportFilePDF(this.data);
          if(type == 'excel')    this.exportFileXLSX(this.data);
          this.showFullData = false;

        }
      })
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;
        this.getData(this.filter);
      }),

    ).subscribe();
  }

  getDataShowPage()
  {
    this.data = [];
    this.data = this.functionService.pagination(this.dataPagination,this.filter.pageNo,this.filter.pageSize,1);
  }

  convertNumberToTime(number) {
    return this.userDate.convertNumberToTime(number);
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
        responsive: false,
      }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  roundNumber(number) {
    return Math.round(number * 100) / 100;
  }

  

  //pdf
  async exportPDF(){
      this.getListFilter();
      this.getData(this.filter,'pdf');

  }
  
  exportFilePDF(data){
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let config:PdfModel = {
      info: {
        title: 'SummaryTrip',
      },
      content:{
        header:{
          title: this.translate.instant('REPORT.TRIP.GENERAL.NAME'),
          params:{
            timeStart:": "+this.dateStart,
            timeEnd:": "+this.dateEnd,
            timeZone:": "+this.timeZone,
          }
        },
        table:[
          {
              woorksheet: {
                name: this.translate.instant('REPORT.TRIP.GENERAL.NAME'),
              },
              total : {
                // group : [
                //   { columnData:'geoName'},
                // ],
                // totalCol:[
                //     { text:this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL'),style:{colSpan:2,alignment:"right",bold:true,fillColor:"#fff"}},
                //     { text:'Tổng',colSpan:4,alignment:"right",bold:true,fillColor:"#fff"},

                //     { columnData:'totalTrip',style:{alignment:"left",bold:true,fillColor:"#fff"}},
                //     { columnData:'totalTime',style:{alignment:"center",bold:true,fillColor:"#fff"}},
                // ],
            
              },
              headerRows: 1,
              widths: ['10%','30%','30%','30%'],
              header :[           
                  [ 
                    { text: '#',alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.TRIP.GENERAL.NAME_TRIP'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.TRIP.COLUMN.TOTAL_DEVICE'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.TRIP.COLUMN.TOTAL_TIME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                  ]
              ],
              body:[
                {columnData:'count',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'tripName',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'totalDevice',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'totalTime',style:{alignment:"center",fillColor:"#fff",bold:false}},
              ],              
          },
          
        ]
          },
      footer:{
        title: this.datepipe.transform(this.dateStart, dateTimeFormat)+ ' - ' + this.datepipe.transform(this.dateEnd, dateTimeFormat)
      }    
    }

    this.pdf.ExportPdf([this.dataExport],config);
    // this.xlsx.exportFileTest([data], config, {});
 
  }
  
  //export
  exportXlSX() {
    this.getListFilter(); 
    this.getData(this.filter,'excel');

  }
  private exportFileXLSX(data) {
    
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let config: XLSXModelTest = {
      file: {
        title: this.translate.instant('REPORT.TRIP.GENERAL.NAME'),
        prefixFileName: "SummaryTrip",
        timeStart:this.dateStart,
        timeEnd:this.dateEnd,
      },
      template: [{
        header: [
          {
            text: this.translate.instant('REPORT.TRIP.GENERAL.NAME'),
            type: "header",
          },
          {
            text: ""
          },
          {
            text: '!timezone',
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START') + ": " + this.dateStart,
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_END') + ": " + this.dateEnd,
          },
         
          {
            text: ""
          },
        ],
        columns: [
          {
            name: "#",
            columnData: "count",
            wch: 5,
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
          },
          {
            name: this.translate.instant('REPORT.TRIP.GENERAL.NAME_TRIP'),
            columnData: "tripName",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 30,
          },
          {
            name: this.translate.instant('REPORT.TRIP.COLUMN.TOTAL_DEVICE'),
            columnData: "totalDevice",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 30,
          },
          {
            name: this.translate.instant('REPORT.TRIP.COLUMN.TOTAL_TIME'),
            columnData: "totalTime",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 30,
          },


        ],
        woorksheet: {
          name: this.deviceName,
        },
        total: {
        
        }
      }]

    };
    this.xlsx.exportFileTest([this.dataExport], config, {});
  }
  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'device',
        field: 'Engine',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px'},
        class: '',
        translate: 'REPORT.TRIP.GENERAL.NAME_TRIP',     
        autoHide: false,
        width: 200,
      },
      {
        title: 'total trip',
        field: 'Engine',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '250px' },
        class: '',
        translate: 'REPORT.TRIP.COLUMN.TOTAL_DEVICE',     
        autoHide: false,
        width: 200,
      },
      {
        title: 'total time',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px','text-align':'center'  },
        class: '',
        translate: 'REPORT.TRIP.COLUMN.TOTAL_TIME',     
        autoHide: false,
        width: 200,
        fuel:true
      },

      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' ,'text-align':'center'},
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 100,
      },

    ]
  }


}




