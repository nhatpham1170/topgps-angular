import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { ReportRouteService } from '@core/report';
import { DeviceService } from '@core/manage';
import { TranslateService } from '@ngx-translate/core';
import { ToastService, ValidatorCustomService, UserDatePipe } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { ExcelService,PdfmakeService,ExcelTestService} from '@core/utils';
import { XLSXModel, XLSXModelTest } from '@core/utils/xlsx/excel.service';
import { PdfModel } from '@core/utils/pdfmake/pdfmake.service';

import { DatePipe } from '@angular/common';
import { UserDateService } from '@core/_base/layout';
import { DecimalPipe } from '@angular/common';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { UserTreeService } from '@core/common';
import { MapUtilityService, MapUtil } from '@core/utils/map';
import { isNull } from 'util';


declare var $: any;
@Component({
  selector: 'report-route',
  templateUrl: './report-route.component.html',
  styleUrls: ['./report-route.component.scss'],
  providers: [DatePipe, UserDatePipe, DecimalPipe]

})
export class ReportRouteComponent implements OnInit {
  @ViewChild('datePicker', { static: true }) 

  datePicker: DateRangePickerComponent;
  formRoute: FormGroup;
  searchFormRoute: FormGroup;
  formCheckList: FormGroup;
  selectdp: ElementRef;
  public datePickerOptions: any = {
    size: 'md',
    select: 'from',
    ranges: 'from',
    optionDatePicker: {
      maxSpan: {
        days: 31
      }
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: true,

  };
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public dataExport: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataDefault: any = [{}];
  public filter: any;
  private unsubscribe: Subject<any>;
  public listDevices: any = [];
  public userIdSelected: number;
  public userIdSelectedEdit: number;
  public showUserTree: boolean = true;
  public dateStart: string;
  public dateEnd: string;
  public timeStart: string;
  public timeEnd: string;
  public deviceId: string;
  public deviceName: string;
  public dateFormat: string;
  public timeFormat: string;
  public timeZone:string;
  public totalAllRowData : any;
  public parentBread:any;
  public isdownLoad : boolean = false;
  public address:string = '';
  public isdownLoadPDF:boolean = false;
  constructor(
    private Route: ReportRouteService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private deviceService: DeviceService,
    private translate: TranslateService,
    private toast: ToastService,
    private currentUser: CurrentUserService,
    private xlsx: ExcelService,
    public datepipe: DatePipe,
    private userDate: UserDateService,
    private userDatePipe: UserDatePipe,
    private pdf : PdfmakeService,
    private userTreeService: UserTreeService,
    private mapUtility:MapUtilityService,
    private mapUtil: MapUtil,

  ) {
    this.dateFormat = this.currentUser.dateFormatPipe;
    this.timeFormat = this.currentUser.timeFormat;
    this.timeZone = this.currentUser.currentUser.timezone;
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.parentBread = [
      {
        name: this.translate.instant('REPORT.REPORTS.GENERAL.REPORTS'),
        translate: "REPORT.REPORTS.GENERAL.REPORTS",
        link: "/reports",
        icon: "flaticon2-graph"
      }
    ]
  }

  ngOnInit() {
    this.updateDataTable();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();
  }

  buildDateNow() {
    this.datePicker.refresh();
    this.deviceId = '';
    setTimeout(function () {
      $('.kt_selectpicker').selectpicker('refresh');
    }, 100);
  }

  private buildForm(): void {

    this.searchFormRoute = this.formBuilder.group({
      dateStart: [''],
      dateEnd: [''],
      deviceId: ['']
    });
  }

  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
   }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
    };
  }

  ChangeUserTree(value) {
    if (value.id > 0) {
      this.userIdSelectedEdit = value.id;
      this.getDevices(value.id);
      // this.deviceId = '';
      this.buildDateNow();
      this.dataTable.update({
        data: [],
        totalRecod: 0
      });
    }
  }

  getNameDevice(text: string) {
    this.deviceName = text;
  }

  getDevices(userId) {
    let params = {
      userId: userId,
      pageNo: -1
    };
    // this.filter.pageNo = -1;
    this.deviceService.list({ params: params }).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      this.listDevices = data.result;
      setTimeout(function () {
        $('.kt_selectpicker').selectpicker('refresh');
      }, 100);
    });

  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  validateForm(deviceId: string) {
    let timeStart = new Date(this.dateStart).getTime();
    let timeEnd = new Date(this.dateEnd).getTime();
    if (timeStart > timeEnd) {
      this.toast.show(
        {
          message: this.translate.instant('REPORT.ROUTE.MESSAGE.ERROR_TIME'),
          type: 'error',
        })
      return false;
    }
    if (deviceId == '' || deviceId == undefined || deviceId == null) {
      this.toast.show(
        {
          message: this.translate.instant('MESSEAGE_CODE.SENSOR.DEVICE_NOT_FOUND'),
          type: 'error',
        })
      return false;
    }
    return true;
  }

  searchRoute(form: any) {

    if (!this.validateForm(form.value.deviceId)) return;
    // this.dataTable.reload({ currentPage: 1 });
    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;
    this.filter.deviceId = form.value.deviceId;
    // this.getData(this.filter);
    this.dataTable.reload({ currentPage: 1 });

  }

  resetFormSearch() {
    this.buildDateNow();
  }

  async getGeocode(item) {
    // get address if isEmpty
    if (!item.startAddress || item.startAddress.length == 0) {
      if (isNull(item.startlatitude) || isNull(item.startlongitude)) return;
      let address = (item.startlatitude || 0).toFixed(5) + ", " + (item.startlongitude || 0).toFixed(5);
      item.addressLoading = true;
      // this.cdr.detectChanges();
      await this.Route.geocode({ params: { lat: item.startlatitude, lng: item.startlongitude } }).pipe(
        tap(
          data => {
            address = data.result.address;
          }
        ),
        finalize(() => {
          item.addressLoading = false;
          item.startAddress = address;
          this.cdr.detectChanges();
          this.cdr.markForCheck();
        })
      ).toPromise();
    }

    if (!item.stopAddress || item.stopAddress.length == 0) {
      if (isNull(item.stoplatitude) || isNull(item.stoplongitude)) return;
      let address = (item.stoplatitude || 0).toFixed(5) + ", " + (item.stoplongitude || 0).toFixed(5);
      item.addressLoading = true;
      // this.cdr.detectChanges();
      this.Route.geocode({ params: { lat: item.stoplatitude, lng: item.stoplongitude } }).pipe(
        tap(
          data => {
            address = data.result.address;
          }
        ),
        finalize(() => {
          item.stopAddress = false;
          item.stopAddress = address;
          this.cdr.detectChanges();
          this.cdr.markForCheck();
        })
      ).subscribe();
    }
  }

  getData(filter) {
    this.Route.list(filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          let pageNo = this.filter.pageNo - 1;
          let pageSize = this.filter.pageSize;
          this.data = result.result.content;
          let j = 1;
          result.result.content = result.result.content.map(day => {
            if (day.data) {
              day.data = day.data.map(row => {
          
                this.getGeocode(row);
                let count = pageNo * pageSize + j;
                let distance = this.roundNumber(row.distance);
                row.distance = distance;
                row.count = count;
                j++;
                return row;
              });
              return day;
            }
          });
          this.totalRecod = result.result.totalRecord
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });

          setTimeout(function () {
            $('.kt_selectpicker').selectpicker('refresh');
          }, 100);
        }
      })
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;
        this.getData(this.filter);
      }),

    ).subscribe();
  }

  convertNumberToTime(number) {
    return this.userDate.convertNumberToTime(number);
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
        responsive: false,

      }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  roundNumber(number) {
    return Math.round(number * 100) / 100;
  }

 

  async getdataExcel(file) {
    if(file =='pdf')
    {
      this.isdownLoadPDF = true;
    }else
    {
      this.isdownLoad = true;
    }
    let params = {
      timeFrom: this.dateStart,
      timeTo: this.dateEnd,
      pageNo: -1,
      deviceId: this.searchFormRoute.value.deviceId
    };
   await this.Route.list(params)
      .pipe(
        tap((result: any) => {
          if (result.status == 200) {
            let all_countRowData = 0;
            let all_totalDuration: any = 0;
            let all_maxSpeed = 0;
            let all_totalDistance = 0;
            let all_totalavgspeed = 0;
            result.result = result.result.map(day => {
              let countRowData = 0;
              let totalDuration = 0;
              let maxSpeed = 0;
              let totalDistance = 0;
              let totalavgspeed = 0;
              let dateGroup = this.userDatePipe.transform(day.timeFrom, "date", null, "datetime");
              day.timeFromUtc = dateGroup;
              if (day.data) {
                let points = [];
                day.data = day.data.map(row => {
                  if(row.startAddressStr == '') points.push([row.startlatitude,row.startlongitude]);
                  // this.getGeocode(row);
                  countRowData++;
                  totalDuration = totalDuration + row.duration;
                  totalavgspeed = totalavgspeed + row.avgspeed;
                  if (maxSpeed < row.maxspeed) maxSpeed = row.maxspeed;
                  row.startAddressStr = this.userDatePipe.transform(row.fromUtc, "HH:mm:ss", null, "datetime");
                  row.stopAddressStr = this.userDatePipe.transform(row.toUtc, "HH:mm:ss", null, "datetime");
                  totalDistance = totalDistance + row.distance;
                  row.distance = this.roundNumber(row.distance);
                  
                  row.durationTime = this.convertNumberToTime(row.duration);
                  row.count = countRowData
                  return row;
                });
                // console.log(points);
                let encodePoints = this.mapUtil.encode(points);
                
                //all
                all_countRowData = all_countRowData + countRowData;
                all_totalDuration = all_totalDuration + totalDuration;
                all_totalDistance = all_totalDistance + totalDistance;
                all_totalavgspeed = all_totalavgspeed + totalavgspeed;
                if (all_maxSpeed < maxSpeed) all_maxSpeed = maxSpeed;
  
              }
              // add result
              day.countRowData = countRowData;
              day.totalDuration = this.convertNumberToTime(totalDuration);;
              day.maxSpeed = maxSpeed;
              day.totalDistance = this.roundNumber(totalDistance);
              totalavgspeed = Math.round(totalDistance / (totalDuration/3600) * 100) / 100;
              if (countRowData == 0) totalavgspeed = 0;
              day.totalavgspeed = totalavgspeed;
  
              return day;
            });
            if (all_countRowData != 0) all_totalavgspeed = Math.round(all_totalDistance / (all_totalDuration/3600) * 100) / 100;
            all_totalDuration = this.convertNumberToTime(all_totalDuration);
            this.totalAllRowData = {
              count: all_countRowData,
              duration: all_totalDuration,
              distance: this.roundNumber(all_totalDistance),
              maxSpeed: all_maxSpeed,
              avgspeed: all_totalavgspeed
            };
            this.dataExport = result.result;
            
            if(file =='pdf')
            {
              this.isdownLoadPDF = false;
            }else
            {
              this.isdownLoad = false;
            }
            this.isdownLoad = false;
            this.isdownLoadPDF = false;
          }
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .toPromise();
      
      if(file =='pdf')
      {
        // this.pdf.ExportPdf(this.dataExport,this.totalAllRowData,param_header,footer);
        this.exportFilePDF(this.dataExport);
      }
      else
      {
       this.exportFileXLSX(this.dataExport);
      }  
  }

  //pdf
  async exportPDF(){

    let validate = this.validateForm(this.searchFormRoute.value.deviceId);
    if (validate )
     {
      this.getdataExcel('pdf')
     }
  }
  exportFilePDF(data){
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let config:PdfModel = {
      info: {
        title: 'Route'
      },
      content:{
        header:{
          title:this.translate.instant('REPORT.ROUTE.EXCEL.REPORT_ROUTE'),
          params:{
            timeStart:": "+this.dateStart,
            timeEnd:": "+this.dateEnd,
            deviceName:": "+this.deviceName,
            timeZone:": "+this.timeZone
          }
        },
        table:[
          {
              woorksheet: {
                name: this.deviceName,
              },
              total : {
                group : [
                  { columnData:'timeFromUtc'},
                ],
                totalCol:[
                    { text:this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL'),style:{colSpan:5,alignment:"right",bold:true,fillColor:"#fff"}},
                    { text:'Tổng',colSpan:5,alignment:"right",bold:true,fillColor:"#fff"},
                    { text:'Tổng',colSpan:5,alignment:"right",bold:true,fillColor:"#fff"},
                    { text:'Tổng',colSpan:5,alignment:"right",bold:true,fillColor:"#fff"},
                    { text:'Tổng',colSpan:5,alignment:"right",bold:true,fillColor:"#fff"},
                    { columnData:'totalDistance' ,style:{alignment:"right",bold:true,fillColor:"#fff"}},
                    { columnData:'totalDuration',style:{alignment:"right",bold:true,fillColor:"#fff"}},
                    { columnData:'totalavgspeed',style:{alignment:"right",bold:true,fillColor:"#fff"}},
                    { columnData:'maxSpeed',style:{alignment:"right",bold:true,fillColor:"#fff"}},
                ],
                totalAll:{
                  title : [{ text:this.translate.instant('MAP.TRACKING.ROUTE.SUMMARY'),alignment:"left",bold:true,fillColor:"#fff",fontSize:20,margin: [0, 20, 0, 5]}],
                  content : [
                  [
                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.TRIPS'),fillColor:"#EEEEEE"},
                    { text:this.totalAllRowData.count}
                  ],
                  [
                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL_DISTANCE'),fillColor:"#EEEEEE"},
                    { text:this.totalAllRowData.distance}
                  ],
                  [
                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.TIME_DURATION'),fillColor:"#EEEEEE"},
                    { text:this.totalAllRowData.duration}
                  ],
                  [
                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.AVERAGE_SPEED'),fillColor:"#EEEEEE"},
                    { text:this.totalAllRowData.avgspeed}
                  ],
                  [
                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.MAX_SPEED'),fillColor:"#EEEEEE"},
                    { text:this.totalAllRowData.maxSpeed}
                  ],
                ]
              }
              },
              headerRows: 2,
              widths: ['4%','8%','20%','8%','20%','10%','10%','10%','10%'],
              header :[           
                  [ 
                    { text: '#',rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_START'),colSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true},
                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_START'),colSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true},
                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_END'),colSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true},
                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_END'),colSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true},
                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL_DISTANCE'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,6,0,0]},
                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.TIME_DURATION'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",margin:[0,6,0,0],bold:true},
                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.AVERAGE_SPEED'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",margin:[0,6,0,0],bold:true},
                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.MAX_SPEED'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",margin:[0,6,0,0],bold:true},
                  ],
                  [
                    '',
                    { text: this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('COMMON.COLUMN.ADDRESS'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('COMMON.COLUMN.ADDRESS'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},        
                    '','','','',
                  ]
              ],
              body:[
                {auto:true},
                {columnData:'startAddressStr',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'startAddress',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'stopAddressStr',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'stopAddress',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'distance',style:{alignment:"right",fillColor:"#fff",bold:false}},
                {columnData:'durationTime',style:{alignment:"right",fillColor:"#fff",bold:false}},
                {columnData:'avgspeed',style:{alignment:"right",fillColor:"#fff",bold:false}},
                {columnData:'maxspeed',style:{alignment:"right",fillColor:"#fff",bold:false}},
              ],              
          },
          
        ]
          },
      footer:{
        title: this.datepipe.transform(this.dateStart, dateTimeFormat)+ ' - ' + this.datepipe.transform(this.dateEnd, dateTimeFormat)+', '+this.deviceName
      }    
    }
    this.pdf.ExportPdf([this.dataExport],config);
    // this.xlsx.exportFileTest([data], config, {});
 
  }
  
  //export
  exportXlSX() {
    let validate = this.validateForm(this.searchFormRoute.value.deviceId);
    if (validate) this.getdataExcel('excel');
  }
  private exportFileXLSX(data) {
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let config: XLSXModelTest = {
      file: {
        title: this.translate.instant('REPORT.ROUTE.GENERAL.REPORT_DEVICE'),
        prefixFileName: "ReportRoute"
      },
      template: [{
        header: [
          {
            text: this.translate.instant('REPORT.ROUTE.EXCEL.REPORT_ROUTE'),
            type: "header",
          },
          {
            text: ""
          },
          
          {
            text: "!timezone"
          },
          
          {
            text: ""
          },
          
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START') + ": " + this.datepipe.transform(this.dateStart, dateTimeFormat),
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_END') + ": " + this.datepipe.transform(this.dateEnd, dateTimeFormat),
          },
          {
            text: this.translate.instant('MANAGE.DEVICE.GENERAL.DEVICE_NAME') + ": " + this.deviceName,
          },
          
          {
            text: ""
          },
        ],
        columns: [
          {
            name: "#",
            columnData: "auto",
            wch: 5,
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
          },
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_START'),
            columnData: "startAddressStr",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 10,
          },
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_START'),
            columnData: "startAddress",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 40,
          },
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_END'),
            columnData: "stopAddressStr",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 10,
          },
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_END'),
            columnData: "stopAddress",
            type: "mergeTime",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },

            wch: 40
          },

          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL_DISTANCE'),
            columnData: "distance",
            style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },
            wch: 20
          },
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.TIME_DURATION'),
            columnData: "durationTime",
            type: "time",
            style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },
            wch: 20
          },
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.AVERAGE_SPEED'),
            columnData: "avgspeed",
            style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },
            wch: 20
          },
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.MAX_SPEED'),
            columnData: "maxspeed",
            style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },

            wch: 20
          },

        ],
        columnsMerge:
        {
          data: [
            "",
            this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),
            this.translate.instant('COMMON.COLUMN.ADDRESS'),
            this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),
            this.translate.instant('COMMON.COLUMN.ADDRESS')
            , "", "", "", ""],
          position: ["A9:A10", "B9:C9", "D9:E9", "F9:F10", "G9:G10", "H9:H10", "I9:I10"]
        },
        woorksheet: {
          name: this.deviceName,
        },
        total: {
          group: "timeFromUtc",
          totalCol: {
            field: [
              {
                text: this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL'),
                style: {
                  font: { name: 'Times New Roman', sz: 14, bold: true },
                  alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                },
                merge: {
                  full: false,
                  range: "4"
                }
              },
              {
                text: " ",
              },
              {
                text: " ",
              },
              {
                text: " ",
              },
              {
                text: " ",
              },
              {
                columnData: "totalDistance",
                style: {
                  font: { name: 'Times New Roman', sz: 14, bold: true },
                  alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                }
              },
              {
                columnData: "totalDuration",
                style: {
                  font: { name: 'Times New Roman', sz: 14, bold: true },
                  alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                }
              },
              {
                columnData: "totalavgspeed",
                style: {
                  font: { name: 'Times New Roman', sz: 14, bold: true },
                  alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                }
              },
              {
                columnData: "maxSpeed",
                style: {
                  font: { name: 'Times New Roman', sz: 14, bold: true },
                  alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                }
              }

            ]
          },
          totalAll: {
            position: {
              top: 2,
              left: 7
            },
            data: [

              {
                key: this.translate.instant('REPORT.ROUTE.COLUMN.TRIPS'),
                value: this.totalAllRowData.count,
                style: { alignment: { vertical: "center", horizontal: "left", wrapText: true }, }
              },
              {
                key: this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL_DISTANCE'),
                value: this.totalAllRowData.distance,
                style: { alignment: { vertical: "center", horizontal: "left", wrapText: true }, }
              },
              {
                key: this.translate.instant('REPORT.ROUTE.COLUMN.TIME_DURATION'),
                value: this.totalAllRowData.duration,
                style: { alignment: { vertical: "center", horizontal: "left", wrapText: true }, }
              },
              {
                key: this.translate.instant('REPORT.ROUTE.COLUMN.AVERAGE_SPEED'),
                value: this.totalAllRowData.avgspeed,
                style: { alignment: { vertical: "center", horizontal: "left", wrapText: true }, }
              },
              {
                key: this.translate.instant('REPORT.ROUTE.COLUMN.MAX_SPEED'),
                value: this.totalAllRowData.maxSpeed,
                style: { alignment: { vertical: "center", horizontal: "left", wrapText: true }, }
              },
            ]
          }

        }
      }]

    };
    this.xlsx.exportFileTest([data], config, {});
  }
  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Move start',
        field: 'moveStart',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '250px' },
        class: '',
        translate: 'REPORT.ROUTE.COLUMN.MOVE_START',
        autoHide: false,
        width: 200,
      },
      {
        title: 'Route',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '250px' },
        class: '',
        translate: 'REPORT.ROUTE.COLUMN.MOVE_END',
        autoHide: false,
        width: 200,
      },
      {
        title: 'Created ',
        field: 'distance',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'REPORT.ROUTE.COLUMN.TOTAL_DISTANCE',
        autoHide: false,
        width: 150,
      },
      {
        title: 'Sended',
        field: 'duration',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'REPORT.ROUTE.COLUMN.TIME_DURATION',
        autoHide: false,
        width: 150,
      },
      {
        title: 'Time send',
        field: 'averageSpeed',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px', 'padding': '6px 0px' },
        class: '',
        translate: 'REPORT.ROUTE.COLUMN.AVERAGE_SPEED',
        autoHide: false,
        width: 150,
      },
      {
        title: 'Time send',
        field: 'maxSpeed',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px', 'text-align': 'left' },
        class: '',
        translate: 'REPORT.ROUTE.COLUMN.MAX_SPEED',
        autoHide: false,
        width: 150,
      },

    ]
  }


}




