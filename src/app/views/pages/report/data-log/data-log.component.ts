import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { ReportRouteService, ReportsService } from '@core/report';
import { DeviceService, SensorService } from '@core/manage';
import { TranslateService } from '@ngx-translate/core';
import { ToastService, ValidatorCustomService, UserDatePipe, UserDateAdvPipe } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { ExcelService,PdfmakeService,ExcelTestService} from '@core/utils';
import { XLSXModel, XLSXModelTest } from '@core/utils/xlsx/excel.service';
import { PdfModel } from '@core/utils/pdfmake/pdfmake.service';

import { DatePipe } from '@angular/common';
import { DecimalPipe } from '@angular/common';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { UserTreeService } from '@core/common';
import { TrackingService } from '@core/map';

declare var $: any;
@Component({
  selector: 'kt-data-log',
  templateUrl: './data-log.component.html',
  styleUrls: ['./data-log.component.scss'],
  providers: [DatePipe, UserDatePipe, DecimalPipe,UserDateAdvPipe]

})
export class DataLogComponent implements OnInit {
  @ViewChild('datePicker', { static: true }) 

  datePicker: DateRangePickerComponent;
  formRoute: FormGroup;
  searchFormRoute: FormGroup;
  formCheckList: FormGroup;
  selectdp: ElementRef;
  public datePickerOptions: any = {
    size: 'md',
    select: 'from',
    ranges: 'from',
    optionDatePicker: {
      maxSpan: {
        days: 31
      }
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: true,

  };
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public dataExport: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataDefault: any = [{}];
  public filter: any;
  private unsubscribe: Subject<any>;
  public listDevices: any = [];
  public userIdSelected: number;
  public userIdSelectedEdit: number;
  public showUserTree: boolean = true;
  public dateStart: string;
  public dateEnd: string;
  public timeStart: string;
  public timeEnd: string;
  public deviceId: string;
  public deviceName: string;
  public dateFormat: string;
  public timeFormat: string;
  public timeZone:string;
  public totalAllRowData : any;
  public parentBread:any;
  public isdownLoad : boolean = false;
  public isdownLoadPDF:boolean = false;
  public deviceTemplate:any;
  public dataPointsByDay:any = [];
  public dataPointsByDayExport:any = [];
  public styleDay = {
    "line-height": "10px", "width": "100px",
    "background":""
  };
  constructor(
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    private toast: ToastService,
    private currentUser: CurrentUserService,
    private xlsx: ExcelService,
    public datepipe: DatePipe,
    private pdf : PdfmakeService,
    private userTreeService: UserTreeService,
    private reportsService:ReportsService,
    private userDateAdv:UserDateAdvPipe,
    private decimalPipe:DecimalPipe,
    private trackingService:TrackingService,
  ) {
    this.dateFormat = this.currentUser.dateFormatPipe;
    this.timeFormat = this.currentUser.timeFormat;
    this.timeZone = this.currentUser.currentUser.timezone;
    this.unsubscribe = new Subject();
    this.buildForm();
    this.initDataTable();
    this.parentBread = [
      {
        name: this.translate.instant('REPORT.REPORTS.GENERAL.REPORTS'),
        translate: "REPORT.REPORTS.GENERAL.REPORTS",
        link: "/reports",
        icon: "flaticon2-graph"
      }
    ]
  }

  ngOnInit() {
    this.updateDataTable();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();
  }

  buildDateNow() {
    this.datePicker.refresh();
    this.deviceId = '';
    setTimeout(function () {
      $('.kt_selectpicker').selectpicker('refresh');
    });
  }

  private buildForm(): void {

    this.searchFormRoute = this.formBuilder.group({
      dateStart: [''],
      dateEnd: [''],
      deviceId: ['']
    });
  }

  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
   }


  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
    };
  }

  ChangeUserTree(value) {
    if (value.id > 0) {
      this.userIdSelectedEdit = value.id;
      this.getDevices(value.id);
      // this.deviceId = '';
      this.buildDateNow();
      this.dataTable.update({
        data: [],
        totalRecod: 0
      });
    }
  }
  onChangeDevice(value){
    if(value>0){
      this.deviceTemplate =  this.listDevices.find(x=>x.id==value);
      this.searchRoute(this.searchFormRoute);
      this.calculatorDayStyle();
      this.cdr.detectChanges();
    }
  }
  calculatorDayStyle(){
    let width = 10;
    this.dataTable.getColumns().forEach(x=>{
      width += (x.width + 20);
    });
   
    if(this.deviceTemplate){
      if(this.deviceTemplate.basicTemplate) width += (this.deviceTemplate.basicTemplate.length * 120);
      width += (this.deviceTemplate.sensors.length * 120);
    }
    this.styleDay['width']=(width-30)+'px';
  }
  getNameDevice(text: string) {
    this.deviceName = text;
  }

  getDevices(userId) {
    let params = {
      userId: userId,
      pageNo: -1
    };
    // this.filter.pageNo = -1;
    this.trackingService.list({params: params}).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      this.listDevices = data.result;
      setTimeout(function () {
        $('.kt_selectpicker').selectpicker('refresh');
      });
    });

  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  validateForm(deviceId: string) {
    let timeStart = new Date(this.dateStart).getTime();
    let timeEnd = new Date(this.dateEnd).getTime();
    if (timeStart > timeEnd) {
      this.toast.show(
        {
          message: this.translate.instant('REPORT.ROUTE.MESSAGE.ERROR_TIME'),
          type: 'error',
        })
      return false;
    }
    if (deviceId == '' || deviceId == undefined || deviceId == null) {
      this.toast.show(
        {
          message: this.translate.instant('MESSEAGE_CODE.SENSOR.DEVICE_NOT_FOUND'),
          type: 'error',
        })
      return false;
    }
    return true;
  }

  searchRoute(form: any) {
    
    this.filter = {};
    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;
    this.filter.deviceIds = form.value.deviceId;
    let dataSearch = [];
    Object.keys(this.filter).forEach(k=>{
      dataSearch.push({
        key:k,
        value : this.filter[k]
      })
    })
    if(this.filter.deviceIds>0){
      // this.getData(this.filter);
      this.dataTable.search(dataSearch);
    }
    else{
      // update not found 
      this.toast.show({type:'info', translate:'COMMON.MESSAGE.PLEASE_CHOOSE_DEVICE'});
    }
    
   
  }

  resetFormSearch() {
    this.buildDateNow();
  }

  getData(filter) {
    this.reportsService.dataLogs({params:filter}).pipe(
      tap(result=>{
        this.data = result.result.content;
        this.dataPointsByDay = this.processDataPointsByDay(result.result.content); 
        this.calculatorDayStyle();      
        this.dataTable.update({
          data: this.dataPointsByDay,
          totalRecod: result.result.totalRecord
        });
      }),
      debounceTime(1000),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  processDataPointsByDay(data){
    let dataResult = [];
    let dataObj = {};
    
 
    data.map(x=>{
      x['base'] = x['base'].map(b=>{
        let id =`base_${b['key']}`;
        x[id] = b['value'];
        b['id'] = id;
        return b;
      })
      if(this.deviceTemplate && !this.deviceTemplate.basicTemplate){
        this.deviceTemplate['basicTemplate'] = x['base'];
      }
      x.dateTime = this.userDateAdv.transform(x.time, "YYYY/MM/DD HH:mm:ss");
      x.latLng = `${this.decimalPipe.transform(x.lat, "1.0-6")}, ${this.decimalPipe.transform(x.lng, "1.0-6")}`;
      // process battery 
      if (x['battery']['isAvailable']) {
        // có pin
        if (x['battery']['isCharged']) {
          x['batteryStr'] = this.translate.instant('COMMON.GENERAL.CHANGED') + " - " + x['battery']['voltage'] + "V - " + (x['battery']['percent'] || 0) + "%";
        }
        else {
          x['batteryStr'] = x['battery']['voltage'] + "V - " + (x['battery']['percent'] || 0) + "%";
          x['power'] = this.translate.instant('COMMON.GENERAL.BATTERY');
        }
      }
      else {
        // ko có pin
        x['batteryStr'] = this.translate.instant('COMMON.GENERAL.NO_BATTERY');
      }

      x.sensors.forEach(s => {
        x[`sensor${s.id}`] = s.value
      })
      if(dataObj[this.userDateAdv.transform(x.time, "YYYY/MM/DD")] == undefined){
        dataObj[this.userDateAdv.transform(x.time, "YYYY/MM/DD")] = [];
      }
      dataObj[this.userDateAdv.transform(x.time, "YYYY/MM/DD")].push(x);
      return x;
    });
    // process dataObj
    dataResult = Object.keys(dataObj).map(k=>{
      return {
        timeFrom: k,
        points:dataObj[k],
      }
    });

    return dataResult;
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;
        this.getData(this.filter);
      }),

    ).subscribe();
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  exportFile(options: { type?: string }) {
    if(this.filter == undefined || this.filter.deviceIds< 0) 
    {
      this.toast.show({type:'info', translate:'COMMON.MESSAGE.PLEASE_CHOOSE_DEVICE'});
      return;
    }
    this.filter['pageNo'] = -1;
    this.isdownLoad = true;
    this.cdr.detectChanges();

    this.reportsService.dataLogs({params: this.filter}).pipe(
      tap(result=>{
        if(result.result.length == 0 || (result.result.length>0 && result.result[0].points.length == 0 )){
          this.toast.show({ type: "error", translate: 'COMMON.MESSAGE.NOT_FOUND_DATA', });
        }
        else{
          this.dataPointsByDayExport = this.processDataPointsByDay(result.result[0].points);
          let summary = {
            startTime: this.userDateAdv.transform(this.dateStart, "datetime",{valueOffset:0}),
            endTime: this.userDateAdv.transform(this.dateEnd, "datetime",{valueOffset:0}),
            sensorTemplate: this.deviceTemplate.sensors,
            basicTemplate: this.deviceTemplate.basicTemplate,
            deviceName: this.deviceTemplate.name,

          }
          switch (options.type) {
            case "xlsx":
              // let data = this.processDataByDay(this.currentEvent.data.routes);
              this.exportFileXLSX(this.dataPointsByDayExport, summary);
              break;
          }     
        }
      }),
      debounceTime(1000),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.isdownLoad = false;
        this.cdr.detectChanges();
        this.cdr.markForCheck();
      })
    ).subscribe();
    setTimeout(()=>{
      this.isdownLoad = false;
      this.cdr.detectChanges();
    },10000);
  }

  private exportFileXLSX(data, summary) {
    let sheets = [];
    data.forEach((v,i)=>{
      let day = this.userDateAdv.transform(v.timeFrom,"YYYY/MM/DD");
      let config:XLSXModel;
      if(i==0){
        config = {
          file: {
            title: this.translate.instant('MAP.TRACKING.CHART.DATA_LOG'),
            prefixFileName: `${summary['deviceName']}___${this.userDateAdv.transform(summary.startTime, "YYYYMMDD_HHmmss", { valueOffset: 0 })}__${this.userDateAdv.transform(summary.endTime, "YYYYMMDD_HHmmss", { valueOffset: 0 })}`,
            typeName:"DataHistory",
            objName:summary['deviceName'],
            timeStart:this.userDateAdv.transform(summary.startTime, "YYYYMMDD_HHmmss", { valueOffset: 0 }),
            timeEnd:this.userDateAdv.transform(summary.endTime, "YYYYMMDD_HHmmss", { valueOffset: 0 })
          },
          header: [
            {
              text: this.translate.instant('MAP.TRACKING.CHART.DATA_LOG'),
              type: "header",
              style: {
                fill: {
                  patternType: "solid", // none / solid
                  fgColor: { rgb: "01B050" },
                },
              },
              hpx: 32
            },
            {
              text: "",
            },
            {
              text: "!timezone",
            },
            {
              text: ""
            },
            {
              text: [this.translate.instant('COMMON.EXCEL.START_TIME') + ": ", , summary['startTime']],
            },
            {
              text: [this.translate.instant('COMMON.EXCEL.END_TIME') + ": ", , summary['endTime']],
              // text: [,,],
            },
            {
              text: "",
            },
            {
              text: [this.translate.instant('COMMON.COLUMN.DEVICE_NAME') + ": ", , summary['deviceName']],
              style: {
                font: { bold: true },
              }
            },
            {
              text: ""
            },
          ],
          columns: [
            {
              name: "#",
              columnData: "auto",
              wch: 5,
            }, 
            {
              name: this.translate.instant('COMMON.COLUMN.TIME_DEVICE'),
              columnData: "time",
              wch: 20,
              type:'datetime',
              format:'YYYY/MM/DD HH:mm:ss'
            },
            {
              name: this.translate.instant('COMMON.COLUMN.UPDATE_TIME'),
              columnData: "updatetimeUTC",
              wch: 20,
              type:'datetime',
              format:'YYYY/MM/DD HH:mm:ss'
            },
            {
              name: this.translate.instant('COMMON.COLUMN.LAT_LNG'),
              columnData: "latLng",
              wch: 20,
            },
            {
              name: this.translate.instant('COMMON.COLUMN.ADDRESS'),
              columnData: "address",
              wch: 40,
            },
            {
              name: this.translate.instant('COMMON.COLUMN.GSM'),
              columnData: "gsm",
              wch: 20,
              type: "number"
            },
            {
              name: this.translate.instant('COMMON.COLUMN.GPS'),
              columnData: "gps",
              wch: 20,
              type: "number"
            },
            {
              name: this.translate.instant('COMMON.COLUMN.POWER')+ "(V)",
              columnData: "powerVoltage",
              wch: 20,
              type: "number"
            },
            {
              name: this.translate.instant('COMMON.COLUMN.BATTERY'),
              columnData: "batteryStr",
              wch: 30,
            },
            {
              name: `${this.translate.instant('COMMON.COLUMN.SPEED')}(km/h)`,
              columnData: "speed",
              wch: 20,
              type: "number"
            },
          ],
          subColumn:[
            {
              text: day,
              style: {
                font: { bold: true },
                fill: {
                  patternType: "solid", // none / solid
                  fgColor: { rgb: "989898" },
                },
              },
              merge:{
                full:true
              }
            },
          ],
          styleColumn: {
            fill: {
              patternType: "solid", // none / solid
              fgColor: { rgb: "92D050" },
            },
            font: { bold: true },
            alignment: { vertical: "center", horizontal: "center", wrapText: true },
          },
          showColumn: true,
          woorksheet: {
            name: summary['deviceName'],
          }
        }
  
      }
      else{
        config = {
          file: {
            title: this.translate.instant('MAP.TRACKING.CHART.DATA_LOG'),
            prefixFileName: `${summary['deviceName']}___${this.userDateAdv.transform(summary.startTime, "YYYYMMDD_HHmmss", { valueOffset: 0 })}__${this.userDateAdv.transform(summary.endTime, "YYYYMMDD_HHmmss", { valueOffset: 0 })}`,
            typeName:"DataHistory",
            objName:summary['deviceName'],
            timeStart:this.userDateAdv.transform(summary.startTime, "YYYYMMDD_HHmmss", { valueOffset: 0 }),
            timeEnd:this.userDateAdv.transform(summary.endTime, "YYYYMMDD_HHmmss", { valueOffset: 0 })
          },
          header: [],
          columns: [
            {
              name: "#",
              columnData: "auto",
              wch: 5,
            }, 
            {
              name: this.translate.instant('COMMON.COLUMN.TIME_DEVICE'),
              columnData: "time",
              wch: 20,
              type:'datetime',
              format:'YYYY/MM/DD HH:mm:ss'
            },
            {
              name: this.translate.instant('COMMON.COLUMN.UPDATE_TIME'),
              columnData: "updatetimeUTC",
              wch: 20,
              type:'datetime',
              format:'YYYY/MM/DD HH:mm:ss'
            },
            {
              name: this.translate.instant('COMMON.COLUMN.LAT_LNG'),
              columnData: "latLng",
              wch: 20,
            },
            {
              name: this.translate.instant('COMMON.COLUMN.GSM'),
              columnData: "gsm",
              wch: 20,
              type: "number"
            },
            {
              name: this.translate.instant('COMMON.COLUMN.GPS'),
              columnData: "gps",
              wch: 20,
              type: "number"
            },
            {
              name: this.translate.instant('COMMON.COLUMN.POWER') + "(V)",
              columnData: "powerVoltage",
              wch: 20,
              type: "number"
            },
            {
              name: this.translate.instant('COMMON.COLUMN.BATTERY') ,
              columnData: "batteryStr",
              wch: 30,
            },
            {
              name: `${this.translate.instant('COMMON.COLUMN.SPEED')}(km/h)`,
              columnData: "speed",
              wch: 20,
              type: "number"
            },
            
          ],
          subColumn:[
            {
              text: day,
              style: {
                font: { bold: true },
                fill: {
                  patternType: "solid", // none / solid
                  fgColor: { rgb: "989898" },
                },
              },
              merge:{
                full:true
              }
            },
          ],
          styleColumn: {
            fill: {
              patternType: "solid", // none / solid
              fgColor: { rgb: "92D050" },
            },
            font: { bold: true },
            alignment: { vertical: "center", horizontal: "center", wrapText: true },
          },
          showColumn: false,
          woorksheet: {
            name: summary['deviceName'],
          }
        }
      }
       // add basic  
       summary.basicTemplate.forEach(s => {
        config.columns.push({
          name: `${s.name}`,
          columnData: `${s.id}`,
          wch: 20,
          type: "number"
        })
      });
     
      // add sensor 
      summary.sensorTemplate.forEach(s => {
        config.columns.push({
          name: `${s.name}(${s.unit || "-"})`,
          columnData: `sensor${s.id}`,
          wch: 20,
          type: "number"
        })
      });
      sheets.push(
        { data: v.points, config: config, option: {} }
      )
    })
    this.xlsx.exportFileAdvanced({ sheets: sheets, mergeSheet: true });
  }
  private initDataTable() {
    let tableConfig = {
      pagination: [
        10, 20, 30, 50
      ],
      columns: [
        {
          title: '#',
          field: 'id',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '40px' },
          width: 40,
          class: '',
          translate: '#',
          autoHide: false,
        },
        {
          title: 'time device',
          field: 'timeDevice',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '135px' },
          width: 135,
          class: '',
          translate: 'COMMON.COLUMN.TIME_DEVICE',
          autoHide: false,
        },
        {
          title: 'update time ',
          field: 'updateTime',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '135px' },
          width: 135,
          class: '',
          translate: 'COMMON.COLUMN.UPDATE_TIME',
          autoHide: false,
        },

        {
          title: 'lat lng',
          field: 'latLng',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '160px' },
          width: 160,
          class: '',
          translate: 'COMMON.COLUMN.LAT_LNG',
          autoHide: false,
        },
        {
          title: 'lat lng',
          field: 'address',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '160px' },
          width: 160,
          class: '',
          translate: 'COMMON.COLUMN.ADDRESS',
          autoHide: false,
        },


        {
          title: 'gsm',
          field: 'gsm',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px' },
          width: 100,
          class: '',
          translate: 'COMMON.COLUMN.GSM',
          autoHide: false,
        },
        {
          title: 'gps',
          field: 'gps',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px' },
          width: 100,
          class: '',
          translate: 'COMMON.COLUMN.GPS',
          autoHide: false,
        },
        {
          title: 'power vol',
          field: 'powerVoltage',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px' },
          width: 100,
          class: '',
          translate: 'COMMON.COLUMN.POWER',
          autoHide: false,
        },
        {
          title: 'battery',
          field: 'battery',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '150px' },
          width: 150,
          class: '',
          translate: 'COMMON.COLUMN.BATTERY',
          autoHide: false,
        },
        {
          title: 'speed',
          field: 'speed',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px' },
          width: 100,
          class: '',
          translate: 'COMMON.COLUMN.SPEED',
          autoHide: true,
        },

      ]
    };
    this.dataTable = new DataTable();
    this.dataTable.init({
      data: [],
      totalRecod: 0,
      paginationSelect: [10, 20, 30, 50],
      columns: tableConfig.columns,
      formSearch: '#formSearch',
      pageSize: 10,
      // isDebug: true,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,

        },
        responsive: false,
        // selecter: true,
      },
      showFull: true
    });
  }

}




