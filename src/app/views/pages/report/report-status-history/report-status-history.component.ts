import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef,EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { ReportStatusHistoryService,ReportRouteService } from '@core/report';
import { DeviceService } from '@core/manage';
import { TranslateService } from '@ngx-translate/core';
import { ToastService, UserDatePipe,UserDateAdvPipe } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { ExcelService,PdfmakeService,ExcelTestService} from '@core/utils';
import { XLSXModel, XLSXModelTest } from '@core/utils/xlsx/excel.service';
import { PdfModel } from '@core/utils/pdfmake/pdfmake.service';

import { DatePipe } from '@angular/common';
import { UserDateService } from '@core/_base/layout';
import { DecimalPipe } from '@angular/common';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { waitDownloadModel } from '../wait-download/wait.download.service'
import { ExportService } from '@core/utils';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { isNull } from 'util';

// end chart
declare var $: any;

@Component({
  selector: 'report-status-history',
  templateUrl: './report-status-history.component.html',
  styleUrls: ['./report-status-history.component.scss'],
  providers: [DatePipe, UserDatePipe, DecimalPipe,UserDateAdvPipe]

})
export class ReportStatusHistory implements OnInit {
  @ViewChild('datePicker', { static: true }) 

  datePicker: DateRangePickerComponent;
  @ViewChild('datePickerSearchChart', { static: true }) 

  datePickerSearchChart: DateRangePickerComponent;
  searchFormChart:FormGroup;
  searchFormSummary: FormGroup;
  selectdp: ElementRef;
  public datePickerOptions: any;
  public datePickerOptionsSearchChart:any;

  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  // show full data
  public showFullData:boolean = true;
  public dataPagination : any = [];
  public dataExport: any = null;
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataDefault: any = [{}];
  public filter: any;
  private unsubscribe: Subject<any>;
  public listDevices: any = [];
  public listSensors: any = [];
  public userIdSelected: number;
  public userIdSelectedEdit: number;
  public showUserTree: boolean = false;
  public dateStart: string;
  public dateEnd: string;
  public timeStart: string;
  public timeEnd: string;
  //time search chart
  public dateStartChart:string;
  public dateEndChart:string;
  public sensorIdSearhChart:any;
  public paramsEmiter: EventEmitter<waitDownloadModel>;
  public deviceId: string;
  public status: string = 'route';

  public deviceIdSearhChart:any;
  public deviceName: string;
  public dateFormat: string;
  public timeFormat: string;
  public timeZone:string;
  public totalAllRowData : any;
  public parentBread:any;
  public isdownLoad : boolean = false;
  public isdownLoadPDF:boolean = false;
  public closeResult: string;
  public checkbetweenChange:boolean = false;
  public listStatus:any;
  public listStatusObj:any;

  // fuel
  public showFuel:boolean = true;

  // export
  public configExport:PdfModel;
 // configChart
  constructor(
    private Route: ReportRouteService,
    private userTreeService: UserTreeService,
    private statusHistory: ReportStatusHistoryService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private deviceService: DeviceService,
    private translate: TranslateService,
    private toast: ToastService,
    private currentUser: CurrentUserService,
    private xlsx: ExcelService,
    public datepipe: DatePipe,
    private userDate: UserDateService,
    private userDatePipe: UserDatePipe,
    private pdf : PdfmakeService,
    private userDateAdv : UserDateAdvPipe,
    private exportService:ExportService

  ) {
    this.getListStatus();

    this.paramsEmiter = new EventEmitter();
    
    this.dateFormat = this.currentUser.dateFormatPipe;
    this.timeFormat = this.currentUser.timeFormat;
    this.timeZone = this.currentUser.currentUser.timezone;
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.parentBread = [
      {
        name: this.translate.instant('REPORT.REPORTS.GENERAL.REPORTS'),
        translate: "REPORT.REPORTS.GENERAL.REPORTS",
        link: "/reports",
        icon: "flaticon2-graph"
      }
    ]
  }

  ngOnInit() {
    this.updateDataTable();
    this.refreshEventType();
    this.datePickerOptions = this.objOption();
    this.userTreeService.event.pipe(
        tap(action => {
          switch (action) {
            case "close":
              this.showUserTree = false;
              break;
            case "open":
              this.showUserTree = true;
              break;
          }
        })
      ).subscribe();
  }

  getListStatus(){
      this.listStatus = [
            {
          
            status:0,
            name:this.translate.instant('REPORT.STATUS_HISTORY.STATUS.STOP')
            },
           {
            status:1,
            name:this.translate.instant('REPORT.STATUS_HISTORY.STATUS.RUN')
           },
           {
          
            status:2,
            name:this.translate.instant('REPORT.STATUS_HISTORY.STATUS.ENGINE_ON')
           },
           {
            status:3,
            name:this.translate.instant('REPORT.STATUS_HISTORY.STATUS.ENGINE_OFF')
           },
           {
            status:4,
            name:this.translate.instant('REPORT.STATUS_HISTORY.STATUS.AIR_ON')

           },
           {
            status:5,
            name:this.translate.instant('REPORT.STATUS_HISTORY.STATUS.AIR_OFF')

           },
    ];
    this.listStatusObj = [
      {
        status:'route',
        name:this.translate.instant('REPORT.STATUS_HISTORY.OBJ.ROUTE'),
        id:[0,1],
      },
      {
        status:'engine',
        name:this.translate.instant('REPORT.STATUS_HISTORY.OBJ.ENGINE'),
        id:[2,3],
      },
      {
        status:'air',
        name:this.translate.instant('REPORT.STATUS_HISTORY.OBJ.AIR'),
        id:[4,5],
      },
    ]
    this.cdr.markForCheck();
    // setTimeout( () => {
    //     $('#listStatus').val('route').selectpicker('refresh');
    //   });
  }

  objOption()
  {
    let obj = {
      size: 'md',
      select: 'from',
      ranges: 'from',
      optionDatePicker: {
        maxSpan: {
          days: 31
        }

      },
      autoSelect: true,
      singleDatePicker: false,
      timePicker: true,
      startDate:moment().subtract(1, "days").startOf('day'),
      endDate:moment().subtract(1, "days").endOf('day'),
    }; 
    return obj;
  }

  renderConfig()
  {
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    this.configExport  = {
      info: {
        title: 'StatusHistory',
      },
      content:{
        header:{
          title:this.translate.instant('MENU.REPORT_STATUS_HISTORY'),
          params:{
            deviceName:": "+this.deviceName,
            timeStart:": "+this.dateStart,
            timeEnd:": "+this.dateEnd,
            timeZone:": "+this.timeZone,
          }
        },
        table:[
          {
              woorksheet: {
                name: this.deviceName,
              },
              total : {
                group : [
                  { columnData:'dateTimeFrom'},
                ],
                totalCol:[
                    { text:this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL'),style:{colSpan:6,alignment:"right",bold:true,fillColor:"#fff"}},
                    { text:''}, { text:''}, { text:''}, { text:''}, { text:''},
                    { columnData:'totalTime',style:{alignment:"right",bold:true,fillColor:"#fff"}},      
                    { columnData:'totalDistance',style:{alignment:"right",bold:true,fillColor:"#fff"}},
                   
      
                ]
              },
              headerRows: 2,
              widths: ['4%','12%','18%','12%','18%','12%','12%','12%'],
              header :[           
                  [ 
                    
                    { text: '#',rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_START'),colSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text:''},
                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_END'),colSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text:''},
                    { text: this.translate.instant('DASHBOARD.STATUS'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                    { text: this.translate.instant('REPORT.STATUS_HISTORY.COLUMN.DURATION'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                    { text: this.translate.instant('DASHBOARD.STATIC.DISTANCE_LABEL'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                  ],
                  [
                    '',
                    { text: this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('COMMON.COLUMN.ADDRESS'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('COMMON.COLUMN.ADDRESS'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    '',
                    '',
                    '',
         
                  ]
              ],
              body:[
                {columnData:'count',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'startTime',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'startAddress',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'stopTime',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'stopAddress',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'statusType',style:{alignment:"right",fillColor:"#fff",bold:false}},
                {columnData:'duration',style:{alignment:"right",fillColor:"#fff",bold:false}},
                {columnData:'distance',style:{alignment:"right",fillColor:"#fff",bold:false}},

       
              ],              
          },
          
        ]
       },
      footer:{
        title: this.datepipe.transform(this.dateStart, dateTimeFormat)+ ' - ' + this.datepipe.transform(this.dateEnd, dateTimeFormat)+', '+this.deviceName
      }    
    };
  }

  buildDateNow() {
    this.datePicker.refresh();
    this.deviceId = '';
    this.status = 'route';
    this.dataTable.update({
        data: [],
      });
    setTimeout( () => {
      $('#listStatus').val('route').selectpicker('refresh');
      $('.kt_selectpicker').selectpicker('refresh');
    });
    
  }

  private buildForm(): void {
    this.searchFormSummary = this.formBuilder.group({
      dateStart: [''],
      dateEnd: [''],
      deviceId: [''],
      status: ['route']
    });

    
  }

  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
    this.renderConfig();
   }


  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
    };
  }

  ChangeUserTree(value) {
    if (value.id > 0) {
      this.userIdSelectedEdit = value.id;
      this.getDevices(value.id);
      this.buildDateNow();
      this.dataTable.update({
        data: [],
        totalRecod: 0
      });
    }
  }

  getNameDevice(device) {
    let deviceId = device.target.value;
    let obj = this.listDevices.find(x => x.id == deviceId);
    this.deviceName = obj.name;    
  }

  getNameStatus(status){
    let listStatus = '';
    if(this.searchFormSummary.value.status == '')
    {
       
        this.listStatus.map(status=>{
            listStatus = listStatus+','+status.name;
            return status;
        })
    }else{
        let listStatusId = this.searchFormSummary.value.status;
    }
  }

  getDevices(userId) {
    let params = {
      userId: userId, 
      pageNo: -1
    };
    // this.filter.pageNo = -1;
    this.deviceService.list({ params: params }).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      this.listDevices = data.result;
      setTimeout( () => {
        $('#listDevice').selectpicker('refresh');
      });
    });

  }

  checkSensorFuel(nameSensor)
  {
    let check = false;
    this.listDevices.find(device =>{
     let sensor =  device.sensors.find(o => o.name == nameSensor);
     if(sensor != undefined)
     {
      if(JSON.parse(sensor.parameters).typeSensor == 'fuel') check = true;
     }
      
    });
    
    return check;
  }
  

  private refreshEventType()
  {
   this.translate.onLangChange.subscribe((event) => {
     let titleDevice = this.translate.instant('REPORT.ROUTE.GENERAL.CHOISE_DEVICE');
     $(function () {
       $('.input-device').selectpicker({title: titleDevice}).selectpicker('refresh');
     });
   });
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  validateForm(deviceId: any) {
    if (deviceId == '' || deviceId == undefined || deviceId == null) {
      this.toast.show(
        {
          message: this.translate.instant('MESSEAGE_CODE.SENSOR.DEVICE_NOT_FOUND'),
          type: 'error',
        })
      return false;
    }
    return true;
  }


  searchSummary(form: any) {
    if (!this.validateForm(form.value.deviceId)) return;
    this.getListFilter();
    this.showFullData = true;
    this.dataTable.reload({ currentPage: 1 });
    // this.dataTable.reload({ currentPage: 1 });

  }

  checkbetween(value)
  {
    this.checkbetweenChange = value;
  }


  resetFormSearch() {
    this.buildDateNow();
  }

  getGeocode(item) {
    // get address if isEmpty
    if (!item.startAddress || item.startAddress.length == 0) {
      if (isNull(item.startLat) || isNull(item.startLng)) return;
      let address = (item.startLat || 0).toFixed(5) + ", " + (item.startLng || 0).toFixed(5);
      item.addressLoading = true;
     
      this.Route.geocode({ params: { lat: item.startLat, lng: item.startLng } }).pipe(
        tap(
          data => {
            address = data.result.address;
            this.cdr.detectChanges();
          }
        ),
        finalize(() => {
          item.addressLoading = false;
          item.startAddress = address;
          this.cdr.detectChanges();
          this.cdr.markForCheck();
        })
      ).subscribe();
    }

    if (!item.stopAddress || item.stopAddress.length == 0) {
      if (isNull(item.stopLat) || isNull(item.stopLng)) return;
      let address = (item.stopLat || 0).toFixed(5) + ", " + (item.stopLng || 0).toFixed(5);
      item.addressLoading = true;
      this.cdr.detectChanges();
      this.Route.geocode({ params: { lat: item.stopLat, lng: item.stopLng } }).pipe(
        tap(
          data => {
            address = data.result.address;
          }
        ),
        finalize(() => {
          item.stopAddress = false;
          item.stopAddress = address;
          this.cdr.detectChanges();
          this.cdr.markForCheck();
        })
      ).subscribe();
    }
  }

  async getData(filter,type?:string) {
    if(type == 'pdf') this.isdownLoadPDF =true;
    if(type == 'excel') this.isdownLoad = true;

    let _this = this;
    // filter.timeFrom = '2020-06-11 00:00:00';
    // filter.timeTo = '2020-06-12 23:59:00';
    // filter.deviceId = 385;
  await this.statusHistory.list(filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          if(type == 'pdf') _this.isdownLoadPDF = false;
          if(type == 'excel') _this.isdownLoad = false;
          let pageNo = this.filter.pageNo - 1;
          let pageSize = this.filter.pageSize;
          let j = 0;

          this.totalRecod = result.result.length;
          result.result = result.result.map(item=>{
            // this.getGeocode(item);
            let totalTime = [];
            let totalDistance = 0;
            let resultConcat = [];
            if(item.routes != null)  resultConcat =  resultConcat.concat(item.routes);
            if(item.engines != null)  resultConcat =  resultConcat.concat(item.engines);
            if(item.airs != null) resultConcat =  resultConcat.concat(item.airs);
           
             resultConcat = resultConcat.map(itemResult=>{
                 j++;
                 let count = pageNo * pageSize + j;
                //  totalTime = totalTime + itemResult.duration;
                 if(totalTime.find(x=>x.statusType == itemResult.statusType))
                 {
                     let existObj = totalTime.find(x=>x.statusType == itemResult.statusType);
                     existObj.duration = existObj.duration +  itemResult.duration;
                 }else{
                  totalTime.push({
                         statusType:itemResult.statusType,
                         duration: itemResult.duration
                     });
                 }
                 this.getGeocode(itemResult);
                 totalDistance = totalDistance + itemResult.distance;
                 itemResult.duration = this.convertNumberToTime(itemResult.duration,true);
                 itemResult.startTime = this.userDateAdv.transform(itemResult.startTime,'HH:mm:ss');
                 itemResult.stopTime = this.userDateAdv.transform(itemResult.stopTime,'HH:mm:ss');
                 itemResult.distance =  this.roundNumber(itemResult.distance);
                 itemResult.statusType =  this.translate.instant('REPORT.STATUS_HISTORY.STATUS.'+itemResult.statusType);
                 itemResult.count = count;
                 return itemResult;
             });
            //  item.totalTime = this.convertNumberToTime(totalTime);
            //  item.totalTime = this.convertNumberToTime(totalTime);
             item.dateTimeFrom = this.userDatePipe.transform(item.timeFrom, "date", null, "datetime");
             item.data = resultConcat;
             item.totalDistance = this.roundNumber(totalDistance);

             let texttotalTime = '';
             totalTime.map((total,i)=>{
               let itemText = this.translate.instant('REPORT.STATUS_HISTORY.STATUS.'+total.statusType) +' '+this.convertNumberToTime(total.duration,true);
               if(i == totalTime.length - 1) texttotalTime = texttotalTime + itemText;
               if(i != totalTime.length - 1) texttotalTime = texttotalTime + itemText+ ', ';
             });
             item.totalTime = texttotalTime;
             return item;
          
          });
  
          this.dataExport = result.result;
          this.dataPagination = result.result;
          this.getDataShowPage();
          if(!type)
          {
          
            this.dataTable.update({
              data: this.data,
              totalRecod: j-1
            });
          }
          if(type == 'pdf')     this.exportFilePDF(this.data);
          if(type == 'excel')    this.exportFileXLSX(this.data);
          this.showFullData = false;
          // setTimeout( () => {
          //   $('.kt_selectpicker').selectpicker('refresh');
          // });

        }
      })
  }
  

  getDataShowPage()
  {
    this.data = [];
    for(let i=0;i<this.dataPagination.length;i++)
    {
        
        this.data.push({
          dateTimeFrom:this.dataPagination[i].dateTimeFrom,
          data:[]
        }); 
        let data = this.dataPagination[i].data;
        for(let j=0;j<data.length;j++)
        {
          let count = this.dataPagination[i].data[j].count;
          if(count > (this.filter.pageNo-1)*this.filter.pageSize && count <= (this.filter.pageNo)*this.filter.pageSize)
          {
            this.data[i].data.push(this.dataPagination[i].data[j]);
          }
        }
    
    }
    this.data = this.data.filter(item=>{
      let data = item.data;
      if(data.length == 0) return false;
      return true;
    })

  }


  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;
        if(this.showFullData)
        {
          this.getData(this.filter);
        }else{
          this.dataTable.isLoading = true;
          setTimeout(() => {
            this.getDataShowPage();
            this.dataTable.update({
              data: this.data
            });
            this.cdr.markForCheck();
            this.cdr.detectChanges();

            this.dataTable.isLoading = false;

          },300);
       
        }
      }),

    ).subscribe();
  }

  convertNumberToTime(number,text) {
    return this.userDate.convertNumberToTime(number,text);
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
        responsive: false,

      }
    });
  }

  dateSelectChangeSearchChart(data)
  {
    this.dateStartChart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEndChart = data.endDate.format("YYYY-MM-DD HH:mm:ss");
  }

  renderDatePickerChart(dateStartChart,endDate)
  {
    this.datePickerOptionsSearchChart = {
      size: 'md',
      select: 'from',
      ranges: 'from',
      optionDatePicker: {
        maxSpan: {
          days: 31
        }

      },
      autoSelect: true,
      singleDatePicker: false,
      timePicker: true,
      startDate:dateStartChart,
      endDate:endDate
    }
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  roundNumber(number) {
    return Math.round(number * 100) / 100;
  }

  async getdataExcel(file) {
    if(file =='pdf')
    {this.isdownLoadPDF = true;}else
    {this.isdownLoad = true; } 
    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;
    this.filter.deviceId = this.searchFormSummary.value.deviceId; 
    this.getData(this.filter,file);

  }

  //pdf
  async exportPDF(){
    let validate = this.validateForm(this.searchFormSummary.value.deviceId);
    if (validate )
     {
      this.filter.format = 'pdf';
      // this.filter.deviceId = this.
      this.getListFilter();
      if(!this.checkbetweenChange) this.getdataExcel('pdf');
     }
  }
  
  getListFilter(){
    this.filter.deviceId = this.searchFormSummary.value.deviceId;
    // this.filter.format = 'excel';
    // this.filter.report = "status_history";
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;   
    let listStatus:any = []; 
    if(this.searchFormSummary.value.status == '')
    {
       
        this.listStatus.map(status=>{
            listStatus.push(status.status);
            return status;
        })
    }else{
        let status = this.searchFormSummary.value.status;
        listStatus = this.listStatusObj.find(x=>x.status == status).id

    }
    this.filter.status = listStatus.toString();
    this.paramsEmiter.emit(this.filter);

  }

  exportFilePDF(data){
    this.renderConfig();
    this.pdf.ExportPdf([this.dataExport],this.configExport);
  }
  
  //export
  exportXlSX() {
    let validate = this.validateForm(this.searchFormSummary.value.deviceId);
    if (validate )
     {
      this.filter.format = 'excel';
      this.getListFilter();
      if(!this.checkbetweenChange) this.getdataExcel('excel');

     }
  }

  convertTextData(text)
  {    
    return this.exportService.convertTextData(text);
  }

  private exportFileXLSX(data) {

    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let config: XLSXModelTest = {
      file: {
        title: this.translate.instant('MENU.REPORT_SUMMARY_DAY'),
        prefixFileName: "StatusHistory",
        devices:this.deviceName,
        timeStart:this.dateStart,
        timeEnd:this.dateEnd,
      },
      template: [{
        header: [
          {
            text: this.translate.instant('MENU.REPORT_STATUS_HISTORY'),
            type: "header",
          },
          {
            text: ""
          },
          {
            text: '!timezone',
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START') + ": " + this.dateStart,
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_END') + ": " + this.dateEnd,
          },
          {
            text: this.translate.instant('MANAGE.DEVICE.GENERAL.DEVICE_NAME') + ": " + this.deviceName,
          },
          {
            text: ""
          },
        ],
        columns: [
          {
            name: "#",
            columnData: "count",
            wch: 5,
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            merge:{
              rowSpan:1
            }
          },
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_START'),
            columnData: "startTime",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 15,
            merge:{
              colSpan:1
            },
          },

          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_START'),
            columnData: "startAddress",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 40,
      
          },
     
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_END'),
            columnData: "startTime",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "stopTime",
            wch: 15,
            merge:{
              colSpan:1
            },
          },
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_START'),
            columnData: "stopAddress",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 40,
      
          },
          {
            name: this.translate.instant('DASHBOARD.STATUS'),
            columnData: "statusType",
            style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
            merge:{
              rowSpan:1
            },
          },
          {
            name: this.translate.instant('REPORT.STATUS_HISTORY.COLUMN.DURATION'),
            columnData: "duration",
            style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
            merge:{
              rowSpan:1
            },
          },
          {
            name:this.translate.instant('DASHBOARD.STATIC.DISTANCE_LABEL'),
            columnData: "distance",
            style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },
            type: "mergeTime",
            merge:{
              rowSpan:1
            },
            wch:20,
          },

      
        ],
        columnsMerge:
        {
          data: [
            "",
            this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),
            this.translate.instant('COMMON.COLUMN.ADDRESS'),
            this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),
            this.translate.instant('COMMON.COLUMN.ADDRESS'),
            "",
            "",
            "",
          ],
          // position: ["A8:A9","B8:B9","C8:E8","F8:H8","I8:J8"]
        },
        woorksheet: {
          name: this.deviceName,
        },
        total: {
            group: "dateTimeFrom",
            totalCol:{
              field: [
                  {
                    text: this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL'),
                    style: {
                      font: { name: 'Times New Roman', sz: 14, bold: true },
                      alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                    },
                    merge: {
                      full: false,
                      range: "5"
                    }
                  },
                  {text: " ",}, {text: " ",}, {text: " ",}, {text: " ",},{text: " ",},
            
                  {
                    columnData: "totalTime",
                    style: {
                      font: { name: 'Times New Roman', sz: 14, bold: true },
                      alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                    }
                  },
                  {
                    columnData: "totalDistance",
                    style: {
                      font: { name: 'Times New Roman', sz: 14, bold: true },
                      alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                    }
                  },
           
                ]
          },
        }
      }]

    };

    this.xlsx.exportFileTest([this.dataExport], config, {});
  }
  setColumns() {
    this.columns = [
        {
            title: 'Time',
            field: 'moveStart',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '150px','text-align':'center' },
            class: '',
            translate: 'MANAGE.ALERT_RULE.NOTIFICATION.TIME',
            autoHide: false,
            width: 200,
          },
          {
            title: 'address',
            field: 'fuel_start',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '300px' },
            class: '',
            translate: 'COMMON.COLUMN.ADDRESS',
            autoHide: false,
            width: 200,
          },
          {
            title: 'Time',
            field: 'moveStart',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '150px','text-align':'center' },
            class: '',
            translate: 'MANAGE.ALERT_RULE.NOTIFICATION.TIME',
            autoHide: false,
            width: 200,
          },
          {
            title: 'address',
            field: 'fuel_start',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '300px' },
            class: '',
            translate: 'COMMON.COLUMN.ADDRESS',
            autoHide: false,
            width: 200,
          },
    ]
  }


}




