import { Component, OnInit,ChangeDetectorRef,ViewChild } from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup } from "@angular/forms";
import { ToastService, UserDateService  } from '@core/_base/layout';
import { ReportsService,Reports,FavoriteReport } from '@core/report';
import { TranslateService } from '@ngx-translate/core';
import { finalize, tap } from 'rxjs/operators';
import { CurrentUserService } from '@core/auth/_services';
import { Subject } from 'rxjs';
import { AppState } from '@core/reducers';
import { UserSetting,PageService } from '@core/_base/layout';
import { currentUser } from '@core/auth/_selectors/auth.selectors';
import Sortable from 'sortablejs';
import { AsciiPipe } from '@core/_base/layout';

// Store
import { Store, select } from '@ngrx/store';
import { GeofenceService } from '@core/manage';
declare var $: any;

@Component({
  selector: 'kt-report-list',
  templateUrl: './report-list.component.html',
  styleUrls: ['./report-list.component.scss'],
  providers: [AsciiPipe]

})
export class ReportListComponent implements OnInit {
  @ViewChild('items', { static: true }) items: any;

  addReportForm: FormGroup;
  public searchText: string = "";

  public listReportsNoFavorite:any = [];
  public listReportFavorite:any = [];
  public closeResult: string;
  public reportSelect:any;
  private keyDelete : string;
  private unsubscribe: Subject<any>;
  private listReports :any = [];
  private nameFavorite:string;
  public listAllReport:any = [];
  public sortablejsOptions;
  private user:any 
  public toggetherReport:boolean = false;
  public listReportFuel:any = [
    {
        id:190,
        title: 'Fuel chart',
        page: '/fuel/chart',
        icon:'icon-chart-bar1',
        permission: 'ROLE_fuel.chart',
        translate: 'REPORT.DATA_LOG.GENERAL.FUEL_GRAPH',
        type:['fuel']

      },
      {
        id:191,
        title: 'report fuel',
        page: '/fuel/changes',
        icon:'icon-gas-pump',
        permission: 'ROLE_report.fuel',
        translate: 'MENU.REPORT_FUEL',
        type:['fuel']
    },

    {
        id:192,
        title: 'fuel summary',
        page: '/fuel/summary',
        icon: 'icon-eco-fuel',
        permission: 'ROLE_fuel.summary',
        translate: 'MAP.TRACKING.ROUTE.SUMMARY',
        type:['fuel']
    },

  ];
  public listReportQcvn:any = [];
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private toast: ToastService,
    private reportService:ReportsService,
    private translate:TranslateService,
    private cdr: ChangeDetectorRef,
    private store: Store<AppState>,
    private userSetting : UserSetting,
    private currentUserService : CurrentUserService,
    private page:PageService,
    private asciiPipe:AsciiPipe

  ){

    this.sortablejsOptions = {
      animation: 270,
      handle: '.drag_div',
      onUpdate: (event: any) => {
        this.updateListFavorite();
      }
    };

    let user:any = this.store.pipe(select(currentUser));
    if (user) {
      select(user.pipe(
        tap((data:any) => {
          if (data) {
            this.nameFavorite = "favorite"+data.id;           
          }
        }),
      ).subscribe().unsubscribe());
    }

    this.listReports = this.page.getListPageReports();
    
    this.listReportQcvn = this.page.getListPageQCVN();
    this.listAllReport = this.listAllReport.concat(this.listReports,this.listReportQcvn,this.listReportFuel);

  }

  ngOnInit() {
    // var sortable = Sortable.create(this.items);

    this.userSetting.nameFavorite = this.nameFavorite;
    this.buildForm();
    this.loadLocalStorageReport();
    this.renderIconFavorite();
    if(this.listReportFavorite.length == 0)
    {
      this.toggetherReport = true;
    }

  }

  searchItem(val)
  {
      this.searchText = val;
      this.toggetherReport = true;
      $('.collapse').collapse();
  }

  clearFilter()
  {
      this.searchText = "";
  }

  private buildForm(): void {
    this.addReportForm = this.formBuilder.group({
      itemReport: [''],
    });
  }

  renderIconFavorite()
  {

    this.listAllReport.map(item=>{
        item.favorite = false;
        if(this.listReportFavorite.find(x => x.id == item.id)) item.favorite = true;
        let translate = this.translate.instant(item.translate);
        item.textSearch =  this.asciiPipe.transform(translate || "");
        return item;
    });

  }

  loadLocalStorageReport()
  {
   
    
    if(this.userSetting.checkLocalStorageFavorite())
    {
      let favoriteReports = this.userSetting.getLocalStorageFavorite();      
      favoriteReports.forEach(v => {
        if(this.renderReportFavorite(v)) this.listReportFavorite.push(this.renderReportFavorite(v));
      });

     if(this.listReportFavorite) this.listReportFavorite =  this.sortOrder(this.listReportFavorite);
    }
    else
    {
      this.getListReportsFavorite();
    }

  }

  getListReportsNoUse(){
    this.reportSelect = [];
  }

  getListReportsFavorite()
  {
    let params = {
      id : this.currentUserService.Id,
    } as Reports;
    this.reportService.listReport(params).pipe(
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      if(data.result == null)
      {
        this.listReportFavorite = this.page.getListPageReports();
      }
      else
      {
        let favoriteReports = data.result.favoriteReports;
        this.userSetting.setLocalStorageFavorite(favoriteReports);
        favoriteReports.forEach(v => {
         if(this.renderReportFavorite(v)) this.listReportFavorite.push(this.renderReportFavorite(v));
        });
        this.listReportFavorite =  this.sortOrder(this.listReportFavorite);
      }
    });
  }

  renderReportFavorite(objIn){

   let key = objIn.key;
   let objOut =  this.listAllReport.find(x => x.translate == key);
   if(objIn.sortOrder)    objOut.sortOrder = objIn.sortOrder;
   return objOut;
  }


  sortOrder(arr)
  {
    return arr.sort((a,b)=> parseInt(a.sortOrder) - parseInt(b.sortOrder));
  }


  deleteReport(){
    let key = this.keyDelete;
    this.listReportFavorite = this.listReportFavorite.filter(function( obj ) {
      return obj.translate !== key;
  });
    this.updateListFavorite();
    let objReport = this.listAllReport.find(x => x.translate === key);
    objReport.favorite = !objReport.favorite;
    this.modalService.dismissAll();
  }

  addReport(item)
  {
    let id = item.id;
    let obj = this.listAllReport.find(x => x.id == id);
    obj.favorite = !obj.favorite; 
    let reportKey = item.translate;
    if(!item.favorite)
    {
        this.listReportFavorite = this.listReportFavorite.filter(function( objFavorite ) {
            return objFavorite.translate !== reportKey;
        }); 
        this.toast.show(
            {
              message: this.translate.instant('REPORT.REPORTS.MESSAGE.MESSEAGE_REMOVE_SUCSSEC'),
              type: 'success',
            })
          return false; 
    }else
    {

        let reportAdd = this.renderReportFavorite({key : reportKey});
        if(reportAdd) this.listReportFavorite.push(reportAdd);
        this.toast.show(
            {
              message: this.translate.instant('REPORT.REPORTS.MESSAGE.MESSEAGE_ADD_SUCSSEC'),
              type: 'success',
            })
    }

    this.updateListFavorite();

  }

  openModalDelete(key,idModal) {
    this.keyDelete = key;
    this.modalService.open(idModal, { windowClass: 'kt-mt-50  modal-holder modal-delete'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  updateListFavorite()
  {
    let arrFavorite:any = [];
    this.listReportFavorite.forEach((v, i) => {
      let item = {
        key : v.translate,
        sortOrder : i,
      } as FavoriteReport;
      arrFavorite.push(item);
    });
    let params = {
      favoriteReport : arrFavorite,
      userId :  this.currentUserService.Id
    } as Reports;
    this.reportService.updateReport(params).pipe(
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      if(data.status == 200)
      {
        this.userSetting.setLocalStorageFavorite(arrFavorite);
        this.modalService.dismissAll();
      }
    });
  }

  // drop(event: CdkDragDrop<string[]>) 
  // {
  //   moveItemInArray(this.listReportFavorite, event.previousIndex, event.currentIndex);
  //   this.updateListFavorite();
  //   //code change position api
  // }



}
