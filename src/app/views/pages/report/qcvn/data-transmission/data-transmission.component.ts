import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef,EventEmitter, } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { finalize, takeUntil, tap, debounceTime,delay } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { QCVNDataTransmissionService } from '@core/report';
import { DeviceService } from '@core/manage';
import { TranslateService } from '@ngx-translate/core';
import { ToastService, UserDatePipe,UserDateAdvPipe,FunctionService } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { ExcelService,PdfmakeService,ExcelTestService} from '@core/utils';
import { XLSXModel, XLSXModelTest } from '@core/utils/xlsx/excel.service';
import { PdfModel } from '@core/utils/pdfmake/pdfmake.service';

import { DatePipe } from '@angular/common';
import { UserDateService } from '@core/_base/layout';
import { DecimalPipe } from '@angular/common';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { ExportService } from '@core/utils';
import { UserTreeService } from '@core/common/_service/user-tree.service';

// end chart
declare var $: any;

@Component({
  selector: 'qcvn-data-transmission',
  templateUrl: './data-transmission.component.html',
  styleUrls: ['./data-transmission.component.scss'],
  providers: [DatePipe, UserDatePipe, DecimalPipe,UserDateAdvPipe]

})
export class DataTransmissionComponent implements OnInit {
  @ViewChild('listDevicePicker', { static: true }) 
  listDevicePicker:any;

  @ViewChild('datePicker', { static: true }) 
  datePicker: DateRangePickerComponent;
  @ViewChild('datePickerSearchChart', { static: true }) 

  datePickerSearchChart: DateRangePickerComponent;
  searchFormChart:FormGroup;
  searchFormQCVN: FormGroup;
  selectdp: ElementRef;
  public datePickerOptions: any;
  public datePickerOptionsSearchChart:any;

  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  // show full data
  public showFullData:boolean = true;
  public dataPagination : any = [];
  public dataExport: any = null;
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataDefault: any = [{}];
  public filter: any;
  private unsubscribe: Subject<any>;
  public listDevices: any = [];
  public listSensors: any = [];
  public userIdSelected: number;
  public userIdSelectedEdit: number;
  public showUserTree: boolean = false;
  public dateStart: string;
  public dateEnd: string;
  public timeStart: string;
  public timeEnd: string;
  //time search chart
  public dateStartExport:string;
  public dateEndExport:string;
  public sensorIdSearhChart:any;
  public deviceId: string = '';
  public status: string;

  public deviceIdSearhChart:any;
  public deviceName: string;
  public dateFormat: string;
  public timeFormat: string;
  public timeZone:string;
  public totalAllRowData : any;
  public parentBread:any;
  public isdownLoad : boolean = false;
  public isdownLoadPDF:boolean = false;
  public closeResult: string;
  public checkbetweenChange:boolean = false;
  public listStatus:any;
  public listStatusObj:any;
  public listDeviced: any = [];
  public deviceSelected:any;
  public paramsEmiter: EventEmitter<any>;

  // fuel
  public showFuel:boolean = true;

  // export
  public configExport:PdfModel;
 // configChart
  constructor(
    private userTreeService: UserTreeService,
    private qcvnDataTransmission: QCVNDataTransmissionService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private deviceService: DeviceService,
    private translate: TranslateService,
    private toast: ToastService,
    private currentUser: CurrentUserService,
    private xlsx: ExcelService,
    public datepipe: DatePipe,
    private userDate: UserDateService,
    private userDatePipe: UserDatePipe,
    private pdf : PdfmakeService,
    private userDateAdv : UserDateAdvPipe,
    private exportService:ExportService,
    private functionService : FunctionService

  ) {

    this.paramsEmiter = new EventEmitter();

    this.dateFormat = this.currentUser.dateFormatPipe;
    this.timeFormat = this.currentUser.timeFormat;
    this.timeZone = this.currentUser.currentUser.timezone;
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.parentBread = [
      {
        name: this.translate.instant('REPORT.REPORTS.GENERAL.REPORTS'),
        translate: "REPORT.REPORTS.GENERAL.REPORTS",
        link: "/reports",
        icon: "flaticon2-graph"
      }
    ]
  }

  ngOnInit() {
    this.updateDataTable();
    this.refreshEventType();
    this.datePickerOptions = this.objOption();
    this.userTreeService.event.pipe(
        tap(action => {
          switch (action) {
            case "close":
              this.showUserTree = false;
              break;
            case "open":
              this.showUserTree = true;
              break;
          }
        })
      ).subscribe();
  }



  objOption()
  {
    let obj = {
      size: 'md',
      select: 'from',
      ranges: 'from',
      optionDatePicker: {
        maxSpan: {
          days: 31
        }

      },
      autoSelect: true,
      singleDatePicker: false,
      timePicker: true,
      startDate:moment().subtract(1, "days").startOf('day'),
      endDate:moment().subtract(1, "days").endOf('day'),
    }; 
    return obj;
  }

  renderConfig(params)
  {
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    this.configExport  = {
      info: {
        title: 'BGTVT_BC_TruyenDuLieu',
      },
      content:{
        header:{
          title:this.translate.instant('REPORT.QCVN.DATA_TRANSMISSION.NAME'),
          params:{
            timeStart:": "+this.dateStartExport,
            timeEnd:": "+this.dateEndExport,
            unit:": "+params.unit,
            numberPlate:": "+params.numberPlate
          }
        },
        table:[
          {
              woorksheet: {
                name: params.numberPlate,
              },
              total : {
              },
              headerRows: 1,
              widths: ['4%','15%','15%','15%','15%','15%','15%'],
              header :[           
                  [ 
                    
                    { text: '#',alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.QCVN.ROUTE.NUMBER_PLATE'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.QCVN.OVER_SPEED.TYPE_WORK'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.QCVN.DATA_TRANSMISSION.GTVT'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.QCVN.DATA_TRANSMISSION.UNIT'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('COMMON.COLUMN.DATE'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.QCVN.DATA_TRANSMISSION.NUMBER_NEW'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                  ],
               
              ],
              body:[
                {columnData:'count',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'numberPlate',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'transportType',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'govManagement',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'nameTransport',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'time',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'numberNewsletter',style:{alignment:"left",fillColor:"#fff",bold:false}}

              ],              
          },
          
        ]
       },
      footer:{
        title: this.datepipe.transform(this.dateStart, dateTimeFormat)+ ' - ' + this.datepipe.transform(this.dateEnd, dateTimeFormat)+ ' - ' + params.numberPlate
      }    
    };
  }

  buildDateNow() {
    this.datePicker.refresh();
    this.deviceId = '';
    this.status = '';
    this.dataTable.update({
        data: [],
      });
    setTimeout( () => {
      $('.kt_selectpicker').selectpicker('refresh');
    });
    
  }

  private buildForm(): void {
    this.searchFormQCVN = this.formBuilder.group({
      dateStart: [''],
      dateEnd: [''],
      deviceId: [''],
      status: ['']
    });

    
  }

  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateStartExport = data.startDate.format("HH:mm:ss DD/MM/YYYY");
    this.dateEndExport = data.endDate.format("HH:mm:ss DD/MM/YYYY");

    // this.renderConfig();
   }


  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
    };
  }

  ChangeUserTree(value) {
    if (value.id > 0) {
      this.userIdSelectedEdit = value.id;
      this.getDevices(value.id);
      this.buildDateNow();
      this.dataTable.update({
        data: [],
        totalRecod: 0
      });
    }
  }

  getNameDevice(text: string) {
    this.deviceName = text;
    let deviceId = this.searchFormQCVN.value.deviceId;
    let numberPlate = '';
    let nameDriver = '';
    if(deviceId == '')
    {
        numberPlate = this.translate.instant('COMMON.GENERAL.ALL');
        nameDriver = this.listDevices[0].nameDriver;
        if(nameDriver == '' || nameDriver == null) nameDriver = this.listDevices[0].userName;

    }else{
        let device = this.listDevices.find(x=>x.id == deviceId);
        numberPlate = device.numberPlate;
        nameDriver = device.nameDriver;
        if(nameDriver == '' || nameDriver == null) nameDriver = device.userName;
        if(numberPlate == '' || numberPlate == null) numberPlate = device.name;
    }
    this.deviceSelected = {
        numberPlate : numberPlate,
        unit : nameDriver
    };

  }


  getDevices(userId) {
    let params = {
      userId: userId, 
      pageNo: -1
    };

    // this.filter.pageNo = -1;
    this.deviceService.list({ params: params }).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      data.result = data.result.filter(device=>{
        return device.qcvn == true && device.transportTypeQcvn > 0;
      })
      this.listDevices = data.result;
      this.paramsEmiter.emit(this.listDevices);

      setTimeout(function () {
        $('.kt_selectpicker').selectpicker('refresh');
      });
    });
  }

  private refreshEventType()
  {
     this.translate.onLangChange.subscribe((event) => {
     let all = this.translate.instant('REPORT.GEOFENCE.SUMMARY.ALL_DEVICE');
     let idTime = this.translate.instant('REPORT.QCVN.ROUTE.TIME');
     let unitTime = this.translate.instant('REPORT.QCVN.ROUTE.UNIT_TIME');
     $('#col-time').text(idTime);
     $('#unit-time').text(unitTime);
     $(function () {
       $('#days').selectpicker({title: all}).selectpicker('refresh');
     });
   });
  }

  changeChecked(event){
    this.listDeviced = event.listChecked;
  }


  checkSensorFuel(nameSensor)
  {
    let check = false;
    this.listDevices.find(device =>{
     let sensor =  device.sensors.find(o => o.name == nameSensor);
     if(sensor != undefined)
     {
      if(JSON.parse(sensor.parameters).typeSensor == 'fuel') check = true;
     }
      
    });
    
    return check;
  }
  


  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  validateForm(deviceId: any) {
    if (deviceId == '' || deviceId == undefined || deviceId == null) {
      this.toast.show(
        {
          message: this.translate.instant('MESSEAGE_CODE.SENSOR.DEVICE_NOT_FOUND'),
          type: 'error',
        })
      return false;
    }
    return true;
  }


  searchSummary(form: any) {
    if(this.getListFilter()) 
    {
        this.showFullData = true;
        this.dataTable.reload({ currentPage: 1 });

    }
  }

  checkbetween(value)
  {
    this.checkbetweenChange = value;
  }


  resetFormSearch() {
    this.buildDateNow();
  }


  async getData(filter,type?:string) {
    if(type == 'pdf') this.isdownLoadPDF =true;
    if(type == 'excel') this.isdownLoad = true;
    let _this = this;
    let pageNo = this.filter.pageNo - 1;
    if(type == 'pdf' || type == 'excel') filter.pageNo = -1;
    let pageSize = this.filter.pageSize;
    let j = 1;

    // filter.timeFrom = '2019-10-23 00:00:00';
    // filter.timeTo = '2019-10-24 23:59:00';
    // filter.deviceIds = 83;
    await this.qcvnDataTransmission.list(filter)
      .pipe(
        delay(300),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          let data:any = [];

          if(type == 'pdf') _this.isdownLoadPDF = false;
          if(type == 'excel') _this.isdownLoad = false;

          result.result.map(itemData=>{
             
                itemData.time = this.userDateAdv.transform(itemData.timeStamp,'DD/MM/YYYY');
                let count = pageNo * pageSize + j;
                itemData.count = count;
                j++;
                data.push(itemData);
                return itemData;
             
          });
          
          this.dataExport = 
          [
            {
              data:data
            }
          ];
          this.getNameDevice('text');
          let params = this.deviceSelected;
          this.dataPagination = data;
          this.getDataShowPage();
          if(!type)
          {
            this.dataTable.update({
              data:this.data,
              totalRecod: j-1
            });
          }
          if(type == 'pdf' && this.dataExport)     this.exportFilePDF(this.dataExport,params);
          if(type == 'excel' &&this.dataExport)    this.exportFileXLSX(this.data,params);
          this.showFullData = false;
        }
      })
  }

 
  getDataShowPage()
  {
    this.data = [];
    this.data = this.functionService.pagination(this.dataPagination,this.filter.pageNo,this.filter.pageSize,1);

  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;
        if(this.showFullData)
        {
          this.getData(this.filter);
        }else{
          this.dataTable.isLoading = true;
          setTimeout(() => {
            this.getDataShowPage();
            this.dataTable.update({
              data: this.data
            });
            this.cdr.markForCheck();
            this.cdr.detectChanges();

            this.dataTable.isLoading = false;

          },300);
       
        }      }),

    ).subscribe();
  }

  convertNumberToTime(number) {
    return this.userDate.convertNumberToTime(number);
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
        responsive: false,

      }
    });
  }


  renderDatePickerChart(dateStartExport,endDate)
  {
    this.datePickerOptionsSearchChart = {
      size: 'md',
      select: 'from',
      ranges: 'from',
      optionDatePicker: {
        maxSpan: {
          days: 31
        }

      },
      autoSelect: true,
      singleDatePicker: false,
      timePicker: true,
      startDate:dateStartExport,
      endDate:endDate
    }
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  roundNumber(number) {
    return Math.round(number * 100000 ) / 100000;
  }

  roundNumber2(number){
    return Math.round(number * 100 ) / 100;

  }

  async getdataExcel(file) {
    if(file =='pdf')
    {this.isdownLoadPDF = true;}else
    {this.isdownLoad = true; } 
    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;
    this.filter.deviceId = this.searchFormQCVN.value.deviceId; 
    this.getData(this.filter,file);

  }

  //pdf
  async exportPDF(){
       // this.filter.deviceId = this.
       if(this.getListFilter())  this.getData(this.filter,'pdf');
       
  }
  

  getListFilter(){
    this.listDeviced = [];
    let check = this.functionService.checkDiffInDays(this.dateStart,this.dateEnd,6);
    if(!check){
        this.toast.show(
            {
              message: this.translate.instant('REPORT.QCVN.SPEED.MESSAGE'),
              type: 'error',
            });
       return false;     
    }
    this.filter.deviceIds = this.searchFormQCVN.value.deviceId;
    if(this.searchFormQCVN.value.deviceId == '')
    {
      this.listDevices.forEach(device => {
        this.listDeviced.push(device.id);
      });
    }else{
        this.listDeviced.push(this.searchFormQCVN.value.deviceId);
    }

    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;
    this.filter.deviceIds = this.listDeviced.toString();
    if(!this.validateForm(this.filter.deviceIds)) return false;   
    return true;
  }

  exportFilePDF(data,params){
    this.renderConfig(params);
    this.pdf.ExportPdf([this.dataExport],this.configExport);
  }
  
  //export
  exportXlSX() {
    if(this.getListFilter()) this.getdataExcel('excel');
  }

  convertTextData(text)
  {    
    return this.exportService.convertTextData(text);
  }

  private exportFileXLSX(data,params) {

    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let config: XLSXModelTest = {
      file: {
        title: this.translate.instant('REPORT.QCVN.DATA_TRANSMISSION.NAME'),
        prefixFileName: "BGTVT_BC_TruyenDuLieu",
        timeStart:this.dateStartExport,
        timeEnd:this.dateEndExport,
      },
      template: [{
        header: [
          {
            text: this.translate.instant('REPORT.QCVN.DATA_TRANSMISSION.NAME'),
            type: "header",
          },
          {
            text: ""
          },
          {
            text: '!timezone',
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START') + ": " + this.dateStart,
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_END') + ": " + this.dateEnd,
          },
          {
            text: this.translate.instant('REPORT.QCVN.ROUTE.UNIT') + ": " + params.unit,
          },
          {
            text: this.translate.instant('REPORT.QCVN.ROUTE.NUMBER_PLATE') + ": " + params.numberPlate,
          },
          
          {
            text: ""
          },
        ],
        columns: [
          {
            name: "#",
            columnData: "count",
            wch: 5,
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
      
          },
          {
            name: this.translate.instant('REPORT.QCVN.ROUTE.NUMBER_PLATE'),
            columnData: "numberPlate",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
         
          },
          {
            name: this.translate.instant('REPORT.QCVN.OVER_SPEED.TYPE_WORK'),
            columnData: "transportType",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
          },
          {
            name: this.translate.instant('REPORT.QCVN.DATA_TRANSMISSION.GTVT'),
            columnData: "govManagement",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
          },
          {
            name: this.translate.instant('REPORT.QCVN.DATA_TRANSMISSION.UNIT'),
            columnData: "nameTransport",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
          },
          {
            name: this.translate.instant('COMMON.COLUMN.DATE'),
            columnData: "time",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
          },
          {
            name: this.translate.instant('REPORT.QCVN.DATA_TRANSMISSION.NUMBER_NEW'),
            columnData: "numberNewsletter",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
          },

        ],
        woorksheet: {
          name: params.numberPlate,
        },
        total: {
         
        }
      }]

    };

    this.xlsx.exportFileTest([this.dataExport], config, {});
  }
  setColumns() {
    this.columns = [
        {
            title: '#',
            field: 'no',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '20px' },
            class: 't-datatable__cell--center',
            translate: '#',
            autoHide: false,
            width: 20,
          },
          {
            title: 'NUMBER_PLATE',
            field: 'time',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '200px' },
            class: '',
            translate: 'REPORT.QCVN.ROUTE.NUMBER_PLATE',
            autoHide: false,
            width: 200,
          },
          
          {
            title: 'TYPE_WORK',
            field: 'Address',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '200px' },
            class: '',
            translate: 'REPORT.QCVN.OVER_SPEED.TYPE_WORK',
            autoHide: false,
            width: 200,
          },
          {
            title: 'GTVT',
            field: 'time',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '200px' },
            class: '',
            translate: 'REPORT.QCVN.DATA_TRANSMISSION.GTVT',
            autoHide: false,
            width: 200,
          },
          {
            title: 'UNIT',
            field: 'Address',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '200px' },
            class: '',
            translate: 'REPORT.QCVN.DATA_TRANSMISSION.UNIT',
            autoHide: false,
            width: 200,
          },
          {
            title: 'DATE',
            field: 'Address',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '200px' },
            class: '',
            translate: 'COMMON.COLUMN.DATE',
            autoHide: false,
            width: 200,
          },
          {
            title: 'NUMBER_NEW',
            field: 'Address',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '200px' },
            class: '',
            translate: 'REPORT.QCVN.DATA_TRANSMISSION.NUMBER_NEW',
            autoHide: false,
            width: 200,
          },
    
    ]
  }


}




