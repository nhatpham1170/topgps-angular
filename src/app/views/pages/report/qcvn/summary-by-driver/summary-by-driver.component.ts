import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef,EventEmitter, } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { finalize, takeUntil, tap, debounceTime,delay } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { QCVNSummaryByDriverService } from '@core/report';
import { DeviceService,DriverService } from '@core/manage';
import { TranslateService } from '@ngx-translate/core';
import { ToastService, UserDatePipe,UserDateAdvPipe,FunctionService } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { ExcelService,PdfmakeService,ExcelTestService} from '@core/utils';
import { XLSXModel, XLSXModelTest } from '@core/utils/xlsx/excel.service';
import { PdfModel } from '@core/utils/pdfmake/pdfmake.service';

import { DatePipe } from '@angular/common';
import { UserDateService } from '@core/_base/layout';
import { DecimalPipe } from '@angular/common';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { ExportService } from '@core/utils';
import { UserTreeService } from '@core/common/_service/user-tree.service';

// end chart
declare var $: any;

@Component({
  selector: 'qcvn-summary-by-driver',
  templateUrl: './summary-by-driver.component.html',
  styleUrls: ['./summary-by-driver.component.scss'],
  providers: [DatePipe, UserDatePipe, DecimalPipe,UserDateAdvPipe]

})
export class SummaryByDriverComponent implements OnInit {
  @ViewChild('listDevicePicker', { static: true }) 
  listDevicePicker:any;

  @ViewChild('datePicker', { static: true }) 
  datePicker: DateRangePickerComponent;
  @ViewChild('datePickerSearchChart', { static: true }) 

  datePickerSearchChart: DateRangePickerComponent;
  searchFormChart:FormGroup;
  searchFormQCVN: FormGroup;
  selectdp: ElementRef;
  public datePickerOptions: any;
  public datePickerOptionsSearchChart:any;

  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  // show full data
  public showFullData:boolean = true;
  public dataPagination : any = [];
  public dataExport: any = null;
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataDefault: any = [{}];
  public filter: any;
  private unsubscribe: Subject<any>;
  public listDevices: any = [];
  public listDrivers: any = [];
  public userIdSelected: number;
  public userIdSelectedEdit: number;
  public showUserTree: boolean = false;
  public dateStart: string;
  public dateEnd: string;
  public timeStart: string;
  public timeEnd: string;
  //time search chart
  public dateStartExport:string;
  public dateEndExport:string;
  public sensorIdSearhChart:any;
  public driverId: string = '';
  public status: string;

  public driverIdSearhChart:any;
  public deviceName: string;
  public dateFormat: string;
  public timeFormat: string;
  public timeZone:string;
  public totalAllRowData : any;
  public parentBread:any;
  public isdownLoad : boolean = false;
  public isdownLoadPDF:boolean = false;
  public closeResult: string;
  public checkbetweenChange:boolean = false;
  public listStatus:any;
  public listStatusObj:any;
  public listDrived: any = [];
  public deviceSelected:any;
  public paramsEmiter: EventEmitter<any>;
  public duration:string;
  // fuel
  public showFuel:boolean = true;

  // export
  public configExport:PdfModel;
 // configChart
  constructor(
    private userTreeService: UserTreeService,
    private qcvnSummary: QCVNSummaryByDriverService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private deviceService: DeviceService,
    private driverService: DriverService,
    private translate: TranslateService,
    private toast: ToastService,
    private currentUser: CurrentUserService,
    private xlsx: ExcelService,
    public datepipe: DatePipe,
    private userDate: UserDateService,
    private userDatePipe: UserDatePipe,
    private pdf : PdfmakeService,
    private userDateAdv : UserDateAdvPipe,
    private exportService:ExportService,
    private functionService : FunctionService

  ) {

    this.paramsEmiter = new EventEmitter();

    this.dateFormat = this.currentUser.dateFormatPipe;
    this.timeFormat = this.currentUser.timeFormat;
    this.timeZone = this.currentUser.currentUser.timezone;
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.parentBread = [
      {
        name: this.translate.instant('REPORT.REPORTS.GENERAL.REPORTS'),
        translate: "REPORT.REPORTS.GENERAL.REPORTS",
        link: "/reports",
        icon: "flaticon2-graph"
      }
    ]
  }

  ngOnInit() {
    this.updateDataTable();
    this.refreshEventType();
    this.datePickerOptions = this.objOption();
    this.userTreeService.event.pipe(
        tap(action => {
          switch (action) {
            case "close":
              this.showUserTree = false;
              break;
            case "open":
              this.showUserTree = true;
              break;
          }
        })
      ).subscribe();
  }



  objOption()
  {
    let obj = {
      size: 'md',
      select: 'from',
      ranges: 'from',
      optionDatePicker: {
        maxSpan: {
          days: 31
        }

      },
      autoSelect: true,
      singleDatePicker: false,
      timePicker: true,
      startDate:moment().subtract(1, "days").startOf('day'),
      endDate:moment().subtract(1, "days").endOf('day'),
    }; 
    return obj;
  }

  renderConfig(params)
  {
    
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    this.configExport  = {
      info: {
        title: 'BGTVT_BC_TongHopTheoLaiXe',
      },
      content:{
        header:{
          title:this.translate.instant('REPORT.QCVN.DRIVER.NAME'),
          params:{
            timeStart:": "+this.dateStartExport,
            timeEnd:": "+this.dateEndExport,
            unit:": "+params.unit,
            driver:": "+params.driverName,
          }
        },
        table:[
          {
              woorksheet: {
                name: params.numberPlate,
              },
              total : {
              },
              headerRows: 2,
              widths: ['4%','6%','6%','6%','10%','10%','10%','6%','10%','10%','6%','6%','6%','6%'],
              header :[           
                [ 
                    
                  { text: '#',rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                  { text: this.translate.instant('REPORT.QCVN.OVER_SPEED.NAME_DRIVER'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,15,0,0]},
                  { text: this.translate.instant('REPORT.QCVN.OVER_SPEED.LICENSE_NUMBER_DRIVER'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,15,0,0]},
                  { text: this.translate.instant('REPORT.QCVN.VEHICLE.ALL_KM'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,15,0,0]},
                  { text: this.translate.instant('REPORT.QCVN.DRIVER.RATION_LITMIT'),colSpan:4,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,2,0,0]},
                  { text:''},{ text:''},{ text:''},
                  { text: this.translate.instant('REPORT.QCVN.VEHICLE.COUNT_OVER_SPEED'),colSpan:4,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,2,0,0]},
                  { text:''},{ text:''},{ text:''},
                  { text: this.translate.instant('REPORT.QCVN.DRIVER.TOTAL_OVERSPEED_4H'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,15,0,0]},
                  { text: this.translate.instant('COMMON.COLUMN.NOTE'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,15,0,0]},
                ],
                [
                  '', '', '', '', 
                  { text: this.translate.instant('REPORT.QCVN.VEHICLE.RATION_5TO10'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                  { text: this.translate.instant('REPORT.QCVN.VEHICLE.RATION_10TO20'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                  { text: this.translate.instant('REPORT.QCVN.VEHICLE.RATION_20TO35'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                  { text: this.translate.instant('REPORT.QCVN.VEHICLE.RATION_OVER30'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                  { text: this.translate.instant('REPORT.QCVN.VEHICLE.COUNT_5TO10'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                  { text: this.translate.instant('REPORT.QCVN.VEHICLE.COUNT_10TO20'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                  { text: this.translate.instant('REPORT.QCVN.VEHICLE.COUNT_20TO35'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                  { text: this.translate.instant('REPORT.QCVN.VEHICLE.COUNT_OVER30'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},

                  '', '',
                ]
               
              ],
              body:[
                {columnData:'count',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'driverName',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'licenseNumber',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'distance',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'ration5to10',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'ration10to20',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'ration20to35',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'ration35',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'numberOverSpeed5To10',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'numberOverSpeed10To20',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'numberOverSpeed20To35',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'numberOverSpeed35',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'totalOverTime4h',style:{alignment:"left",fillColor:"#fff",bold:false}},

                {columnData:'note',style:{alignment:"left",fillColor:"#fff",bold:false}},
              ],              
          },
          
        ]
       },
      footer:{
        title: this.datepipe.transform(this.dateStart, dateTimeFormat)+ ' - ' + this.datepipe.transform(this.dateEnd, dateTimeFormat)+ ' - ' + params.numberPlate
      }    
    };
  }

  buildDateNow() {
    this.datePicker.refresh();
    this.driverId = '';
    this.status = '';
    this.dataTable.update({
        data: [],
      });
    setTimeout( () => {
      $('.kt_selectpicker').selectpicker('refresh');
    });
    
  }

  private buildForm(): void {
    this.searchFormQCVN = this.formBuilder.group({
      dateStart: [''],
      dateEnd: [''],
      driverId: [''],
      tooFour: ['']
    });

    
  }

  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateStartExport = data.startDate.format("HH:mm:ss DD/MM/YYYY");
    this.dateEndExport = data.endDate.format("HH:mm:ss DD/MM/YYYY");

    // this.renderConfig();
   }


  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
    };
  }

  ChangeUserTree(value) {
    if (value.id > 0) {
      let name = value.name;
      if(name == '' || name == undefined) name = value.username;
      this.userIdSelectedEdit = value.id;
      this.deviceSelected = {
        unit:value.name
      };
      this.getDrivers(value.id);
      this.buildDateNow();
      this.dataTable.update({
        data: [],
        totalRecod: 0
      });
    }
  }

  getNameDevice(text: string) {
    this.deviceName = text;
    let driverId = this.searchFormQCVN.value.driverId;
    let nameDriver = '';
    if(driverId == '')
    {
        nameDriver = 'Tất cả';

    }else{
        let driver = this.listDrivers.find(x=>x.id == driverId);
        nameDriver = driver.name;
        if(nameDriver == '' || nameDriver == null) nameDriver = driver.licenseNumber;
    }
    this.deviceSelected.driverName = nameDriver;


  }

  getDrivers(userId) {
    let params = {
      userId: userId, 
      pageNo: -1
    };

    // this.filter.pageNo = -1;
    this.driverService.list({ params: params }).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {

      this.listDrivers = data.result;

      setTimeout(function () {
        $('.kt_selectpicker').selectpicker('refresh');
      });
    });
  }

  private refreshEventType()
  {
     this.translate.onLangChange.subscribe((event) => {
     let all = this.translate.instant('COMMON.GENERAL.ALL');
     let idTime = this.translate.instant('REPORT.QCVN.ROUTE.TIME');
     let unitTime = this.translate.instant('REPORT.QCVN.ROUTE.UNIT_TIME');
     $('#col-time').text(idTime);
     $('#unit-time').text(unitTime);
     $(function () {
       $('#days').selectpicker({title: all}).selectpicker('refresh');
     });
   });
  }


  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  validateForm(driverId: any) {
    if (driverId == '' || driverId == undefined || driverId == null) {
      this.toast.show(
        {
          message: this.translate.instant('REPORT.QCVN.DRIVER.NOTFOUND'),
          type: 'error',
        })
      return false;
    }
    return true;
  }


  searchSummary(form: any) {
    if(this.getListFilter()) 
    {
        this.showFullData = true;
        this.dataTable.reload({ currentPage: 1 });

    }
  }

  checkbetween(value)
  {
    this.checkbetweenChange = value;
  }


  resetFormSearch() {
    this.buildDateNow();
  }


  async getData(filter,type?:string) {
    if(type == 'pdf') this.isdownLoadPDF =true;
    if(type == 'excel') this.isdownLoad = true;
    let _this = this;
    let pageNo = this.filter.pageNo - 1;
    if(type == 'pdf' || type == 'excel') filter.pageNo = -1;
    let pageSize = this.filter.pageSize;
    let j = 1;

    // filter.timeFrom = '2019-10-23 00:00:00';
    // filter.timeTo = '2019-10-24 23:59:00';
    // filter.driverIds = 83;
    await this.qcvnSummary.list(filter)
      .pipe(
        delay(300),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          let data:any = [];

          if(type == 'pdf') _this.isdownLoadPDF = false;
          if(type == 'excel') _this.isdownLoad = false;
          result.result.map(itemData=>{
                let total = itemData.numberOverSpeed5To10 + itemData.numberOverSpeed10To20 + itemData.numberOverSpeed20To35 + itemData.numberOverSpeed35;
                let ration5to10 = 0;
                let ration10to20 = 0;
                let ration20to35 = 0;
                let ration35 = 0;

                if(total != 0) ration5to10 = (itemData.numberOverSpeed5To10 / total)*100;
                if(total != 0) ration10to20 = (itemData.numberOverSpeed10To20 / total)*100;
                if(total != 0) ration20to35 = (itemData.numberOverSpeed20To35 / total)*100;
                if(total != 0) ration35 = (itemData.numberOverSpeed35 / total)*100;

                itemData.ration5to10 = this.roundNumber2(ration5to10)+' %';
                itemData.ration10to20 = this.roundNumber2(ration10to20)+' %';
                itemData.ration20to35 = this.roundNumber2(ration20to35)+' %';
                itemData.ration35 = this.roundNumber2(ration35)+' %';

                itemData.distance = this.roundNumber2(itemData.distance);
                if(itemData.driverName == 'unknown') itemData.driverName = this.translate.instant('SIM.UNKNOWN');
                let count = pageNo * pageSize + j;
                itemData.count = count;
                j++;
                data.push(itemData);
                return itemData;
          });
          
          this.dataExport = 
          [
            {
              data:data
            }
          ];
          this.getNameDevice('text');
          let params = this.deviceSelected;
          this.dataPagination = data;
          this.getDataShowPage();
          if(!type)
          {
            this.dataTable.update({
              data:data,
              totalRecod: j-1
            });
          }
          if(type == 'pdf' && this.dataExport)     this.exportFilePDF(this.dataExport,params);
          if(type == 'excel' &&this.dataExport)    this.exportFileXLSX(this.data,params);
          this.showFullData = false;
        }
      })
  }

 
  getDataShowPage()
  {
    this.data = [];
    this.data = this.functionService.pagination(this.dataPagination,this.filter.pageNo,this.filter.pageSize,1);

  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;
        if(this.showFullData)
        {
          this.getData(this.filter);
        }else{
          this.dataTable.isLoading = true;
          setTimeout(() => {
            this.getDataShowPage();
            this.dataTable.update({
              data: this.data
            });
            this.cdr.markForCheck();
            this.cdr.detectChanges();

            this.dataTable.isLoading = false;

          },300);
       
        }      }),

    ).subscribe();
  }

  convertMinus(second)
  {
    let m = Math.ceil(second/60);
    return m;
  }

  convertNumberToTime(number) {
    return this.userDate.convertNumberToTime(number);
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
        responsive: false,

      }
    });
  }


  renderDatePickerChart(dateStartExport,endDate)
  {
    this.datePickerOptionsSearchChart = {
      size: 'md',
      select: 'from',
      ranges: 'from',
      optionDatePicker: {
        maxSpan: {
          days: 31
        }

      },
      autoSelect: true,
      singleDatePicker: false,
      timePicker: true,
      startDate:dateStartExport,
      endDate:endDate
    }
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  roundNumber(number) {
    return Math.round(number * 100000 ) / 100000;
  }

  roundNumber2(number){
    return Math.round(number * 100 ) / 100;

  }

  async getdataExcel(file) {
    if(file =='pdf')
    {this.isdownLoadPDF = true;}else
    {this.isdownLoad = true; } 
    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;
    this.filter.driverId = this.searchFormQCVN.value.driverId; 
    this.getData(this.filter,file);

  }

  //pdf
  async exportPDF(){
       // this.filter.driverId = this.
       if(this.getListFilter())  this.getData(this.filter,'pdf');
       
  }
  

  getListFilter(){
    this.listDrived = [];
    let check = this.functionService.checkDiffInDays(this.dateStart,this.dateEnd,6);
    if(!check){
        this.toast.show(
            {
              message: this.translate.instant('REPORT.QCVN.SPEED.MESSAGE'),
              type: 'error',
            });
       return false;     
    }
    this.filter.driverIds = this.searchFormQCVN.value.driverId;
    if(this.searchFormQCVN.value.driverId == '')
    {
      this.listDrivers.forEach(d => {
        this.listDrived.push(d.id);
      });
    }else{
        this.listDrived.push(this.searchFormQCVN.value.driverId);
    }

    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;
    this.filter.driverIds = this.listDrived.toString();

    if(!this.validateForm(this.filter.driverIds)) return false;   
    return true;
  }

  exportFilePDF(data,params){
    this.renderConfig(params);
    this.pdf.ExportPdf([this.dataExport],this.configExport);
  }
  
  //export
  exportXlSX() {
    if(this.getListFilter()) this.getdataExcel('excel');
  }

  convertTextData(text)
  {    
    return this.exportService.convertTextData(text);
  }

  private exportFileXLSX(data,params) {

    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let config: XLSXModelTest = {
      file: {
        title: this.translate.instant('REPORT.QCVN.DRIVER.NAME'),
        prefixFileName: "BGTVT_BC_TongHopTheoLaiXe",
        timeStart:this.dateStartExport,
        timeEnd:this.dateEndExport,
      },
      template: [{
        header: [
          {
            text: this.translate.instant('REPORT.QCVN.DRIVER.NAME'),
            type: "header",
          },
          {
            text: ""
          },
          {
            text: '!timezone',
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START') + ": " + this.dateStart,
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_END') + ": " + this.dateEnd,
          },
          {
            text: this.translate.instant('REPORT.QCVN.DRIVER.NAME_EXPORT') + ": " + params.driverName,
          },
      
          {
            text: ""
          },
        ],
        columns: [
          {
            name: "#",
            columnData: "count",
            wch: 5,
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            merge:{
              rowSpan:1
            }
          },

          {
            name: this.translate.instant('REPORT.QCVN.OVER_SPEED.NAME_DRIVER'),
            columnData: "driverName",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 15,
            merge:{
              rowSpan:1
            }
          },
          {
            name: this.translate.instant('REPORT.QCVN.OVER_SPEED.LICENSE_NUMBER_DRIVER'),
            columnData: "licenseNumber",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 15,
            merge:{
              rowSpan:1
            }
          },
          {
            name: this.translate.instant('REPORT.QCVN.VEHICLE.ALL_KM'),
            columnData: "distance",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 15,
            merge:{
              rowSpan:1
            }
          },
          {
            name: this.translate.instant('REPORT.QCVN.DRIVER.RATION_LITMIT'),
            columnData: "ration5to10",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
            merge:{
              colSpan:3
            }
          },
          {
            name: this.translate.instant('REPORT.QCVN.VEHICLE.RATION_LITMIT'),
            columnData: "ration10to20",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
           
          },
          {
            name: this.translate.instant('REPORT.QCVN.VEHICLE.RATION_LITMIT'),
            columnData: "ration20to35",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
        
          },
          {
            name: this.translate.instant('REPORT.QCVN.VEHICLE.RATION_LITMIT'),
            columnData: "ration35",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
        
          },
          {
            name: this.translate.instant('REPORT.QCVN.VEHICLE.COUNT_OVER_SPEED'),
            columnData: "numberOverSpeed5To10",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
            merge:{
              colSpan:3
            }
          },
          {
            name: this.translate.instant('REPORT.QCVN.VEHICLE.COUNT_OVER_SPEED'),
            columnData: "numberOverSpeed10To20",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
        
          },
          {
            name: this.translate.instant('REPORT.QCVN.VEHICLE.COUNT_OVER_SPEED'),
            columnData: "numberOverSpeed20To35",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
         
          },
          {
            name: this.translate.instant('REPORT.QCVN.VEHICLE.COUNT_OVER_SPEED'),
            columnData: "numberOverSpeed35",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
            hpx:10
         
          },
          {
            name: this.translate.instant('REPORT.QCVN.DRIVER.TOTAL_OVERSPEED_4H'),
            columnData: "totalOverTime4h",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
            merge:{
              rowSpan:1
            }
          },
          {
            name: this.translate.instant('COMMON.COLUMN.NOTE'),
            columnData: "note",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
            merge:{
              rowSpan:1
            }
          },
        ],
        columnsMerge:
        {
          data: [
            "",
            "",
            "",
            "",
         
            this.translate.instant('REPORT.QCVN.VEHICLE.RATION_5TO10'),
            this.translate.instant('REPORT.QCVN.VEHICLE.RATION_10TO20'),
            this.translate.instant('REPORT.QCVN.VEHICLE.RATION_20TO35'),
            this.translate.instant('REPORT.QCVN.VEHICLE.RATION_OVER30'),
            this.translate.instant('REPORT.QCVN.VEHICLE.COUNT_5TO10'),
            this.translate.instant('REPORT.QCVN.VEHICLE.COUNT_10TO20'),
            this.translate.instant('REPORT.QCVN.VEHICLE.COUNT_20TO35'),
            this.translate.instant('REPORT.QCVN.VEHICLE.COUNT_OVER30'),
            "",
            "",

          ],
          // position: ["A8:A9","B8:B9","C8:E8","F8:H8","I8:J8"]
        },
        woorksheet: {
          name: params.numberPlate,
        },
        total: {
         
        }
      }]

    };

    this.xlsx.exportFileTest([this.dataExport], config, {});
  }
  setColumns() {
    this.columns = [
          {
            title: 'RATION_5TO10',
            field: 'time',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '120px' },
            class: '',
            translate: 'REPORT.QCVN.VEHICLE.RATION_5TO10',
            autoHide: false,
            width: 200,
          },
          {
            title: 'RATION_10TO20',
            field: 'COORDINATE',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '150px' },
            class: '',
            translate: 'REPORT.QCVN.VEHICLE.RATION_10TO20',
            autoHide: false,
            width: 200,
          },
          {
            title: 'RATION_20TO35',
            field: 'Address',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '150px' },
            class: '',
            translate: 'REPORT.QCVN.VEHICLE.RATION_20TO35',

            autoHide: false,
            width: 200,
          },
          {
            title: 'RATION_OVER30',
            field: 'time',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '100px' },
            class: '',
            translate: 'REPORT.QCVN.VEHICLE.RATION_OVER30',

            autoHide: false,
            width: 200,
          },
          {
            title: 'COUNT_5TO10',
            field: 'COORDINATE',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '120px' },
            class: '',
            translate: 'REPORT.QCVN.VEHICLE.COUNT_5TO10',

            autoHide: false,
            width: 200,
          },
          {
            title: 'COUNT_10TO20',
            field: 'Address',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '150px' },
            class: '',
            translate: 'REPORT.QCVN.VEHICLE.COUNT_10TO20',

            autoHide: false,
            width: 200,
          },
          {
            title: 'COUNT_20TO35',
            field: 'Address',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '150px' },
            class: '',
            translate: 'REPORT.QCVN.VEHICLE.COUNT_20TO35',

            autoHide: false,
            width: 200,
          },
          {
            title: 'COUNT_OVER30',
            field: 'Address',
            allowSort: false,
            isSort: false,
            dataSort: '',
            style: { 'width': '100px' },
            class: '',
            translate: 'REPORT.QCVN.VEHICLE.COUNT_OVER30',

            autoHide: false,
            width: 200,
          },
    ]
  }


}




