import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PartialsModule } from '@app/views/partials/partials.module';
import { RouterModule, Routes } from '@angular/router';
import { NgbDropdownModule, NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ClipboardModule } from 'ngx-clipboard';
import { NgxPermissionsModule } from 'ngx-permissions';
//report
import { ReportComponent } from './report.component';
import { ReportRouteComponent } from './report-route/report-route.component';
import { ReportRouteSimpleComponent } from './report-route-simple/report-route-simple.component';

import { ReportEngineComponent } from './report-engine/report-engine.component';
import { ReportGeneralComponent } from './report-summary-devices/report-summary-devices';
import { ReportSummaryDay } from './report-summary-day/report-summary-day.component';
import { ReportStatusHistory } from './report-status-history/report-status-history.component';
import { ReportTollStation } from './report-toll-station/report-toll-station.component';
import { ReportStopPointComponent } from './report-stop-point/report-stop-point.component';


// QCVN
import { RouteComponent } from './qcvn/route/route.component';
import { SpeedComponent } from './qcvn/speed/speed.component';
import { OverSpeedComponent } from './qcvn/over-speed/over-speed.component';
import { Driving4hComponent } from './qcvn/driving-4h/driving-4h.component';
import { Driving10hComponent } from './qcvn/driving-10h/driving-10h.component';
import { DataTransmissionComponent } from './qcvn/data-transmission/data-transmission.component';

import { StopComponent } from './qcvn/stop/stop.component';
import { SummaryByVehicleComponent } from './qcvn/summary-by-vehicle/summary-by-vehicle.component';
import { SummaryByDriverComponent } from './qcvn/summary-by-driver/summary-by-driver.component';

// report trip

import { ReportTripComponent } from './report-trip/report-trip.component';
import { ReportTripDetailComponent } from './report-trip-detail/report-trip-detail.component';


// report geofence
import { ReportGeofenceSummaryComponent } from './report-geofence-summary/report-geofence-summary.component';
import { ReportGeofenceDetailComponent } from './report-geofence-detail/report-geofence-detail.component';

import { ReportTemperatureComponent } from './report-temperature/report-temperature.component';

//
import { ModuleGuard, AuthGuard } from '@core/auth';
import { ReportDeviceComponent } from './report-device/report-device.component';
import { ReportsComponent } from './reports/reports.component';

//wait download report
import { WaitDownloadComponent } from './wait-download/wait-download.component';
//geofence tool
import { SortablejsModule } from 'ngx-sortablejs';


import { CoreModule } from '@core/core.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';// Angular Material
import { ChartsModule } from 'ng2-charts';

import {DragDropModule} from '@angular/cdk/drag-drop';
import { DataLogComponent } from './data-log/data-log.component';
import { ReportListComponent } from './report-list/report-list.component';

const routes: Routes = [{
  path: '',
  component: ReportComponent,
  children: [
    {
      path: 'temperature',
      component: ReportTemperatureComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data: {
        permisison: 'ROLE_report.temperature',
      },
    },
    {
      path: 'geofence-detail',
      component: ReportGeofenceDetailComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data: {
        permisison: 'ROLE_report.geofence_detail',
      },
    },
    {
      path: 'trip-detail',
      component: ReportTripDetailComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data: {
        permisison: 'ROLE_report.trip_detail',
      },
    },
    {
      path: 'geofence-summary',
      component: ReportGeofenceSummaryComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data: {
        permisison: 'ROLE_report.geofence_summary',
      },
    },
    {
      path: 'trip-summary',
      component: ReportTripComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data: {
        permisison: 'ROLE_report.trip_summary',
      },
    },
    {
      path: 'summary-by-days',
      component: ReportSummaryDay,
      canActivate: [ModuleGuard, AuthGuard],
      data: {
        permisison: 'ROLE_report.summary_day',
      },
    },
    {
      path: 'fuel',
      canActivate: [ModuleGuard, AuthGuard],
      data: {
        permisison: 'ROLE_report.fuel',
      },
    },
    {
      path: 'device',
      component: ReportDeviceComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data: {
        permisison: 'ROLE_report.device',
      },
    },
    {
      path: 'route',
      component: ReportRouteComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data: {
        permisison: 'ROLE_report.route',
      },
    },
    {
      path: 'route-simple',
      component: ReportRouteSimpleComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data: {
        permisison: 'ROLE_report.route_simple',
      },
    },
    {
      path: 'engine',
      component: ReportEngineComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data: {
        permisison: 'ROLE_report.engine',
      },
    },
    {
      path: 'summary-devices',
      component: ReportGeneralComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data: {
        permisison: 'ROLE_report.general',
      },
    },
    {
      path: 'reports',
      component: ReportsComponent,
      canActivate: [ModuleGuard, AuthGuard],
    },
    {
      path: 'lists',
      component: ReportListComponent,
      canActivate: [ModuleGuard, AuthGuard],
    },
    {
      path: 'history',
      component: DataLogComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data:{
        permisison:'ROLE_report.data_log',
      }
    },
    {
      path: 'status-history',
      component: ReportStatusHistory,
      canActivate: [ModuleGuard, AuthGuard],
      data:{
        permisison:'ROLE_report.status_history',
      }
    },
    {
      path: 'toll-station',
      component: ReportTollStation,
      canActivate: [ModuleGuard, AuthGuard],
      data:{
        permisison:'ROLE_report.toll_station',
      }
    },
    {
      path: 'stop-point',
      component: ReportStopPointComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data:{
        permisison:'ROLE_report.stop_point',
      }
    },
    
    
    {path: '', component: ReportListComponent,
    canActivate: [ModuleGuard, AuthGuard],},

    // QCVN
    {
      path: 'gov/journey',
      component: RouteComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data:{
        permisison:'ROLE_report.qcvn_route',
      }
    },
    {
      path: 'gov/speed',
      component: SpeedComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data:{
        permisison:'ROLE_report.qcvn_speed',
      }
    },
    {
      path: 'gov/over-speed',
      component: OverSpeedComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data:{
        permisison:'ROLE_report.qcvn_over_speed',
      }
    },
    {
      path: 'gov/driving',
      component: Driving4hComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data:{
        permisison:'ROLE_report.qcvn_driving_4h',
      }
    },
    {
      path: 'gov/stop',
      component: StopComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data:{
        permisison:'ROLE_report.qcvn_driving_4h',
      }
    },
    {
      path: 'gov/summary-by-vehicle',
      component: SummaryByVehicleComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data:{
        permisison:'ROLE_report.qcvn_summary_by_vehicle',
      }
    },
    {
      path: 'gov/summary-by-driver',
      component: SummaryByDriverComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data:{
        permisison:'ROLE_report.qcvn_summary_by_driver',
      }
    },
    {
      path: 'gov/driving-on-day',
      component: Driving10hComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data:{
        permisison:'ROLE_report.qcvn_driving_10h',
      }
    },

    {
      path: 'gov/data-transmission',
      component: DataTransmissionComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data:{
        permisison:'ROLE_report.qcvn_data_transmission',
      }
    },

    
    
    
  ]
}]

@NgModule({

  imports: [
    CoreModule,
    CommonModule,
    RouterModule.forChild(routes),
    PartialsModule,
    NgbDropdownModule,
    NgbModalModule,
    NgbModule,
    ClipboardModule,
    NgxPermissionsModule,
    NgSelectModule,
    AutocompleteLibModule,
    FormsModule,
    ReactiveFormsModule,
    DragDropModule,
    ChartsModule,
    SortablejsModule
  ],
  providers: [ModuleGuard],
  declarations: [
    WaitDownloadComponent,
     ReportSummaryDay,
     ReportGeneralComponent,
     ReportTemperatureComponent,
     ReportComponent, 
     ReportDeviceComponent,
     ReportsComponent,
     ReportEngineComponent,
     ReportRouteComponent,
     ReportGeofenceSummaryComponent,
     ReportGeofenceDetailComponent,
     DataLogComponent,
     ReportListComponent,
     ReportRouteSimpleComponent,
     ReportStatusHistory,
     ReportTollStation,
     ReportStopPointComponent,
     //QCVN
     RouteComponent,
     SpeedComponent,
     OverSpeedComponent,
     Driving4hComponent,
     StopComponent,
     SummaryByVehicleComponent,
     SummaryByDriverComponent,
     Driving10hComponent,
     DataTransmissionComponent,
     ReportTripComponent,
     ReportTripDetailComponent
    ]
})
export class ReportModule { }
