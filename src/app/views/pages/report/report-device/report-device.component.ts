import { Component, OnInit, ElementRef, ChangeDetectorRef, ViewChild, EventEmitter } from '@angular/core';
import { finalize, takeUntil, tap } from 'rxjs/operators';
import { Subject, observable } from 'rxjs';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { Device, DeviceTypeService } from '@core/admin';
import { Device, DeviceService, User } from '@core/manage';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { HttpClient } from '@angular/common/http';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { TranslateService } from '@ngx-translate/core';
import { ToastService, UserDatePipe } from '@core/_base/layout';
import { DeviceUtilityService } from '@core/manage/utils/device-utility.service';
import { ExcelService } from '@core/utils';
import { XLSXModel } from '@core/utils/xlsx/excel.service';
import { headersToString } from 'selenium-webdriver/http';
import { RequestFileService } from '@core/common/_services/request-file.service';
import { RequestFile } from '@core/common/_models/request-file';
import { DeviceConfigService } from '@core/common/_services/device-config.service';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { ReportDeviceService } from '@core/report';
import { MapConfig, MapConfigModel } from '@core/utils/map';
import { DecimalPipe } from '@angular/common';
declare var $: any;
declare var ClipboardJS: any;

@Component({
  selector: 'kt-report-device',
  templateUrl: './report-device.component.html',
  styleUrls: ['./report-device.component.scss'],
  providers: [UserDatePipe, DecimalPipe]
})
export class ReportDeviceComponent implements OnInit {
  public dataTable: DataTable = new DataTable();
  public action: string;
  public currentReason: any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public formEdit: FormGroup;
  public formAdd: FormGroup;
  private tableConfig: any;
  private currentForm: FormGroup;
  public currentModel: Device;
  public commandsForm: FormGroup;
  public isShowSearchAdvanced: boolean;
  public deviceType: any[];
  public simType: any[];
  public groupDevice: any[];
  public parentBread = [
    {
        name: this.translate.instant('REPORT.REPORTS.GENERAL.REPORTS'),
        translate: "REPORT.REPORTS.GENERAL.REPORTS",
        link:"/reports",
        icon:"flaticon2-graph"
    }
 ];
  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  private userIdSelecte: number;
  public currentUser: User;
  public dateTimeServer: string;
  // feature device  
  public showUserTree = true;
  public allowReOrder: boolean = false;
  // permisison 
  public permissions: any = {
    sell: "device.action.sell"
  }
  public mapConfig: MapConfigModel;

  public statusExport: {
    all: number, lostGPS: number, lostGPRS: number, historyTransfer: number, expired: number, stop: number,
    run: number, inactive: number, nodata: number, lostSignal: number
  };
  constructor(
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private deviceService: DeviceService,
    private currentService: DeviceService,
    private translate: TranslateService,
    private toast: ToastService,
    private deviceUtility: DeviceUtilityService,
    private xlsx: ExcelService,
    private requestFile: RequestFileService,
    private deviceConfig: DeviceConfigService,
    private userTreeService: UserTreeService,
    private reportDeviceService: ReportDeviceService,
    private userDate: UserDatePipe,
    private decimalPipe: DecimalPipe,
  ) {
    this.action = "";
    this.tableConfig = {
      pagination: [
        10, 20, 30, 50
      ],
      columns: [
        {
          title: '#',
          field: 'id',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '20px' },
          width: 20,
          class: 't-datatable__cell--center',
          translate: '#',
          autoHide: false,
        },
        {
          title: 'Tài khoản',
          field: 'userName',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px' },
          width: 100,
          class: '',
          translate: 'COMMON.COLUMN.USERNAME',
          autoHide: false,
        },
        {
          title: 'IMEI',
          field: 'imei',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '125px' },
          width: 125,
          class: '',
          translate: 'COMMON.COLUMN.IMEI',
          autoHide: true,
        },
        {
          title: 'VIN',
          field: 'VIN',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '125px' },
          width: 125,
          class: '',
          translate: 'COMMON.COLUMN.VIN',
          autoHide: true,
        },
        {
          title: 'Name',
          field: 'name',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.NAME',
          autoHide: true,
        },
        {
          title: 'Loai',
          field: 'typeName',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px' },
          width: 100,
          class: '',
          translate: 'COMMON.COLUMN.TYPE',
          autoHide: true,
        },
        {
          title: 'address',
          field: 'address',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '160px' },
          class: '',
          width: 160,
          translate: 'COMMON.COLUMN.ADDRESS',
          autoHide: true,
        },
        {
          title: 'battery',
          field: 'batteryStr',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '110px' },
          class: '',
          width: 110,
          translate: 'COMMON.COLUMN.BATTERY',
          autoHide: true,
        },
        {
          title: 'status',
          field: 'statusStr',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '110px' },
          class: '',
          width: 110,
          translate: 'COMMON.COLUMN.STATUS_CAR',
          autoHide: true,
        },
        {
          title: 'Trạng thái',
          field: 'active',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '70px' },
          class: '',
          width: 70,
          translate: 'COMMON.COLUMN.ACTIVE',
          autoHide: true,
        },
      ]
    };
    this.unsubscribe = new Subject();
    this.mapConfig = new MapConfig().configs();
    this.dataTable.init({
      data: [],
      totalRecod: 0,
      paginationSelect: [1, 2, 5, 10, 20],
      columns: this.tableConfig.columns,
      formSearch: '#formSearch',
      pageSize: 10,
      isDebug: true,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: true,
      }
    });
    this.currentReason = {};
    this.isShowSearchAdvanced = false;
    this.deviceType = [];
    this.simType = [];
    this.groupDevice = [];
    this.parrentEmiter = new EventEmitter();
    this.currentUser = new User();
    this.dateTimeServer = "";
  }
  ngOnInit() {

    this.getAllConfig();
    // listening dataTable event
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.getData(option);
      })
    ).subscribe();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();

    // this.dataTable.reload({});

    // this.createFormEdit();
    $(function () {
      $('.kt_selectpicker').selectpicker();
      $('.kt_datepicker').datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: "bottom left",
        templates: {
          leftArrow: '<i class="la la-angle-left"></i>',
          rightArrow: '<i class="la la-angle-right"></i>'
        },
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true
      });
    });
  }

	/**
	 * Get list by data option
	 * @param option option search list
	 */
  private getData(option) {
    if (option['orderBy'] == undefined || option['orderBy'].length == 0) {
      option['orderBy'] = "sortOrder";
      option['orderType'] = "asc";
    }
    this.reportDeviceService.list({ params: option }).pipe(
      tap((data: any) => {
        this.dateTimeServer = data.datetime;
        data.result.content = this.processDevice(data.result.content, this.dateTimeServer);
        this.dataTable.update({
          data: data.result.content,
          totalRecod: data.result.totalRecord
        });
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  getDataSelected() {
    if (this.dataTable.dataSelected.length == 0) {
      this.toast.show({ translate: "MANAGE.DEVICE.MESSAGE.NO_DEVICE_SELECTED" });
    }
    return this.dataTable.dataSelected;
  }

  get formArr(): FormArray {
    if (this.commandsForm) {
      return this.commandsForm.get('itemRows') as FormArray;
    }
  }

  toggleSearchAdvanced() {
    this.isShowSearchAdvanced = !this.isShowSearchAdvanced;
  }

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.currentForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
  /**
   * Change size databale
   * @param $elm 
   */
  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();

  }

  search(data?: any) {
    let listSearch = [];

    if (this.userIdSelecte != undefined) {
      listSearch.push({ key: 'userId', value: this.userIdSelecte });
    }

    this.dataTable.search(data || listSearch);
  }
  resetFormSearch() {
    $('#formSearch .selectpicker').val('default').selectpicker("refresh");
    $('#formSearch .kt_datepicker').val("").datepicker("update");
    $("#formSearch :input").each(function () {
      switch (this.type) {
        case 'checkbox':
          $(this).prop('checked', false);
          break;
        default:
          $(this).val('');
          break;
      }
    });
    this.search([{ key: 'userId', value: this.userIdSelecte }, { key: 'orderBy', value: 'sortOrder' }, { key: 'orderType', value: 'asc' }]);
  }

  getAllConfig() {
    this.deviceConfig.get().then(x => {
      this.deviceType = x.deviceTypes;
      this.simType = x.simTypes;
      setTimeout(() => {
        $('.selectpicker-device-type').selectpicker("refresh");
        $('.selectpicker-sim-type').selectpicker("refresh");
      }, 100);
    });
  }

  getGroupDevice(userId: number) {

  }
  userTreeChangeUser(value) {
    if (value.id > 0) {
      this.userIdSelecte = value.id;
      this.currentUser = value;
      this.search();
    }
  }
  sendEvent() {
    this.parrentEmiter.emit({ id: 5024, path: '234,5305,235' });
  }
  copied(val) {
    this.toast.copied(val);
  }

  // feature export excel file
  exportXlSX(option?: { all: true }) {
    let _this = this;
    let data = [];
    if (option && option.all === true) {
      let apiSearch = this.dataTable.getDataSearch();
      apiSearch['pageNo'] = -1;
      if (this.dataTable.getPaginations()['total'] > 1000) {
        let requestFile = new RequestFile();
        requestFile.type = "DEVICE__LIST_DEVICE";
        requestFile.fileExtention = "XLSX";
        requestFile.params = apiSearch;
        this.requestFile.create(requestFile, { notifyGlobal: true }).subscribe();
      }
      else {
        this.reportDeviceService.list({ params: apiSearch }).pipe(
          tap((data: any) => {
            // data.result.content = 
            // this.processStatus(ata.result.content)
            this.dateTimeServer = data.datetime;
            let dataTemp = data.result.map(x => {
              x.activeStr = (x.active == "1" ? _this.translate.instant('COMMON.GENERAL.ACTIVE') : _this.translate.instant('COMMON.GENERAL.INACTIVE'));
              return x;
            });
            dataTemp = this.processDevice(dataTemp, this.dateTimeServer);
            this.processStatus(dataTemp);
            this.exportFileXLSX(dataTemp);
          }),
          takeUntil(this.unsubscribe),
          finalize(() => {
            this.cdr.markForCheck();
          })
        ).subscribe();
      }
    }
    else {
      let dataTemp = this.getDataSelected();
      if (dataTemp.length > 0) {
        dataTemp = dataTemp.map(x => {
          x.activeStr = (x.active == "1" ? _this.translate.instant('COMMON.GENERAL.ACTIVE') : _this.translate.instant('COMMON.GENERAL.INACTIVE'));
          return x;
        });
        this.exportFileXLSX(dataTemp);
      }
    }

  }
  exportPDF() {

  }
  private exportFileXLSX(data) {
    this.processStatus(data);
    const statusConfig = this.mapConfig.status;

    let config: XLSXModel = {
      file: {
        title: this.translate.instant('MANAGE.DEVICE.GENERAL.LIST_DEVICE'),
        prefixFileName: "ReportDevice",
        typeName:"ReportDevice",
      },
      header: [
        {
          text: this.translate.instant('REPORT.DEVICE.GENERAL.REPORT_DEVICE'),
          type: "header"
        },
        {
          text: "",
        },
        {
          text: "!timezone",
        },
        {
          text: ""
        },
        {
          text: this.translate.instant('COMMON.GENERAL.DATETIME') + ": " + this.userDate.transform(this.dateTimeServer, "datetime", null, "datetime"),
        },
        {
          text: ""
        },
      ],
      columns: [
        {
          name: "#",
          columnData: "auto",
          wch: 5
        }, {
          name: this.translate.instant('COMMON.COLUMN.USERNAME'),
          columnData: "userName",
          wch: 20,
        },
        {
          name: this.translate.instant('COMMON.COLUMN.IMEI'),
          columnData: "imei",
          wch: 20
        },
        {
          name: this.translate.instant('COMMON.COLUMN.VIN'),
          columnData: "VIN",
          wch: 20
        },
        {
          name: this.translate.instant('COMMON.COLUMN.UPDATE_TIME'),
          columnData: "timestampStr",
          wch: 20
        },
        {
          name: this.translate.instant('COMMON.COLUMN.NAME'),
          columnData: "name",
          wch: 25
        },
        {
          name: this.translate.instant('COMMON.COLUMN.TYPE'),
          columnData: "typeName",
          wch: 12
        },
        {
          name: this.translate.instant('COMMON.COLUMN.POWER'),
          columnData: "power",
          wch: 15,
          // type: "date",
        },
        {
          name: this.translate.instant('COMMON.COLUMN.BATTERY'),
          columnData: "batteryStr",
          wch: 15,
          // type: "date",
        },
        {
          name: this.translate.instant('COMMON.COLUMN.SIGNAL'),
          columnData: "signal",
          wch: 15,
          // type: "date",
        },
        {
          name: this.translate.instant('COMMON.COLUMN.STATUS_CAR'),
          columnData: "statusStr",
          wch: 20,
        },
        {
          name: this.translate.instant('COMMON.COLUMN.ACTIVE'),
          columnData: "activeStr",
          wch: 15,
        },
        {
          name: this.translate.instant('COMMON.COLUMN.ADDRESS'),
          columnData: "address",
          wch: 40,
          // type: "date",
        },
      ],
      woorksheet: {
        name: this.translate.instant('MANAGE.DEVICE.GENERAL.DEVICE'),
      }
    };
    let headers = [];
    let _this = this;
    let text = "";

    Object.keys(this.statusExport).forEach((v) => {

      if (_this.statusExport[v] > 0) {
        switch (v) {
          case "all":
            break;
          default:
            text = _this.translate.instant(statusConfig[v].text) + ": " + _this.statusExport[v];
            headers.push({ text: text, style: { font: { bold: true } } });
            break;
        }
      }
    });
    text = _this.translate.instant('COMMON.GENERAL.ALL') + ": " + this.statusExport.all;
    headers.push({ text: text, style: { font: { bold: true } } });
    headers.push({ text: "" });
    headers.forEach(x => {
      config.header.push(x);
    });

    this.xlsx.exportFile(data, config, {});
  }
  private processStatus(devices) {
    let status = {
      all: devices.length, lostGPS: 0, lostGPRS: 0, historyTransfer: 0, expired: 0, stop: 0,
      run: 0, inactive: 0, nodata: 0, lostSignal: 0
    };
    devices.map(x => {

      //status 
      switch (x.status) {
        case "lost_gps":
          status.lostGPS++;
          break;
        case "lost_gprs":
          status.lostGPRS++;
          break;
        case "history_transfer":
          status.historyTransfer++;
          break;
        case "expired":
          status.expired++;
          break;
        case "stop":
          status.stop++;
          break;
        case "run":
          status.run++;
          break;
        case "inactive":
          status.inactive++;
          break;
        case "nodata":
          status.nodata++;
          break;

        default:
          status.lostSignal++;
          break;
      }
      return x;
    });
    this.statusExport = status;
  }

  private processDevice(devices, dateTimeServer) {
    let status = {
      all: devices.length, lostGPS: 0, lostGPRS: 0, historyTransfer: 0, expired: 0, stop: 0,
      run: 0, inactive: 0, nodata: 0, lostSignal: 0
    };
    const statusConfig = this.mapConfig.status;
    let dataResult = devices.map(x => {
      x['address'] = x['address'] || "";
      x['typeName'] = x['type']['name'] || "";
      x['power'] = this.translate.instant('COMMON.GENERAL.POWER_ENGINE');
      if (x['battery']['isAvailable']) {
        // có pin
        if (x['battery']['isCharged']) {
          x['batteryStr'] = this.translate.instant('COMMON.GENERAL.CHANGED') + " " + (x['battery']['percent'] || 0) + "%";
        }
        else {
          x['batteryStr'] = (x['battery']['percent'] || 0) + "%";
          x['power'] = this.translate.instant('COMMON.GENERAL.BATTERY');
        }
      }
      else {
        // ko có pin
        x['batteryStr'] = this.translate.instant('COMMON.GENERAL.NO_BATTERY');
      }
      x['timestampStr'] = this.userDate.transform(x['timestampUTC'], 'datetime');
      //status
      x['signal'] = "OFF";
      switch (x.status) {
        case "lost_gps":
          x.statusType = statusConfig.lostGPS;
          x.statusStr = this.translate.instant(statusConfig.lostGPS.text);
          status.lostGPS++;
          break;
        case "lost_gprs":
          x.statusType = statusConfig.lostGPRS;
          x.statusStr = this.translate.instant(statusConfig.lostGPRS.text);
          status.lostGPRS++;
          break;
        case "history_transfer":
          x.statusType = statusConfig.historyTransfer;
          x.statusStr = this.translate.instant(statusConfig.historyTransfer.text);
          status.historyTransfer++;
          break;
        case "expired":
          x.statusType = statusConfig.expired;
          x.statusStr = this.translate.instant(statusConfig.expired.text);
          status.expired++;
          break;
        case "stop":
          x.statusType = statusConfig.stop;
          x.statusStr = this.translate.instant(statusConfig.stop.text);
          status.stop++;
          x['signal'] = "ON";
          break;
        case "run":
          x.statusType = statusConfig.run;
          x.statusStr = this.translate.instant(statusConfig.run.text) + " - " + this.decimalPipe.transform(x.speed || 0, '1.0-2') + " km/h";
          status.run++;
          x['signal'] = "ON";
          break;
        case "inactive":
          x.statusType = statusConfig.inactive;
          x.statusStr = this.translate.instant(statusConfig.inactive.text);
          status.inactive++;
          break;
        case "nodata":
          x.statusType = statusConfig.nodata;
          x.statusStr = this.translate.instant(statusConfig.nodata.text);
          status.nodata++;
          break;

        default:
          x.statusType = statusConfig.lostSignal;
          x.statusStr = this.translate.instant(statusConfig.lostSignal.text);
          status.lostSignal++;
          break;
      }
      // ON/OFF
      return x;
    });
    this.statusExport = status;
    return dataResult;

  }
}


