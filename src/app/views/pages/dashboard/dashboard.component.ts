
// Angular
import { Store, select } from '@ngrx/store';
import { AppState } from '../../../../../src/app/core/reducers';
import { Observable } from 'rxjs';
import { currentUser, Logout } from '@core/auth';
import { User } from '@core/manage';
import { UserManageService } from '@app/core/manage/_service/user-manage.service';
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Component, OnInit,ViewChild, ChangeDetectorRef,EventEmitter, Output } from '@angular/core';
// chart js
import { MultiDataSet, Label, PluginServiceGlobalRegistrationAndOptions,BaseChartDirective} from 'ng2-charts';
import { ChartType,ChartOptions,ChartDataSets } from 'chart.js';
// Translate
import { TranslateService } from '@ngx-translate/core';
//color config
import { MapConfig, MapConfigModel } from '@core/utils/map';
//alert number
import { EventEmitterService } from '@app/core/common/_service/eventEmitter.service';
import { PageService } from '@core/_base/layout';
// import { UserDatePipe } from '@core/_base/layout';
import { UserDateService } from '@core/_base/layout';// am4charts
import * as moment from 'moment';

import { element } from 'protractor';
declare var $: any;

@Component({// am4charts
	selector: 'kt-dashboard',// am4charts
	templateUrl: './dashboard.component.html',
	styleUrls: ['dashboard.component.scss'],

})
export class DashboardComponent implements OnInit {
	@ViewChild('mycanvas',{static:false}) chart: BaseChartDirective;  //<------This is important
 
	user$: Observable<User>;
	public statusClose : boolean = false;
	public menuDashBoard:any;
	public infoParent:any;
	public params:any = [];
	private unsubscribe: Subject<any>;
	public paramUser:any = [];
	public doughnutOptions: ChartOptions;
	public doughnutChartLabels: Label[];
	public doughnutChartData: MultiDataSet = [];
	public doughnutChartColor: any ;
	public doughnutChartType: ChartType;
	public totalDevices:any;
	public doughnutChartPlugins: PluginServiceGlobalRegistrationAndOptions[];
	public listLable: any;
	//active/inactive
	public deviceActive:number;
	public listOnOff: any;
	public listLabelOnoff : any;
	//sell/stock
	public listSell : any;
	public listSellLabel:any;
	//color
	public colorOnline : string = '#0abb87';
	public colorOffline : string = '#1f292f';
	public colorInactive : string = '#d8d8d8';
	public colorExpired : string = '#4e1d0e';
	public colorSell : string = "rgb(40, 97, 80)";
	public colorNoSell : string = "rgb(171, 107, 128)";
	// map config
	public mapConfig: MapConfigModel;
	//number alert
	public numberAlert : number = 0;
	// data
	public data:any;
	//chart 10
	// public renderConfigChart:any;
	// config char
	public ConfigChart:any; 
	public ConfigChartIdling:any; 
	public ConfigChartDistance:any;
	public settingDashBoard = [
		{
			type:"status",
			name:"DASHBOARD.STATUS",
			lists:[
				{
					id:"chart_online",
					name:"DASHBOARD.PARAMS.CHART_STATUS",
					permisson:"",
					checked:true
				},
				{
					id:"active",
					name:"DASHBOARD.PARAMS.ACTIVE",
					permisson:"",
					checked:true
				}
				
			]
		},
		{
			type:"staticic",
			name:"DASHBOARD.STATICIC",
			lists:[
				{
					id:"distances",
					name:"DASHBOARD.STATIC.TOP_10_DISTANCE",
					permisson:"",
					checked:true
				},
			
				{
					id:"idlings",
					name:"DASHBOARD.STATIC.TOP_10_IDLING",
					permisson:"",
					checked:true
				},
				{
					id:"stop",
					name:"DASHBOARD.STATIC.TOP_10_STOP",
					permisson:"",
					checked:true
				},
			]
		}
	];
	constructor(
		private store: Store<AppState>,
		private manageUser: UserManageService,
		private cdr: ChangeDetectorRef,
		private translate: TranslateService,
		private eventEmitterService: EventEmitterService,
		private page:PageService,
		private userDatePipe : UserDateService,

		) 
		{
			this.unsubscribe = new Subject();
			this.buildColor();
			this.menuDashBoard = this.page.getListPageDashboard();
			this.menuDashBoard[4].value = this.numberAlert;
			this.paramUser = this.page.getListPageUser();
		}

	ngOnInit(): void {

		if(localStorage.getItem("settingDashBoard") == null || localStorage.getItem("settingDashBoard") == undefined) 
		{
			this.setLocalStorageSettingDB();

		}else{
			this.settingDashBoard = this.getLocalStorageSettingDB();
		}
		
		// console.log(JSON.parse(localStorage.getItem('settingDashBoard'))''
		this.buildDoughnut();
		this.user$ = this.store.pipe(select(currentUser));
		this.eventEmitterService.alertChanged.subscribe((params) => {
			this.numberAlert = params;
		 });
	}

	setLocalStorageSettingDB()
	{ 
	
	 localStorage.setItem('settingDashBoard', JSON.stringify(this.settingDashBoard));
		
	}

	getLocalStorageSettingDB()
	{ 
			return JSON.parse(localStorage.getItem('settingDashBoard'));

	}

	checkDisplay(idCheckbox){
		let checked = true;
		this.settingDashBoard = this.getLocalStorageSettingDB();
		if(!this.settingDashBoard[0].lists.find(x => x.id === idCheckbox))
		{
			checked = this.settingDashBoard[1].lists.find(x => x.id === idCheckbox).checked;
		}else {
			checked = this.settingDashBoard[0].lists.find(x => x.id === idCheckbox).checked;
		}

		return checked;
	}

	checkCount(type){
		let count = 0;
		this.settingDashBoard = this.getLocalStorageSettingDB();

		// console.log(this.settingDashBoard);

		if(this.settingDashBoard.find(x => x.type == type))
		{			
			let list = this.settingDashBoard.find(x => x.type == type).lists;
			list.filter(item => {
				if(item.checked) count++;
			});			
		}		
		return count;
	}

	checkStatusClose(button)
	{
		this.statusClose = !this.statusClose;
	}

	changeCheckbox(idCheckbox){

		this.settingDashBoard = this.getLocalStorageSettingDB();
		this.settingDashBoard.map(box=>{
			box.lists  = box.lists.map(item => {
				if(item.id == idCheckbox) item.checked = !item.checked;
				return item;
			})
			return box;
		})

		this.setLocalStorageSettingDB();

	}

	buildColor()
	{
		this.mapConfig = new MapConfig().configs();
		this.colorOnline = this.mapConfig.status.online.colorCode;
		this.colorOffline = this.mapConfig.status.lostSignal.colorCode;
		this.colorInactive = this.mapConfig.status.inactive.colorCode;
		this.colorExpired = this.mapConfig.status.expired.colorCode;
		this.colorSell = this.mapConfig.status.sell.colorCode;
		this.colorNoSell = this.mapConfig.status.noSell.colorCode;
	}

	sortObjectByValue(obj, key) {
		obj.sort((a, b) => (a[key] > b[key]) ? 1 : ((b[key] > a[key]) ? -1 : 0));
		obj = obj.reverse();
		return obj;
	  }

	buildDoughnut(){

		this.manageUser.getInfoParent({})
		.pipe(
			takeUntil(this.unsubscribe),
			finalize(() => {
			  this.cdr.markForCheck();
			})
		  )
        .subscribe((data: any) => {
		  if (data.status == 200) {
			data.result.top_10_stops = this.sortObjectByValue(data.result.top_10_stops,'duration');
			data.result.top_10_idlings = this.sortObjectByValue(data.result.top_10_idlings,'stopEngineOnTime');
			data.result.top_10_distances = this.sortObjectByValue(data.result.top_10_distances,'distance');

			 this.ConfigChart = {
				 data:data.result.top_10_stops,
				 type:'stop',
				 background:'#ea5228'
			 } as chartModel;
			 this.ConfigChartIdling = {
				data:data.result.top_10_idlings,
				type:'idling',
				background:'#ff9e00'
			 } as chartModel;
			 this.ConfigChartDistance = {
				data:data.result.top_10_distances,
				type:'distance',
				background:'rgb(152, 202, 2)'
			 } as chartModel;
			 
			  //end render chart
			  this.totalDevices = data.result.device.total;
			  let sell = data.result.device.isSell;
			  let noSell = this.totalDevices - sell;
			  let stock = data.result.device.stock;
			  let online = data.result.device.online;
			  let active = data.result.device.active;
			  let inactive = data.result.device.inactive;
			  let offline = data.result.device.offline;
			  let expired = data.result.device.expired;
			  let run = data.result.device.run;
			  let lostGps = data.result.device.lostGps;
			  let idling = data.result.device.idling;

			  let stop =  data.result.device.stop + idling;
			  this.infoParent = [data.result.parent];
			  this.numberAlert = data.result.alert;
			  this.deviceActive = active;
			  this.data = [
				{
				  pie1: this.translate.instant('DASHBOARD.PARAMS.ONLINE'),
				  value1: online,
				  pie2:this.translate.instant('COMMON.LIST_DEVICE.RUN'),
				  value2:run,
				},
				{
				  pie1: this.translate.instant('DASHBOARD.PARAMS.OFFLINE'),
				  value1: offline,
				  pie2:this.translate.instant('COMMON.LIST_DEVICE.STOP'),
				  value2:stop,
				},
				{
				  pie2:this.translate.instant('COMMON.LIST_DEVICE.EXPIRED'),
				  value2:expired,
				},
				 {
				  pie2:this.translate.instant('COMMON.LIST_DEVICE.LOST_SIGNAL'),
				  value2:lostGps,
				},
				{
				  pie2:this.translate.instant('DASHBOARD.PARAMS.OFFLINE'),
				  value2:offline
				}
			  ];
	

			this.listLable = [
				// {
				// 	title : 'DASHBOARD.PARAMS.ONLINE',
				// 	value : online,
				// 	style : {'background-color':this.colorOnline}
				// },
				{
					title : 'COMMON.LIST_DEVICE.RUN',
					value : run,
					style : {'background-color':'rgb(152, 202, 2)'}
				},
				{
					title : 'COMMON.LIST_DEVICE.STOP',
					value : stop,
					style : {'background-color':'rgb(234, 82, 40)'}
				},
				{
					title : 'COMMON.LIST_DEVICE.EXPIRED',
					value : expired,
					style : {'background-color':'rgb(78, 29, 14)'}
				},
				{
					title : 'COMMON.LIST_DEVICE.LOST_SIGNAL',
					value : lostGps,
					style : {'background-color':'#6C3483'}
				},
				{
					title : 'DASHBOARD.PARAMS.OFFLINE',
					value : offline,
					style : {'background-color':'#1f292f'}
				}
			];

			// chart active and inactive

			this.listOnOff = [
				{
					title: this.buildRatio(active,this.totalDevices)+"%",
					style : {
						'background-color':this.colorOnline,
						'width':this.buildRatio(active,this.totalDevices)+"%"
					}
				},
				{
					title:100-this.buildRatio(active,this.totalDevices)+"%",
					style : {
						'background-color': this.colorInactive,
						'width':100-this.buildRatio(active,this.totalDevices)+"%"
					}
				}
			];

			this.listLabelOnoff = [
				{
					title : 'DASHBOARD.PARAMS.ACTIVE',
					value : active,
					style : {
						'background-color':this.colorOnline,
					}
				},
				{
					title :'DASHBOARD.PARAMS.INACTIVE',
					value : inactive,
					style : {
						'background-color':this.colorInactive,
					}
				}
			];

			// chart sell/stock


			this.listSell = [
				{
					title: this.buildRatio(sell,this.totalDevices)+"%",
					style : {
						'background-color':this.colorSell,
						'width':this.buildRatio(sell,this.totalDevices)+"%"
					}
				},
				{
					title:100-this.buildRatio(sell,this.totalDevices)+"%",
					style : {
						'background-color': this.colorNoSell,
						'width':100-this.buildRatio(sell,this.totalDevices)+"%"
					}
				}
			];

			this.listSellLabel = [
				{
					title : 'DASHBOARD.PARAMS.SELL',
					value : sell,
					style : {
						'background-color':this.colorSell,
					}
				},
				{
					title :'DASHBOARD.PARAMS.STOCK',
					value : noSell,
					style : {
						'background-color':this.colorNoSell,
					}
				}
			];
			
			this.doughnutChartPlugins = [];
		  }
		});
		

		this.doughnutChartType = 'pie';

	}

	buildRatio(ele,total){
		if(total == 0) return 0;
		return Math.ceil(ele*100/total);
	}

}

export class chartModel{
	data:any;
	type:string;
	showNumber?:boolean;
	background?:string;
} 