// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { DashboardComponent } from './dashboard.component';
import { TranslateModule } from '@ngx-translate/core';
import { ChartsModule } from 'ng2-charts';
import { NgbDropdownModule, NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgxPermissionsModule } from 'ngx-permissions';
//chart
import { ChartDBComponent } from './charts-db/charts-db.component';
import { ChartStatusComponent } from './chart-status/chart-status.component';

import { NgSelectModule } from '@ng-select/ng-select';
import { ModuleGuard } from '@core/auth';

@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		CoreModule,
		ChartsModule,
		NgbModule,
		NgbDropdownModule,
		NgSelectModule,
		RouterModule.forChild([
			{
				path: '',
				component: DashboardComponent,
				canActivate:[ModuleGuard]
			},
			
		]),
		NgxPermissionsModule.forChild(),
		TranslateModule
	],
	providers: [
	],
	declarations: [
		DashboardComponent,
		ChartDBComponent,
		ChartStatusComponent
	]
})
export class DashboardModule {	
}
