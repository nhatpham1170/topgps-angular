import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { MultiDataSet, Label, PluginServiceGlobalRegistrationAndOptions, BaseChartDirective } from 'ng2-charts';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { UserManageService } from '@app/core/manage/_service/user-manage.service';
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';

// Translate
import { TranslateService } from '@ngx-translate/core';
declare var $;
import * as moment from 'moment';

@Component({
  selector: 'kt-charts-db',
  templateUrl: './charts-db.component.html',
  styleUrls: ['./charts-db.component.scss'],
})
export class ChartDBComponent implements OnInit {
  public barChartLabels: Label[] = [];
  public barChartOptions: ChartOptions;
  public barChartData: ChartDataSets[];
  public data: any;
  public loading: boolean = false;
  @Input() ConfigChart: any;
  @Output() onChangeDate = new EventEmitter<string>();
  @ViewChild('datePicker', { static: true }) datePicker: DateRangePickerComponent;

  public datePickerOptions: any = {
    startDate: moment().subtract(1, 'days'),
    select: 'from',
    autoSelect: true,
    singleDatePicker: true,
    timePicker: true,
  };
  	// number column
	public maxColumn:number = 5;
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public pieChartColors = [
    {
      backgroundColor: [],
    },];
  public customDate: any = [];
  public labelActive: string = "Today";
  private unsubscribe: Subject<any>;
  public label_y: string = '';
  private fieldConfig = [
    {
      type: 'stop',
      label: "DASHBOARD.STATIC.TOP_10_STOP",
      textUnit: this.translate.instant('DASHBOARD.STATIC.DURATION'),
      label_y: 'DASHBOARD.STATIC.DURATION_LABEL',
      field: 'top_10_stops',
      column: "duration"
    },
    {
      type: 'idling',
      label: "DASHBOARD.STATIC.TOP_10_IDLING",
      textUnit: this.translate.instant('DASHBOARD.STATIC.DURATION'),
      label_y: 'DASHBOARD.STATIC.IDLING_LABEL',

      field: 'top_10_idlings',
      column: "stopEngineOnTime"
    },
    {
      type: 'distance',
      label: "DASHBOARD.STATIC.TOP_10_DISTANCE",
      textUnit: this.translate.instant('DASHBOARD.STATIC.DISTANCE'),
      label_y: 'DASHBOARD.STATIC.DISTANCE_LABEL',

      field: 'top_10_distances',
      column: "distance"
    }
  ];
  public chartCurrent;
  public count: number = 0;
  public allowNext: boolean = true;
  public allowPrevious: boolean = true;
  public showNumber: boolean = false;
  constructor(
    private manageUser: UserManageService,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,

  ) {
  }
  ngOnInit() {
    this.renderChartCurrent();
    this.renderConfigChartFunction();
  }

  renderBackgroundColor() {
    let bg;
    if (this.ConfigChart.background) bg = this.ConfigChart.background;
    bg.forEach(element => {

    });
  }

  renderChartCurrent() {
    this.chartCurrent = this.fieldConfig.find(x => x.type === this.ConfigChart.type);
    this.datePickerOptions.key = this.chartCurrent.type;
  }

  async dateSelectChange(data1) {
    if(data1.startDate == null) return;
    
    this.loading = true;
    this.onChangeDate.emit(data1.startDate.format("YYYY-MM-DD"));
    let params = {
      type: this.chartCurrent.type,
      date: data1.startDate.format("YYYY-MM-DD")
    };
    if (this.count == 1) {
      await this.manageUser.getInfoParent(params)
        .pipe(
          debounceTime(2000),
          tap((data: any) => {
            if (data.status == 200) {
              //render chart
              let field = this.chartCurrent.field;
              this.ConfigChart.data = data.result[field];
              this.ConfigChart.data = this.sortObjectByValue(this.ConfigChart.data, field);
              this.ConfigChart.data = this.lengthDataChart(this.ConfigChart.data);

              this.loading = false;
            }
          }),
          finalize(() => {
            this.cdr.markForCheck();
          })
        ).toPromise();
      this.renderConfigChartFunction();
    }

    if (this.count == 0) {
      this.count = 1;
      this.loading = false;
    }

  }

  loadCustomDate() {

    this.customDate = [
      {
        id: 1,
        label: 'Today',
        startDate: moment().startOf('day').format("YYYY-MM-DD HH:mm:ss"),
        endDate: moment().endOf('day').format("YYYY-MM-DD HH:mm:ss")
      },
      {
        id: 2,
        label: 'Yesterday',
        startDate: moment().subtract(1, 'days').startOf('day').format("YYYY-MM-DD HH:mm:ss"),
        endDate: moment().subtract(1, 'days').endOf('day').format("YYYY-MM-DD HH:mm:ss")
      },
      {
        id: 3,
        label: 'Last 7 Days',
        startDate: moment().subtract(6, 'days').startOf('day').format("YYYY-MM-DD HH:mm:ss"),
        endDate: moment().endOf('day').format("YYYY-MM-DD HH:mm:ss")
      },
    ];
  }

  setDate(id) {
    let obj = this.customDate.find(x => x.id === id);
    this.labelActive = obj.label;
    this.onChangeDate.emit(obj);
  }


  previousDate() {
    this.datePicker.previousDay();
  }

  nextDate() {
    this.datePicker.nextDay();
  }

  cropString(text) {
    let textLaster = text;
    if (text.length >= 15) {
      textLaster = text.substring(0, 15) + "...";
    }
    return textLaster;
  }

  sortObjectByValue(obj, key) {
    obj.sort((a, b) => (a[key] > b[key]) ? 1 : ((b[key] > a[key]) ? -1 : 0));
    obj = obj.reverse();
    return obj;
  }

  lengthDataChart(dataConfig)
  {
    if(dataConfig.length >= this.maxColumn)
    {
      dataConfig = dataConfig.slice(0,this.maxColumn);
    }

    if(dataConfig.length > 0 && dataConfig.length < this.maxColumn)
    {
      let numberInsert = this.maxColumn - dataConfig.length;
      for (let i = 0; i < numberInsert; i++) {
        dataConfig.push({name:'_'});
      }
    }
    return dataConfig;
  }

  getDataStep(maxData)
  {
    let dataStep = 0;
    dataStep = Math.round(maxData)/4;
    dataStep = Math.ceil(dataStep / 10) * 10;
    return dataStep;
  }

  renderConfigChartFunction() {
    let dataConfig = this.ConfigChart.data;
    dataConfig = this.lengthDataChart(dataConfig);
    if (this.ConfigChart.showNumber) {
      this.showNumber = this.ConfigChart.showNumber;
    }
    let data = [];
    let dataStep = 0;
    let maxData = 0;
    let label = this.chartCurrent.label;
    let textUnit = this.chartCurrent.textUnit;
    this.label_y = this.chartCurrent.label_y;
    this.barChartLabels = [];
    let listDevices = [];
    let index = 0;
    dataConfig.forEach(element => {
      index++;
      let count1 = index.toString();
      // set number
      if (this.showNumber) {
        this.barChartLabels.push(count1);
      }
      else {
        if(dataConfig.length > 1) this.barChartLabels.push(this.cropString(element.name));
        if(dataConfig.length <= 1) this.barChartLabels.push(element.name);
      }
      let duration = element[this.chartCurrent.column];
      if(duration > maxData) maxData = duration;
      if(duration > 0 )  data.push(duration);
     
      listDevices.push(element.name);
      // set background
      if (this.ConfigChart.background) {
        this.pieChartColors[0].backgroundColor.push(this.ConfigChart.background);
      }

    });
    dataStep = this.getDataStep(maxData);
    maxData = dataStep*5;
    //options
    var _this = this;
    this.barChartOptions = {
      legend: {
        position: 'left',
        display: false,
        reverse: false
      },
      // title: {
      // 	display: true,
      // 	position : 'left'
      // },
      responsive: true,

      scales: {
        xAxes: [{
          gridLines: {
            drawTicks: false
          },
          ticks: {
            padding: 10,

          }
        }],
        yAxes: [{
          gridLines: {
            drawTicks: false
          },
          ticks: {
            lineHeight: 2,
            padding: 10,
            min: 0,
            max: 86400,
            stepSize: 21600,
            callback: function (label:number, index, labels, ) {
              var hours = Math.floor(label / 3600).toString();
              var minutes = (Math.floor(label / 60) % 60).toString();
              var output = hours;
              // var output  = hours + ' h ' + minutes + ' min';

              return output;
            }
          }

        }]
      },

      tooltips: {
        displayColors: false,
        xPadding: 15,
        yPadding: 15,
        titleFontSize: 14,
        titleFontColor: "#48465b",
        backgroundColor: '#fff',
        bodyFontColor: "#48465b",
        borderColor: "#48465b",
        borderWidth: 2,
        callbacks: {
          title: function (tooltipItem, data) {
            return listDevices[tooltipItem[0].index];
          },
          label: function (tooltipItem, data) {
            let value = tooltipItem.value;
            // let output = this.convertNumberToTime(value);
            var hours = Math.floor(parseInt(value) / 3600);
            
            var minutes = Math.floor(parseInt(value) / 60) % 60;
            var seconds = parseInt(value) - (hours*3600 + minutes*60) ;
            var output = hours + ' '+ _this.translate.instant('DASHBOARD.STATIC.HOURS')+' '+ minutes + ' '+_this.translate.instant('DASHBOARD.STATIC.MIN');
            if(_this.chartCurrent.type == 'idling')
            {
              var minutes = Math.floor(parseInt(value) / 60);
              output = minutes + ' '+_this.translate.instant('DASHBOARD.STATIC.MIN')+' '+ seconds + ' '+_this.translate.instant('DASHBOARD.STATIC.SECONDS');
            }
            return textUnit + ": " + output;
          }
        },
        titleAlign: 'center',
      },

    };
      // type idling
      if (this.chartCurrent.type == 'idling') {
        if(dataStep < 60) dataStep = 60;
        maxData = dataStep*5;
        // console.log(dataStep);
        // console.log(maxData);

        this.barChartOptions.scales.yAxes[0].ticks = {
          lineHeight: 2,
          padding: 10,
          min: 0,
          max:maxData,
          stepSize: dataStep,
          callback: function (label:number, index, labels, ) {
            var output = '';
            var minutes = Math.floor(Number(label) / 60).toString();
      
            output = minutes;
            // var output  = hours + ' h ' + minutes + ' min';

            return output;
          }
        };
      }
        // end type idling  
    // type distance
    if (this.chartCurrent.type == 'distance') {

      this.barChartOptions.scales.yAxes[0].ticks = {
        padding: 10,
        max:maxData,
        callback: function (label, index, labels, ) {
          return label;
        }
      };
      this.barChartOptions.scales.yAxes[0].ticks.stepSize = dataStep;
      this.barChartOptions.tooltips = {
        displayColors: false,
        xPadding: 15,
        yPadding: 15,
        titleFontSize: 14,
        titleFontColor: "#48465b",
        backgroundColor: '#fff',
        bodyFontColor: "#48465b",
        borderColor: "#48465b",
        borderWidth: 2,
        callbacks: {
          title: function (tooltipItem, data) {
            return listDevices[tooltipItem[0].index];
          },
          label: function (tooltipItem, data) {
            let value = parseInt(tooltipItem.value);
            value = Math.round(value);
            var output = value + ' km';
            return textUnit + ": " + output;
          }
        },
        titleAlign: 'center',
      };
    }
    // view defaut yAxes (data empty)
    if (dataConfig.length == 0) {
      let min = 0;
      let max = 0;
      let stepSize = 0;
      if (this.chartCurrent.type == 'stop') {
        min = 0;
        max = 86400;
        stepSize = 21600;
      }
      if (this.chartCurrent.type == 'idling') {
        min = 0;
        max = 3600;
        stepSize = 900;
      }
      if (this.chartCurrent.type == 'distance') {
        min = 0;
        max = 20;
        stepSize = 5;
      }
      this.barChartOptions.scales.yAxes[0].ticks.min = min;
      this.barChartOptions.scales.yAxes[0].ticks.max = max;
      this.barChartOptions.scales.yAxes[0].ticks.padding = 10;

      this.barChartOptions.scales.yAxes[0].ticks.stepSize = stepSize;
      this.barChartOptions.scales.yAxes[0].gridLines.drawOnChartArea = false;
    };
    // data	  
    this.data = data;
    this.barChartData =
      [
        {
          data: data,
          label: label
        },
      ];
  }


}