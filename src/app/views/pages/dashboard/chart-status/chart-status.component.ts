import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { MultiDataSet, Label, PluginServiceGlobalRegistrationAndOptions, BaseChartDirective } from 'ng2-charts';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { TranslateService } from '@ngx-translate/core';
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import { ReportFuelService } from '@core/report/_services/report-fuel.service';
am4core.useTheme(am4themes_animated);
@Component({
	selector: 'kt-chart-status',
	templateUrl: './chart-status.component.html',
  styleUrls: ['./chart-status.component.scss'],
})
export class ChartStatusComponent implements OnInit {
    @Input() listLable: any;
    @Input() data: any;

    public timeStart:string;
    public timeEnd:string;
    public sensorIds:string;
    public deviceIds:string;
    public listChart:any = [];
    public chartData : any = [];
    public isLoading : boolean = true;
    private chart:any;
	constructor(
        // private unsubscribe: Subject<any>,
        private cdr: ChangeDetectorRef,
        private translateService: TranslateService
	) {
        // this.unsubscribe = new Subject();
    }

	ngOnInit(): void {   
        if(this.chart) this.chart.dispose();
        am4core.disposeAllCharts(); 
        // if(this.chart)   am4core.disposeAllCharts();
        this.renderChart();
    }
    
    ngOnDestroy():void {
        am4core.disposeAllCharts(); 

    }

    renderChart()
    {
       // Set theme
/**
 * --------------------------------------------------------
 * This demo was created using amCharts V4 preview release.
 *
 * V4 is the latest installement in amCharts data viz
 * library family, to be released in the first half of
 * 2018.
 *
 * For more information and documentation visit:
 * https://www.amcharts.com/docs/v4/
 * --------------------------------------------------------
 */

// Set theme
am4core.useTheme(am4themes_animated);

// Create chart instance
this.chart = am4core.create("chartdiv", am4charts.PieChart);

// Let's cut a hole in our Pie chart the size of 40% the radius
this.chart.innerRadius = am4core.percent(40);
this.chart.responsive.enabled = true;
// Add data
this.chart.data = this.data;
// console.log(this.data);

// Add and configure Series
var pieSeries = this.chart.series.push(new am4charts.PieSeries());
pieSeries.dataFields.value = "value1";
pieSeries.dataFields.category = "pie1";
pieSeries.propertyFields.fill = "color1";

pieSeries.slices.template.stroke = am4core.color("#fff");
pieSeries.slices.template.strokeWidth = 2;
pieSeries.slices.template.strokeOpacity = 1;
// pieSeries.colors.step = 2;
pieSeries.colors.list = [
    am4core.color('#0abb87'),
    am4core.color('#1f292f'),
]
// Disabling labels and ticks on inner circle
pieSeries.labels.template.disabled = true;
pieSeries.ticks.template.disabled = true;

// Add second series
var pieSeries2 = this.chart.series.push(new am4charts.PieSeries());
pieSeries2.dataFields.value = "value2";
pieSeries2.dataFields.category = "pie2";
pieSeries2.slices.template.stroke = am4core.color("#fff");
pieSeries2.slices.template.strokeWidth = 2;
pieSeries2.slices.template.strokeOpacity = 1;

pieSeries2.labels.template.disabled = true;
pieSeries2.ticks.template.disabled = true;

pieSeries2.colors.list = [
    am4core.color('rgb(152, 202, 2)'),
    am4core.color('rgb(234, 82, 40)'),
    am4core.color('rgb(78, 29, 14)'),
   am4core.color('#6C3483'),
   am4core.color('#1f292f'),

]
   

}

  async  drawChart()
    {

    }
    


}
