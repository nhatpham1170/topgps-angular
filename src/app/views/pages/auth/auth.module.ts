// Angular
import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
// Material
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule } from '@angular/material';
// Translate
import { TranslateModule } from '@ngx-translate/core';
// NGRX
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
// CRUD
import { InterceptService } from '../../../core/_base/crud/';
// Module components
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

import { AuthNoticeComponent } from './auth-notice/auth-notice.component';
// Auth
import { AuthEffects, AuthGuard, authReducer, AuthService,PermissionEffects,permissionsReducer, ModuleGuard } from '../../../core/auth';
import { LogoutComponent } from './logout/logout.component';
import { DeviceTypeService, SimTypeService } from '@core/admin';
// import { SimTypeService } from '../../../core/admin';
import { NgbDropdownModule, NgbTabsetModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { PartialsModule } from '@app/views/partials/partials.module';
import { LoginV1Component } from './login/layout/login-v1/login-v1.component';
import { LoginV2Component } from './login/layout/login-v2/login-v2.component';
import { LoginV3Component } from './login/layout/login-v3/login-v3.component';

const routes: Routes = [
	{
		path: '',
		component: AuthComponent,		
		children: [
			{
				path: '',
				canActivate:[ModuleGuard],
				redirectTo: 'login',
				pathMatch: 'full'
			},
			{
				path: 'login',
				canActivate:[ModuleGuard],
				component: LoginComponent,
				data: {returnUrl: window.location.pathname}
			},
			{
				path: 'reset-password',
				component: ResetPasswordComponent
			},
			{
				path: 'register',
				component: RegisterComponent
			},
			{
				path: 'forgot-password',
				component: ForgotPasswordComponent,
			},
			{
				path: 'logout',
				component: LogoutComponent,
			}
		]
	}
];


@NgModule({
	imports: [		
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MatButtonModule,
		RouterModule.forChild(routes),
		MatInputModule,
		MatFormFieldModule,
		MatCheckboxModule,
		NgbDropdownModule,
		PartialsModule,
		TranslateModule.forChild(),
		StoreModule.forFeature('auth', authReducer),
		StoreModule.forFeature('permissions', permissionsReducer),
		EffectsModule.forFeature([AuthEffects,PermissionEffects])
	],
	providers: [
		InterceptService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: InterceptService,
			multi: true
		},
		// SimTypeService,
		// DeviceTypeService
	],
	exports: [
		AuthComponent,
		
	],
	declarations: [
		AuthComponent,
		LoginComponent,
		RegisterComponent,
		ForgotPasswordComponent,
		AuthNoticeComponent,
		LogoutComponent,
		ResetPasswordComponent,
		LoginV1Component,
		LoginV2Component,
		LoginV3Component
	],

})

export class AuthModule {
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: AuthModule,
			providers: [
				AuthService,
				AuthGuard,
				ModuleGuard
			]
		};
	}
}
