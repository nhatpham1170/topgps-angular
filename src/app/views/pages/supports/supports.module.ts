import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { PartialsModule } from '@app/views/partials/partials.module';
import { RouterModule, Routes } from '@angular/router';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ModuleGuard, AuthGuard } from '@core/auth'; 
import { CoreModule } from '@core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ClipboardModule } from 'ngx-clipboard';

import { SupportsComponent } from './supports.component';
import { DebugComponent } from './debug/debug.component';
import { DeviceLoginComponent } from './device-login/device-login.component';
import { SimInfoComponent } from './sim-info/sim-info.component';
// import { LostSignalReportComponent } from './lost-signal/lost-signal.component';


const routes: Routes = [{
    path: '',
    component: SupportsComponent,
    data: {
        permissions: 'ROLE_report.supports'
    },
    children: [
     {
         path: 'debug',
         component: DebugComponent,
         canActivate: [ModuleGuard, AuthGuard],
         data: {
            permisison: 'ROLE_report.debug',
         }
     },
     {
        path: 'device-login',
        component: DeviceLoginComponent,
        canActivate: [ModuleGuard, AuthGuard],
        data: {
           permisison: 'ROLE_report.device_session_login',
        }
    },
    {
        path: 'sim-info',
        component: SimInfoComponent,
        canActivate: [ModuleGuard, AuthGuard],
        data: {
           permisison: 'ROLE_report.sim',
        }
    },
    // {
    //     path: 'lost-signal',
    //     component: LostSignalReportComponent,
    //     canActivate: [ModuleGuard, AuthGuard],
    //     data: {
    //        permisison: 'ROLE_report.lost_signal',
    //     }
    // }
    ]
  }]

@NgModule({
    imports: [
        CoreModule,
        CommonModule,
        RouterModule.forChild(routes),
        PartialsModule,
        NgxPermissionsModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        ClipboardModule
    ],
    providers: [ModuleGuard],
    declarations: [
        SupportsComponent,
        DebugComponent,
        DeviceLoginComponent,
        SimInfoComponent,
        // LostSignalReportComponent
    ],
})
export class SupportsModule { }