import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';

import { Subject } from 'rxjs';
import { tap, debounceTime, finalize } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ExcelService } from '@core/utils';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { ToastService, ValidatorCustomService, UserDateAdvPipe } from '@core/_base/layout';
import { SupportsService, Supports } from '@core/supports';
import { XLSXModel } from '@core/utils/xlsx/excel.service';

declare var $: any;
declare var ClipboardJS: any;

@Component({
  selector: 'kt-sim-info',
  templateUrl: './sim-info.component.html',
  styleUrls: ['./sim-info.component.scss'],
  providers: [ UserDateAdvPipe ]
})
export class SimInfoComponent implements OnInit {
  @ViewChild('datePicker', { static: true })

  public datePicker: DateRangePickerComponent;
  public searchFormSimLog: FormGroup;

  public datePickerOptions: any = {
    size: 'md',
    select: 'from',
    ranges: 'from',
    optionDatePicker: {
      maxSpan: {
        days: 31
      }
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: true,
  };

  public dateStart: string;
  public dateEnd: string;

  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public isdownLoad: boolean = false;
  public dataExport: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataDefault: any = [{}];
  public filter: any;
  private unsubscribe: Subject<any>;

  public closeResult: string;
  constructor(
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    private toast: ToastService,
    private xlsx: ExcelService,
    private modalService: NgbModal,
    private supportsService: SupportsService,
    private validatorCT: ValidatorCustomService,
    private userDateAdv: UserDateAdvPipe,
  ) {
    this.buildForm();
    this.initDataTable();
  }

  ngOnInit() {
    this.updateDataTable();
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;
        this.getData(this.filter);
      }),

    ).subscribe();
  }

  searchSimLog(form: any) {
    if (form.value.imei == '' || form.value.imei == undefined || form.value.imei == null) {
      this.toast.show(
        {
          message: this.translate.instant('MESSEAGE_CODE.DEVICE.IMEI_NOT_EMPTY'),
          type: 'info',
        });
        this.dataTable.isLoading = false;
      return;
    }
 
    this.filter = {};

    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;
    this.filter.imei = form.value.imei.trim();

    this.dataTable.reload({ currentPage: 1 });
  }

  resetFormSearch() {
    this.datePicker.refresh();
    this.searchFormSimLog.reset();
  }

  getData(filter: Supports) {
    this.dataTable.isLoading = true;
   
    this.supportsService.getSimInfoLogs({ params: filter }).pipe(
      tap(result => {
        if (result.status == 200) {
          // this.data = result.result.content;
          this.data = this.processDataPointsByDay(result.result.content); 
          this.totalRecod = result.result.totalRecord
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
        }
      }),
      debounceTime(1000),
      finalize(() => {
        this.dataTable.isLoading = false;
        this.cdr.markForCheck();
        this.cdr.detectChanges();
      })
    ).subscribe();
  }

  buildForm() {
    this.searchFormSimLog = this.formBuilder.group({
      dateStart: [''],
      dateEnd: [''],
      imei: ['', [this.validatorCT.required]]
    });
  }

  private processDataPointsByDay(data) {
    let dataResult = [];
    let dataObj = {};

    data.map(item => {
      // console.log(item);
      item['time'] = this.userDateAdv.transform(item.timestamp, "YYYY/MM/DD HH:mm:ss");
      item['timeUpdate'] = this.userDateAdv.transform(item.timestampUpdate, "YYYY/MM/DD HH:mm:ss");

      if(dataObj[this.userDateAdv.transform(item.timestamp, "YYYY/MM/DD")] == undefined){
        dataObj[this.userDateAdv.transform(item.timestamp, "YYYY/MM/DD")] = [];
      }
      dataObj[this.userDateAdv.transform(item.timestamp, "YYYY/MM/DD")].push(item);

      return item;
    });

     dataResult = Object.keys(dataObj).map(key=>{
      return {
        timeFrom: key,
        points:dataObj[key],
      }
    });

    // console.log('ok: ', dataResult);

    return dataResult;
  }

  exportFile(options: { type?: string }) {
    if(this.filter == undefined || this.filter.imei == '') 
    {
      this.toast.show({type:'info', translate:'COMMON.MESSAGE.PLEASE_ENTER_DEVICE'});
      return;
    }
    this.filter['pageNo'] = -1;
    this.isdownLoad = true;
    this.cdr.detectChanges();

    this.supportsService.getSimInfoLogs({params: this.filter}).pipe(
      tap(result=>{
        if(result.result.length == 0){
          this.toast.show({ type: "error", translate: 'COMMON.MESSAGE.NOT_FOUND_DATA', });
        }
        else{
          this.dataExport = this.processDataPointsByDay(result.result);
          let summary = {
            startTime: this.userDateAdv.transform(this.dateStart, "datetime",{valueOffset:0}),
            endTime: this.userDateAdv.transform(this.dateEnd, "datetime",{valueOffset:0}),
            imei: this.filter.imei

          }
          switch (options.type) {
            case "xlsx":
              // let data = this.processDataByDay(this.currentEvent.data.routes);
              this.exportFileXLSX(this.dataExport, summary);
              break;
            case "txt":
              
              break;
          }     
        }
      }),
      debounceTime(1000),
      finalize(() => {
        this.isdownLoad = false;
        this.cdr.detectChanges();
        this.cdr.markForCheck();
      })
    ).subscribe();
    setTimeout(()=>{
      this.isdownLoad = false;
      this.cdr.detectChanges();
    },10000);
  }

  private exportFileXLSX(data, summary) {
    let sheets = [];
    data.forEach((v,i)=>{
      let day = this.userDateAdv.transform(v.timeFrom,"YYYY/MM/DD");
      let config: XLSXModel;
      if(i==0){
        config = {
          file: {
            title: this.translate.instant('MENU.SIM_INFO'),
            prefixFileName: `${summary['imei']}___${this.userDateAdv.transform(summary.startTime, "YYYYMMDD_HHmmss", { valueOffset: 0 })}__${this.userDateAdv.transform(summary.endTime, "YYYYMMDD_HHmmss", { valueOffset: 0 })}`,
            typeName:"SimInfoHistory",
            objName:summary['imei'],
            timeStart:this.userDateAdv.transform(summary.startTime, "YYYYMMDD_HHmmss", { valueOffset: 0 }),
            timeEnd:this.userDateAdv.transform(summary.endTime, "YYYYMMDD_HHmmss", { valueOffset: 0 })
          },
          header: [
            {
              text: this.translate.instant('MENU.SIM_INFO'),
              type: "header",
              style: {
                font: { name: 'Times New Roman', sz: 14, bold: true },
                alignment: { vertical: "center", horizontal: "center", wrapText: true },
                
              },
              hpx: 32
            },
            {
              text: "",
            },
            {
              text: "!timezone",
            },
            {
              text: ""
            },
            {
              text: [this.translate.instant('COMMON.EXCEL.START_TIME') + ": ", , summary['startTime']],
            },
            {
              text: [this.translate.instant('COMMON.EXCEL.END_TIME') + ": ", , summary['endTime']],
              // text: [,,],
            },
            {
              text: "",
            },
            {
              text: [this.translate.instant('TECHNICAL_SUPPORT.IMEI') + ": ", , summary['imei']],
              style: {
                font: { bold: true },
              }
            },
            {
              text: ""
            },
          ],
          columns: [
            {
              name: "#",
              columnData: "auto",
              wch: 5,
            }, 
            {
              name: this.translate.instant('COMMON.COLUMN.TIME_DEVICE'),
              columnData: "time",
              wch: 20,
              type:'datetime',
              format:'YYYY/MM/DD HH:mm:ss'
            },
            {
              name: this.translate.instant('COMMON.COLUMN.UPDATE_TIME'),
              columnData: "timeUpdate",
              wch: 20,
              type:'datetime',
              format:'YYYY/MM/DD HH:mm:ss'
            },
            {
              name: this.translate.instant('TECHNICAL_SUPPORT.SIM_INFO.SIM_NO'),
              columnData: "simno",
              wch: 20,
              style: {
                alignment: { horizontal: "left" },
              },
            },
            {
              name: this.translate.instant('TECHNICAL_SUPPORT.SIM_INFO.SENDER'),
              columnData: "sender",
              wch: 20,
              // type: "number"
            },
            {
              name: this.translate.instant('TECHNICAL_SUPPORT.SIM_INFO.DEVICE_NAME'),
              columnData: "deviceName",
              wch: 20,
              type: "number",
              style: {
                alignment: { horizontal: "left" },
              },
            },
            {
              name: this.translate.instant('TECHNICAL_SUPPORT.SIM_INFO.NUMBER_PLATE'),
              columnData: "numberPlate",
              wch: 20,  
              type: "number"
            },
            {
              name: this.translate.instant('TECHNICAL_SUPPORT.SIM_INFO.CONTENT'),
              columnData: "content",
              wch: 50,
            },
          ],
          subColumn:[
            {
              text: day,
              style: {
                font: { bold: true },
                fill: {
                  patternType: "solid", // none / solid
                  fgColor: { rgb: "cccccc" },
                },
              },
              merge:{
                full:true
              }
            },
          ],
          styleColumn: {
            fill: {
              patternType: "solid", // none / solid
              fgColor: { rgb: "eeeeee" },
            },
            font: { bold: true },
            alignment: { vertical: "center", horizontal: "center", wrapText: true },
          },
          showColumn: true,
          woorksheet: {
            name: summary['imei'],
          }
        }
  
      }
      else{
        config = {
          file: {
            title: this.translate.instant('MENU.SIM_INFO'),
            prefixFileName: `${summary['imei']}___${this.userDateAdv.transform(summary.startTime, "YYYYMMDD_HHmmss", { valueOffset: 0 })}__${this.userDateAdv.transform(summary.endTime, "YYYYMMDD_HHmmss", { valueOffset: 0 })}`,
            typeName:"SimInfoHistory",
            objName:summary['imei'],
            timeStart:this.userDateAdv.transform(summary.startTime, "YYYYMMDD_HHmmss", { valueOffset: 0 }),
            timeEnd:this.userDateAdv.transform(summary.endTime, "YYYYMMDD_HHmmss", { valueOffset: 0 })
          },
          header: [],
          columns: [
            {
              name: "#",
              columnData: "auto",
              wch: 5,
            }, 
            {
              name: this.translate.instant('COMMON.COLUMN.TIME_DEVICE'),
              columnData: "time",
              wch: 20,
              type:'datetime',
              format:'YYYY/MM/DD HH:mm:ss'
            },
            {
              name: this.translate.instant('COMMON.COLUMN.UPDATE_TIME'),
              columnData: "timeUpdate",
              wch: 20,
              type:'datetime',
              format:'YYYY/MM/DD HH:mm:ss'
            },
            {
              name: this.translate.instant('TECHNICAL_SUPPORT.SIM_INFO.SIM_NO'),
              columnData: "simno",
              wch: 20,
              style: {
                alignment: { horizontal: "left" },
              },
            },
            {
              name: this.translate.instant('TECHNICAL_SUPPORT.SIM_INFO.SENDER'),
              columnData: "sender",
              wch: 20,
              // type: "number"
            },
            {
              name: this.translate.instant('TECHNICAL_SUPPORT.SIM_INFO.DEVICE_NAME'),
              columnData: "deviceName",
              wch: 20,
              type: "number"
            },
            {
              name: this.translate.instant('TECHNICAL_SUPPORT.SIM_INFO.NUMBER_PLATE'),
              columnData: "numberPlate",
              wch: 20,  
              type: "number"
            },
            {
              name: this.translate.instant('TECHNICAL_SUPPORT.SIM_INFO.CONTENT'),
              columnData: "content",
              wch: 50,
            },
          ],
          subColumn:[
            {
              text: day,
              style: {
                font: { bold: true },
                fill: {
                  patternType: "solid", // none / solid
                  fgColor: { rgb: "cccccc" },
                },
              },
              merge:{
                full:true
              }
            },
          ],
          styleColumn: {
            fill: {
              patternType: "solid", // none / solid
              fgColor: { rgb: "eeeeee" },
            },
            font: { bold: true },
            alignment: { vertical: "center", horizontal: "center", wrapText: true },
          },
          showColumn: false,
          woorksheet: {
            name: summary['imei'],
          }
        }
      }
       
      sheets.push(
        { data: v.points, config: config, option: {} }
      )
    })
    this.xlsx.exportFileAdvanced({ sheets: sheets, mergeSheet: true });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
  }

  copied(val) {
    this.toast.copied(val);
  }

  showDetailLog(content, item) {
    this.dataDefault = item;
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder', size: 'md', }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }



  private initDataTable() {
    let tableConfig = {
      pagination: [
        10, 20, 30, 50
      ],
      columns: [
        {
          title: 'id',
          field: 'id',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '20px' },
          width: 20,
          class: '',
          translate: '#',
          autoHide: false,
        },
        {
          title: 'imei',
          field: 'imei',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '135px' },
          width: 135,
          class: '',
          translate: 'TECHNICAL_SUPPORT.IMEI',
          autoHide: false,
        },
        {
          title: 'time device',
          field: 'timeDevice',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '135px' },
          width: 135,
          class: '',
          translate: 'COMMON.COLUMN.TIME_DEVICE',
          autoHide: false,
        },
        {
          title: 'time update',
          field: 'timeUpdate',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '135px' },
          width: 135,
          class: '',
          translate: 'COMMON.COLUMN.UPDATE_TIME',
          autoHide: false,
        },
        {
          title: 'simno',
          field: 'simno',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '135px' },
          width: 135,
          class: '',
          translate: 'TECHNICAL_SUPPORT.SIM_INFO.SIM_NO',
          autoHide: false,
        },
        {
          title: 'sender',
          field: 'sender',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '135px' },
          width: 135,
          class: '',
          translate: 'TECHNICAL_SUPPORT.SIM_INFO.SENDER',
          autoHide: false,
        },
        {
          title: 'deviceName',
          field: 'deviceName',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '135px' },
          width: 135,
          class: '',
          translate: 'TECHNICAL_SUPPORT.SIM_INFO.DEVICE_NAME',
          autoHide: false,
        },
        {
          title: 'numberPlate',
          field: 'numberPlate',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '135px' },
          width: 135,
          class: '',
          translate: 'TECHNICAL_SUPPORT.SIM_INFO.NUMBER_PLATE',
          autoHide: false,
        },
        {
          title: 'action',
          field: 'action',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '80px' },
          width: 80,
          class: '',
          translate: 'COMMON.ACTIONS.ACTIONS',
          autoHide: false,
        },

      ]
    };
    this.dataTable.init({
      data: [],
      totalRecod: 0,
      paginationSelect: [10, 20, 30, 50],
      columns: tableConfig.columns,
      formSearch: '#formSearch',
      pageSize: 10,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,

        },
        selecter: false,
        responsive: false,
      },
      showFull: true
    });
  }
}
