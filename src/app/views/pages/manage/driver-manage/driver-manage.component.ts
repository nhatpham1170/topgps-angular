import { Component, OnInit, Input, ElementRef, ChangeDetectorRef,ViewChild} from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { finalize, takeUntil, tap ,debounceTime,distinctUntilChanged,map,filter, delay} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { DeviceGroup,DeviceGroupService,DeviceService,DriverService,Driver} from '@core/manage';
import { DomSanitizer} from '@angular/platform-browser';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { ValidatorCustomService, UserDatePipe } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import * as moment from 'moment';

declare var $: any;
@Component({
  selector: 'kt-driver-manage-component',
  templateUrl: './driver-manage.component.html',
  styleUrls: ['./driver-manage.component.scss'],

})
export class DriverManageComponent implements OnInit {

  @ViewChild('dateBegin', { static: true }) 
  dateBegin: DateRangePickerComponent;

  @ViewChild('dateExpire', { static: true }) 
  dateExpire: DateRangePickerComponent;

  formDriver: FormGroup;
  searchFormDriver : FormGroup;
  selectdp: ElementRef;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idDriverEdit: number;
  public dataDefault: any = [{}];
  public idDriverDelete: any;
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public listDevices : any = [];
  public checkallboxed:boolean;
  public userIdSelected:number;
  public keySearchFilter:string = 'name';
  public listChecked:any =[];
  public countChecked:number = 0;
  public showUserTree = true;
  public beginDate:string;
  public expired:string;
  public datePickerOptionsBegin: any = {
    size: 'md',
    autoSelect: true,
    singleDatePicker: true,
    timePicker: true,
    key:'begin',
    startDate:moment().subtract(0, "days").startOf('day'),
  };
  public datePickerOptionsExpired: any = {
    size: 'md',
    autoSelect: true,
    singleDatePicker: true,
    timePicker: true,
    startDate:moment().subtract(0, "days").startOf('day'),

    key:'expired'
  };
  constructor(
    private DeviceGroup: DeviceGroupService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private sanitizer:DomSanitizer,
    private modalService: NgbModal,
    private deviceService: DeviceService,
    private userTreeService: UserTreeService,
    private driver: DriverService,
    private validatorCT: ValidatorCustomService,
    private currentUser: CurrentUserService,

  ) {
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    
    this.updateDataTable();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();
   }

  private buildForm(): void {
    let _this = this;
    this.formDriver = this.formBuilder.group({
        name: ["",Validators.required],
        phone: ["",Validators.required],
        email: [""],
        address: [""],
        driverCode: [""],
        licenseNumber: [""],
        beginDate: [""],
        expireDate: [""],
        description: [""],
    });
    this.searchFormDriver = this.formBuilder.group({
        nameDriver: [''],
        driverCode: [''],
        licenseNumber: ['']
    });
  }

  resetFormSearch()
  {
    this.searchFormDriver.reset();
    this.filter.name = '';
    this.filter.driverCode = '';
    this.filter.licenseNumber = '';
    this.dataTable.reload({ currentPage: 1 });

    // this.getData(this.userIdSelected);
  }
  
  buttonAddNew(content) {
    this.isEdit = false;
    this.formDriver.reset();
    this.open(content);
    this.datePickerOptionsBegin.startDate = moment().subtract(0, "days").startOf('day');
    this.datePickerOptionsExpired.startDate = moment().subtract(0, "days").startOf('day');
  }

  open(content) {           
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder',size: 'lg'  }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  getIdAction(id, idModal) {
    this.idDriverDelete = id;
    this.modalService.open(idModal, { windowClass: 'kt-mt-50  modal-holder modal-delete',backdrop:'static'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    }); }

  onSubmitDriver(form: any) {
    if (this.formDriver.invalid) {
      return;
    }
    let params = {
      userId: this.userIdSelected,
      name:this.formDriver.value.name,
      phone:this.formDriver.value.phone,
      driverCode:this.formDriver.value.driverCode,
      licenseNumber:this.formDriver.value.licenseNumber,
      beginDate:this.beginDate,
      expireDate:this.expired,
      email:this.formDriver.value.email,
      address:this.formDriver.value.address,
      description:this.formDriver.value.description,
    } as Driver;
    if (this.isEdit) {
      params.id = this.idDriverEdit;
      this.driver.update(params,{ notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
        this.dataTable.reload({ currentPage: 1 });
        //   this.getData(this.userIdSelected);
          this.closeModal();
        }
      });
      return;
    }
    this.driver.create(params,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        // this.getData(this.userIdSelected);
        this.dataTable.reload({ currentPage: 1 });

        this.closeModal();
      }})
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
      orderBy:'createdAt',
      orderType:"DESC"
    };
  }
 
  dateSelectChangeExpired(data) {
      this.expired = data.startDate.format("YYYY-MM-DD");
   }

   dateSelectChangeBegin(data) {
    this.beginDate = data.startDate.format("YYYY-MM-DD");    
   }
    
  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }
 
  ChangeUserTree(value) {
    if (value.id > 0) {
      this.userIdSelected = value.id;
      this.dataTable.reload({ currentPage: 1 });
    }
    // this.search();
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  deleteDriver() {
    let id = this.idDriverDelete;
    this.driver.delete(id,{ notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.dataTable.reload({ currentPage: 1 });
        // this.getData(this.userIdSelected);
        this.closeModal();
      }
    });
  }

   editDriver(item,content){
    this.isEdit = true;
    this.idDriverEdit = item.id;
    this.open(content);  
    this.datePickerOptionsBegin.startDate = item.beginDate;
    this.datePickerOptionsExpired.startDate = item.expireDate;

    // this.beginDate = item.beginDate;
    // this.expired = item.expireDate;
    this.dataDefault = [item];    
  }

  loadSelectBootstrap(){
    $('.bootstrap-select').selectpicker();
  }

  searchDriver(form:any)
  {
    if(form.value.nameDriver == null) form.value.nameDriver = '';
    if(form.value.driverCode == null) form.value.driverCode = '';
    if(form.value.licenseNumber == null) form.value.licenseNumber = '';

    let params = {
        name : form.value.nameDriver,
        driverCode : form.value.driverCode,
        licenseNumber : form.value.licenseNumber,
    } as Driver;
    this.filter.name = params.name;
    this.filter.driverCode = params.driverCode;
    this.filter.licenseNumber = params.licenseNumber;
    this.dataTable.reload({ currentPage: 1 });

    // this.getData(this.userIdSelected);
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;     
       this.getData(this.userIdSelected);
      }),
    ).subscribe();
  }

  get f() {
   if(this.formDriver != undefined) return this.formDriver.controls;
  }

  getData(userId) {
    if(userId != undefined) this.filter.userId = userId;
    
    this.driver.listDriver(this.filter)
      .pipe(
        delay(300),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
        //example data
        this.data = result.result.content;        
        this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          // setTimeout(()=>{
          //   $('select').selectpicker();
          // })
          
        }
      })
  }


 sanitize(url:string){
      return this.sanitizer.bypassSecurityTrustUrl(url);
    }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout:{
        body:{
          scrollable:false,
          maxHeight:600,
        },
        selecter:false,
        responsive:false
        }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }
  
  setColumns() {
    this.columns = [
      { 
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Name',
        field: 'name',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'MANAGE.DRIVER.GENERAL.NAME',
        autoHide: true,
        width: 100,
      },
      {
        title: 'driver code',
        field: 'count',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'MANAGE.DRIVER.GENERAL.DRIVER_CODE',
        autoHide: true,
        width: 150,
      },
      {
        title: 'Phone',
        field: 'editBy',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '120px' },
        class: '',
        translate: 'MANAGE.DRIVER.GENERAL.PHONE',
        autoHide: true,
        width: 120,
      },
      {
        title: 'Email',
        field: 'updated',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '120px' },
        class: '',
        translate: 'MANAGE.DRIVER.GENERAL.EMAIL',
        autoHide: true,
        width: 120,
      },
      {
        title: 'Address',
        field: 'updated',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '120px'},
        class: '',
        translate: 'MANAGE.DRIVER.GENERAL.ADDRESS',
        autoHide: true,
        width: 120,
      },
      {
        title: 'License',
        field: 'count',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px'},
        class: '',
        translate: 'MANAGE.DRIVER.GENERAL.LICENSE_NUMBER',
        autoHide: true,
        width: 150,
      },
      {
        title: 'begin',
        field: 'updated',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '120px'},
        class: '',
        translate: 'MANAGE.DRIVER.GENERAL.BEGIN_DATE',
        autoHide: true,
        width: 120,
      },
      {
        title: 'exprie',
        field: 'updated',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '120px' },
        class: '',
        translate: 'MANAGE.DRIVER.GENERAL.EXPRIE_DATE',
        autoHide: true,
        width: 120,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: true,
        width: 100,
      },
    ]
  }


}




