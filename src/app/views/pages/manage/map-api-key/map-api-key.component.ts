import {
	Component,
	OnInit,
	ChangeDetectorRef,
	EventEmitter,
	ViewChild,
	ElementRef,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { User, MapApiKey } from '@core/manage';

import { UserManageService } from '@core/manage/_service/user-manage.service';
import { EventEmitterService } from '@core/common/_service/eventEmitter.service';
import { PageService, ToastService } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import {
	NgbModal,
	ModalDismissReasons,
	NgbModalRef,
} from '@ng-bootstrap/ng-bootstrap';
import { finalize, takeUntil, tap, debounceTime, delay } from 'rxjs/operators';
import { Subject } from 'rxjs';

declare var $: any;


const TITLE_FORM_EDIT: string = 'COMMON.ACTIONS.EDIT';
const TITLE_FORM_ADD: string = 'COMMON.ACTIONS.ADD';

@Component({
	selector: 'kt-map-api-key',
	templateUrl: './map-api-key.component.html',
	styleUrls: ['./map-api-key.component.scss'],
})
export class MapApiKeyComponent implements OnInit {
	public idLogin: number;
	public idKeySelected: number;
	public isEdit: boolean;
	public parrentEmiter: EventEmitter<any | { id: number; path: string }>;

	public: any = [{}];
	public listUserAdd: any = [{}];
	public pageNo = 1;
	public pageSize = 10;
	public orderBy = 'createdAt';
	public orderType = 'DESC';
	public filter: any;
	public dataTable: DataTable = new DataTable();
	public data: any = [];
	public columns: any = [];
	public totalRecod: number = 0;
	public paginationSelect: any = [];

	// user-uses table
	public dataUserUse: any = [];
	public tableUserUses: DataTable = new DataTable();
	public filterUserUses: any;
	public columnsUserUses: any = [];
	public paginationSelectUserUses: any = [];
	public totalRecodUserUses: number = 0;

	public idUserUse: number;
	private modalDeleteUserUse: NgbModalRef;
	private modalUserTree: NgbModalRef;
	private currentReasonUserUse: any;

	public showUserTree = true;
	public listPages: any = [];

	public listAddUserUses: any = [];
	public resultAddUserUses: any = [];

	public listEditUserUses: any = [];
	private userIdSelected: number;
	public currentUser: any;

	public mapKeyFormTitle: string;
	public formMapApiKey: FormGroup;
	public searchFormApiKeyMap: FormGroup;
	public currentData: any = [{}];

	public closeResult: string;
	public rootId: number;

	@ViewChild('frmUserTree', { static: true }) frmUserTree: ElementRef;
	@ViewChild('frmdeleteUserUse', { static: true }) frmdeleteUserUse: ElementRef;

	constructor(
		private cdr: ChangeDetectorRef,
		private modalService: NgbModal,
		private fb: FormBuilder,
		private manageUser: UserManageService,
		private eventEmitterService: EventEmitterService,
		private userTreeService: UserTreeService,
		private CurrentUserService: CurrentUserService,
		private toast: ToastService,

	) {

		this.parrentEmiter = new EventEmitter();
		this.listUserAdd = [];
		this.buildForm();

		this.setColumns();
		this.buildPagination();
		this.setPaginationSelect();
		this.setDataTable();

		this.setColumnsUserUses();
		this.buildPaginationUserUses();
		this.setPaginationSelectUserUses();
		this.setDataTableUserUses();
	}

	ngOnInit() {

		this.idLogin = this.CurrentUserService.currentUser.id;
		this.eventEmitterService.loadUpdateTree.subscribe((result: any) => {
			this.currentUser.name = '1';

			this.parrentEmiter.emit(result);
		});
		this.currentUser = new User();

		this.userTreeService.event
			.pipe(
				tap((action) => {
					switch (action) {
						case 'close':
							this.showUserTree = false;
							break;
						case 'open':
							this.showUserTree = true;
							break;
					}
				})
			)
			.subscribe();

		this.dataTable.eventUpdate
			.pipe(
				tap((option) => {
					this.filter.pageNo = option.pageNo;
					this.filter.pageSize = option.pageSize;
					this.getData(this.userIdSelected);

				})
			)
			.subscribe();

		this.dataTable.reload({});
	}

	onSubmit() {

		if (this.formMapApiKey.invalid) {
			return;
		}

		var mapApikey = new MapApiKey();
		mapApikey.userId = this.userIdSelected;
		mapApikey.apiKey = this.formMapApiKey.get('apiKey').value;
		mapApikey.name = this.formMapApiKey.get('name').value;
		mapApikey.type = this.formMapApiKey.get('type').value;
		mapApikey.description = this.formMapApiKey.get('description').value;
		if (this.isEdit) {
			mapApikey.id = this.idKeySelected;
			this.manageUser
				.updateKey(mapApikey, { notifyGlobal: true })
				.subscribe((result: any) => {
					if (result.status == 200) {
						this.dataTable.reload({ currentPage: 1 });
						this.modalService.dismissAll();
					}
				});
			return;
		}

		this.manageUser
			.postKey(mapApikey, { notifyGlobal: true })
			.pipe(
				finalize(() => {
					this.cdr.markForCheck();
				})
			)
			.subscribe((result: any) => {
				if (result.status == 201) {
					this.dataTable.reload({ currentPage: 1 });
					this.modalService.dismissAll();
				}
			}
			);
	}

	changeActive(mapKey: MapApiKey) {
		this.manageUser
			.changeActive(
				{ id: mapKey.id, active: mapKey.active == 1 ? 0 : 1 },
				{ notifyGlobal: true }
			)
			.pipe(
				finalize(() => {
					this.cdr.markForCheck();
				})
			)
			.subscribe((respone: any) => {
				if (respone.status) {
					this.getData(this.userIdSelected);
				}
			});
	}

	deleteMapApiKey() {
		this.manageUser
			.deleteKey(this.idKeySelected, { notifyGlobal: true })
			.pipe(
				tap(
					(data) => {
						if (data.status == 200) {
							this.modalService.dismissAll();
							this.dataTable.reload({});
						}
					},
					(error) => { }
				)
			)
			.subscribe();
	}

	getUserUses(id) {
		this.filterUserUses.apiKeyId = id;
		this.manageUser.getUserUses(this.filterUserUses)
			.pipe(
				finalize(() => {
					this.dataUserUse.isLoading = false;
					this.cdr.markForCheck();
					this.cdr.detectChanges();
				}),
				debounceTime(300)
			)
			.subscribe((result: any) => {
				if (result.status == 200) {
					this.dataUserUse = result.result.content;
					this.totalRecodUserUses = result.result.totalRecord;
					this.tableUserUses.update({
						data: this.dataUserUse,
						totalRecod: this.totalRecodUserUses,
					});
				}
			});

	}

	getData(userId) {
		if (userId != undefined) this.filter.userId = userId;
		this.filter.orderBy = this.orderBy;
		this.filter.orderType = this.orderType;

		this.manageUser
			.getKey(this.filter)
			.pipe(

				delay(300),
				finalize(() => {
					this.cdr.markForCheck();
				})
			)
			.subscribe((result: any) => {
				if (result.status == 200) {
					this.data = result.result.content;
					this.totalRecod = result.result.totalRecord;
					this.dataTable.update({
						data: this.data,
						totalRecod: this.totalRecod,
					});

				}
			});
	}

	buttonAddNew(content) {
		this.isEdit = false;
		this.mapKeyFormTitle = TITLE_FORM_ADD;

		this.tableUserUses.update({ data: [], totalRecod: 0 });

		this.formMapApiKey.controls['apiKey'].enable();
		this.formMapApiKey.reset();
		$(function () {
			$('.kt_selectpicker').selectpicker();
		});
		this.open(content);
	}

	buttonEdit(item, content) {
		this.isEdit = true;
		this.mapKeyFormTitle = TITLE_FORM_EDIT;
		this.currentData = [item];
		this.idKeySelected = item.id;
		this.formMapApiKey.controls['apiKey'].disable();

		// console.log(item);
		// this.currentUser = {
		// 	id: item.userId,
		// 	username: item.userName,
		// 	name: item.name,
		// 	type: item.userType,
		// 	phone: item.phone,
		// 	email: item.email
		// };

		$(function () {
			$('.kt_selectpicker').selectpicker();
			$('.kt_selectpicker').val(item.type).selectpicker('refresh');
		});


		this.tableUserUses.reload({});

		this.modalService
			.open(content, {
				windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder custom-md',
			})
			.result.then(
				(result) => {
					this.closeResult = `Closed with: ${result}`;
				},
				(reason) => {
					this.closeResult = `Dismissed ${this.getDismissReason(
						reason
					)}`;
				}
			);
	}

	openDeleteUserUse(id, modal) {
		this.idUserUse = id;

		this.modalDeleteUserUse = this.modalService
			.open(modal, {
				windowClass: 'kt-mt-50 modal-dialog-scrollable',
				size: 'sm',
				backdrop: 'static',
			});
		this.modalDeleteUserUse.result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			this.currentReasonUserUse = reason;
		});

	}


	deleteUserUse() {
		this.manageUser.deleteUserUseApiKey(this.idUserUse, { notifyGlobal: true })
			.pipe(
				tap(data => {
					if (data.status == 200) {
						this.modalDeleteUserUse.close();
						this.tableUserUses.reload({});
					}
				},
					(error) => { }
				)
			).subscribe();
	}

	buttonDelete(id: number, modal) {
		this.idKeySelected = id;
		this.modalService
			.open(modal, {
				windowClass: 'kt-mt-50 modal-dialog-scrollable',
				size: 'sm',
				backdrop: 'static',
			})
			.result.then(
				(result) => {
					this.closeResult = `Closed with: ${result}`;
				},
				(reason) => {
					this.closeResult = `Dismissed ${this.getDismissReason(
						reason
					)}`;
				}
			);
	}

	openUserTree() {

		if (this.tableUserUses.data != []) {
			this.resultAddUserUses = this.tableUserUses.data;
		}

		this.modalUserTree = this.modalService.open(this.frmUserTree, {
			windowClass: 'kt-mt-50 modal-dialog-scrollable custom-sm',
			backdrop: 'static',
		});

		this.modalUserTree.result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			this.currentReasonUserUse = reason;
		});
	}

	open(content) {
		this.modalService
			.open(content, {
				windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder',
				size: 'md'
			})
			.result.then(
				(result) => {
					this.closeResult = `Closed with: ${result}`;
				},
				(reason) => {
					this.closeResult = `Dismissed ${this.getDismissReason(
						reason
					)}`;
				}
			);
	}

	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}

	buildForm(): void {
		this.formMapApiKey = this.fb.group({
			apiKey: ['', [Validators.required]],
			name: ['', [Validators.required]],
			type: ['google', [Validators.required]],
			userUses: [''],
			description: [''],
		});

		this.searchFormApiKeyMap = this.fb.group({
			apiKey: [''],
			name: [''],
			subAccount: [false],
		});
	}

	searchMapKeyApi(form: any) {
		if (form.value.name == null || form.value.name == undefined) {
			form.value.name = '';
		}
		if (form.value.apiKey == null || form.value.apiKey == undefined) {
			form.value.apiKey = '';
		}
		if (
			form.value.subAccount == '' ||
			form.value.subAccount == null ||
			form.value.subAccount == undefined
		) {
			form.value.subAcount = false;
		}
		this.filter.name = form.value.name;
		this.filter.apiKey = form.value.apiKey;
		this.filter.parentId = this.userIdSelected;
		this.filter.subAccount = form.value.subAccount;
		this.filter.orderBy = this.orderBy;
		this.filter.orderType = this.orderType;

		this.dataTable.isLoading = true;
		this.cdr.detectChanges();
		this.manageUser
			.searchKey(this.filter)
			.pipe(
				finalize(() => {
					this.dataTable.isLoading = false;
					this.cdr.markForCheck();
					this.cdr.detectChanges();
				}),
				debounceTime(300)
			)
			.subscribe((data: any) => {
				if (data.status == 200) {
					this.data = data.result.content;
					this.totalRecod = data.result.totalRecord;
					this.dataTable.update({
						data: this.data,
						totalRecod: this.totalRecod,
					});
				}
			});
	}

	searchUserUses(userName) {
		this.filterUserUses.userName = userName;
		this.manageUser
			.getUserUses(this.filterUserUses)
			.pipe(
				finalize(() => {
					this.tableUserUses.isLoading = false;
					this.cdr.markForCheck();
					this.cdr.detectChanges();
				}),
				debounceTime(300)
			)
			.subscribe((data: any) => {
				if (data.status == 200) {
					this.dataUserUse = data.result.content;
					this.totalRecodUserUses = data.result.totalRecord;
					this.tableUserUses.update({
						data: this.dataUserUse,
						totalRecod: this.totalRecodUserUses,
					});
				}
			});

	}

	setDataTable() {
		this.dataTable.init({
			data: this.data,
			totalRecod: this.totalRecod,
			paginationSelect: this.paginationSelect,
			columns: this.columns,
			layout: {
				body: {
					scrollable: false,
					maxHeight: 600,
				},
				selecter: false,
				responsive: true
			},
		});
	}

	setDataTableUserUses() {
		this.tableUserUses.init({
			data: this.dataUserUse,
			totalRecod: this.totalRecodUserUses,
			paginationSelect: this.paginationSelectUserUses,
			columns: this.columnsUserUses,
			layout: {
				body: {
					scrollable: false,
					maxHeight: 600,
				},
				selecter: false,
			},
		});
		this.tableUserUses.eventUpdate
			.pipe(
				tap((option) => {
					this.filterUserUses.pageNo = option.pageNo;
					this.filterUserUses.pageSize = option.pageSize;
					this.getUserUses(this.idKeySelected);
				})
			)
			.subscribe();
	}

	resetFormSearch() {
		this.filter.pageNo = 1;
		this.searchFormApiKeyMap.reset();
	}

	resetSearchUserUse(id) {
		this.filterUserUses.pageNo = 1;
		this.filterUserUses.userName = '';
		$('#ipUserUse').val('');

		this.getUserUses(id);
	}

	onResize($elm) {
		this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
		this.cdr.detectChanges();
	}

	ChangeUserTree(value) {
		if (value.id > 0) {
			this.userIdSelected = value.id;
			this.currentUser = value;
			this.search();
		}
	}

	search(data?: any) {
		let listSearch = [];

		if (this.userIdSelected != undefined) {
			listSearch.push({ key: 'userId', value: this.userIdSelected });
		}

		this.dataTable.search(data || listSearch);
	}

	buildPagination() {
		this.filter = {
			pageNo: 1,
			pageSize: 10,
			orderBy:'createdAt',
			orderType:'DESC'
		};
	}

	buildPaginationUserUses() {
		this.filterUserUses = {
			pageNo: 1,
			pageSize: 10,
		};
	}

	setPaginationSelect() {
		this.paginationSelect = [10, 20, 30, 40, 50];
	}

	setPaginationSelectUserUses() {
		this.paginationSelectUserUses = [5, 10, 20, 30, 40];
	}

	clickCheckbox(node) {

		if (this.containsObject(node)) {
			this.removeSendPerson(node);
			return false;
		}

		this.listAddUserUses.push(node); // item checked
	}

	getListIdSelectedUser() {
		let userIdChecked;
		userIdChecked = this.listAddUserUses.map(item => {
			let x = parseInt(item.data.id, 10);
			return x;
		});


		// if (this.dataUserUse.length != 0) {
		// 	userIdsCurrent = this.dataUserUse.map(item => {
		// 		let x = parseInt(item.userId, 10);
		// 		return x;
		// 	});

		// 	//get checked value does not exist value in table uses
		// 	userIdResult = userIdChecked.filter(element => !userIdsCurrent.includes(element));
		// 	return userIdResult;
		// }

		return userIdChecked;
	}

	removeSendPerson(node) {
		this.parrentEmiter.emit({ id: node });
		this.listAddUserUses.splice(this.listAddUserUses.findIndex(
			(v) => v.id === node.data.id
		), 1);
	}

	containsObject(obj) {
		for (let i = 0; i < this.listAddUserUses.length; i++) {
			if (this.listAddUserUses[i] === obj) {
				return true;
			}
		}
		return false;
	}


	addCheckedUserUses() {
		let params = {
			userIds: this.getListIdSelectedUser(),
			apiKeyId: this.idKeySelected
		}

		if (params.userIds.length == 0) {
			this.toast.show({ translate: "MESSEAGE_CODE.USER_MAP_KEY.USER_NOT_EMPTY", type:'error'});
			return;

		}

		this.manageUser.createUserUseApiKey(params, { notifyGlobal: true })
			.pipe(
				finalize(() => {
					this.cdr.markForCheck();
				})
			)
			.subscribe((result: any) => {
				if (result.status == 201) {
					this.tableUserUses.reload({ currentPage: 1 });
					this.modalUserTree.close();
				}
			}
			);
	}

	clearChecked() {
		this.listAddUserUses = [];
		this.modalUserTree.close();
	}

	get f() {
		if (this.formMapApiKey != undefined) return this.formMapApiKey.controls;
	}

	setColumns() {
		this.columns = [
			{
				title: '#',
				field: 'no',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { width: '20px' },
				width: 20,
				class: '',
				translate: '#',
				autoHide: false,
			},
			{
				title: 'Name',
				field: 'apiname',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { width: '180px', margin: '0px' },
				width: 150,
				class: '',
				translate: 'COMMON.COLUMN.NAME',
				autoHide: false,
			},
			{
				title: 'Key',
				field: 'key',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { width: '200px' },
				width: 180,
				class: '',
				translate: 'MANAGE.MAP_API_KEY.API_KEY',
				autoHide: true,
			},
			{
				title: 'Owner',
				field: 'owner',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { width: '100px' },
				width: 100,
				class: '',
				translate: 'MANAGE.MAP_API_KEY.API_KEY_OWNER',
				autoHide: true,
			},
			{
				title: 'Type',
				field: 'type',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { width: '100px' },
				width: 100,
				class: '',
				translate: 'MANAGE.MAP_API_KEY.TYPE',
				autoHide: true,
			},
			{
				title: 'Create At',
				field: 'createdAt',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { width: '100px' },
				width: 100,
				class: '',
				translate: 'COMMON.COLUMN.CREATED_DATE',
				autoHide: true,
			},
			{
				title: 'Active',
				field: 'active',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { width: '100px' },
				width: 100,
				class: '',
				translate: 'COMMON.COLUMN.ACTIVE',
				autoHide: false,
			},
			{
				title: 'Actions',
				field: 'action',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { width: '100px' },
				width: 100,
				class: '',
				translate: 'COMMON.COLUMN.ACTIONS',
				autoHide: false,
			},
		];
	}

	setColumnsUserUses() {
		this.columnsUserUses = [
			{
				title: '#',
				field: 'id',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { 'width': '20px' },
				width: 20,
				class: '',
				translate: '#',
				autoHide: false,
			},
			{
				title: 'User Name',
				field: 'useruse',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { width: '100px', margin: '0px' },
				width: 100,
				class: '',
				translate: 'COMMON.COLUMN.NAME',
				autoHide: true,
			},

			{
				title: 'parent',
				field: 'parent',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { width: '100px' },
				width: 100,
				class: '',
				translate: 'MANAGE.MAP_API_KEY.API_KEY_OWNER',
				autoHide: true,
			},
			{
				title: 'action',
				field: 'action',
				allowSort: false,
				isSort: false,
				dataSort: '',
				style: { width: '50px' },
				width: 50,
				class: '',
				translate: 'COMMON.COLUMN.DELETE',
				autoHide: false,
			},
		];
	}
}
