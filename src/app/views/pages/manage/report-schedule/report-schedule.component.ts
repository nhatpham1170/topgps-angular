import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { finalize, takeUntil, tap ,debounceTime} from 'rxjs/operators';
import { Subject, from } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { ReportSchedule,ReportScheduleService } from '@core/manage';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { DeviceService} from '@core/manage';
import { NgbTimepickerConfig} from '@ng-bootstrap/ng-bootstrap';
import { NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import { FunctionService,ToastService, PageService,UserDatePipe } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { TranslateService } from '@ngx-translate/core';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import * as moment from 'moment';
import { UserTreeService } from '@core/common';

declare var $: any;
@Component({
  selector: 'kt-report-schedule',
  templateUrl: './report-schedule.component.html',
  styleUrls: ['./report-schedule.component.scss'],
  providers: [NgbTimepickerConfig,UserDatePipe]
})
export class ReportScheduleComponent implements OnInit {

  @ViewChild('datePicker', { static: true }) 

  datePicker: DateRangePickerComponent;
  formReportSchedule: FormGroup;
  searchFormReportSchedule : FormGroup;
  formCheckList:FormGroup;
  selectdp: ElementRef;

  public datePickerOptions: any = {
    size: 'md',
    select: 'to',
    ranges: 'to',
    optionDatePicker: {
      maxSpan: {
        days: 31
      }
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: true,

  };
  public dateStart : any;
  public dateEnd : any;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public dateFormat: string;
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idReportScheduleEdit: number;
  public dataDefault: any = [{}];
  public idReportScheduleDelete: any;
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public listDeviced : any = [];
  public listDevices : any = [];
  public userIdSelected:number;
  public countChecked:number;
  public userIdSelectedEdit:number;
  public listReports: any = [];
  public format:string = 'excel';
  // schedule time
  public isDaily:boolean = false;
  public timeDaily : NgbTimeStruct = {hour: 0, minute: 0, second: 0};
  public timeWeekly : NgbTimeStruct = {hour: 0, minute: 0, second: 0};
  public isWeekly:boolean = false;
  public showUserTree = true;

  public validateObjectEmail : any = {
    message : '',
    status : false
  };
  public emails:string = '';

  constructor(
    private ReportSchedule: ReportScheduleService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private deviceService: DeviceService,
    private config: NgbTimepickerConfig,
    private functionService : FunctionService,
    private currentUser: CurrentUserService,
    private translate:TranslateService,
    private toast: ToastService,
    private page : PageService,
    private userDatePipe : UserDatePipe,
    private userTreeService: UserTreeService,

  ) {
    this.unsubscribe = new Subject();
    this.configTimePicker();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.getListReports();
    this.resetVar();

    // this.buildDateNow();
  }

  ngOnInit() {
    this.updateDataTable();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();
   }

  
   dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
   }

  configTimePicker()
   {
    this.config.spinners = false;
   }

   resetVar()
   {
    this.timeDaily  = {hour: 0, minute: 0, second: 0};
    this.timeWeekly  = {hour: 0, minute: 0, second: 0};
    this.isDaily = false;
    this.isWeekly = false;
    // this.datePickerOptions.startDate = moment().format("YYYY-MM-DD HH:mm:ss");
    // this.datePickerOptions.endDate =  moment().add(30, 'day').format("YYYY-MM-DD HH:mm:ss");
   
   }

   private buildForm(): void {
    this.formReportSchedule = this.formBuilder.group({
     name:['',Validators.required],
     type:[''],
     format:[''],
     dateStart: [''],
     dateEnd: [''],
     isDaily:[''],
     isWeekly:[''],
     timeDaily:[''],
     timeWeekly:[''],
     email:['']
    });
    this.searchFormReportSchedule = this.formBuilder.group({
      name: ['']
    });
  }

  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-900',backdrop:'static'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  
  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
      orderBy:'createdAt',
      orderType:'DESC'
    };
  }

  ChangeUserTree(value) {    
    if (value.id > 0) {this.userIdSelected = value.id;}
    this.search();
    }

    changeCheckedDevice(event){
    this.listDeviced = event.listChecked;
    this.countChecked = event.countChecked;
  }

  getIdAction(id, idModal) {
    this.idReportScheduleDelete = id;
    this.modalService.open(idModal, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-delete',backdrop:'static'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  deleteScheduleReport() {
    let id = this.idReportScheduleDelete;
    this.ReportSchedule.delete(id,{ notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.modalService.dismissAll();        
        this.dataTable.reload({currentPage:1});
      } 
    });
  }


  selectDate(event, number) {
    if (number == 0) {
      this.dateStart = this.functionService.convertDate(event)
      return;
    }
    this.dateEnd =  this.functionService.convertDate(event);
  }

  getListReports(){
    this.listReports = this.page.getListPageReports();
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }

  buttonAddNew(content) {
    this.resetVar();
    this.isEdit = false;
    this.formReportSchedule.reset();
    this.open(content);
    this.getListDevices();
  }

  getListDevices(){
    let option : any = [];
    option['pageNo'] = -1;
    option['userId'] = this.userIdSelected;

    this.deviceService.list({ params: option }).pipe(
    ).subscribe((data: any) => {
      this.dataDefault = [{}];
      this.listDevices = data.result;
      setTimeout(function () {
        $('.kt_selectpicker').selectpicker();
      }, 100);
    });
  }
  editReportSchedule(id,content) {
    this.isEdit = true;
    this.idReportScheduleEdit = id;
    let params = {
      id : id,
      userId : this.userIdSelected
    } as ReportSchedule;
    this.ReportSchedule.detail(params).pipe(
      tap((data: any) => {
        this.emails = data.result.pushSetting.emails;
        this.isDaily = data.result.schedule.daily.isDaily;
        this.isWeekly = data.result.schedule.weekly.isWeekly;
        this.timeDaily = this.convertTimeToUtc(data.result.schedule.daily.time);
        this.timeWeekly = this.convertTimeToUtc(data.result.schedule.weekly.time);
        this.listDevices = data.result.devices;
        this.datePickerOptions.startDate = this.dateStart =  data.result.schedule.dateStart;
        this.datePickerOptions.endDate = this.dateEnd =data.result.schedule.dateEnd;
        this.dataDefault = [data.result];
        this.datePickerOptions.endDate  =  this.userDatePipe.transform(this.datePickerOptions.endDate, "YYYY-MM-DD HH:mm:ss",null,"datetime");
        this.datePickerOptions.startDate  =  this.userDatePipe.transform(this.datePickerOptions.startDate, "YYYY-MM-DD HH:mm:ss",null,"datetime");
        setTimeout(function () {
          $('.kt_selectpicker').selectpicker();
        }, 100);
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
    this.open(content);
  }
  
  searchReportSchedule(form:any)
  {
    this.filter.name = form.value.name;
    this.dataTable.reload({currentPage:1});
  }

  onKey(event: any) { // without type info
    this.validateObjectEmail = this.functionService.validateListEmail(event.target.value);
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;
        this.getData(this.userIdSelected);
      }),
    ).subscribe();
  }

  convertTimeToUtc(time){
    let date = "2019-12-12 "+time.hour+":"+time.minute+":"+time.second;
    let timeDatePicker = {
     hour:  parseInt(this.userDatePipe.transform(date, "H",null,"datetime")) , 
     minute:  parseInt(this.userDatePipe.transform(date, "m",null,"datetime")) ,
     second:  parseInt(this.userDatePipe.transform(date, "s",null,"datetime")) 
    }
    return timeDatePicker;
  }

  //end checked

  get f() {
   if(this.formReportSchedule != undefined) return this.formReportSchedule.controls;
  }

  search() {
    let listSearch = [];
    if (this.userIdSelected != undefined) {
      listSearch.push({ key: 'userId', value: this.userIdSelected });
    }
    this.dataTable.search(listSearch);
  }

  getData(userId) {
    if(userId != undefined) this.filter.userId = userId;
    this.ReportSchedule.list(this.filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.data = result.result.content;
          result.result.content = result.result.content.map(schedule => {
            let isSchedule = false;
            let keySchedule = '';
            let isDaily = schedule.schedule.daily.isDaily ;
            let isWeekly = schedule.schedule.weekly.isWeekly ;
            if(isDaily || isWeekly) 
            {
              isSchedule =true;
              keySchedule = "MANAGE.REPORT_SCHEDULE.COLUMN.DAILY_WEEKLY";
            }
            if(isDaily && !isWeekly) keySchedule = "MANAGE.REPORT_SCHEDULE.COLUMN.DAILY";
            if(!isDaily && isWeekly) keySchedule = "MANAGE.REPORT_SCHEDULE.COLUMN.WEEKLY";

            schedule.suspended = isSchedule;
            schedule.keySchedule = keySchedule;

            return schedule;
          });
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          setTimeout(function () {
            $('select.selectpicker').selectpicker('refresh');
          }, 100);
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect, 
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
				selecter:false,
			  }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  onSubmit(form: any) {
    if (this.formReportSchedule.invalid) {
      return;
    }
    if(form.value.type == -1){
      this.toast.show(
        {
          message: this.translate.instant('MANAGE.REPORT_SCHEDULE.MESSAGE.TYPE_REPORT'),
          type : 'error',
        })
      return;
    }
    let validateMail = this.functionService.validateListEmail(form.value.email);
    if(validateMail.status)
    {
      this.toast.show(
        {
          message:validateMail.message,
          type : 'error',
        })
      return;
    }

    this.validateObjectEmail = {
      message: "",
      status:false,
    }
    if(form.value.isDaily || form.value.isWeekly)
    {
      if(form.value.email == '')
      {
          this.validateObjectEmail = {
            message: this.translate.instant('COMMON.VALIDATION.PLEASE_ENTER_EMAIL'),
            status:true,
          }
        return;
      }
    }

    let params = {
      userId : this.userIdSelected,
      name:form.value.name,
      typeReport : form.value.type,
      formatReport: form.value.format,
      schedule : {
        dateStart : this.dateStart,
        dateEnd : this.dateEnd,
        daily : {
          isDaily : form.value.isDaily,
          time : form.value.timeDaily
        },
        weekly : {
          isWeekly : form.value.isWeekly,
          time : form.value.timeWeekly
        } 
      },
      pushSetting:{
        emails:form.value.email
      },
      deviceIds : this.listDeviced
    } as ReportSchedule;

    if (this.isEdit) {
      params.id = this.idReportScheduleEdit;
      this.ReportSchedule.update(params,{ notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          this.dataTable.reload({});
          this.closeModal();
        }
      });
      return;
    }

    this.ReportSchedule.create(params,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        this.dataTable.reload({});
        this.closeModal();
      }})
  }
  
  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'NAME',
        field: 'imei',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'MANAGE.REPORT_SCHEDULE.COLUMN.NAME',
        autoHide: false,
        width: 100,
      },
      {
        title: 'TYPE',
        field: 'command',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '130px' },
        class: '',
        translate: 'MANAGE.REPORT_SCHEDULE.COLUMN.TYPE',
        autoHide: true,
        width: 100,
      },
      {
        title: 'FORMAT ',
        field: 'created',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '80px' },
        class: '',
        translate: 'MANAGE.REPORT_SCHEDULE.COLUMN.FORMAT',
        autoHide: true,
        width: 100,
      },
      {
        title: 'lenght device',
        field: 'send',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '80px','text-align':'center' },
        class: '',
        translate: 'MANAGE.REPORT_SCHEDULE.COLUMN.DEVICE',
        autoHide: true,
        width: 100,

      },
      {
        title: 'SCHEDULE',
        field: 'send',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '170px' ,'text-align':'center' },
        class: '',
        translate: 'MANAGE.REPORT_SCHEDULE.COLUMN.SCHEDULE',
        autoHide: true,
        width: 100,

      },

      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 100,
      },
    ]
  }


}




