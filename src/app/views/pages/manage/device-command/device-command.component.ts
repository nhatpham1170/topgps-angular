import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { finalize, takeUntil, tap ,debounceTime} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { DeviceCommand,DeviceCommandService } from '@core/manage';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { DeviceService} from '@core/manage';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { select, Store } from '@ngrx/store';
import { AppState } from '@core/reducers';
import { currentUser } from '@core/auth';
import { DeviceConfigService } from '@core/common';

declare var $: any;
@Component({
  selector: 'kt-device-command',
  templateUrl: './device-command.component.html',
  styleUrls: ['./device-command.component.scss']

})
export class DeviceCommandComponent implements OnInit {
  formCommand: FormGroup;
  searchFormCommand : FormGroup;
  formCheckList:FormGroup;
  selectdp: ElementRef;

  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idCommandEdit: number;
  public dataDefault: any = [{}];
  public idCommandDelete: any;
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public listDeviced : any = [];
  public listDevices : any = [];
  public userIdSelected:number;
  public countChecked:number;
  public userIdSelectedEdit:number;
  public checkallboxed:boolean;
  public showUserTree:boolean = true;
  public permissionLock:string = 'ROLE_command.action.lock';
  public deviceTypes: any[];
  public types: any[];
  public commands: any[];
  public commandStr: string;
  public commandName: string;
  public isSend:boolean = false;
  constructor(
    private Command: DeviceCommandService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private deviceService: DeviceService,
    private userTreeService: UserTreeService,
    private store: Store<AppState>,
    private deviceConfig:DeviceConfigService

  ) {
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    
    this.updateDataTable();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();
   }

 
   private buildForm(): void {
    this.formCommand = this.formBuilder.group({
      imei: [''],
      name: [''],
      content:['',Validators.required],
      commandType:['']
    });
    this.searchFormCommand = this.formBuilder.group({
      imei: [''],
      deviceName: [''],
      commandText:['']
    });
    this.formCheckList = this.formBuilder.group({
      checkallboxed: ['']
    });
    
  }


  
  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-900'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   this.showUserTreeFunction();
  }

  showUserTreeFunction(){
    let userModel = {
      type: 0,
    };
    let user$:any = this.store.pipe(select(currentUser));
    if (user$) {
      select(user$.subscribe(function (data) {
        Object.assign(userModel, data);
      }
      ).unsubscribe());
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  
  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
      orderBy:'createdAt',
      orderType:'DESC'
    };
  }

  changeUser(value){
    if (value.id > 0) {
      this.userIdSelectedEdit = value.id;
      this.getDevices(value.id);
    }
  }

  ChangeUserTree(value) {    
    if (value.id > 0) {
      this.userIdSelected = value.id;
    }
    this.search();
    }

  changeChecked(event){
    this.listDeviced = event.listChecked;
    this.countChecked = event.countChecked;
  }

  getDevices(userId) {
    this.filter.userId = userId;
    this.filter.pageNo = -1;
    // this.filter.pageNo = -1;
    this.deviceService.list({ params: this.filter }).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      this.listDevices = data.result;
      this.addIdToFormArray();
    });
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }

  buttonAddNew(content) {
    
    this.isEdit = false;
    // this.commandName = "";
    // this.commandStr = "";
    this.loadDeviceTypes();
    this.formCommand.reset();
    this.open(content);
    setTimeout(() => {
      $('.kt-selectpicker-send-command').selectpicker();
    });
   
    // this.getDevices(this.userIdSelected);
  }

  changeLock(id,status)
  {
    let sendStatus = 1;
    if(status == sendStatus)
    {
      sendStatus = 0;
    }
    let params = {
      status : sendStatus,
      id: id
    } as DeviceCommand;
    this.Command.update(params, { notifyGlobal: true }).pipe(
      tap((data: any) => {
        this.dataTable.reload({});

      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  editCommand(id,content) {
    this.isEdit = true;
    this.idCommandEdit = id;
    let params = {
      id :id,
      userId:this.userIdSelected
    };
    this.Command.list(params).pipe(
      tap((data: any) => {
        this.dataDefault = data.result.content;
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
    this.open(content);
  }
  
  searchCommand(form:any)
  {
    this.filter.imei = this.searchFormCommand.value.imei || "";
    this.filter['commandText'] = this.searchFormCommand.value.commandText || "";
    this.filter['deviceName'] = this.searchFormCommand.value.deviceName || "";
    
    this.getData(this.userIdSelected);
  }
  resetFormSearch(){
    this.searchFormCommand.reset();
    this.filter.imei = this.searchFormCommand.value.imei || "";
    this.filter['commandText'] = this.searchFormCommand.value.commandText || "";
    this.filter['deviceName'] = this.searchFormCommand.value.deviceName || "";
    this.getData(this.userIdSelected);
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter = option;
        this.getData(this.userIdSelected);
      }),
      
    ).subscribe();
  }

  //checked
  addIdToFormArray()
  {
      this.formCheckList.controls.devices = new FormArray([]);
      for(var i = 0; i < this.listDevices.length ; i++)
      {
        for (let index = 0; index < this.listDevices.length; index++) {
          let id = 'id'+ this.listDevices[index].id;
          this.formCheckList.controls.devices['controls'][id] =  new FormControl();
        }
      }
      this.isCheckSelected();
  }

  isCheckSelected()
  {
      this.checkallboxed = this.listDevices.every(function(item:any) {
        return item.checked == true;
      });
      this.getListIdSelected();
  }
  
  checkUncheckAll()
  {
      for (var i = 0; i < this.listDevices.length; i++) {  
        this.listDevices[i].checked = this.checkallboxed;
      };
      this.getListIdSelected();
  }
  
  getListIdSelected()
  {
      let listIdSelected : any = [];
      for (var i = 0; i < this.listDevices.length; i++) 
      {
        if(this.listDevices[i].checked)
        {
          let obj = {
            id:this.listDevices[i].id,
            type:this.listDevices[i].typeDevice
          }
          listIdSelected.push(obj);
        }
      }
      this.listDeviced = listIdSelected,
      this.countChecked = listIdSelected.length;
  }
  //end checked

  get f() {
   if(this.formCommand != undefined) return this.formCommand.controls;
  }

  search() {
    let listSearch = [];

    if (this.userIdSelected != undefined) {
      listSearch.push({ key: 'userId', value: this.userIdSelected });
    }
    this.dataTable.search(listSearch);
  }

  getData(userId) {
    if(userId != undefined) this.filter.userId = userId;
   
    if(this.filter['imei']!=undefined && this.filter['imei'].length ==0) delete this.filter.imei;
    if(this.filter['deviceName']!=undefined && this.filter['deviceName'].length==0) delete this.filter.deviceName;
    if(this.filter['commandText']!=undefined && this.filter['commandText'].length==0) delete this.filter.commandText;
    this.Command.list(this.filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          setTimeout(function () {
            $('.kt-form select').selectpicker('refresh');
          }, 100);
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect, 
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
        selecter:false,
        responsive:false,
        },
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  onSubmit(form: any) {
    if (this.formCommand.invalid) {
      return;
    }
    let params = {
      commandName:this.formCommand.get('name').value,
      commandText:this.formCommand.get('content').value,
      devices:this.listDeviced
    } as DeviceCommand;
    this.Command.create(params,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        this.dataTable.reload({});
        this.closeModal();
      }})
  }
  
  loadDeviceTypes()
  {
    if (this.deviceTypes == undefined) {
      this.deviceConfig.get().then(x => {
        this.deviceTypes = x.deviceTypes;
        this.types = this.deviceTypes.map(x => x.name);
      });
    }
    else {
      this.types = this.deviceTypes.map(x => x.name);     
    }
  
  }
  selecteDeviceType(value) {
    if (value.length > 0) {
      let type = this.deviceTypes.find(x => x.name == value);
      if (type) {
        if(Array.isArray(type.command)){
          this.commands = type.command;
        }
        // this.commands = Object.keys(type.command).map(x => {
        //   return { name: x, command: type.command[x] };
        // });
      }
      else this.commands = [];
    }
    else this.commands = [];
    setTimeout(() => {
      $('.selectpicker-command').val('default').selectpicker('refresh');
    });
  }
  selecteCommandType(value) {
    this.formCommand.get('content').setValue(value.target.selectedOptions[0].value);
    this.formCommand.get('name').setValue(value.target.selectedOptions[0].text);
  }
  
  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
        headerShow:false,
      },
      {
        title: 'name device',
        field: 'deviceName',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'COMMON.COLUMN.DEVICE_NAME',
        autoHide: true,
        width: 110,
      },
      {
        title: 'Imei',
        field: 'imei',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '120px' },
        class: '',
        translate: 'Imei',
        autoHide: false,
        width: 120,
      },
    
      {
        title: 'device type',
        field: 'deviceType',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'COMMON.COLUMN.TYPE',
        autoHide: true,
        width: 110,
      },
      {
        title: 'NAME',
        field: 'commandName',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'COMMON.COLUMN.COMMAND_NAME',
        autoHide: true,
        width: 110,
      },
      {
        title: 'COMMAND ',
        field: 'command',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'MANAGE.COMMAND.GENERAL.COMMAND',
        autoHide: true,
        width: 110,
      },
      {
        title: 'createdAt ',
        field: 'createdAt',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '130px' },
        class: '',
        translate: 'COMMON.COLUMN.CREATED_DATE',
        autoHide: true,
        width: 130,
      },
      {
        title: 'created by',
        field: 'createdBy',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px','text-align': 'center' },
        class: '',
        translate: 'COMMON.COLUMN.CREATED_BY',
        autoHide: true,
        width: 100,
      },
      // {
      //   title: 'Sended',
      //   field: 'send',
      //   allowSort: false,
      //   isSort: false,
      //   dataSort: '',
      //   style: { 'width': '80px','text-align':'center' },
      //   class: '',
      //   translate: 'MANAGE.COMMAND.GENERAL.SENDED',
      //   autoHide: true,
      //   width: 80,

      // },
      {
        title: 'Time send',
        field: 'sendTime',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '130px' },
        class: '',
        translate: 'MANAGE.COMMAND.GENERAL.TiME_SEND',
        autoHide: true,
        width: 130,

      },
      // {
      //   title: 'RESPONSE',
      //   field: 'response',
      //   allowSort: false,
      //   isSort: false,
      //   dataSort: '',
      //   style: { 'width': '80px','text-align':'center' },
      //   class: '',
      //   translate: 'MANAGE.COMMAND.GENERAL.RESPONSED',
      //   autoHide: true,
      //   width: 80,

      // },
      {
        title: 'response time',
        field: 'responseTime',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '130px' },
        class: '',
        translate: 'MANAGE.COMMAND.GENERAL.TIME_RESPONSE',
        autoHide: true,
        width: 130,
      },
      {
        title: 'Response Text',
        field: 'responseText',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '120px','text-align': 'center' },
        class: '',
        translate: 'MANAGE.COMMAND.GENERAL.CONTENT_RESPONSE',
        autoHide: true,
        width: 120,
      },
      {
        title: 'status',
        field: 'status',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '80px','text-align': 'center' },
        class: '',
        translate: 'COMMON.COLUMN.STATUS',
        autoHide: false,
        width: 80,
        headerShow:false
      },
      
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 117,
        headerShow:false
      },
    ]
  }


}




