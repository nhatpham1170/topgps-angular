import { Component, OnInit, Input, ElementRef, ChangeDetectorRef,ViewChild} from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { finalize, takeUntil, tap ,debounceTime,distinctUntilChanged,map,filter, delay} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { GeofenceGroupService,GeofenceGroup, GeofenceService} from '@core/manage';
import { DomSanitizer} from '@angular/platform-browser';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { ValidatorCustomService, UserDatePipe } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import * as moment from 'moment';

declare var $: any;
@Component({
  selector: 'kt-geofence-group-component',
  templateUrl: './geofence-group.component.html',
  styleUrls: ['./geofence-group.component.scss'],

})
export class GeofenceGroupComponent implements OnInit {

  @ViewChild('dateBegin', { static: true }) 
  dateBegin: DateRangePickerComponent;

  @ViewChild('dateExpire', { static: true }) 
  dateExpire: DateRangePickerComponent;

  formGeofenceGroup: FormGroup;
  searchFormGeofenceGroup : FormGroup;
  selectdp: ElementRef;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idGeofenceGroupEdit: number;
  public dataDefault: any = [{}];
  public idGeofenceGroupDelete: any;
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public listGeofences : any = [];
  public checkallboxed:boolean;
  public userIdSelected:number;
  public keySearchFilter:string = 'name';
  public listChecked:any =[];
  public countChecked:number = 0;
  public showUserTree = true;
  public beginDate:string;
  public expired:string;
  public optionWideCheckList: any = { type: "geofence" };

  constructor(
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private sanitizer:DomSanitizer,
    private modalService: NgbModal,
    private userTreeService: UserTreeService,
    private GeofenceGroup: GeofenceGroupService,
    private geofenceGroupService: GeofenceGroupService,
    private geofenceService: GeofenceService,

  ) {
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    this.getListGeofences();
    this.updateDataTable();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();
   }

  private buildForm(): void {
    let _this = this;
    this.formGeofenceGroup = this.formBuilder.group({
        name: ["",Validators.required],
        description: [""],
        createDate: [''],
        createBy : ['']
    });
    this.searchFormGeofenceGroup = this.formBuilder.group({
        nameGeofenceGroup: [''],
        GeofenceGroupCode: [''],
        licenseNumber: ['']
    });
  }

  
  getListGeofences(){
    let option : any = [];
    option['pageNo'] = -1;
    
    if(this.userIdSelected != undefined)    option['userId'] = this.userIdSelected;
    this.geofenceService.list({ params: option }).pipe().subscribe((data: any) => {
          this.dataDefault = [{}];
          this.listGeofences = data.result;
    });
    
  }

  resetFormSearch()
  {
    this.searchFormGeofenceGroup.reset();
  }
  
  buttonAddNew(content) {
    this.isEdit = false;
    this.formGeofenceGroup.reset();
    let listChecked = [];
    this.convertChecked(listChecked);
    this.open(content);
  }

  open(content) {           
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder',size: 'lg',backdrop:'static'  }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  getIdAction(id, idModal) {
    this.idGeofenceGroupDelete = id;
    this.modalService.open(idModal, { windowClass: 'kt-mt-50  modal-holder modal-delete',backdrop:'static'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    }); }

  onSubmitGeofenceGroup(form: any) {
    if (this.formGeofenceGroup.invalid) {
      return;
    }
    let params = {
      userId: this.userIdSelected,
      name:this.formGeofenceGroup.value.name,
      geofenceIds:this.listChecked
    } as GeofenceGroup;
    if (this.isEdit) {
      params.id = this.idGeofenceGroupEdit;
      this.geofenceGroupService.update(params,{ notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
        this.dataTable.reload({ currentPage: 1 });
          this.closeModal();
        }
      });
      return;
    }
    this.geofenceGroupService.create(params,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        // this.getData(this.userIdSelected);
        this.dataTable.reload({ currentPage: 1 });

        this.closeModal();
      }})
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10
    };
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }
 
  ChangeUserTree(value) {
    if (value.id > 0) {
      this.userIdSelected = value.id;
      this.getListGeofences();

      this.dataTable.reload({ currentPage: 1 });
    }
    // this.search();
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  deleteGeofenceGroup() {
    let id = this.idGeofenceGroupDelete;
    this.GeofenceGroup.delete(id,{ notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.dataTable.reload({ currentPage: 1 });
        // this.getData(this.userIdSelected);
        this.closeModal();
      }
    });
  }

  convertChecked(listChecked)
  {
    this.listGeofences.map(geofence => {
      let checked = false;
      let id = geofence.id;

      if(listChecked.includes(id)) checked = true;
      geofence.checked = checked;
      return geofence;
    });

  }

   editGeofenceGroup(item,content){
    this.isEdit = true;
    this.idGeofenceGroupEdit = item.id;
    let params = {
      userId : this.userIdSelected,
      id:this.idGeofenceGroupEdit
    };
      this.dataDefault =  [item];
      let listChecked = item.geoFenceIds;
      this.convertChecked(listChecked);
      this.open(content); 
  }

  loadSelectBootstrap(){
    $('.bootstrap-select').selectpicker();
  }

  searchGeofenceGroup(form:any)
  {
    if(form.value.nameGeofenceGroup == null) form.value.nameGeofenceGroup = '';
    if(form.value.GeofenceGroupCode == null) form.value.GeofenceGroupCode = '';
    if(form.value.licenseNumber == null) form.value.licenseNumber = '';

    let params = {
        name : form.value.nameGeofenceGroup,
    } as GeofenceGroup;
    this.filter.name = params.name;
    this.dataTable.reload({ currentPage: 1 });

    // this.getData(this.userIdSelected);
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;     
       this.getData(this.userIdSelected);
      }),
    ).subscribe();
  }

  get f() {
   if(this.formGeofenceGroup != undefined) return this.formGeofenceGroup.controls;
  }

  changeChecked(event){
    this.listChecked = event.listChecked;
    this.countChecked = event.countChecked;
  }

  getData(userId) {
    if(userId != undefined) this.filter.userId = userId;
    
    this.GeofenceGroup.listGeofenceGroup(this.filter)
      .pipe(
        delay(300),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
        //example data
        this.data = result.result.content;     
        this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          // setTimeout(()=>{
          //   $('select').selectpicker();
          // })
          
        }
      })
  }


 sanitize(url:string){
      return this.sanitizer.bypassSecurityTrustUrl(url);
    }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout:{
        body:{
          scrollable:false,
          maxHeight:600,
        },
        selecter:false,
        }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }
  
  setColumns() {
    this.columns = [
      { 
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Name',
        field: 'name',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'MANAGE.DRIVER.GENERAL.NAME',
        autoHide: true,
        width: 100,
      },
      {
        title: 'descript',
        field: 'updated',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '120px' },
        class: '',
        translate: 'MANAGE.GEOFENCE_GROUP.GENERAL.COUNT',
        autoHide: true,
        width: 120,
      },
      {
        title: 'create by',
        field: 'updated',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '120px'},
        class: '',
        translate: 'COMMON.COLUMN.CREATED_BY',
        autoHide: true,
        width: 120,
      },
      {
        title: 'create date',
        field: 'updated',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '120px'},
        class: '',
        translate: 'COMMON.COLUMN.CREATED_DATE',
        autoHide: true,
        width: 120,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: true,
        width: 100,
      },
    ]
  }


}




