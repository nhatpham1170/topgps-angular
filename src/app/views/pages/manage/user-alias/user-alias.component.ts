import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef, EventEmitter,ViewChildren,QueryList  } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { UserAlias, UserAliasService, Device, DeviceService,DeviceGroupService } from '@core/manage';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { UserManageService } from '@app/core/manage/_service/user-manage.service';
import { ToastrService } from 'ngx-toastr';
import { ToastService, UserDateAdvPipe } from '@core/_base/layout';
import { RoleAliasService } from '@core/manage';
import { User, CurrentUserService } from '@core/auth';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { SelectPickerComponent } from '@app/views/partials/content/widgets/select-picker/select-picker.component';

import { ListGroupDeviceComponent } from '@app/views/partials/content/widgets/list-group-device/list-group-device.component';
import { number } from '@amcharts/amcharts4/core';

declare var $: any;
@Component({
  selector: 'kt-user-alias',
  templateUrl: './user-alias.component.html',
  styleUrls: ['./user-alias.component.scss'],
  providers:[UserDateAdvPipe]
})
export class UserAliasComponent implements OnInit {

  formUserAlias: FormGroup;
  searchFormUserAlias: FormGroup;
  selectdp: ElementRef;
  public checkListDataChangeEmiter: EventEmitter<any>;
  public userIdChangeEmiter:EventEmitter<any>;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idUserAliasEdit: number;
  public dataDefault: any = [{}];
  public idUserAliasDelete: any;
  public filter: any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public idUserAction: any;
  public userIdSelected: number;
  public userSelected: User;
  public userSelectedOld: User;
  public showUserTree: boolean = true;
  public listRoles: any = [];
  public userEdit: any;
  public listDevice: Array<Device>;
  public listDevices:Array<Device>;
  public listDeviceSelected: Array<any>;
  public listDeviceGroupSelected: Array<any>;

  public isChangeUser: boolean = true;
  public isResetForm: boolean = true;
  public countChecked:number;
  public listChecked:any;
  public listDeviceGroup: any = [];
  public userId:number;
  public dataDefaultComponent;
  public datetimeOptionsStartTime: any = { 
    key:'startTime',
    singleDatePicker: true, 
    size: 'md',
   timePicker: true, 
   format: 'datetime',
    autoSelect: false,
     btnClear: true, 
     optionDatePicker: { 
       autoPlay: true,
      drops:"up", 
    } ,
  
    };
  public datetimeOptionsEndTime: any = { singleDatePicker: true, 
    key:'endTime',
    size: 'md', 
    timePicker: true, 
    format: 'datetime', 
    autoSelect: false, 
    btnClear: true, 
    optionDatePicker: { 
      autoPlay: true,
      drops:"up",
    },

  };
  public isShowBoxListDevice:boolean = false;
  @ViewChild('startTimeCpn', { static: true }) startTimeCpn: DateRangePickerComponent;
  @ViewChild('endTimeCpn', { static: true }) endTimeCpn: DateRangePickerComponent;
  @ViewChild('boxListDevices', { static: true }) boxListDevices: SelectPickerComponent;

  constructor(
    private roleAlias: RoleAliasService,
    private manageUser: UserManageService,
    private UserAlias: UserAliasService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private toast: ToastService,
    private ngToast: ToastrService,
    private deviceService: DeviceService,
    private userTreeService: UserTreeService,
    private currentUser: CurrentUserService,
    private userDateAdv:UserDateAdvPipe,
    private DeviceGroup: DeviceGroupService,


  ) {
    this.userIdChangeEmiter = new EventEmitter();

    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.checkListDataChangeEmiter = new EventEmitter();
  }

  ngOnInit() {
    this.updateDataTable();
    // this.getData();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();
    this.buildFormSearch();

  }

  private buildFormSearch(){
    this.searchFormUserAlias = this.formBuilder.group({
      username: [''],
      subAccount:false, 
    });
  }
  private buildForm(user?: any): void {
    let _this = this;
    this.datetimeOptionsEndTime['startDate'] = null;
    this.datetimeOptionsEndTime['endDate'] = null;
    this.datetimeOptionsEndTime['autoSelect'] = false;
    this.datetimeOptionsStartTime['startDate'] = null;
    this.datetimeOptionsStartTime['endDate'] = null;
    this.datetimeOptionsStartTime['autoSelect'] = false;

    if (user) {
      this.formUserAlias = this.formBuilder.group({
        username: [user.username || "", Validators.required],
        role: [user.roleId || 0, Validators.required],
        name: [user.name || ""],
        email: [user.email || '', [Validators.required, Validators.email]],
        phone: [user.phone || ''],
        startTime: [user.startTime || ''],
        endTime: [user.endTime || ''],
        aliasId: [user.aliasId || 0],
      });
      if(user.endTime){
        this.datetimeOptionsEndTime['startDate'] = this.userDateAdv.transform(user.endTime,'YYYY-MM-DD HH:mm:ss');
        this.datetimeOptionsEndTime['endDate'] = this.userDateAdv.transform(user.endTime,'YYYY-MM-DD HH:mm:ss');
        this.datetimeOptionsEndTime['autoSelect'] = true;
      }
      if(user.startTime){
        this.datetimeOptionsStartTime['startDate'] = this.userDateAdv.transform(user.startTime,'YYYY-MM-DD HH:mm:ss');
        this.datetimeOptionsStartTime['endDate'] = this.userDateAdv.transform(user.startTime,'YYYY-MM-DD HH:mm:ss');
        this.datetimeOptionsStartTime['autoSelect'] = true;
      }
      
    }
    else {
      this.formUserAlias = this.formBuilder.group({
        username: ['', Validators.required],
        role: ['', Validators.required],
        name: [''],
        email: ['', [Validators.required, Validators.email]],
        phone: [''],
        startTime: [''],
        endTime: [''],
      });
    }
    this.listDeviceSelected = null;
    this.isResetForm = true;
   
  }

  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', size: "lg", backdrop: "static" }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getListRoleAlias(userId?) {
    let params = {
      pageNo: -1,
      userId: userId || this.userIdSelected
    };
    let _this = this;
    this.roleAlias.getListRoles(params)
      .pipe(
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.listRoles = result.result;
          setTimeout(function () {
            $('.kt_selectpicker').selectpicker();
            if (_this.isEdit) {
              $('#roleAlias').val(_this.userEdit.roleId).selectpicker('refresh');
            }
          }, 100);
        }
      })
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10
    };
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  buttonAddNew(content) {
    let _this = this;
    this.getListRoleAlias();
    this.isEdit = false;
    // this.formUserAlias.reset();
    this.buildForm();
    this.open(content);
    this.formUserAlias.controls['username'].setValue(`${this.userSelected.username}_`);
    this.formUserAlias.updateValueAndValidity();
    // setTimeout(() => {
    //   $(".datetimepicker-user-alias").datepicker({
    //     autoclose: true,
    //     clearBtn: true,
    //     todayHighlight: true,
    //     format: _this.currentUser.dateFormat.toLowerCase(),
    //   });
    // });
  }


  changeUserTree(value) {
    if (value.id > 0) {
      if (this.userIdSelected != value.id) this.isChangeUser = true;
      this.userSelectedOld = this.userSelected;
      this.userSelected = value;
      this.userIdSelected = value.id;
      this.userId = value.id;
    }
    this.search();
  }

  changeChecked(event){
    this.listChecked = event.listChecked;
    this.countChecked = event.countChecked;
  }

  editUserAlias(item, content) {

    let _this = this;
    this.getListRoleAlias( item.aliasId);
    this.isEdit = true;
    this.idUserAliasEdit = item.id;
    this.userEdit = item;
    let params = {
      id: item.id,
      userId: item.aliasId
      //  this.userIdSelected
    };
    this.UserAlias.list(params).pipe(
      tap((data: any) => {
        data.result.content = data.result.content.map(item => {
          item.roleId = parseInt(item.roleId);
          return item;
        })
        this.dataDefault = data.result.content;
        let deviceIds = [];
        this.dataDefault[0].devices.map(device=>{
          deviceIds.push(device.id);
          return device;
        });
        let groupIds = [];
        this.dataDefault[0].deviceGroups.map(group=>{
          groupIds.push(group.id);
          return group;
        });
        this.dataDefaultComponent = {
          listDeviced:deviceIds,
          listGrouped:groupIds
        };
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
    let idGroupDevice = [];
    item.deviceGroups.map(item=>{
      idGroupDevice.push(item.id);
    });
    // list device
    let iDevice = [];
    item.devices.map(item=>{
      iDevice.push(item.id);
    });

    $(function(){
      $('#select-device-group').selectpicker('val',idGroupDevice);
    })
   

    this.buildForm(item);
    this.open(content);
  }

  deleteUser() {
    let id = this.idUserAction;
    let params = {
      userId: this.userIdSelected,
      id: id
    } as UserAlias
    this.UserAlias.delete(params, { notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.getData();
        this.modalService.dismissAll();
      }
    });
  }

  onHideModal(id: string) {
    let idModal = '#' + id;
    $(idModal).modal('hide');
  }

  openModal(params) {
    let idModal = params.idModal;
    let confirm = params.confirm;
    if (confirm) {
      $(idModal).appendTo('body').modal({
        backdrop: 'static',
        keyboard: false
      })
    } else {
      $(idModal).appendTo('body').modal('show');
    }
  }

  copied(val) {
    this.toast.copied(val);
  }

  getIdAction(id, idModal) {
    this.idUserAction = id;
    this.modalService.open(idModal, { windowClass: 'kt-mt-50  modal-holder modal-delete' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  resetPassword() {
    let params = {
      id: this.idUserAction,
      userId: this.userIdSelected
    };
    this.UserAlias.resetPassword(params, { notifyGlobal: true }).subscribe((result: any) => {
      this.modalService.dismissAll();
    });
  }

  searchUserAlias(form: any) {
    this.filter.username = form.value.username;
    this.filter['includeSubAccount'] = form.value.subAccount;
    this.getData();
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter = option;
        this.getData();
      }),

    ).subscribe();
  }

  get f() {
    if (this.formUserAlias != undefined) return this.formUserAlias.controls;
  }

  search() {
    let listSearch = [];

    if (this.userIdSelected != undefined) {
      listSearch.push({ key: 'userId', value: this.userIdSelected });
    }
    this.dataTable.search(listSearch);
  }

  getData() {
    this.filter['includeSubAccount'] = this.searchFormUserAlias.value.subAccount;
    this.UserAlias.list(this.filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          $(function () {
            $('select.selectpicker').selectpicker();
          });
        }
      })
  }
  onSubmit(form?: any) {

    if (this.formUserAlias.invalid) {
      return;
    }
    let params = {
      deviceIds:this.listDeviceSelected,
      deviceGroups:this.listDeviceGroupSelected,
      username: this.formUserAlias.get('username').value,
      name: this.formUserAlias.get('name').value,
      email: this.formUserAlias.get('email').value,
      phone: this.formUserAlias.get('phone').value,
      userId: this.userIdSelected,
      roleId: this.formUserAlias.get('role').value,
      startTime: this.formUserAlias.get('startTime').value ,
      endTime: this.formUserAlias.get('endTime').value,
    } as UserAlias;


    if (this.isEdit) {
      params.id = this.idUserAliasEdit;
      this.UserAlias.update(params, { notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          this.getData();
          this.closeModal();
        }
      });
      return;
    }
    this.UserAlias.create(params, { notifyGlobal: true })
      .pipe(
        tap((result:any)=>{
          if (result.status == 201) {
            this.getData();
            this.closeModal();
          }
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe();
  }
  openTabAdvanced() {
    if (this.isResetForm) {
      this.isResetForm = false;
    }

    let paramsEmiter = {
      userId:this.userIdSelected,
      dataDefault: {
        listDeviced:[],
        listGrouped:[]
      }
    }

    if(this.isEdit)
    {
       paramsEmiter = {
        userId:this.userIdSelected,
        dataDefault: this.dataDefaultComponent
      }
    }


  
    this.userIdChangeEmiter.emit(paramsEmiter);
  }
 
  changeListDeviceGroup(event)
  {
    this.listDeviceGroupSelected = event.listGroup;
    this.listDeviceSelected = event.listDevice;
  }
  
  onWidgetCheckChange(event) {
    this.listDeviceSelected = event.listChecked.map((x) => {
      return {
        id: x.id
      }
    });
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
      }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();

  }

  closeModal() {
    this.modalService.dismissAll();
  }

  onChangeEndTime(data) {
    if(data.endDate!=null){
      this.formUserAlias.get("endTime").setValue( data.endDate.format("YYYY-MM-DD HH:mm:ss"));
    }
    else{
      this.formUserAlias.get("endTime").setValue("");
    }
    // this.formUserAlias.get("endTime").setValue( data.endDate.format("YYYY-MM-DD HH:mm:ss"));
  }
  onChangeStartTime(data) {
    if(data.endDate!=null){
      this.formUserAlias.get("startTime").setValue( data.endDate.format("YYYY-MM-DD HH:mm:ss"));
    }
    else{
      this.formUserAlias.get("startTime").setValue("");
    }
    
  }

  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        width: 20,
        class: '',
        translate: '#',
        autoHide: false,
      },
      {
        title: 'Username',
        field: 'username',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px', 'margin': '0px' },
        width: 110,
        class: '',
        translate: 'COMMON.COLUMN.USERNAME',
        autoHide: false,
      },
      {
        title: 'Role',
        field: 'role',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px', 'margin': '0px' },
        width: 110,
        class: '',
        translate: 'MANAGE.USER.GENERAL.ROLE_NAME',
        autoHide: false,
      },
      {
        title: 'Full name',
        field: 'groupName',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '130px' },
        width: 110,
        class: '',
        translate: 'MANAGE.USER.GENERAL.FULL_NAME',
        autoHide: true,
      },
      {
        title: 'Phone',
        field: 'groupName',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        width: 110,
        class: '',
        translate: 'MANAGE.USER.GENERAL.PHONE',
        autoHide: false,
      },
      {
        title: 'Email',
        field: 'groupName',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        width: 110,
        class: '',
        translate: 'MANAGE.USER.GENERAL.EMAIL',
        autoHide: true,
      },
      {
        title: 'Status',
        field: 'groupName',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '104px', 'text-align': 'center' },
        width: 104,
        class: '',
        translate: 'COMMON.COLUMN.STATUS',
        autoHide: false,
      },
      {
        title: 'Create At',
        field: 'createdAt',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '78px' },
        width: 80,
        class: '',
        translate: 'COMMON.COLUMN.CREATED_DATE',
        autoHide: true,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        width: 117,
        class: '',
        translate: 'COMMON.COLUMN.ACTIONS',
        autoHide: false,
      },
    ]
  }

}
