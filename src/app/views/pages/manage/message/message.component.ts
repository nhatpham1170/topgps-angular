import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef,EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { finalize, takeUntil, tap ,debounceTime,delay} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { MessageService,message} from '@core/manage';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { select, Store } from '@ngrx/store';
import { AppState } from '@core/reducers';
import { currentUser } from '@core/auth';
import FroalaEditor from 'froala-editor';
import * as moment from 'moment';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';

declare var $: any;
@Component({
  selector: 'kt-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']

})
export class MessageComponent implements OnInit {
  @ViewChild('datePicker', { static: true }) 

  datePicker: DateRangePickerComponent;
  formSendMessage: FormGroup;
  searchFormMessage : FormGroup;
  formCheckList:FormGroup;
  selectdp: ElementRef;
  public parrentEmiter: EventEmitter<any | { id: number, path: string }>;

  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idMessageEdit: number;
  public dataDefault: any = [{}];
  public listUserEdit : any = [{}];
  public idMessageDelete: any;
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public userIdSelected:number;
  public countChecked:number;
  public userIdSelectedEdit:number;
  public showUserTree:boolean = true;
  public listSendUsers:any= [];
  public contentChange:any;
  public showLoaddingSearch:boolean = false;
  public countCharacters:number = 0;
  public dateStart: string;
  public dateEnd: string;
  public datePickerOptions: any = {
    size: 'md',
    select: 'from',
    ranges: 'from',
    optionDatePicker: {
      maxSpan: {
        days: 31
      }
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: true,
    startDate:moment().subtract(6, 'days'),
    endDate:moment() 
  };
  public modules={
    toolbar: [
      ['bold', 'italic', 'underline'],        // toggled buttons
      // ['blockquote', 'code-block'],
  
      // [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      // [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      // [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      // [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      // [{ 'direction': 'rtl' }],                         // text direction
  
      [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      // [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
  
      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      // [{ 'font': [] }],
      // [{ 'align': [] }],
  
      // ['clean'],                                         // remove formatting button
  
      // ['link', 'image', 'video']                         // link and image, video
    ],
  };
  constructor(
    private Message: MessageService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
 
    private store: Store<AppState>,

  ) {
    this.parrentEmiter = new EventEmitter();

    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    this.getData(this.userIdSelected);
    this.updateDataTable();

   }


   private buildForm(): void {
    this.formSendMessage = this.formBuilder.group({
      title: ['',Validators.required],
      contentMessage:['',Validators.required]
    });
    this.searchFormMessage = this.formBuilder.group({
      title: ['']
    });

  }

  searchFormMessageButton(form){
  
    this.filter = {
      pageNo: 1,
      pageSize: 10,
      timeFrom: this.dateStart,
      timeTo : this.dateEnd
    };
    if(form.value.title) this.filter.title = form.value.title;
    this.getData(this.userIdSelected);
  }
  
  open(content,classModal) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder '+classModal}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   this.showUserTreeFunction();
  }

  showUserTreeFunction(){
    let userModel = {
      type: 0,
    };
    let user$:any = this.store.pipe(select(currentUser));
    if (user$) {
      select(user$.subscribe(function (data) {
        Object.assign(userModel, data);
      }
      ).unsubscribe());
    }
      // code display use-tree
      let type = userModel.type;
      this.showUserTree = false;
      if(type == 0 || type == 1) this.showUserTree = true;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  
  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
      orderBy:'createdAt',
      orderType:'DESC'
    };
  }

  changeUser(value){
    if (value.id > 0) {
      this.userIdSelectedEdit = value.id;
    }
  }

  ChangeUserTree(value) {    
    if (value.id > 0) {
      this.userIdSelected = value.id;
    }
    this.search();
    }


  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }

  buttonAddNew(content) {
    this.countCharacters = 0;
    this.listSendUsers = [];
    this.listUserEdit = []; 
    this.isEdit = false;
    this.formSendMessage.reset();
    this.open(content,'modal-lg-900');
    // this.getDevices(this.userIdSelected);
  }

  editMessage(item,content) {
    this.isEdit = true;
    this.dataDefault = item;
    this.listUserEdit = item.userRevice;
    // console.log(this.listUserEdit);
    this.open(content,'modal-lg-900');
  }
  
  resetFormSearch(){
    this.datePicker.refresh();
    this.searchFormMessage.reset();

  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter = option;
        this.getData(this.userIdSelected);
      }),
      
    ).subscribe();
  }

  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
   }

  get f() {
   if(this.formSendMessage != undefined) return this.formSendMessage.controls;
  }

  search() {
    let listSearch = [];

    if (this.userIdSelected != undefined) {
      listSearch.push({ key: 'userId', value: this.userIdSelected });
    }
    this.dataTable.search(listSearch);
  }

 containsObject(obj) {
    var i;
    for (i = 0; i < this.listSendUsers.length; i++) {
        if (this.listSendUsers[i] === obj) {
            return true;
        }
    }
    return false;
}

  clickCheckbox(node)
  {
      let  item = node.data;
      if(this.containsObject(node)) 
      {
          this.removeSendPerson(node);
          return false;
      }
      this.listSendUsers.push(node);
      // console.log(this.listSendUsers);
  }

  removeSendPerson(node)
  {
    this.parrentEmiter.emit({id:node});
    //   console.log(id);
    this.listSendUsers.splice(this.listSendUsers.findIndex(v => v.data.id === node.data.id), 1);
  }

  getData(userId) {
    if(userId != undefined) this.filter.userId = userId;
    this.showLoaddingSearch = true;
    this.Message.listSend(this.filter)
      .pipe(
        delay(300),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          this.showLoaddingSearch = false;
          setTimeout(function () {
            $('.kt_selectpicker').selectpicker('refresh');
          }, 100);
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect, 
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
				selecter:false,
			  }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  getListId()
  {
      let listId = [];
      this.listSendUsers.forEach(element => {
          let id = element.data.id;
          listId.push(id);
      });
      return listId;
  }

  onSelectionChanged(event)
  {
    this.countCharacters = event.text.length - 1;
    // console.log(event.text.length);
  }

  deleteMessage(id,modal)
  {
    this.idMessageEdit = id;
    this.open(modal,'modal-delete');
  }

  deleteMessageSend()
  {
    this.Message.deleteSend(this.idMessageEdit,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 200) {
        this.getData(this.userIdSelected);
        this.modalService.dismissAll();
      }})
  }

  onSubmit(form: any) {
    // console.log(this.formSendMessage.invalid);
    if (this.formSendMessage.invalid) {
      return;
    }
    let params = {
      title:form.value.title.toString(),
      userIds:this.getListId().toString(),
      message:form.value.contentMessage.toString()
    } as message;
    // console.log(params);
    this.Message.createSend(params,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        this.dataTable.reload({});
        this.closeModal();
      }})
  }
  
  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'title message',
        field: 'imei',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '200px' },
        class: '',
        translate: 'MANAGE.MESSAGE.COLUMN.TITLE',
        autoHide: false,
        width: 100,
      },
      {
        title: 'content',
        field: 'command',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '400px' },
        class: '',
        translate: 'MANAGE.MESSAGE.COLUMN.CONTENT',
        autoHide: true,
        width: 200,
      },
      {
        title: 'count user ',
        field: 'created',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'MANAGE.MESSAGE.COLUMN.COUNT_USER',
        autoHide: true,
        width: 100,
      },
      {
        title: 'Sended',
        field: 'send',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px','text-align':'center' },
        class: '',
        translate: 'MANAGE.COMMAND.GENERAL.TiME_SEND',
        autoHide: true,
        width: 100,

      },

      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 117,
      },
    ]
  }


}




