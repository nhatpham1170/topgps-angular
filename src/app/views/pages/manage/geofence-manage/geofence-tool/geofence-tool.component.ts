
import { max } from 'moment';
import { Component, OnInit, ChangeDetectorRef, NgModule, Input, Output, EventEmitter, ComponentFactoryResolver, Injector } from '@angular/core';
import * as L from 'leaflet';
import { MapConfigModel, MapConfig, MapUtil, MapService, Item } from '@core/utils/map';
import { GeofenceService, GeofenceModel,GeofenceGroupService,GeofenceGroup } from '@core/manage';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { ValidatorCustomService, ToastService } from '@core/_base/layout';
import { TranslateService } from '@ngx-translate/core';
import { tap, takeUntil, finalize } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import { style } from '@angular/animations';

declare var $;
@Component({
  selector: 'kt-geofence-tool',
  templateUrl: './geofence-tool.component.html',
  styleUrls: ['./geofence-tool.component.scss'],
  // providers: [MapService]
})
export class GeofenceToolComponent implements OnInit {
  public mapConfig: MapConfigModel;

  @Input() listDevices: Array<any>;
  @Input() geofenceInput?: GeofenceModel;
  @Input() currentUser: any;
  @Input() listGeofenceGroup:any;
  @Output() geofenceResult: EventEmitter<{ status: string, message: string, data: any }>;
  public geofenceInfo: GeofenceModel;
  public geoFenceMap: GeofenceModel;
  public geofenceForm: FormGroup;
  public currentLayer: any;
  public map: L.Map;
  public latlngArr: FormArray;
  public isCricle: boolean;
  public optionsLayer: {
    color: string,
    fill: boolean,
    fillColor: string,
    fillOpacity: number,
    opacity: number,
    stroke: boolean
    weight: number,
  }

  public editableLayers = new L.FeatureGroup();
  public options;
  public drawOptions = {
    position: 'topright',
    draw: {
      polyline: false,
      circle: {
        shapeOptions: {
          // color: "#aaaaaa", // default #3388ff
          // fill: true,
          // fillColor: "red",// default #3388ff
          // fillOpacity: 0.2, // default 0.2
          // opacity: 0.5, // default 0.5
          // stroke: true, // default true
          // weight: 4,  // default: 4
        }
      },
      circlemarker: false,
      marker: false,
    },
    edit: {
      // featureGroup: this.editableLayers,
      edit: false,
      remove: false,
    },
  };
  public layers = {};
  public typeAction: string;
  public listData: Array<any>;
  public layerDevice: L.FeatureGroup;
  public currentPopup: L.Layer;
  public showListDevice: boolean = false;
  private mapService: MapService;
  constructor(
   
    private resolver: ComponentFactoryResolver,
    private injector: Injector,
    private cdr: ChangeDetectorRef,
    private geofenceService: GeofenceService,
    private mapUtil: MapUtil,
    private fb: FormBuilder,
    private validatorCT: ValidatorCustomService,
    private translate: TranslateService,
    private sanitizer: DomSanitizer,
    private toast:ToastService,
    ) {
    this.typeAction = "add";
    this.mapService = new MapService(this.resolver,this.injector);
    this.options = this.mapService.init();
    this.isCricle = false;
    this.mapConfig = this.mapService.getConfigs();
    this.listData = [];
    this.geofenceResult = new EventEmitter();
    let roadmap = L.tileLayer(this.mapConfig.mapType.roadmap.layer['link'], this.mapConfig.mapType.roadmap.layer['option']);
    let satellite = L.tileLayer(this.mapConfig.mapType.satellite.layer['link'], this.mapConfig.mapType.satellite.layer['option']);

    this.layers[this.translate.instant(this.mapConfig.mapType.roadmap.translate)] = roadmap;
    this.layers[this.translate.instant(this.mapConfig.mapType.satellite.translate)] = satellite;

  }

  ngOnInit() {
    setTimeout(()=>{
      $('.kt_selectpicker').selectpicker();
    });  
    this.listData = this.processData(this.listDevices);
    this.mapService.config.options.elementProcessText = "mapProcessText";
    if (this.geofenceInput) {
      this.typeAction = "edit";
      if(this.geofenceInput.encodedPoints === null )
      {
        this.geofenceInput.encodedPoints = this.mapUtil.encode(this.geofenceInput.points);
      }
      this.geofenceInfo = new GeofenceModel(this.geofenceInput);
      let points = this.mapUtil.decode(this.geofenceInfo.encodedPoints);

      //geofence group select
      let id = [''];
      if(this.geofenceInput.geofenceGroups && this.geofenceInput.geofenceGroups.length > 0)
      {
        id = [];
        this.geofenceInput.geofenceGroups.map(geofence=>{
          if(typeof geofence == 'object')
          {
            id.push(geofence.id);
          }else{
            id.push(geofence);
          }
          return geofence;
        })      
      }
      $(function(){
        $('#typeGroupGeofence').selectpicker('val',id);
      })
    }
    else {
      this.geofenceInfo = new GeofenceModel();
    }
    this.geofenceInfo = new GeofenceModel();
    this.optionsLayer = {
      color: this.geofenceInfo.color,
      fill: this.geofenceInfo.fill,
      fillColor: this.geofenceInfo.fillColor,
      fillOpacity: this.geofenceInfo.fillOpacity,
      opacity: this.geofenceInfo.opacity,
      stroke: this.geofenceInfo.stroke,
      weight: this.geofenceInfo.weight,
    };

    this.createForm(this.geofenceInfo);
    this.geofenceInfo.userId = this.currentUser.id;
    // let clientRectHeader = document.getElementById('geofenceHeader').getBoundingClientRect();     
    // let clientRectFooter = document.getElementById('geofenceFooter').getBoundingClientRect();
    // let clientRect = document.getElementById('geofenceConfig').getBoundingClientRect();
    // let maxHeightWapper = clientRect.height - (clientRectFooter.height);
    // $("#geofenceBody").css("max-height", maxHeightWapper);

    this.cdr.detectChanges();
  }
  onDrawReady(drawControl) {
    // Do stuff with map     
  }

  onMapReady(map) {
    this.map = map;
    L.control.layers(this.layers).addTo(this.map);
    this.mapService.setMap(this.map);
    this.renderGeofence(this.geofenceInput, true);
  }


  createForm(geofence: GeofenceModel) {

    // covert points
    geofence.latlngs.map(x => {
      x.lat = parseFloat(parseFloat(x.lat.toString()).toFixed(5));
      x.lng = parseFloat(parseFloat(x.lng.toString()).toFixed(5));
      return x;
    });
    geofence.points = geofence.latlngs.map(x => {
      return [x.lat, x.lng]
    });
    if (geofence.radius) geofence.radius = parseFloat(parseFloat(geofence.radius.toString()).toFixed(5));
    this.geofenceForm = this.fb.group({
      name: new FormControl({ value: geofence.name, disabled: false }, [Validators.required, this.validatorCT.maxLength(64)]),
      typeGroupGeofence: new FormControl({ value: geofence.geofenceGroups, disabled: false }, []),
      description: new FormControl({ value: geofence.description, disabled: false }, [this.validatorCT.maxLength(256)]),
      radius: new FormControl({ value: geofence.radius, disabled: false }, [this.validatorCT.float]),
      color: new FormControl({ value: geofence.color, disabled: false }),
      opacity: new FormControl({ value: geofence.opacity * 10, disabled: false }),
      fillColor: new FormControl({ value: geofence.fillColor, disabled: false }),
      fillOpacity: new FormControl({ value: geofence.fillOpacity * 10, disabled: false }),
      latlngArr: this.fb.array([])
    });

    this.geofenceInfo.neLat = geofence.neLat 
    this.geofenceInfo.neLng = geofence.neLng 
    this.geofenceInfo.swLat = geofence.swLat 
    this.geofenceInfo.swLng = geofence.swLng 

    this.geofenceInfo.latLngBounds = geofence.latLngBounds;
    
    if (geofence.latlngs.length > 0) {
      this.latlngArr = this.geofenceForm.get('latlngArr') as FormArray;
      this.latlngArr.clear();
      geofence.latlngs.map(x => {
        this.latlngArr.push(this.createLatlng(x));
      });
    }
    // $(function(){
    //   $('#typeGroupGeofence').selectpicker('val', geofence.groupIds);

    // })

    // setTimeout(()=>{
    //   // let id = '';
   
      
    //   // if(typeof geofence.geofenceGroups == 'string')
    //   // {
        
    //   //   id = geofence.geofenceGroups;
    //   // }else
    //   // {
    //   //   if(geofence.geofenceGroups && geofence.geofenceGroups.length > 0)
    //   //   {
    //   //     id = geofence.geofenceGroups[0].id;
    
    //   //   }
    //   // }
     
    //   $('#typeGroupGeofence').selectpicker('val', [17]);
    // })
   
    this.geofenceInfo = Object.assign(this.geofenceInfo, geofence);
    this.updateLayout();

  }
  getOptions(geofence: GeofenceModel) {
    return {
      color: geofence.color,
      fill: geofence.fill,
      fillColor: geofence.fillColor,
      fillOpacity: geofence.fillOpacity,
      opacity: geofence.opacity,
      stroke: geofence.stroke,
      weight: geofence.weight,
    };
  }
  renderGeofence(geofence: GeofenceModel, fitBound?: boolean) {
    // console.log(geofence);
    let _this = this;
    if (geofence) {
      let polygon1: any = new L.Rectangle([
        [geofence.neLat,geofence.neLng],
        [geofence.neLat,geofence.swLng],
        [geofence.swLat,geofence.swLng],
        [geofence.swLat,geofence.neLng],
      ],{ color:'#abb8c9'});
      // polygon1.options = {
      //   color:'#abb8c9',
        
      // };
      if(polygon1) polygon1.addTo(this.map);
      this.isCricle = false;
      if (this.currentLayer) this.currentLayer.remove();
      let layer = new L.FeatureGroup();
      switch (geofence.type) {
        case "polygon":
          let polygon: any = new L.Polygon(geofence.points);
          this.currentLayer = polygon;
          this.currentLayer.options = this.getOptions(geofence);
          this.currentLayer.layerType = "polygon";
          this.currentLayer.editing.enable();
          this.currentLayer.on('edit', function (e) {
            _this.layerEdited(e.target);
            _this.geofenceInfo.latLngBounds = _this.currentLayer.getBounds(),{padding:[0,0]};
            _this.geofenceInfo.neLat = _this.geofenceInfo.latLngBounds['_northEast'].lat;
            _this.geofenceInfo.neLng = _this.geofenceInfo.latLngBounds['_northEast'].lng;
            _this.geofenceInfo.swLat = _this.geofenceInfo.latLngBounds['_southWest'].lat;
            _this.geofenceInfo.swLng = _this.geofenceInfo.latLngBounds['_southWest'].lng; 
          });

          geofence.latlngs = geofence.points.map(x => {
            return { lat: x[0], lng: x[1] };
          })
          // this.setLatlngForm(latlngs);
         
          this.createForm(geofence);
          this.currentLayer.addTo(this.map);
          if (fitBound) {
            let _this= this;
            setTimeout(()=>{
              _this.map.fitBounds(_this.currentLayer.getBounds(),{padding:[50,50]});
            })
          }
          break;
        case "circle":
          this.isCricle = true;
          let circle: any = new L.Circle(geofence.points[0], geofence.radius);
          this.currentLayer = circle;
          this.currentLayer.options = this.getOptions(geofence);
          this.currentLayer.layerType = "circle";
          this.currentLayer.editing.enable();
          this.currentLayer.on('edit', function (e) {
            _this.layerEdited(e.target);
            _this.geofenceInfo.latLngBounds = _this.currentLayer.getBounds(),{padding:[0,0]};
            _this.geofenceInfo.neLat = _this.geofenceInfo.latLngBounds['_northEast'].lat;
            _this.geofenceInfo.neLng = _this.geofenceInfo.latLngBounds['_northEast'].lng;
            _this.geofenceInfo.swLat = _this.geofenceInfo.latLngBounds['_southWest'].lat;
            _this.geofenceInfo.swLng = _this.geofenceInfo.latLngBounds['_southWest'].lng; 
          });
          geofence.latlngs = geofence.points.map(x => {
            return { lat: x[0], lng: x[1] };
          })
          this.createForm(geofence);
          this.currentLayer.addTo(this.map);
          if (fitBound) {
            let _this= this;
            setTimeout(()=>{
              _this.map.fitBounds(_this.currentLayer.getBounds(),{padding:[50,50]});
            })
          }
          break;
      }

    }else{
      if (this.currentLayer) this.currentLayer.remove();
      this.geofenceInfo = new GeofenceModel();
      this.createForm(this.geofenceInfo);
    }

  }
  onReset() {
    this.renderGeofence(this.geofenceInput, true);
  }


  renderLatlngs(latlngs, radius, type: string) {

    this.geofenceInfo.radius = radius || 0;
    this.geofenceInfo.type = type || "";
    this.geofenceInfo.geofenceGroups = this.geofenceForm.value.typeGroupGeofence;
    
    this.geofenceInfo.points = latlngs.map(x => {
      return [x.lat, x.lng];
    })
    this.geofenceInfo.latlngs = latlngs;
    this.createForm(this.geofenceInfo);
    this.cdr.detectChanges();
  }
  setLatlngForm(latlngs: Array<{ lat: number, lng: number }>) {
    this.latlngArr = this.geofenceForm.get('latlngArr') as FormArray;
    this.latlngArr.clear();
    latlngs.map(x => {
      this.latlngArr.push(this.createLatlng(x));
    });
  }
  onCreated(layer) {
    layer.layer.editing.enable();
    this.editableLayers.addLayer(layer.layer);
    let _this = this;
    if (this.currentLayer) {
      this.currentLayer.remove();
    }
    this.currentLayer = layer.layer;
    let latlngs;
    let radius;
    let type;
    this.isCricle = false;
    switch (layer.layerType) {
      case "rectangle":
        let rectangle: L.Rectangle = layer.layer;
        latlngs = rectangle.getLatLngs()[0];
        type = "polygon";
        break;
      case "circle":
        let circle: L.Circle = layer.layer;
        latlngs = [circle.getLatLng()];
        radius = circle.getRadius();
        // circle.setStyle(this.optionsLayer);
        this.isCricle = true;
        type = "circle";
        break;
      case "polygon":
        let polygon: L.Polygon = layer.layer;
        latlngs = polygon.getLatLngs()[0];

        type = "polygon";
        break;
    }
    this.currentLayer['layerType'] = layer.layerType;
    // set options
    this.currentLayer.options = this.optionsLayer;
    this.currentLayer.on('edit', function (e) {
      _this.layerEdited(e.target);
    });
    this.geofenceInfo.latLngBounds = this.currentLayer.getBounds(),{padding:[0,0]};

    this.geofenceInfo.neLat = this.geofenceInfo.latLngBounds['_northEast'].lat;
    this.geofenceInfo.neLng = this.geofenceInfo.latLngBounds['_northEast'].lng;
    this.geofenceInfo.swLat = this.geofenceInfo.latLngBounds['_southWest'].lat;
    this.geofenceInfo.swLng = this.geofenceInfo.latLngBounds['_southWest'].lng;
    this.renderLatlngs(latlngs, radius, type);
  }

  layerEdited(layer) {
    let latlngs;
    let radius;
    this.isCricle = false;
    this.geofenceInfo.latLngBounds = this.currentLayer.getBounds(),{padding:[0,0]};

    this.geofenceInfo.neLat = this.geofenceInfo.latLngBounds['_northEast'].lat;
    this.geofenceInfo.neLng = this.geofenceInfo.latLngBounds['_northEast'].lng;
    this.geofenceInfo.swLat = this.geofenceInfo.latLngBounds['_southWest'].lat;
    this.geofenceInfo.swLng = this.geofenceInfo.latLngBounds['_southWest'].lng;  
    let type;
    switch (layer.layerType) {
      case "rectangle":
        let rectangle: L.Rectangle = layer;
        latlngs = rectangle.getLatLngs()[0];
        type = "polygon";
        break;
      case "circle":
        let circle: L.Circle = layer;
        latlngs = [circle.getLatLng()];
        radius = circle.getRadius();
        this.isCricle = true;
        type = "circle";
        break;
      case "polygon":
        let polygon: L.Polygon = layer;
        latlngs = polygon.getLatLngs()[0];
        type = "polygon";
        break;
    }
    this.renderLatlngs(latlngs, radius, type);
  }
  onResize($elm) {
    this.mapService.resize();

    // let clientRectFooter = document.getElementById('geofenceFooter').getBoundingClientRect();
    // let clientRect = document.getElementById('geofenceConfig').getBoundingClientRect();
    // let maxHeightWapper = clientRect.height - (clientRectFooter.height);

    // setTimeout(() => {
    //   $("#geofenceBody").css("max-height", maxHeightWapper);
    // });

    this.cdr.detectChanges();
  }
  updateLayout() {
    this.mapService.resize();
    // let clientRectFooter = document.getElementById('geofenceFooter').getBoundingClientRect();
    // let clientRect = document.getElementById('geofenceConfig').getBoundingClientRect();
    // let maxHeightWapper = clientRect.height - (clientRectFooter.height);

    // setTimeout(() => {
    //   $("#geofenceBody").css("max-height", maxHeightWapper);
    // });

    this.cdr.detectChanges();
  }
  onSubmit() {
    this.geofenceForm.markAllAsTouched();
    setTimeout(() => {
      const firstElementWithError = document.querySelector('#formGeofenceTool .invalid-feedback');
      if (firstElementWithError) {
        firstElementWithError.scrollIntoView({ block: 'center' });
      }
    });

    if (!this.geofenceForm.invalid && this.geofenceInfo.type.length > 0) {
      this.geofenceInfo.encodedPoints = this.mapUtil.encode(this.geofenceInfo.points);
       
      if (this.typeAction == "edit") {
        this.geofenceService.update(this.geofenceInfo, { notifyGlobal: true }).pipe(
          tap(data => {
            if (data.status == 200) {
              this.geofenceResult.emit({ status: "success", message: "", data: data.result });
            }
          }),
          finalize(() => {
            this.cdr.markForCheck();
          })
        ).subscribe();
      }
      else {
        this.geofenceService.create(this.geofenceInfo, { notifyGlobal: true }).pipe(
          tap(data => {
            if (data.status == 201) {
              this.geofenceResult.emit({ status: "success", message: "", data: data.result });
            }
          }),
          finalize(() => {
            this.cdr.markForCheck();
          })
        ).subscribe();
      }
    }
    else {
      if(!this.geofenceForm.invalid){
        this.toast.show({type:'warning',translate:"MANAGE.GEOFENCE.TOOL.PLEASE_CREATE_GEOFENCE"})
      }
      
      
      // console.log(this.geofenceInfo);

      // console.log( this.geofenceInfo.points);
      // console.log( this.geofenceInfo.latlngs.map(x=>{ return JSON.stringify(x)}).join(","));
    }
  }
  formChange(value) {
    let _this = this;
    // this.geofenceForm.markAllAsTouched();
    this.geofenceInfo.groupIds = $('#typeGroupGeofence').val();
    if (this.geofenceForm.invalid && !this.geofenceForm.get('name').errors) return;
    this.geofenceInfo.name = this.geofenceForm.get('name').value;
    this.geofenceInfo.description = this.geofenceForm.get('description').value;
    this.geofenceInfo.radius = Number.parseFloat(this.geofenceForm.get('radius').value);
    this.geofenceInfo.color = this.geofenceForm.get('color').value;
    this.geofenceInfo.opacity = this.geofenceForm.get('opacity').value / 10;
    this.geofenceInfo.fillColor = this.geofenceForm.get('fillColor').value;
    this.geofenceInfo.groupIds = $('#typeGroupGeofence').val();
    // console.log(this.geofenceInfo.groupIds);
    this.geofenceInfo.geofenceGroups = this.geofenceForm.get('typeGroupGeofence').value;

    this.geofenceInfo.fillOpacity = this.geofenceForm.get('fillOpacity').value / 10;
    
    // set point
    if (this.formLatLngs.value.length > 0) {
      this.geofenceInfo.points = this.formLatLngs.value.map(x => {
        return [x.lat, x.lng];
      })
    }


    // set options
    this.optionsLayer = {
      color: this.geofenceInfo.color,
      fill: this.geofenceInfo.fill,
      fillColor: this.geofenceInfo.fillColor,
      fillOpacity: this.geofenceInfo.fillOpacity,
      opacity: this.geofenceInfo.opacity,
      stroke: this.geofenceInfo.stroke,
      weight: this.geofenceInfo.weight,
    };

    this.renderGeofence(this.geofenceInfo, true);

    // if (this.currentLayer) {
    //   let layerTemp = this.currentLayer;
    //   this.currentLayer.remove();
    //   this.currentLayer = layerTemp;
    //   this.currentLayer.editing.enable();
    //   this.currentLayer.on('edit', function (e) {
    //     _this.layerEdited(e.target);
    //   });
    //   this.currentLayer.options = this.optionsLayer;
    //   this.currentLayer.addTo(this.map);
    // }

  }
  createLatlng(latlng: { lat: number, lng: number }): FormGroup {
    return this.fb.group({
      lat: [latlng.lat, this.validatorCT.float],
      lng: [latlng.lng, this.validatorCT.float]
    });
  }
  get formLatLngs(): FormArray {
    if (this.geofenceForm)
      return this.geofenceForm.get('latlngArr') as FormArray;
  }

  get f() {
    if (this.geofenceForm) return this.geofenceForm.controls;
  }
  onSetting($elm) {
    // let clientRectHeader = document.getElementById('geofenceHeader').getBoundingClientRect();
    // let clientRectFooter = document.getElementById('geofenceFooter').getBoundingClientRect();
    // let clientRect = document.getElementById('geofenceConfig').getBoundingClientRect();
    // let maxHeightWapper = clientRect.height - (clientRectFooter.height);
    // $("#geofenceBody").css("max-height", maxHeightWapper);
    this.cdr.detectChanges();
  }
  processData(data) {
    let _this = this;
    const icons = _this.mapConfig.icons;
    const status = this.mapConfig.status;
    let dataNew = data.map(x => {
      let indexIcon = 0;
      if (x['icon']['name']) {
        let indexIconSearch = _this.mapConfig.icons.findIndex(i => i.name == x['icon']['name']);
        if (indexIconSearch >= 0) {
          indexIcon = indexIconSearch;
        }
      }
      x.iconType = Object.assign({}, icons[indexIcon]);
      x.iconTypeProcess = Object.assign({}, icons[indexIcon]);

      if (x['lat'] == "null") x['lat'] = null;
      if (x['lng'] == "null") x['lng'] = null;

      x.iconTypeProcess.icon = x.iconTypeProcess.icon
        .replace('viewBox="0 0 34 34"', 'viewBox="0 0 24 24"')
        .replace('viewBox="0 0 48 48"', 'viewBox="0 0 38 38"')
        .replace('transform="rotate({iconRotate})"', "")
        .replace('{iconFill}', "#fff");
      x.iconTypeProcess.icon =
        _this.sanitizer.bypassSecurityTrustHtml(x.iconTypeProcess.icon);

      switch (x.status) {
        case "lost_gps":
          x.statusType = status.lostGPS;
          break;
        case "lost_gprs":
          x.statusType = status.lostGPRS; break;
        case "history_transfer":
          x.statusType = status.historyTransfer;
          break;
        case "expired":
          x.statusType = status.expired;
          break;
        case "stop":
          x.statusType = status.stop;
          break;
        case "run":
          x.statusType = status.run;
          break;
        case "inactive":
          x.statusType = status.inactive;
          break;
        case "nodata":
          x.statusType = status.nodata;
          break;
        default:
          x.statusType = status.lostSignal;
          break;
      }

      let item = new Item(x);
      return item;
    });
    return dataNew;
  }

  // public deviceMarkers: Array<L.Layer> = [];
  onWidgetCheckChange(data) {
    if (this.layerDevice) this.layerDevice.remove();
    if (data.listChecked.length > 0) {
      this.layerDevice = new L.FeatureGroup(); data.listChecked.map(x => {
        let marker = this.mapService.createDevice(x, this.currentPopup, this.map, "style-one");
        if (marker) this.layerDevice.addLayer(marker);
      });
      this.layerDevice.addTo(this.map);
      if(this.layerDevice.getLayers().length>0){
        this.map.fitBounds(this.layerDevice.getBounds(), { padding: [50, 50] });
      }
    }

  }
  changeShowListDevice() {
    this.showListDevice = !this.showListDevice;
    this.cdr.detectChanges();
  }

}
