import { Component, OnInit, Input, ElementRef, ChangeDetectorRef,ViewChild} from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { finalize, takeUntil, tap ,debounceTime,distinctUntilChanged,map,filter} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { DeviceGroup,DeviceGroupService,DeviceService} from '@core/manage';
import { DomSanitizer} from '@angular/platform-browser';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { UserTreeService } from '@core/common/_service/user-tree.service';

declare var $: any;
@Component({
  selector: 'kt-device-group',
  templateUrl: './device-group.component.html',
  styleUrls: ['./device-group.component.scss'],

})
export class DeviceGroupComponent implements OnInit {
  formDeviceGroup: FormGroup;
  searchFormDeviceGroup : FormGroup;
  selectdp: ElementRef;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idDeviceGroupEdit: number;
  public dataDefault: any = [{}];
  public idDeviceGroupDelete: any;
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public listDevices : any = [];
  public checkallboxed:boolean;
  public userIdSelected:number;
  public keySearchFilter:string = 'name';
  public listChecked:any =[];
  public countChecked:number = 0;
  public showUserTree = true;
  constructor(
    private DeviceGroup: DeviceGroupService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private sanitizer:DomSanitizer,
    private modalService: NgbModal,
    private deviceService: DeviceService,
    private userTreeService: UserTreeService,


  ) {
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    
    this.updateDataTable();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();
   }

  private buildForm(): void {
    this.formDeviceGroup = this.formBuilder.group({
      nameDeviceGroup: ['',Validators.required]
    });
    this.searchFormDeviceGroup = this.formBuilder.group({
      nameDeviceGroup: [''],
    })
  }

  changeChecked(event){
    this.listChecked = event.listChecked;
    this.countChecked = event.countChecked;
  }

  buttonAddNew(content) {
    this.isEdit = false;
    this.getListDevices(content);
    this.formDeviceGroup.reset();
  }

  open(content) {           
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder',size: 'lg'  }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  onSubmitDeviceGroup(form: any) {
    if (this.formDeviceGroup.invalid) {
      return;
    }
    let params = {
      userId: this.userIdSelected,
      name: form.value.nameDeviceGroup,
      deviceIds : this.listChecked
    } as DeviceGroup;
    if (this.isEdit) {
      params.id = this.idDeviceGroupEdit;
      this.DeviceGroup.update(params,{ notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          this.getData(this.userIdSelected);
          this.closeModal();
        }
      });
      return;
    }
    this.DeviceGroup.create(params,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        this.getData(this.userIdSelected);
        this.closeModal();

      }})
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10
    };
  }

  onHideModal(id: string) {
    let idModal = '#' + id;
    $(idModal).modal('hide');
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }

  search() {
    let listSearch = [];

    if (this.userIdSelected != undefined) {
      listSearch.push({ key: 'user_id', value: this.userIdSelected });
    }
    this.dataTable.search(listSearch);
  }

  ChangeUserTree(value) {
    if (value.id > 0) {
      this.userIdSelected = value.id;
    }
    this.search();
  }


  getIdAction(id,idModal) {
    this.idDeviceGroupDelete = id;
    this.openModal({idModal:idModal,confirm : true});
  }

  openModal(params) {
     let idModal = params.idModal;
     let confirm = params.confirm;
     if(confirm)
     {$(idModal).appendTo('body').modal({backdrop: 'static',keyboard: false})}
     else{$(idModal).appendTo('body').modal('show');}
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  deleteDeviceGroup() {
    let id = this.idDeviceGroupDelete;
    this.DeviceGroup.delete(id,{ notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.getData(this.userIdSelected);
      }
    });
    this.onHideModal('modal-delete-DeviceGroup');
  }

   editDeviceGroup(id,content){
    this.isEdit = true;
    this.idDeviceGroupEdit = id;
    let params = {
      userId : this.userIdSelected
    }    
     this.DeviceGroup.detail(params,id).toPromise().then(data=>{
      this.dataDefault =  [data.result];
      this.listDevices = data.result.devices;    
    });
    this.open(content);  
  }

  //checked
  
  //end checked

  getListDevices(content){
    let option : any = [];
    option['pageNo'] = -1;
    if(this.userIdSelected != undefined)    option['userId'] = this.userIdSelected;

    // option['orderBy'] = 'imei';
    // option['orderType'] = 'DESC';

    this.deviceService.list({ params: option }).pipe(
    ).subscribe((data: any) => {
      this.dataDefault = [{}];
      this.listDevices = data.result;
    });
    this.open(content);
  }

  loadSelectBootstrap(){
    $('.bootstrap-select').selectpicker();
  }

  searchDeviceGroup(form:any)
  {
    this.filter.name = form.value.nameDeviceGroup;
    this.getData(this.userIdSelected);
  }

  resetFormSearch(){
    this.searchFormDeviceGroup.reset();
    this.filter.name = "";
    this.getData(this.userIdSelected);
    
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter = option;       
        this.getData(this.userIdSelected);
      }),
    ).subscribe();
  }

  get f() {
   if(this.formDeviceGroup != undefined) return this.formDeviceGroup.controls;
  }

  getData(userId) {
    if(userId != undefined) this.filter.user_id = userId;
    this.DeviceGroup.listDeviceGroup(this.filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
        //example data
        this.data = result.result.content;
        this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          setTimeout(() => {
            $('select.selectpicker').selectpicker('refresh');
          });
          
        }
      })
  }


 sanitize(url:string){
      return this.sanitizer.bypassSecurityTrustUrl(url);
    }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout:{
        body:{
          scrollable:false,
          maxHeight:600,
        },
        selecter:false,
        }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();

  }
  
  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Name',
        field: 'name',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'COMMON.COLUMN.NAME',
        autoHide: false,
        width: 100,
      },
      {
        title: 'Count',
        field: 'count',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '120px','text-align': 'center' },
        class: '',
        translate: 'MANAGE.DEVICE_GROUP.COLUMN.COUNT_DEVICE',
        autoHide: true,
        width: 100,
      },
      {
        title: 'Edit by',
        field: 'editBy',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px','text-align': 'center' },
        class: '',
        translate: 'COMMON.COLUMN.CREATED_BY',
        autoHide: true,
        width: 100,
      },
      {
        title: 'Updated',
        field: 'updated',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px','text-align': 'center' },
        class: '',
        translate: 'COMMON.COLUMN.UPDATED_DATE',
        autoHide: true,
        width: 100,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: true,
        width: 117,
      },
    ]
  }


}




