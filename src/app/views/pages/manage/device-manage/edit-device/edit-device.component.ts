import { Component, OnInit, EventEmitter, Input, ViewChild, ElementRef, Output, ChangeDetectorRef } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef, NgbTypeahead, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { DeviceService, Device, User, Sensor, SensorParameter, SensorService, Driver, DriverService, DeviceGroup, TypeSensor, CameraService, Camera } from '@core/manage';
import { tap, count, takeUntil, finalize } from 'rxjs/operators';
import { Observable, Subject, merge } from 'rxjs';
import { DeviceType, SimType, Deparment, DeviceTypeService, SensorType, TransportTypeQCVN, TransportType } from '@core/admin';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { UserDatePipe, MessageError, ValidatorCustomService, UserDateAdvPipe } from '@core/_base/layout';
import { NgxPermissionsService } from 'ngx-permissions';
import { TranslateService } from '@ngx-translate/core';
import { MatGridTileHeaderCssMatStyler } from '@angular/material';
import { DeviceConfigService } from '@core/common/_services/device-config.service';
import { CurrentUserService, UserOnServerCreated } from '@core/auth';
import { TypesUtilsService } from '@core/_base/crud';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { type, array } from '@amcharts/amcharts4/core';
import moment from 'moment';
import { findIndex } from 'lodash';
import { cos } from '@amcharts/amcharts4/.internal/core/utils/Math';


declare var $: any;
const TITLE_FORM_EDIT: string = "COMMON.ACTIONS.EDIT";
const TITLE_FORM_ADD: string = "COMMON.ACTIONS.ADD";
const TITLE_FORM_DELETE: string = "COMMON.ACTIONS.DELETE";
const states = ['°C', '°F', 'lit', 'l', "kg", "km", "m", "cm", "db"];
const ICON_SENSOR_DEFAULT: string = "icon-tag";

export function isNumber(c: AbstractControl) {
  return (!Number.parseInt(c.value)) ? {
    isNumber: true
  } : null;
}
@Component({
  selector: 'kt-edit-device',
  templateUrl: './edit-device.component.html',
  styleUrls: ['./edit-device.component.scss'],
  providers: [UserDatePipe, UserDateAdvPipe]
})
export class EditDeviceComponent implements OnInit {


  @Input() editOpen: EventEmitter<Device>;
  @Input() dataConfig: EventEmitter<Device>;
  @Output() editResult: EventEmitter<{ status: string, message: string, data: any }>;
  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;

  public device: Device;
  public closeResult: string;
  public userSelect: User;
  public deviceTypes: DeviceType[];
  public simTypes: SimType[];
  public iconTypes: any[];
  public groupDevices: DeviceGroup[];
  private unsubscribe: Subject<any>;
  private currentReason: any;
  private currentReasonSensor: any;
  public frmDeviceEditBase: FormGroup;
  public frmdeviceEditAdvanced: FormGroup;
  public frmdeviceEditExtension: FormGroup;
  public configMessage: MessageError[];
  public configMessageSensor: MessageError[];
  public listSensors: Sensor[];
  public formSensor: FormGroup;
  public formSensorBase: FormGroup;
  public formCamera: FormGroup;
  public dataCamera: any = [{}];
  public dataCameraEdit: any = [{}];
  public idCameraEdit: number;
  public formSensorAdvance: FormGroup;
  public currentSensorTemplate: any;
  public listTransportType: TransportType[];
  public listTransportTypeQcvn: TransportTypeQCVN[];
  public listDriver: Driver[];
  public listDeparmentManage: Deparment[];
  public sendToDeparment: boolean = true;
  public allowReOrder: boolean = false;
  public sensorParameters: string[];
  public sensorsTemplate: any[];
  public deviceGroupEventEmiter: EventEmitter<number>;
  public addDriverEmiter: EventEmitter<number>;
  private loadFormAdvanced: boolean = false;
  private modalSersor: NgbModalRef;
  private modalCamera: NgbModalRef;
  private deviceOld: Device;
  public isSend: boolean = false;
  public modelUnit;
  public typeSensors: Array<TypeSensor>;
  public icons = ['icon-tag', 'icon-thermometer-half', 'icon-gas-station', 'icon-tank', 'icon-tachometer-alt', "icon-manometer", "icon-odometer", "icon-route"];
  public minStopDuration: NgbTimeStruct;


  public qcvn: boolean;

  @ViewChild('content', { static: true }) content: ElementRef;
  @ViewChild('sensorTmp', { static: true }) sensorTmp: ElementRef;
  @ViewChild('cameraModal', { static: true }) cameraModal: ElementRef;

  @ViewChild('instance', { static: true }) instance: NgbTypeahead;
  public focus$ = new Subject<string>();
  public click$ = new Subject<string>();
  public sensorAction: string = "edit";
  public cameraActionEdit: boolean = false;

  public sensorTmpTitle: string = "";
  public sensorIdSelected: number = 0;
  private deviceType: DeviceType;
  public iconSensorSelect: string = "icon-tag";
  public typeSensorSelect: TypeSensor;
  public typeSensorSelectDefault: TypeSensor;
  public listTimeZones = [];



  constructor(private deviceService: DeviceService,
    private cameraService: CameraService,
    private sensorService: SensorService,
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private userDate: UserDatePipe,
    private permissionService: NgxPermissionsService,
    private translateService: TranslateService,
    private validatorCT: ValidatorCustomService,
    private deviceConfig: DeviceConfigService,
    private driverService: DriverService,
    private currentUser: CurrentUserService,
    private typeUtil: TypesUtilsService,
    private deviceTypeService: DeviceTypeService,
    private userDateAdv: UserDateAdvPipe) {
    // this.devices;
    this.userSelect = new User();
    this.userSelect.clear();
    this.editResult = new EventEmitter();
    this.unsubscribe = new Subject();
    this.currentReason = {};
    this.simTypes = [];
    this.deviceTypes = [];
    this.groupDevices = [];
    this.iconTypes = [];
    this.listSensors = [];
    this.sensorsTemplate = [];
    this.sensorParameters = [];
    // this.confirmEmiter = new EventEmitter();
    this.currentSensorTemplate = {};
    this.setConfigError();

    // create device group
    this.deviceGroupEventEmiter = new EventEmitter();
    this.addDriverEmiter = new EventEmitter();
    this.deviceOld = new Device();

    this.typeSensors = this.sensorService.getAllTypeSensor();
    this.typeSensorSelect = this.typeSensors.find(x => x.type == 'other');
    this.typeSensorSelectDefault = this.typeSensors.find(x => x.type == 'other');
    this.listTimeZones = this.getTimeZones();

  }
  ngOnInit() {
 
    if (this.editOpen != undefined) {
      this.editOpen.pipe(
        tap(device => {
          this.device = device;
          this.qcvn = device.qcvn;
          this.createForm(this.device);
          this.createFormExtensions(this.device);
          this.createFormAdvanced(this.device);
          this.open(this.content);

        })
      ).subscribe();
    }

    if (!this.device) {
      // this.deviceService.detail().pipe(
      //   tap(data => {
      //     this.dataConfig = data.result.data;
      //   }),
      //   takeUntil(this.unsubscribe),
      //   finalize(() => { this.cdr.markForCheck() })
      // ).subscribe();
    }


  }
  search = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    // const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => (!this.instance || !this.instance.isPopupOpen())));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? states
        : states.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)))
    );
  }

  open(content) {
    
    this.loadFormAdvanced = false;
    if (this.dataConfig == undefined) {
      this.deviceConfig.get().then(x => {
        this.dataConfig = x;
        this.simTypes = x.simTypes;
        this.deviceTypes = x.deviceTypes;
        
        this.iconTypes = x.deviceIcons;
        this.listDeparmentManage = x.govs || [];
        this.listTransportType = x.transportTypes || [];
        this.listTransportTypeQcvn = x.transportTypeQcvns || [];
        this.loadDeviceSelectpicker();
      });
    
    }
    else {
      // this.dataConfig = this.deviceTypes.map(x => x.name);
      this.loadDeviceSelectpicker();
    }
    this.getDeviceGroup(this.device.userId);
    this.listSensors = [];
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', size: 'lg', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });

    setTimeout(() => {
      $('.kt-selectpicker__edit-device').selectpicker();
      $('#edTimeZone').val(this.device.extensions.timeZone).selectpicker("refresh");
    });


  }
  private loadDeviceSelectpicker() {
    let _this = this;
    setTimeout(() => {
      $('.kt-selectpicker__edit-device').selectpicker();
      $('#edSimType').val(_this.device.simType).selectpicker("refresh");
      $('#edIconType').val(_this.device.icon).selectpicker("refresh");
      $('#edDeviceType').val(_this.device.typeDevice).selectpicker("refresh");

      // $('#edGroupDevice').val(_this.device.groupDevices.map(x=>x['id'])).selectpicker("refresh");      
      $('#dateSell').datepicker({
        format: _this.currentUser.dateFormat.toLowerCase(),
        todayHighlight: true,
        // orientation: "bottom left",
        // templates: {
        //   leftArrow: '<i class="la la-angle-left"></i>',
        //   rightArrow: '<i class="la la-angle-right"></i>'
        // },
        clearBtn: true,
        autoclose: true
      });
    });
  }
  private createFormAdvanced(device: Device) {
    // this.sendToDeparment = (device.vehicelSendToDeparment==1?true:false) || false;
    if(!device.extensions.consumptionRate) device.extensions.consumptionRate = 0;

    this.frmdeviceEditAdvanced = this.fb.group({
      numberPlate: [device.numberPlate || ""],
      frameNumber: [device.frameNumber || ""],
      vinNumber: [device.vinNumber || ""],
      engineNumber: [device.engineNumber || ""],
      transportType: [device.transportType || ""],
      transportTypeQcvn: [device.transportTypeQcvn || ""],
      driver: [device.driver || ""],
      sendToGovernment: [(device.sendToGovernment == 1 ? true : false) || false],
      governmentManage: [device.governmentId || ""],
      coefficient: [device.coefficient || 1, this.validatorCT.float],
      consumptionRate: [device.extensions.consumptionRate || 0, this.validatorCT.float],
      usingTollFee: [(device.usingTollFee == 1 ? true : false || false)],
      corridor: ['']

    });

    // disable for the select box when not support send to gov
    this.frmdeviceEditAdvanced.controls['governmentManage'].disable();
    this.frmdeviceEditAdvanced.controls['transportTypeQcvn'].disable();
     $('.sendToGov').selectpicker('refresh');
  }

  private createForm(device: Device) {

    // this.datePickerOptions['autoSelect'] = false;
    // this.datePickerOptions['disabled'] = true;
    this.frmDeviceEditBase = this.fb.group({
      imei: new FormControl({
        value: device.imei,
        disabled: (this.permissionService.getPermission("ROLE_device.edit.imei") == undefined ? true : false),
      }, [Validators.required, this.validatorCT.rangeLength(10, 15)]),
      userName: new FormControl({ value: device.userName, disabled: true }),
      // machineCode: [device.machineCode],
      name: [device.name, this.validatorCT.rangeLength(1, 64)],
      icon: [device.icon],
      groupIds: [device.groupDevices.map(x => x['id'])],//[device.simno]
      simno: new FormControl({
        value: device.simno,
        disabled: (this.permissionService.getPermission("ROLE_device.edit.sim") == undefined ? true : false)
      }, (this.permissionService.getPermission("ROLE_device.edit.sim") ? this.validatorCT.phone : [])),
      simType: new FormControl({
        value: device.simType,
        disabled: (this.permissionService.getPermission("ROLE_device.edit.sim") == undefined ? true : false)
      }),
      typeDevice: new FormControl({
        value: device.typeDevice,
        disabled: (this.permissionService.getPermission("ROLE_device.edit.type") == undefined ? true : false)
      }, this.validatorCT.select),
      serviceExpire: new FormControl({
        value: this.userDate.transform(device.serviceExpire),
        disabled: (this.permissionService.getPermission("ROLE_device.edit.service_expire") == undefined ? true : false)
      }),
      // serviceExpireOld: new FormControl({
      //   value: device.serviceExpire,
      //   disabled:true
      // }),
      activeWarrantyDate: new FormControl({
        value: this.userDate.transform(device.warrantyActiveDate),
        disabled: true
      }),
      warrantyExpiredDate: new FormControl({
        value: this.userDate.transform(device.warrantyExpiredDate),
        disabled: true
      }),
      activeWarranty:
        new FormControl({
          value: (device.activeWarranty == 1 ? true : false),
          disabled: (this.permissionService.getPermission("ROLE_device.edit.active_customer") == undefined ? true : false)
        }),
      status:
        new FormControl({
          value: (device.active == 1 ? true : false),
          disabled: (this.permissionService.getPermission("ROLE_device.edit.active_status") == undefined ? true : false)
        }),
      desc: [device.description],
      descAdmin: [device.descAdmin],
      isSell: new FormControl({
        value: (device.isSell == 1 ? true : false),
        disabled: (this.permissionService.getPermission("ROLE_device.edit.sold") == undefined ? true : false)
      }),
      sellDate: new FormControl({
        value: this.userDate.transform(device.sellAt),
        disabled: false
      }),
      sellUpdatedDate: new FormControl({
        value: this.userDate.transform(device.sellUpdated, "date"),
        disabled: true
      }),
    });


    let _this = this;
    setTimeout(() => {
      $('#dServiceExpire').datepicker({
        format: _this.currentUser.dateFormat.toLowerCase(),
        todayHighlight: true,
        autoclose: true
      });
    })
  }

  private createFormExtensions(device: Device) {

    if (!device.extensions) device.extensions = {
			timeoutGprs: 20,
			consumptionRate: 0,
			minSpeed: 3,
			timeZone: 0,
			minStopDuration: 600,
		};

    if(!device.extensions.timeoutGprs) device.extensions.timeoutGprs = 20;
    if(!device.extensions.minSpeed) device.extensions.minSpeed = 3;
    if(!device.extensions.timeZone) device.extensions.timeZone = 0;
    if(!device.extensions.minStopDuration) device.extensions.minStopDuration = 600;
    
    this.frmdeviceEditExtension = this.fb.group({
      timeoutGprs: [device.extensions.timeoutGprs || 20, [this.validatorCT.required, this.validatorCT.number, this.validatorCT.min(5)]],
      minSpeed: [device.extensions.minSpeed || 3, [this.validatorCT.number, this.validatorCT.min(1)]],
      isDebug: [(device.isDebug == 1 ? true : false || false)],
      timeZone: [device.extensions.timeZone || 0],
      minStopDuration: [device.extensions.minStopDuration || 600, ],
    });

    let value = this.frmdeviceEditExtension.controls.minStopDuration.value;
    this.minStopDuration = {
      hour: 0,
      minute: Math.floor(value / 60),
      second: value % 60
    }

  }

  private getDeviceGroup(userId) {

    this.deviceService.getGroupDevice(userId).pipe(
      tap(
        data => {
          this.groupDevices = data.result;
          let _this = this;
          let items = [];
          this.device.groupDevices.forEach(x => {
            items.push(x['id']);
          });
          setTimeout(() => {
            $('#edGroupDevice').val(items).selectpicker("refresh");
          });
        }
      ),
      finalize(() => this.cdr.markForCheck())
    ).subscribe();
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  changeUser(userSelect) {
    this.userSelect = userSelect;
  }

  changeTimepicker(event) {

    // if(this.minStopDuration.hour === 0 && this.minStopDuration.minute === 0 && this.minStopDuration.second === 0) {
         
    //   this.frmdeviceEditExtension.get('minStopDuration').setValidators(this.validatorCT.  );
    //   this.frmdeviceEditExtension.get('minStopDuration').updateValueAndValidity();
    //   this.frmdeviceEditExtension.get('minStopDuration').markAsTouched();
    //   return;
    // }
    if(this.minStopDuration.hour != 0) {
      this.minStopDuration = {hour: 0, minute: 0, second: 0};
      
      this.frmdeviceEditExtension.get('minStopDuration').setValidators(this.validatorCT.max(59));
      this.frmdeviceEditExtension.get('minStopDuration').updateValueAndValidity();
      this.frmdeviceEditExtension.get('minStopDuration').markAsTouched();

      return;
    } 
    if( this.minStopDuration.minute == 0 && this.minStopDuration.second < 5) {
      this.minStopDuration = {hour: 0, minute: 0, second: 0};
      
      this.frmdeviceEditExtension.get('minStopDuration').setValidators(this.validatorCT.min(5));
      this.frmdeviceEditExtension.get('minStopDuration').updateValueAndValidity();
      this.frmdeviceEditExtension.get('minStopDuration').markAsTouched();
      
      return;
    }
    else {
      this.frmdeviceEditExtension.get('minStopDuration').clearValidators();
      this.frmdeviceEditExtension.get('minStopDuration').updateValueAndValidity();
    }
      
  }

  onSubmit() {
    
    if (!this.frmDeviceEditBase.invalid) {
      this.device.imei = this.frmDeviceEditBase.get('imei').value;
      this.device.name = this.frmDeviceEditBase.get('name').value;
      this.device.icon = this.frmDeviceEditBase.get('icon').value;
      this.device.groupIds = $('#edGroupDevice').val().join(",");
      this.device.simno = this.frmDeviceEditBase.get('simno').value;
      this.device.simType = this.frmDeviceEditBase.get('simType').value;
      this.device.typeDevice = this.frmDeviceEditBase.get('typeDevice').value;
      this.device.activeWarranty = (this.frmDeviceEditBase.get('activeWarranty').value ? 1 : 0);
      this.device.active = (this.frmDeviceEditBase.get('status').value ? 1 : 0);
      this.device.description = this.frmDeviceEditBase.get("desc").value;
      this.device.descAdmin = this.frmDeviceEditBase.get("descAdmin").value;
      this.device.isSell = this.frmDeviceEditBase.get("isSell").value ? 1 : 0;

      // if(this.frmDeviceEditBase.get("serviceExpireOld").value && this.frmDeviceEditBase.get("serviceExpireOld").value.length >0)
      // this.device.serviceExpire = `${this.typeUtil.stringToISODateStr($('#dateSell').val(), this.currentUser.dateFormat.toLowerCase())} ${this.userDateAdv.transform(this.frmDeviceEditBase.get("serviceExpireOld").value, "hh:mm:ss")}`;
      this.device.serviceExpire = `${this.typeUtil.stringToISODateStr($('#dServiceExpire').val(), this.currentUser.dateFormat.toLowerCase())}`;
      this.device.sellAt = this.typeUtil.stringToISODateStr($('#dateSell').val(), this.currentUser.dateFormat.toLowerCase());
    }
    else {
      $("#kt_nav_tab_base").click();
      this.frmDeviceEditBase.markAllAsTouched();
      setTimeout(() => {
        const firstElementWithError = document.querySelector('#deviceEditFormBase .invalid-feedback');
        if (firstElementWithError) {
          firstElementWithError.scrollIntoView({ block: 'center' });
        }
      });
      return;
    }
    if (this.loadFormAdvanced) {
      if (!this.frmdeviceEditAdvanced.invalid) {
        this.device.numberPlate = this.frmdeviceEditAdvanced.get('numberPlate').value;
        this.device.frameNumber = this.frmdeviceEditAdvanced.get('frameNumber').value;
        this.device.vinNumber = this.frmdeviceEditAdvanced.get('vinNumber').value;
        this.device.engineNumber = this.frmdeviceEditAdvanced.get('engineNumber').value;
        this.device.transportType = this.frmdeviceEditAdvanced.get('transportType').value;
        this.device.transportTypeQcvn = this.frmdeviceEditAdvanced.get('transportTypeQcvn').value;
        this.device.driver = this.frmdeviceEditAdvanced.get('driver').value;
        this.device.coefficient = this.frmdeviceEditAdvanced.get('coefficient').value;
        this.device.sendToGovernment = (this.frmdeviceEditAdvanced.get('sendToGovernment').value ? 1 : 0);
        this.device.governmentManage = this.frmdeviceEditAdvanced.get('governmentManage').value;
        this.device.extensions.consumptionRate = this.frmdeviceEditAdvanced.get('consumptionRate').value;
        this.device.usingTollFee = this.frmdeviceEditAdvanced.controls.usingTollFee.value ? 1 : 0;
        this.device.corridor = this.frmdeviceEditAdvanced.controls.corridor.value ? 1 : 0;
      }
      else {
        $("#kt_nav_tab_advanced").click();
        this.frmdeviceEditAdvanced.markAllAsTouched();
        setTimeout(() => {
          const firstElementWithError = document.querySelector('#deviceEditFormAdvanced .invalid-feedback');
          if (firstElementWithError) {
            firstElementWithError.scrollIntoView({ block: 'center' });
          }
        });
        return;
      }
    }

    let isSecond = this.minStopDuration.minute * 60 + this.minStopDuration.second;

    if (!this.frmdeviceEditExtension.invalid) {
      this.device.extensions.timeoutGprs = this.frmdeviceEditExtension.get('timeoutGprs').value;
      this.device.extensions.minStopDuration = isSecond;
      this.device.extensions.timeZone = this.frmdeviceEditExtension.get('timeZone').value;
      this.device.extensions.minSpeed = this.frmdeviceEditExtension.get('minSpeed').value;
      this.device.isDebug = this.frmdeviceEditExtension.controls.isDebug.value ? 1 : 0;
      
    }
    else {
      $("#kt_nav_tab_extensions").click();
      this.frmdeviceEditExtension.markAllAsTouched();
      setTimeout(() => {
        const firstElementWithError = document.querySelector('#deviceEditFormExtension .invalid-feedback');
        if (firstElementWithError) {
          firstElementWithError.scrollIntoView({ block: 'center' });
        }
      });
      return;
    }

    if (((this.frmdeviceEditAdvanced.valid && this.loadFormAdvanced) || !this.loadFormAdvanced) && this.frmDeviceEditBase.valid && this.frmdeviceEditExtension.valid) {
      this.isSend = true;
      this.deviceService.update(this.device, { notifyGlobal: true }).pipe(
        tap(result => {
          if (result.status == 200) {

            this.modalService.dismissAll(this.currentReason);
            this.editResult.emit({ status: 'success', message: "", data: this.device });
          }
        },
          error => {
            this.editResult.emit({ status: 'error', message: error.message, data: this.device });
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.isSend = false;
          this.cdr.markForCheck();
        })

      ).subscribe();
    }
  }

  openTabSensors(refesh?: boolean) {
    // get list sensor
    if (this.listSensors.length == 0 || refesh === true) {
      this.sensorService.getSensors(this.device.id).pipe(
        tap(data => {
          data.result.content = data.result.content.map(x => {
            if (x.keyLanguage && x.keyLanguage.length > 0) {
              x.keyLanguage = "SENSOR_TEMPLATE." + x.keyLanguage;
            }
            else {
              let keyLanguage = "SENSOR_TEMPLATE." + x.calculationType.toUpperCase().replace(/ /g, "_");
              if (this.translateService.instant(keyLanguage) != keyLanguage) {
                x.keyLanguage = keyLanguage;
              }
              else x.keyLanguage = x.calculationType;
            }
            return x;
          });
          // process icon 
          data.result.content = data.result.content.map(x => {
            // x.parameters = "";
            x.iconType = this.typeSensors.find(sensor => sensor.type == x.parameters.typeSensor) || this.typeSensorSelectDefault;
            return x;
          });

          this.listSensors = data.result.content;
        },
          error => {

          }),
        takeUntil(this.unsubscribe),
        finalize(this.cdr.markForCheck)
      ).subscribe();
    }

  }

  openTabCamera() {
    let params = {
      pageNo: 1,
      pageSize: 10,
      deviceId: this.device.id
    };

    this.cameraService.list({ params: params }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.dataCamera = data.result.content;
        }

      }),
      takeUntil(this.unsubscribe),
      finalize(this.cdr.markForCheck)
    ).subscribe();
  }

  addSensor() {
    this.sensorAction = "add";
    this.sensorTmpTitle = TITLE_FORM_ADD;
    // 
    this.getDeviceType();
    if (this.sensorParameters.length == 0 || this.sensorsTemplate.length == 0) {
      this.getSensorConfig();
    }
    else {
      setTimeout(() => {
        $(".kt-selectpicker__sensor").selectpicker();
      });
    }
    this.createFormSensor();
    this.modalSersor = this.modalService.open(this.sensorTmp, { windowClass: 'kt-mt-50 modal-dialog-scrollable', backdrop: 'static' });
    this.modalSersor.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReasonSensor = reason;
    });
  }

  addCamera() {
    this.cameraActionEdit = false;
    this.sensorTmpTitle = TITLE_FORM_ADD;

    this.createFormCamera();
    this.modalCamera = this.modalService.open(this.cameraModal, { windowClass: 'kt-mt-50 modal-dialog-scrollable', backdrop: 'static' });
    this.modalCamera.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReasonSensor = reason;
    });
  }

  editCamera(camera) {
    this.cameraActionEdit = true;
    this.sensorTmpTitle = TITLE_FORM_EDIT;

    this.idCameraEdit = camera.id;
    // this.dataCameraEdit = [camera];
    // console.log( this.dataCameraEdit);
    this.createFormCamera(camera);
    this.modalCamera = this.modalService.open(this.cameraModal, { windowClass: 'kt-mt-50 modal-dialog-scrollable', backdrop: 'static' });
    this.modalCamera.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReasonSensor = reason;
    });
  }

  deleteCamera(deleteCameraModal, id) {
    this.idCameraEdit = id;
    this.modalCamera = this.modalService.open(deleteCameraModal, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-delete', backdrop: 'static' });
    this.modalCamera.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReasonSensor = reason;
    });
  }

  deleteCameraItem() {
    let id = this.idCameraEdit;
    this.cameraService.delete(id, { notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.modalCamera.close();
        this.openTabCamera();
      }
    });
  }

  onSubmitCamera(form) {
    let name = form.value.name;
    let cameraId = form.value.id;
    let deviceId = this.device.id;
    let description = form.value.description;
    let params = {
      name: name,
      cameraId: cameraId,
      deviceId: deviceId,
      description: description,
      id: this.idCameraEdit
    } as Camera;
    if (this.cameraActionEdit) {
      this.cameraService.update(params, { notifyGlobal: true }).pipe(
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
        .subscribe((result: any) => {
          if (result.status == 200) {
            this.modalCamera.close();
            this.openTabCamera();
          }
        });
      return;
    }
    this.cameraService.create(params, { notifyGlobal: true }).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
      .subscribe((result: any) => {
        if (result.status == 201) {
          this.modalCamera.close();
          this.openTabCamera();
        }
      })
  }

  editSensor(id: number) {
    let _this = this;
    this.sensorAction = "edit";
    this.sensorTmpTitle = TITLE_FORM_EDIT;
    this.sensorIdSelected = id;

    let index = this.listSensors.findIndex(x => x.id == id);
    let sensor = new Sensor();
    if (index >= 0) {
      // process sensor
      sensor = Object.assign({}, sensor, this.listSensors[index]);
    }
    this.createFormSensor(sensor);
    this.getDeviceType();
    if (this.sensorParameters.length == 0 || this.sensorsTemplate.length == 0) {
      this.getSensorConfig(sensor);
    }
    else {
      setTimeout(() => {
        $(".kt-selectpicker__sensor").selectpicker();
        $("#sensorTemplateFrm").val(sensor.calculationType).selectpicker('refresh');
        $("#sensorParameterFrm").val(sensor.parameters.parameter_name).selectpicker('refresh');
        _this.sensorTemplateChange(sensor.calculationType);
      });
    }

    this.modalSersor = this.modalService.open(this.sensorTmp, { windowClass: 'kt-mt-50 modal-dialog-scrollable', backdrop: 'static' })
    this.modalSersor.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReasonSensor = reason;
    });
  }
  deleteSensor(id: number) {
    this.sensorAction = "delete";
    this.sensorTmpTitle = TITLE_FORM_DELETE;
    this.sensorIdSelected = id;
    // this.modalService.dismissAll(this.currentReason);
    this.modalSersor = this.modalService.open(this.sensorTmp, { windowClass: 'kt-mt-50 modal-dialog-scrollable', size: 'sm', backdrop: 'static' });
    this.modalSersor.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReasonSensor = reason;
    });
  }
  onSensorSubmit() {
    switch (this.sensorAction) {
      case 'edit':
        this.editSensorAction();
        break;
      case 'add':
        this.addSensorAction();
        break;
      case 'delete':
        this.deleteSensorAction();
        break;
    }
  }

  onCameraSubmit() {
    // switch (this.cameraAction) {
    //   case 'edit':
    //     this.editSensorAction();
    //     break;
    //   case 'add':
    //     this.addSensorAction();
    //     break;
    //   case 'delete':
    //     this.deleteSensorAction();
    //     break;
    // }
  }

  get error() {
    if (this.formCamera != undefined) return this.formCamera.controls;
  }

  createFormCamera(camera?) {
    if (camera) {
      this.formCamera = this.fb.group({
        name: [camera.name || "", Validators.required],
        id: [camera.cameraId || "", Validators.required],
        description: [camera.description || ""]
      });
    } else {
      this.formCamera = this.fb.group({
        name: ["", Validators.required],
        id: ["", Validators.required],
        description: [""]
      });
    }

  }

  createFormSensor(sensor?: Sensor) {
    this.typeSensorSelect = this.typeSensorSelectDefault;
    if (sensor) {
      this.modelUnit = sensor.parameters.unit || "";
      this.iconSensorSelect = sensor.parameters.icon || ICON_SENSOR_DEFAULT;
      this.typeSensorSelect = this.typeSensors.find(x => x.type == sensor.parameters.typeSensor) || this.typeSensorSelectDefault;
      this.formSensorBase = this.fb.group({
        name: [sensor.name || "", Validators.required],
        template: [sensor.calculationType || "", this.validatorCT.select],
        parameter: [sensor.parameters.parameter_name || "", this.validatorCT.select],
        unit: [sensor.parameters.unit || ""],
        formula: [sensor.parameters.formula || "[value]", this.validatorCT.formula],
        calibration: [sensor.parameters.calibration || "", this.validatorCT.calibration],
        min: [sensor.parameters.min || 0, this.validatorCT.number],
        max: [sensor.parameters.max || 1000, this.validatorCT.number],
      });
      this.formSensorAdvance = this.fb.group({
        increase: [sensor.parameters.increase || 20, [this.validatorCT.number, this.validatorCT.min(0)]],
        decrease: [sensor.parameters.decrease || 20, [this.validatorCT.number, this.validatorCT.min(0)]],
        offset: [sensor.parameters.offset || 0, this.validatorCT.number],
        round: [sensor.parameters.round || 0, this.validatorCT.number],
        sortOrder: [sensor.sortOrder || 1, this.validatorCT.number],
        showOnMap: [sensor.parameters.show_on_map ? true : false],
        description: [sensor.description || ""],
      });
    }
    else {
      this.modelUnit = "";
      this.iconSensorSelect = ICON_SENSOR_DEFAULT;
      this.formSensorBase = this.fb.group({
        name: ["", Validators.required],
        template: ["", this.validatorCT.select],
        parameter: ["", this.validatorCT.select],
        unit: [""],
        formula: ["[value]", this.validatorCT.formula],
        calibration: ["", this.validatorCT.calibration],
        min: [0, this.validatorCT.number],
        max: [1000, this.validatorCT.number],
      });
      this.formSensorAdvance = this.fb.group({
        increase: [20, [this.validatorCT.number, this.validatorCT.min(0)]],
        decrease: [20, [this.validatorCT.number, this.validatorCT.min(0)]],
        offset: [0, this.validatorCT.number],
        round: [0, this.validatorCT.number],
        sortOrder: [1, this.validatorCT.number],
        showOnMap: [false],
        description: [""],
      });
    }
  }

  addSensorAction() {
    let sensor = new Sensor();
    if (!this.formSensorBase.invalid && !this.formSensorAdvance.invalid) {
      let parameters: any = {};
      sensor.deviceId = this.device.id;
      sensor.name = this.formSensorBase.get('name').value;
      sensor.calculationType = this.formSensorBase.get('template').value;
      // parameters.icon = this.iconSensorSelect;
      parameters.typeSensor = this.typeSensorSelect.type;

      if (this.currentSensorTemplate.type != undefined)
        parameters.type = this.formSensorBase.get('template').value;
      if (this.currentSensorTemplate.parameter_name != undefined)
        parameters.parameter_name = this.formSensorBase.get('parameter').value;
      if (this.currentSensorTemplate.unit != undefined)
        parameters.unit = this.formSensorBase.get('unit').value;
      if (this.currentSensorTemplate.formula != undefined)
        parameters.formula = this.formSensorBase.get('formula').value;
      if (this.currentSensorTemplate.calibration != undefined)
        parameters.calibration = this.formSensorBase.get('calibration').value;
      if (this.currentSensorTemplate.min != undefined)
        parameters.min = this.formSensorBase.get('min').value;
      if (this.currentSensorTemplate.max != undefined)
        parameters.max = this.formSensorBase.get('max').value;

      if (this.currentSensorTemplate.increase != undefined)
        parameters.increase = this.formSensorAdvance.get('increase').value;
      if (this.currentSensorTemplate.decrease != undefined)
        parameters.decrease = this.formSensorAdvance.get('decrease').value;
      if (this.currentSensorTemplate.offset != undefined)
        parameters.offset = this.formSensorAdvance.get('offset').value;
      if (this.currentSensorTemplate.round != undefined)
        parameters.round = this.formSensorAdvance.get('round').value;
      if (this.currentSensorTemplate.show_on_map != undefined)
        parameters.show_on_map = (this.formSensorAdvance.get('showOnMap').value == 1 ? true : false);
      sensor.parameters = parameters;

      this.sensorService.create(sensor, { notifyGlobal: true }).pipe(
        tap(
          (data) => {
            if (data.status == 201) {
              this.modalSersor.close();
              this.openTabSensors(true);
            }
          },
          error => { }
        )
      ).subscribe();
    }
    else {
      this.formSensorBase.markAllAsTouched();
      this.formSensorAdvance.markAllAsTouched();
      if (this.formSensorBase.invalid) {
        $("#kt_nav_tab_sensor_base").click();
      } else if (this.formSensorAdvance.invalid) {
        $("#kt_nav_tab_sensor_advance").click();
      }
      setTimeout(() => {
        const firstElementWithError = document.querySelector('#formSensor .invalid-feedback');
        if (firstElementWithError) {
          firstElementWithError.scrollIntoView({ block: 'center' });
        }
      });

    }
  }
  editSensorAction() {
    let sensorId = this.sensorIdSelected;
    if (!this.formSensorBase.invalid && !this.formSensorAdvance.invalid) {
      let sensor = this.listSensors.find(x => x.id == sensorId);
      let parameters: any = {};
      sensor.deviceId = this.device.id;
      sensor.name = this.formSensorBase.get('name').value;
      sensor.calculationType = this.formSensorBase.get('template').value;
      // parameters.icon = this.iconSensorSelect;
      parameters.typeSensor = this.typeSensorSelect.type;
      if (this.currentSensorTemplate.type != undefined)
        parameters.type = this.formSensorBase.get('template').value;
      if (this.currentSensorTemplate.parameter_name != undefined)
        parameters.parameter_name = this.formSensorBase.get('parameter').value;
      if (this.currentSensorTemplate.unit != undefined)
        parameters.unit = this.formSensorBase.get('unit').value;
      if (this.currentSensorTemplate.formula != undefined)
        parameters.formula = this.formSensorBase.get('formula').value;
      if (this.currentSensorTemplate.calibration != undefined)
        parameters.calibration = this.formSensorBase.get('calibration').value;
      if (this.currentSensorTemplate.min != undefined)
        parameters.min = this.formSensorBase.get('min').value;
      if (this.currentSensorTemplate.max != undefined)
        parameters.max = this.formSensorBase.get('max').value;

      if (this.currentSensorTemplate.increase != undefined)
        parameters.increase = this.formSensorAdvance.get('increase').value;
      if (this.currentSensorTemplate.decrease != undefined)
        parameters.decrease = this.formSensorAdvance.get('decrease').value;
      if (this.currentSensorTemplate.offset != undefined)
        parameters.offset = this.formSensorAdvance.get('offset').value;
      if (this.currentSensorTemplate.round != undefined)
        parameters.round = this.formSensorAdvance.get('round').value;
      if (this.currentSensorTemplate.show_on_map != undefined)
        parameters.show_on_map = (this.formSensorAdvance.get('showOnMap').value == 1 ? true : false);
      sensor.parameters = parameters;

      sensor.sortOrder = this.formSensorAdvance.get('sortOrder').value;
      sensor.description = this.formSensorAdvance.get('description').value;

      this.sensorService.update(sensor, { notifyGlobal: true }).pipe(
        tap(
          (data) => {
            if (data.status == 200) {
              this.modalSersor.close();
              this.openTabSensors(true);
            }
          },
          error => { }
        )
      ).subscribe();
    }
    else {
      this.formSensorBase.markAllAsTouched();
      this.formSensorAdvance.markAllAsTouched();
      if (this.formSensorBase.invalid) {
        $("#kt_nav_tab_sensor_base").click();
      } else if (this.formSensorAdvance.invalid) {
        $("#kt_nav_tab_sensor_advance").click();
      }
      setTimeout(() => {
        const firstElementWithError = document.querySelector('#formSensor .invalid-feedback');
        if (firstElementWithError) {
          firstElementWithError.scrollIntoView({ block: 'center' });
        }
      });
    }

  }
  deleteSensorAction() {
    this.sensorService.delete(this.sensorIdSelected, { notifyGlobal: true }).pipe(
      tap(
        (data) => {
          if (data.status == 200) {
            this.modalSersor.close();
            this.openTabSensors(true);
          }
        },
        error => { }
      )
    ).subscribe();
  }

  getSensorConfig(sensor?) {
    let _this = this;
    this.sensorService.getAllConfigSensor().pipe(
      tap(data => {
        this.sensorParameters = data.result.parameters;
        data.result.templates = data.result.templates.map(x => {
          if (x.keyLanguage && x.keyLanguage.length > 0) {
            x.keyLanguage = "SENSOR_TEMPLATE." + x.keyLanguage;
          }
          else {
            x.keyLanguage = x.name;
          }
          return x;
        });

        this.sensorsTemplate = data.result.templates;
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
        setTimeout(() => {
          $(".kt-selectpicker__sensor").selectpicker();
          if (sensor) {
            $("#sensorTemplateFrm").val(sensor.calculationType).selectpicker('refresh');
            $("#sensorParameterFrm").val(sensor.parameters.parameter_name).selectpicker('refresh');
            _this.sensorTemplateChange(sensor.calculationType);
          }
        });

      })
    ).subscribe();

  }

  async drop(event: CdkDragDrop<string[]>) {
    if (event.previousIndex == event.currentIndex) return;

    let deviceId = this.listSensors[event.previousIndex].deviceId;
    let arrayCheck = this.listSensors.filter(x => x.deviceId == deviceId);
    let index = 0;

    moveItemInArray(arrayCheck, event.previousIndex, event.currentIndex);
    let params = arrayCheck.map(item => {
      index++;
      return { id: item.id, sortOrder: index };
    });
    await this.sensorService.sortOrder(params, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          moveItemInArray(this.listSensors, event.previousIndex, event.currentIndex);
          this.cdr.detectChanges();
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(this.cdr.markForCheck)
    ).toPromise();
  }

  sortOrder() {
    this.allowReOrder = !this.allowReOrder;
  }

  async getDeviceType() {
    if (!this.deviceType || this.deviceType.id != this.device.typeDevice) {
      await this.deviceTypeService.detail(this.device.typeDevice).pipe(
        tap(response => {
          this.deviceType = response.result;
          this.cdr.detectChanges();
        }),
        finalize(this.cdr.markForCheck)
      ).toPromise();
    }

  }

  sensorTemplateChange(value) {
    let index = this.sensorsTemplate.findIndex(x => x.name == value);
    if (index >= 0) {
      this.currentSensorTemplate = this.sensorsTemplate[index].parameters;
      // set form default
    }
  }
  /**
     * Checking control validation
     *
     * @param controlName: string => Equals to formControlName
     * @param validationType: string => Equals to valitors name
     */
  isControlHasError(controlName: string, validationType: string): boolean {

    const control = this.frmDeviceEditBase.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
  private setConfigError() {
    this.configMessage = [];
    this.configMessage.push(
      {
        identifier: "imei",
        rules: [{
          type: "required",
          prompt: this.translateService.instant('COMMON.VALIDATION.REQUIRED_FIELD_NAME')
        }]
      }
    );
    this.configMessage.push(
      {
        identifier: "typeDevice",
        rules: [{
          type: "required",
          prompt: this.translateService.instant('COMMON.VALIDATION.PLEASE_CHOOSE_TYPE')
        }]
      }
    );
    // set config message for sensor
    this.configMessageSensor = [];

  }
  get f() {
    if (this.frmDeviceEditBase) return this.frmDeviceEditBase.controls;
  }
  get fAddvanced() {
    if (this.frmdeviceEditAdvanced) return this.frmdeviceEditAdvanced.controls;
  }

  get fExtentions() {
    if (this.frmdeviceEditExtension) return this.frmdeviceEditExtension.controls;
  }

  get fSensorBase() {
    if (this.formSensorBase) return this.formSensorBase.controls;
  }
  get fSensorAdvance() {
    if (this.formSensorAdvance) return this.formSensorAdvance.controls;
  }
  createDeviceGroup() {
    this.deviceGroupEventEmiter.emit(this.device.userId);
  }
  createDeviceGroupResult(result) {
    if (result.status == "success") {
      let deviceGroup = result.data as DeviceGroup;
      this.groupDevices.push(deviceGroup);
      this.getDeviceGroup(this.device.userId);
      let items = [];
      this.device.groupDevices.forEach(x => {
        items.push(x['id']);
      });
      items.push(deviceGroup.id);
      this.frmDeviceEditBase.controls['groupIds'].setValue(items.join(','));
      this.cdr.detectChanges();

      setTimeout(() => {
        $('#edGroupDevice').selectpicker();
        $('#edGroupDevice').val(items).selectpicker("refresh");
      }, 100)
    }
  }

  loadEditAdvanced() {
    let _this = this;
    if (this.loadFormAdvanced) return;
    else this.loadFormAdvanced = true;
    // // get list transport
    // // get list driver
    // // get list deparment manage
    // this.getListDeparmentManage();
    this.getListDriver(this.device);
    setTimeout(() => {
      $('.kt-selectpicker__edit-device--advanced').selectpicker();
      $('#edDeparment').val(_this.device.governmentId).selectpicker("refresh");
      $('#edTransportType').val(_this.device.transportType).selectpicker("refresh");
      $('#edTransportTypeQcvn').val(_this.device.transportTypeQcvn).selectpicker("refresh");

    })
  }

  getListDriver(device?: Device) {
    this.driverService.list({ params: { userId: this.device.userId, pageNo: 0 } }).pipe(
      tap(data => {
        this.listDriver = data.result;
        setTimeout(() => {
          if (device) {
            $("#edDriver").val(device.driverId).selectpicker('refresh');
          }
        });
      }),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }
  // getListDeparmentManage() {
  //   // this.listDeparmentManage = [];
  //   // let deparment = new Deparment();
  //   // deparment.name = "test";
  //   // deparment.id = 1;
  //   // this.listDeparmentManage.push(deparment);
  //   setTimeout(() => {
  //     $('#edDeparment').selectpicker();
  //     $('#edDeparment').val(this.device.governmentManage).selectpicker('refresh');
  //   });
  // }

  changeSendToGovernment(value) {

    if (this.frmdeviceEditAdvanced.get('sendToGovernment').value) {

      this.frmdeviceEditAdvanced.controls['governmentManage'].enable();
      this.frmdeviceEditAdvanced.controls['transportTypeQcvn'].enable();
      $('.sendToGov').selectpicker('refresh');

      this.frmdeviceEditAdvanced.get('numberPlate').setValidators(this.validatorCT.required);
      this.frmdeviceEditAdvanced.get('numberPlate').updateValueAndValidity();
      this.frmdeviceEditAdvanced.get('numberPlate').markAsTouched();

      this.frmdeviceEditAdvanced.get('governmentManage').setValidators(this.validatorCT.select);
      this.frmdeviceEditAdvanced.get('governmentManage').updateValueAndValidity();
      this.frmdeviceEditAdvanced.get('governmentManage').markAsTouched();

      this.frmdeviceEditAdvanced.get('transportTypeQcvn').setValidators(this.validatorCT.select);
      this.frmdeviceEditAdvanced.get('transportTypeQcvn').updateValueAndValidity();
      this.frmdeviceEditAdvanced.get('transportTypeQcvn').markAsTouched();
    }
    else {
      this.frmdeviceEditAdvanced.get('governmentManage').clearValidators();
      this.frmdeviceEditAdvanced.get('transportTypeQcvn').clearValidators();
      this.frmdeviceEditAdvanced.get('numberPlate').clearValidators();
      
      this.frmdeviceEditAdvanced.controls['governmentManage'].disable();
      this.frmdeviceEditAdvanced.controls['transportTypeQcvn'].disable();
      $('.sendToGov').selectpicker('refresh');
    }
  }

  //  Add driver
  addDriver() {
    this.addDriverEmiter.emit(this.device.userId);
  }
  // addDriverResult
  addDriverResult(result) {
    if (result.status == "success") {
      let driver = result.data as Driver;
      this.listDriver.push(driver);
      let _this = this;
      setTimeout(() => {
        $('#edDriver').val(driver.id).selectpicker("refresh");
      })
    }
  }
  selectIconSensor(icon) {
    this.iconSensorSelect = icon;
    this.cdr.detectChanges();
  }
  selectSensorType(type) {
    this.typeSensorSelect = type;
    this.cdr.detectChanges();
  }
  serviceExpireChange(date) {
    this.frmDeviceEditBase.get("serviceExpire").setValue(date['startDate']);
  }

  getTimeZones() {
    let listTimeZone = [];

    this.currentUser.listTimeZone.forEach(function (item) {
      var i = listTimeZone.findIndex(x => x.GMTOffset == item.GMTOffset);
      if(i <= -1){
        listTimeZone.push({GMTOffsetMinute: parseInt(item.GMTOffsetMinute), GMTOffset: item.GMTOffset});
      }
    });

    listTimeZone.sort((a, b) => a.GMTOffsetMinute - b.GMTOffsetMinute);

    // convert UTC ==> UTC +00:00
    listTimeZone.find(item => item.GMTOffset == 'UTC').GMTOffset = 'UTC +00:00';

    return listTimeZone;
  }
}
