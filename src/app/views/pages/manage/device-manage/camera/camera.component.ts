import { Component, OnInit, EventEmitter, Input, ViewChild, ElementRef, Output, ChangeDetectorRef } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DeviceGroup, DeviceGroupService, DriverService, Driver } from '@core/manage';
import { tap, count, takeUntil, finalize, timeout } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidatorCustomService, UserDatePipe } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import * as momment from  'moment';

declare var $: any;

@Component({
  selector: 'kt-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.scss'],
  providers: [UserDatePipe],
})
export class CameraComponent implements OnInit {

  @Input() addCameraOpen: EventEmitter<number>;
  @Output() adddriverResult: EventEmitter<{ status: string, message: string, data: any }>;
  public closeResult: string;
  private unsubscribe: Subject<any>;
  private userId: number;
  public form: FormGroup;
  private modalReference: NgbModalRef;
  public isSend:boolean = false;
  @ViewChild('content', { static: true }) content: ElementRef;
  constructor(
    private fb: FormBuilder,
    private driverSevice: DriverService,
    private validatorCT: ValidatorCustomService,
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef,
    private currentUser: CurrentUserService,
    private userDate: UserDatePipe) {
    this.adddriverResult = new EventEmitter();
    this.unsubscribe = new Subject();
  }
  ngOnInit() {
    if (this.addCameraOpen != undefined) {
      this.addCameraOpen.pipe(
        tap(userId => {
          this.userId = userId;
          this.open(this.content);
        })
      ).subscribe();
    }
  }
  open(content) {
    this.createForm();
    this.modalReference = this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', backdrop: 'static' });
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    setTimeout(() => {
      $('.kt-selectpicker-send-command').selectpicker();
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  createForm() {
    let _this = this;
    this.form = this.fb.group({
      name: ["", [Validators.required, this.validatorCT.maxLength(64)]],
      phone: ["", [Validators.required,this.validatorCT.phone]],
      email: ["", [this.validatorCT.email, this.validatorCT.maxLength(128)]],
      address: ["", [this.validatorCT.maxLength(255)]],
      driverCode: ["", [this.validatorCT.maxLength(20)]],
      licenseNumber: ["", this.validatorCT.maxLength(30)],
      beginDate: ["", this.validatorCT.maxLength(30)],
      expireDate: ["", this.validatorCT.maxLength(30)],
      description: ["", this.validatorCT.maxLength(256)],
    });
    setTimeout(() => {
      $("#dExpireDate").datepicker({
        autoclose: true,
        clearBtn: true,
        todayHighlight: true,
        format: _this.currentUser.dateFormat.toLowerCase(),
      });
      $("#dBeginDate").datepicker({
        autoclose: true,
        clearBtn: true,
        todayHighlight: true,
        format: _this.currentUser.dateFormat.toLowerCase(),
      });
    });
    this.isSend = false;
  }
  onSubmit() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.isSend = true;
      let driver = new Driver();
      driver.name = this.form.get('name').value;
      driver.phone = this.form.get('phone').value;
      driver.email = this.form.get('email').value;
      driver.address = this.form.get('address').value;
      driver.driverCode = this.form.get('driverCode').value;
      driver.licenseNumber = this.form.get('licenseNumber').value;
      driver.expireDate = momment($('#dExpireDate').datepicker("getDate")).format("YYYY-MM-DD");
      driver.beginDate = momment($('#dBeginDate').datepicker("getDate")).format("YYYY-MM-DD");
      driver.description = this.form.get('description').value;
      driver.userId = this.userId;

      this.driverSevice.create(driver, { notifyGlobal: true }).pipe(
        tap(result => {
          if (result.status == 201) {
            this.modalReference.close();
            this.adddriverResult.emit({ status: 'success', message: "", data: result.result });
          }
          else {
            this.adddriverResult.emit({ status: 'error', message: "", data: result.result });
          }
        },
          error => {
            this.adddriverResult.emit({ status: 'error', message: "", data: error.result });
          }),
        timeout(3000),
        takeUntil(this.unsubscribe),
        finalize(()=>{
          this.isSend =false;
          this.cdr.markForCheck();
        })
      ).subscribe();
    }
    else {
      setTimeout(() => {
        const firstElementWithError = document.querySelector('#form .invalid-feedback');
        if (firstElementWithError) {
          firstElementWithError.scrollIntoView({ block: 'center' });
        }
      });
    }
  }
}
