import { Component, OnInit, EventEmitter, Input, ViewChild, ElementRef, Output, ChangeDetectorRef } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DeviceService, Device, User } from '@core/manage';
import { tap, count, takeUntil, finalize } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'kt-sell-device',
  templateUrl: './sell-device.component.html',
  styleUrls: ['./sell-device.component.scss']
})
export class SellDeviceComponent implements OnInit {

  @Input() sellOpen: EventEmitter<Device[]>;  
  @Output() sellResult: EventEmitter<{ status: string, message: string, data: any }>;
  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  public devices: Device[];
  public closeResult: string;
  public userSelect: User;
  private unsubscribe: Subject<any>;
  private currentReason: any;
  public isSend:boolean = false;
  @ViewChild('content', { static: true }) content: ElementRef;
  constructor(private deviceService: DeviceService,
    private modalService: NgbModal,
    private cdr:ChangeDetectorRef) {
    this.devices = [];
    this.userSelect = new User();
    this.userSelect.clear();
    this.sellResult = new EventEmitter();
    this.unsubscribe = new Subject();
    this.currentReason = {};
  }
  ngOnInit() {
    if (this.sellOpen != undefined) {
      this.sellOpen.pipe(
        tap(devices => {
          this.devices = devices;
          this.open(this.content);
        })
      ).subscribe();
    }
  }
  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', size: 'lg', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      // this.currentReason = reason;      
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  changeUser(userSelect) {
    this.userSelect = userSelect;
  }
  onSubmit() {    
    this.isSend = true;
    let deviceIds = this.devices.map(x => x.id);
    this.deviceService.moves({ ids: deviceIds, userId: this.userSelect.id }, { notifyGlobal: true }).pipe(
      tap(result => {
        if (result.status == 200) {
          this.modalService.dismissAll(this.currentReason);
          this.sellResult.emit({ status: 'success', message: "", data: this.userSelect });
        }
        else {
          this.sellResult.emit({ status: 'error', message: result.message, data: this.userSelect });
        }
      }),
      
      takeUntil(this.unsubscribe),
      finalize(() => {      
        this.isSend = false;  
        this.cdr.markForCheck();
      })
    ).subscribe();
  }
}
