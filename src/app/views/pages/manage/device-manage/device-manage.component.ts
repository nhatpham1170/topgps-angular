import { type } from '@amcharts/amcharts4/core';

import { Component, OnInit, ElementRef, ChangeDetectorRef, ViewChild, EventEmitter } from '@angular/core';
import { finalize, takeUntil, tap } from 'rxjs/operators';
import { Subject, observable } from 'rxjs';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { Device, DeviceTypeService } from '@core/admin';
import { Device, DeviceService, User } from '@core/manage';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { HttpClient } from '@angular/common/http';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { TranslateService } from '@ngx-translate/core';
import { ToastService, UserDatePipe } from '@core/_base/layout';
import { DeviceUtilityService } from '@core/manage/utils/device-utility.service';
import { ExcelService } from '@core/utils';
import { XLSXModel } from '@core/utils/xlsx/excel.service';
import { headersToString } from 'selenium-webdriver/http';
import { RequestFileService } from '@core/common/_services/request-file.service';
import { RequestFile } from '@core/common/_models/request-file';
import { DeviceConfigService } from '@core/common/_services/device-config.service';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { identifierModuleUrl } from '@angular/compiler';
import { itemBody } from '@core/utils/pdfmake/pdfmake.service';

declare var $: any;
declare var ClipboardJS: any;

const TITLE_FORM_EDIT: string = "COMMON.ACTIONS.EDIT";
const TITLE_FORM_ADD: string = "COMMON.ACTIONS.ADD";
const TITLE_FORM_DELETE: string = "COMMON.ACTIONS.DELETE";

enum ACTION { edit, add, delete };

@Component({
  selector: 'kt-device-manage',
  templateUrl: './device-manage.component.html',
  styleUrls: ['./device-manage.component.scss'],
  // providers:[ExcelService],
})
export class DeviceManageComponent implements OnInit {
  public dataTable: DataTable = new DataTable();
  public action: string;
  public currentReason: any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public formEdit: FormGroup;
  public formAdd: FormGroup;
  public titlePopup: string = TITLE_FORM_ADD;
  private tableConfig: any;
  private currentForm: FormGroup;
  public currentModel: Device;
  public commandsForm: FormGroup;
  public isShowSearchAdvanced: boolean;
  public deviceType: any[];
  public simType: any[];
  public groupDevice: any[];

  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  private userIdSelecte: number;
  public currentUser: User;
  public dateTimeServer: string;
  // feature device
  public sellDeviceEmitter: EventEmitter<Device[]>;
  public sendCommandEmitter: EventEmitter<Device[]>;
  public renewsDeviceEmitter: EventEmitter<Device[]>;
  public importDeviceEmitter: EventEmitter<Device[]>;
  public editDeviceEmitter: EventEmitter<Device>;
  public deviceGroupEventEmiter: EventEmitter<number>;
  public addDriverEmiter: EventEmitter<number>;
  public addCameraEmiter: EventEmitter<Device>;

  public showUserTree = true;
  public allowReOrder: boolean = false;
  // permisison 
  public permissions: any = {
    sell: "device.asarction.sell"
  }

  constructor(
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private deviceService: DeviceService,
    private currentService: DeviceService,
    private translate: TranslateService,
    private toast: ToastService,
    private deviceUtility: DeviceUtilityService,
    private xlsx: ExcelService,
    private requestFile: RequestFileService,
    private deviceConfig: DeviceConfigService,
    private userTreeService: UserTreeService
  ) {
    this.action = "";
    this.tableConfig = {
      pagination: [
        10, 20, 30, 50
      ],
      columns: [
        {
          title: '#',
          field: 'id',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '20px' },
          width: 20,
          class: 't-datatable__cell--center',
          translate: '#',
          autoHide: false,
        },
        {
          title: 'Tài khoản',
          field: 'userName',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px' },
          width: 100,
          class: '',
          translate: 'COMMON.COLUMN.USERNAME',
          autoHide: false,
        },
        {
          title: 'IMEI',
          field: 'imei',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '125px' },
          width: 125,
          class: '',
          translate: 'COMMON.COLUMN.IMEI',
          autoHide: true,
        },
        {
          title: 'Name',
          field: 'name',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.NAME',
          autoHide: true,
        },
        {
          title: 'Loai',
          field: 'typeName',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px' },
          width: 100,
          class: '',
          translate: 'COMMON.COLUMN.TYPE',
          autoHide: true,
        },
        {
          title: 'Sim',
          field: 'simno',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '110px' },
          class: '',
          width: 110,
          translate: 'COMMON.COLUMN.SIM',
          autoHide: true,
        },
        {
          title: 'Hạn dịch vụ',
          field: 'serviceExpire',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '110px' },
          class: '',
          width: 110,
          translate: 'COMMON.COLUMN.SERVICE_EXPIRE',
          autoHide: true,
        },
        {
          title: 'Hạn bảo hành',
          field: 'warrantyExpiredDate',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '110px' },
          class: '',
          width: 110,
          translate: 'COMMON.COLUMN.WARRANTY_EXPIRE',
          autoHide: true,
        },
        {
          title: 'Trạng thái',
          field: 'status',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '70px' },
          class: '',
          width: 70,
          translate: 'COMMON.COLUMN.STATUS',
          autoHide: true,
        },
        {
          title: 'Đã bán',
          field: 'isSell',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '70px' },
          class: '',
          width: 70,
          translate: 'COMMON.COLUMN.IS_SELL',
          permission:"",
          autoHide: false,
          permisison:'ROLE_device.edit.sold'
        },
        {
          title: 'Actions',
          field: 'action',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '117px' },
          class: '',
          width: 117,
          translate: 'COMMON.ACTIONS.ACTIONS',
          autoHide: false,
        },
      ]
    };
    this.unsubscribe = new Subject();
    this.dataTable.init({
      data: [],
      totalRecod: 0,
      paginationSelect: [1, 2, 5, 10, 20],
      columns: this.tableConfig.columns,
      formSearch: '#formSearch',
      pageSize: 10,
      isDebug: false,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: true,
      }
    });
    this.currentReason = {};
    this.isShowSearchAdvanced = false;
    this.deviceType = [];
    this.simType = [];
    this.groupDevice = [];
    // console.log(this.dataTable);
    this.parrentEmiter = new EventEmitter();
    this.currentUser = new User();
    this.sellDeviceEmitter = new EventEmitter();
    this.sendCommandEmitter = new EventEmitter();
    this.renewsDeviceEmitter = new EventEmitter();
    this.importDeviceEmitter = new EventEmitter();
    this.editDeviceEmitter = new EventEmitter();
    this.deviceGroupEventEmiter = new EventEmitter();
    this.addDriverEmiter = new EventEmitter();
    this.addCameraEmiter = new EventEmitter();

    this.dateTimeServer = "";

  }
  ngOnInit() {

    this.getAllConfig();
    // listening dataTable event
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.getData(option);
      })
    ).subscribe();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();

    $(function () {
      $('select').selectpicker();
      $('.kt_datepicker').datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: "bottom left",
        templates: {
          leftArrow: '<i class="la la-angle-left"></i>',
          rightArrow: '<i class="la la-angle-right"></i>'
        },
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true
      });
    });
  }

	/**
	 * Create form 
	 * @param Device model
	 */
  createFormAdd(model?: Device) {
    this.formAdd = this.fb.group({
      name: ["", Validators.required],
      description: [""],
      // sortOrder: [1],
    });

    this.commandsForm = this.fb.group({
      itemRows: this.fb.array([])
    });

  }


  createFormEdit(model: Device) {
    this.formEdit = this.fb.group({
      // name: [model.name, Validators.required],
      // description: [model.description],
      // createdBy: [model.createdBy],
      // createdDate: [model.createdDate],
      // updatedBy: [model.updatedBy],
      // updatedDate: [model.updatedDate],
      // sortOrder: [model.sortOrder],
    });

    // let contentForm = [];
    // let _this = this;
    // model.commands.forEach(function (command) {
    //   contentForm.push(_this.fb.group({
    //     name: [command.name],
    //     commandStr: [command.commandStr],
    //   }));
    // });
    // this.commandsForm = this.fb.group({
    //   itemRows: this.fb.array(contentForm)
    // });

  }

	/**
	 * Get list by data option
	 * @param option option search list
	 */
  private getData(option) {
    if (option['orderBy'] == undefined || option['orderBy'].length == 0) {
      option['orderBy'] = "sortOrder";
      option['orderType'] = "asc";
    }
    this.deviceService.list({ params: option }).pipe(
      tap((data: any) => {
        this.dateTimeServer = data.datetime;
        data.result.content = this.deviceUtility.processDevices(data.result.content, this.dateTimeServer);

        this.dataTable.update({
          data: data.result.content,
          totalRecod: data.result.totalRecord
        });
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();

      })
    ).subscribe();
  }

	/**
	 * Open popup template
	 * @param content template
	 * @param type action type
	 * @param data data for content 
	 */
  open(content, type, item?: any) {

    this.action = type;

    switch (type) {
      case 'edit':
        this.editFnc(item.id, content);
        break;
      case 'add':
        this.addFnc(content);
        break;
      case 'delete':
        this.deleteFnc(item.id, content);
        break;
    }
  }

  editFnc(id: number, content) {
    this.titlePopup = TITLE_FORM_EDIT;
    // this.currentForm = this.formEdit;
    // this.deviceService.detail(id).pipe(
    //   tap(body => {
    //     this.currentModel = body.result as Device;
    //     this.createFormEdit(this.currentModel);
    //     this.currentForm = this.formEdit;
    //     this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', size: 'lg', backdrop: 'static' }).result.then((result) => {
    //       this.closeResult = `Closed with: ${result}`;
    //     }, (reason) => {
    //       this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    //       this.currentReason = reason;
    //     });
    //   }),
    //   takeUntil(this.unsubscribe),
    //   finalize(() => {
    //     this.cdr.markForCheck();
    //   })
    // ).subscribe();  
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', size: 'lg', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }

  addFnc(content) {
    this.titlePopup = TITLE_FORM_ADD;
    this.createFormAdd();
    this.currentForm = this.formAdd;
    this.currentModel = new Device();
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', size: 'lg', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }

  deleteFnc(id: number, content) {
    this.titlePopup = TITLE_FORM_DELETE;
    this.currentModel = new Device();
    this.currentModel.id = id;
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', size: 'sm', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }

  /**
 * Submit actions
 */
  onSubmit() {
    switch (this.action) {
      case "edit":
        if (!this.currentForm.invalid) {
          this.editAction();
        }
        break;
      case "add":
        if (!this.currentForm.invalid) {
          this.addAction();
        }
        break;
      case "delete":
        this.deleteAction();
        break;
    }
  }

  addAction() {
    this.currentModel.name = this.currentForm.get('name').value;
    this.currentModel.sortOrder = this.currentForm.get('sortOrder').value;
    this.currentModel.description = this.currentForm.get('description').value;
    // this.currentModel.commands = this.formArr.value;
    this.currentService.create(this.currentModel, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.modalService.dismissAll(this.currentReason);
          this.dataTable.reload({});
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  editAction() {
    this.currentModel.name = this.currentForm.get('name').value;
    this.currentModel.sortOrder = this.currentForm.get('sortOrder').value;
    this.currentModel.description = this.currentForm.get('description').value;
    // this.currentModel.commands = this.formArr.value;
    this.currentService.update(this.currentModel, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.modalService.dismissAll(this.currentReason);
          this.dataTable.reload({});
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  deleteAction() {
    this.deviceService.delete(this.currentModel.id, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.modalService.dismissAll(this.currentReason);
          this.dataTable.reload({});
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }
  changeStatus(status: number) {
    let dataSelected = this.getDataSelected();
    let arrStatus = [0, 1];
    if (dataSelected.length > 0) {
      let ids = (dataSelected.map(x => x.id));
      if (arrStatus.findIndex(x => x == status) >= 0) {
        this.currentService.changeStatus({ ids: ids, status: status }, { notifyGlobal: true }).pipe(
          tap(data => {
            if (data.status == 200) {
              this.modalService.dismissAll(this.currentReason);
              this.dataTable.reload({});
            }
          }),
          takeUntil(this.unsubscribe),
          finalize(() => {
            this.cdr.markForCheck();
          })
        ).subscribe();
      }
    }

    // this.deviceService.changeStatus(status)
  }
  getDataSelected() {
    if (this.dataTable.dataSelected.length == 0) {
      this.toast.show({ translate: "MANAGE.DEVICE.MESSAGE.NO_DEVICE_SELECTED" });
    }
    return this.dataTable.dataSelected;
  }

  removeCommand(index: number) {
    this.formArr.removeAt(index);
  }

  addCommand() {
    this.formArr.push(this.fb.group({
      name: [''],
      commandStr: [''],
    }));
  }

  get formArr(): FormArray {
    if (this.commandsForm) {
      return this.commandsForm.get('itemRows') as FormArray;
    }
  }

  toggleSearchAdvanced() {
    this.isShowSearchAdvanced = !this.isShowSearchAdvanced;
  }

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.currentForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
  /**
   * Change size databale
   * @param $elm 
   */
  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }
  /**
 * Dismiss Reason Popup
 * @param reason 
 */
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  test(dangerTpl) {
    // this.toast.copied();
    // this.dataTable.search();
    // console.log("test");
    // let value = $('.selectpicker').selectpicker('val');
    // console.log(value);
  }
  search(data?: any) {
    let listSearch = [];

    if (this.userIdSelecte != undefined) {
      listSearch.push({ key: 'userId', value: this.userIdSelecte });
     
    }

    this.dataTable.search(data || listSearch);
  }
  resetFormSearch() {
    $('#formSearch .selectpicker').val('default').selectpicker("refresh");
    $('#formSearch .kt_datepicker').val("").datepicker("update");
    $("#formSearch :input").each(function () {
      switch (this.type) {
        case 'checkbox':
          $(this).prop('checked', false);
          break;
        default:
          $(this).val('');
          break;
      }
    });
    this.search([{ key: 'userId', value: this.userIdSelecte }, { key: 'orderBy', value: 'sortOrder' }, { key: 'orderType', value: 'asc' }]);
    // this.dataTable.search();
  }

  getAllConfig() {
    this.deviceConfig.get().then(x => {

      this.deviceType = x.deviceTypes;
      // console.log(this.deviceType);
      this.simType = x.simTypes;
      setTimeout(() => {
        $('.selectpicker-device-type').selectpicker("refresh");
        $('.selectpicker-sim-type').selectpicker("refresh");
      }, 100);
    });
    // this.deviceType = result.deviceTypes;
    // this.simType = result.simTypes;
    // console.log(result);
    // this.deviceService.getAllConfig()
    //   .pipe(
    //     tap(data => {
    //       if (data.status == 200) {
    //         this.deviceType = data.result.data.deviceTypes;
    //         this.simType = data.result.data.simTypes;
    //         setTimeout(() => {
    //           $('.selectpicker-device-type').selectpicker("refresh");
    //           $('.selectpicker-sim-type').selectpicker("refresh");
    //         }, 100);
    //       }
    //     }),
    //     takeUntil(this.unsubscribe),
    //     finalize(() => {
    //       this.cdr.markForCheck();
    //     })
    //   )
    //   .subscribe();
  }
  getGroupDevice(userId: number) {

  }
  userTreeChangeUser(value) {
    if (value.id > 0) {
      this.userIdSelecte = value.id;
      this.currentUser = value;
      this.search();
    }
    // console.log(value);
  }
  sendEvent() {
    this.parrentEmiter.emit({ id: 5024, path: '234,5305,235' });
  }
  copied(val) {
    this.toast.copied(val);
  }

  //#region Feature sell device

  /**
   * Sell device fnc
   */
  sellDevice() {
    let data = this.getDataSelected();
    if (data.length > 0) {
      this.sellDeviceEmitter.emit(data);
    }
  }

  /**
   * Sell device result
   * @param result 
   */
  sellResult(result) {
    if (result.status == "success") {
      this.parrentEmiter.emit(result.data);
      this.dataTable.reload({});
    }
  }
  //#endregion
  // sendCommand
  sendCommand() {
    let data = this.getDataSelected();
    if (data.length > 0) {
      this.sendCommandEmitter.emit(data);
    }
  }
  sendCommandResult(result) {
  }

  //#region Feature renews device

  /**
   * Sell device fnc
   */
  renewsDevice() {
    let data = this.getDataSelected();
    if (data.length > 0) {
      this.renewsDeviceEmitter.emit(data);
    }
  }

  /**
   * Sell device result
   * @param result 
   */
  renewsDeviceResult(result) {
    if (result.status == "success") {
      this.dataTable.reload({});
    }
  }
  //#endregion

  //#region Feature renews device

  /**
   * Sell device fnc
   */
  importDevice() {
    this.importDeviceEmitter.emit();
  }

  /**
   * Sell device result
   * @param result 
   */
  importDeviceResult(result) {
    if (result.status == "success") {
      this.parrentEmiter.emit();
      this.dataTable.reload({});
    }
  }
  //#endregion

  //#endregion

  //#region Feature edit device

  /**
   * edit device fnc
   */
  editDevice(device: Device) {
    this.editDeviceEmitter.emit(device);
  }

  /**
   * edit device result
   * @param result 
   */
  editDeviceResult(result) {
    if (result.status == "success") {
      this.dataTable.reload({});
    }
  }
  //#endregion
  /**
   * Create device group
   */
  createDeviceGroup() {
    this.deviceGroupEventEmiter.emit(this.userIdSelecte);
  }

  /**
   *Add driver
   */
  addDriver() {
    this.addDriverEmiter.emit(this.userIdSelecte);
  }

  addCamera() {
    let data = this.getDataSelected();
    if (data.length > 0) {
      this.addCameraEmiter.emit(data);
    }
  }
  // feature export excel file
  exportXlSX(option?: { all: true }) {
    let _this = this;
    let data = [];
    if (option && option.all === true) {
      let apiSearch = this.dataTable.getDataSearch();
      apiSearch['pageNo'] = -1;
      if (this.dataTable.getPaginations()['total'] > 1000) {
        let requestFile = new RequestFile();
        requestFile.type = "DEVICE__LIST_DEVICE";
        requestFile.fileExtention = "XLSX";
        requestFile.params = apiSearch;
        this.requestFile.create(requestFile, { notifyGlobal: true }).subscribe();
      }
      else {
        this.deviceService.list({ params: apiSearch }).pipe(
          tap((data: any) => {
            let dataTemp = data.result.map(x => {
              x.status = (x.status == "1" ? _this.translate.instant('COMMON.GENERAL.ACTIVE') : _this.translate.instant('COMMON.GENERAL.INACTIVE'));
              return x;
            })
            this.exportFileXLSX(dataTemp);
          }),
          takeUntil(this.unsubscribe),
          finalize(() => {
            this.cdr.markForCheck();
          })
        ).subscribe();
      }
    }
    else {
      let dataTemp = this.getDataSelected();
      if (dataTemp.length > 0) {
        dataTemp = dataTemp.map(x => {
          x.status = (x.status == "1" ? _this.translate.instant('COMMON.GENERAL.ACTIVE') : _this.translate.instant('COMMON.GENERAL.INACTIVE'));
          return x;
        });
        this.exportFileXLSX(dataTemp);
      }
    }

  }
  exportPDF() {

  }
  private exportFileXLSX(data) {
    let config: XLSXModel = {
      file: {
        title: this.translate.instant('MANAGE.DEVICE.GENERAL.LIST_DEVICE'),
        prefixFileName: "Device",
        objName:"Device",
      },
      header: [
        {
          text: this.translate.instant('MANAGE.DEVICE.GENERAL.LIST_DEVICE'),
          type: "header"
        },
        {
          text: ""
        },
        {
          text: "!timezone"
        },
        {
          text: ""
        }
      ],
      columns: [
        {
          name: "#",
          columnData: "auto",
          wch: 5
        }, {
          name: this.translate.instant('COMMON.COLUMN.USERNAME'),
          columnData: "userName",
          wch: 20,
        },
        {
          name: this.translate.instant('COMMON.COLUMN.IMEI'),
          columnData: "imei",
          wch: 25
        },
        {
          name: this.translate.instant('COMMON.COLUMN.NAME'),
          columnData: "name",
          wch: 25
        },
        {
          name: this.translate.instant('COMMON.COLUMN.TYPE'),
          columnData: "typeName",
          wch: 12
        },
        {
          name: this.translate.instant('COMMON.COLUMN.SIM'),
          columnData: "simno",
          wch: 15
        },
        {
          name: this.translate.instant('COMMON.COLUMN.SERVICE_EXPIRE'),
          columnData: "serviceExpire",
          wch: 15,
          type: "date",
        },
        {
          name: this.translate.instant('COMMON.COLUMN.WARRANTY_EXPIRE'),
          columnData: "warrantyExpiredDate",
          wch: 15,
          type: "date",
        },
        {
          name: this.translate.instant('COMMON.COLUMN.STATUS'),
          columnData: "status",
          wch: 15,
        },
      ],
      woorksheet: {
        name: this.translate.instant('MANAGE.DEVICE.GENERAL.DEVICE'),
      }
    }
    this.xlsx.exportFile(data, config, {});
  }
  async drop(event: CdkDragDrop<string[]>) {
    // console.log(event);
    if (event.previousIndex == event.currentIndex) return;
    

    // filter data 

    let userId = this.dataTable.data[event.previousIndex].userId;
    let pageSize = this.dataTable.getPaginations().pageSize;
    let currentPage = this.dataTable.getPaginations().currentPage;
    let index = pageSize * (currentPage - 1);
    let arrayCheck = this.dataTable.data.filter(x => x.userId == userId);

    moveItemInArray(arrayCheck, event.previousIndex, event.currentIndex);
    if (arrayCheck.length == this.dataTable.data.length) {
      // let params:{ id: number, sortOrder: number }[]= [];
      let params = arrayCheck.map(x => {
        index++;
        return { id: x.id, sortOrder: index };
      });      
      let result = false;
      await this.deviceService.sortOrder(params, { notifyGlobal: true }).pipe(
        tap(data => {
          if (data.status == 200) {
            result = true;
            moveItemInArray(this.dataTable.data, event.previousIndex, event.currentIndex);
            this.cdr.detectChanges();
          }
        }),
        takeUntil(this.unsubscribe),
        finalize(this.cdr.markForCheck)
      ).toPromise();         
    }

  }
  sortOrder() {
    this.allowReOrder = !this.allowReOrder;
  }
  soldDevice(item) {
    this.deviceService.sell({id:item.id}, { notifyGlobal: true }).pipe(
      tap(result => {
        if (result.status == 200) {
          this.dataTable.reload({});
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }
}


