import { Component, OnInit, EventEmitter, Input, ViewChild, ElementRef, Output, ChangeDetectorRef } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DeviceService, Device, User } from '@core/manage';
import { tap, count, takeUntil, finalize, timeout } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DeviceType } from '@core/admin';
import { Icon } from '@core/manage/_models/icon';
import { XlsxService } from '@core/utils';
import { DeviceConfigService } from '@core/common/_services/device-config.service';

declare var $: any;
@Component({
  selector: 'kt-import-device',
  templateUrl: './import-device.component.html',
  styleUrls: ['./import-device.component.scss']
})
export class ImportDeviceComponent implements OnInit {

  @Input() importDeviceOpen: EventEmitter<any>;
  @Input() config: any[];
  @Output() importDeviceResult: EventEmitter<{ status: string, message: string, data: any }>;
  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  public devices: Device[];
  public closeResult: string;
  private unsubscribe: Subject<any>;
  private currentReason: any;
  public types: DeviceType[];
  public icons: Icon[];
  public imeis: { imei: string, status: string }[];
  public imeiImport;
  public countImeiUpdate: number = 0;
  public statusUpdated: string = "";
  private iconIdSelected: number = 0;
  private deviceTypeIdSelected: number = 0;
  private configAll: any;
  private listData:Array<{imei:string,vinNumber:string,frameNumber:string}> = [];
  public isSend:boolean = false;


  @ViewChild('content', { static: true }) content: ElementRef;

  constructor(private deviceService: DeviceService,
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef,
    private xlsx: XlsxService,
    private deviceConfig:DeviceConfigService) {    
    this.importDeviceResult = new EventEmitter();
   
    this.imeis = [];
    this.unsubscribe = new Subject();
    this.currentReason = {};
    this.listData = [];
  }
  ngOnInit() {
    if (this.importDeviceOpen != undefined) {
      this.importDeviceOpen.pipe(
        tap(() => {
          this.open(this.content);
        })
      ).subscribe();
    }
    
  }
  open(content) {
    if (this.config == undefined) {
      this.deviceConfig.get().then(x => {
        this.configAll = x;
        this.types = this.configAll.deviceTypes;
        this.icons = this.configAll.deviceIcons;
        setTimeout(() => {
          $('.selectpicker-device-type').selectpicker("refresh");
          $('.selectpicker-sim-type').selectpicker("refresh");
        }, 100);
      });
    }
    else {
      this.configAll = this.config;
      this.types = this.configAll.deviceTypes;
      this.icons = this.configAll.deviceIcons;
    }
    this.resetForm();
    this.imeis = [];
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    setTimeout(() => {
      $('.kt-selectpicker-import-device').selectpicker();
      $(".auto-format-text").change(function () {
        var val = $(this).val().replace(/,/g, "\n").replace(/ /g, "");
        $(this).val(val);
      });
    });
  }
  resetForm() {
    this.imeiImport = "";
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  onSubmit() {
    let _this=this;
    this.isSend = true;
    // proceess device
    let listDevice = [];
    
    this.imeis.forEach(x => {
      let deviceObj = {imei:x.imei,vinNumber:"",frameNumber:""};
      let index = _this.listData.find(d=>d.imei ==x.imei);
      if(index) listDevice.push(index);
      else listDevice.push(deviceObj);
    });
    let param = { devices: listDevice };

    if (this.deviceTypeIdSelected > 0) {
      param['deviceTypeId'] = this.deviceTypeIdSelected;
    }
    if (this.iconIdSelected > 0) {
      param['iconTypeId'] = this.iconIdSelected;
    }

    this.deviceService.imporDevice(param, { notifyGlobal: true }).pipe(
      tap(result => {
        if (result.status == 201) {
          this.importDeviceResult.emit({ status: 'success', message: "", data: [] });
          // this.processDataResult("success", result.result.content);
          this.modalService.dismissAll(this.currentReason);
        }
      },
        error => {
          if (error.error.status == "400") {
            this.processDataResult("error", error.error.result);
            this.importDeviceResult.emit({ status: 'error', message: "", data: [] });
          }
        }
      ),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.isSend = false;
        this.cdr.markForCheck();
      })
    ).subscribe();
  }
  removeImei(imei: string) {
    this.imeis = this.imeis.filter(x => x.imei != imei);
  }
  chooseFile() {
    let file: HTMLElement = document.getElementById("fileName") as HTMLElement;
    file.click();
  }
  changeFile(files) {
    let _this = this;
    let imeis: string[] = [];
    this.xlsx.readXLSXToJson(files, function (data) {
      if (Object.keys(data).length > 0) {
        data[Object.keys(data)[0]].forEach((x, i) => {
          if (x[0] != undefined && i > 0) {
            let imei = x[0].toString();
            let object = {imei:"",vinNumber:"",frameNumber:""};
            // imei:x[0].toString(),vinNumber:x[1].toString(),frameNumber:x[2].toString()
            if(x[0]) object['imei'] = x[0].toString();
            if(x[1]) object['vinNumber'] = x[1].toString();
            if(x[2]) object['frameNumber'] = x[2].toString();
            _this.listData.push(object);
            imeis.push(imei);
          }
        })
      }
      if (imeis.length > 0) {
        _this.imeiImport += (_this.imeiImport.trim().length > 0 ? "\n" : "") + imeis.join("\n");
      }
    });

  }
  addImeis() {
    let _this = this;
    let value = this.imeiImport as String;
    let imeis = value.trim().replace(/\n/g, ",").split(',');
    imeis.forEach(x => {
      if (x.length > 6 && x.length < 20) {
        if (_this.imeis.findIndex(imei => imei.imei == x) < 0) {
          _this.imeis.push({ imei: x, status: "" });
        }
      }
    });
    this.cdr.markForCheck();
  }
  clearData() {
    this.imeis = [];
    this.cdr.markForCheck();
  }
  changeDeviceType(value) {
    this.deviceTypeIdSelected = value;
  }
  changeIcon(value) {
    this.iconIdSelected = value;
  }
  processDataResult(status: string, data: []) {
    if(Array.isArray(data)){
      this.imeis = this.imeis.map(imei => {
        if (data.findIndex(x => x['imei'] == imei.imei))
          imei.status = status;
        else imei.status = "";
        return imei;
      });
    }
    
  }

}
