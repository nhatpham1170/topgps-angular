import { Component, OnInit, EventEmitter, Input, ViewChild, ElementRef, Output, ChangeDetectorRef } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DeviceService, Device, User, PointService, ExtendModel } from '@core/manage';
import { tap, count, takeUntil, finalize } from 'rxjs/operators';
import { Subject } from 'rxjs';
import * as moment from 'moment'; // add this 1 of 4
import { TranslateService } from '@ngx-translate/core';
import { CurrentUserService } from '@core/auth';
import { ToastService, ValidatorCustomService } from '@core/_base/layout';
import { NgxPermissionsService } from 'ngx-permissions';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';

declare var $: any;
@Component({
  selector: 'kt-renews-device',
  templateUrl: './renews-device.component.html',
  styleUrls: ['./renews-device.component.scss']
})
export class RenewsDeviceComponent implements OnInit {

  @Input() sellOpen: EventEmitter<Device[]>;
  @Output() sellResult: EventEmitter<{ status: string, message: string, data: any }>;
  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  public confirmRenews: EventEmitter<any> = new EventEmitter();
  public devices: Device[];
  public closeResult: string;
  public userSelect: User;
  private unsubscribe: Subject<any>;
  private currentReason: any;
  private dateNow;
  public dateSelected: string;
  public monthSelect: number;
  public totalPoint: number;
  public totalDay: number;
  public totalError: number;
  public totalSuccess: number;
  public statusUpdate: string;
  public isSend:boolean = false;
  public renewsObj:any = {};
  public rangeMonths:Array<{value:number,text:string}> =[
    {value:6,text:"MANAGE.DEVICE.RENEWS.MONTHS.ADD_6_MONTHS"},
    {value:12,text:"MANAGE.DEVICE.RENEWS.MONTHS.ADD_1_YEAR"},
    {value:24,text:"MANAGE.DEVICE.RENEWS.MONTHS.ADD_2_YEARS"},
    {value:36,text:"MANAGE.DEVICE.RENEWS.MONTHS.ADD_3_YEARS"},    
  ];
  public optionsPayAt = {
    singleDatePicker:true,
    size: 'md',
    select: 'from',
    timePicker:false,
    format:'date',
    autoSelect:false,
    btnClear:true,
    optionDatePicker:{
      drops:'down',
      autoPlay:true
    }
} 
  private isUsedOldService:boolean = false;
  public formRenews: FormGroup;
  @ViewChild('content', { static: true }) content: ElementRef;
  @ViewChild('payAtCpn', { static: false }) payAtCpn: DateRangePickerComponent;
  constructor(private deviceService: DeviceService,
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    private currentUser: CurrentUserService,
    private toast: ToastService,
    private pointService:PointService,
    private permisionService:NgxPermissionsService,
    private validatorCT:ValidatorCustomService,
    private fb: FormBuilder) {
    this.devices = [];
    this.userSelect = new User();
    this.userSelect.clear();
    this.sellResult = new EventEmitter();
    this.unsubscribe = new Subject();
    this.currentReason = {};
    this.dateNow = moment().format(this.currentUser.dateFormat);
    this.dateSelected = this.dateNow;
    this.statusUpdate = "";
    // this.monthSelect = 12;
  }
  ngOnInit() {
    if(this.permisionService.getPermission('ROLE_device.renewed.one_month'))
    {
      this.rangeMonths.unshift({ value: 1, text: "MANAGE.DEVICE.RENEWS.MONTHS.ADD_1_MONTH" });
    }
    this.monthSelect = 12;
    if (this.sellOpen != undefined) {
      this.sellOpen.pipe(
        tap(devices => {
          // let date = this.dateNow;
          this.calculatorMonth(this.monthSelect, devices, true);
          this.open(this.content);
        })
      ).subscribe();
    }
    setTimeout(() => {
      $("#monthRenews").selectpicker();    
      
    });
  }
  open(content) {
    this.createForm();
    let _this=this;
    this.statusUpdate = "";
    this.totalError = 0;
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal-xl', size: 'lg', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      // this.currentReason = reason;      
    });
    
    setTimeout(() => {
      $("#monthRenews").val(this.monthSelect).selectpicker();    
      $(".selectpicker").selectpicker();    
    });

  }
  private createForm() {
   
    this.optionsPayAt.autoSelect = false;
    if(this.payAtCpn) this.payAtCpn.refresh();
    this.formRenews = this.fb.group({
      // receiveUserName: new FormControl({ value: userReceive.username, disabled: true }),
      // receiveUserId: new FormControl({ value: userReceive.id, disabled: true }),
      // transactionPoint: new FormControl({ value: 0, disabled: false },[this.validatorCT.min(6)]),     
      renewsAmount: new FormControl({ value: 0, disabled: false }, [this.validatorCT.number, this.validatorCT.min(0)]),
      // renewsDescription: [ "",],
      renewsDescription: new FormControl({ value: "", disabled: false }, []),
      usedOldService: new FormControl({ value: false, disabled: false }, []),
      payAt: new FormControl({ value: "", disabled: false }, []),
      // renewsDescription: new FormControl({ value: "", disabled: false },[this.validatorCT.required]),
    });
    // setTimeout(()=>{
    //   $("#formMovePoint .selectpicker").selectpicker();
    // });
  }
  onChangeMonthRenews(months){
    this.monthSelect = months;
    this.calculatorMonth(months,this.devices);

  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  changeUser(userSelect) {
    this.userSelect = userSelect;
  }
  onSubmit() {    
    let valid = true;
    if (this.userSelect['currentPoint'] == undefined || Number.parseInt(this.userSelect['currentPoint']) < this.totalPoint) {
      valid = false;
      this.toast.show({ translate: "MANAGE.DEVICE.RENEWS.VALIDATION.POINTS_NOT_ENOUGH", type: "info" });
    }
    else if (this.devices.length == 0) {
      valid = false;
      this.toast.show({ translate: "MANAGE.DEVICE.RENEWS.VALIDATION.NO_DEVICES_HAVE_BEEN_SELECTED", type: "info" });
    }
    else if (this.totalPoint == 0) {
      valid = false;
      this.toast.show({ translate: "MANAGE.DEVICE.RENEWS.VALIDATION.NO_DEVICES_HAVE_BEEN_RENEWED", type: "info" });
    }
    this.formRenews.markAllAsTouched();
    if (this.formRenews.valid) {
      if (valid) {
        this.confirmRenews.emit();
      }
    };
  }
  confirmResult(value){    
    if(value){
      this.isSend = true;
      this.renewsDevices();
      this.cdr.detectChanges();
    }       
    
  }

  renewsDevices(){
    let renewsObject = new ExtendModel();
    renewsObject.description = $("#renewsDescription").val();
    renewsObject.deviceIds = this.devices.map(x => x.id);
    renewsObject.isPay = $("#renewsPay").val();
    renewsObject.month = this.monthSelect;
    renewsObject.userIdExtend = this.userSelect.id;     
    if(this.renewsObj['payAt'])renewsObject.payAt = this.renewsObj['payAt'];
    if(this.renewsObj["billImg"]) renewsObject.billImg = this.renewsObj["billImg"] ;
    renewsObject.amount = this.formRenews.get('renewsAmount').value;

    this.pointService.extend(renewsObject,{notifyGlobal:true}).pipe(
      tap(result => {
        if (result.status == 200) {
          let data = result.result;
          this.statusUpdate = "success";        
          this.modalService.dismissAll(this.currentReason);
          this.sellResult.emit({ status: 'success', message: "", data: this.userSelect });
        }
      },
        error => {
          this.statusUpdate = "error";
          if (Array.isArray(error.error.result)) {
            let data = error.error.result;
            let _this = this;
            this.devices = this.devices.map(
              d => {
                d['updated'] = true;
                if (data.findIndex(x => x.id == d.id) >= 0) {
                  _this.totalError++;
                  d['renewsStatus'] = "error";
                }
                return d;
              }
            );
          }
          this.sellResult.emit({ status: 'error', message: error.message, data: this.userSelect });
          this.cdr.detectChanges();
        }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.isSend = false;
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  /**
   * Remove device
   * @param id 
   */
  removeDevice(id) {
    this.devices = this.devices.filter(x => x.id != id);
    this.calculatorMonth(this.monthSelect, this.devices);
  }

  /**
   * Calculator day add and point
   * @param date 
   * @param data 
   * @param init 
   */
  calculatorMonth(months, data, init?: boolean) {
    this.totalDay = 0;
    this.totalPoint = 0;
    this.totalError = 0;
    this.dateSelected = months;
    let devices = data.map(x => {
      
      x["day_renews"] = "0";
      x["point_renews"] = months;
      x["dateExpireNew"] = "";
      if (init) {
        x["renewsStatus"] = "";
        x["updated"] = false;
      }
      if (x["renewsStatus"] == "error") this.totalError++;

      let dateExpire = "";
      if (x.serviceExpire == null || x.serviceExpire.length == 0) {
        dateExpire = moment().subtract(1, "days").format("YYYY/MM/DD");
      }
      else{
        dateExpire = x.serviceExpire.split(" ")[0];
      }
      let dateExist = moment(dateExpire, "YYYY/MM/DD").diff(moment(this.dateNow, this.currentUser.dateFormat));
      let days = 0;
      if (dateExist >= 0) {
        // date valid
        x["dateExpireNew"] = moment(x.serviceExpire, "YYYY/MM/DD HH:mm:ss").add("months", months).format("YYYY/MM/DD HH:mm:ss");
        // x["dateExpireNew"] = moment(x.serviceExpire, "YYYY/MM/DD HH:mm:ss").add("months", months).format(this.currentUser.dateFormat + " " +this.currentUser.timeFormat);
        // days = moment.duration(moment(date, this.currentUser.dateFormat).diff(moment(dateExpire, "YYYY/MM/DD"))).asDays();
      }
      else {
        // date invalid
        if(this.isUsedOldService){
          x["dateExpireNew"] = moment(x.serviceExpire, "YYYY/MM/DD HH:mm:ss").add("months", months).format("YYYY/MM/DD HH:mm:ss");
        }
        else{
          x["dateExpireNew"] = moment(this.dateNow, this.currentUser.dateFormat).add("months", months).format("YYYY/MM/DD HH:mm:ss");
        }
        // days = moment.duration(moment(date, this.currentUser.dateFormat).diff(moment(this.dateNow, this.currentUser.dateFormat))).asDays() + 1;
      }
      this.totalPoint += Math.trunc(months);
      if (days > 0) {
        // this.totalDay += Math.trunc(days);
        
        // x["day_renews"] = days.toFixed(0);
        // x["point_renews"] = days.toFixed(0);
      }
      if (x.serviceExpire != null && x.serviceExpire.length > 0) {

      }
      return x;
    });
    this.devices = devices;
    this.cdr.markForCheck();
  }

  /**
   * Calculator day add and point
   * @param date 
   * @param data 
   * @param init 
   */
  calucaltor(date, data, init?: boolean) {
    this.totalDay = 0;
    this.totalPoint = 0;
    this.totalError = 0;
    this.dateSelected = date;
    let devices = data.map(x => {
      x["day_renews"] = "0";
      x["point_renews"] = "0";
      if (init) {
        x["renewsStatus"] = "";
        x["updated"] = false;
      }
      if (x["renewsStatus"] == "error") this.totalError++;

      let dateExpire = "";
      if (x.serviceExpire == null || x.serviceExpire.length == 0) {
        dateExpire = moment().subtract(1, "days").format("YYYY/MM/DD");
      }
      else{
        dateExpire = x.serviceExpire.split(" ")[0];
      }
      let dateExist = moment(dateExpire, "YYYY/MM/DD").diff(moment(this.dateNow, this.currentUser.dateFormat));
      let days = 0;
      if (dateExist >= 0) {
        days = moment.duration(moment(date, this.currentUser.dateFormat).diff(moment(dateExpire, "YYYY/MM/DD"))).asDays();
      }
      else {
        days = moment.duration(moment(date, this.currentUser.dateFormat).diff(moment(this.dateNow, this.currentUser.dateFormat))).asDays() + 1;
      }
      if (days > 0) {
        this.totalDay += Math.trunc(days);
        this.totalPoint += Math.trunc(days);
        x["day_renews"] = days.toFixed(0);
        x["point_renews"] = days.toFixed(0);
      }
      if (x.serviceExpire != null && x.serviceExpire.length > 0) {

      }
      return x;
    });
    this.devices = devices;
    this.cdr.markForCheck();
  }
  billImgChange(value){
    if(value.length>0){
      this.renewsObj['billImg'] = value[0].file
    }
  }
  payAtMove(value){    
    if(value.endDate){
      this.renewsObj['payAt'] = value.endDate.format("YYYY-MM-DD HH:mm:ss");      
    }
    else{
      this.renewsObj['payAt'] = "";
    }
    this.formRenews.get('payAt').setValue(this.renewsObj['payAt']);
  }
  get fRenews() {
    return this.formRenews.controls;
  }

  onChangeUsedOldService(value){
    this.isUsedOldService = this.formRenews.get('usedOldService').value;    
    this.calculatorMonth(this.monthSelect, this.devices);
  }
  changeTypePay(value){
    if(value == 1){
      this.optionsPayAt.autoSelect = true;
      this.payAtCpn.refresh();
      this.formRenews.get('payAt').setValidators(this.validatorCT.required);
      this.formRenews.get('payAt').updateValueAndValidity();
      this.formRenews.get('payAt').markAsTouched();
    }
    else{
      this.optionsPayAt.autoSelect = false;
      this.payAtCpn.refresh();
      this.formRenews.get('payAt').clearValidators();
      this.formRenews.get('payAt').updateValueAndValidity();
      this.formRenews.get('payAt').markAsTouched();
    }
  }
}
