import { Component, OnInit, EventEmitter, Input, ViewChild, ElementRef, Output, ChangeDetectorRef } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DeviceGroup, DeviceGroupService } from '@core/manage';
import { tap, count, takeUntil, finalize, timeout } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var $: any;

@Component({
  selector: 'kt-create-device-group',
  templateUrl: './create-device-group.component.html',
  styleUrls: ['./create-device-group.component.scss']
})
export class CreateDeviceGroupComponent implements OnInit {

  @Input() createDeviceGroupOpen: EventEmitter<number>;
  @Output() createDeviceGroupResult: EventEmitter<{ status: string, message: string, data: any }>;
  public closeResult: string;
  private unsubscribe: Subject<any>;
  private userId: number;
  public form: FormGroup;
  private modalReference: NgbModalRef;
  public isSend:boolean = false;
  @ViewChild('content', { static: true }) content: ElementRef;
  constructor(
    private fb: FormBuilder,
    private deviceGroupSevice: DeviceGroupService,
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef) {
    this.createDeviceGroupResult = new EventEmitter();
    this.unsubscribe = new Subject();
  }
  ngOnInit() {
    if (this.createDeviceGroupOpen != undefined) {
      this.createDeviceGroupOpen.pipe(
        tap(userId => {
          this.userId = userId;
          this.open(this.content);
        })
      ).subscribe();
    }
  }
  open(content) {
    this.createForm();
    this.modalReference = this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', backdrop: 'static' });
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    setTimeout(() => {
      $('.kt-selectpicker-send-command').selectpicker();
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  createForm() {
    this.form = this.fb.group({
      groupName: ["", Validators.required],
    })
  }
  onSubmit() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.isSend = true;
      let deviceGroup = new DeviceGroup();
      deviceGroup.name = this.form.get('groupName').value;
      deviceGroup.userId = this.userId;
      this.deviceGroupSevice.create(deviceGroup, { notifyGlobal: true }).pipe(
        tap(result => {
          if (result.status == 201) {
            this.modalReference.close();
            this.createDeviceGroupResult.emit({ status: 'success', message: "", data: result.result });
          }
          else {
            this.createDeviceGroupResult.emit({ status: 'error', message: "", data: result.result });
          }
        },
          error => {
            this.createDeviceGroupResult.emit({ status: 'error', message: "", data: error.result });
          }),
        takeUntil(this.unsubscribe),
        finalize(()=>{
          this.isSend = false;
          this.cdr.markForCheck();
        }),
      ).subscribe();
    }
  }
}
