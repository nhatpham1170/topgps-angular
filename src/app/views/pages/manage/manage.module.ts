import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';// Angular Material
import { SharedModule } from '@app/core/common';
import { CommonModule } from '@angular/common';
import { NgbDropdownModule, NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {NgxMaskModule} from "ngx-mask";
import { SortablejsModule } from 'ngx-sortablejs';

// Translate
import { TranslateModule } from '@ngx-translate/core';
// Component

import { TaskRemindComponent } from './task-remind/task-remind.component';

import { TripComponent } from './trip/trip.component';
import { EventLogComponent } from './activity-log/event-log/event-log.component';

import { DriverManageComponent } from './driver-manage/driver-manage.component';

import { DeviceManageComponent } from './device-manage/device-manage.component';

import { DeviceCommandComponent } from './device-command/device-command.component';
import { DeviceGroupComponent } from './device-group/device-group.component';
import { UserManageComponent } from './user-manage/user-manage.component';
import { UserAliasComponent } from './user-alias/user-alias.component';
import { RoleAliasComponent } from './role-alias/role-alias.component';
import { GeofenceGroupComponent } from './geofence-group/geofence-group.component';

import { AlertRulesComponent } from './alert-rules/alert-rules.component';
import { AlertComponent } from './alert/alert.component';
import { ReportScheduleComponent } from './report-schedule/report-schedule.component';
import { PoiManageComponent } from './poi-manage/poi-manage.component';

import { ManageComponent } from './manage.component';
import { ModuleGuard, AuthGuard } from '../../../../../src/app/core/auth';
import { PartialsModule } from '@app/views/partials/partials.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { ResizeObserverDirective } from '@core/_base/layout';
import { CoreModule } from '@core/core.module';
import { ClipboardModule } from 'ngx-clipboard';
import { SellDeviceComponent } from './device-manage/sell-device/sell-device.component';
// import { SendCommandComponent } from './device-manage/send-command/send-command.component';
import { ImportDeviceComponent } from './device-manage/import-device/import-device.component';
import { RenewsDeviceComponent } from './device-manage/renews-device/renews-device.component';
import { EditDeviceComponent } from './device-manage/edit-device/edit-device.component';
import { NgxPermissionsModule } from 'ngx-permissions';
import { CreateDeviceGroupComponent } from './device-manage/create-device-group/create-device-group.component';
import { AddDriverComponent } from './device-manage/add-driver/add-driver.component';
import { CameraComponent } from './device-manage/camera/camera.component';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { GeofenceManageComponent } from './geofence-manage/geofence-manage.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
// import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletDrawModule } from '@asymmetrik/ngx-leaflet-draw';
import { GeofenceToolComponent } from './geofence-manage/geofence-tool/geofence-tool.component';
import { PoiToolComponent } from './poi-manage/poi-tool/poi-tool.component';

//activity log
import { ActivityLogComponent } from './activity-log/activity-log.component';
//message
import { MessageComponent } from './message/message.component';
import { QuillModule } from 'ngx-quill';

// Map API Key
import { MapApiKeyComponent } from './map-api-key/map-api-key.component';

const routes: Routes = [{
  path: '',
  component: ManageComponent,

  children: [
    {
      path: 'pois',
      component: PoiManageComponent,
      canActivate: [AuthGuard, ModuleGuard],
      data: {
        // permisison: 'ROLE_manage.poi',
      }
    },
    {
      path: 'geofence-group',
      component: GeofenceGroupComponent,
      canActivate: [AuthGuard, ModuleGuard],
      data: {
        permisison: 'ROLE_manage.geofence_group',
      }
    },
    {
      path: 'devices',
      component: DeviceManageComponent,
      canActivate: [AuthGuard, ModuleGuard],
      data: {
        permisison: 'ROLE_manage.device',
      }
    },
    {
      path: 'command',
      canActivate: [AuthGuard, ModuleGuard],
      component: DeviceCommandComponent,
      data: {
        permisison: 'ROLE_manage.command',
      }
    },
    {
      path: 'device-group',
      canActivate: [AuthGuard, ModuleGuard],
      component: DeviceGroupComponent,
      data: {
        permisison: 'ROLE_manage.device_group',
      }
    },
    {
      path: 'users',
      canActivate: [AuthGuard, ModuleGuard],
      component: UserManageComponent,
      data: {
        permisison: 'ROLE_manage.user',
      }
    },
    {
      path: 'user-alias',
      canActivate: [AuthGuard, ModuleGuard],
      component: UserAliasComponent,
      data: {
        permisison: 'ROLE_manage.alias',
      }
    },
    {
      path: 'activity-log',
      canActivate: [AuthGuard, ModuleGuard],
      component: ActivityLogComponent,
      data: {
        permisison: 'ROLE_manage.activity_log',
      }
    },
    {
      path: 'role-alias',
      canActivate: [AuthGuard, ModuleGuard],
      component: RoleAliasComponent,
      data: {
        permisison: 'ROLE_manage.role_alias',
      }
    },
    {
      path: 'notifications',
      canActivate: [AuthGuard, ModuleGuard],
      component: AlertComponent,
      data: {
        permisison: 'ROLE_manage.alert',
      }
    },
    {
      path: 'alert-rules',
      canActivate: [AuthGuard, ModuleGuard],
      component: AlertRulesComponent,
      data: {
        permisison: 'ROLE_manage.alert_rules',
      }
    },
    {
      path: 'trip',
      canActivate: [AuthGuard, ModuleGuard],
      component: TripComponent,
      data: {
        permisison: 'ROLE_manage.trip',
      }
    },
    {
      path: 'task-remind',
      canActivate: [AuthGuard, ModuleGuard],
      component: TaskRemindComponent,
      data: {
        permisison: 'ROLE_manage.trip',
      }
    },
    
    {
      path: 'geofences',
      canActivate: [AuthGuard, ModuleGuard],
      component: GeofenceManageComponent,
      data: {
        permisison: 'ROLE_manage.geofence',
      }
    },
    {
      path: 'report-schedule',
      canActivate: [AuthGuard, ModuleGuard],
      component: ReportScheduleComponent,
      data: {
        permisison: 'ROLE_manage.schedule_report',
      }
    },
    {
      path: 'message',
      canActivate: [AuthGuard, ModuleGuard],
      component: MessageComponent,
      data: {
        permisison: 'ROLE_manage.message',
      }
    },
    {
      path: 'driver',
      component: DriverManageComponent,
      canActivate: [AuthGuard, ModuleGuard],
      data: {
        permisison: 'ROLE_manage.driver',
      }
    },
    {
      path: 'camera',
      component: CameraComponent,
      canActivate: [AuthGuard, ModuleGuard],
      data: {
        permisison: 'ROLE_manage.camera',
      }
    },
    {
      path: 'map-api-key',
      component: MapApiKeyComponent,
      canActivate: [AuthGuard, ModuleGuard],
      data: {
        permisison: 'ROLE_manage.api_key',
      }
    }
  ]
}];

@NgModule({

  imports: [
    QuillModule.forRoot(),
    SharedModule,
    CommonModule,
    CoreModule,
    RouterModule.forChild(routes),
    NgxPermissionsModule.forChild(),
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    TranslateModule,
    NgbDropdownModule,
    NgbModule,
    NgbModalModule,
    PartialsModule,
    NgSelectModule,
    ClipboardModule,
    SortablejsModule,
    DragDropModule,
    LeafletModule.forRoot(),
    LeafletDrawModule.forRoot(),
    NgxMaskModule.forRoot(),

  ],
 
  providers: [ModuleGuard],
  declarations: [
    TripComponent,
    PoiToolComponent,
    PoiManageComponent,
    ManageComponent,
    DeviceManageComponent,
    DeviceCommandComponent,
    UserAliasComponent,
    RoleAliasComponent,
    DeviceGroupComponent,
    DriverManageComponent ,
    UserManageComponent,
    SellDeviceComponent,
    // SendCommandComponent,
    ImportDeviceComponent,
    RenewsDeviceComponent,
    EditDeviceComponent,
    CreateDeviceGroupComponent,
    AddDriverComponent,
    AlertRulesComponent,
    AlertComponent,
    GeofenceManageComponent,
    GeofenceToolComponent,
    ActivityLogComponent,
    ReportScheduleComponent,
    EventLogComponent,
    MessageComponent,
    GeofenceGroupComponent,
    CameraComponent,
    MapApiKeyComponent,
    TaskRemindComponent
  ],
  entryComponents: [PoiToolComponent,],
  exports: [PoiToolComponent]
})
export class ManageModule { }
