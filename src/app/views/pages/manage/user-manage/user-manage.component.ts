import { Component, OnInit, ChangeDetectorRef,EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { UserManageService } from '@app/core/manage/_service/user-manage.service';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { EventEmitterService } from '@app/core/common/_service/eventEmitter.service';
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { DataTable } from '@app/core/_base/layout/models/datatable.model';
import { Subject } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { AppState } from '@app/core/reducers';
import { currentUser } from '@app/core/auth/_selectors/auth.selectors';
import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { User } from '@core/manage';
import { AuthService } from '@core/auth';
import { TranslateService } from '@ngx-translate/core';
import { ToastService, MenuAsideService } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth'

import * as $ from 'jquery';
import { isFulfilled } from 'q';
import { MenuConfig } from '@core/_config/default/menu.config';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
import { result } from 'lodash';
declare var $: any;

const TITLE_FORM_ADD: string = "COMMON.ACTIONS.ADD";
const TITLE_FORM_DELETE: string = "COMMON.ACTIONS.DELETE";

@Component({
  selector: 'kt-user-manage',
  templateUrl: './user-manage.component.html',
  styleUrls: ['./user-manage.component.scss']
})

export class UserManageComponent implements OnInit {
  saveFormUsers: FormGroup;
  searchFormUsers: FormGroup;
  public listUsers;
  public pageNo = 1;
  public pageSize = 10;
  public orderBy = "createdAt";
  public orderType = "DESC";
  public totalItems;
  public config: any;
  public nameUserTree = '#tree-search';
  public userEdit: any = [];
  public userIdSelected;
  public addNewUser: string = 'addNewUser'; // user tree add new parent
  public userTreeRoot: string = 'userTreeRoot'; // user tree sidebar
  public dataDefault: any = [{}]; 
  public notificationTabDefault: any = [{}];
  public dataSelected: any;
  public idParentUserEdit: any;
  public idUserEdit: any; // id user edit
  public idUserAction: any; // id user when reset or delete
  public usernameAction: string = '';
  public isParentUser: boolean = false;
  public isEdit: boolean = false; // 
  public isSearch: boolean = false;
  public idLogin;
  public columns: any = [];
  public paginationSelect: any = [];
  public dataTable: DataTable = new DataTable();
  public data: any = [];
  public totalRecod: number = 0;
  private unsubscribe: Subject<any>;
  public filter: any;
  public currentUser:User;
  public translationsArr = [];
  public closeResult: string;
  public listTimeZone :any = [];
  public languageList :any = [];
  public weekFirstDay :any = [];
  public decimalSeprerator:any= [];
  public unitVolume :any = [];
  public unitWeight :any = [];
  public unitTemperature :any = [];
  public unitDistance :any = [];
  public dateFormat :any = [];
  public timeFormat :any = [];
  public listRoles :any = [];
  public listUserType :any = [];
  public listPageMain:any = [];
  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  public showUserTree = true;

  public listNotification : any = [];
  public listChecked:any =[];
  public countChecked:number = 0;
  
  constructor(
    private manageUser: UserManageService,
    private userTreeService: UserTreeService,
    private eventEmitterService: EventEmitterService,
    private formBuilder: FormBuilder,
    private toast: ToastService,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private CurrentUserService : CurrentUserService,
    private auth: AuthService,
    private menuAsideService:MenuAsideService,

  ){
    this.idParentUserEdit = this.getIdUserTreeAddNew();
    this.parrentEmiter = new EventEmitter();
    // this.getInfoByIdUser(this.userIdSelected);
    this.unsubscribe = new Subject();
    this.setColumns();
    this.buildPagination(); // build config pagination
    this.setPaginationSelect();
    this.setDataTable();
    this.loadParamsetUser();


  }

  ngOnInit() {
    this.eventEmitterService.loadUpdateTree.subscribe((result: any) => {
      this.currentUser.name = '1';

      this.parrentEmiter.emit(result);
    });
    // this.buildDataDefault();//load data form 
    this.buildForm(); // build value form
    this.updateDataTable();
    this.currentUser = new User();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();
  }

  updateUserTree(item){
    let params = {
      id:item.id,
      path:item.path
    };
    this.ChangeUserTree(item);
    this.parrentEmiter.emit(params);
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;

        //this.filter = option;
        this.getData(this.userIdSelected);
      }),

    ).subscribe();
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  loadParamsetUser(){
    this.listTimeZone = this.CurrentUserService.listTimeZone;
    this.languageList = this.CurrentUserService.listLanguage;
    this.weekFirstDay = this.CurrentUserService.listWeekFirst;
    this.decimalSeprerator = this.CurrentUserService.listDecimal;
    this.unitVolume = this.CurrentUserService.listVolume;
    this.unitWeight = this.CurrentUserService.listWeight;
    this.unitDistance = this.CurrentUserService.listDistance;
    this.unitTemperature = this.CurrentUserService.listTemperature;
    this.timeFormat = this.CurrentUserService.listTimeFormat;
    this.dateFormat = this.CurrentUserService.listDateFormat;
    this.listUserType = this.CurrentUserService.listUserType;
    // this.listRoles = this.CurrentUserService.listRoles;
    this.listPageMain = this.menuAsideService.getPageMain();

     this.auth.searchRoles({}).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 200) {
        this.listRoles  = result.result.content;
        // this.listRoles = [
        //   {id: "2", name: "Distributor"},
        //   {id: "3", name: "User"}];
      }
    }) ;

  }

  getData(id) {
    if (id == undefined) id = this.idLogin;
    this.filter.parentId = id;
    this.filter.orderBy = this.orderBy;
    this.filter.orderType = this.orderType;
    this.dataTable.isLoading = true;
    this.cdr.detectChanges();
    this.manageUser.search(this.filter)
      .pipe(
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.dataTable.isLoading = false;
          this.cdr.markForCheck();
          this.cdr.detectChanges();
        }),
        debounceTime(300)
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
         

          $(function () {
            $('select.selectpicker').selectpicker();
          });
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
				selecter:false,
			  }
    });
  }

  searchUser(form: any) {
    if (form.value.username == null || form.value.username == undefined) {
      form.value.username = '';
    }
    if (form.value.email == null || form.value.email == undefined) {
      form.value.email = '';
    }
    if (form.value.phone == null || form.value.phone == undefined) {
      form.value.phone = '';
    }
    if (form.value.subAccount == '' || form.value.subAccount == null || form.value.subAccount == undefined) {
      form.value.subAcount = false;
    }
    this.filter.name = form.value.username;
    this.filter.email = form.value.email;
    this.filter.phone = form.value.phone;
    this.filter.parentId = this.userIdSelected;
    this.filter.includeSubAccount = form.value.subAccount;
    this.filter.orderBy = this.orderBy;
    this.filter.orderType = this.orderType;
    this.dataTable.isLoading = true;
    this.cdr.detectChanges();
    this.manageUser.search(this.filter)
      .pipe(
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.dataTable.isLoading = false;
          this.cdr.markForCheck();
          this.cdr.detectChanges();
        }),
        debounceTime(300)
      )
      .subscribe((data: any) => {
        console.log('zzz', data);
        if (data.status == 200) {
          this.data = data.result.content;
          this.totalRecod = data.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
        }
      });
  }

  resetFormSearch() {
    this.filter.pageNo = 1;
    this.searchFormUsers.reset();
  }

  buttonAddNew(content) {
    this.buildForm();
    this.saveFormUsers.reset();
    this.isEdit = false;
    this.open(content);
    setTimeout(() => {
      $('.kt_selectpicker').selectpicker('refresh');
    }, 200);
    // this.buildObjectUser();
  }

  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder',size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  getIdAction(id, idModal) {
    this.idUserAction = id;
    this.openModal({ idModal: idModal, confirm: true });
  }

  resetPassword() {
    let params = {
      id:this.idUserAction,
      password: '123456'
    };
    this.manageUser.resetPassword(params, { notifyGlobal: true }).subscribe((result: any) => {
      $('.modal').modal('hide');
    });

  }

  getInforIdSelected(id) {
    this.idUserEdit = id;
    this.manageUser.get(id)
      .pipe(
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.dataSelected = [result.result];
        }
      });
  }

  editUser(id, content) {
    this.isEdit = true;
    this.open(content);
   
      this.idUserEdit = id;
      this.manageUser.get(id)
        .pipe(
          takeUntil(this.unsubscribe),
          finalize(() => {
            this.cdr.markForCheck();
          })
        )
        .subscribe((result: any) => {
          if (result.status == 200) {
            let data = result.result;
            this.dataDefault = [data];
            let pageMain = data['pageMain'] || this.menuAsideService.getPageMainDefault().page;
            
            this.saveFormUsers.get('pageMain').setValue(pageMain);
            setTimeout(function () {
              $('.kt_selectpicker').selectpicker();
              $('#pageMain').val(pageMain).selectpicker('refresh');
            });
          }
        }); 
        // this.openTabNotification();
      
        
  }

  loadSelectBootstrap() {
    $('.bootstrap-select').selectpicker();
  }

  deleteUser() {
    let id = this.idUserAction;
    this.manageUser.delete(id,{ notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.getData(this.userIdSelected);
        let userRemove = data.result;
        let params = {
          id: userRemove.parentId,
          path : userRemove.path
        }
        this.parrentEmiter.emit(params);
      } 
    });
    $('.modal').modal('hide');
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10
    };
  }

  resetPagination(totalElements) {
    this.config.totalItems = totalElements;
    this.config.currentPage = this.pageNo;
  }

  private buildForm(): void {
    let user = new User();
   
    this.saveFormUsers = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(6)]],
      name: ['', Validators.required],
      phone: [''],
      email: ['', [Validators.email]],
      role: [''],
      timezone: [''],
      language: [''],
      type: ['', Validators.required],
      description: [''],
      address: [''],
      distance: [''],
      volume: [''],
      weight: [''],
      date: [''],
      time: [''],
      temperature: [''],
      fisrtday: [''],
      seprerator: [''],
      pageMain:[this.menuAsideService.getPageMainDefault().page],

      sosType: [''],
      batteryType: [''],
      powerType: [''],
      geoInType: [''],
      geoOutType: [''],
      engineOnType: [''],
      engineOffType: [''],
      vibrationType: [''],

    });
    setTimeout(()=>{
      $("#pageMain").selectpicker();
      $("#pageMain").val(this.menuAsideService.getPageMainDefault().page).selectpicker('refresh');
    });
    this.searchFormUsers = this.formBuilder.group({
      username: [''],
      email: [''],
      phone: [''],
      subAccount: [false]
    })
  }

  get f() {
    if (this.saveFormUsers) return this.saveFormUsers.controls;
  }

  getIdUserTreeAddNew() {
    var parentId = this.idLogin;
    return parentId;
  }

  openTabNotification() {
    this.isEdit = true;

    this.manageUser.listNotifications(this.idUserEdit)
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if(result.result.notification) {
     
      }

      if (result.status == 200) {
       
        let data = result.result.notification;
        
        console.log(data);
      
        this.saveFormUsers.controls['sosType'].setValue(+data[0].avtive);
  
      }
    });
  }

  onSubmit(form: any) {
    if (this.saveFormUsers.invalid) {
      return;
    }
    let params = {
      "parentId": this.idParentUserEdit,
      "aliasId": 0,
      "path": "",
      "username": form.value.username,
      "type": form.value.type,
      "name": form.value.name,
      "surname": "",
      "phone": form.value.phone,
      "description": form.value.description,
      "status": 1,
      "active": 1,
      "createdAt": "",
      "stockDevice": 0,
      "totalDevice": 0,
      "totalPoint": 0,
      "isRenewed": 1,
      "timezone": form.value.timezone,
      "language": form.value.language,
      "unitDistance": form.value.distance,
      "unitVolume": form.value.volume,
      "unitTemperature": form.value.temperature,
      "unitWeight": form.value.weight,
      "dateFormat": form.value.date,
      "timeFormat": form.value.time,
      "weekFirstDay": form.value.fisrtday,
      "markerStyle": "html",
      "carSorting": "plate",
      "decimalSeprerator": form.value.seprerator,
      "roleId": form.value.role,
      "pageMain":form.value.pageMain,
    
    } as User;

    let paramNotification = {
      userId: this.idUserEdit,
      notification: [
        {
          "type": "SOS",
          "active":  form.value.sosType ? 1 : 0
        },
        {
          "type": "LOW_BATTERY",
          "active":  form.value.batteryType ? 1 : 0
        },
        {
          "type": "EXTERNAL_POWER_CUT",
          "active":  form.value.powerType ? 1 : 0
        },
        {
          "type": "GEOFENCE_IN",
          "active":  form.value.geoInType ? 1 : 0
        },
        {
          "type": "GEOFENCE_OUT",
          "active":  form.value.geoOutType ? 1 : 0
        },
        {
          "type": "ENGINE_ON",
          "active":  form.value.engineOnType ? 1 : 0
        },
        {
          "type": "ENGINE_OFF",
          "active":  form.value.engineOffType ? 1 : 0
        },
        {
          "type": "VIBRATION",
          "active":  form.value.vibrationType ? 1 : 0
        },
        
      ]
    };

    if(form.value.email != '')
    {
      params.email = form.value.email;
    }
    if (this.isEdit) {
      params.id = this.idUserEdit;
      this.manageUser.put(params,{ notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          let params = {
            id: this.idParentUserEdit,
            path : result.result.path
          }
          this.getData(this.idParentUserEdit);
          this.parrentEmiter.emit(params);
          this.modalService.dismissAll();
        }
      });

      this.manageUser.putNotification(paramNotification).subscribe((result: any) => {
        if(result.status == 200) {
          this.modalService.dismissAll();
        }
      }); 
     
      return;
    };

    this.manageUser.post(params,{ notifyGlobal: true }).subscribe((result: any) => {
      if (result.status == 201) {
        let params = {
          id: this.idParentUserEdit,
          path : result.result.path
        }
        this.getData(this.idParentUserEdit);
        this.parrentEmiter.emit(params);
        this.modalService.dismissAll();
      }
    })
   
  }

  convertChecked(listChecked)
  {
    this.listNotification.map(type => {
      let active = false;
      let id = type.id;

      if(listChecked.includes(id)) active = true;
      type.active = active;
      return type;
    });

  }

  changeCheckedTypeNotification(event) {
    this.listChecked = event.listChecked;
    this.countChecked = event.countChecked;
  }

  resetLoadEvent(localStorageUser, parentId) {
    this.eventEmitterService.resetLocalStorageName();
    this.loadUserTree(localStorageUser);
    this.getData(parentId);
  }

  loadUserTree(localStorageUser) {
    localStorage.setItem(this.userTreeRoot, JSON.stringify(localStorageUser));
  }

  onHideModal(id: string) {
    let idModal = '#' + id;
    $(idModal).modal('hide');
  }

  openModal(params) {
    let idModal = params.idModal;
    let confirm = params.confirm;
    if (confirm) {
      $(idModal).appendTo('body').modal({
        backdrop: 'static',
        keyboard: false
      })
    } else {
      $(idModal).appendTo('body').modal('show');
    }
  }

  getInfoByIdUser(id) {
    this.manageUser.get(id).subscribe((result: any) => {
      if (result.status == 200) {
        let data = result.result;
        this.dataDefault = [data];
      }
    });
  }
  
  search() {
    let listSearch = [];

    if (this.userIdSelected != undefined) {
      listSearch.push({ key: 'userId', value: this.userIdSelected });
    }
    this.dataTable.search(listSearch);
  }

  ChangeUserTree(value) {
    if (value.id > 0) {
      this.userIdSelected = value.id;
      this.currentUser = value;
      this.search();
    }
  }

  getParrentUser(value)
  {
    if (value.id > 0) {
      if(this.idParentUserEdit != value.id) 
      {
        this.idParentUserEdit = value.id;
        this.buildDataDefault();

      }
    }
    if(this.idParentUserEdit == undefined) this.idParentUserEdit = this.userIdSelected;
  }

  buildDataDefault() {
      // this.getInfoByIdUser(id);
      this.manageUser.get(this.idParentUserEdit)
        .pipe(
          finalize(() => {
            this.cdr.markForCheck();
          })
        )
        .subscribe((result: any) => {
          if (result.status == 200) {
            let data = result.result;
            console.log(`data ${data}`);
            this.dataDefault[0].timezone = data.timezone;
            this.dataDefault[0].language = data.language;
            this.dataDefault[0].unitDistance = data.unitDistance;
            this.dataDefault[0].unitVolume = data.unitVolume;
            this.dataDefault[0].unitTemperature = data.unitTemperature;
            this.dataDefault[0].unitWeight = data.unitWeight;
            this.dataDefault[0].dateFormat = data.dateFormat;
            this.dataDefault[0].timeFormat = data.timeFormat;
            this.dataDefault[0].weekFirstDay = data.weekFirstDay;
            this.dataDefault[0].decimalSeprerator = data.decimalSeprerator;
            setTimeout(() => {
              $('.kt_selectpicker').selectpicker('refresh');
            }, 200);
          }
        });
  }

    /**
   * Change size databale
   * @param $elm 
   */
  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();

  }

  copied(val) {
    this.toast.copied(val);
  }

  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        width: 20,
        class: '',
        translate: '#',
        autoHide: false,
      },
      {
        title: 'Username',
        field: 'username',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px', 'margin': '0px' },
        width: 110,
        class: '',
        translate: 'COMMON.COLUMN.USERNAME',
        autoHide: false,
      },
      {
        title: 'Roles',
        field: 'Roles',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '140px' },
        width: 110,
        class: '',
        translate: 'MANAGE.USER.GENERAL.ROLE_NAME',
        autoHide: true,
      },
      {
        title: 'Full name',
        field: 'groupName',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '130px' },
        width: 110,
        class: '',
        translate: 'MANAGE.USER.GENERAL.FULL_NAME',
        autoHide: true,
      },
      {
        title: 'Phone',
        field: 'groupName',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        width: 110,
        class: '',
        translate: 'MANAGE.USER.GENERAL.PHONE',
        autoHide: false,
      },
      {
        title: 'Email',
        field: 'groupName',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        width: 110,
        class: '',
        translate: 'MANAGE.USER.GENERAL.EMAIL',
        autoHide: true,
      },
      {
        title: 'Status',
        field: 'groupName',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '104px', 'text-align':'center' },
        width: 104,
        class: '',
        translate: 'COMMON.COLUMN.STATUS',
        autoHide: false,
      },
      {
        title: 'Create At',
        field: 'createdAt',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '78px' },
        width: 80,
        class: '',
        translate: 'COMMON.COLUMN.CREATED_DATE',
        autoHide: true,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' }, 
        width: 117,
        class: '',
        translate: 'COMMON.COLUMN.ACTIONS',
        autoHide: false,
      },
    ]
  }


}



