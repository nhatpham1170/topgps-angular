import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { finalize, takeUntil, tap ,debounceTime,delay} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { DeviceCommand,DeviceCommandService,ActivityLogService } from '@core/manage';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { DeviceService} from '@core/manage';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { select, Store } from '@ngrx/store';
import { AppState } from '@core/reducers';
import { currentUser } from '@core/auth';
import { TranslateService } from '@ngx-translate/core';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';

declare var $: any;
import * as moment from 'moment';


@Component({
  selector: 'kt-activity-log',
  templateUrl: './activity-log.component.html'
})
export class ActivityLogComponent implements OnInit {
  formCommand: FormGroup;
  searchFormActivityLog : FormGroup;
  formCheckList:FormGroup;
  selectdp: ElementRef;
  @ViewChild('datePicker', { static: true }) 
  datePicker: DateRangePickerComponent;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idCommandEdit: number;
  public dataDefault: any = [{}];
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public userIdSelected:number;
  public userIdSelectedEdit:number;
  public showUserTree:boolean = true;
  public textDetail:string = '';
  public showLoaddingSearch:boolean = false;
//   public listActions = [
//     'delete','update','extend','move','extension','change_status','sell','sort_order','update_profile','update_path_child','update_path_alias',
//     'update_pass_alias','update_pass','update_alias','create','add_point','sell_point','buy_point','login','resetpass','create_alias','delete_alias',

// ];
public listActions = [
   {id:'delete',translate:'MANAGE.ACTIVITY_LOG.ACTIONS.DELETE'},
   {id:'create',translate:'MANAGE.ACTIVITY_LOG.ACTIONS.CREATE'},

   {id:'create_alias',translate:'MANAGE.ACTIVITY_LOG.ACTIONS.CREATE_ALIAS'},
   {id:'update_alias',translate:'MANAGE.ACTIVITY_LOG.ACTIONS.UPDATE_ALIAS'},
   {id:'delete_alias',translate:'MANAGE.ACTIVITY_LOG.ACTIONS.DELETE_ALIAS'},

   {id:'update',translate:'MANAGE.ACTIVITY_LOG.ACTIONS.UPDATE'},
   {id:'extend',translate:'MANAGE.ACTIVITY_LOG.ACTIONS.EXTEND'},
   {id:'move',translate:'MANAGE.ACTIVITY_LOG.ACTIONS.MOVE'},
   {id:'extension',translate:'MANAGE.ACTIVITY_LOG.ACTIONS.EXTENSION'},
   {id:'change_status',translate:'MANAGE.ACTIVITY_LOG.ACTIONS.CHANGE_STATUS'},
   {id:'sell',translate:'MANAGE.ACTIVITY_LOG.ACTIONS.SELL'},
   {id:'sort_order',translate:'MANAGE.ACTIVITY_LOG.ACTIONS.SORT_ORDER'},
   {id:'update_profile',translate:'MANAGE.ACTIVITY_LOG.ACTIONS.UPDATE_PROFILE'},
   {id:'update_path_child',translate:'MANAGE.ACTIVITY_LOG.ACTIONS.UPDATE_PATH_CHILD'},
   {id:'login',translate:'MANAGE.ACTIVITY_LOG.ACTIONS.LOGIN'},

];

public listObject = [
  {id:'user_setting',translate:'MANAGE.ACTIVITY_LOG.OBJECT.USER_SETTING',permission: 'ROLE_manage.role_alias',columnData:"username",textData:"COMMON.COLUMN.USERNAME"},
  {id:'role_alias',translate:'MANAGE.ACTIVITY_LOG.OBJECT.ROLE_ALIAS',permission: 'ROLE_manage.role_alias',columnData:"name",textData:"COMMON.COLUMN.USERNAME"},
  {id:'devices',translate:'MANAGE.ACTIVITY_LOG.OBJECT.DEVICES',permission: 'ROLE_manage.device',columnData:"modifiedBy",textData:"Tài khoản"},
  {id:'device_icon',translate:'MANAGE.ACTIVITY_LOG.OBJECT.DEVICE_ICON',permission: 'ROLE_admin.icon',columnData:"name",textData:"Biểu tượng"},
  {id:'device_group',translate:'MANAGE.ACTIVITY_LOG.OBJECT.DEVICE_GROUP',permission: 'ROLE_manage.device_group'},
  {id:'permission',translate:'MANAGE.ACTIVITY_LOG.OBJECT.PERMISSION',permission: 'ROLE_admin.permission'},
  {id:'card',translate:'MANAGE.ACTIVITY_LOG.OBJECT.CARD',permission: 'ROLE_card.manage'},
  {id:'user_session',translate:'MANAGE.ACTIVITY_LOG.OBJECT.USER_SESSION',permission: ''},
  {id:'loginpage',translate:'MANAGE.ACTIVITY_LOG.OBJECT.LOGIN_PAGE',permission: 'ROLE_admin.login_page',columnData:"host",textData:"Trang"},
  {id:'user',translate:'MANAGE.ACTIVITY_LOG.OBJECT.USER',permission: 'ROLE_manage.user',columnData:"username",textData:"Tài khoản"},
  {id:'sensor_template',translate:'MANAGE.ACTIVITY_LOG.OBJECT.SENSOR_TEMPLATE',permission: 'ROLE_admin.sensor_template',columnData:"name",textData:"Mẫu cảm biến"},
  {id:'device_types',translate:'MANAGE.ACTIVITY_LOG.OBJECT.DEVICE_TYPES',permission: 'ROLE_admin.device_type',columnData:"name",textData:"Loại thiết bị"},
  {id:'roles',translate:'MANAGE.ACTIVITY_LOG.OBJECT.ROLES',permission: 'ROLE_admin.role',columnData:"name",textData:"Vai trò"},
  {id:'sim_type',translate:'MANAGE.ACTIVITY_LOG.OBJECT.SIM_TYPE',permission: 'ROLE_admin.sim_type',columnData:"nameKey",textData:"Loại"},
  {id:'transport_type',translate:'MANAGE.ACTIVITY_LOG.OBJECT.TRANSPORT_TYPE',permission: 'ROLE_admin.transport_type',columnData:"name",textData:"Loại vận tải"},
  {id:'poi_type',translate:'MANAGE.ACTIVITY_LOG.OBJECT.POI_TYPE',permission: 'ROLE_admin.poi_type',columnData:"name",textData:"MANAGE.ACTIVITY_LOG.OBJECT.TRANSPORT_TYPE"},
  {id:'poi',translate:'MANAGE.ACTIVITY_LOG.OBJECT.POI',permission: 'ROLE_manage.poi',columnData:"name",textData:"MANAGE.ACTIVITY_LOG.OBJECT.POI"},
  {id:'toll_station',translate:'MANAGE.ACTIVITY_LOG.OBJECT.TOLL_STATION',permission: 'ROLE_admin.toll_station',columnData:"name",textData:"MANAGE.ACTIVITY_LOG.OBJECT.TOLL_STATION"},
  {id:'camera',translate:'MANAGE.ACTIVITY_LOG.OBJECT.CAMERA',permission: 'ROLE_device.action.camera',columnData:"name",textData:"MANAGE.ACTIVITY_LOG.OBJECT.CAMERA"},
  {id:'sensor',translate:'MANAGE.ACTIVITY_LOG.OBJECT.SENSOR',permission: '',columnData:"name",textData:"MANAGE.ACTIVITY_LOG.OBJECT.SENSOR"},
  {id: 'geofence', translate: 'MANAGE.ACTIVITY_LOG.OBJECT.GEOFENCE', permission: 'ROLE_manage.geofence', columnData: 'name', textData: 'MANAGE.ACTIVITY_LOG.OBJECT.GEOFENCE'},
  {id: 'map_key', translate: 'MANAGE.ACTIVITY_LOG.OBJECT.MAP_KEY', permission: 'ROLE_manage.api_key', columnData: 'userName', textData: 'MANAGE.ACTIVITY_LOG.OBJECT.MAP_KEY'},
];

  public datePickerOptions: any = {
    size: 'md',
    select: 'from',
    ranges: 'from',
    optionDatePicker: {
      maxSpan: {
        days: 31
      }
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: true,
    startDate:moment().subtract(6, 'days'),
    endDate:moment() 
  };
  public dateStart: string;
  public dateEnd: string;
  constructor(
    private Command: DeviceCommandService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private store: Store<AppState>,
    private activityLog : ActivityLogService,
    private translate: TranslateService,
    private userTreeService:UserTreeService

  ) {
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    setTimeout(function () {
      $('.kt_selectpicker').selectpicker('refresh');
    }, 100);
    this.updateDataTable();
    this.refreshEventType();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();
   }

 
   private buildForm(): void {
    this.searchFormActivityLog = this.formBuilder.group({
        objectType:[''],
        actionType:[''],
        subAccount:['']
    });
  }

  private refreshEventType()
  {
   this.translate.onLangChange.subscribe((event) => {
     let titleObject = this.translate.instant('MANAGE.ACTIVITY_LOG.GENERAL.OBJECT_TYPE');
     let titleAction = this.translate.instant('MANAGE.ACTIVITY_LOG.GENERAL.ACTION_TYPE');
     $(function () {
       $('.input-object').selectpicker({title: titleObject}).selectpicker('refresh');
       $('.input-action').selectpicker({title: titleAction}).selectpicker('refresh');
     });
   });
  }

  convertAction(id)
  {
    // console.log(this.listActions.find(o => o.id === id));
   let obj = this.listActions.find(o => o.id === id);
   if(obj == undefined || obj == null) return id;
   return this.listActions.find(o => o.id === id).translate;
  }

  convertObject(id)
  {
    // console.log(this.listActions.find(o => o.id === id));
   let obj = this.listObject.find(o => o.id === id);
   if(obj == undefined || obj == null) return id;
   return this.listObject.find(o => o.id === id).translate;
  }

  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
   }

   showDetailLog(content,item)
   {
     this.dataDefault = item;
     this.open(content);
   }

  
  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-900'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   this.showUserTreeFunction();
  }

  showUserTreeFunction(){
    let userModel = {
      type: 0,
    };
    let user$:any = this.store.pipe(select(currentUser));
    if (user$) {
      select(user$.subscribe(function (data) {
        Object.assign(userModel, data);
      }
      ).unsubscribe());
    }
      // code display use-tree
      let type = userModel.type;
      
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  
  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,

      timeFrom: moment().subtract(6, 'days').format("YYYY-MM-DD HH:mm:ss"),
      timeTo : moment().format("YYYY-MM-DD HH:mm:ss")
    };
  }

  changeUser(value){
    if (value.id > 0) {
      this.userIdSelectedEdit = value.id;
    }
  }

  ChangeUserTree(value) {    
    if (value.id > 0) {
      this.userIdSelected = value.id;
    }
    this.search();
    }
 
  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }

  resetFormSearch() {
    this.filter.pageNo = 1;
    this.searchFormActivityLog.reset();
    this.datePicker.refresh();
    setTimeout(function () {
      $('.kt_selectpicker').selectpicker('refresh');
    }, 100);
  }

  searchActivityLog(form:any)
  {
    
    if (form.value.objectType == null || form.value.objectType == undefined) {
      form.value.objectType = '';
    }

    if (form.value.actionType == null || form.value.actionType == undefined) {
      form.value.actionType = '';
    }

    if (form.value.subAccount == '' || form.value.subAccount == null || form.value.subAccount == undefined) {
      form.value.subAcount = false;
    }
    this.filter.actionType = form.value.actionType.toString();
		this.filter.objType = form.value.objectType.toString();
    this.filter.subAccount = form.value.subAccount;
    this.filter.timeFrom   = this.dateStart;
    this.filter.timeTo     = this.dateEnd;
    this.filter.pageNo     = 1;

    this.getData(this.userIdSelected);
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;

        this.getData(this.userIdSelected);
      }),
      
    ).subscribe();
  }

  //checked

  //end checked

  get f() {
   if(this.formCommand != undefined) return this.formCommand.controls;
  }

  search() {
    let listSearch = [];

    if (this.userIdSelected != undefined) {
      listSearch.push({ key: 'userId', value: this.userIdSelected });
    }
    this.dataTable.search(listSearch);
  }

  getData(userId) {
    if(userId != undefined) this.filter.userId = userId;
    this.showLoaddingSearch = true;
    this.activityLog.search(this.filter)
      .pipe(
        delay(300),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          result.result.content.map(content=>{
            let platform = '';
            if(content.loginWith.cornerstone) platform = platform+content.loginWith.cornerstone;
            if(content.loginWith.type) platform = platform+' - '+content.loginWith.type;
            if(content.loginWith.browser) platform = platform+' - '+content.loginWith.browser;
            content.platform = platform;
            let objType = content.objType;
            if(objType == "user_session")
            {
              content.event_desc = content.objNewOut.username+"- login sucssec";
            }

            return content;
          });
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          $(function () {
            $('.kt_selectpicker').selectpicker();
          });
          this.showLoaddingSearch = false;
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect, 
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
				selecter:false,
			  }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  
  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'username',
        field: 'imei',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'COMMON.COLUMN.USERNAME',
        autoHide: false,
        width: 100,
      },
      // {
      //   title: 'name object',
      //   field: 'Type',
      //   allowSort: false,
      //   isSort: false,
      //   dataSort: '',
      //   style: { 'width': '110px' },
      //   class: '',
      //   translate:'MANAGE.ACTIVITY_LOG.GENERAL.OBJECT_NAME',
      //   autoHide: true,
      //   width: 100,
      // },
      {
        title: 'Type',
        field: 'Type',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '130px' },
        class: '',
        translate:'MANAGE.ACTIVITY_LOG.GENERAL.OBJECT_TYPE',
        autoHide: true,
        width: 100,
      },
      {
        title: 'action ',
        field: 'actions',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'COMMON.COLUMN.WORK',
        autoHide: true,
        width: 100,
      },
      {
        title: 'Login',
        field: 'send',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '200px' },
        class: '',
        translate: 'MANAGE.ACTIVITY_LOG.GENERAL.BASIC',
        autoHide: true,
        width: 100,

      },

      {
        title: 'date',
        field: 'updated',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'COMMON.COLUMN.CREATED_DATE',
        autoHide: true,
        width: 100,
      },
      {
        title: 'event desc',
        field: 'event',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '200px'},
        class: '',
        translate: 'COMMON.COLUMN.DESCRIPTION',
        autoHide: true,
        width: 100,
      },
      {
        title: 'action ',
        field: 'actions',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: true,
        width: 117,
      },

    ]
  }
}




