import {
	Component,
	OnInit,
	Output,
	EventEmitter,
	Input,
	ElementRef,
	ViewChild,
	ChangeDetectorRef,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

declare var $: any;
import * as moment from 'moment';
import { arraysAreNotAllowedMsg } from '@ngrx/store/src/models';

@Component({
	selector: 'kt-event-log',
	templateUrl: './event-log.component.html',
	styleUrls: ['./event-log.component.scss'],
})
export class EventLogComponent implements OnInit {
	@Input() object1: any;
	@Input() object2: any;
	@Input() objType: any;
	@Input() idObject: any;
	@Input() actionType: string = 'update';
	@Input() detail: boolean = false;

	public textEvent: string;
	public listObject = [
		{
			id: 'user_setting',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.USER_SETTING',
			permission: 'ROLE_manage.role_alias',
			columnData: 'username',
			textData: 'COMMON.COLUMN.USERNAME',
		},
		{
			id: 'role_alias',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.ROLE_ALIAS',
			permission: 'ROLE_manage.role_alias',
			columnData: 'name',
			textData: 'COMMON.COLUMN.USERNAME',
		},
		{
			id: 'devices',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.DEVICES',
			permission: 'ROLE_manage.device',
			columnData: 'modifiedBy',
			textData: 'COMMON.COLUMN.USERNAME',
		},
		{
			id: 'device_icon',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.DEVICE_ICON',
			permission: 'ROLE_admin.icon',
			columnData: 'name',
			textData: 'ADMIN.DEVICE_ICON.GENERAL.ICONTYPE',
		},
		{
			id: 'device_group',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.DEVICE_GROUP',
			permission: 'ROLE_manage.device_group',
			columnData: 'name',
			textData: 'MANAGE.ACTIVITY_LOG.OBJECT.DEVICE_GROUP',
		},
		{
			id: 'permission',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.PERMISSION',
			permission: 'ROLE_admin.permission',
			columnData: 'name',
			textData: 'MANAGE.ACTIVITY_LOG.OBJECT.PERMISSION',
		},
		{
			id: 'card',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.CARD',
			permission: 'ROLE_card.manage',
		},
		{
			id: 'user_session',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.USER_SESSION',
			permission: '',
		},
		{
			id: 'loginpage',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.LOGIN_PAGE',
			permission: 'ROLE_admin.login_page',
			columnData: 'host',
			textData: 'MANAGE.ACTIVITY_LOG.EVENT.PAGE',
		},
		{
			id: 'user',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.USER',
			permission: 'ROLE_manage.user',
			columnData: 'username',
			textData: 'COMMON.COLUMN.USERNAME',
		},
		{
			id: 'sensor_template',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.SENSOR_TEMPLATE',
			permission: 'ROLE_admin.sensor_template',
			columnData: 'name',
			textData: 'MENU.SENSOR_MODEL',
		},
		{
			id: 'device_types',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.DEVICE_TYPES',
			permission: 'ROLE_admin.device_type',
			columnData: 'name',
			textData: 'MENU.DEVICE_TYPE',
		},
		{
			id: 'roles',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.ROLES',
			permission: 'ROLE_admin.role',
			columnData: 'name',
			textData: 'MANAGE.ACTIVITY_LOG.OBJECT.ROLES',
		},
		{
			id: 'sim_type',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.SIM_TYPE',
			permission: 'ROLE_admin.sim_type',
			columnData: 'nameKey',
			textData: 'MANAGE.ACTIVITY_LOG.OBJECT.SIM_TYPE',
		},
		{
			id: 'transport_type',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.TRANSPORT_TYPE',
			permission: 'ROLE_admin.transport_type',
			columnData: 'name',
			textData: 'MANAGE.ACTIVITY_LOG.OBJECT.TRANSPORT_TYPE',
		},
		{
			id: 'poi',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.POI',
			permission: 'ROLE_admin.poi',
			columnData: 'name',
			textData: 'MANAGE.ACTIVITY_LOG.OBJECT.POI',
		},
		{
			id: 'poi_type',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.POI_TYPE',
			permission: 'ROLE_admin.poi_type',
			columnData: 'name',
			textData: 'MANAGE.ACTIVITY_LOG.OBJECT.POI_TYPE',
		},
		{
			id: 'toll_station',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.TOLL_STATION',
			permission: 'ROLE_admin.toll_station',
			columnData: 'name',
			textData: 'MANAGE.ACTIVITY_LOG.OBJECT.TOLL_STATION',
		},
		{
			id: 'camera',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.CAMERA',
			permission: 'ROLE_admin.camera',
			columnData: 'name',
			textData: 'MANAGE.ACTIVITY_LOG.OBJECT.POI',
		},
		{
			id: 'sensor',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.SENSOR',
			permission: 'ROLE_device.action.sensor',
			columnData: 'name',
			textData: 'MANAGE.ACTIVITY_LOG.OBJECT.SENSOR',
		},
		{
			id: 'geofence',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.GEOFENCE',
			permission: 'ROLE_manage.geofence',
			columnData: 'name',
			textData: 'MANAGE.ACTIVITY_LOG.OBJECT.GEOFENCE',
		},
		{
			id: 'map_key',
			translate: 'MANAGE.ACTIVITY_LOG.OBJECT.MAP_KEY',
			permission: '',
			columnData: 'userName',
			textData: 'MANAGE.ACTIVITY_LOG.OBJECT.MAP_KEY',
		},
	];
	private listRemoveKeys = [
		'updatedAt',
		'typeDevice',
		'createdAt',
		'neLat',
		'neLng',
		'swLat',
		'swLng',
		'encodedPoints',
	];
	// private listRemoveKeys = ['updatedAt','typeDevice'];

	private loginText;
	constructor(private translate: TranslateService) {}

	ngOnInit() {
		this.render();
		// change language
		this.translate.onLangChange.subscribe((event) => {
			this.render();
		});
	}

	convertObject(id) {
		let obj = this.listObject.find((o) => o.id === id);
		return obj;
	}

	renderTextRole(arrOld, arrNew) {
		let text = '';
		if (arrOld.length > 0) {
			let textRemove =
				'- ' +
				this.translate.instant(
					'MANAGE.ACTIVITY_LOG.EVENT.REMOVE_PERMISSION'
				);
			let textValue = '';
			arrOld.forEach((element) => {
				textValue = textValue + element + ', ';
			});
			text = text + '<p>' + textRemove + '<b> ' + textValue + '</b></p>';
		}
		if (arrNew.length > 0) {
			let textAdd =
				'- ' +
				this.translate.instant(
					'MANAGE.ACTIVITY_LOG.EVENT.ADD_PERMISSION'
				);
			let textValue = '';
			arrNew.forEach((element) => {
				textValue = textValue + element + ', ';
			});
			text = text + '<p>' + textAdd + '<b> ' + textValue + '</b></p>';
		}
		return text;
	}

	hasTranslation(key: string): boolean {
		let textTranslation = 'COMMON.COLUMN.' + key.toUpperCase();
		return this.translate.instant(textTranslation) !== textTranslation;
	}

	public render() {
		let textData = '';
		let columnValue = '';
		let columnData = '';
		let objectType = this.convertObject(this.objType);
		if (objectType) columnData = objectType.columnData;

		if (objectType && objectType.textData) {
			textData = this.translate.instant(objectType.textData);
		}
		if (this.object2) {
			columnValue = this.object2[columnData] || '';
		} else {
			columnValue = this.object1[columnData] || '';
		}
		let login = '';
		//update

		switch (this.actionType) {
			case 'update_profile':
			case 'update':
			case 'update_alias':
				let change = this.compareObject(this.object1, this.object2);

				var text = '';
				// change: key: {new: [{}] | "" | int, old: [{}] | "" |} cac gtri thay doi
				if (Object.keys(change).length > 0) {
					Object.keys(change).forEach((x) => {
						//check exist key translate
						let fieldEvent = x;
						if (this.hasTranslation(x)) {
							fieldEvent = this.translate.instant(
								'COMMON.COLUMN.' + x.toUpperCase()
							);
						}
						// text detail

						if (this.detail) {

							//check value is image
							let valueOld = this.checkImage(change[x].old);
							let valuenew = this.checkImage(change[x].new);

							text =
								'' +
								text +
								'<p>- <b>' +
								fieldEvent +
								'</b> ' +
								this.translate.instant(
									'MANAGE.ACTIVITY_LOG.EVENT.FROM'
								) +
								' <b>' +
								valueOld +
								'</b> ' +
								this.translate.instant(
									'MANAGE.ACTIVITY_LOG.EVENT.TO'
								) +
								' <b>' +
								valuenew +
								'</b></p> ';

							//roles
							if (this.objType == 'roles') {
								text = this.renderTextRole(
									valueOld,
									change[x].new
								);
							}

							//user_setting
							if (this.objType == 'user_setting')
								text = this.covnertDetailChange(
									change[x].old,
									change[x].new,
									{ key: 'key' }
								);

							//role_alias
							if (
								this.objType == 'role_alias' &&
								x == 'permissions'
							)
								text = this.covnertDetailChange(
									change[x].old,
									change[x].new,
									{ key: 'name' }
								);

							//toll_station
							if (this.objType == 'toll_station' && x == 'info') {
								text = this.covnertDetailChange(
									change[x].old,
									change[x].new,
									{ key: 'transportType' }
								);
							}
						} else {
							text = text + '<b>' + fieldEvent + '</b>, ';
						}
					});
				}

				this.textEvent =
					textData +
					' <b>' +
					columnValue +
					'</b> ' +
					this.translate
						.instant('MANAGE.ACTIVITY_LOG.EVENT.CHANGE')
						.toLowerCase() +
					' ' +
					text;

				// code detail
				break;
			case 'create':
			case 'create_alias':
				this.textEvent =
					this.translate.instant('MANAGE.ACTIVITY_LOG.EVENT.CREATE') +
					' ' +
					textData +
					' <b> ' +
					columnValue +
					'</b> ';
				break;
			case 'delete':
			case 'delete_alias':
				this.textEvent =
					this.translate.instant('MANAGE.ACTIVITY_LOG.EVENT.DELETE') +
					' ' +
					textData +
					' <b> ' +
					columnValue +
					'</b> ';
				break;

			default:
		}
		//  // user setting
		// if(this.objType == 'user_setting')
		// {

		//   this.textEvent = text;
		// }
		// user session

		if (this.objType == 'user_session') {
			let username = this.object2.username;
			this.textEvent =
				'<b>' +
				username +
				'</b> ' +
				this.translate.instant('MANAGE.ACTIVITY_LOG.EVENT.LOGIN');
		}
	}

	public checkImage(url) {
		let img = url;
		if (this.checkURL(url)) img = "<img width='40px' src='" + url + "' />";
		return img;
	}

	public checkURL(url) {
		if (typeof url != 'string') return false;
		return url.includes('photos.vnetgps.com');
	}

	// renderArrayData(oldArr, newArr) {
	// 	let text = '';
	// 	// let
	// }

	covnertDetailChange(oldObj, newObj, option?) {
		let text = '';
		let detailChange = this.compareRoleAlias(oldObj, newObj, option);
		if (detailChange.add) {
			detailChange.add.forEach((element) => {
				text =
					text +
					'<p>- ' +
					this.translate.instant('COMMON.ACTIONS.ADD') +
					'<b> ' +
					this.convertLanguageByKey(element) +
					'</b></p> ';
			});
		}
		if (detailChange.remove) {
			detailChange.remove.forEach((element) => {
				text =
					text +
					'<p>- ' +
					this.translate.instant('MANAGE.ACTIVITY_LOG.EVENT.DELETE') +
					' <b>' +
					this.convertLanguageByKey(element) +
					'</b></p> ';
			});
		}

		if (this.objType == 'user_setting' && detailChange.sortOrder)
			text = text + '<p> -' + detailChange.sortOrder;

		return text;
	}

	convertLanguageByKey(key) {
		let prefix = '';
		if (this.objType == 'role_alias') prefix = 'PERMISSIONS.';
		if (this.objType == 'toll_station') prefix = 'TOLL_STATION.COLUMN.';
		let AllKey = prefix + key;
		AllKey = AllKey.toUpperCase();
		return this.translate.instant(AllKey);
	}

	public compareRoleAlias(objOld, objNew, option?) {
		let key;
		if (option) key = option.key;
		// {
		// 	key: 'key';
		// }
		let result: any = {
			remove: [],
			add: [],
			update: [],
		};
		if (objOld.length != objNew.length) {
			objOld.forEach((elementOld) => {
				let name = elementOld[key];
				if (!objNew.find((obj) => obj[key] === name))
					result.remove.push(name);
			});
	
			let keyChange = 'price';
			objNew.map((x) => {
				let oldItem;
				objOld = objOld.map((y) => {
					// check item update
					if (x[key] == y[key]) {
						y['remove'] = false;
						oldItem = x;
						if (x[keyChange] != y[keyChange]) {
							x[keyChange + '_old'] = y[keyChange];
							result.update.push(x);
						}
					}
					return y;
				});
				if (oldItem == undefined) {
					result.add.push(x);
				}
				result.remove = objOld.filter((y) => !y.remove);
			});
			// objNew.forEach(() => {

			// 	// objOld.some(x=>{
			// 	// 	if(x[key]== element[key] && x[keyChange]!= element[keyChange]){
			// 	// 		element[keyChange+"_old"] =  x[keyChange];
			// 	// 		result.update.push(element);
			// 	// 		return true;
			// 	// 	}
			// 	// 	return false;
			// 	// })
			// });

			// objNew.forEach((elementNew) => {
			// 	let name = elementNew[key];
			// 	if (!objOld.find((obj) => obj[key] === name) )
			// 		result.add.push(name);

			// 	if(objOld.find((obj) => ))
			// });
		}
		if (objOld.length == objNew.length) {
			let change = this.compareObject(objOld, objNew);

			// objOld.forEach(elementOld => {
			// 	let name = elementOld[key];
			// 	if() {

			// 	}
			// });
		}
		if (this.objType == 'user_setting' && objOld.length == objNew.length)
			result.sortOrder = ' Change position';

		return result;
	}

	public compareObject(obj1, obj2) {
		var _this = this;
		// Make sure an object to compare is provided
		if (
			!obj2 ||
			Object.prototype.toString.call(obj2) !== '[object Object]'
		) {
			return obj1;
		}

		//
		// Variables
		//

		var diffs = {};
		var key;

		//
		// Methods
		//

		/**
		 * Check if two arrays are equal
		 * @param  {Array}   arr1 The first array
		 * @param  {Array}   arr2 The second array
		 * @return {Boolean}      If true, both arrays are equal
		 */
		var arraysMatch = function (arr1, arr2) {
			// Check if the arrays are the same length
			if (arr1.length !== arr2.length) return false;

			// Check if all items exist and are in the same order
			for (var i = 0; i < arr1.length; i++) {
				if (arr1[i] !== arr2[i]) return false;
			}

			// Otherwise, return true
			return true;
		};

		/**
		 * Compare two items and push non-matches to object
		 * @param  {*}      item1 The first item
		 * @param  {*}      item2 The second item
		 * @param  {String} key   The key in our object
		 */
		var compare = function (item1, item2, key) {
			// Get the object type
			var type1 = Object.prototype.toString.call(item1);
			var type2 = Object.prototype.toString.call(item2);

			// If type2 is undefined it has been removed
			if (type2 === '[object Undefined]') {
				if (!_this.removeKeys(key)) {
					diffs[key] = {
						old: null,
						new: null,
					};
				}

				return;
			}

			// If items are different types
			if (type1 !== type2) {
				//insert code
				if (!_this.removeKeys(key)) {
					diffs[key] = {
						old: item1,
						new: item2,
					};
				}

				// diffs[key] = item2;
				return;
			}

			// If an object, compare recursively
			if (type1 === '[object Object]') {
				var objDiff = _this.compareObject(item1, item2);
				if (Object.keys(objDiff).length > 1) {
					diffs[key] = objDiff;
				}

				return;
			}

			// If an array, compare
			if (type1 === '[object Array]') {
				if (!arraysMatch(item1, item2)) {
					// get value diffence
					let arr_diff = _this.arr_diff(item1, item2);
					if (!_this.removeKeys(key)) {
						diffs[key] = {
							old: arr_diff.old,
							new: arr_diff.new,
						};
					}

					// diffs[key] = item2;
				}
				return;
			}
			// Else if it's a function, convert to a string and compare
			// Otherwise, just compare
			if (type1 === '[object Function]') {
				if (item1.toString() !== item2.toString()) {
					if (!_this.removeKeys(key)) {
						diffs[key] = {
							old: item1,
							new: item2,
						};
					}

					// diffs[key] = item2;
				}
			} else {
				if (item1 !== item2) {
					if (!_this.removeKeys(key)) {
						diffs[key] = {
							old: item1,
							new: item2,
						};
					}

					// diffs[key] = item2;
				}
			}
		};
		// Loop through the first object
		for (key in obj1) {
			if (obj1.hasOwnProperty(key)) {
				compare(obj1[key], obj2[key], key);
			}
		}

		// Loop through the second object and find missing items
		for (key in obj2) {
			if (obj2.hasOwnProperty(key)) {
				if (!obj1[key] && obj1[key] !== obj2[key]) {
					if (!_this.removeKeys(key)) {
						diffs[key] = {
							old: null,
							new: obj2[key],
						};
					}
				}
			}
		}

		// diffs = this.removeKeys(diffs);

		// Return the object of differences
		return diffs;
	}

	removeKeys(key) {
		if ($.inArray(key, this.listRemoveKeys) != -1) {
			return true;
		}
		return false;
	}

	//difference 2 array
	arr_diff(a1, a2) {
		var a = {
			old: [],
			new: [],
		};

		for (var i = 0; i < a1.length; i++) {
			if (!a2.includes(a1[i])) a['old'].push(a1[i]);
		}

		for (var i = 0; i < a2.length; i++) {
			if (!a1.includes(a2[i])) a['new'].push(a2[i]);
		}

		return a;
	}
}
