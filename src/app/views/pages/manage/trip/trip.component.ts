import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef,EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { finalize, takeUntil, tap ,debounceTime,delay} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { TranslateService } from '@ngx-translate/core';
import { tollSegment,TollsegmentService, GeoService } from '@core/admin';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { CdkDragDrop, moveItemInArray,transferArrayItem} from '@angular/cdk/drag-drop';
import { AsciiPipe, ToastService } from '@core/_base/layout';
import { async } from 'q';
import { GeofenceService,DeviceService,TripService,TripsModel, DeviceGroupService} from '@core/manage';
import { PageService,itemPage,FunctionService } from '@core/_base/layout';
import { parameterEmit } from '@app/views/partials/content/widgets/list-group-device/list-group-device.component';


declare var $: any;
@Component({
  selector: 'kt-trip',
  templateUrl: './trip.component.html',
  styleUrls: ['./trip.component.scss'], 
  providers: [AsciiPipe]

})
export class TripComponent implements OnInit {
   
  paramsCurrency: FormGroup;
  formSegmentType: FormGroup;
  searchFormSegmentType : FormGroup;
  selectdp: ElementRef;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idsegmentTypeEdit: number;
  public dataDefault: any = [{}];
  public idsegmentTypeDelete: any;
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public isTwoWay:number = 0;
  public listTollStationed:any = [];
  public listTollStationedId:any = [];
  public disableAdd:boolean = false;
  
  public parameters:any [];
  public contentDescriptopn:any;
  public editSelectDeviceType:boolean = false;
  public sortablejsOptions;
  public paramsEmiter: EventEmitter<any>;
  public isSelectToll:boolean = false;
  public userIdSelected:number;
  public listGeofences : any = [];
  public listDevices : any = [];
  public listDeviceGroups: any = [];
  public countChecked:number = 0;
  public listCheckedDevices:any =[];
  public listCheckedGroup:any =[];
  public isLoadDevice:boolean = false;
  public keySearchFilter:string = 'name';
  public objPageTrip:itemPage;
  public userIdChangeEmiter:EventEmitter<parameterEmit>;
  public dataDefaultComponent;
  public showUserTree:boolean = true;
  public parrentEmiter:EventEmitter<any> = new EventEmitter();

  // qcvn
 
  constructor(
    private segment: TollsegmentService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private asciiPipe:AsciiPipe,
    private translate: TranslateService,
    private geo : GeoService,
    private toast: ToastService,
    private geofenceService: GeofenceService,
    private deviceService: DeviceService,
    private page:PageService,
    private trips:TripService,
    private deviceGroup: DeviceGroupService,

  ) {
    this.userIdChangeEmiter = new EventEmitter();
    this.objPageTrip = this.page.getPageById(122232);
    this.paramsEmiter = new EventEmitter();
    this.sortablejsOptions = {
      animation: 270,
      handle: '.drag_div',
      onUpdate: (event: any) => {
        // this.updateListFavorite();
      }
    };
    this.unsubscribe = new Subject();
   
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.buildForm();
    this.refreshEventType();

  }

  async ngOnInit() {
    // await this.getListDevices();
    // await this.getListGeofences();
    // await this.getListDeviceGroup();
     
    this.updateDataTable();

    // this.dataTable.reload({ currentPage: 1 });

 }

 loadParams()
 {
  if(!this.paramsCurrency)  this.paramsCurrency = this.formBuilder.group({});
  this.parameters.map(item=>{
    
    let name = "name_"+item.id;
    this.paramsCurrency.controls[name] = new FormControl({ value: item.value, disabled: false }); 
    return item;
  })
 }

  private buildForm(): void {
    this.formSegmentType = this.formBuilder.group({
        name: ['',Validators.required],
        isTwoWay: ['0',Validators.required],

      });
    this.searchFormSegmentType = this.formBuilder.group({
      name: [''],
      roadIdSearch:[''],  
    });

  }

  remove_character(str_to_remove, str) {
    let reg = new RegExp(str_to_remove);
    str = str.toString().replace(/,/g, "");
    return str;
  }


  test(number)
  {
    if(number == this.parameters.length-1) return true;
    return false;
  }

  buttonAddNew(content) {
    if(this.listGeofences) this.listGeofences = this.listGeofences.map(toll=>{
       toll.checked = false;
       return toll;
    });
    this.listTollStationed = [];
    
    this.isEdit = false;
    this.isSelectToll = false;
    this.open(content);

    this.formSegmentType = this.formBuilder.group({
      name: ['',Validators.required],
      isTwoWay: [true,Validators.required],
    });

  }
  
  open(content) {

    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-800'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }



  getNameGeofence(id,listObj){
    // if(this.listGeofences != undefined && this.listGeofences.find(x=>x.id == id))  return this.listGeofences.find(x=>x.id == id).name;
    if(listObj != undefined && listObj.find(x=>x.id == id))  return listObj.find(x=>x.id == id).name;

  }


  removeToll(id)
  {
    this.listTollStationed = this.listTollStationed.filter(function( obj ) {
      return obj.id !== id;
    });
    this.listGeofences.find(toll=> toll.id == id).checked = false;
  }

  getListIdTollSelected()
  {
    this.listTollStationedId = [];
    this.listTollStationed.map(obj=>{
      this.listTollStationedId.push(obj.id);
      return obj;
    });
  }

  changeChecked(event){
    if(event.checkedAll)
    {
      this.listTollStationed = event.listFullSelected;
      return;
    }
    if(event.countChecked == 0)
    {
      this.listTollStationed = [];
      return;
    }
    if(event.action)
    {
      let checked = event.action.checked;
      let id = event.action.id;
      let name = event.action.name;
      if(checked == true) this.listTollStationed.push({
        id:id,
        name:name
      });
      if(checked == false) this.listTollStationed = this.listTollStationed.filter(toll=> {
        return toll.id !=id;
      })
    }
  }

  onSubmitTollStation(form: any) {
    this.getListIdTollSelected();
    if (this.formSegmentType.invalid) {
      return;
    }
      
    if(this.listTollStationedId.length == 0){
      this.toast.show({message:'xin vui lòng chọn vùng',type:'error'});
      return;
    }

    // if(this.listCheckedDevices.length == 0){
    //     this.toast.show({message:'xin vui lòng chọn thiết bị',type:'error'});
    //     return;
    //   }

    if(form.value.isTwoWay == true) form.value.isTwoWay = 1;
    if(form.value.isTwoWay == false) form.value.isTwoWay = 0;

    let params = {
        userId: this.userIdSelected,
        name:form.value.name,
        stations : this.listTollStationedId.toString(),
        groupIds : this.listCheckedGroup,
        deviceIds:this.listCheckedDevices,
      
        status:form.value.isTwoWay,
    } as TripsModel; 
   
    if(this.isEdit) {
      params.id = this.idsegmentTypeEdit;      
      this.trips.update(params,{ notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          this.getData();
          this.modalService.dismissAll();
        }
      });
      return;
    }

    this.trips.create(params,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        this.getData();
        this.modalService.dismissAll();

      }
    });
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
      orderBy:'createdAt',
      orderType: 'DESC'
    };
  }

  onHideModal(id: string) {
    let idModal = '#' + id;
    $(idModal).modal('hide');
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }

  getIdAction(id,idModal) {
    this.idsegmentTypeDelete = id;
    this.modalService.open(idModal, { windowClass: 'kt-mt-50  modal-holder modal-delete',backdrop:'static'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openModal(params) {
     let idModal = params.idModal;
     let confirm = params.confirm;
     if(confirm)
     {$(idModal).appendTo('body').modal({backdrop: 'static',keyboard: false})}
     else{$(idModal).appendTo('body').modal('show');}
  }

  deleteTollSegment() {
    let id = this.idsegmentTypeDelete;
    this.trips.delete(id, { notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.getData();
        this.modalService.dismissAll();
      }
    });
    
  }

  getNameIsWay(id){
    let text = "Không";
    if(id == 1) text = "Có";
    return text;
  }

  editTollSegment(item,content) {
    this.isEdit = true;
    this.listTollStationed = [];
  
    if(item.listToll) this.listTollStationed = item.listToll;
    this.listGeofences.map(toll=>{
      toll.checked = false;
      if(this.listTollStationed.find(x=>x.id == toll.id)) toll.checked = true;
      return toll;
    })

    //default list group and device
    this.dataEdit = item;
    this.dataDefaultComponent = {
      listGrouped:item.groupIds,
      listDeviced:item.deviceIds
    };
    // end list group and device

    if(item.isTwoWay == 1) item.isTwoWay = true;
    if(item.isTwoWay == 0) item.isTwoWay = false;

    this.idsegmentTypeEdit = item.id;
    this.formSegmentType = this.formBuilder.group({
      name: new FormControl({ value: item.name, disabled: false },Validators.required),
      isTwoWay: new FormControl({ value: item.status, disabled: false }),
    });
    this.open(content);
  }

  sortOrder(arr)
  {
    return arr.sort((a,b)=> parseInt(a.sortOrder) - parseInt(b.sortOrder));
  }

  loadSelectBootstrap(){
		$('.bootstrap-select').selectpicker();
  }

  private refreshEventType()
  {
     this.translate.onLangChange.subscribe((event) => {
     let country = this.translate.instant('ADMIN.TOLL_STATION.COLUMN.SELECT_COUNTRY');
     let type = this.translate.instant('MANAGE.POI.GENERAL.TYPE_POI');
     
     $(function () {
       $('#listType').selectpicker({title: type}).selectpicker('refresh');
       $('#countrySearch').selectpicker({title: country}).selectpicker('refresh');

     });
   });
  }

 async userTreeChangeUser(value) {
   this.dataTable.isLoading = true;
    if (value.id > 0) {
      this.userIdSelected = value.id;
      await this.getListDevices();
      await this.getListGeofences();
      await this.getListDeviceGroup();      
      this.dataTable.reload({ currentPage: 1 });
      }
  }
  changeListDeviceGroup(event)
  {

    this.listCheckedGroup = event.listGroup;
    this.listCheckedDevices = event.listDevice;
  }
  
  loadDevices(){
    let paramsEmiter = {
      userId:this.userIdSelected,
      dataDefault:{},
      listDevices:this.listDevices,
      listGroups:this.listDeviceGroups
    } as parameterEmit;
    if(this.isEdit)
    {
      paramsEmiter.dataDefault = this.dataDefaultComponent;
    }
    this.userIdChangeEmiter.emit(paramsEmiter);
  }

   getListDeviceGroup(){
    let option : any = [];
    option['pageSize'] = 10;
    option['pageNo'] = -1;
    
    if(this.userIdSelected != undefined)    option['user_id'] = this.userIdSelected;
    return  this.deviceGroup.list({ params: option }).pipe(
        finalize(() => {
            this.cdr.markForCheck();
          }),
          tap((data: any) => {
            if(data.status == 200)
            {
              this.listDeviceGroups = data.result;       
            }
      
      
          })
    ).toPromise();
  }

  getListDevices(){
    let option : any = [];
    option['pageNo'] = -1;
    if(this.userIdSelected != undefined)    option['userId'] = this.userIdSelected;
    return  this.deviceService.list({ params: option }).pipe(
        finalize(() => {
            this.cdr.markForCheck();
          }),
          tap((data: any) => {
            if(data.status == 200)
            {
              this.listDevices = data.result;
              this.isLoadDevice = true;
              
            }
      
      
          })
    ).toPromise();
  }

  getListGeofences(){
    let option : any = [];
    option['pageNo'] = -1;
    
    if(this.userIdSelected != undefined)    option['userId'] = this.userIdSelected;
   return  this.geofenceService.list({ params: option }).pipe(
    tap(data=>{
      
      this.dataDefault = [{}];
      this.listGeofences = data.result;
    }),
    finalize(() => {
        this.cdr.markForCheck();
        })
    ).toPromise();
    
  }

  searchTollSegment(form:any)
  {
    if(form.value.name == null ||  form.value.name == '')  form.value.name = '';
    if(form.value.roadIdSearch == null ||  form.value.roadIdSearch == '')  form.value.roadIdSearch = '';
    this.filter.name = form.value.name;
    this.filter.roadId = form.value.roadIdSearch;
    
    this.dataTable.reload({ currentPage: 1 });
  }

  resetFormSearch()
  {
    this.searchFormSegmentType.reset();
    this.buildPagination();
    $('select').selectpicker("refresh");
    this.dataTable.reload({ currentPage: 1 });

  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;  
        this.getData();
      }),
      
    ).subscribe();
  }

  get f() {
   if(this.formSegmentType != undefined) return this.formSegmentType.controls;
  }

  changeActive(item:TripsModel) {
    let params = {
      id:item.id,
      status :  (item.status == 1 ? 0 : 1)
    } as TripsModel;
    this.trips.changeActive(params, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.dataTable.reload({});
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  getlistName(listIds,listObj)
  {
    // console.log('listObj',listObj);
    if(listIds != null && listObj != undefined)
    {
      let stationIds = listIds;
      if(typeof stationIds == 'string') stationIds = stationIds.split(",");
      let nameGeofence = '';
      let listToll = [];
      stationIds.forEach((id,i) => {
        let nameById = '';
        if(this.getNameGeofence(id,listObj))  nameById = this.getNameGeofence(id,listObj);
        if(i != stationIds.length -1) nameGeofence = nameGeofence+nameById+', ';
        if(i == stationIds.length -1) nameGeofence = nameGeofence+nameById;
        listToll.push({
          id:id,
          name:nameById
        });
      });

      return {
        listToll : listToll,
        names:nameGeofence
      };
      // list name device

    }
  }

  getData() {
    if(this.userIdSelected != undefined) this.filter.userId = this.userIdSelected;

    this.trips.list(this.filter)
      .pipe(
        delay(400),
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          result.result.content.map(toll=>{
            // list name geofence
              let objGeofence = this.getlistName(toll.stations,this.listGeofences);
              toll.listToll = objGeofence.listToll;
              toll.nameGeofence = objGeofence.names;

            // list name group

            let objGroup = this.getlistName(toll.groupIds,this.listDeviceGroups);
            toll.nameGroups = objGroup.names;

            // list name device

            let objDevice = this.getlistName(toll.deviceIds,this.listDevices);
            toll.nameDevices = objDevice.names;
            
            return toll;
          });
          this.data = result.result.content;
          //data test
         
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          // $(function () {
          //   $('select').selectpicker();
          // });
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect, 
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
                selecter:false,
                responsive:false
              }
              
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
  }
  
  drop(event: CdkDragDrop<any[]>) {

    let data:any = [];
    moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    data = event.container.data.map((item,i)=>{
     
      let obj = {
        id:item.id,
        sortOrder:i
      };
      return obj;
    });
     this.segment.sortOrder(data, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.cdr.detectChanges();
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(this.cdr.markForCheck)
    ).toPromise();   
  }

  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '40px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Name',
        field: 'name',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '250px' },
        class: '',
        translate: 'COMMON.COLUMN.NAME',
        autoHide: false,
        width: 250,
      },

      {
        title: 'List',
        field: 'limitspeed',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '300px' },
        class: '',
        translate: 'MANAGE.TRIP.COLUMN.LIST',
        autoHide: true,
        width: 182,
      },
    
      {
        title: 'group device',
        field: 'device',
        allowSort: false,
        isSort: false,
        dataSort: '', 
        style: { 'width': '300px' },
        class: '',
        translate: 'MENU.GROUP_DEVICE',
        autoHide: true,
        width: 182,
      },
      {
        title: 'device',
        field: 'device',
        allowSort: false,
        isSort: false,
        dataSort: '', 
        style: { 'width': '300px' },
        class: '',
        translate: 'DASHBOARD.DEVICE',
        autoHide: true,
        width: 182,
      },
  
      
      {
        title: 'status',
        field: 'status',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px','text-align':'center' },
        class: '',
        translate: 'COMMON.COLUMN.STATUS',
        autoHide: true,
        width: 350,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 182,
      },
    ]
  }


}
