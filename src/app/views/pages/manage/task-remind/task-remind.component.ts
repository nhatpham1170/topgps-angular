import { group } from '@angular/animations';
import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { finalize, takeUntil, tap, debounceTime, map } from 'rxjs/operators';
import { Subject, interval } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { TaskRemind, TaskRemindService, GeofenceService, Device } from '@core/manage';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DeviceService } from '@core/manage';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { ToastService, ValidatorCustomService, UserDateAdvPipe } from '@core/_base/layout';
import { NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter } from '@ng-bootstrap/ng-bootstrap';
import { ValidationService } from '@core/common/_service/validation.service';
import { TranslateService } from '@ngx-translate/core';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { AlertRule } from '@core/manage/_models/alert-rules';
import moment from 'moment';
import { object, array } from '@amcharts/amcharts4/core';
import { delay } from 'q';

declare var $: any;
@Component({
    selector: 'kt-task-remind',
    templateUrl: './task-remind.component.html',
    styleUrls: ['./task-remind.component.scss'], 
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateNativeAdapter },UserDateAdvPipe]

})
export class TaskRemindComponent implements OnInit {
  public formAlert: FormGroup;
  public formBasic: FormGroup;
  public formEventType: FormGroup;
  public formNotification: FormGroup;
  public formDevice: FormGroup;
  public formGeofence: FormGroup;
  public formEventParams: FormGroup; 
  public searchFormAlert: FormGroup;  
  public control: FormControl;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];    
  public idAlertEdit: number;
  public dataDefault: any = [{}];
  public idAlertDelete: any;
  public filter: any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  //global
  public listDeviced: any = [];
  public listDevices: any = [];
  public listDayed: any = [];
  public countChecked: number;
  public eventType: number;
  public eventTypeActive: string;
  public dateTime:string;
  public parameterValue: any;
  public parameterValueText: any = '';
  public userIdSelected:number;
  //schedule  
  //end push setting
 
  public eventAlert: any = [];
  public listEventAlert: any = [];
  public listWeeks: any = [];
  public listGeofence: any = [];
  public eventTypes: Array<any> = [];
  public checkListGeofence: boolean = false;
  public checkListDevice: boolean = false;
  public periodFromConfig: any = {
    key: "periodFrom", singleDatePicker: true, size: 'md', timePicker: false, format: 'date', autoSelect: true, btnClear: true,
    optionDatePicker: { autoPlay: true, drops: 'up' }
  };
  public periodToConfig: any = {
    key: "periodTo", singleDatePicker: true, size: 'md', timePicker: false, format: 'date', autoSelect: true, btnClear: true,
    optionDatePicker: { autoPlay: true, drops: 'up' }
  };
  public checkListDataChangeEmiter: EventEmitter<any>;
  public checkListGeofenceChangeEmiter: EventEmitter<any>;
  public listDevice: Array<Device> = [];
  public listDeviceSelected: Array<any> = [];
  public isChangeUser: boolean = true;
  public listGeofences: Array<Device> = [];
  public listGeofenceSelected: Array<any> = [];
  public currentModel:TaskRemind;
  public currentReason: any;  
  public isShowRepeat:boolean = false;
  public ishowListWeeks:boolean = false;
  public showUserTree:boolean = true;
  @ViewChild('datePicker', { static: true }) 
  datePicker: DateRangePickerComponent;

  @ViewChild('periodFrom', { static: false }) periodFrom: DateRangePickerComponent;
  @ViewChild('periodTo', { static: false }) periodTo: DateRangePickerComponent;
  public templateDefault: { name: string,id:number, template: any,group:string } = {
    id:3,
    name: "battery_low",
    group:"device",
    template: {}
  };
  public currentTemplate: { name: string,id:number, template: any,group:string } = {
    id:3,
    name: "battery_low",
    group:"device",
    template: {
      percent: 20,
      interval: "000000",
    }
  };
  public eventTypeGroup:Array<string> = [
    "device","action","in_out"
  ];

  public datePickerOptions: any = {
    format:"datetime",
    key: "datePicker", singleDatePicker: true, size: 'md', timePicker: true, autoSelect: true,
    optionDatePicker: { autoPlay: true, drops: 'up' },
  

  };

  public datePickerOptionsDayPass: any = {
    key: "datePickerDayPass", singleDatePicker: true, size: 'md', timePicker: false, format: 'date', autoSelect: true,
    optionDatePicker: { autoPlay: true, drops: 'up' }

  };

  public datePickerOptionsDayNext: any = {
    key: "datePickerDayNext", singleDatePicker: true, size: 'md', timePicker: false, format: 'date', autoSelect: true,
    optionDatePicker: { autoPlay: true, drops: 'up' }

  };

  public listRepeat:Array<any> = [];
  
  public eventTypeConfigs: Array<{ name: string,id:number, template: any,group:string }>=[];
  constructor(
    private remindService: TaskRemindService,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private deviceService: DeviceService,
    private toast: ToastService,
    private geofenceService: GeofenceService,
    private validate: ValidatorCustomService,
    private translate: TranslateService,
    private userDateAdv:UserDateAdvPipe,
  ) {
    this.unsubscribe = new Subject();
    this.checkListDataChangeEmiter = new EventEmitter();
    this.checkListGeofenceChangeEmiter = new EventEmitter();
    this.listWeeks = [
      { id: 2, name: 'MO', checked: true },
      { id: 3, name: 'TU', checked: true },
      { id: 4, name: 'WE', checked: true },
      { id: 5, name: 'TH', checked: true },
      { id: 6, name: 'FR', checked: true },
      { id: 7, name: 'SA', checked: false },
      { id: 1, name: 'SU', checked: false }
    ];
    this.eventTypeConfigs = this.remindService.configTemplate;
    // this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    this.updateDataTable();
    // this.getData();
    this.loadListEventTypeSelect();
    this.buildFormSearch();
    $(function () {
      $('.selectpicker').selectpicker();
    })
  }

  buildListRepeat(){
    this.listRepeat = [
      {id:'no',name:"Không"},
      {id:'day',name:"Hàng ngày"},
      {id:'week',name:"Hàng tuần"},
      {id:'month',name:"Hàng tháng"},
      {id:'year',name:"Hàng năm"},
    ];
  }

  private loadListEventTypeSelect() {
    this.remindService.eventTypes({
      sessionStore: true,
    })
      .pipe(
        tap((data) => {
          this.eventTypes = data.result;
          setTimeout(() => {
            $('#eventTypeId').selectpicker('refresh');
          });
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      ).subscribe();
  }
  public getEventTypeGroups(){
    let result = [];
    this.eventTypeConfigs.forEach(x=>{
      if(!result.some(i=>i == x.group)) 
        result.push(x.group)
    });
    return result;
  }

  public getEventTypesByGroup(group?:string){
    if(group){
      return this.eventTypeConfigs.filter(x=>x.group == group);
    }else{
      return this.eventTypeConfigs;
    }
  }
  get getEventTypeGroupActive():string{
    return this.getEventTypeGroups().find(x=>x == this.currentTemplate.group)
  }

  getListRepeat(e)
  {
    this.checkShowListWeek(e.target.value);
  }


  private getListEventType() {
    this.remindService.eventTypes({
      // sessionStore: true,
    })
      .pipe(
        tap((result) => {
          if (result.status == 200) {
            this.eventAlert = result.result;
            this.eventTypeActive = this.currentTemplate.name;
            // this.buildFormEventParams(this.currentTemplate);
            this.listEventAlert = [];
            for (var i = 0; i < this.eventAlert.length; i++) {
              this.listEventAlert = this.listEventAlert.concat(this.eventAlert[i].eventTypes);

            }
          }
          this.cdr.detectChanges();
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      ).subscribe();
  }

 checkValueInput(val){
   if(val < 0) return 0;
   return val;
 }

  private buildFormEventParams(eventType: any) {      
    switch (eventType.name) {
      case "maintenance_by_distance":
          this.formEventParams = new FormGroup({
            odometer: new FormControl(eventType.template.odometer, [this.validate.required]),
            odometer_before: new FormControl(eventType.template.odometer_before, [this.validate.required]),
            odometer_after: new FormControl(eventType.template.odometer_after, [this.validate.required]),
            day_next: new FormControl(eventType.template.day_next, [this.validate.required]),
            day_pass: new FormControl(eventType.template.day_pass, [this.validate.required]),
          });
          break;  
      case "maintenance_by_hours":
              this.formEventParams = new FormGroup({
                total_hours: new FormControl(eventType.template.total_hours, [this.validate.required]),
                pass_hours: new FormControl(eventType.template.pass_hours, [this.validate.required]),
                next_hours: new FormControl(eventType.template.next_hours, [this.validate.required]),
                day_pass: new FormControl(eventType.template.day_pass, [this.validate.required]),
              });
          break;  
      default:
        this.formEventParams = new FormGroup({
          value: new FormControl(""),
          interval: new FormControl("000000")
        });
        break;
    }
    this.formEventParams.clearAsyncValidators();
    this.formEventParams.markAllAsTouched();
    this.formEventParams.updateValueAndValidity();

    this.cdr.detectChanges();

  }

  private buildFormSearch() {
    this.searchFormAlert = this.fb.group({
      name: new FormControl(""),
    });
  }

  checkShowListWeek(day)
  {
    this.isShowRepeat = false;
    if(day == 'day' && day != undefined) this.isShowRepeat = true;
  }

  private buildForm(item?: TaskRemind): void {
    this.buildListRepeat();
    if(item){
      this.formBasic = this.fb.group({
        name: new FormControl(item.name, Validators.required),
        description: new FormControl(item.description),        
        active: new FormControl(item.suspended),
      });
      this.formDevice = this.fb.group({
        deviceIds: new FormControl(item.deviceIds),///???
        isAllDevices: new FormControl(item.extendedParams.isAllDevices),
      });

      // set current template
      
      this.currentTemplate = this.eventTypeConfigs.find(x=>x.name == item.eventType);     
      if(!this.currentTemplate) this.currentTemplate = this.templateDefault;
      // if(this.currentTemplate) this.currentTemplate.template = item.parameterValue;
      this.formEventType = this.fb.group({
        eventType: new FormControl(this.currentTemplate.name, this.validate.select),
        params: new FormControl(),
      });
      this.checkShowListWeek(item.schedule.repeatSelect);
      this.buildFormEventParams(this.currentTemplate);


      if(this.currentTemplate.template['odometer']!=undefined)  this.formEventParams.controls["odometer"].setValue(this.checkValueInput(item.parameterValue['odometer']));
      if(this.currentTemplate.template['odometer_before']!=undefined)  this.formEventParams.controls["odometer_before"].setValue(this.checkValueInput(item.parameterValue['odometer_before']));
      if(this.currentTemplate.template['odometer_after']!=undefined)  this.formEventParams.controls["odometer_after"].setValue(this.checkValueInput(item.parameterValue['odometer_after']));
      if(this.currentTemplate.template['total_hours']!=undefined)  this.formEventParams.controls["total_hours"].setValue(this.checkValueInput(item.parameterValue['total_hours']));
      if(this.currentTemplate.template['pass_hours']!=undefined)  this.formEventParams.controls["pass_hours"].setValue(this.checkValueInput(item.parameterValue['pass_hours']));
      if(this.currentTemplate.template['next_hours']!=undefined)  this.formEventParams.controls["next_hours"].setValue(this.checkValueInput(item.parameterValue['next_hours']));

      if(this.currentTemplate.template['day_next']!=undefined)  this.formEventParams.controls["day_next"].setValue(this.checkValueInput(item.parameterValue['day_next']));
      if(this.currentTemplate.template['day_pass']!=undefined)  this.formEventParams.controls["day_pass"].setValue(this.checkValueInput(item.parameterValue['day_pass']));
      if (item.parameterValue.day_next > 0) {
        this.datePickerOptionsDayNext['startDate'] = this.userDateAdv.transform(item.parameterValue.day_next,"YYYY/MM/DD HH:mm:ss");         
      }          
      if (item.parameterValue.day_pass > 0) {
        this.datePickerOptionsDayPass['startDate'] = this.userDateAdv.transform(item.parameterValue.day_pass,"YYYY/MM/DD HH:mm:ss");      
      }   


      this.formEventParams.updateValueAndValidity();

      let daysForm = [];   
      this.listWeeks.forEach(x => {        
        daysForm.push(this.fb.group({
          itemId: x.id,
          itemName: x.name,
          itemChecked: item.schedule.weekly.day.some(d=>d==x.id),
        }));
      });      
      if (item.schedule.periodForm > 0) {
        this.periodFromConfig['startDate'] = this.userDateAdv.transform(item.schedule.periodForm,"YYYY/MM/DD HH:mm:ss");         
      }          
      if (item.schedule.periodTo > 0) {
        this.periodToConfig['startDate'] = this.userDateAdv.transform(item.schedule.periodTo,"YYYY/MM/DD HH:mm:ss");      
      }    
      

      if (item.schedule.dateTime > 0) {
        this.datePickerOptions['startDate'] = this.userDateAdv.transform(item.schedule.dateTime,"YYYY/MM/DD HH:mm:ss");         
      }    
      

      this.formNotification = this.fb.group({
        textPrimary: new FormControl(item.textPrimary, Validators.required),
        notificationSMS: new FormControl(item.pushSetting.smsPhones.join(","), [this.validate.phones]),
        notificationEmail: new FormControl(item.pushSetting.emails.join(","), [this.validate.emails]),
        notificationIsPushOnMobile: new FormControl(item.pushSetting.pushEnabled),
        notificationIsPushSOS: new FormControl(item.pushSetting.sos),
  
        scheduleDays: new FormControl(""),
        scheduleTimeStart: new FormControl(this.convertUnixToTime(item.schedule.weekly.timeStart),[this.validate.minLength(6)]),
        scheduleTimeEnd: new FormControl(this.convertUnixToTime(item.schedule.weekly.timeEnd),[this.validate.minLength(6)]),
        periodFrom: new FormControl(item.schedule.periodForm),
        periodTo: new FormControl(item.schedule.periodTo),
        periodActive: new FormControl( item.schedule.periodActive == true ? "1":"0"),
        listWeeks: this.fb.array(daysForm),
        days: new FormControl(item.schedule.weekly.day),
        repeatSelect : new FormControl('no'),
        dateTime: new FormControl(item.schedule.dateTime),
      });      
  
      setTimeout(() => {
       $('#repeatSelect1').val(item.schedule.repeatSelect).selectpicker('refresh'); 
      });
      this.formGeofence = this.fb.group({
        geofenceStatus: new FormControl(item.extendedParams.geofenceStatus),        
        isAllGeofence: new FormControl(item.extendedParams.isAllGeofence),
        geofenceIds: new FormControl(item.geofenceIds),
      });        
    }
    else{      
      this.currentTemplate = this.eventTypeConfigs[0];
      // new form
      this.formBasic = this.fb.group({
        name: new FormControl("", Validators.required),
        description: new FormControl(""),        
        active: new FormControl(true),
      });
      this.formDevice = this.fb.group({
        deviceIds: new FormControl([]),
        isAllDevices: new FormControl(true),
      });
      this.formEventType = this.fb.group({
        eventType: new FormControl(this.currentTemplate.name, this.validate.select),
        params: new FormControl(),
      });
      let daysForm = [];   
      this.listWeeks.forEach(x => {
        daysForm.push(this.fb.group({
          itemId: x.id,
          itemName: x.name,
          itemChecked: x.checked,
        }));
      });
  
      this.formNotification = this.fb.group({
        textPrimary: new FormControl("", Validators.required),
        notificationSMS: new FormControl("", [this.validate.phones]),
        notificationEmail: new FormControl("", [this.validate.emails]),
        notificationIsPushOnMobile: new FormControl(true),
        notificationIsPushSOS: new FormControl(true),
  
        scheduleDays: new FormControl(""),
        scheduleTimeStart: new FormControl("000000",[this.validate.minLength(6)]),
        scheduleTimeEnd: new FormControl("235959",[this.validate.minLength(6)]),
        periodFrom: new FormControl(0),
        periodTo: new FormControl(0),
        periodActive: new FormControl("0"),
        listWeeks: this.fb.array(daysForm),
        days: new FormControl(this.listWeeks.filter(x=>x.checked === true).map(x=>x.id)),
        repeatSelect : new FormControl('no'),
        dateTime: new FormControl(0),
      });       
  
      this.formGeofence = this.fb.group({
        geofenceStatus: new FormControl("0"),
       
        isAllGeofence: new FormControl(true),
        geofenceIds: new FormControl([]),
      });   
  
      this.buildFormEventParams(this.currentTemplate);
    }

    this.formNotification.get("listWeeks").valueChanges.subscribe(x=>{      
      let days = [];
      x.forEach(day=>{
        if(day.itemChecked === true) days.push(day.itemId);
      })
      this.formNotification.controls["days"].setValue(days);      
    });

    this.formDevice.get('isAllDevices').valueChanges.subscribe(x=>{    
      if(x === false){
        this.formDevice.get('deviceIds').setValidators(this.validate.required);        
      } else  {
        this.formDevice.get('deviceIds').clearValidators();        
        this.formDevice.get('deviceIds').markAsTouched();
      }
      this.formDevice.get('deviceIds').updateValueAndValidity();
      
    })    
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
      orderBy: 'createdAt',
      orderType: 'DESC'
    };
  }
  get formArr(): FormArray {
    if (this.formNotification) {
      return this.formNotification.get('listWeeks') as FormArray;
    }
  }
  onChangeEventAlert(item) {
    this.eventTypeActive = item.name;
    this.formEventType.controls['eventType'].setValue(this.eventTypeActive);
    $('.tab-event a').removeClass('active');
    let eventType = this.eventTypeConfigs.find(x => x.name === item.name);

    if (eventType) {
      this.currentTemplate = eventType;
    } else {
      this.currentTemplate = this.templateDefault;
    }
    // set form 
    this.buildFormEventParams(this.currentTemplate);
    
  } 

  getListDevices() {
    if (!this.checkListDevice) {
      let option: any = [];
      option['pageNo'] = -1;
      if (this.listDevices.length == 0) {
        this.deviceService.list({ params: option }).pipe(
        ).subscribe((data: any) => {
          this.dataDefault = [{}];
          this.listDevices = data.result;
          this.checkListDevice = true;
        });
      }
    }
  }

  ChangeUserTree(value) {
    if (value.id > 0) {
      $('#frmSearch .selectpicker').val('default').selectpicker("refresh");
      this.filter.userId = value.id;
      this.userIdSelected = value.id;
      this.getData();
      // this.listDevicePicker.reset();
      // this.userIdSelectedEdit = value.id;
      // this.getDevices(value.id);
      // this.buildDateNow();
      // this.dataTable.update({
      //   data: [],
      //   totalRecod: 0
      // });
    }
  }

  getDevices() {
    if (!this.isChangeUser) {
      let listTemp = JSON.parse(JSON.stringify(this.listDevice));
      listTemp = this.processActiveCheckList(listTemp,this.currentModel.deviceIds);
      this.checkListDataChangeEmiter.emit(listTemp);
    }
    else {
      const params = {
        pageNo: -1,
      }

      this.deviceService.list({ params: params }).pipe(
        tap((data) => {
          this.listDevice = data.result;
          let listTemp = JSON.parse(JSON.stringify(this.listDevice));        
          listTemp = this.processActiveCheckList(listTemp,this.currentModel.deviceIds);
          this.checkListDataChangeEmiter.emit(listTemp);
          this.isChangeUser = false;
          this.cdr.detectChanges();
        },
          (error => {
            this.checkListDataChangeEmiter.emit([]);
          })),

        takeUntil(this.unsubscribe),
        finalize(this.cdr.markForCheck)
      ).subscribe();
    }
  }

  getGeofences() {
    this.checkListGeofenceChangeEmiter.emit([]);

    if (!this.isChangeUser) {
      let listTemp = JSON.parse(JSON.stringify(this.listGeofences));
      listTemp = this.processActiveCheckList(listTemp,this.currentModel.geofenceIds);
      this.checkListGeofenceChangeEmiter.emit(listTemp);
      this.cdr.detectChanges();
    }
    else {
      const params = {
        pageNo: -1,
      }
      params['orderBy'] = "sortOrder";
      params['orderType'] = "asc";
      this.geofenceService.list({ params: params }).pipe(
        tap((data) => {
          this.listGeofences = data.result;
          let listTemp = JSON.parse(JSON.stringify(this.listGeofences));
          listTemp = this.processActiveCheckList(listTemp,this.currentModel.geofenceIds);                    
          this.checkListGeofenceChangeEmiter.emit(listTemp);
          
          this.isChangeUser = false;
          this.cdr.detectChanges();
        }),
        takeUntil(this.unsubscribe),
        finalize(this.cdr.markForCheck)
      ).subscribe();
    }
  }
  private processActiveCheckList(arrOriginal:Array<any>,arrSelect:Array<any>){  
    arrOriginal = arrOriginal.map(x=>{      
      x.checked = arrSelect.some(s=>s==x.id);
      return x;
    });
    return arrOriginal;
  }

  onWidgetCheckChange(event) {
    this.listDeviceSelected = event.listChecked.map((x) => x.id);
    this.formDevice.controls['deviceIds'].setValue(this.listDeviceSelected);
  }

  onChangeListGeofence(event) {
    this.listGeofenceSelected = event.listChecked.map((x) =>x.id);
    this.formGeofence.controls['geofenceIds'].setValue(this.listGeofenceSelected);
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  private action: string = "";
  openPopup(content, type: string, item?: any) {
    this.action = type;
    switch (type) {
      case "edit":
        this.editFnc(content, item);
        break;
      case "add":
        this.addFnc(content);
        break;
      case "delete":
        this.deleteFnc(content, item);
        break;
    }
  }
  
  editFnc(content, item: any) {
    // build form
    // set value    
    let model = new TaskRemind();
    Object.assign(model,item);
    this.currentModel = model;
    this.buildForm(item);
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-holder modal-dialog-scrollable modal-lg-1000', size: 'lg', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
    let _this = this;
    setTimeout(() => {
      _this.getDevices();
      _this.getGeofences();
    });
  }
  addFnc(content) {
    $(function () {
      $('.selectpicker').selectpicker();
    })
    // build form
    // reset from 
    this.currentModel = new TaskRemind();
    this.buildForm();
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-holder modal-dialog-scrollable modal-lg-1000', size: 'lg', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });

    let _this = this;
    setTimeout(() => {
      _this.getDevices();
      _this.getGeofences();
    });
  }
  
  deleteFnc(content, item: any) {
    this.currentModel = item;
    this.currentModel.id = item.id;
    this.modalService.open(content, { windowClass: 'kt-mt-50  modal-holder modal-delete', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }
  onSubmit() {
    switch (this.action) {
      case "edit":
        this.editAction();
        break;
      case "add":
        this.addAction();
        break;
      case "delete":
        this.deleteAction();
        break;
    }
  }
 
  editAction() {
    let validForm = this.validForm();
    if(validForm){
      const model = this.getDataForm();
      this.remindService.updateRemind(model,{notifyGlobal:true}).pipe(
        tap(data=>{
          if (data.status == 200) {
            this.modalService.dismissAll(this.currentReason);
            this.dataTable.reload({ currentPage: 1 });

          }         
        })
      ).subscribe();
    }  
  }
 
  addAction() {    
    let validForm = this.validForm();
    if(validForm){
      const model = this.getDataForm();
      this.remindService.createTaskRemind(model,{notifyGlobal:true}).pipe(
        tap(data=>{
          if (data.status == 201) {
            this.modalService.dismissAll(this.currentReason);
            this.dataTable.reload({ currentPage: 1 });

          }         
        })
      ).subscribe();
    }    
  }
 
  deleteAction() {
    this.remindService.delete(this.currentModel.id, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.modalService.dismissAll(this.currentReason);
          this.dataTable.reload({});
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  private validForm():boolean{
    let result = true;
    this.formBasic.markAllAsTouched();
    this.formEventType.markAllAsTouched();
    this.formEventParams.markAllAsTouched();
    this.formDevice.markAllAsTouched();
    this.formGeofence.markAllAsTouched();
    this.formNotification.markAllAsTouched();

    if (this.formBasic.invalid) {
      $("#kt_tab_header_base").click();
      result = false;
    }
    else if (this.formEventType.invalid || this.formEventParams.invalid) {
      $("#kt_tab_header_info_type").click();
      result = false;
    }
    else if (this.formDevice.invalid) {
      $("#kt_tab_header_device").click();
      result = false;
    }
    else if (this.formGeofence.invalid) {
      $("#kt_tab_header_geofence").click();
      result = false;
    }
    else if (this.formNotification.invalid) {
      $("#kt_tab_header_notification").click();
      result = false;
    }

    setTimeout(() => {
      const firstElementWithError = document.querySelector('#formAlert .invalid-feedback');
      if (firstElementWithError) {
        firstElementWithError.scrollIntoView({ block: 'center' });
      }
    });
    return result;
  }

  private getDataForm():TaskRemind{
    let model = this.currentModel;
    model.name = this.formBasic.controls['name'].value;
    model.description = this.formBasic.controls['description'].value;
    model.suspended = this.formBasic.controls['active'].value;
    // event type 
    model.eventType = this.currentTemplate.name;
    model.eventTypeId = this.currentTemplate.id;   
    if(this.currentTemplate.template['odometer']!=undefined)
    model.parameterValue.odometer = this.formEventParams.controls['odometer'].value;
    
    if(this.currentTemplate.template['odometer_before']!=undefined)
    model.parameterValue.odometer_before = this.formEventParams.controls['odometer_before'].value;

    if(this.currentTemplate.template['odometer_after']!=undefined)
    model.parameterValue.odometer_after = this.formEventParams.controls['odometer_after'].value;

    if(this.currentTemplate.template['total_hours']!=undefined)
    model.parameterValue.total_hours = this.formEventParams.controls['total_hours'].value;

    if(this.currentTemplate.template['pass_hours']!=undefined)
    model.parameterValue.pass_hours = this.formEventParams.controls['pass_hours'].value;

    if(this.currentTemplate.template['next_hours']!=undefined)
    model.parameterValue.next_hours = this.formEventParams.controls['next_hours'].value;

    if(this.currentTemplate.template['day_pass']!=undefined)
    model.parameterValue.day_pass = this.formEventParams.controls['day_pass'].value;

    if(this.currentTemplate.template['day_next']!=undefined)
    model.parameterValue.day_next = this.formEventParams.controls['day_next'].value;


    // device
    model.deviceIds =  this.formDevice.controls['deviceIds'].value || [];
    model.extendedParams.isAllDevices = this.formDevice.controls['isAllDevices'].value;
    if(model.extendedParams.isAllDevices){
      model.deviceIds = [];
    }
    // geofence
    model.extendedParams.geofenceStatus = this.formGeofence.controls['geofenceStatus'].value;    
    model.extendedParams.isAllGeofence = this.formGeofence.controls['isAllGeofence'].value;
    model.geofenceIds = this.formGeofence.controls['geofenceIds'].value || [];
    if(model.extendedParams.geofenceStatus == 0 || model.extendedParams.isAllGeofence){
      model.geofenceIds = [];
    }
    // notification
    model.textPrimary                 = this.formNotification.controls['textPrimary'].value;
    model.schedule.weekly.day         = this.formNotification.controls['days'].value;      
    // model.schedule.weekly.timeStart   = this.convertTimeToUnix( this.formNotification.controls['scheduleTimeStart'].value);
    
   
    // model.schedule.weekly.dateTime     = this.convertTimeToUnix( this.formNotification.controls['scheduleTimeEnd'].value);
    model.schedule.periodActive       = this.formNotification.controls['periodActive'].value == "1"?true:false;      
    model.schedule.periodForm         = this.formNotification.controls['periodFrom'].value;
    model.schedule.periodTo           = this.formNotification.controls['periodTo'].value;
    model.schedule.repeatSelect           = this.formNotification.controls['repeatSelect'].value;

    model.schedule.dateTime           = this.formNotification.controls['dateTime'].value;


    // notification - config     
    let emails:string  =  this.formNotification.controls['notificationEmail'].value || "";
    if(emails.trim().length>0) model.pushSetting.emails = emails.split(",");     
    // model.pushSetting.phones          = this.formNotification.controls['phones'].value;
    let smsPhones:string  =  this.formNotification.controls['notificationSMS'].value || "";
    if(smsPhones.trim().length>0)model.pushSetting.smsPhones = smsPhones.split(",");      
    model.pushSetting.pushEnabled     = this.formNotification.controls['notificationIsPushOnMobile'].value;
    model.pushSetting.sos             = this.formNotification.controls['notificationIsPushSOS'].value;
    model.userId = this.userIdSelected;
    return model;
  }

  private convertTimeToUnix(str:string,format:string = "HHmmss"):number{    
    try{
      return moment("2000/01/01 " + str,"YYYY/MM/DD " + format).unix() -  moment("2000/01/01 000000" ,"YYYY/MM/DD HHmmss").unix();
    }catch{}   
    return 0;
  }
  private convertUnixToTime(unix:number,format:string = "HHmmss"):string{    
    try{       
      return moment.unix(moment().utc().startOf('day').unix() + unix ).utc().format("HHmmss");
    }catch{}   
    return "000000";
  }
  searchAlert(form: any) {    
    this.dataTable.search();    
  }

  resetFormSearch() {    
    this.dataTable.resetSearch();
    $('#frmSearch .selectpicker').val('default').selectpicker("refresh");
    this.dataTable.search();

  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageSize = option.pageSize;
        this.filter.pageNo = option.pageNo;
        this.filter.userId = this.userIdSelected;
        this.getData();
      }),
    ).subscribe();
  }

  get f() {
    if (this.formAlert != undefined) return this.formAlert.controls;
  }

  getData() {
    this.remindService.list(this.filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          setTimeout(() => {
            $('select.selectpicker').selectpicker('refresh');
          });
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      formSearch: '#frmSearch',
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
      }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();

  }

  closeModal() {
    this.modalService.dismissAll();
  }


  changeLock(id, status) {
    let sendStatus = 1;
    if (status == sendStatus) {
      sendStatus = 0;
    }
    let params = {
      suspended: sendStatus,
      id: id
    } as TaskRemind;
    this.remindService.changeLock(params, { notifyGlobal: true }).pipe(
      tap((data: any) => {
        this.dataTable.reload({});
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  convertStringToArray(string) {
    if (string == '') return [];
    let list = string.split(',');
    return list;
  }
 
  getIdAction(id, idModal) {
    this.idAlertEdit = id;
    this.modalService.open(idModal, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-delete', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  dateSelectChange(data) 
  {
    let dateTime = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    dateTime = moment(dateTime,"YYYY-MM-DD HH:mm:ss").unix();
    this.formNotification.get("dateTime").setValue(dateTime);
  }

  dateSelectChangeDayPass(data) 
  {
    let date = data.endDate.startOf('day').unix();
    this.formEventParams.controls["day_pass"].setValue(date); 

  }

  dateSelectChangeDayNext(data) 
  {

    let date = data.endDate.startOf('day').unix();
    this.formEventParams.controls["day_next"].setValue(date); 

  }

  onChangePeriodTo(data) {
    if(data.endDate!=null) 
      this.formNotification.get("periodTo").setValue(data.endDate.endOf('day').unix());
    else this.formNotification.get("periodTo").setValue(0);    
  }
  onChangePeriodFrom(data) {
    if(data.endDate!=null)
      this.formNotification.get("periodFrom").setValue(data.endDate.startOf('day').unix());
    else this.formNotification.get("periodFrom").setValue(0);
  }
  onClickTabEventType(){
    setTimeout(() => {
      const firstElementWithError = document.querySelector('#eventTypes'+this.currentTemplate.name);
      if (firstElementWithError) {
        firstElementWithError.scrollIntoView({ block: 'center' });
      }
    });
  }
  get fAlert() {
    return this.formAlert.controls;
  }
  get fBasic() {
    return this.formBasic.controls;
  }
  get fNotification() {
    return this.formNotification.controls;
  }
  get fEventType() {
    return this.formEventType.controls;
  }
  get fGeofence() {
    return this.formGeofence.controls;
  }
  get fDevice() {
    return this.formDevice.controls;
  }
  get fEventParams() {
    return this.formEventParams.controls;
  }
  deleteAlert() {
    let id = this.idAlertEdit;
    this.remindService.delete(id, { notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.getData();
        this.modalService.dismissAll();
      }
    });
  }

  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '30px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Tên',
        field: 'name',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '160px' },
        class: '',
        translate: 'COMMON.COLUMN.NAME',
        autoHide: false,
        width: 160,
      },
      {
        title: 'Type Alert',
        field: 'typeAlert',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '200px' },
        class: '',
        translate: 'MANAGE.TASK_REMIND.GENERAL.TYPE_TASK',
        autoHide: true,
        width: 150,
      },
      {
        title: 'Notification text',
        field: 'notifi_text',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '160px' },
        class: '',
        translate: 'MANAGE.ALERT_RULE.GENERAL.NOTIFICATION_TEXT',
        autoHide: true,
        width: 160,
      },
      {
        title: 'Descript text',
        field: 'notifi_text',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'COMMON.COLUMN.DESCRIPTION',
        autoHide: true,
        width: 150,
      },
      {
        title: 'Status',
        field: 'status',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '80px', 'text-align': 'center' },
        class: '',
        translate: 'COMMON.COLUMN.STATUS',
        autoHide: true,
        width: 80,

      },
      {
        title: 'Created ',
        field: 'created',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'COMMON.COLUMN.CREATED_DATE',
        autoHide: true,
        width: 100,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 100,
      },
    ]
  }



}
