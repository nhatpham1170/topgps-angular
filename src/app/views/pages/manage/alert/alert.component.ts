import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { finalize, takeUntil, tap ,debounceTime} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { TranslateService } from '@ngx-translate/core';
import { AlertService,Alert,AlertPopup,AlertRulesService } from '@core/manage';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FunctionService } from '@core/_base/layout/services/function.service';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { DeviceService} from '@core/manage';
import * as moment from 'moment';
import * as FileSaver from 'file-saver';

declare var $: any;
@Component({
  selector: 'kt-alerts',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  @ViewChild('datePicker', { static: true }) 
  datePicker: DateRangePickerComponent;

  alerts: FormGroup;
  searchFormALert : FormGroup;
  selectdp: ElementRef;
  public dataTable: DataTable = new DataTable();
  public datePickerOptions: any = {
    size: 'md',
    select: 'from',
    ranges: 'from',
    optionDatePicker: {
      maxSpan: {
        days: 31
      }
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: true,
    startDate:moment().subtract(6, 'days'),
    endDate:moment() 
  };
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idAlertsEdit: number;
  public dataDefault: any = [{}];
  public idAlertsDelete: any;
  public listEventTypes : any = [];
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public dateStart: string;
  public dateEnd: string;
  public listDevices : any = [];
  public eventTypeConfigs: Array<{ name: string,id:number, template: any,group:string }>=[];
  // qcvn

  constructor(
    private alert: AlertService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private alertRules: AlertRulesService,
    private translate: TranslateService,
    private func: FunctionService,
    private deviceService: DeviceService,


  ) {
    this.eventTypeConfigs = this.alertRules.configTemplate;
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    // console.log(111);
    // this.alert.changeLog().pipe(
    //   tap((data: any) => {
    //     console.log(data);
    //   }),
    //   takeUntil(this.unsubscribe),
    //   finalize(() => {
    //     this.cdr.markForCheck();
    //   })
    // ).subscribe();

    this.updateDataTable();
    this.getData();
    this.loadListEventType();
    this.getDevices();
    this.refreshEventType();

    // $(function () {
    //   $('select').selectpicker();
    //   });
 }

 private loadListEventType()
 {
  if(this.func.checkLocalStorage('eventType'))
  {
    this.listEventTypes = this.func.getLocalStorage('eventType');
    
  }else{ this.getListEventType();}
  setTimeout(() => {
    $('.kt_selectpicker ').selectpicker('refresh');
  }); 
 }

 private refreshEventType()
 {
  this.translate.onLangChange.subscribe((event) => {
    let titleDevice = this.translate.instant('MANAGE.DEVICE.GENERAL.DEVICE_NAME');
    let title = this.translate.instant('MANAGE.ALERT_RULE.GENERAL.TYPE_ALERT');
    $(function () {
      $('.input-event').selectpicker({title: title}).selectpicker('refresh');
      $('.input-device').selectpicker({title: titleDevice}).selectpicker('refresh');

      
    });
  });
 }

  private buildForm(): void {
    this.alerts = this.formBuilder.group({
      nameAlerts: ['',Validators.required],
      nameKey: ['',Validators.required],
      limitSpeed : [''],
      qcvnCode: [''],
    });
    this.searchFormALert = this.formBuilder.group({
      nameALert: [''],
      nameDevice:[''],
      dateStart: [''],
      dateEnd: [''],
    })
  }

  buttonAddNew(content) {
    this.isEdit = false;
    this.alerts.reset();
    this.open(content);
    setTimeout(() => {
        $('.kt_selectpicker').selectpicker('refresh');
      });
  }
  
  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  getDevices() {
    let params = {
      pageNo : -1
    };
    // this.filter.pageNo = -1;
    this.deviceService.list({ params: params }).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      this.listDevices = data.result;
      setTimeout(() => {
        $('.kt_selectpicker').selectpicker('refresh');
      });

    });
  }
  
  private getListEventType(){
    this.alertRules.getEventType({ })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 200) {
        result.result.forEach(element => {
          this.listEventTypes = this.listEventTypes.concat(element.eventTypes);
          this.func.setLocalStorage('eventType',this.listEventTypes);
        });
  
      }
    }) 
  }
	public alertPopup: AlertPopup;

  async showAlert(content, id) {
		await this.alert.alertDetail(id).pipe(
			tap(data => {
				if (data.status == 200) {
          
					this.alertPopup = data.result;
				}
				this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal--fit kt-modal-clear', size: 'lg', backdrop: 'static' })
					.result.then((result) => {
						this.closeResult = `Closed with: ${result}`;

					}, (reason) => {
						this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
					});
			},
				error => {
					// this.toastService.show({ translate: error.messageCode, type: "warning" });
				}),

			finalize(() => this.cdr.markForCheck())
		).toPromise();
    // update is read
    this.markAsRead(id, 0);

	}

  resetFormSearch() {

    this.searchFormALert.reset();
    // this.filter.timeFrom = '',
    // this.filter.deviceIds = '',
    // this.filter.eventKeys = '',
    // this.filter.timeTo = '',
    this.buildPagination();
    this.getData();
    this.buildDateNow();
  }

  buildDateNow() {
    this.datePicker.refresh();
    setTimeout(function () {
      $('select').selectpicker('refresh');
    });
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
      timeFrom : moment().subtract(6, 'days').format("YYYY-MM-DD HH:mm:ss"),
      timeTo : moment().format("YYYY-MM-DD HH:mm:ss"),

    };
  }
 
  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
   }

  onHideModal(id: string) {
    let idModal = '#' + id;
    $(idModal).modal('hide');
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }

 
  getIdAction(id,idModal) {
    this.idAlertsDelete = id;
    this.openModal({idModal:idModal,confirm : true});
  }

  openModal(params) {
     let idModal = params.idModal;
     let confirm = params.confirm;
     if(confirm)
     {$(idModal).appendTo('body').modal({backdrop: 'static',keyboard: false})}
     else{$(idModal).appendTo('body').modal('show');}
  }

  deleteAlert() {
    let id = this.idAlertsDelete;
    this.alert.delete(id, { notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.getData();
      }
    });
    $('.modal').modal('hide');
  }

  searchALert(form){
    if(form.value.nameDevice != 'null' && form.value.nameDevice != null)
    {
      this.filter.deviceIds =  form.value.nameDevice.toString();    
    }
    if(form.value.nameALert != 'null' && form.value.nameALert != null)
    {
      this.filter.eventKeys =  form.value.nameALert.toString();    
    }
    this.filter.timeFrom = this.dateStart.toString();
    this.filter.timeTo = this.dateEnd.toString();

    this.getData();
  }


  editALert(id,content) {
    this.isEdit = true;
    this.idAlertsEdit = id;
  
    this.alert.detail(id).pipe(
      tap((data: any) => {
        let dataDefault = data.result;
        dataDefault.device_name = dataDefault.device.name;
        dataDefault.event_key = dataDefault.event.key;
        this.dataDefault = [dataDefault];
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
    this.open(content);
  }

  loadSelectBootstrap(){
		$('.bootstrap-select').selectpicker();
  }

  searchAlerts(form:any)
  {
    this.filter.name = form.value.nameAlerts;
    this.getData();
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter = option;
        this.getData();
      }),
      
    ).subscribe();
  }

  get f() {
   if(this.alerts != undefined) return this.alerts.controls;
  }

  downloadReport(item)
  {
    let url = item.reportUrl;    
    FileSaver.saveAs(url);
		this.markAsRead(item.id, 0);
    // window.open(url);
  }


  
	markAsRead(id:number,isRead:number)
	{
		let isReadUpdate = 1;
		if(isRead == 1)
		{
			isReadUpdate = 0;
		}
		let params = {
			id:id,
			isRead : isReadUpdate
		};
		
		this.alert.markAsRead(params)
		.subscribe((result: any) => {
		  if (result.status == 200) {
        this.getData();
		  }
		})
	}

  markAllRead()
	{
		this.alert.markAllRead({}) 
		.pipe(
			//takeUntil(this.unsubscribe),
			finalize(() => {
			  this.cdr.markForCheck();
			})
		  )
		.subscribe((result: any) => {
		  if (result.status == 200) {
        this.getData();
		  }
		})	
	}

  getData() {
    this.alert.getListAlert(this.filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          result.result.content.map(item =>{
            if(item.eventType == 'none')
            {
              item.deviceName = this.translate.instant("MENU.REPORT_GENERAL");
              item.eventKey   = "REPORT.REPORTS.GENERAL.REPORTS";
              item.message   = this.translate.instant("REPORT.REPORTS.MESSAGE.DONE");
            }
            return item;
          });
          this.data = result.result.content;
          
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
       
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect, 
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
				selecter:false,
			  }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
  }
  
  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '40px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Device name',
        field: 'Device name',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'MANAGE.ALERT.GENERAL.OBJECT',
        autoHide: false,
        width: 182,
      },
      {
        title: 'Event Type',
        field: 'eventType',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'MANAGE.ALERT.GENERAL.TYPE_ALERT',
        autoHide: true,
        width: 182,
      },
      {
        title: 'Message',
        field: 'message',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'MANAGE.ALERT.GENERAL.MESSAGE_CONTENT',
        autoHide: true,
        width: 182,
      },
      {
        title: 'Address',
        field: 'address',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'COMMON.COLUMN.ADDRESS',
        autoHide: true,
        width: 182,
      },
      {
        title: 'Status',
        field: 'status',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px','text-align':'center'},
        class: '',
        translate: 'COMMON.COLUMN.STATUS',
        autoHide: true,
        width: 182,
      },
      {
        title: 'Create by',
        field: 'createdBy',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'COMMON.COLUMN.CREATED_DATE',
        autoHide: true,
        width: 182,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 182,
      },
    ]
  }
  public getEventTypeGroups(){
    let result = [];
    this.eventTypeConfigs.forEach(x=>{
      if(!result.some(i=>i == x.group)) 
        result.push(x.group)
    });
    return result;
  }

  public getEventTypesByGroup(group?:string){
    if(group){
      return this.eventTypeConfigs.filter(x=>x.group == group);
    }else{
      return this.eventTypeConfigs;
    }
  }


}
