import { Component, OnInit, ChangeDetectorRef, NgModule, Input, Output, EventEmitter, ComponentFactoryResolver, Injector } from '@angular/core';
import * as L from 'leaflet';
import { MapConfigModel, MapConfig, MapUtil, MapService, Item } from '@core/utils/map';
import { PoiModel,PoiService } from '@core/manage';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { ValidatorCustomService, ToastService } from '@core/_base/layout';
import { TranslateService } from '@ngx-translate/core';
import { tap, takeUntil, finalize } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import { style } from '@angular/animations';
import "leaflet/dist/images/marker-shadow.png";
import { PoiType,PoiTypeService,ListIconPoiType } from '@core/admin';

declare var $;
@Component({
  selector: 'kt-poi-tool',
  templateUrl: './poi-tool.component.html',
  styleUrls: ['./poi-tool.component.scss'],
    providers: [ListIconPoiType]

})
export class PoiToolComponent implements OnInit {
  public mapConfig: MapConfigModel;

  @Input() listDevices: Array<any>;
  @Input() listTypePoi:Array<any>;
  @Input() poiInput?: PoiModel;
  @Input() currentUser: any;
  @Output() poiResult: EventEmitter<{ status: string, message: string, data: any }>;
  public poiInfo: PoiModel;
  public poiMap: PoiModel;
  public poiForm: FormGroup;
  public currentLayer: any;
  public map: L.Map;
  public latlngArr: FormArray;
  public isCricle: boolean;
  public myIcon = L.icon({
    iconUrl: './assets/img/marker-icon.png',
    iconSize: [25, 41],
    iconAnchor: [16, 45],
    // popupAnchor: [-3, -76],
    // shadowUrl: 'my-icon-shadow.png',
    // shadowSize: [68, 95],
    // shadowAnchor: [0, 0]
});
  public optionsLayer: {
    color: string,
    fill: boolean,
    fillColor: string,
    fillOpacity: number,
    opacity: number,
    stroke: boolean
    weight: number,
  }
  public editableLayers = new L.FeatureGroup();
  public options;
  public drawOptions = {
    position: 'topright',
    draw: {
      polyline: false,
      polygon:false,
      circle:false,
      circlemarker: false,
      rectangle:false,
      marker: {
        iconUrl:'./assets/img/marker-icon.png'
      },
    },
    edit: {
      // featureGroup: this.editableLayers,
      edit: false,
      remove: false,
    },
  };
  
  public layers = {};
  public typeAction: string;
  public listData: Array<any>;
  public layerDevice: L.FeatureGroup;
  public currentPopup: L.Layer;
  public showListDevice: boolean = false;
  private mapService: MapService;

  constructor(
    private listIconPoiType :ListIconPoiType,
    private cdr: ChangeDetectorRef,
    // private poiService: poiService,
    private mapUtil: MapUtil,
    private fb: FormBuilder,
    private validatorCT: ValidatorCustomService,
    private translate: TranslateService,
    private toast: ToastService,
    private resolver: ComponentFactoryResolver,
    private injector: Injector,
    private sanitizer: DomSanitizer,
    private poiTypeService: PoiTypeService,
    private poi:PoiService
    ) {
    this.typeAction = "add";
    this.mapService = new MapService(this.resolver,this.injector);

    this.options = this.mapService.init();
    this.isCricle = false;
    this.mapConfig = this.mapService.getConfigs();
    this.listData = [];
    this.poiResult = new EventEmitter();
    let roadmap = L.tileLayer(this.mapConfig.mapType.roadmap.layer['link'], this.mapConfig.mapType.roadmap.layer['option']);
    let satellite = L.tileLayer(this.mapConfig.mapType.satellite.layer['link'], this.mapConfig.mapType.satellite.layer['option']);

    this.layers[this.translate.instant(this.mapConfig.mapType.roadmap.translate)] = roadmap;
    this.layers[this.translate.instant(this.mapConfig.mapType.satellite.translate)] = satellite;

  }

  ngOnInit() {
    // this.getListPoiType();
    this.listData = this.processData(this.listDevices);
    this.mapService.config.options.elementProcessText = "mapProcessText";
    if (this.poiInput) {
      this.typeAction = "edit";
      this.poiInfo = new PoiModel(this.poiInput);
      // console.log(this.poiInfo);
      // console.log('vandinh');
      let points = this.mapUtil.decode(this.poiInfo.encodedPoints);
    }
    else {
      this.poiInfo = new PoiModel();
    }
    // this.poiInfo = new PoiModel();
    this.optionsLayer = {
      color: this.poiInfo.color,
      fill: this.poiInfo.fill,
      fillColor: this.poiInfo.fillColor,
      fillOpacity: this.poiInfo.fillOpacity,
      opacity: this.poiInfo.opacity,
      stroke: this.poiInfo.stroke,
      weight: this.poiInfo.weight,
    };

    this.createForm(this.poiInfo);
    this.poiInfo.userId = this.currentUser.id;
    // // let clientRectHeader = document.getElementById('poiHeader').getBoundingClientRect();     
    // let clientRectFooter = document.getElementById('poiFooter').getBoundingClientRect();
    // let clientRect = document.getElementById('poiConfig').getBoundingClientRect();
    // let maxHeightWapper = clientRect.height - (clientRectFooter.height);
    // $("#poiBody").css("max-height", maxHeightWapper);

    this.cdr.detectChanges();
  }

  getListPoiType(){
    this.poiTypeService.list({pageNo:-1}).subscribe(data=>{
      if(data.status == 200)
      {
        data.result.map(poiType=>{
          let name = "ADMIN.POI_TYPE.LANGUAGE."+poiType.name;
          poiType.name = name;
          let iconClass = this.listIconPoiType.getIconByKey(poiType.type);
          poiType.iconClass = iconClass;

          return poiType;
        })
        this.listTypePoi = data.result;
  
      }
    })
  }
  onDrawReady(drawControl) {
    // Do stuff with map     
  }

  

  onMapReady(map) {    
    this.map = map;
    L.control.layers(this.layers).addTo(this.map);
    this.mapService.setMap(this.map);
    this.renderpoi(this.poiInput, true);
  }
  createForm(poi: PoiModel) {
    
    // covert points
    poi.latlngs.map(x => {
      x.lat = parseFloat(parseFloat(x.lat.toString()).toFixed(5));
      x.lng = parseFloat(parseFloat(x.lng.toString()).toFixed(5));
      return x;
    });
    poi.points = poi.latlngs.map(x => {
      return [x.lat, x.lng]
    });
    if (poi.radius) poi.radius = parseFloat(parseFloat(poi.radius.toString()).toFixed(5));
    this.poiForm = this.fb.group({
      name: new FormControl({ value: poi.name, disabled: false }, [Validators.required, this.validatorCT.maxLength(64)]),
      typePoi: new FormControl({ value: poi.typePoi, disabled: false },[]),

      description: new FormControl({ value: poi.description, disabled: false }, [this.validatorCT.maxLength(256)]),
      radius: new FormControl({ value: poi.radius, disabled: false }, [this.validatorCT.float]),
      color: new FormControl({ value: poi.color, disabled: false }),
      opacity: new FormControl({ value: poi.opacity * 10, disabled: false }),
      fillColor: new FormControl({ value: poi.fillColor, disabled: false }),
      fillOpacity: new FormControl({ value: poi.fillOpacity * 10, disabled: false }),
      latlngArr: this.fb.array([])
    });
 
    setTimeout(()=>{
      $('#listPoiTypeTool').val(poi.poiType).selectpicker('refesh');
    });
    if (poi.latlngs.length > 0) {
      this.latlngArr = this.poiForm.get('latlngArr') as FormArray;
      this.latlngArr.clear();
      
      poi.latlngs.map(x => {
        this.latlngArr.push(this.createLatlng(x));
      });
    }
    this.poiInfo = Object.assign(this.poiInfo, poi);
    this.updateLayout();

  }
  getOptions(poi: PoiModel) {
    return {
      color: poi.color,
      fill: poi.fill,
      fillColor: poi.fillColor,
      fillOpacity: poi.fillOpacity,
      opacity: poi.opacity,
      stroke: poi.stroke,
      weight: poi.weight,
    };
  }
  renderpoi(poi: PoiModel, fitBound?: boolean) {
    let _this = this;
    if (poi) {
      this.isCricle = false;
      if (this.currentLayer) this.currentLayer.remove();
      switch (poi.type) {
        case "polygon":
          let polygon: any = new L.Polygon(poi.points);
          this.currentLayer = polygon;
          this.currentLayer.options = this.getOptions(poi);
          this.currentLayer.layerType = "polygon";
          this.currentLayer.editing.enable();
          this.currentLayer.on('edit', function (e) {
            _this.layerEdited(e.target);
          });

          poi.latlngs = poi.points.map(x => {
            return { lat: x[0], lng: x[1] };
          })
          // this.setLatlngForm(latlngs);
          this.createForm(poi);
          this.currentLayer.addTo(this.map);
          if (fitBound) {
            this.map.fitBounds(poi.points, { padding: [50, 50] });
            if (this.map.getZoom() > 1) {
              this.map.setZoom(this.map.getZoom() - 1);
            }
          }
          break;
        case "circle":
          this.isCricle = true;
          let circle: any = new L.Circle(poi.points[0], poi.radius);
          this.currentLayer = circle;
          this.currentLayer.options = this.getOptions(poi);
          this.currentLayer.layerType = "circle";
          this.currentLayer.editing.enable();
          this.currentLayer.on('edit', function (e) {
            _this.layerEdited(e.target);
          });
          poi.latlngs = poi.points.map(x => {
            return { lat: x[0], lng: x[1] };
          })
          this.createForm(poi);
          this.currentLayer.addTo(this.map);
          if (fitBound) {
            this.map.fitBounds(circle.getBounds(), { padding: [20, 20] });
            if (this.map.getZoom() > 1) {
              this.map.setZoom(this.map.getZoom() - 1);
            }
          }
          break;
          case "marker":            
  
            // let marker: any = new L.Marker(poi.points[0]).addTo(this.map);     
            let marker: any = L.marker(poi.points[0],{icon:this.myIcon}).addTo(this.map);
            this.currentLayer = marker;
            // this.currentLayer.options = this.getOptions(poi);
            this.currentLayer.layerType = "marker";
            this.currentLayer.editing.enable();
            
            this.currentLayer.on('moveend', function (e) {                            
              _this.layerEdited(e.target);
            });
           let latlngs = this.currentLayer.getLatLng();
           
            poi.latlngs = poi.points.map(x => {
              return { lat: x[0], lng: x[1] };
            })
            // // this.setLatlngForm(latlngs);
            this.createForm(poi);
            // console.log(this.currentLayer);
            if (fitBound) {            
              let _this= this;
              setTimeout(()=>{
                _this.map.fitBounds(poi.points,{padding:[50,50],maxZoom:15});

                // _this.map.fitBounds(poi.points, { padding: [100, 100] });
                // _this.map.setZoom(_this.map.getZoom() + 5);
              })
            }

            break;
      }

    }

  }
  onReset() {
    this.poiForm.reset();
  
    this.renderpoi(this.poiInput, true);
    setTimeout( () => {
      $('.kt_selectpicker').val('').selectpicker('refresh');
    });
  }


  renderLatlngs(latlngs, radius, type: string) {

    this.poiInfo.radius = radius || 0;
    this.poiInfo.type = type || "";
    
    this.poiInfo.points = latlngs.map(x => {
      return [x.lat, x.lng];
    })
    this.poiInfo.latlngs = latlngs;
    this.createForm(this.poiInfo);
    this.cdr.detectChanges();
  }
  setLatlngForm(latlngs: Array<{ lat: number, lng: number }>) {
    this.latlngArr = this.poiForm.get('latlngArr') as FormArray;
    this.latlngArr.clear();
    latlngs.map(x => {
      this.latlngArr.push(this.createLatlng(x));
    });
  }
  onCreated(layer) {
    layer.layer.editing.enable();
    
    this.editableLayers.addLayer(layer.layer);
    let _this = this;
    if (this.currentLayer) {
      this.currentLayer.remove();
    }
    this.currentLayer = layer.layer;
    let latlngs;
    let radius;
    let type;
    this.isCricle = false;
    switch (layer.layerType) {
      case "rectangle":
        let rectangle: L.Rectangle = layer.layer;
        latlngs = rectangle.getLatLngs()[0];
        type = "polygon";
        break;
      case "circle":
        let circle: L.Circle = layer.layer;
        latlngs = [circle.getLatLng()];
        radius = circle.getRadius();
        // circle.setStyle(this.optionsLayer);
        this.isCricle = true;
        type = "circle";
        break;
      case "polygon":
        let polygon: L.Polygon = layer.layer;
        latlngs = polygon.getLatLngs()[0];

        type = "polygon";
        break;
      case "marker":
          let marker: L.Marker = layer.layer;
          latlngs = marker.getLatLng();
          latlngs = [latlngs];
          type = "marker";
          break;
    }

    this.currentLayer['layerType'] = layer.layerType;
    // set options
    this.currentLayer.options = this.optionsLayer;
    this.currentLayer.on('edit', function (e) {
      _this.layerEdited(e.target);      
    });
    if(this.currentLayer['layerType'] == 'marker')
    {
      this.currentLayer.on('moveend', function (e) {
        // console.log(e);
        
        _this.layerEdited(e.target);      
      });
    }
    this.renderLatlngs(latlngs, radius, type);
  }

  layerEdited(layer) {

    let latlngs;
    let radius;
    this.isCricle = false;
    let type;
    switch (layer.layerType) {
      case "rectangle":
        let rectangle: L.Rectangle = layer;
        latlngs = rectangle.getLatLngs()[0];
        type = "polygon";
        break;
      case "circle":
        let circle: L.Circle = layer;
        latlngs = [circle.getLatLng()];
        radius = circle.getRadius();
        this.isCricle = true;
        type = "circle";
        break;
      case "polygon":
        let polygon: L.Polygon = layer;
        latlngs = polygon.getLatLngs()[0];
        type = "polygon";
        break;
      case "marker":
          let marker: L.Marker = layer;          
          latlngs = marker.getLatLng();          
          latlngs = [latlngs];
          type = "marker";
          break;
    }
    this.renderLatlngs(latlngs, radius, type);
  }
  onResize($elm) {
    this.mapService.resize();

    // let clientRectFooter = document.getElementById('poiFooter').getBoundingClientRect();
    // let clientRect = document.getElementById('poiConfig').getBoundingClientRect();
    // let maxHeightWapper = clientRect.height - (clientRectFooter.height);

    // setTimeout(() => {
    //   $("#poiBody").css("max-height", maxHeightWapper);
    // });

    this.cdr.detectChanges();
  }
  updateLayout() {
    this.mapService.resize();
    // let clientRectFooter = document.getElementById('poiFooter').getBoundingClientRect();
    // let clientRect = document.getElementById('poiConfig').getBoundingClientRect();
    // let maxHeightWapper = clientRect.height - (clientRectFooter.height);

    // setTimeout(() => {
    //   $("#poiBody").css("max-height", maxHeightWapper);
    // });

    this.cdr.detectChanges();
  }
  onSubmit() {
    
    // if(this.poiForm.value.typePoi == undefined || this.poiForm.value.typePoi == ''){
    //   this.toast.show({message:this.translate.instant('MANAGE.POI.MESSAGE.NOT_TYPE'),type:'error'});
    //   return;
    // }

    this.poiForm.markAllAsTouched();
    setTimeout(() => {
      const firstElementWithError = document.querySelector('#formpoiTool .invalid-feedback');
      if (firstElementWithError) {
        firstElementWithError.scrollIntoView({ block: 'center' });
      }
    });
    if(this.poiInfo.type.length == 0) this.toast.show({message:this.translate.instant('MANAGE.POI.MESSAGE.NOT_MARKET'),type:'error'});
    if (!this.poiForm.invalid && this.poiInfo.type.length > 0) {

      this.poiInfo.encodedPoints = this.mapUtil.encode(this.poiInfo.points);
      this.poiInfo.poiType = this.poiForm.value.typePoi;
      this.poiInfo.latitude = this.poiInfo.points[0][0];
      this.poiInfo.longitude = this.poiInfo.points[0][1];
      
      if (this.typeAction == "edit") {
        this.poi.update(this.poiInfo, { notifyGlobal: true }).pipe(
          tap(data => {
            if (data.status == 200) {
              this.poiResult.emit({ status: "success", message: "", data: data.result });
            }
          }),
          finalize(() => {
            this.cdr.markForCheck();
          })
        ).subscribe();
      }
      else {
        this.poi.create(this.poiInfo, { notifyGlobal: true }).pipe(
          tap(data => {
            if (data.status == 201) {
              this.poiResult.emit({ status: "success", message: "", data: data.result });
            }
          }),
          finalize(() => {
            this.cdr.markForCheck();
          })
        ).subscribe();
       }
    }
    else {
      
      // console.log(this.poiInfo);

      // console.log( this.poiInfo.points);
      // console.log( this.poiInfo.latlngs.map(x=>{ return JSON.stringify(x)}).join(","));

      // console.log(2);
    }
  }
  formChange(value) {
    let _this = this;
    this.poiForm.markAllAsTouched();

    if (this.poiForm.invalid && !this.poiForm.get('name').errors) return;
    this.poiInfo.name = this.poiForm.get('name').value;
    this.poiInfo.typePoi = this.poiForm.get('typePoi').value;

    this.poiInfo.description = this.poiForm.get('description').value;
    this.poiInfo.radius = Number.parseFloat(this.poiForm.get('radius').value);
    this.poiInfo.color = this.poiForm.get('color').value;
    this.poiInfo.opacity = this.poiForm.get('opacity').value / 10;
    this.poiInfo.fillColor = this.poiForm.get('fillColor').value;
    this.poiInfo.fillOpacity = this.poiForm.get('fillOpacity').value / 10;
    
    // set point
    if (this.formLatLngs.value.length > 0) {
      this.poiInfo.points = this.formLatLngs.value.map(x => {
        return [x.lat, x.lng];
      })
    }



    // set options
    this.optionsLayer = {
      color: this.poiInfo.color,
      fill: this.poiInfo.fill,
      fillColor: this.poiInfo.fillColor,
      fillOpacity: this.poiInfo.fillOpacity,
      opacity: this.poiInfo.opacity,
      stroke: this.poiInfo.stroke,
      weight: this.poiInfo.weight,
    };

    this.renderpoi(this.poiInfo, false);

    // if (this.currentLayer) {
    //   let layerTemp = this.currentLayer;
    //   this.currentLayer.remove();
    //   this.currentLayer = layerTemp;
    //   this.currentLayer.editing.enable();
    //   this.currentLayer.on('edit', function (e) {
    //     _this.layerEdited(e.target);
    //   });
    //   this.currentLayer.options = this.optionsLayer;
    //   this.currentLayer.addTo(this.map);
    // }

  }
  createLatlng(latlng: { lat: number, lng: number }): FormGroup {
    return this.fb.group({
      lat: [latlng.lat, this.validatorCT.float],
      lng: [latlng.lng, this.validatorCT.float]
    });
  }
  get formLatLngs(): FormArray {
    if (this.poiForm)
      return this.poiForm.get('latlngArr') as FormArray;
  }

  get f() {
    if (this.poiForm) return this.poiForm.controls;
  }
  onSetting($elm) {
    // let clientRectHeader = document.getElementById('poiHeader').getBoundingClientRect();
    // let clientRectFooter = document.getElementById('poiFooter').getBoundingClientRect();
    // let clientRect = document.getElementById('poiConfig').getBoundingClientRect();
    // let maxHeightWapper = clientRect.height - (clientRectFooter.height);
    // $("#poiBody").css("max-height", maxHeightWapper);
    this.cdr.detectChanges();
  }
  processData(data) {
    let _this = this;
    const icons = _this.mapConfig.icons;
    const status = this.mapConfig.status;
    let dataNew = data.map(x => {
      let indexIcon = 0;
      if (x['icon']['name']) {
        let indexIconSearch = _this.mapConfig.icons.findIndex(i => i.name == x['icon']['name']);
        if (indexIconSearch >= 0) {
          indexIcon = indexIconSearch;
        }
      }
      x.iconType = Object.assign({}, icons[indexIcon]);
      x.iconTypeProcess = Object.assign({}, icons[indexIcon]);

      if (x['lat'] == "null") x['lat'] = null;
      if (x['lng'] == "null") x['lng'] = null;

      x.iconTypeProcess.icon = x.iconTypeProcess.icon
        .replace('viewBox="0 0 34 34"', 'viewBox="0 0 24 24"')
        .replace('viewBox="0 0 48 48"', 'viewBox="0 0 38 38"')
        .replace('transform="rotate({iconRotate})"', "")
        .replace('{iconFill}', "#fff");
      x.iconTypeProcess.icon =
        _this.sanitizer.bypassSecurityTrustHtml(x.iconTypeProcess.icon);

      switch (x.status) {
        case "lost_gps":
          x.statusType = status.lostGPS;
          break;
        case "lost_gprs":
          x.statusType = status.lostGPRS; break;
        case "history_transfer":
          x.statusType = status.historyTransfer;
          break;
        case "expired":
          x.statusType = status.expired;
          break;
        case "stop":
          x.statusType = status.stop;
          break;
        case "run":
          x.statusType = status.run;
          break;
        case "inactive":
          x.statusType = status.inactive;
          break;
        case "nodata":
          x.statusType = status.nodata;
          break;
        default:
          x.statusType = status.lostSignal;
          break;
      }

      let item = new Item(x);
      return item;
    });
    return dataNew;
  }

  // public deviceMarkers: Array<L.Layer> = [];
  onWidgetCheckChange(data) {
    if (this.layerDevice) this.layerDevice.remove();


    if (data.listChecked.length > 0) {
      this.layerDevice = new L.FeatureGroup(); data.listChecked.map(x => {
        let marker = this.mapService.createDevice(x, this.currentPopup, this.map, "style-one");
        if(marker)this.layerDevice.addLayer(marker);
      });
      this.layerDevice.addTo(this.map);
      this.map.fitBounds(this.layerDevice.getBounds(), { padding: [50, 50] });
    }

  }
  changeShowListDevice() {
    this.showListDevice = !this.showListDevice;
    this.cdr.detectChanges();
  }

}
