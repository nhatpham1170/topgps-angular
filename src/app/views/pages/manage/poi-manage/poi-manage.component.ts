import { Component, OnInit, ElementRef, ChangeDetectorRef, ViewChild, EventEmitter } from '@angular/core';
import { finalize, takeUntil, tap, delay } from 'rxjs/operators';
import { Subject, observable } from 'rxjs';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { Device, DeviceTypeService } from '@core/admin';
import { Device, DeviceService, User, GeofenceService, PoiModel, PoiService } from '@core/manage';

import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { DataTable } from '@core/_base/layout/models/datatable.model';

import { TranslateService } from '@ngx-translate/core';
import { ToastService, UserDatePipe } from '@core/_base/layout';
import { DeviceUtilityService } from '@core/manage/utils/device-utility.service';
import { ExcelService } from '@core/utils';
import { RequestFileService } from '@core/common/_services/request-file.service';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { ReportDeviceService } from '@core/report';
import { MapConfig, MapConfigModel } from '@core/utils/map';
import { DecimalPipe } from '@angular/common';
import { TrackingService } from '@core/map';
import { PoiType,PoiTypeService,ListIconPoiType } from '@core/admin';

declare var $: any;
declare var ClipboardJS: any;
const TITLE_FORM_EDIT: string = "COMMON.ACTIONS.EDIT";
const TITLE_FORM_ADD: string = "COMMON.ACTIONS.ADD";
const TITLE_FORM_DELETE: string = "COMMON.ACTIONS.DELETE";

@Component({
  selector: 'kt-poi-manage',
  templateUrl: './poi-manage.component.html',
  styleUrls: ['./poi-manage.component.scss'],
  providers: [UserDatePipe, DecimalPipe,ListIconPoiType]
})
export class PoiManageComponent implements OnInit {
  public dataTable: DataTable = new DataTable();
  public action: string;
  public currentReason: any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public formEdit: FormGroup;
  public formAdd: FormGroup;
  private tableConfig: any;
  private currentForm: FormGroup;
  public currentModel: PoiModel;
  public commandsForm: FormGroup;
  public isShowSearchAdvanced: boolean;
  public deviceType: any[];
  public simType: any[];
  public groupDevice: any[];
  public centerMap = { latlng: { lat: 21.062078, lng: 105.824428 }, zoom: 11 };
  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  private userIdSelecte: number;
  public currentUser: User;
  public dateTimeServer: string;
  // feature device  
  public showUserTree = true;
  public allowReOrder: boolean = false;
  public titlePopup: string;
  public geofenceEdit: PoiModel;
  // permisison 
  public permissions: any = {
    sell: "device.action.sell"
  }
  public mapConfig: MapConfigModel;
  public statusExport: {
    all: number, lostGPS: number, lostGPRS: number, historyTransfer: number, expired: number, stop: number,
    run: number, inactive: number, nodata: number, lostSignal: number
  };
  public listTypePoi = [];
  public listDevices: Array<Device> = [];
  private lastUserId: number = 0;
  constructor(
    private listIconPoiType :ListIconPoiType,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private translate: TranslateService,
    private toast: ToastService,
    private deviceUtility: DeviceUtilityService,
    private userTreeService: UserTreeService,
    private trackingService: TrackingService,
    private poiService: PoiService,
    private poiTypeService: PoiTypeService,

  ) {
    this.getListPoiType();
    this.action = "";
    this.tableConfig = {
      pagination: [
        10, 20, 30, 50
      ],
      columns: [
        {
          title: '#',
          field: 'id',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '20px' },
          width: 20,
          class: 't-datatable__cell--center',
          translate: '#',
          autoHide: false,
        },
        {
          title: 'Name',
          field: 'name',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '200px' },
          width: 200,
          class: '',
          translate: 'COMMON.COLUMN.NAME',
          autoHide: true,
        },
        {
          title: 'Loai',
          field: 'type',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '150px' },
          width: 150,
          class: '',
          translate: 'COMMON.COLUMN.TYPE',
          autoHide: true,
        },
        {
          title: 'description',
          field: 'description',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '300px' },
          class: '',
          width: 300,
          translate: 'COMMON.COLUMN.DESCRIPTION',
          autoHide: true,
        },
        {
          title: 'Create at',
          field: 'createdAt',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px' },
          class: '',
          width: 100,
          translate: 'COMMON.COLUMN.CREATED_DATE',
          autoHide: true,
        },
        {
          title: 'Trạng thái',
          field: 'active',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '70px' },
          class: '',
          width: 70,
          translate: 'COMMON.COLUMN.STATUS',
          autoHide: true,
        },
        {
          title: 'actions',
          field: 'actions',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '117px' },
          class: '',
          width: 80,
          translate: 'COMMON.COLUMN.ACTIONS',
          autoHide: true,
        },
      ]
    };
    this.unsubscribe = new Subject();
    this.mapConfig = new MapConfig().configs();
    this.dataTable.init({
      data: [],
      totalRecod: 0,
      paginationSelect: [10, 20, 30,40, 50],
      columns: this.tableConfig.columns,
      formSearch: '#formSearch',
      pageSize: 10,
      // isDebug: true,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        responsive:false
        // selecter: true,
      }
    });
    this.currentReason = {};
    this.isShowSearchAdvanced = false;
    this.deviceType = [];
    this.simType = [];
    this.groupDevice = [];
    this.parrentEmiter = new EventEmitter();
    this.currentUser = new User();
    this.dateTimeServer = "";
  }
  ngOnInit() {
    // listening dataTable event
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.getData(option);
      })
    ).subscribe();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();

    // this.dataTable.reload({});

    // this.createFormEdit();
    $(function () {
      $('.kt_datepicker').datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: "bottom left",
        templates: {
          leftArrow: '<i class="la la-angle-left"></i>',
          rightArrow: '<i class="la la-angle-right"></i>'
        },
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true
      });
    });
  }

  getListPoiType(){
    this.poiTypeService.list({pageNo:-1},).subscribe(data=>{
      if(data.status == 200)
      {
        data.result.map(poiType=>{
          let name = "ADMIN.POI_TYPE.LANGUAGE."+poiType.name;
          poiType.name = name;
          let iconClass = this.listIconPoiType.getIconByKey(poiType.type);
          poiType.iconClass = iconClass;
          return poiType;
        })
        this.listTypePoi = data.result;
        setTimeout(()=>{
          $('.kt_selectpicker').selectpicker();
        })
      }
    })
  }
	/**
	 * Get list by data option
	 * @param option option search list
	 */
  private getData(option) {
    // if (option['orderBy'] == undefined || option['orderBy'].length == 0) {
    //   option['orderBy'] = "sortOrder";
    //   option['orderType'] = "asc";
    // }
    this.poiService.list({ params: option }).pipe(
      delay(300),
      tap((data: any) => {
        this.dateTimeServer = data.datetime;
        // data.result.content = this.processDevice(data.result.content, this.dateTimeServer);
        data.result.content.map(item=>{
          let key = item.typeName;
          let iconClass = this.listIconPoiType.getIconByKey(key);
          let name = "ADMIN.POI_TYPE.LANGUAGE."+item.poiTypeName;
          item.poiTypeName = name;
          item.iconClass = iconClass;
          return item;
        })
      
        this.dataTable.update({
          data: data.result.content,
          totalRecod: data.result.totalRecord,
          
        });
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
        this.cdr.detectChanges();
      })
    ).subscribe();
  }

  getDataSelected() {
    if (this.dataTable.dataSelected.length == 0) {
      this.toast.show({ translate: "MANAGE.DEVICE.MESSAGE.NO_DEVICE_SELECTED" });
    }
    return this.dataTable.dataSelected;
  }

  get formArr(): FormArray {
    if (this.commandsForm) {
      return this.commandsForm.get('itemRows') as FormArray;
    }
  }

  toggleSearchAdvanced() {
    this.isShowSearchAdvanced = !this.isShowSearchAdvanced;
  }

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.currentForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
  /**
   * Change size databale
   * @param $elm 
   */
  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  search(data?: any) {

    let listSearch = [];
    if (this.userIdSelecte != undefined) {
      listSearch.push({ key: 'userId', value: this.userIdSelecte });
    }

    this.dataTable.search(data || listSearch);
  }
  resetFormSearch() {
    $('#listPoiType').val('').selectpicker("refresh");
    $('#namePoi').val('');
    this.search([{ key: 'userId', value: this.userIdSelecte }, { key: 'orderBy', value: 'sortOrder' }, { key: 'orderType', value: 'asc' }]);
  }


  getGroupDevice(userId: number) {

  }
  userTreeChangeUser(value) {

    if (value.id > 0) {
      this.userIdSelecte = value.id;
      this.currentUser = value;
      setTimeout(()=>{
        $('#listPoiType').val('').selectpicker("refresh");
      })
      this.search();
    }
  }
  sendEvent() {
    this.parrentEmiter.emit({ id: 5024, path: '234,5305,235' });
  }
  copied(val) {
    this.toast.copied(val);
  }

  // feature export excel file
  open(content, type, item?: any) {
    
    this.action = type;

    switch (type) {
      case 'edit':
        this.editFnc(item, content);
        break;
      case 'add':
        this.addFnc(content);
        break;
      case 'delete':
        this.deleteFnc(item.id, content);
        break;
    }
  }
  async editFnc(item, content) {
    this.titlePopup = TITLE_FORM_EDIT;
    this.currentForm = this.formEdit; 
    item.latlngs = [
      {
        lat:item.latitude,
        lng:item.longitude
      }
    ];
    item.points = [
      [item.latitude,item.longitude]
    ];
    item.type = "marker";
    await this.getListDevice(this.userIdSelecte);
    // check and get listDevices
    this.geofenceEdit = new PoiModel(item);
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal-xl kt-modal--fit', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
    // let id = 61;
    // this.geofenceService.detail(id).pipe(
    //   tap(body => {
    //     this.geofenceEdit = new PoiModel(body.result);
    //     this.geofenceEdit = new PoiModel(item);
    //     this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal-xl kt-modal--fit', backdrop: 'static' }).result.then((result) => {
    //       this.closeResult = `Closed with: ${result}`;
    //     }, (reason) => {
    //       this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    //       this.currentReason = reason;
    //     });
    //   }),
    //   takeUntil(this.unsubscribe),
    //   finalize(() => {
    //     this.cdr.markForCheck();
    //   })
    // ).subscribe();
  }

  async addFnc(content) {
    this.titlePopup = TITLE_FORM_ADD;
    await this.getListDevice(this.userIdSelecte);
    // check and get listDevices
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal-xl kt-modal--fit', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }
  deleteFnc(id: number, content) {
    this.titlePopup = TITLE_FORM_DELETE;
    this.currentModel = new PoiModel();
    this.currentModel.id = id;
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', size: 'sm', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }
  onSubmit() {
    switch (this.action) {
      case "edit":
        if (!this.currentForm.invalid) {
          this.editAction();
        }
        break;
      case "add":
        if (!this.currentForm.invalid) {
          this.addAction();
        }
        break;
      case "delete":
        this.deleteAction();
        break;
    }
  }
  addAction() {
    this.modalService.dismissAll();

  }

  editAction() {
    this.modalService.dismissAll();

  }

  deleteAction() {
    this.poiService.delete(this.currentModel.id, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.modalService.dismissAll(this.currentReason);
          this.dataTable.reload({});
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  changeActive(item: PoiModel) {
    this.poiService.changeActive({ id: item.id, active: (item.active == 1 ? 0 : 1),description:item.description }, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.dataTable.reload({});
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }
  geofenceResult(data) {
    // console.log(data);
    
    if (data.status == "success") {
      this.modalService.dismissAll(this.currentReason);
      this.dataTable.reload({});
    }
  }
  async getListDevice(userId: number) {
    if (userId != this.lastUserId) {
      this.lastUserId = userId;
     await this.trackingService.list({ params: { groupId: -1, userId: this.currentUser.id } }).pipe(
        tap(data => {
          this.listDevices = data.result;
        }),
        finalize(() => {
          this.cdr.markForCheck();
          this.cdr.detectChanges();
        })
      ).toPromise();
    }
    else {
      return this.listDevices;
    }


  }
  /**
* Dismiss Reason Popup
* @param reason 
*/
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}


