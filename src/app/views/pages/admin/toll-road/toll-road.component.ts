import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { finalize, takeUntil, tap ,debounceTime} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { TranslateService } from '@ngx-translate/core';
import { tollRoad,TollRoadService,GeoService } from '@core/admin';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {CdkDragDrop, moveItemInArray,transferArrayItem} from '@angular/cdk/drag-drop';
import { AsciiPipe, UserDateAdvPipe } from '@core/_base/layout';

declare var $: any;
@Component({
  selector: 'kt-toll-road',
  templateUrl: './toll-road.component.html',
  styleUrls: ['./toll-road.component.scss'],
  providers: [AsciiPipe,UserDateAdvPipe]

})
export class TollRoadComponent implements OnInit {
  formroadType: FormGroup;
  searchFormroadType : FormGroup;
  selectdp: ElementRef;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idroadTypeEdit: number;
  public dataDefault: any = [{}];
  public idroadTypeDelete: any;
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public listCountry:any;
  public listType:any;
  // qcvn
 
  constructor(
    private road: TollRoadService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private geo : GeoService,
    private asciiPipe:AsciiPipe,
    private translate: TranslateService,
    private userDateAdv : UserDateAdvPipe,

  ) {
    this.unsubscribe = new Subject();
    this.getListCountry();

    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.getListType();
    this.buildForm();
    this.refreshEventType();

  }

  ngOnInit() {
    this.updateDataTable();
    this.dataTable.reload({ currentPage: 1 });

 }

  private buildForm(): void {
    this.formroadType = this.formBuilder.group({
        name: ['',Validators.required],
        type: ['',Validators.required],
        countryId: ['',Validators.required],
      });
    this.searchFormroadType = this.formBuilder.group({
      name: [''],
      typeSearch:[''],
      countryIdSearch:['']
    })
  }

  buttonAddNew(content) {
    this.isEdit = false;
    // this.formroadType.reset();
    this.open(content);
    this.formroadType = this.formBuilder.group({
        name: ['',Validators.required],
        type: [0,Validators.required],
        countryId: [237,Validators.required],
      });
    setTimeout( () => {
        $('select').selectpicker();
      });
  }
  
  open(content) {

    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  getListType(){

    setTimeout( () => {
        this.listType = [
            {id:0,name:'high way'},
            {id:1,name:'national road'},
            {id:2,name:'province road'},
        ];
        $('#listType').selectpicker('refresh');
      });
  }
  
 async getListCountry()
  {
    let name="country";    
    if(localStorage.getItem(name) == 'undefined' || localStorage.getItem(name) == null)
    {
        await  this.geo.listCountry()
        .pipe(
            takeUntil(this.unsubscribe),
            finalize(() => {
              this.cdr.markForCheck();
            })
          )
        .subscribe(data=>{
            if(data.status == 200) this.listCountry = data.result;
            this.listCountry.map(country=>{
              country.textSearch = this.asciiPipe.transform(country.name || "");
              return country;
            })
            localStorage.setItem(name,JSON.stringify(this.listCountry));

            setTimeout(() => {
                $('#countrySearch').val(237).selectpicker('refresh'); 
            });
          })

    }else{
      this.listCountry = localStorage.getItem(name);
      this.listCountry = JSON.parse(this.listCountry);
      setTimeout(() => {
        $('#countrySearch').val(237).selectpicker('refresh'); 
       });
    } 
  }

  onSubmitTollStation(form: any) {
    if (this.formroadType.invalid) {
      return;
    }
    let roadType = {
      name: form.value.name,
      type: form.value.type,
      countryId: form.value.countryId,
    } as tollRoad; 
    if(this.isEdit) {
      roadType.id = this.idroadTypeEdit;      
      this.road.update(roadType,{ notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          this.getData();
          this.modalService.dismissAll();
        }
      });
      return;
    }
    this.road.create(roadType,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        this.getData();
        this.modalService.dismissAll();

      }
    });
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
      orderBy:'sortOrder',
      orderType: 'ASC'
    };
  }

  onHideModal(id: string) {
    let idModal = '#' + id;
    $(idModal).modal('hide');
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }


  getIdAction(id,idModal) {
    this.idroadTypeDelete = id;
    this.modalService.open(idModal, { windowClass: 'kt-mt-50  modal-holder modal-delete',backdrop:'static'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openModal(params) {
     let idModal = params.idModal;
     let confirm = params.confirm;
     if(confirm)
     {$(idModal).appendTo('body').modal({backdrop: 'static',keyboard: false})}
     else{$(idModal).appendTo('body').modal('show');}
  }

  deleteTollRoad() {
    let id = this.idroadTypeDelete;
    this.road.delete(id, { notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.getData();
        this.modalService.dismissAll();
      }
    });
    
  }

  getNameCountry(id)
  {
      return this.listCountry.find(x=>x.id == id).name;
  }

  editTollRoad(item,content) {
 
    this.isEdit = true;
    this.idroadTypeEdit = item.id;
    setTimeout(()=>{
        $('select').selectpicker('refresh');
      });
 
    // this.road.list(params).pipe(
    //   tap((data: any) => {
    //     this.dataDefault = data.result.content;
    //     setTimeout(() => {
    //         $('.kt_selectpicker').selectpicker('refresh');
    //       }, 100);
    //   }),
    //   takeUntil(this.unsubscribe),
    //   finalize(() => {
    //     this.cdr.markForCheck();
    //   })
    // ).subscribe();
    item.createdAt = this.userDateAdv.transform(item.createdAt,'datetime');
    this.open(content);
    this.formroadType = this.formBuilder.group({
        name: new FormControl({ value: item.name, disabled: false },Validators.required),
        type: new FormControl({ value: item.type, disabled: false },Validators.required),
        countryId: new FormControl({ value: item.countryId, disabled: false },Validators.required),
        createAt: new FormControl({ value: item.createdAt, disabled: true }),
      });
  
  }

  loadSelectBootstrap(){
		$('.bootstrap-select').selectpicker();
  }

  private refreshEventType()
  {
     this.translate.onLangChange.subscribe((event) => {
     let country = this.translate.instant('ADMIN.TOLL_STATION.COLUMN.SELECT_COUNTRY');
     let type = this.translate.instant('MANAGE.POI.GENERAL.TYPE_POI');
     
     $(function () {
       $('#listType').selectpicker({title: type}).selectpicker('refresh');
       $('#countrySearch').selectpicker({title: country}).selectpicker('refresh');

     });
   });
  }

  searchTollRoad(form:any)
  {
    if(form.value.name == null ||  form.value.name == '')  form.value.name = '';
    if(form.value.typeSearch == null ||  form.value.typeSearch == '')  form.value.typeSearch = '';
    if(form.value.countryIdSearch == null ||  form.value.countryIdSearch == '') form.value.countryIdSearch = '';
    this.filter.name = form.value.name;
    this.filter.type = form.value.typeSearch;
    this.filter.countryId = form.value.countryIdSearch;
    
    this.dataTable.reload({ currentPage: 1 });
  }

  resetFormSearch()
  {
    this.searchFormroadType.reset();
    this.buildPagination();
    $('select').selectpicker("refresh");
    this.dataTable.reload({ currentPage: 1 });

  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;  
        this.getData();
      }),
      
    ).subscribe();
  }

  get f() {
   if(this.formroadType != undefined) return this.formroadType.controls;
  }

  getData() {
    this.road.list(this.filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          result.result.content.map(toll=>{
            toll.keyName = "ADMIN.TOLL_ROAD.COLUMN."+toll.typeName;
            return toll;
          });
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          // $(function () {
          //   $('select').selectpicker();
          // });
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect, 
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
                selecter:false,
                responsive:false
              }
              
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
  }
  
  drop(event: CdkDragDrop<any[]>) {

    let data:any = [];
    moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    data = event.container.data.map((item,i)=>{
     
      let obj = {
        id:item.id,
        sortOrder:i
      };
      return obj;
    });
     this.road.sortOrder(data, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.cdr.detectChanges();
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(this.cdr.markForCheck)
    ).toPromise();   
  }

  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '40px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Name',
        field: 'name',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'COMMON.COLUMN.NAME',
        autoHide: false,
        width: 150,
      },
      {
        title: 'Type',
        field: 'limitspeed',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'COMMON.COLUMN.TYPE',
        autoHide: true,
        width: 150,
      },
      {
        title: 'Country',
        field: 'qcvnCode',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'ADMIN.TOLL_STATION.COLUMN.COUNTRY',
        autoHide: true,
        width: 150,
      },
      {
        title: 'Create by',
        field: 'createdBy',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'COMMON.COLUMN.CREATED_DATE',
        autoHide: true,
        width: 150,
      },
  
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 117,
      },
    ]
  }


}
