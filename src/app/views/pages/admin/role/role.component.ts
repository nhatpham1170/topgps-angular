import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AuthService, Role } from '@core/auth';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

declare var $: any;
@Component({
  selector: 'kt-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {
  formRole: FormGroup;
  searchFormRole: FormGroup;

  selectdp: ElementRef;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idRoleEdit: number;
  public dataDefault: any = [{}];
  public idRoleDelete: any;
  public listNameGroupObj = [];
  public listNameGroup = [];
  public nameGroupEdit = { name: '' };
  public filter: any;
  public checked: boolean = true;
  private unsubscribe: Subject<any>;
  public listPermission = [];
  public listParentRoles = [];
  public masterSelected: boolean;
  private listCheckedPermission = [];
  public closeResult: string;

  constructor(
    private auth: AuthService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private translateService: TranslateService

  ) {
    this.unsubscribe = new Subject();
    this.buildForm();

    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    this.getListRoleParent();
    this.updateDataTable();
    this.getData();
  }

  //gán permission group và list permission vào formArray 
  addIdToFormArray() {
    this.formRole.controls.permissions = new FormArray([]);
    for (var i = 0; i < this.listNameGroup.length; i++) {
      let group = this.listNameGroup[i].permissionGroup;
      let permissions = this.listNameGroup[i].permissons;
      this.formRole['controls'][group] = new FormControl();
      for (let index = 0; index < permissions.length; index++) {
        let id = 'id' + permissions[index].id;
        this.formRole.controls.permissions['controls'][id] = new FormControl();
      }
    }
    // console.log(this.formRole.controls);
  }

  // lấy list permission mặc định false
  getAllPermissions() {
    let id = 0;
    this.auth.getRoleById(id).pipe(
      tap((data: any) => {

        data.result.permissions = data.result.permissions.map((group) => {
          let groupTemp = `PERMISSIONS.GENERAL.${group.permissionGroup}`.toUpperCase();
          group.groupTranslate = (this.translateService.instant(groupTemp) === groupTemp ?
            group.permissionGroup : this.translateService.instant(groupTemp));
          group.permissons = group.permissons.map((permission) => {
            let temp = `PERMISSIONS.${permission.permissionName}`.toUpperCase();
            permission.nameTranslate = (this.translateService.instant(temp) === temp ?
              permission.permissionName : this.translateService.instant(temp));
            return permission;
          })
          return group;
        });
        this.dataDefault = [data.result];
        this.listNameGroup = data.result.permissions;

        this.addIdToFormArray();
        setTimeout(() => {
          $('.kt_selectpicker').selectpicker();
        }, 300);
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  // Tích chọn tất cả theo nhóm permission
  checkUncheckAll(group) {
    for (var i = 0; i < this.listNameGroup.length; i++) {
      if (this.listNameGroup[i].permissionGroup == group) {
        let groupChecked = this.listNameGroup[i].checked;
        let listPermissions = this.listNameGroup[i].permissons;
        for (var j = 0; j < listPermissions.length; j++) {
          listPermissions[j].checked = groupChecked;
        }
        break;
      }
    }
  }

  //Tích chọn từng permission
  isCheckSelected(group = '') {
    for (var i = 0; i < this.listNameGroup.length; i++) {
      if (group == '') {
        let listPermissions = this.listNameGroup[i].permissons;
        this.listNameGroup[i].checked = listPermissions.every(function (item: any) {
          return item.checked == true;
        });
      }
      if (this.listNameGroup[i].permissionGroup == group) {
        let listPermissions = this.listNameGroup[i].permissons;
        this.listNameGroup[i].checked = listPermissions.every(function (item: any) {
          return item.checked == true;
        });
        break;
      }
    }
  }

  //lấy tất cả giá trị permisson đã chọn
  getIsSelectedItemList() {
    this.listCheckedPermission = [];
    for (var i = 0; i < this.listNameGroup.length; i++) {
      let permissons = this.listNameGroup[i].permissons;
      for (var index = 0; index < permissons.length; index++) {
        if (permissons[index].checked) {
          this.listCheckedPermission.push(permissons[index].id);
        }
      }
    }
  }

  private buildForm(): void {
    this.formRole = this.formBuilder.group({
      name: ['', [Validators.required]],
      nameParent: ['', Validators.required],
      description: [''],
      group_user: ['']
    });
    this.searchFormRole = this.formBuilder.group({
      name: [''],
      namegroup: ['']
    })
  }

  buttonAddNew(content) {
    this.isEdit = false;
    this.getAllPermissions();
    this.formRole.reset();
    this.open(content);
  }

  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder', size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  onSubmitRole(form: any) {
    if (this.formRole.invalid) {
      return;
    }
    this.getIsSelectedItemList();
    let params = {
      "name": form.value.name,
      "parentId": form.value.nameParent,
      "description": form.value.description,
      "listPermission": this.listCheckedPermission
    } as Role;
    if (this.isEdit) {
      params.id = this.idRoleEdit;
      this.auth.updateRoleById(params, { notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          this.getData();
          this.closeModal();
        }
      });
      return;
    }
    this.auth.addRole(params, { notifyGlobal: true })
      .pipe(
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 201) {
          this.getData();
          this.closeModal();

        }

      });
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10
    };
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  onHideModal(id: string) {
    let idModal = '#' + id;
    $(idModal).modal('hide');
  }

  selectEvent(item) {
    this.nameGroupEdit = item;
  }

  onChangeSearch(val: string) {
    this.nameGroupEdit = {
      name: val
    };
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  getIdAction(id, idModal) {
    this.idRoleDelete = id;
    this.openModal({ idModal: idModal, confirm: true });
  }

  openModal(params) {
    let idModal = params.idModal;
    let confirm = params.confirm;
    if (confirm) {
      $(idModal).appendTo('body').modal({ backdrop: 'static', keyboard: false })
    } else { $(idModal).appendTo('body').modal('show'); }
  }

  deleteRole() {
    let id = this.idRoleDelete;
    this.auth.deleteRoleById(id, { notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.getData();
      }
    });
    $('.modal').modal('hide');

  }

  editRole(id, content) {
    this.isEdit = true;
    this.idRoleEdit = id;
    this.auth.getRoleById(id).pipe(
      tap((data: any) => {
        setTimeout(() => {
          $('.kt_selectpicker ').selectpicker();
        }, 100);
        data.result.permissions = data.result.permissions.map((group) => {
          let groupTemp = `PERMISSIONS.GENERAL.${group.permissionGroup}`.toUpperCase();
          group.groupTranslate = (this.translateService.instant(groupTemp) === groupTemp ?
            group.permissionGroup : this.translateService.instant(groupTemp));
          group.permissons = group.permissons.map((permission) => {
            let temp = `PERMISSIONS.${permission.permissionName}`.toUpperCase();
            permission.nameTranslate = (this.translateService.instant(temp) === temp ?
              permission.permissionName : this.translateService.instant(temp));
            return permission;
          })
          return group;
        });
        this.dataDefault = [data.result];
        this.listNameGroup = data.result.permissions;
        this.addIdToFormArray();
        this.isCheckSelected();

      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
    this.open(content);
  }

  loadSelectBootstrap() {
    $('.bootstrap-select').selectpicker();
  }

  convertToObj(data) {
    for (var i = 0; i < data.length; i++) {
      let obj = {
        name: data[i]
      }
      this.listNameGroupObj.push(obj);
    }
  }

  getListRoleParent() {
    let params = {
      pageNo: -1
    }
    this.auth.getListRoles(params)
      .pipe(
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.listParentRoles = result.result;
          setTimeout(() => {
            $('.kt_selectpicker ').selectpicker();
          }, 300);
        }
      })
  }

  searchRole(form: any) {
    this.filter.name = form.value.name;
    this.filter.groupName = form.value.namegroup;
    this.getData();
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter = option;
        this.getData();
      }),
    ).subscribe();
  }

  get f() {
    if (this.formRole != undefined) return this.formRole.controls;
  }

  getData() {
    this.auth.searchRoles(this.filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          $(function () {
            $('.kt_selectpicker').selectpicker();
          });
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
      }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();

  }

  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '40px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Name',
        field: 'name',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'ADMIN.ROLE.GENERAL.NAME',
        autoHide: false,
        width: 182,
      },
      {
        title: 'Name Parent',
        field: 'parentName',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'ADMIN.ROLE.GENERAL.NAME_PARENT',
        autoHide: false,
        width: 182,

      },
      {
        title: 'Description',
        field: 'description',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'ADMIN.ROLE.GENERAL.DESCRIPTION',
        autoHide: true,
        width: 182,

      },
      {
        title: 'Create At',
        field: 'createdAt',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'COMMON.CREATED_DATE',
        autoHide: true,
        width: 182,


      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 182,
      },
    ]
  }


}
