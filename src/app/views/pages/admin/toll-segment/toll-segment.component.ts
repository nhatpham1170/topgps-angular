import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef,EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { finalize, takeUntil, tap ,debounceTime} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { TranslateService } from '@ngx-translate/core';
import { tollSegment,TollsegmentService,TollRoadService, TollStationService, TransportTypeService, GeoService } from '@core/admin';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {CdkDragDrop, moveItemInArray,transferArrayItem} from '@angular/cdk/drag-drop';
import { AsciiPipe, ToastService } from '@core/_base/layout';
import { async } from 'q';

declare var $: any;
@Component({
  selector: 'kt-toll-segment',
  templateUrl: './toll-segment.component.html',
  styleUrls: ['./toll-segment.component.scss'], 
  providers: [AsciiPipe]

})
export class TollSegmentComponent implements OnInit {
   
  paramsCurrency: FormGroup;
  formSegmentType: FormGroup;
  searchFormSegmentType : FormGroup;
  selectdp: ElementRef;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idsegmentTypeEdit: number;
  public dataDefault: any = [{}];
  public idsegmentTypeDelete: any;
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public listCountry:any;
  public listType:any;
  public listTollRoad:any;
  public isTwoWay:number = 0;
  public listTollStation:any;
  public listStartToll:any;
  public listEndToll:any;
  public listTollStationed:any = [];
  public listTollStationedId:any = [];
  public disableAdd:boolean = false;
  public listTransportType:any = [];
  public listTranportTypeCreated:any =[];
  public parameters:any [];
  public contentDescriptopn:any;
  public editSelectDeviceType:boolean = false;
  public currencyDefault = "VND";
  public listCurrency:any = [];
  public sortablejsOptions;
  public paramsEmiter: EventEmitter<any>;
  public isSelectToll:boolean = false;
  // qcvn
 
  constructor(
    private segment: TollsegmentService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private tollRoad : TollRoadService,
    private asciiPipe:AsciiPipe,
    private translate: TranslateService,
    private tollStationService:TollStationService,
    private transport:TransportTypeService,
    private geo : GeoService,
    private toast: ToastService,


  ) {
     this.paramsEmiter = new EventEmitter();
    this.sortablejsOptions = {
      animation: 270,
      handle: '.drag_div',
      onUpdate: (event: any) => {
        // this.updateListFavorite();
      }
    };
    this.unsubscribe = new Subject();
    this.getListTollRoad();
    this.getTollStation();

    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.getListType();
    this.buildForm();
    this.refreshEventType();

  }

  ngOnInit() {
    this.updateDataTable();
    this.getListTransportType();

    this.dataTable.reload({ currentPage: 1 });

 }

 getListCurrency()
 {
   this.listCountry.map(country=>{
     if(!this.listCurrency.find(x=>x.name == country.currencyCode))
     {
       this.listCurrency.push({name:country.currencyCode});
     }
   });

 }

 changeCurrency(Currency)
 {
   let newCurrency = Currency.target.value;
   this.parameters.map(params=>{
     params.price = 0;
     params.value = 0;
     params.currencyUnit = newCurrency;
     let name = "name_"+params.id;
     let object = {};
      object[name] = 0;
     this.paramsCurrency.patchValue(object);
     return params;
   });  
   this.currencyDefault = newCurrency;
   setTimeout(()=>{
     $('.select-currency').val(this.currencyDefault).selectpicker('refresh');
   })

 }

 async getListCountry()
 {
   let name="country";    
   if(localStorage.getItem(name) == 'undefined' || localStorage.getItem(name) == null)
   {
       await  this.geo.listCountry()
       .pipe(
           takeUntil(this.unsubscribe),
           finalize(() => {
             this.cdr.markForCheck();
           })
         )
       .subscribe(data=>{
           if(data.status == 200) 
           {
            this.listCountry = data.result;
            this.getListCurrency();
            localStorage.setItem(name,JSON.stringify(this.listCountry));
           }
     
         })

   }else{
     this.listCountry = localStorage.getItem(name);
     this.listCountry = JSON.parse(this.listCountry);
     this.getListCurrency();
   } 
 }


 loadParams()
 {
  if(!this.paramsCurrency)  this.paramsCurrency = this.formBuilder.group({});
  this.parameters.map(item=>{
    
    let name = "name_"+item.id;
    this.paramsCurrency.controls[name] = new FormControl({ value: item.value, disabled: false }); 
    return item;
  })
 }

  private buildForm(): void {
    this.formSegmentType = this.formBuilder.group({
        name: ['',Validators.required],
        roadID: ['',Validators.required],
        entryStationId: ['',],
        exitStationId: ['',],
        distance:[''],
        isTwoWay: ['0',Validators.required],

      });
    this.searchFormSegmentType = this.formBuilder.group({
      name: [''],
      roadIdSearch:[''],  
    });

  }

  remove_character(str_to_remove, str) {
    let reg = new RegExp(str_to_remove);
    str = str.toString().replace(/,/g, "");
    return str;
  }

  changePrice(event,item)
  {
    let price = event.target.value;   

    let idTransport = item.id;
      this.parameters.map(params=>{
      price = this.remove_character(',',price);
      price = parseFloat(price);
      if(params.id == idTransport) 
      {
        params.value = price
        params.amount = price

      }
      return params;
    });
    
  }

  removePrice(idTransport)
  {
    this.disableAdd = true;
    this.editSelectDeviceType = false;

    this.parameters = $.grep(this.parameters, function(e){ 
      return e.id != idTransport; 
    });
    // this.getlistTranportTypeCreated();

  }

  
  getListTransportType(){
    this.contentDescriptopn = '';   
     
    if(sessionStorage.getItem('listTransportType1') == undefined)
    {
      let params = {
        pageNo:-1,
        orderBy:'sortOrder',
        orderType: 'asc'
      };
      this.transport.list(params).pipe(
        tap((data: any) => {
       
          this.listTransportType = data.result;
          this.mapListTransportType();
          sessionStorage.setItem('listTransportType1',  JSON.stringify(data.result));

        }),
       
        finalize(() => {
          this.cdr.markForCheck();
        })
      ).subscribe();
    }else{
      this.listTransportType = sessionStorage.getItem('listTransportType1');
      this.listTransportType = JSON.parse(this.listTransportType);
      
      this.mapListTransportType();
    }

    this.listPriceDefault();

  }

  test(number)
  {
    if(number == this.parameters.length-1) return true;
    return false;
  }

  async listPriceDefault(){
    this.currencyDefault = "VND";

    let listPrice = [35000,50000,75000,120000,180000];
    this.parameters = this.listTransportType;
    this.parameters .map((transport,i) =>{
      let value = listPrice[i];
      transport.value = value;
      transport.amount = value;
      return transport;
    });

    this.loadParams(); 

  }


  mapListTransportType()
  {
      this.listTransportType.map(transport =>{
      this.contentDescriptopn = this.contentDescriptopn + '- '+ transport.name+ ': ' + transport.description + "<br>";
      return transport;
      });
  }

  buttonAddNew(content) {
    if(this.listStartToll) this.listStartToll = this.listStartToll.map(toll=>{
       toll.checked = false;
    });
    this.listTollStationed = [];
    this.isEdit = false;
    this.isSelectToll = false;
    this.open(content);
    this.listPriceDefault();
    this.getListCountry();

    this.formSegmentType = this.formBuilder.group({
      name: ['',Validators.required],
      roadID: ['',Validators.required],
      distance:[''],
      isTwoWay: [true,Validators.required],
    });
    setTimeout( () => {
          $('#roadID').selectpicker('refresh'); 
          $('.select-currency').val(this.currencyDefault).selectpicker('refresh');
      });
  }
  
  open(content) {

    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-800'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  getListType(){

    setTimeout( () => {
        this.listType = [
            {id:0,name:'high way'},
            {id:1,name:'national segment'},
            {id:2,name:'province segment'},
        ];
        $('#listType').selectpicker('refresh');
      });
  }

  
  getlistTranportTypeCreated()
  {
    this.listTranportTypeCreated = [];
    this.listTransportType.forEach(element => {
      let id = element.id;
      if(!this.parameters.find( x=>x.id == id))
      {
        element.value = 0;
        element.amount = 0;
        element.transportType = id;
        this.listTranportTypeCreated.push(element);
      }
    });
    this.parameters.push(this.listTranportTypeCreated[0]);
    // this.buildForm();
    setTimeout(() => {
     $('.select-devicetype').selectpicker('refresh'); 
    });
  }

  createParamsets(){
    this.getlistTranportTypeCreated();
    
    this.disableAdd = false;
    if(this.parameters.length != this.listTransportType.length) this.disableAdd = true;

    setTimeout(() => {
      document.getElementById('croll-to').scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' });
    });
  }
  
  getListTollRoad()
  {
    this.tollRoad.list({pageNo:-1})
    .pipe(
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
    .subscribe(data=>{
        if(data.status == 200) this.listTollRoad = data.result;
        this.listTollRoad = this.listTollRoad.filter(country=>{
          if(country.typeName == "HIGHWAY")
          {
            country.textSearch = this.asciiPipe.transform(country.name || "");
            return true;
          }
          return false;
          // return country;
        })
        // sessionStorage.setItem(name,JSON.stringify(this.listCountry));

        setTimeout(()=>{
          $('select').selectpicker();
    
        })
      })

  }

  getNameRoad(id)
  {
   
    if(this.listTollRoad != undefined && this.listTollRoad.find(x=>x.id == id))   return this.listTollRoad.find(x=>x.id == id).name;
  }

 async getTollStation()
  {
    let params = {
        pageNo:-1
    };

  let name="listTollStation";
  if(sessionStorage.getItem(name) == undefined)
    {
      await  this.tollStationService.list({ params:params})
        .pipe(
            takeUntil(this.unsubscribe),
            finalize(() => {
              this.cdr.markForCheck();
            })
          )
        .subscribe(data=>{
            
            if(data.status == 200)
            {
                this.listTollStation = data.result;
                this.listTollStation.map(country=>{
                  country.textSearch = this.asciiPipe.transform(country.name || "");
                  return country;
                });
                setTimeout(() => {
                    $('#exitStationId').selectpicker('refresh'); 
                    $('#entryStationId').selectpicker('refresh'); 
                  });
                  this.getData();
            }
            sessionStorage.setItem(name,JSON.stringify(this.listTollStation));
        
          })
    }else{
      this.listTollStation = sessionStorage.getItem(name);
      this.listTollStation = JSON.parse(this.listTollStation);
    }
  }

  getNameTollStation(id){
    if(this.listTollStation != undefined && this.listTollStation.find(x=>x.id == id))  return this.listTollStation.find(x=>x.id == id).name;
  }

  selectTollStation(event)
  {
      let listToll = this.formSegmentType.value.entryStationId;
      this.listTollStationed = [];
      if(listToll.length > 0)
      {
        for (var i = 0; i < listToll.length; ++i) 
        {
          this.listTollStationed.push({
            id:listToll[i],
            name:this.getNameTollStation(listToll[i])
          });
        }
      }

      this.getListIdTollSelected();
  }

  removeToll(id)
  {
    this.listTollStationed = this.listTollStationed.filter(function( obj ) {
      return obj.id !== id;
    });
    this.listStartToll.find(toll=> toll.id == id).checked = false;
  }

  getListIdTollSelected()
  {
    this.listTollStationedId = [];
    this.listTollStationed.map(obj=>{
      this.listTollStationedId.push(obj.id);
      return obj;
    });
  }

  changeChecked(event){
    if(event.action)
    {
      let checked = event.action.checked;
      let id = event.action.id;
      let name = event.action.name;
      if(checked == true) this.listTollStationed.push({
        id:id,
        name:name
      });
      if(checked == false) this.listTollStationed = this.listTollStationed.filter(toll=> {
        return toll.id !=id;
      })
    }
  }

  getListTollStationByTollRoadId(id)
  {
   return this.listTollStation.filter(toll=>{
      return toll.roadId == id;  
  });
  }

  openListtoll(modalTollStation){
    this.listStartToll.map(toll=>{
       toll.checked = false;
       return toll;
    });
    this.listStartToll.map(toll=>{
      if(this.listTollStationed.find(x=>x.id == toll.id)) toll.checked = true;
      return toll;
    });
    this.open(modalTollStation);
  }
  selectTollRoad(event,modalTollStation?)
  {
    this.isSelectToll = true;
    let roadId;
    if(typeof event == 'number')
    {
      roadId = event
    }else{
      roadId = event.target.value;
    }
    this.listStartToll  = this.getListTollStationByTollRoadId(roadId); 

    if(this.listStartToll) this.listStartToll = this.listStartToll.map(toll=>{
      toll.checked = false;
      return toll;
   }); 
    if(modalTollStation)  this.open(modalTollStation);
    this.listTollStationed = [];

  }

  onSubmitTollStation(form: any) {
    this.getListIdTollSelected();
    let parameters:any = [];    
    this.parameters.map((params,i)=>{
      parameters.push({
        transportType : params.id,
        // currencyUnit : this.currencyDefault,
        amount : params.value
      });
      return params;
    });
    if (this.formSegmentType.invalid) {
      return;
    }
      
    if(this.listTollStationedId.length == 0){
      this.toast.show({message:this.translate.instant('ADMIN.TOLL_SEGMENT.MESSAGE.PLEASE_TOLL_STATION'),type:'error'});
      return;
    }

    if(form.value.isTwoWay == true) form.value.isTwoWay = 1;
    if(form.value.isTwoWay == false) form.value.isTwoWay = 0;

    let segmentType = {
        name:form.value.name,
        roadId:form.value.roadID,
        // entryStationId:form.value.entryStationId,
        // exitStationId:form.value.exitStationId,
        stationIds : this.listTollStationedId.toString(),
        unit:this.currencyDefault,
        distance:form.value.distance,
        isTwoWay:form.value.isTwoWay,
        info:JSON.stringify(parameters)
    } as tollSegment; 
    if(this.isEdit) {
      segmentType.id = this.idsegmentTypeEdit;      
      this.segment.update(segmentType,{ notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          this.getData();
          this.modalService.dismissAll();
        }
      });
      return;
    }
    this.segment.create(segmentType,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        this.getData();
        this.modalService.dismissAll();

      }
    });
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
      orderBy:'sortOrder',
      orderType: 'ASC'
    };
  }

  onHideModal(id: string) {
    let idModal = '#' + id;
    $(idModal).modal('hide');
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }

  getIdAction(id,idModal) {
    this.idsegmentTypeDelete = id;
    this.modalService.open(idModal, { windowClass: 'kt-mt-50  modal-holder modal-delete',backdrop:'static'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openModal(params) {
     let idModal = params.idModal;
     let confirm = params.confirm;
     if(confirm)
     {$(idModal).appendTo('body').modal({backdrop: 'static',keyboard: false})}
     else{$(idModal).appendTo('body').modal('show');}
  }

  deleteTollSegment() {
    let id = this.idsegmentTypeDelete;
    this.segment.delete(id, { notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.getData();
        this.modalService.dismissAll();
      }
    });
    
  }

  getNameIsWay(id){
    let text = "Không";
    if(id == 1) text = "Có";
    return text;
  }

  getNameCountry(id)
  {
      return this.listCountry.find(x=>x.id == id).name;
  }

  editTollSegment(item,content) {
  this.isEdit = true;
  this.listPriceDefault(); 
  this.getListCountry();
  this.listTollStationed = [];
  if(item.listToll) this.listTollStationed = item.listToll;
  this.listStartToll  = this.getListTollStationByTollRoadId(item.roadId); 
  this.listStartToll.map(toll=>{
    if(this.listTollStationed.find(x=>x.id == toll.id)) toll.checked = true;
  });

  if(item.info.length > 0)
  {
    this.currencyDefault = item.unit;      

    item.info.map(itemInfo=>{
        itemInfo.id = itemInfo.transportType;
        itemInfo.value = itemInfo.amount;
        if(this.listTransportType.find(x=>x.id == itemInfo.transportType))
        {

          itemInfo.name = this.listTransportType.find(x=>x.id == itemInfo.transportType).name;
          itemInfo.description = this.listTransportType.find(x=>x.id == itemInfo.transportType).description;
        } 
       
        return itemInfo;
      });
      this.parameters = item.info;
      this.parameters = this.sortOrder(this.parameters);
      if(this.parameters.length != this.listTransportType.length) this.disableAdd = true;
      this.loadParams();
  }
  setTimeout(()=>{

    $('.select-currency').val(this.currencyDefault).selectpicker('refresh');
    // $('#exitStationId').selectpicker();
    // $('#entryStationId').selectpicker();
    $('#roadID').selectpicker();

  })


    if(item.isTwoWay == 1) item.isTwoWay = true;
    if(item.isTwoWay == 0) item.isTwoWay = false;

    this.idsegmentTypeEdit = item.id;
    this.formSegmentType = this.formBuilder.group({
      name: new FormControl({ value: item.name, disabled: false },Validators.required),
      roadID: new FormControl({ value: item.roadId, disabled: false },Validators.required),
      entryStationId: new FormControl({ value: item.entryStationId, disabled: false }),
      exitStationId: new FormControl({ value: item.exitStationId, disabled: false }),
      distance: new FormControl({ value: item.distance, disabled: false }),
      isTwoWay: new FormControl({ value: item.isTwoWay, disabled: false }),
    });
    this.open(content);

  }

  sortOrder(arr)
  {
    return arr.sort((a,b)=> parseInt(a.sortOrder) - parseInt(b.sortOrder));
  }

  loadSelectBootstrap(){
		$('.bootstrap-select').selectpicker();
  }

  private refreshEventType()
  {
     this.translate.onLangChange.subscribe((event) => {
     let country = this.translate.instant('ADMIN.TOLL_STATION.COLUMN.SELECT_COUNTRY');
     let type = this.translate.instant('MANAGE.POI.GENERAL.TYPE_POI');
     
     $(function () {
       $('#listType').selectpicker({title: type}).selectpicker('refresh');
       $('#countrySearch').selectpicker({title: country}).selectpicker('refresh');

     });
   });
  }

  searchTollSegment(form:any)
  {
    if(form.value.name == null ||  form.value.name == '')  form.value.name = '';
    if(form.value.roadIdSearch == null ||  form.value.roadIdSearch == '')  form.value.roadIdSearch = '';
    this.filter.name = form.value.name;
    this.filter.roadId = form.value.roadIdSearch;
    
    this.dataTable.reload({ currentPage: 1 });
  }

  resetFormSearch()
  {
    this.searchFormSegmentType.reset();
    this.buildPagination();
    $('select').selectpicker("refresh");
    this.dataTable.reload({ currentPage: 1 });

  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;  
        this.getData();
      }),
      
    ).subscribe();
  }

  get f() {
   if(this.formSegmentType != undefined) return this.formSegmentType.controls;
  }

  getData() {
    this.segment.list(this.filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          result.result.content.map(toll=>{
            toll.keyName = "ADMIN.TOLL_SEGMENT.COLUMN."+toll.typeName;
            if(toll.stationIds != null)
            {
              let stationIds = toll.stationIds.split(",");
              let names = '';
              let listToll = [];
              stationIds.forEach((id,i) => {
                let nameById = '';
                if(this.getNameTollStation(id))  nameById = this.getNameTollStation(id);


                if(i != stationIds.length -1) names = names+nameById+', ';
                if(i == stationIds.length -1) names = names+nameById;
                listToll.push({
                  id:id,
                  name:nameById
                });
              });
              toll.listToll = listToll;
              toll.names = names;
            }
            return toll;
          });
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          // $(function () {
          //   $('select').selectpicker();
          // });
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect, 
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
                selecter:false,
                responsive:false
              }
              
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
  }
  
  drop(event: CdkDragDrop<any[]>) {

    let data:any = [];
    moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    data = event.container.data.map((item,i)=>{
     
      let obj = {
        id:item.id,
        sortOrder:i
      };
      return obj;
    });
     this.segment.sortOrder(data, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.cdr.detectChanges();
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(this.cdr.markForCheck)
    ).toPromise();   
  }

  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '40px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Name',
        field: 'name',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '250px' },
        class: '',
        translate: 'COMMON.COLUMN.NAME',
        autoHide: false,
        width: 250,
      },
      {
        title: 'Type',
        field: 'limitspeed',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'ADMIN.TOLL_SEGMENT.COLUMN.ROAD',
        autoHide: true,
        width: 182,
      },
      {
        title: 'Country',
        field: 'qcvnCode',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '350px' },
        class: '',
        translate: 'ADMIN.TOLL_SEGMENT.COLUMN.LIST_TOLL',
        autoHide: true,
        width: 350,
      },
      // {
      //   title: 'Create by',
      //   field: 'createdBy',
      //   allowSort: false,
      //   isSort: false,
      //   dataSort: '',
      //   style: { 'width': '182px' },
      //   class: '',
      //   translate: 'ADMIN.TOLL_SEGMENT.COLUMN.END_TOLL',
      //   autoHide: true,
      //   width: 200,
      // },
      {
        title: 'Create by',
        field: 'createdBy',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px','text-align':'center' },
        class: '',
        translate: 'ADMIN.TOLL_SEGMENT.COLUMN.IS_WAY',
        autoHide: true,
        width: 182,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 182,
      },
    ]
  }


}
