import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { finalize, takeUntil, tap ,debounceTime} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { TranslateService } from '@ngx-translate/core';
import { SensorType,SensorTypeService } from '@core/admin';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

declare var $: any;
const PARAMETER_DEFAULT  = {
  type:"",
  min: "0",
  max: "1000",
  parameter_name:"",
  unit:"",
  formula: "[value]", 
  calibration:"",
  offset:"0",
  round:"0",
  show_on_map:"0",
  increase:"20",
  decrease:"20",  
};
@Component({
  selector: 'kt-sensor-type',
  templateUrl: './sensor-type.component.html',
  styleUrls: ['./sensor-type.component.scss']
})
export class SensorTypeComponent implements OnInit {
  formSensor: FormGroup;
  searchFormSensor : FormGroup;
  selectdp: ElementRef;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idSensorEdit: number;
  public dataDefault: any = [{}];
  public idSensorDelete: any;
  public filter :any;
  private unsubscribe: Subject<any>;
  public translationsArr = [];
  public parameters = [{}];
  public closeResult: string;
  private isRepeater : boolean = false;
  constructor(
    private sensor: SensorTypeService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    private modalService: NgbModal
  ) {
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    this.updateDataTable();
    this.getData();
 }

  private buildForm(): void {
    this.formSensor = this.formBuilder.group({
      nameSensor: ['',Validators.required],
      keyLanguageSensor: ['',Validators.required],
      typeSensor : [''],
      description: [''],
    });
    this.searchFormSensor = this.formBuilder.group({
      nameSensor: [''],    
    })
  }

  buttonAddNew(content) { 
    this.parameters = this.convertParameters(PARAMETER_DEFAULT);
    this.isEdit = false;
    this.formSensor.reset();
    this.open(content);
    this.addRepeaterForm('#kt_repeater_1');
  }

  addRepeaterForm(classDiv,initEmpty = false){
      setTimeout(() => {
        $(classDiv).repeater({
          initEmpty: initEmpty,      
          defaultValues: {
          
          },
          show: function () {
            $(this).slideDown();
          },
          hide: function (deleteElement) {                
            $(this).slideUp(deleteElement);                 
          }   
        });
      }, 600);
  }

  createParamsets(){
    setTimeout(() => {
      document.getElementById('create-paramsets').scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' });
    },300);
  }
  
  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  

  getContentTable(elementTable){
    let content:any = {};
    for (var i = 0; i < elementTable.length; i++) {
      let key = elementTable[i].firstChild.textContent;
      if(key != '')
      {
        content[elementTable[i].firstChild.textContent] = elementTable[i].childNodes[1].textContent;
      }
  }
  return JSON.stringify(content);
  }

  onSubmitSensor(form: any) {
    var elements = $("#table-sensor tbody tr");
    var parameters =  this.getContentTable(elements);
    if (this.formSensor.invalid) {
      return;
    }
    let sensorType = new SensorType({
      "name": form.value.nameSensor,
      "keyLanguage": form.value.keyLanguageSensor,
      "description": form.value.description,
      "parameters": parameters,
    });
    if(this.isEdit) {
      sensorType.id = this.idSensorEdit;
      this.sensor.update(sensorType,{ notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          this.getData();
        }
      });
      this.modalService.dismissAll();
      return;
    }
    this.sensor.create(sensorType,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        this.getData();
      }
    });
    this.modalService.dismissAll();
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10
    };
  }

  onHideModal(id: string) {
    let idModal = '#' + id;
    $(idModal).modal('hide');
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }

  getIdAction(id,idModal) {
    this.idSensorDelete = id;
    this.openModal({idModal:idModal,confirm : true});
  }

  openModal(params) {
     let idModal = params.idModal;
     let confirm = params.confirm;
     if(confirm)
     {$(idModal).appendTo('body').modal({backdrop: 'static',keyboard: false})}
     else{$(idModal).appendTo('body').modal('show');}
  }

  deleteSensor() {
    let id = this.idSensorDelete;
    this.sensor.delete(id, { notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.getData();
      }
    });
    $('.modal').modal('hide');
  }

  editSensor(id,content) {
    this.addRepeaterForm('#kt_repeater_1');
    this.isEdit = true;
    this.idSensorEdit = id;
    let params = {
      id :id
    };
    this.sensor.list(params).pipe(
      tap((data: any) => {        
        this.parameters = this.convertParameters(data.result.content[0].parameters);
        this.dataDefault = data.result.content;
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
    this.open(content);
  }

  convertParameters(parameters)
  {
    let result = [];
    for(var property in parameters)
    {
      let obj = {
        key: property,
        value : parameters[property]
      }
    result.push(obj);
    }
    return result;
   }

  loadSelectBootstrap(){
		$('.bootstrap-select').selectpicker();
  }

  searchSensor(form:any)
  {
    this.filter.name = form.value.nameSensor;
    this.getData();
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter = option;
        this.getData();
      }),
      
    ).subscribe();
  }

  get f() {
   if(this.formSensor != undefined) return this.formSensor.controls;
  }

  getData() {
    this.sensor.list(this.filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          result.result.content = result.result.content.map(x=>{
            x.keyTranslate = ( x.keyLanguage!=undefined?("SENSOR_TEMPLATE." + x.keyLanguage.toString().toUpperCase()):"");
            return x;
          });
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          $(function () {
            $('.kt_selectpicker').selectpicker();
          });
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect, 
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
				selecter:false,
			  }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();

  }
  
  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '40px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Name',
        field: 'name',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'COMMON.COLUMN.NAME',
        autoHide: false,
        width: 150,
      },
      {
        title: 'Key language',
        field: 'keyLanguage',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'COMMON.COLUMN.KEY_LANGUAGE',
        autoHide: false,
        width: 150,
      },
      {
        title: 'Translate language',
        field: 'keyTranslate',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'COMMON.COLUMN.KEY_TRANSLATED',
        autoHide: false,
        width: 150,
      },    
      {
        title: 'Edit By',
        field: 'modifiedBy',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'COMMON.EDIT_BY',
        autoHide: false,
        width: 150,
      },
      {
        title: 'Description',
        field: 'description',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'COMMON.COLUMN.DESCRIPTION',
        autoHide: true,
        width: 150,

      },
      {
        title: 'Created update',
        field: 'created',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '120px' },
        class: '',
        translate: 'COMMON.COLUMN.UPDATED_DATE',
        autoHide: true,
        width: 120,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 117,
      },
    ]
  }


}
