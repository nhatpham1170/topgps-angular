import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef,EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { finalize, takeUntil, tap ,debounceTime} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NotifiGlobalService,Notifi} from '@core/admin';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { select, Store } from '@ngrx/store';
import { AppState } from '@core/reducers';
import { currentUser } from '@core/auth';

import FroalaEditor from 'froala-editor';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { TranslateService } from '@ngx-translate/core';

declare var $: any;
@Component({
  selector: 'kt-notifi-global',
  templateUrl: './notifi-global.component.html',
  styleUrls: ['./notifi-global.component.scss']

})
export class NotifiGlobalComponent implements OnInit {
  @ViewChild('datePicker', { static: true }) 
  datePicker: DateRangePickerComponent;
  formNotifiGlobal: FormGroup;
  searchFormNotifi : FormGroup;
  formCheckList:FormGroup;
  selectdp: ElementRef;
  public datePickerOptions: any = {
    size: 'md',
    // select: 'to',
    ranges: 'to',
    optionDatePicker: {
      maxSpan: {
        days: 31
      }
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: true,

  };
  public parrentEmiter: EventEmitter<any | { id: number, path: string }>;

  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idNotifiEdit: number;
  public dataDefault: any = [{}];
  public idNotifiDelete: any;
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
 public userIdSelectedEdit:number;
  public checkallboxed:boolean;
  public showUserTree:boolean = true;
  public listSendUsers:any= [];
  public contentChange:any;
  public dateStart: string;
  public dateEnd: string;
  public languages;
  public langDefault:string = 'en';
  public loadingEditor:boolean = true;
  public contentChangeLogVi:any;
  public contentChangeLogEn:any;
  public showLoaddingSearch:any;
  public modules={
    toolbar: [
      ['bold', 'italic', 'underline'],        // toggled buttons
      // ['blockquote', 'code-block'],
  
      [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      // [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      // [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      // [{ 'direction': 'rtl' }],                         // text direction
  
      [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      // [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
  
      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'font': [] }],
      [{ 'align': [] }],
  
      // ['clean'],                                         // remove formatting button
  
      ['link', 'image', 'video']                         // link and image, video
    ],
  };
  public settingEditor = {
   
    fullPage: true,
    charCounterMax: 10,
    charCounterCount: true,
    events: {
        'keyup': function (keyupEvent) {
    
            // console.log(this);
          }
      }
     // Set the image upload URL.
  };

  constructor(
    private Notifi: NotifiGlobalService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private userTreeService: UserTreeService,
    private store: Store<AppState>,
    private translate: TranslateService,

  ) {
    this.parrentEmiter = new EventEmitter();

    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    this.getData();
    this.updateDataTable();
    this.loadLanguages();
   }

   private buildForm(): void {
    this.formNotifiGlobal = this.formBuilder.group({
      title: ['',Validators.required],
      titleVi: ['',Validators.required],
      contentSystemLogEn : [''],
      contentSystemLogVi : [''],
    });
    this.searchFormNotifi = this.formBuilder.group({
      title: ['']
    });
  }

  loadTab(){
    // console.log(this.dateStart);
    this.datePickerOptions = {
      size: 'md',
      // select: 'to',
      ranges: 'to',
      optionDatePicker: {
        maxSpan: {
          days: 31
        }
      },
      autoSelect: true,
      singleDatePicker: false,
      timePicker: true,
  
    };
  }

  loadLanguages()
  {
    let vi = true;
    let en = false;
   this.langDefault = this.translate.getDefaultLang();
   if(this.langDefault == 'en')
   {
     vi = false;
     en = true;
   }else{
     vi = true;
     en = false;
   }
   this.languages = [
   {
     lang:'vi',
     src:"./assets/media/flags/001-vietnam.svg",
     active:vi
   },
   {
     lang:'en',
     src:"./assets/media/flags/012-uk.svg",
     active:en
   }
  ];
 
     setTimeout(function () {
       $('.kt_selectpicker').selectpicker();
     }, 100);
  }

  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-900'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   this.showUserTreeFunction();
  }

  showUserTreeFunction(){
    let userModel = {
      type: 0,
    };
    let user$:any = this.store.pipe(select(currentUser));
    if (user$) {
      select(user$.subscribe(function (data) {
        Object.assign(userModel, data);
      }
      ).unsubscribe());
    }
      // code display use-tree
      let type = userModel.type;
      this.showUserTree = false;
      if(type == 0 || type == 1) this.showUserTree = true;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  
  buildPagination() {
    this.filter = { 
      pageNo: 1,
      pageSize: 10,
      orderBy:'createdAt',
      orderType:'DESC'
    };
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }

  buttonAddNew(content) {
    this.isEdit = false;
    this.formNotifiGlobal.reset();
    this.open(content);
    // this.getDevices(this.userIdSelected);
  }

  editNotifi(item,content) {
    this.dataDefault = [item];
    this.isEdit = true;
    this.idNotifiEdit = item.id;
    this.datePickerOptions.startDate = item.timeStart;
    this.datePickerOptions.endDate = item.timeEnd;

    this.open(content);
  }
  
  searchNotifi(form:any)
  {
    this.filter.title = form.value.title;
    this.filter.pageNo = 1;

    this.getData();
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter = option;
        this.getData();
      }),
      
    ).subscribe();
  }

  get f() {
   if(this.formNotifiGlobal != undefined) return this.formNotifiGlobal.controls;
  }

  getData() {
    this.Notifi.list(this.filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          setTimeout(function () {
            $('.kt_selectpicker').selectpicker('refresh');
          }, 100);
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect, 
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
				selecter:false,
			  }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  deleteNotifiConfim(){
    this.Notifi.delete(this.idNotifiDelete,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 200) {
        this.dataTable.reload({});
        this.modalService.dismissAll();
      }})
  }

  deleteNotifi(id,content)
  {
    this.idNotifiDelete = id;
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-delete'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  dateSelectChange(data) {
    // console.log(data);
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
   }

  onSubmit(form: any) {
    if (this.formNotifiGlobal.invalid) {
        // console.log(this.formNotifiGlobal.invalid);
      return;
    }
    let contentChangeLogEn = form.value.contentSystemLogEn;
    let contentChangeLogVi = form.value.contentSystemLogVi;
    let params = {
      title:form.value.title,
      titleVi:form.value.titleVi,
      contentVi:contentChangeLogVi,
      content:contentChangeLogEn,
      timeStart:this.dateStart,
      timeEnd:this.dateEnd
    } as Notifi;
    if (this.isEdit) {
      params.id = this.idNotifiEdit;
      this.Notifi.update(params,{ notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          this.dataTable.reload({});
          this.closeModal();
        }
      });
      return;
    }
    this.Notifi.create(params,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        this.dataTable.reload({});
        this.closeModal();
      }})
  }
  
  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'title notifi',
        field: 'imei',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '500px' },
        class: '',
        translate: 'MANAGE.MESSAGE.COLUMN.TITLE',
        autoHide: false,
        width: 400,
      },
      {
        title: 'content',
        field: 'command',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'REPORT.ROUTE.GENERAL.DATE_START',
        autoHide: true,
        width: 100,
      },
      {
        title: 'count user ',
        field: 'created',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'REPORT.ROUTE.GENERAL.DATE_END',
        autoHide: true,
        width: 100,
      },

      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 117,
      },
    ]
  }


}




