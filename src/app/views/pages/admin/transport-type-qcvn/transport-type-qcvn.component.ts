import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { finalize, takeUntil, tap ,debounceTime} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { TranslateService } from '@ngx-translate/core';
import { TransportTypeQCVN,TransportTypeServiceQCVN } from '@core/admin';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

declare var $: any;
@Component({
  selector: 'kt-transport-type-qcvn',
  templateUrl: './transport-type-qcvn.component.html',
  styleUrls: ['./transport-type-qcvn.component.scss']
})
export class TransportTypeQCVNComponent implements OnInit {
  formTransportTypeQCVN: FormGroup;
  searchFormTransportTypeQCVN : FormGroup;
  selectdp: ElementRef;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idTransportTypeQCVNEdit: number;
  public dataDefault: any = [{}];
  public idTransportTypeQCVNDelete: any;
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  // qcvn
  public listQCVN:any =
   [
    {id:100,name : 'Khach = 100'},
    {id:200,name : 'Bus = 200'},
    {id:300,name : 'HopDong = 300'},
    {id:400,name : 'DuLich = 400'},
    {id:500,name : 'Container = 500'},
    {id:600,name : 'XeTai = 600'},
    {id:700,name : 'Taxi = 700'},
    {id:800,name : 'TaxiTai = 800'},
    {id:900,name : 'XeDauKeo = 900'},

  ]
  constructor(
    private transport: TransportTypeServiceQCVN,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal
  ) {
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    this.updateDataTable();
    this.getData();
 }

  private buildForm(): void {
    this.formTransportTypeQCVN = this.formBuilder.group({
      nameTransportTypeQCVN: ['',Validators.required],
      nameKey: ['',Validators.required],
      limitSpeed : [''],
      qcvnCode: [''],
    });
    this.searchFormTransportTypeQCVN = this.formBuilder.group({
      nameTransportTypeQCVN: ['']
    })
  }

  buttonAddNew(content) {
    this.isEdit = false;
    this.formTransportTypeQCVN.reset();
    this.open(content);
    setTimeout(() => {
        $('.kt_selectpicker').selectpicker('refresh');
      }, 100);
  }
  
  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  

  onSubmitTransportTypeQCVN(form: any) {
    if (this.formTransportTypeQCVN.invalid) {
      return;
    }
    let TransportTypeQCVN = {
      name: form.value.nameTransportTypeQCVN,
      nameKey: form.value.nameKey,
      limitSpeed : form.value.limitSpeed,
      qcvnCode : form.value.qcvnCode,
    } as TransportTypeQCVN; 
    if(this.isEdit) {
      TransportTypeQCVN.id = this.idTransportTypeQCVNEdit;
      this.transport.update(TransportTypeQCVN,{ notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          this.getData();
          this.modalService.dismissAll();

        }
      });
      return;
    }
    this.transport.create(TransportTypeQCVN,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        this.getData();
        this.modalService.dismissAll();

      }
    });
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
      orderBy:'createdAt',
      orderType: 'DESC'
    };
  }

  onHideModal(id: string) {
    let idModal = '#' + id;
    $(idModal).modal('hide');
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }

  getNameQCVN(id)
  {
    let name : string = '';
    let item = this.listQCVN.find(x => x.id === id);
    if(item != undefined)  name = item.name;
    return name;
  
  }

  getIdAction(id,idModal) {
    this.idTransportTypeQCVNDelete = id;
    this.modalService.open(idModal, { windowClass: 'kt-mt-50  modal-holder modal-delete',backdrop:'static'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openModal(params) {
     let idModal = params.idModal;
     let confirm = params.confirm;
     if(confirm)
     {$(idModal).appendTo('body').modal({backdrop: 'static',keyboard: false})}
     else{$(idModal).appendTo('body').modal('show');}
  }

  deleteTransportTypeQCVN() {
    let id = this.idTransportTypeQCVNDelete;
    this.transport.delete(id, { notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.getData();
        this.modalService.dismissAll();
      }
    });
  }

  editTransportTypeQCVN(id,content) {
    this.isEdit = true;
    this.idTransportTypeQCVNEdit = id;
    let params = {
      id :id
    };
    this.transport.list(params).pipe(
      tap((data: any) => {
        this.dataDefault = data.result.content;
        setTimeout(() => {
            $('.kt_selectpicker').selectpicker('refresh');
          }, 100);
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
    this.open(content);
  }

  loadSelectBootstrap(){
		$('.bootstrap-select').selectpicker();
  }

  searchTransportTypeQCVN(form:any)
  {
    this.filter.name = form.value.nameTransportTypeQCVN;
    this.getData();
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter = option;
        this.getData();
      }),
      
    ).subscribe();
  }

  get f() {
   if(this.formTransportTypeQCVN != undefined) return this.formTransportTypeQCVN.controls;
  }

  getData() {
    this.transport.list(this.filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          // $(function () {
          //   $('select').selectpicker();
          // });
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect, 
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
				selecter:false,
			  }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
  }
  
  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '40px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Name',
        field: 'name',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'COMMON.COLUMN.NAME',
        autoHide: false,
        width: 182,
      },
      {
        title: 'Limit Speed',
        field: 'limitspeed',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'ADMIN.TRANSPORT_TYPE.COLUMN.LIMITSPEDD',
        autoHide: true,
        width: 182,
      },
      {
        title: 'QNCN',
        field: 'qcvnCode',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'ADMIN.TRANSPORT_TYPE.COLUMN.QNCN_CODE',
        autoHide: true,
        width: 182,
      },
      {
        title: 'Create by',
        field: 'createdBy',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'COMMON.COLUMN.CREATED_BY',
        autoHide: true,
        width: 182,
      },
      {
        title: 'Created update',
        field: 'created',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'COMMON.COLUMN.UPDATED_DATE',
        autoHide: true,
        width: 182,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 182,
      },
    ]
  }


}
