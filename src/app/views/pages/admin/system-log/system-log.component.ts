import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { finalize, takeUntil, tap ,debounceTime,delay} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { TranslateService } from '@ngx-translate/core';
import { SystemChangeLogService } from '@core/admin';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FunctionService } from '@core/_base/layout/services/function.service';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { DeviceService} from '@core/manage';
import FroalaEditor from 'froala-editor';
// Import all Froala Editor plugins.
import 'froala-editor/js/plugins.pkgd.min.js';
// Import a single Froala Editor plugin.
// import 'froala-editor/js/plugins/align.min.js';
import { UserManageService } from '@app/core/manage/_service/user-manage.service';

// Import a Froala Editor language file.
import 'froala-editor/js/languages/vi.js';
import Quill from 'quill';

import * as moment from 'moment';
declare var $: any;
@Component({
  selector: 'kt-system-log',
  templateUrl: './system-log.component.html',
  styleUrls: ['./system-log.component.scss']
})
export class SystemLogComponent implements OnInit {
  @ViewChild('datePicker', { static: true }) 
  datePicker: DateRangePickerComponent;
  @Input() form: FormGroup;
  @Input() control: string;
  formSystemLog: FormGroup;
  searchChangeLog : FormGroup;
  selectdp: ElementRef;
  public dataTable: DataTable = new DataTable();

  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idChangeLogsEdit: number;
  public dataDefault: any = [{}];
  public idChangeLogsDelete: any;
  public listEventTypes : any = [];
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public contentChangeLog:any;
  public contentChangeLogVi:any;
  public contentChangeLogEn:any;
  public showLoaddingSearch:boolean = false; 

  public listTypeVersion : any = [
      {
        id:'web',
        name:'web'
      },
      {
        id:'api',
        name:'api'
      },  
      {
        id:'app',
        name:'app'
    },
  ];
  // qcvn
  public languages;
  public langDefault:string = 'en';
  public settingEditor;
  public loadingEditor:boolean = true;
  @ViewChild('editor', { static: true }) 

  name = 'Angular';
  public modules={
    toolbar: [
      ['bold', 'italic', 'underline'],        // toggled buttons
      // ['blockquote', 'code-block'],
  
      [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      // [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      // [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      // [{ 'direction': 'rtl' }],                         // text direction
  
      [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      // [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
  
      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'font': [] }],
      [{ 'align': [] }],
  
      // ['clean'],                                         // remove formatting button
  
      ['link', 'image', 'video']                         // link and image, video
    ],
  };
  logChange($event) {

  }
  constructor(
    private changeLog: SystemChangeLogService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private translate: TranslateService,
    private manageUser: UserManageService,


  ) {
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    // var quill = new Quill('#editor1', {
    //   theme: 'snow'
    // });
    this.updateDataTable();
    this.getData();
    this.refreshEventType();
    this.loadLanguages();
    setTimeout(function () {
      $('.kt_selectpicker').selectpicker();
    }, 100);
 }

 getLangDefault()
 {

  this.langDefault = this.translate.getDefaultLang();
 }

 loadLanguages()
 {
   let vi = true;
   let en = false;
  this.langDefault = this.translate.getDefaultLang();
  if(this.langDefault == 'en')
  {
    vi = false;
    en = true;
  }else{
    vi = true;
    en = false;
  }
  this.languages = [
  {
    lang:'vi',
    src:"./assets/media/flags/001-vietnam.svg",
    active:vi
  },
  {
    lang:'en',
    src:"./assets/media/flags/012-uk.svg",
    active:en
  }
 ];
 }

 hasTranslation(key: string): boolean {
  return this.translate.instant(key) !== key;
 }

 checkURL(url) {
  return(url.match(/\.(jpeg|jpg|gif|png)/) != null);
}

 private refreshEventType()
 {
  this.translate.onLangChange.subscribe((event) => {
    let title = this.translate.instant('ADMIN.SYSTEM_LOG.COLUMN.VERSION_TYPE');
    $(function () {
      $('.type-system-log').selectpicker({title: title}).selectpicker('refresh');
    });
  });
 }

  private buildForm(): void {
    this.formSystemLog = this.formBuilder.group({
      nameSystemLog: ['',Validators.required],
      typeSystemLog: [''],
      contentSystemLogEn : [''],
      contentSystemLogVi : [''],

    });
    this.searchChangeLog = this.formBuilder.group({
      nameSystemLog: [''],
      typeSystemLog:[''],
    
    })
  }

  
  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-900',backdrop:"static"}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  onSubmitSystemLog(form:any)
  {
    if (this.formSystemLog.invalid) {
        return;
      }
      let nameSystemLog = form.value.nameSystemLog;
      let typeSystemLog = form.value.typeSystemLog;
      let contentChangeLogEn = form.value.contentSystemLogEn;
      let contentChangeLogVi = form.value.contentSystemLogVi;

      // let contentVi = this.contentChangeLogVi.getContents();

      let params = {
          version:nameSystemLog,
          type:typeSystemLog,
          description: contentChangeLogEn,
          descriptionVi:contentChangeLogVi
      } as changeLogModel;
      if (this.isEdit) {
        
        params.id = this.idChangeLogsEdit;
        this.changeLog.update(params,{ notifyGlobal: true }).subscribe((result: any) => {
          if (result.status == 200) {
            this.modalService.dismissAll();
            this.dataTable.reload({});
          }
        });
        return;
      }
      this.changeLog.create(params,{ notifyGlobal: true })
      .pipe(
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 201) {
          this.dataTable.reload({});
          this.modalService.dismissAll();
        }})
  }


  resetFormSearch() {

    this.searchChangeLog.reset();
    setTimeout(function () {
      $('.kt_selectpicker').selectpicker('refresh');
    }, 100);
    this.buildPagination();
    this.getData();
  }


  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
      orderType:'DESC',
      orderBy:"releasedAt"
    };
  }


  onHideModal(id: string) {
    let idModal = '#' + id;
    $(idModal).modal('hide');
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }

 
  getIdAction(id,idModal) {
    this.idChangeLogsDelete = id;
    this.openModal({idModal:idModal,confirm : true});
  }

  openModal(params) {
     let idModal = params.idModal;
     let confirm = params.confirm;
     if(confirm)
     {$(idModal).appendTo('body').modal({backdrop: 'static',keyboard: false})}
     else{$(idModal).appendTo('body').modal('show');}
  }

  deleteChangeLogs() {
    // let id = this.idChangeLogsDelete;
    // this.alert.delete(id, { notifyGlobal: true }).subscribe((data: any) => {
    //   if (data.status == 200) {
    //     this.getData();
    //   }
    // });
    // $('.modal').modal('hide');
  }

  searchSystemChangeLog(form){

    if(form.value.nameSystemLog != 'null' && form.value.nameSystemLog != null)
    {
      this.filter.version =  form.value.nameSystemLog.toString();    
    }
    if(form.value.typeSystemLog != 'null' && form.value.typeSystemLog != null)
    {
      this.filter.type =  form.value.typeSystemLog.toString();    
    }

    this.getData();
  }


  buttonAddNew(content) {
    this.loadLanguages();
    this.getLangDefault();
    this.open(content);
    this.isEdit = false;
    this.formSystemLog.reset();
    setTimeout(function () {
      $('.kt_selectpicker').selectpicker();
    }, 100);
  }

  editSystemLog(item,content) {
    this.loadLanguages();
    this.getLangDefault();
    this.open(content);
    this.idChangeLogsEdit = item.id;
    this.isEdit = true;
    this.dataDefault =item;

  }


  loadSelectBootstrap(){
		$('.bootstrap-select').selectpicker();
  }

  searchChangeLogs(form:any)
  {
    this.filter.name = form.value.nameChangeLogs;
    this.getData();
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;

        this.getData();
      }),
      
    ).subscribe();
  }

  get f() {
   if(this.formSystemLog != undefined) return this.formSystemLog.controls;
  }

  getData() {
    this.showLoaddingSearch = true;

    this.changeLog.list(this.filter)
      .pipe(
        delay(300),
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          this.showLoaddingSearch = false;
          $(function () {
            $('.kt_selectpicker').selectpicker();
          });
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect, 
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
				selecter:false,
			  }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
  }
  
  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '40px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Version name',
        field: 'Device name',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '400px' },
        class: '',
        translate: 'ADMIN.SYSTEM_LOG.COLUMN.VERSION_NAME',
        autoHide: false,
        width: 182,
      },
      {
        title: 'Version type',
        field: 'eventType',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'ADMIN.SYSTEM_LOG.COLUMN.VERSION_TYPE',
        autoHide: false,
        width: 182,
      },
      {
        title: 'Author',
        field: 'message',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '180px' },
        class: '',
        translate: 'COMMON.COLUMN.CREATED_BY',
        autoHide: true,
        width: 182,
      },
      {
        title: 'Create by',
        field: 'createdBy',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'COMMON.COLUMN.CREATED_DATE',
        autoHide: true,
        width: 182,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 182,
      },
    ]
  }
  deleteALert(){
    
  }


}
export class changeLogModel {
  id:number;
  version:string;
  type:string;
  description:string;
  descriptionVi:string
}