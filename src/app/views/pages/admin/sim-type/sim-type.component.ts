import { Component, OnInit, ElementRef, ChangeDetectorRef, ViewChild, EventEmitter } from '@angular/core';
import { finalize, takeUntil, tap } from 'rxjs/operators';
import { Subject, observable } from 'rxjs';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { SimTypeService, SimType } from '@core/admin';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataTable } from '@core/_base/layout/models/datatable.model';

declare var $: any;
const TITLE_FORM_EDIT: string = "COMMON.ACTIONS.EDIT";
const TITLE_FORM_ADD: string = "COMMON.ACTIONS.ADD";
const TITLE_FORM_DELETE: string = "COMMON.ACTIONS.DELETE";
enum ACTION { edit, add, delete };

@Component({
	selector: 'kt-sim-type',
	templateUrl: './sim-type.component.html',
	styleUrls: ['./sim-type.component.scss'],
})
export class SimTypeComponent implements OnInit {
	public resize:EventEmitter<any>;
	public dataTable: DataTable = new DataTable();
	action: string;
	popupEdit: {
		reason?,
		simType?: SimType
	};
	private unsubscribe: Subject<any>;
	public closeResult: string;
	public simTypeForm: FormGroup;
	public titlePopup: string = TITLE_FORM_ADD;
	private tableConfig: any;
	
	constructor(
		private cdr: ChangeDetectorRef,
		private modalService: NgbModal,
		private fb: FormBuilder,
		private simTypeService: SimTypeService
	) {
		this.action = "";
		this.tableConfig = {
			formSearch:"#formSearch",
			pagination: [
				10, 20, 30, 50
			],
			columns: [
				{
					title: '#',
					field: 'no',
					allowSort: false,
					isSort: false,
					dataSort: '',
					style: { 'width': '20px' },
					width: 20,
					class: 't-datatable__cell--center',
					translate: '#',
					autoHide: false,
				},
				{
					title: 'Key',
					field: 'nameKey',
					allowSort: true,
					isSort: false,
					dataSort: '',
					style: { 'width': '250px' },
					width: 182,
					class: '',
					translate: 'ADMIN.SIM_TYPE.GENERAL.NAME_KEY',
					autoHide: false,
				},
				{
					title: 'Name',
					field: 'nameKey2',
					allowSort: false,
					isSort: false,
					dataSort: '',
					style: { 'width': '250px' },
					width: 182,
					class: '',
					translate: 'ADMIN.SIM_TYPE.GENERAL.NAME',
				},
				{
					title: 'Created Date',
					field: 'createdDate',
					allowSort: false,
					isSort: false,
					dataSort: '',
					style: { 'width': '182px' },
					width: 182,
					class: '',
					translate: 'ADMIN.SIM_TYPE.GENERAL.CREATED_DATE',
				},
				{
					title: 'Sort Order',
					field: 'sortOrder',
					allowSort: true,
					isSort: false,
					dataSort: '',
					style: { 'width': '182px' },
					class: '',
					width: 182,
					translate: 'ADMIN.SIM_TYPE.GENERAL.SORT_ORDER',
				},
				{
					title: 'Actions',
					field: 'action',
					allowSort: false,
					isSort: false,
					dataSort: '',
					style: { 'width': '117px' },
					class: '',
					width: 117,
					translate: 'COMMON.ACTIONS.ACTIONS',
					autoHide: false,
				},
			]
		};
		this.unsubscribe = new Subject();
		this.dataTable.init({
			data: [],
			totalRecod: 0,
			paginationSelect: [1, 2, 5, 10, 20],
			columns: this.tableConfig.columns,
			formSearch: '#formSearch',
			isDebug: true,
			layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
				// selecter:true,
			  }
		});
		this.popupEdit = {};
	}
	ngOnInit() {
		// listening dataTable event
		this.dataTable.eventUpdate.pipe(
			tap(option => {
				this.getData(option);
			})
		).subscribe();
		this.dataTable.reload({});
		this.createForm();
		$(function () {
			$('.kt_selectpicker').selectpicker();
		});
	}

	/**
	 * Create form 
	 * @param simType model
	 */
	createForm(simType?: SimType) {
		this.simTypeForm = this.fb.group({
			nameKey: [(simType ? simType.nameKey : ""), Validators.required],
			sortOrder: [(simType ? simType.sortOrder : 1)],
		});
	}

	/**
	 * Get list by data option
	 * @param option option search list
	 */
	private getData(option) {
		this.simTypeService.list({ params: option }).pipe(
			tap((data: any) => {
				this.dataTable.update({
					data: data.result.content,
					totalRecod: data.result.totalRecord
				});
			}),
			takeUntil(this.unsubscribe),
			finalize(() => {
				this.cdr.markForCheck();
			})
		).subscribe();
	}

	/**
	 * Open popup template
	 * @param content template
	 * @param type action type
	 * @param data data for content 
	 */
	open(content, type, data?: any) {

		this.action = type;
		switch (type) {
			case 'edit':
				this.titlePopup = TITLE_FORM_EDIT;
				this.popupEdit.simType = new SimType();
				this.popupEdit.simType.id = data.id;
				this.popupEdit.simType.nameKey = data.name;
				this.popupEdit.simType.sortOrder = data.sortOrder;
				this.createForm(this.popupEdit.simType);
				break;
			case 'add':
				this.titlePopup = TITLE_FORM_ADD;
				this.popupEdit.simType = new SimType();
				this.createForm();
				break;
			case 'delete':
				this.titlePopup = TITLE_FORM_DELETE;
				this.popupEdit.simType = new SimType();
				this.popupEdit.simType.id = data.id;
				break;
		}
		this.modalService.open(content, { windowClass: 'kt-mt-50',backdrop: 'static' },).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			this.popupEdit.reason = reason;
		});
	}
	/**
	 * Dismiss Reason Popup
	 * @param reason 
	 */
	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}
	/**
	 * Submit actions
	 */
	onSubmit() {

		switch (this.action) {
			case "edit":
				if (!this.simTypeForm.invalid) {
					this.popupEdit.simType.nameKey = this.simTypeForm.get('nameKey').value;
					this.popupEdit.simType.sortOrder = this.simTypeForm.get('sortOrder').value;
					this.simTypeService.update(this.popupEdit.simType, { notifyGlobal: true }).pipe(
						tap(data => {
							if (data.status == 200) {
								this.modalService.dismissAll(this.popupEdit.reason);
								this.dataTable.reload({});
							}
						}),
						takeUntil(this.unsubscribe),
						finalize(() => {
							this.cdr.markForCheck();
						})
					).subscribe();
				}

				break;
			case "add":
				if (!this.simTypeForm.invalid) {
					this.popupEdit.simType.nameKey = this.simTypeForm.get('nameKey').value;
					this.popupEdit.simType.sortOrder = this.simTypeForm.get('sortOrder').value;
					this.simTypeService.create(this.popupEdit.simType, {
						notifyGlobal: true
					}).pipe(
						tap(data => {
							if (data.status == 201) {
								this.modalService.dismissAll(this.popupEdit.reason);
								this.dataTable.reload({});
							}
						}),
						takeUntil(this.unsubscribe),
						finalize(() => {
							this.cdr.markForCheck();
						})
					).subscribe();
				}

				break;
			case "delete":
				this.simTypeService.delete(this.popupEdit.simType.id, { notifyGlobal: true }).pipe(
					tap(data => {
						if (data.status == 200) {
							this.modalService.dismissAll(this.popupEdit.reason);
							this.dataTable.reload({});
						}
					}),
					takeUntil(this.unsubscribe),
					finalize(() => {
						this.cdr.markForCheck();
					})
				).subscribe();
				break;
		}
	}

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.simTypeForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}
	private check:boolean = true;
	public valueTest:string = "";
	test(){
		this.check= !this.check;
		if(this.check){
			this.dataTable.updateLayout({ width: 1024, height: 800 });
		}
		else{
			this.dataTable.updateLayout({ width: 600, height: 800 });
		}
	}
	onResize($elm) {
		// this.check= !this.check;
		// if(this.check){
		// 	this.valueTest = "123";
		// }
		// else{
		// 	this.valueTest = "abc";
		// }
		this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
		// this.cdr.markForCheck();
	}
}
