import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { finalize, takeUntil, tap ,debounceTime} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { TranslateService } from '@ngx-translate/core';
import { TransportType,TransportTypeService } from '@core/admin';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {CdkDragDrop, moveItemInArray,transferArrayItem} from '@angular/cdk/drag-drop';

declare var $: any;
@Component({
  selector: 'kt-transport-type',
  templateUrl: './transport-type.component.html',
  styleUrls: ['./transport-type.component.scss']
})
export class TransportTypeComponent implements OnInit {
  formTransportType: FormGroup;
  searchFormTransportType : FormGroup;
  selectdp: ElementRef;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idTransportTypeEdit: number;
  public dataDefault: any = [{}];
  public idTransportTypeDelete: any;
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  // qcvn
 
  constructor(
    private transport: TransportTypeService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal
  ) {
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    this.updateDataTable();
    this.getData();
 }

  private buildForm(): void {
    this.formTransportType = this.formBuilder.group({
      nameTransportType: ['',Validators.required],
      nameKey: ['',Validators.required],
      description: [''],
    });
    this.searchFormTransportType = this.formBuilder.group({
      nameTransportType: ['']
    })
  }

  buttonAddNew(content) {
    this.isEdit = false;
    this.formTransportType.reset();
    this.open(content);
    setTimeout(() => {
        $('.kt_selectpicker').selectpicker('refresh');
      }, 100);
  }
  
  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  

  onSubmitTransportType(form: any) {
    if (this.formTransportType.invalid) {
      return;
    }
    let TransportType = {
      name: form.value.nameTransportType,
      nameKey: form.value.nameKey,
      description: form.value.description,
    } as TransportType; 
    if(this.isEdit) {
      TransportType.id = this.idTransportTypeEdit;      
      this.transport.update(TransportType,{ notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          this.getData();
          this.modalService.dismissAll();
        }
      });
      return;
    }
    this.transport.create(TransportType,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        this.getData();
        this.modalService.dismissAll();

      }
    });
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
      orderBy:'sortOrder',
      orderType: 'ASC'
    };
  }

  onHideModal(id: string) {
    let idModal = '#' + id;
    $(idModal).modal('hide');
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }


  getIdAction(id,idModal) {
    this.idTransportTypeDelete = id;
    this.modalService.open(idModal, { windowClass: 'kt-mt-50  modal-holder modal-delete',backdrop:'static'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openModal(params) {
     let idModal = params.idModal;
     let confirm = params.confirm;
     if(confirm)
     {$(idModal).appendTo('body').modal({backdrop: 'static',keyboard: false})}
     else{$(idModal).appendTo('body').modal('show');}
  }

  deleteTransportType() {
    let id = this.idTransportTypeDelete;
    this.transport.delete(id, { notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.getData();
        this.modalService.dismissAll();
      }
    });
    
  }

  editTransportType(id,content) {
    this.isEdit = true;
    this.idTransportTypeEdit = id;
    let params = {
      id :id
    };
    this.transport.list(params).pipe(
      tap((data: any) => {
        this.dataDefault = data.result.content;
        setTimeout(() => {
            $('.kt_selectpicker').selectpicker('refresh');
          }, 100);
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
    this.open(content);
  }

  loadSelectBootstrap(){
		$('.bootstrap-select').selectpicker();
  }

  searchTransportType(form:any)
  {
    this.filter.name = form.value.nameTransportType;
    this.getData();
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter = option;
        this.getData();
      }),
      
    ).subscribe();
  }

  get f() {
   if(this.formTransportType != undefined) return this.formTransportType.controls;
  }

  getData() {
    this.transport.list(this.filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          // $(function () {
          //   $('select').selectpicker();
          // });
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect, 
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
				selecter:false,
			  }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
  }
  
  drop(event: CdkDragDrop<any[]>) {

    let data:any = [];
    moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    data = event.container.data.map((item,i)=>{
     
      let obj = {
        id:item.id,
        sortOrder:i
      };
      return obj;
    });
     this.transport.sortOrder(data, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.cdr.detectChanges();
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(this.cdr.markForCheck)
    ).toPromise();   
  }

  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '40px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Name',
        field: 'name',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'COMMON.COLUMN.NAME',
        autoHide: false,
        width: 182,
      },
      {
        title: 'key',
        field: 'limitspeed',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'ADMIN.POI_TYPE.GENERAL.KEY',
        autoHide: true,
        width: 182,
      },
      {
        title: 'QNCN',
        field: 'qcvnCode',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '500px' },
        class: '',
        translate: 'COMMON.COLUMN.DESCRIPTION',
        autoHide: true,
        width: 300,
      },
      {
        title: 'Create by',
        field: 'createdBy',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'COMMON.COLUMN.CREATED_BY',
        autoHide: true,
        width: 182,
      },
  
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 182,
      },
    ]
  }


}
