import { Component, OnInit, ChangeDetectorRef, NgModule, Input, Output, EventEmitter, ComponentFactoryResolver, Injector ,ViewChild} from '@angular/core';
import * as L from 'leaflet';
import { MapConfigModel, MapConfig, MapUtil, MapService, Item } from '@core/utils/map';
import { TollStationModel,TollStationService, TollRoadService } from '@core/admin';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { ValidatorCustomService, ToastService ,UserDateAdvPipe, AsciiPipe} from '@core/_base/layout';
import { TranslateService } from '@ngx-translate/core';
import { tap, takeUntil, finalize } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import { style } from '@angular/animations';
import "leaflet/dist/images/marker-shadow.png";
import * as moment from 'moment';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { DeviceConfigService } from '@core/common/_services/device-config.service';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { TransportType,TransportTypeService,GeoService } from '@core/admin';
import { Subject } from 'rxjs';
import { CurrencyPipe } from '@angular/common';

declare var $;
@Component({
  selector: 'kt-station-tool',
  templateUrl: './station-tool.component.html',
  styleUrls: ['./station-tool.component.scss'],
    providers: [UserDateAdvPipe,CurrencyPipe]

})
export class StationToolComponent implements OnInit {
  paramsCurrency: FormGroup;

  public mapConfig: MapConfigModel;
  @Input() listCountry:any;
  @Input() listDevices: Array<any>;
  @Input() poiInput?: TollStationModel;
  @Input() currentUser: any;
  @Output() poiResult: EventEmitter<{ status: string, message: string, data: any }>;
  public countryDefault:number = 237;
  public poiInfo: TollStationModel;
  public poiMap: TollStationModel;
  public poiForm: FormGroup;
  public currentLayer: any;
  public map: L.Map;
  public latlngArr: FormArray;
  public isCricle: boolean;
  public editSelectDeviceType:boolean = false;
  public listTollRoad:any;
  public listTollRoadbyWay:any;
  public optionsLayer: {
    color: string,
    fill: boolean,
    fillColor: string,
    fillOpacity: number,
    opacity: number,
    stroke: boolean
    weight: number,
  }
  private unsubscribe: Subject<any>;
  public editableLayers = new L.FeatureGroup();
  public options;
  public drawOptions = {
    position: 'topright',
    draw: {
      polyline: false,
      circle: {
        shapeOptions: {
        }
      },
      circlemarker: false,
      rectangle:true,
      marker: false,
      polygon:false
    },
    edit: {
      // featureGroup: this.editableLayers,
      edit: false,
      remove: false,
    },
  };
  public datetimeOptionsStartTime: any = { 
    key:'startTime',
    singleDatePicker: true, 
    size: 'md',
    timePicker: true, 
    format: 'datetime',
    autoSelect: false,
     btnClear: true, 
     optionDatePicker: { 
       autoPlay: true,
      drops:"up", 
    } ,
  
    };
  public datetimeOptionsEndTime: any = { singleDatePicker: true, 
    key:'endTime',
    size: 'md', 
    timePicker: true, 
    format: 'datetime', 
    autoSelect: false, 
    btnClear: true, 
    optionDatePicker: { 
      autoPlay: true,
      drops:"up",
    },

  };
  @ViewChild('startTimeCpn', { static: true }) startTimeCpn: DateRangePickerComponent;
  @ViewChild('endTimeCpn', { static: true }) endTimeCpn: DateRangePickerComponent;

  public beginDate:string;
  public expired:string;
  public parameters:any [];
  public moneyDefault:number = 10000;

  public layers = {};
  public typeAction: string;
  public listData: Array<any>;
  public layerDevice: L.FeatureGroup;
  public currentPopup: L.Layer;
  public showListDevice: boolean = false;
  private mapService: MapService;
  public checkTypeStation:number = 0;
  private latLngBounds:any;
  public closeResult: string;
  public listTransportType:any = [];
  public contentDescriptopn:any;
  public currencyDefault = "VND";
  public listCurrency:any = [];
  public listProvinces:any;
  public listTranportTypeCreated:any =[];
  public disableAdd:boolean = false;
  public typeTollStation:any = 0;
  public countryId:number = 237;
  // public listCity:any = [];
  constructor(
    private modalService: NgbModal,
    private transport:TransportTypeService,
    private deviceConfig: DeviceConfigService,
    private cdr: ChangeDetectorRef,
    // private poiService: poiService,
    private mapUtil: MapUtil,
    private fb: FormBuilder,
    private validatorCT: ValidatorCustomService,
    private translate: TranslateService,
    private toast: ToastService,
    private resolver: ComponentFactoryResolver,
    private injector: Injector,
    private sanitizer: DomSanitizer,
    private tollStationService: TollStationService,
    private userDateAdv:UserDateAdvPipe,
    private currenCy:CurrencyPipe,
    private geo:GeoService,
    private road: TollRoadService,
    private asciiPipe:AsciiPipe

    ) {

    // this.getListCity();
    this.typeAction = "add";
    this.mapService = new MapService(this.resolver,this.injector);

    this.options = this.mapService.init();
    this.isCricle = false;
    this.mapConfig = this.mapService.getConfigs();
    this.listData = [];
    this.poiResult = new EventEmitter();
    let roadmap = L.tileLayer(this.mapConfig.mapType.roadmap.layer['link'], this.mapConfig.mapType.roadmap.layer['option']);
    let satellite = L.tileLayer(this.mapConfig.mapType.satellite.layer['link'], this.mapConfig.mapType.satellite.layer['option']);

    this.layers[this.translate.instant(this.mapConfig.mapType.roadmap.translate)] = roadmap;
    this.layers[this.translate.instant(this.mapConfig.mapType.satellite.translate)] = satellite;

  }

  ngOnInit() {
    this.selectCoutry(this.countryDefault);
    this.getListCurrency();
    this.getListTransportType();
    this.getListTollRoad();
    this.resetVar();
    this.listData = this.processData(this.listDevices);
    this.mapService.config.options.elementProcessText = "mapProcessText";
    if (this.poiInput) {
      this.typeAction = "edit";
      this.poiInfo = new TollStationModel(this.poiInput);
      this.typeTollStation = this.poiInput.type;
      let points = this.mapUtil.decode(this.poiInfo.encodedPoints); 
    }
    else {
      this.poiInfo = new TollStationModel();
    $('#country').val(237).selectpicker('refresh');

    }
    // this.poiInfo = new TollStationModel();
    this.optionsLayer = {
      color: this.poiInfo.color,
      fill: this.poiInfo.fill,
      fillColor: this.poiInfo.fillColor,
      fillOpacity: this.poiInfo.fillOpacity,
      opacity: this.poiInfo.opacity,
      stroke: this.poiInfo.stroke,
      weight: this.poiInfo.weight,
    };

    this.createForm(this.poiInfo);
    this.cdr.detectChanges();
  }

  private buildForm(): void {
    if(!this.paramsCurrency)  this.paramsCurrency = this.fb.group({});
    this.parameters.map(item=>{
      
      let name = "name_"+item.id;
      this.paramsCurrency.controls[name] = new FormControl({ value: item.value, disabled: false }); 
      return item;
    })
    

  }

  onDrawReady(drawControl) {
    // Do stuff with map     
  }

  tabGlobal(){
    $('.kt-map').css('display','block');
    $('.kt-left').css('width','auto');

  }

  tabAdvanced(){
    $('.kt-map').css('display','none');
    $('.kt-left').css('width','100%');
    setTimeout(()=>{
      $('.select-currency').val(this.currencyDefault).selectpicker('refresh');
    })
  }

  async getListTollRoad(){
   await this.road.list({pageNo:-1}).subscribe(data=>{
      
      if(data.status == 200)
      {
        this.listTollRoad = data.result;
        this.listTollRoad.map(city=>{
          city.textSearch = this.asciiPipe.transform(city.name || "");
          return city;
        })
        this.getListTollRoadByHighway(this.typeTollStation);
          setTimeout(()=>{
            $('#tollRoad').selectpicker();
          })
      }
    })
  }


  getListTollRoadByHighway(isHighWay,roadId?:number)
  {
    if(!this.listTollRoad) return;
    this.typeTollStation = isHighWay;
    let stand:any = [];
    let highway:any = [];
    this.listTollRoad.map(country=>{
        country.textSearch = this.asciiPipe.transform(country.name || "");
        if(country.typeName == "HIGHWAY" && country.countryId == this.countryId) highway.push(country);
        if(country.typeName != "HIGHWAY" && country.countryId == this.countryId) stand.push(country);
        return country;
      });

      if(isHighWay == 1)
      {this.listTollRoadbyWay = highway;
      }else{this.listTollRoadbyWay = stand;}


    setTimeout(()=>{
      if(roadId)
      {
        $('#tollRoad').val(roadId).selectpicker('refresh');
      }else{
       if(this.poiForm && this.typeAction != 'edit')  this.poiForm.value.tollRoad = undefined;
      $('#tollRoad').val(undefined).selectpicker('refresh');
      }
    })
  }

  getListCurrency()
  {
    this.listCountry.map(country=>{
      if(!this.listCurrency.find(x=>x.name == country.currencyCode))
      {
        this.listCurrency.push({name:country.currencyCode});

      }
    });
 
  }

  changeCurrency(Currency)
  {
    let newCurrency = Currency.target.value;
    this.parameters.map(params=>{
      params.price = 0;
      params.value = 0;
      params.currencyUnit = newCurrency;
      let name = "name_"+params.id;
      let object = {};
       object[name] = 0;
      this.paramsCurrency.patchValue(object);
      return params;
    });  
    this.currencyDefault = newCurrency;
    this.poiInfo.info = this.parameters;

    setTimeout(()=>{
      $('.select-currency').val(this.currencyDefault).selectpicker('refresh');
    })

  }

  selectCoutry(item)
  {
    let countryId;
    if(typeof item == 'number')
    {
      countryId = item;
    }  else
    {
      countryId = item.target.value
    }
    this.countryId = countryId;
    this.setStoreCountry(countryId);
    this.getListTollRoadByHighway(this.typeTollStation);

  }

  filterRoadByCountryId(countryId)
  {
    if(this.listTollRoad) this.listTollRoadbyWay= this.listTollRoad.filter(road=> road.countryId == countryId);
  
  }

  setStoreCountry(countryId)
  {
    let name="listCountry_";
    name = name+countryId;
    if(localStorage.getItem(name) == undefined)
    {
      let params = {
        countryId:countryId
      };
      this.geo.listProvince(params).subscribe(data=>{
        this.listProvinces = data.result;
        this.listProvinces.map(city=>{
          city.textSearch = this.asciiPipe.transform(city.name || "");
          return city;
        })
        localStorage.setItem(name,  JSON.stringify(this.listProvinces));
        
      })
    }else{
      this.listProvinces = localStorage.getItem(name);
      this.listProvinces = JSON.parse(this.listProvinces);
    }
    

    if(this.poiForm) this.poiForm.value.city = undefined;
    if(this.poiForm)  this.poiForm.value.tollRoad = undefined;
    setTimeout(()=>{
      $('#tollRoad').val(undefined).selectpicker('refresh');
      $('#listProvinces').val(undefined).selectpicker('refresh');
    })

    }

  async listPriceDefault(){
    this.currencyDefault = "VND";
    let listPrice = [35000,50000,75000,120000,180000];
    this.parameters = this.listTransportType;
    this.parameters .map((transport,i) =>{
      let value = listPrice[i];
      transport.value = value;
      transport.price = value;
      return transport;
    });

    
  }

  addRepeaterForm(classDiv,initEmpty = false){
    setTimeout(() => {
      $(classDiv).repeater({
        initEmpty: initEmpty,      
        defaultValues: {
        
        },
        show: function () {
          $(this).slideDown();
        },
        hide: function (deleteElement) {                
          $(this).slideUp(deleteElement);                 
        }   
      });
    }, 600);
  }

  resetVar(){
    this.addRepeaterForm('#kt_repeater_1',true);
    // setTimeout(()=>{
    //   $('select').selectpicker();

    // })

  }


  onMapReady(map) {    
    this.map = map;
    L.control.layers(this.layers).addTo(this.map);
    this.mapService.setMap(this.map);
    this.renderpoi(this.poiInput, true);
  }

  createForm(poi: TollStationModel) {
    // covert points
    poi.latlngs.map(x => {
      x.lat = parseFloat(parseFloat(x.lat.toString()).toFixed(5));
      x.lng = parseFloat(parseFloat(x.lng.toString()).toFixed(5));
      return x;
    });
    poi.points = poi.latlngs.map(x => {
      return [x.lat, x.lng]
    });
    if (poi.radius) poi.radius = parseFloat(parseFloat(poi.radius.toString()).toFixed(5));
    this.poiForm = this.fb.group({
      name: new FormControl({ value: poi.name, disabled: false }, [Validators.required, this.validatorCT.maxLength(64)]),
      typeStation: new FormControl({ value: poi.type, disabled: false },[]),
      geofence: new FormControl({ value: poi.typeGeofence, disabled: false },[]),
    
      tollRoad: new FormControl({ value: poi.roadId, disabled: false },[]),

      city: new FormControl({ value: poi.provinceId, disabled: false },[]),
      active: new FormControl({ value: poi.active, disabled: false },[]),
      description: new FormControl({ value: poi.description, disabled: false }, [this.validatorCT.maxLength(256)]),
      radius: new FormControl({ value: poi.radius, disabled: false }, [this.validatorCT.float]),
      color: new FormControl({ value: poi.color, disabled: false }),
      opacity: new FormControl({ value: poi.opacity * 10, disabled: false }),
      fillColor: new FormControl({ value: poi.fillColor, disabled: false }),
      fillOpacity: new FormControl({ value: poi.fillOpacity * 10, disabled: false }),
      latlngArr: this.fb.array([]),
      listCurrency:[]
    });

    this.poiInfo.neLat = poi.neLat 
    this.poiInfo.neLng = poi.neLng 
    this.poiInfo.swLat = poi.swLat 
    this.poiInfo.swLng = poi.swLng 

    // let polygon1: any = new L.Rectangle([
    //   [poi.neLat,poi.neLng],
    //   [poi.neLat,poi.swLng],
    //   [poi.swLat,poi.swLng],
    //   [poi.swLat,poi.neLng],
    // ]);
    // if(polygon1) polygon1.addTo(this.map);

    this.poiInfo.latLngBounds = poi.latLngBounds;
    this.checkTypeStation =  poi.type;
    if(poi.endTime != null){
      this.datetimeOptionsEndTime['startDate'] = this.userDateAdv.transform(poi.endTime,'YYYY-MM-DD HH:mm:ss');
      this.datetimeOptionsEndTime['endDate'] = this.userDateAdv.transform(poi.endTime,'YYYY-MM-DD HH:mm:ss');
      this.datetimeOptionsEndTime['autoSelect'] = true;
    }
    if(poi.startTime != null){
      this.datetimeOptionsStartTime['startDate'] = this.userDateAdv.transform(poi.startTime,'YYYY-MM-DD HH:mm:ss');
      this.datetimeOptionsStartTime['endDate'] = this.userDateAdv.transform(poi.startTime,'YYYY-MM-DD HH:mm:ss');
      this.datetimeOptionsStartTime['autoSelect'] = true;
    }

    //parameter
    if(!this.parameters)
    {
      this.listPriceDefault();
      this.buildForm();
      if(poi.info.length > 0)
      {
        
        this.currencyDefault = this.poiInfo.currencyUnit;      
        poi.info.map(itemInfo=>{
          itemInfo.id = itemInfo.transportType;
          itemInfo.value = itemInfo.price;
          if(this.listTransportType.find(x=>x.id == itemInfo.transportType))
          {
            itemInfo.name = this.listTransportType.find(x=>x.id == itemInfo.transportType).name;
            itemInfo.description = this.listTransportType.find(x=>x.id == itemInfo.transportType).description;
          } 
         
          return itemInfo;
        });
        this.parameters = poi.info;
        if(this.parameters.length != this.listTransportType.length) this.disableAdd = true;
        this.buildForm();

      }
    }
      // if(!this.listTollRoad) return;
      // this.typeTollStation = isHighWay;

    // this.buildForm();
    this.countryId = poi.countryId;
    setTimeout(()=>{

      this.getListTollRoadByHighway(poi.type,poi.roadId);
      this.poiForm.value.tollRoad = poi.roadId;
      // $('select').val(poi.provinceId).selectpicker('refesh');
      $('#tollRoad').val(poi.roadId).selectpicker('refresh');
      $('#country').val(poi.countryId).selectpicker('refresh');
      $('#listProvinces').val(poi.provinceId).selectpicker('refresh');

    });
    if (poi.latlngs.length > 0) {
      this.latlngArr = this.poiForm.get('latlngArr') as FormArray;
      this.latlngArr.clear();
      
      poi.latlngs.map(x => {
        this.latlngArr.push(this.createLatlng(x));
      });
    }
    this.poiInfo = Object.assign(this.poiInfo, poi);
    // this.updateLayout();

  }
  getOptions(poi: TollStationModel) {
    return {
      color: poi.color,
      fill: poi.fill,
      fillColor: poi.fillColor,
      fillOpacity: poi.fillOpacity,
      opacity: poi.opacity,
      stroke: poi.stroke,
      weight: poi.weight,
    };
  }

  changePrice(event,item)
  {
    let price = event.target.value;   

    let idTransport = item.id;
      this.parameters.map(params=>{
      price = this.remove_character(',',price);
      price = parseFloat(price);
      if(params.id == idTransport) 
      {
        params.value = price
        params.price = price

      }
      return params;
    });
    
  }

 addTollStationToCache()
  {
    let params = {
        pageNo:-1
    };

   let name="listTollStation";
   this.tollStationService.list({ params:params})
        .pipe(
            takeUntil(this.unsubscribe),
            finalize(() => {
              this.cdr.markForCheck();
            })
          )
        .subscribe(data=>{
  
            if(data.status == 200)
            {
              let  listTollStation = data.result;
              listTollStation.map(country=>{
                  country.textSearch = this.asciiPipe.transform(country.name || "");
                  return country;
                });
                sessionStorage.setItem(name,JSON.stringify(listTollStation));

            }
          })
  }

  removePrice(idTransport)
  {
    this.disableAdd = true;
    this.editSelectDeviceType = false;

    this.parameters = $.grep(this.parameters, function(e){ 
      return e.id != idTransport; 
    });
    // this.getlistTranportTypeCreated();

  }

   remove_character(str_to_remove, str) {
    let reg = new RegExp(str_to_remove);
    str = str.toString().replace(/,/g, "");
    // str = str.toString().replace(',', '');
    // str = str.replace(',', '');

    return str;
  }

 dateSelectChangeExpired(data) {
    this.expired = '';
   if(data.endDate != null) this.expired = data.endDate.format("YYYY-MM-DD");  
   this.poiInfo.endTime = this.expired;
   
 }

 dateSelectChangeBegin(data) {
   this.beginDate = '';
   if(data.startDate != null)this.beginDate = data.startDate.format("YYYY-MM-DD");  
   this.poiInfo.startTime = this.beginDate;
   
 }



//  renderFormtpMapTest(poi: TollStationModel)
//  {
//   let _this = this;
//   if (poi) {
//     let points = _this.mapUtil.decode(this.poiInfo.encodedPoints);
//     switch (poi.typeGeofence) {
//       case "rectangle":
//         let polygon: any = new L.Rectangle(points);
//         this.currentLayer = polygon;
//         _this.poiInfo.latLngBounds = _this.currentLayer.getBounds(),{padding:[0,0]};
//         _this.poiInfo.neLat = _this.poiInfo.latLngBounds['_northEast'].lat;
//         _this.poiInfo.neLng = _this.poiInfo.latLngBounds['_northEast'].lng;
//         _this.poiInfo.swLat = _this.poiInfo.latLngBounds['_southWest'].lat;
//         _this.poiInfo.swLng = _this.poiInfo.latLngBounds['_southWest'].lng;  
        

//         poi.latlngs = points.map(x => {
//           return { lat: x[0], lng: x[1] };
//         })
//         // this.setLatlngForm(latlngs);
//         this.currentLayer.addTo(this.map);
//         case "circle":
//             let circle: any = new L.Circle(points[0], poi.radius);
//         this.currentLayer = circle;
//         this.currentLayer.options = this.getOptions(poi);
//         this.currentLayer.layerType = "circle";
//         this.currentLayer.editing.enable();
//         this.currentLayer.on('edit', function (e) {            
//           _this.layerEdited(e.target);
//           _this.poiInfo.latLngBounds = _this.currentLayer.getBounds(),{padding:[0,0]};

//           _this.poiInfo.neLat = _this.poiInfo.latLngBounds['_northEast'].lat;
//           _this.poiInfo.neLng = _this.poiInfo.latLngBounds['_northEast'].lng;
//           _this.poiInfo.swLat = _this.poiInfo.latLngBounds['_southWest'].lat;
//           _this.poiInfo.swLng = _this.poiInfo.latLngBounds['_southWest'].lng;  
//         });
//         poi.latlngs = points.map(x => {
//           return { lat: x[0], lng: x[1] };
//         })
        
//     }

//   }
//  }



 renderFormtpMap(poi: TollStationModel, fitBound?: boolean)
 {
  let _this = this;
  if (poi) {
    let points = _this.mapUtil.decode(this.poiInfo.encodedPoints);
    this.isCricle = false;
    if (this.currentLayer) this.currentLayer.remove();
    switch (poi.typeGeofence) {
      case "rectangle":
        let polygon: any = new L.Rectangle(points);
        this.currentLayer = polygon;
        this.currentLayer.options = this.getOptions(poi);
        this.currentLayer.layerType = "rectangle";
        this.currentLayer.editing.enable();
        this.currentLayer.on('edit', function (e) {
          _this.layerEdited(e.target);
          _this.poiInfo.latLngBounds = _this.currentLayer.getBounds(),{padding:[0,0]};
          _this.poiInfo.neLat = _this.poiInfo.latLngBounds['_northEast'].lat;
          _this.poiInfo.neLng = _this.poiInfo.latLngBounds['_northEast'].lng;
          _this.poiInfo.swLat = _this.poiInfo.latLngBounds['_southWest'].lat;
          _this.poiInfo.swLng = _this.poiInfo.latLngBounds['_southWest'].lng;  
        });
        

        poi.latlngs = points.map(x => {
          return { lat: x[0], lng: x[1] };
        })
        // this.setLatlngForm(latlngs);
        this.currentLayer.addTo(this.map);
        if (fitBound) {            
          let _this= this;
          setTimeout(()=>{
            _this.map.fitBounds(_this.currentLayer.getBounds(),{padding:[50,50]});
          })
        }
        break;
      case "circle":
    
        this.isCricle = true;
        let circle: any = new L.Circle(points[0], poi.radius);
        this.currentLayer = circle;
        this.currentLayer.options = this.getOptions(poi);
        this.currentLayer.layerType = "circle";
        this.currentLayer.editing.enable();
        this.currentLayer.on('edit', function (e) {            
          _this.layerEdited(e.target);
          _this.poiInfo.latLngBounds = _this.currentLayer.getBounds(),{padding:[0,0]};

          _this.poiInfo.neLat = _this.poiInfo.latLngBounds['_northEast'].lat;
          _this.poiInfo.neLng = _this.poiInfo.latLngBounds['_northEast'].lng;
          _this.poiInfo.swLat = _this.poiInfo.latLngBounds['_southWest'].lat;
          _this.poiInfo.swLng = _this.poiInfo.latLngBounds['_southWest'].lng;  
        });
        poi.latlngs = points.map(x => {
          return { lat: x[0], lng: x[1] };
        })
        
        this.currentLayer.addTo(this.map);
         if (fitBound) {            
          let _this= this;
          setTimeout(()=>{
            _this.map.fitBounds(_this.currentLayer.getBounds(),{padding:[50,50]});
          })
        }
        break;
        case "marker":            
          let marker: any = new L.Marker(poi.points[0]);
          this.currentLayer = marker;
          // this.currentLayer.options = this.getOptions(poi);
          this.currentLayer.layerType = "marker";         
          this.currentLayer.addTo(this.map);
          if (fitBound) {
            this.map.fitBounds(poi.points, { padding: [50, 50] });
            if (this.map.getZoom() > 1) {
              this.map.setZoom(this.map.getZoom() - 1);
            }
          }
          break;
    }

  }
 }

  renderpoi(poi: TollStationModel, fitBound?: boolean) {
    let _this = this;
    if (poi) {
      
    let polygon1: any = new L.Rectangle([
      [poi.neLat,poi.neLng],
      [poi.neLat,poi.swLng],
      [poi.swLat,poi.swLng],
      [poi.swLat,poi.neLng],
    ],{ color:'#abb8c9'});
    // polygon1.options = {
    //   color:'#abb8c9',
      
    // };
    if(polygon1) polygon1.addTo(this.map);
      let points = _this.mapUtil.decode(this.poiInfo.encodedPoints);
      this.isCricle = false;
      if (this.currentLayer) this.currentLayer.remove();
      switch (poi.typeGeofence) {
        case "rectangle":
          let polygon: any = new L.Rectangle(points);
          this.currentLayer = polygon;
          this.currentLayer.options = this.getOptions(poi);
          this.currentLayer.layerType = "rectangle";
          this.currentLayer.editing.enable();
          this.currentLayer.on('edit', function (e) {
            _this.layerEdited(e.target);
            _this.poiInfo.latLngBounds = _this.currentLayer.getBounds(),{padding:[0,0]};
            _this.poiInfo.neLat = _this.poiInfo.latLngBounds['_northEast'].lat;
            _this.poiInfo.neLng = _this.poiInfo.latLngBounds['_northEast'].lng;
            _this.poiInfo.swLat = _this.poiInfo.latLngBounds['_southWest'].lat;
            _this.poiInfo.swLng = _this.poiInfo.latLngBounds['_southWest'].lng;  
          });

          poi.latlngs = points.map(x => {
            return { lat: x[0], lng: x[1] };
          })
          // this.setLatlngForm(latlngs);
          this.createForm(poi);
          this.currentLayer.addTo(this.map);
          if (fitBound) {            
            let _this= this;
            setTimeout(()=>{
              _this.map.fitBounds(_this.currentLayer.getBounds(),{padding:[50,50]});
            })
          }
          break;
        case "circle":
      
          this.isCricle = true;
          let circle: any = new L.Circle(points[0], poi.radius);
          this.currentLayer = circle;
          this.currentLayer.options = this.getOptions(poi);
          this.currentLayer.layerType = "circle";
          this.currentLayer.editing.enable();
          this.currentLayer.on('edit', function (e) {            
            _this.layerEdited(e.target);
            _this.poiInfo.latLngBounds = _this.currentLayer.getBounds(),{padding:[0,0]};

            _this.poiInfo.neLat = _this.poiInfo.latLngBounds['_northEast'].lat;
            _this.poiInfo.neLng = _this.poiInfo.latLngBounds['_northEast'].lng;
            _this.poiInfo.swLat = _this.poiInfo.latLngBounds['_southWest'].lat;
            _this.poiInfo.swLng = _this.poiInfo.latLngBounds['_southWest'].lng;  
          });
          poi.latlngs = points.map(x => {
            return { lat: x[0], lng: x[1] };
          })
          
          this.currentLayer.addTo(this.map);
   
          this.createForm(poi);
          if (fitBound) {            
            let _this= this;
            setTimeout(()=>{
              _this.map.fitBounds(_this.currentLayer.getBounds(),{padding:[50,50]});
            })
          }
          break;
          case "marker":            
            let marker: any = new L.Marker(poi.points[0]);
            this.currentLayer = marker;
            // this.currentLayer.options = this.getOptions(poi);
            this.currentLayer.layerType = "marker";         
            this.currentLayer.addTo(this.map);
            if (fitBound) {
              this.map.fitBounds(poi.points, { padding: [50, 50] });
              if (this.map.getZoom() > 1) {
                this.map.setZoom(this.map.getZoom() - 1);
              }
            }
            break;
      }

    }

  }

  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  showQuestionDeviceType(content)
  {

    this.open(content);
  }

  getlistTranportTypeCreated()
  {
    this.listTranportTypeCreated = [];
    this.listTransportType.forEach(element => {
      let id = element.id;
      if(!this.parameters.find( x=>x.id == id))
      {
        element.value = 0;
        element.price = 0;
        element.transportType = id;
        this.listTranportTypeCreated.push(element);
      }
    });
    this.parameters.push(this.listTranportTypeCreated[0]);
    this.buildForm();
    setTimeout(() => {
     $('.select-devicetype').selectpicker('refresh'); 
    });
  }

  changeTypeDevice(newTranport)
  {
    this.editSelectDeviceType = false;
    let idNew = parseInt(newTranport.target.value);
    this.parameters.pop();
    let newItem =  this.listTransportType.find(x=>x.id == idNew);
    this.parameters.push(newItem);
    $('.dropdown-menu.show').removeClass('show');


  }

  test(number)
  {
    if(number == this.parameters.length-1) return true;
    return false;
  }

  createParamsets(){
    this.editSelectDeviceType = true;
    this.getlistTranportTypeCreated();
    
    this.disableAdd = false;
    if(this.parameters.length != this.listTransportType.length) this.disableAdd = true;

    setTimeout(() => {
      document.getElementById('croll-to').scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' });
    });
  }

  onReset() {
    this.poiForm.reset();
  
    this.renderpoi(this.poiInput, true);
    // setTimeout( () => {
    //   $('select').selectpicker('refresh');
    // });
  }


  renderLatlngs(latlngs, radius, type: string) {    
    this.poiInfo.radius = radius || 0;
    this.poiInfo.typeGeofence = type || "";
    this.poiInfo.type = this.poiForm.value.typeStation;
    this.poiInfo.name = this.poiForm.value.name;
    this.poiInfo.provinceId = this.poiForm.value.city;
    this.poiInfo.roadId = this.poiForm.value.tollRoad;

    this.poiInfo.active = this.poiForm.value.active;
    $('#country').val(237).selectpicker('refresh');

    this.poiInfo.points = latlngs.map(x => {
      return [x.lat, x.lng];
    })
    this.poiInfo.latlngs = latlngs;
    this.poiInfo.encodedPoints = this.mapUtil.encode(this.poiInfo.points);
    // covert points
 
    this.createForm(this.poiInfo);
    this.cdr.detectChanges();
  }
  setLatlngForm(latlngs: Array<{ lat: number, lng: number }>) {
    this.latlngArr = this.poiForm.get('latlngArr') as FormArray;
    this.latlngArr.clear();
    latlngs.map(x => {
      this.latlngArr.push(this.createLatlng(x));
    });
  }
  onCreated(layer) {
    layer.layer.editing.enable();
    this.editableLayers.addLayer(layer.layer);
    let _this = this;
    if (this.currentLayer) {
      this.currentLayer.remove();
    }
    
    this.currentLayer = layer.layer;
    let latlngs;
    let radius;
    let type;
    this.isCricle = false;
    this.poiInfo.latLngBounds = this.currentLayer.getBounds(),{padding:[0,0]};

    this.poiInfo.neLat = this.poiInfo.latLngBounds['_northEast'].lat;
    this.poiInfo.neLng = this.poiInfo.latLngBounds['_northEast'].lng;
    this.poiInfo.swLat = this.poiInfo.latLngBounds['_southWest'].lat;
    this.poiInfo.swLng = this.poiInfo.latLngBounds['_southWest'].lng;   
    
    switch (layer.layerType) {
      case "rectangle":
        let rectangle: L.Rectangle = layer.layer;
        latlngs = rectangle.getLatLngs()[0];
        type = "rectangle";
        break;
      case "circle":
        let circle: L.Circle = layer.layer;
        latlngs = [circle.getLatLng()];
        radius = circle.getRadius();
        // circle.setStyle(this.optionsLayer);
        this.isCricle = true;
        type = "circle";
        break;
      case "polygon":
        let polygon: L.Polygon = layer.layer;
        latlngs = polygon.getLatLngs()[0];

        type = "polygon";
        break;
        case "marker":
          let marker: L.Marker = layer.layer;
          latlngs = marker.getLatLng();
          latlngs = [latlngs];
          type = "marker";
          break;
    }

    this.currentLayer['layerType'] = layer.layerType;
    // set options
    this.currentLayer.options = this.optionsLayer;
    this.currentLayer.on('edit', function (e) {
      _this.layerEdited(e.target);
    });
    
    this.renderLatlngs(latlngs, radius, type);
  }

  layerEdited(layer) {
    let latlngs;
    let radius;
    this.isCricle = false;
    let type;
    switch (layer.layerType) {
      case "rectangle":
        let rectangle: L.Rectangle = layer;
        latlngs = rectangle.getLatLngs()[0];
        type = "rectangle";
        break;
      case "circle":
        let circle: L.Circle = layer;
        latlngs = [circle.getLatLng()];
        radius = circle.getRadius();
        this.isCricle = true;
        type = "circle";
        break;
      case "polygon":
        let polygon: L.Polygon = layer;
        latlngs = polygon.getLatLngs()[0];
        type = "polygon";
        break;
      case "marker":
          let marker: L.Marker = layer.layer;
          latlngs = marker.getLatLng();
          latlngs = [latlngs];
          type = "marker";
          break;
    }
    this.renderLatlngs(latlngs, radius, type);
  }
  onResize($elm) {
    this.mapService.resize();

    // let clientRectFooter = document.getElementById('poiFooter').getBoundingClientRect();
    // let clientRect = document.getElementById('poiConfig').getBoundingClientRect();
    // let maxHeightWapper = clientRect.height - (clientRectFooter.height);

    // setTimeout(() => {
    //   $("#poiBody").css("max-height", maxHeightWapper);
    // });

    this.cdr.detectChanges();
  }
  updateLayout() {
    this.mapService.resize();
    let clientRectFooter = document.getElementById('poiFooter').getBoundingClientRect();
    let clientRect = document.getElementById('poiConfig').getBoundingClientRect();
    let maxHeightWapper = clientRect.height - (clientRectFooter.height);

    setTimeout(() => {
      $("#poiBody").css("max-height", maxHeightWapper);
    });

    this.cdr.detectChanges();
  }

  getListTransportType(){
    this.contentDescriptopn = '';   
     
    if(sessionStorage.getItem('listTransportType1') == undefined)
    {
      this.transport.list({pageNo:-1}).pipe(
        tap((data: any) => {
       
          this.listTransportType = data.result;
          this.mapListTransportType();
          sessionStorage.setItem('listTransportType1',  JSON.stringify(data.result));

        }),
       
        finalize(() => {
          this.cdr.markForCheck();
        })
      ).subscribe();
    }else{
      this.listTransportType = sessionStorage.getItem('listTransportType1');
      this.listTransportType = JSON.parse(this.listTransportType);
      
      this.mapListTransportType();
    }

  }

  mapListTransportType()
  {
      this.listTransportType.map(transport =>{
      this.contentDescriptopn = this.contentDescriptopn + '- '+ transport.name+ ': ' + transport.description + "<br>";
      return transport;
      });
  }

  onSubmit() {
    
    // console.log(this.poiForm.value.tollRoad );

    if(this.poiForm.value.city == undefined || this.poiForm.value.city == ''){
      this.toast.show({message:this.translate.instant('ADMIN.TOLL_STATION.MESSAGE.PlEASE_COUNTRY'),type:'error'});
      return;
    }
//  console.log(this.poiForm.value.tollRoad);
    if(this.poiForm.value.tollRoad == undefined || this.poiForm.value.tollRoad == ''){
      this.toast.show({message:this.translate.instant('ADMIN.TOLL_STATION.MESSAGE.PLEASE_ROAD'),type:'error'});
      return;
    }

    this.poiForm.markAllAsTouched();
    setTimeout(() => {
      const firstElementWithError = document.querySelector('#formpoiTool .invalid-feedback');
      if (firstElementWithError) {
        firstElementWithError.scrollIntoView({ block: 'center' });
      }
    });
    
    if(this.poiInfo.typeGeofence.length == 0) 
    {
      this.toast.show({message:this.translate.instant('ADMIN.TOLL_STATION.MESSAGE.CREATE_TOLL_ONMAP'),type:'error'});
      return;
    }


    this.formChange();
    
    if (!this.poiForm.invalid && this.poiInfo.typeGeofence.length > 0) {      
      // this.poiInfo.encodedPoints = this.mapUtil.encode(this.poiInfo.points);
  
      if (this.typeAction == "edit") {
        this.tollStationService.update(this.poiInfo, { notifyGlobal: true }).pipe(
          tap(data => {
            if (data.status == 200) {
              this.poiResult.emit({ status: "success", message: "", data: data.result });
            }
          }),
          finalize(() => {
            this.cdr.markForCheck();
          })
        ).subscribe();
      }
      else {
        this.tollStationService.create(this.poiInfo, { notifyGlobal: true }).pipe(
          tap(data => {
            if (data.status == 201) {
              this.poiResult.emit({ status: "success", message: "", data: data.result });
            }
          }),
          finalize(() => {
            this.cdr.markForCheck();
          })
        ).subscribe();
      }
      this.addTollStationToCache();
    }
    else {
      
      // console.log(this.poiInfo);

      // console.log( this.poiInfo.points);
      // console.log( this.poiInfo.latlngs.map(x=>{ return JSON.stringify(x)}).join(","));

      // console.log(2);
    }
  }

  formChangeMap()
  {
    this.poiInfo.radius = Number.parseFloat(this.poiForm.get('radius').value);
    this.poiInfo.color = this.poiForm.get('color').value;
    this.poiInfo.opacity = this.poiForm.get('opacity').value / 10;
    this.poiInfo.fillColor = this.poiForm.get('fillColor').value;
    this.poiInfo.fillOpacity = this.poiForm.get('fillOpacity').value / 10;
        
    // set point
    if (this.formLatLngs.value.length > 0) {
      this.poiInfo.points = this.formLatLngs.value.map(x => {
        return [x.lat, x.lng];
      })
    }
    this.renderFormtpMap(this.poiInfo,true);

  }

  formChange() {
    let _this = this;
    this.poiForm.markAllAsTouched();    
    if (this.poiForm.invalid && !this.poiForm.get('name').errors) return;
    this.poiInfo.name = this.poiForm.get('name').value;
    this.poiInfo.provinceId = this.poiForm.get('city').value;
    this.poiInfo.roadId = this.poiForm.get('tollRoad').value;

    this.poiInfo.type = 0;
    this.poiInfo.type = this.checkTypeStation;
    this.poiInfo.startTime = this.beginDate;
    this.poiInfo.endTime = this.expired;
    this.poiInfo.currencyUnit = this.currencyDefault;

    // this.poiInfo.typeGeofence = this.poiForm.get('typeStation').value;
    this.poiInfo.active = this.poiForm.get('active').value
    this.poiInfo.description = this.poiForm.get('description').value;
    this.poiInfo.radius = Number.parseFloat(this.poiForm.get('radius').value);
    this.poiInfo.color = this.poiForm.get('color').value;
    this.poiInfo.opacity = this.poiForm.get('opacity').value / 10;
    this.poiInfo.fillColor = this.poiForm.get('fillColor').value;
    this.poiInfo.fillOpacity = this.poiForm.get('fillOpacity').value / 10;
    
    // set point
    if (this.formLatLngs.value.length > 0) {
      this.poiInfo.points = this.formLatLngs.value.map(x => {
        return [x.lat, x.lng];
      })
    }
    // convert map paramsmeter
    let parameters:any = [];    
    this.parameters.map((params,i)=>{
      parameters.push({
        transportType : params.id,
        // currencyUnit : this.currencyDefault,
        price : params.value
      });
      return params;
    });
    this.poiInfo.info = parameters;
    // set options
    this.optionsLayer = {
      color: this.poiInfo.color,
      fill: this.poiInfo.fill,
      fillColor: this.poiInfo.fillColor,
      fillOpacity: this.poiInfo.fillOpacity,
      opacity: this.poiInfo.opacity,
      stroke: this.poiInfo.stroke,
      weight: this.poiInfo.weight,
    };

    this.renderpoi(this.poiInfo, true);


  }
  createLatlng(latlng: { lat: number, lng: number }): FormGroup {
    return this.fb.group({
      lat: [latlng.lat, this.validatorCT.float],
      lng: [latlng.lng, this.validatorCT.float]
    });
  }
  get formLatLngs(): FormArray {
    if (this.poiForm)
      return this.poiForm.get('latlngArr') as FormArray;
  }

  get f() {
    if (this.poiForm) return this.poiForm.controls;
  }
  onSetting($elm) {
    // let clientRectHeader = document.getElementById('poiHeader').getBoundingClientRect();
    // let clientRectFooter = document.getElementById('poiFooter').getBoundingClientRect();
    // let clientRect = document.getElementById('poiConfig').getBoundingClientRect();
    // let maxHeightWapper = clientRect.height - (clientRectFooter.height);
    // $("#poiBody").css("max-height", maxHeightWapper);
    // this.cdr.detectChanges();
  }
  processData(data) {
    let _this = this;
    const icons = _this.mapConfig.icons;
    const status = this.mapConfig.status;
    let dataNew = data.map(x => {
      let indexIcon = 0;
      if (x['icon']['name']) {
        let indexIconSearch = _this.mapConfig.icons.findIndex(i => i.name == x['icon']['name']);
        if (indexIconSearch >= 0) {
          indexIcon = indexIconSearch;
        }
      }
      x.iconType = Object.assign({}, icons[indexIcon]);
      x.iconTypeProcess = Object.assign({}, icons[indexIcon]);

      if (x['lat'] == "null") x['lat'] = null;
      if (x['lng'] == "null") x['lng'] = null;

      x.iconTypeProcess.icon = x.iconTypeProcess.icon
        .replace('viewBox="0 0 34 34"', 'viewBox="0 0 24 24"')
        .replace('viewBox="0 0 48 48"', 'viewBox="0 0 38 38"')
        .replace('transform="rotate({iconRotate})"', "")
        .replace('{iconFill}', "#fff");
      x.iconTypeProcess.icon =
        _this.sanitizer.bypassSecurityTrustHtml(x.iconTypeProcess.icon);

      switch (x.status) {
        case "lost_gps":
          x.statusType = status.lostGPS;
          break;
        case "lost_gprs":
          x.statusType = status.lostGPRS; break;
        case "history_transfer":
          x.statusType = status.historyTransfer;
          break;
        case "expired":
          x.statusType = status.expired;
          break;
        case "stop":
          x.statusType = status.stop;
          break;
        case "run":
          x.statusType = status.run;
          break;
        case "inactive":
          x.statusType = status.inactive;
          break;
        case "nodata":
          x.statusType = status.nodata;
          break;
        default:
          x.statusType = status.lostSignal;
          break;
      }

      let item = new Item(x);
      return item;
    });
    return dataNew;
  }

  // public deviceMarkers: Array<L.Layer> = [];
  onWidgetCheckChange(data) {
    if (this.layerDevice) this.layerDevice.remove();


    if (data.listChecked.length > 0) {
      this.layerDevice = new L.FeatureGroup(); data.listChecked.map(x => {
        let marker = this.mapService.createDevice(x, this.currentPopup, this.map, "style-one");
        if(marker)this.layerDevice.addLayer(marker);
      });
      this.layerDevice.addTo(this.map);
      this.map.fitBounds(this.layerDevice.getBounds(), { padding: [50, 50] });
    }

  }
  changeShowListDevice() {
    this.showListDevice = !this.showListDevice;
    this.cdr.detectChanges();
  }

}
