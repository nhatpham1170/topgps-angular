import { Component, OnInit, ElementRef, ChangeDetectorRef, ViewChild, EventEmitter } from '@angular/core';
import { finalize, takeUntil, tap, delay } from 'rxjs/operators';
import { Subject, observable } from 'rxjs';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { Device, DeviceTypeService } from '@core/admin';
import { Device, DeviceService, User, GeofenceService, PoiModel } from '@core/manage';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { DataTable } from '@core/_base/layout/models/datatable.model';

import { TranslateService } from '@ngx-translate/core';
import { ToastService, UserDatePipe } from '@core/_base/layout';
import { DeviceUtilityService } from '@core/manage/utils/device-utility.service';
import { ExcelService } from '@core/utils';
import { RequestFileService } from '@core/common/_services/request-file.service';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { ReportDeviceService } from '@core/report';
import { MapConfig, MapConfigModel, MapUtil } from '@core/utils/map';
import { DecimalPipe } from '@angular/common';
import { TrackingService } from '@core/map';
import { array } from '@amcharts/amcharts4/core';
import { DeviceConfigService } from '@core/common/_services/device-config.service';
import { TollStationModel,TollStationService,GeoService, TollRoadService } from '@core/admin';
import { AsciiPipe } from '@core/_base/layout';
import * as L from 'leaflet';

// import { delay } from 'q';

declare var $: any;
declare var ClipboardJS: any;
const TITLE_FORM_EDIT: string = "COMMON.ACTIONS.EDIT";
const TITLE_FORM_ADD: string = "COMMON.ACTIONS.ADD";
const TITLE_FORM_DELETE: string = "COMMON.ACTIONS.DELETE";

@Component({
  selector: 'kt-toll-station',
  templateUrl: './toll-station.component.html',
  styleUrls: ['./toll-station.component.scss'],
  providers: [UserDatePipe, DecimalPipe,AsciiPipe]
})
export class TollStationComponent implements OnInit {
  searchFormStation: FormGroup;

  public dataTable: DataTable = new DataTable();
  public action: string;
  public currentReason: any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public formEdit: FormGroup;
  public formAdd: FormGroup;
  private tableConfig: any;
  private currentForm: FormGroup;
  public currentModel: PoiModel;
  public commandsForm: FormGroup;
  public isShowSearchAdvanced: boolean;
  public deviceType: any[];
  public simType: any[];
  public groupDevice: any[];

  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  private userIdSelecte: number;
  public currentUser: User;
  public dateTimeServer: string;
  // feature device  
  public showUserTree = true;
  public allowReOrder: boolean = false;
  public titlePopup: string;
  public geofenceEdit: PoiModel;
  public listTollRoad:any;
  public listTollRoadByType:any;
  public pleaseSelectType:string;
  // permisison 
  public permissions: any = {
    sell: "device.action.sell"
  }
  public colorFill:string = '#3388ff';
  public mapConfig: MapConfigModel;
  public statusExport: {
    all: number, lostGPS: number, lostGPRS: number, historyTransfer: number, expired: number, stop: number,
    run: number, inactive: number, nodata: number, lostSignal: number
  };
  public listDevices: Array<Device> = [];
  private lastUserId: number = 0;
  public typeStation = [];
  public typeGeofence;
  public listCity:any;
  public listCountry:any;
  public filter:any;
  constructor(
    private road: TollRoadService,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private translate: TranslateService,
    private toast: ToastService,
    private deviceUtility: DeviceUtilityService,
    private xlsx: ExcelService,
    private requestFile: RequestFileService,
    private userTreeService: UserTreeService,
    private deviceConfig:DeviceConfigService,
    private tollStationService:TollStationService,
    private geo : GeoService,
    private asciiPipe:AsciiPipe,
    private mapUtil: MapUtil,

  ) {
    this.pleaseSelectType = this.translate.instant('COMMON.VALIDATION.PLEASE_CHOOSE_TYPE');
    this.getListCountry();
    this.selectCoutry(237);
    this.getListTollRoad();
    this.action = "";
    this.buildForm();
    this.tableConfig = {
      pagination: [
        10, 20, 30, 50
      ],
      columns: [
        {
          title: '#',
          field: 'id',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '20px' },
          width: 20,
          class: 't-datatable__cell--center',
          translate: '#',
          autoHide: false,
        },
        {
          title: 'Name',
          field: 'name',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '300px' },
          width: 150,
          class: '',
          translate: 'COMMON.COLUMN.NAME',
          autoHide: true,
        },
        {
          title: 'Type',
          field: 'type',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px' },
          width: 100,
          class: '',
          translate: 'COMMON.COLUMN.TYPE',
          autoHide: true,
        },
        {
          title: 'Type Geofence',
          field: 'typegeofence',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '150px' },
          width: 150,
          class: '',
          translate: 'ADMIN.TOLL_SEGMENT.COLUMN.ROAD',
          autoHide: true,
        },
        {
          title: 'City',
          field: 'type',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px' },
          width: 100,
          class: '',
          translate: 'ADMIN.TOLL_STATION.COLUMN.CITY',
          autoHide: true,
        },
        // {
        //   title: 'description',
        //   field: 'description',
        //   allowSort: false,
        //   isSort: false,
        //   dataSort: '',
        //   style: { 'width': '200px' },
        //   class: '',
        //   width: 200,
        //   translate: 'COMMON.COLUMN.DESCRIPTION',
        //   autoHide: true,
        // },
        {
          title: 'Create at',
          field: 'createdAt',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px' },
          class: '',
          width: 100,
          translate: 'COMMON.COLUMN.CREATED_DATE',
          autoHide: true,
        },
        {
          title: 'Trạng thái',
          field: 'active',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '70px' },
          class: '',
          width: 70,
          translate: 'COMMON.COLUMN.STATUS',
          autoHide: true,
        },
        {
          title: 'actions',
          field: 'actions',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '117px' },
          class: '',
          width: 80,
          translate: 'COMMON.COLUMN.ACTIONS',
          autoHide: true,
        },
      ]
    };
    this.unsubscribe = new Subject();
    this.mapConfig = new MapConfig().configs();
    this.dataTable.init({
      data: [],
      totalRecod: 0,
      paginationSelect: [1, 2, 5, 10, 20],
      columns: this.tableConfig.columns,
      formSearch: '#formSearch',
      pageSize: 10,
      // isDebug: true,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        responsive:false
        // selecter: true,
      }
    });
    this.currentReason = {};
    this.isShowSearchAdvanced = false;
    this.deviceType = [];
    this.simType = [];
    this.groupDevice = [];
    this.parrentEmiter = new EventEmitter();
    this.currentUser = new User();
    this.dateTimeServer = "";
  }

  renderBoundByTollStation()
  {
    this.tollStationService.list({ params: {pageNo:-1} }).pipe(
      tap((data: any) => {        
        data.result.map(toll=>{
         let points = this.mapUtil.decode(toll.encodedPoints);
         let circle:any;
         if(toll.typeGeofence == 'circle')
         {
            circle = new L.Circle(points[0], toll.radius);
            let bound : {
              _northEast:{
                lat:number,
                lng:number
            },
            _southWest:{
                lat:number,
                lng:number
                }  
             };
             bound = circle.getBounds(),{padding:[0,0]};
         }else{
            circle = new L.Rectangle(points);
         }
        let bound : {
         _northEast:{
           lat:number,
           lng:number
       },
       _southWest:{
           lat:number,
           lng:number
           }  
        }
        bound = circle.getBounds(),{padding:[0,0]};
       let neLat = bound['_northEast'].lat;
       let neLng = bound['_northEast'].lng;
       let swLat = bound['_southWest'].lat;
       let swLng = bound['_southWest'].lng;
      //  this.tollStationService.updateTest({neLat:neLat,neLng:neLng,swLat:swLat,swLng:swLng,id:toll.id}).pipe(
      //    tap(data => {
      //    }),
      //    finalize(() => {
      //      this.cdr.markForCheck();
      //    })
      //  )
        return toll;
        });
      }),
   
      finalize(() => {
        this.cdr.markForCheck();
        this.cdr.detectChanges();
      })
    ).subscribe();
  }
  ngOnInit() {
    // this.renderBoundByTollStation();
    this.buildPagination();
    this.getData();
    setTimeout( () => {
      $('.kt_selectpicker').selectpicker();
    });
    // listening dataTable event
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;
        this.getData(this.filter);
      })
    ).subscribe();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();

    $(function () {
      // $('select').selectpicker();
      $('.kt_datepicker').datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: "bottom left",
        templates: {
          leftArrow: '<i class="la la-angle-left"></i>',
          rightArrow: '<i class="la la-angle-right"></i>'
        },
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true
      });
    });
  }


  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
    };
  }

  getNameRoad(id)
  {
    if(this.listTollRoad.find(x=>x.id == id))  return this.listTollRoad.find(x=>x.id == id).name;
    return '';
  }

  async getListTollRoad(){
    await this.road.list({pageNo:-1}).subscribe(data=>{
       
       if(data.status == 200)
       {
         this.listTollRoad = data.result;
         this.listTollRoad.map(city=>{
           city.textSearch = this.asciiPipe.transform(city.name || "");
           return city;
         });
       }
     })
   }

  private buildForm(): void {
    this.searchFormStation = this.fb.group({
      name: [''],
      typeGeofence: [''],
      tollRoad:[],
      typeStation: [''],
      listCity:[''],
      listCountry:['237']
    });
  }

  toggleSearchAdvanced() {
    this.isShowSearchAdvanced = !this.isShowSearchAdvanced;
    $('#countrySearch').val(237).selectpicker('refresh');
    this.selectCoutry(237);
    // this.getListCountry();
  }

  selectTypeRoad(event)
  {
    let type = event.target.value;
    this.listTollRoadByType = this.listTollRoad.filter(toll=>{
      return toll.type != type;
    });
    this.searchFormStation.value.tollRoad = null;
    setTimeout(()=>{
      $('#tollRoadSearch').val(null).selectpicker('refresh');
    })
  }

  selectCoutry(item)
  {
    let countryId;
    if(typeof item == 'number')
    {
      countryId = item;
    }  else
    {
      countryId = item.target.value
    }
   this.setStoreCountry(countryId);
  }

  setStoreCountry(countryId)
  {
    let name="listCountry_";
    name = name+countryId;
    if(localStorage.getItem(name) == undefined)
    {
      let params = {
        countryId:countryId
      };
      this.geo.listProvince(params).subscribe(data=>{
        this.listCity = data.result;
        this.listCity.map(city=>{
          city.textSearch = this.asciiPipe.transform(city.name || "");
          return city;
        })
        // device.textSearch =  this.asciiPipe.transform(device.name || "");
        localStorage.setItem(name,  JSON.stringify(this.listCity));

      })
    }else{
      this.listCity = localStorage.getItem(name);
      this.listCity = JSON.parse(this.listCity);
     
    }
    setTimeout(()=>{
      $('#listCity1').selectpicker('refresh');
    })
    }

  async getListCountry()
  {
    await  this.geo.listCountry().subscribe(data=>{
      if(data.status == 200) this.listCountry = data.result;
      this.listCountry.map(country=>{
        country.textSearch = this.asciiPipe.transform(country.name || "");
        return country;
      })
      setTimeout(() => {
       $('#countrySearch').selectpicker('refresh'); 
      });
    })
  }

	/**
	 * Get list by data option
	 * @param option option search list
	 */
  private getData(option?) {
    this.tollStationService.list({ params: this.filter }).pipe(
      delay(300),
      tap((data: any) => {        
        data.result.content.map(toll=>{
          let typeName = this.translate.instant('ADMIN.TOLL_STATION.COLUMN.STANDARD');
          if(toll.type == 1) typeName = this.translate.instant('ADMIN.TOLL_STATION.COLUMN.OPTION');
          let cityName =  this.listCity.find(x=>x.id == toll.provinceId).name;
          let nameGeofence = this.translate.instant('ADMIN.TOLL_STATION.COLUMN.CIRCLE');
          if(toll.typeGeofence == 'rectangle') nameGeofence = this.translate.instant('ADMIN.TOLL_STATION.COLUMN.RECTANGLE');
          toll.typeName = typeName;
          toll.cityName = cityName;
          toll.fillColor = this.colorFill;
          toll.nameRoad = this.getNameRoad(toll.roadId);
          toll.nameGeofence = nameGeofence;
          return toll;
        });
  
        this.dataTable.update({
          data: data.result.content,
          totalRecod: data.result.totalRecord
        });
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
        this.cdr.detectChanges();
      })
    ).subscribe();
  }

  getDataSelected() {
    if (this.dataTable.dataSelected.length == 0) {
      this.toast.show({ translate: "MANAGE.DEVICE.MESSAGE.NO_DEVICE_SELECTED" });
    }
    return this.dataTable.dataSelected;
  }

  get formArr(): FormArray {
    if (this.commandsForm) {
      return this.commandsForm.get('itemRows') as FormArray;
    }
  }


	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.currentForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
  /**
   * Change size databale
   * @param $elm 
   */
  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  search(data?: any) {
    let listSearch = [];
    this.dataTable.search(data || listSearch);

  }

  resetFormSearch() {
    this.searchFormStation.reset();
    this.buildPagination();
    this.listTollRoadByType = [];
    setTimeout(()=>{
      $('select').selectpicker('refresh');
    });
    this.dataTable.reload({ currentPage: 1 });
  
  }


  getGroupDevice(userId: number) {

  }

  userTreeChangeUser(value) {

    if (value.id > 0) {
      this.userIdSelecte = value.id;
      this.currentUser = value;
      this.search();
    }
  }
  sendEvent() {
    this.parrentEmiter.emit({ id: 5024, path: '234,5305,235' });
  }
  copied(val) {
    this.toast.copied(val);
  }

  // feature export excel file
  open(content, type, item?: any) {
    
    this.action = type;

    switch (type) {
      case 'edit':
        this.editFnc(item, content);
        break;
      case 'add':
        this.addFnc(content);
        break;
      case 'delete':
        this.deleteFnc(item.id, content);
        break;
    }


  }

  searchtollStation(form){
    this.buildPagination();
    if(this.searchFormStation.value.name == null ||  this.searchFormStation.value.name == '')  this.searchFormStation.value.name = '';
    if(this.searchFormStation.value.typeStation == null ||  this.searchFormStation.value.typeStation == '')  this.searchFormStation.value.typeStation = '';
    if(this.searchFormStation.value.listCity == null ||  this.searchFormStation.value.listCity == '') this.searchFormStation.value.listCity = '';

    this.filter.name = this.searchFormStation.value.name;
    this.filter.type = this.searchFormStation.value.typeStation;
    if(this.searchFormStation.value.tollRoad != null || this.searchFormStation.value.tollRoad != undefined)
    {
      this.filter.roadId = this.searchFormStation.value.tollRoad;

    }
    this.filter.provinceId = this.searchFormStation.value.listCity;
    this.filter.countryId = $('#countrySearch').val();
    
    
    this.dataTable.reload({ currentPage: 1 });
  }
  async editFnc(item, content) {
    this.titlePopup = TITLE_FORM_EDIT;
    this.currentForm = this.formEdit;    
    
    // check and get listDevices
    this.geofenceEdit = new PoiModel(item);
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal-xl kt-modal--fit', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
    // let id = 61;
    // this.geofenceService.detail(id).pipe(
    //   tap(body => {
    //     this.geofenceEdit = new PoiModel(body.result);
    //     this.geofenceEdit = new PoiModel(item);
    //     this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal-xl kt-modal--fit', backdrop: 'static' }).result.then((result) => {
    //       this.closeResult = `Closed with: ${result}`;
    //     }, (reason) => {
    //       this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    //       this.currentReason = reason;
    //     });
    //   }),
    //   takeUntil(this.unsubscribe),
    //   finalize(() => {
    //     this.cdr.markForCheck();
    //   })
    // ).subscribe();
  }

  async addFnc(content) {
    this.titlePopup = TITLE_FORM_ADD;
    // check and get listDevices
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal-xl kt-modal--fit', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }
  deleteFnc(id: number, content) {
    this.titlePopup = TITLE_FORM_DELETE;
    this.currentModel = new PoiModel();
    this.currentModel.id = id;
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', size: 'sm', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }
  onSubmit() {
    switch (this.action) {
      case "edit":
        if (!this.currentForm.invalid) {
          this.editAction();
        }
        break;
      case "add":
        if (!this.currentForm.invalid) {
          this.addAction();
        }
        break;
      case "delete":
        this.deleteAction();
        break;
    }
  }
  addAction() {
    this.modalService.dismissAll();

  }

  editAction() {
    this.modalService.dismissAll();

  }

  deleteAction() {
    this.tollStationService.delete(this.currentModel.id, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.modalService.dismissAll(this.currentReason);
          this.dataTable.reload({currentPage:1});
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  changeActive(item: TollStationModel) {
    this.tollStationService.changeActive({ id: item.id, active: (item.active == 1 ? 0 : 1) }, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.dataTable.reload({});
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  geofenceResult(data) {
    if (data.status == "success") {
      this.modalService.dismissAll(this.currentReason);
      this.dataTable.reload({});
    }
  }


  /**
* Dismiss Reason Popup
* @param reason 
*/
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}


