// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { SortablejsModule } from 'ngx-sortablejs';

// NGRX
import { StoreModule } from '@ngrx/store';
import { InterceptService } from '../../../core/_base/crud/';
// Translate
import { TranslateModule } from '@ngx-translate/core';
// ng autocomplete
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
//services
import { SharedModule } from '@app/core/common/shared.module';

// Components
import { NotifiGlobalComponent } from './notifi-global/notifi-global.component';
import { PoiTypeComponent } from './poi-type/poi-type.component';
import { TollStationComponent } from './toll-station/toll-station.component';
import { StationToolComponent } from './toll-station/station-tool/station-tool.component';

import { SystemLogComponent } from './system-log/system-log.component';
import { PermissionComponent } from './permission/permission.component';
import { RoleComponent } from './role/role.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { SimTypeComponent } from './sim-type/sim-type.component';
import { DeviceTypeComponent } from './device-type/device-type.component';
import { SensorTypeComponent } from './sensor-type/sensor-type.component';
import { IconTypeComponent } from './icon-type/icon-type.component';
import { AdminComponent } from './admin.component';
import { NgbDropdownModule, NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageTemplateComponent } from './page-template/page-template.component';
import { ResizeObserverDirective } from '@core/_base/layout';
import { CoreModule } from '@core/core.module';
import { WidgetModule } from '@app/views/partials/content/widgets/widget.module';
import { TreeModule } from 'angular-tree-component';
import { TransportTypeComponent } from './transport-type/transport-type.component';
import { AuthGuard, ModuleGuard } from '@core/auth';

import { TollRoadComponent } from './toll-road/toll-road.component';
import { TollSegmentComponent } from './toll-segment/toll-segment.component';


import { ImageCropperModule } from 'ngx-image-cropper';
import { QuillModule } from 'ngx-quill';
import { FeedbackManageComponent } from './feedback-manage/feedback-manage.component';
import { PartialsModule } from '@app/views/partials/partials.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
// import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletDrawModule } from '@asymmetrik/ngx-leaflet-draw';
import { NgxMaskModule} from 'ngx-mask';
import { TransportTypeQCVNComponent } from './transport-type-qcvn/transport-type-qcvn.component';
import { DragDropModule } from '@angular/cdk/drag-drop';


const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'permission',
        component: PermissionComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.permission',
        }
      },
      {
        path: 'role',
        component: RoleComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.role',
        }
      },
      {
        path: 'login-page',
        component: LoginPageComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.login_page',
        }
      },
      {
        path: 'device-type',
        component: DeviceTypeComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.device_type',
        }
      },
      {
        path: 'sim-type',
        component: SimTypeComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.sim_type',
        }
      },
      {
        path: 'sensor-type',
        component: SensorTypeComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.sensor_template',
        }
      },
      {
        path: 'icon-type',
        component: IconTypeComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.icon',
        }
      },
      {
        path: 'page-template',
        component: PageTemplateComponent,
      },
      {
        path: 'transport-type',
        component: TransportTypeComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.transport_type',
        }
      },
      {
        path: 'transport-type-qcvn',
        component: TransportTypeQCVNComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.transport_type_qcvn',
        }
      },
      {
        path: 'system-change-log',
        component: SystemLogComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.system_log',
        },
      },
      {
        path: 'feedback',
        component: FeedbackManageComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.feedback',
        },
      },
      {
        path: 'notices',
        component: NotifiGlobalComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.notices',
        }
      },
      {
        path: 'poi-type',
        component: PoiTypeComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.poi_type',
        }
      },
      {
        path: 'toll-station',
        component: TollStationComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.toll_station',
        }
      },
      {
        path: 'toll-road',
        component: TollRoadComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.toll_road',
        }
      },
      {
        path: 'toll-segment',
        component: TollSegmentComponent,
        canActivate: [AuthGuard, ModuleGuard],
        data: {
          permisison: 'ROLE_admin.toll_segment',
        }
      },
      
    ]
  }
];

@NgModule({

  imports: [
    QuillModule.forRoot(),
    PartialsModule,
    CommonModule,
    CoreModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    NgbModule,
    NgbDropdownModule,
    NgbModalModule,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    SharedModule,
    WidgetModule,
    ImageCropperModule,
    TreeModule.forRoot(),
    LeafletModule.forRoot(),
    LeafletDrawModule.forRoot(),
    QuillModule.forRoot(),
    NgxMaskModule.forRoot(),
    PartialsModule,
    DragDropModule,
    SortablejsModule
  ],
  providers: [
  ],
  declarations: [
    NotifiGlobalComponent,
    SystemLogComponent,
    TransportTypeComponent,
    PermissionComponent,
    RoleComponent,
    LoginPageComponent,
    SimTypeComponent,
    DeviceTypeComponent,
    SensorTypeComponent,
    IconTypeComponent,
    AdminComponent,
    PageTemplateComponent,
    PoiTypeComponent ,
    TollStationComponent,
    StationToolComponent,
    FeedbackManageComponent,
    TransportTypeQCVNComponent,
    TollRoadComponent,
    TollSegmentComponent
    // ResizeObserverDirective,  
  ],


  // exports:[AdminComponent]
})
export class AdminModule { }
