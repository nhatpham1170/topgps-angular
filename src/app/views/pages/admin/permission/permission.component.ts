import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { finalize, takeUntil, tap ,debounceTime} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AuthService,Permission } from '@core/auth';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

declare var $: any;
@Component({
  selector: 'kt-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent implements OnInit {
  formPermission: FormGroup;
  searchFormPermission : FormGroup;

  selectdp: ElementRef;
  public isLoading: boolean = false;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idPermissonEdit: number;
  public dataDefault: any = [{}];
  public loadingSave: Boolean = false;
  public loadingData: boolean = false;
  public idPermissonDelete: any;
  public listNameGroupObj = [];
  public isPermissionError: boolean = false;
  public errorMessage: string = '';
  public listNameGroup = [];
  public keyword = 'name';
  public nameGroupEdit = { name: '' };
  public filter :any;
  public translationsArr = [];
  public closeResult: string;

  private unsubscribe: Subject<any>;

  constructor(
    private auth: AuthService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    private modalService: NgbModal
  ) {
    this.unsubscribe = new Subject();
    this.translations();
  }

  ngOnInit() {
    this.buildForm();
    this.buildPagination();
    this.loadListPermissionGroup();
    this.setColumns();
    this.setPaginationSelect();
    this.updateDataTable();
    this.setDataTable();
    this.getData();

  }

  private buildForm(): void {
    this.formPermission = this.formBuilder.group({
      name: ['', Validators.required],
      description: [''],
      inherit:[false],
    });
    this.searchFormPermission = this.formBuilder.group({
      name: [''],
      namegroup: ['']
    })
  }

  buttonAddNew(content) {
    this.isEdit = false;
    this.formPermission.reset();
    this.open(content);

  }

  closeModal(){
    $('.modal').modal('hide');
  }

  onSubmitPermission(form: any) {
    if (this.formPermission.invalid) {
      return;
    }
    let nameGroup = this.nameGroupEdit.name;
    let params = {
      "name": form.value.name,
      "groupName": nameGroup,
      "description": form.value.description,
      "isExtend":form.value.inherit ? '1':'0'
    } as Permission;
    this.loadingSave = true;
    if (this.isEdit) {
      this.auth.updatePermission(this.idPermissonEdit,params).subscribe((result: any) => {
        this.loadingSave = true;
        if (result.status == 200) {
          this.loadingSave = false;
          this.onshowToastSuccess(this.translationsArr['SUCCESS'], this.translationsArr['UPDATE_PERMISSION_NAME'] + form.value.name);
          this.getData();
          // this.dataTable.reload({})
        }
        else {
          this.loadingSave = false;
          this.isPermissionError = true;
          this.errorMessage = result.message;
          this.onshowToastFail(this.translationsArr['FAILED'], this.translationsArr['UPDATE_PERMISSION_NAME'] + form.value.name);
        }
      }, (err: HttpErrorResponse) => {
        this.isPermissionError = true;
      });
      this.modalService.dismissAll();
      return;
    }
    this.auth.addPermission(params)
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        this.loadingSave = false;
        this.onshowToastSuccess(this.translationsArr['SUCCESS'], this.translationsArr['ADD_PERMISSION_NAME'] + form.value.name);
        this.getData();
      }
      else {
        this.loadingSave = false;
        this.isPermissionError = true;
        this.errorMessage = result.message;
      }
    }, (err: HttpErrorResponse) => {
      this.isPermissionError = true;
    });
    this.modalService.dismissAll();
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10
    };
  }

  open(content) {
    
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  translations(){
    this.buildTranslations();
    this.translate.onLangChange.subscribe((event) => {
      this.translationsArr['SUCCESS'] = event.translations.COMMON.SUCCESS;
      this.translationsArr['FAILED'] = event.translations.COMMON.FAILED;
      this.translationsArr['UPDATE_PERMISSION_NAME'] = event.translations.ADMIN.PERMISSION.GENERAL.UPDATE_PERMISSION_NAME;
      this.translationsArr['UPDATE_PERMISSION_FAILED'] = event.translations.ADMIN.PERMISSION.GENERAL.UPDATE_PERMISSION_FAILED;
      this.translationsArr['ADD_PERMISSION_NAME'] = event.translations.ADMIN.PERMISSION.GENERAL.ADD_PERMISSION_NAME;
      this.translationsArr['DELETE_PERMISSION_SUCCESS'] = this.translate.instant('ADMIN.PERMISSION.GENERAL.DELETE_PERMISSION_SUCCESS');
      this.translationsArr['DELETE_PERMISSION_FAILED'] = this.translate.instant('ADMIN.PERMISSION.GENERAL.DELETE_PERMISSION_FAILED');

  })}
  buildTranslations(){
    this.translationsArr['SUCCESS'] = this.translate.instant('COMMON.SUCCESS');
    this.translationsArr['FAILED'] = this.translate.instant('COMMON.FAILED');
    this.translationsArr['UPDATE_PERMISSION_NAME'] = this.translate.instant('ADMIN.PERMISSION.GENERAL.UPDATE_PERMISSION_NAME');
    this.translationsArr['UPDATE_PERMISSION_FAILED'] = this.translate.instant('ADMIN.PERMISSION.GENERAL.UPDATE_PERMISSION_FAILED');
    this.translationsArr['ADD_PERMISSION_NAME'] = this.translate.instant('ADMIN.PERMISSION.GENERAL.ADD_PERMISSION_NAME');
    this.translationsArr['DELETE_PERMISSION_SUCCESS'] = this.translate.instant('ADMIN.PERMISSION.GENERAL.DELETE_PERMISSION_SUCCESS');
    this.translationsArr['DELETE_PERMISSION_FAILED'] = this.translate.instant('ADMIN.PERMISSION.GENERAL.DELETE_PERMISSION_FAILED');

  }

  onshowToastSuccess(content: string, title: string) {
    this.toastr.success(title, content, {
      timeOut: 2000,
      positionClass: 'toast-bottom-right'
    });
  }

  onshowToastFail(content: string, title: string) {
    this.toastr.error(title, content, {
      timeOut: 2000,
      positionClass: 'toast-bottom-right'
    });
  }

  get f() {
    if(this.formPermission != undefined) return this.formPermission.controls;
    
   }

  onHideModal(id: string) {
    let idModal = '#' + id;
    $(idModal).modal('hide');
  }

  selectEvent(item) {
    this.nameGroupEdit = item;
  }

  onChangeSearch(val: string) {
    this.nameGroupEdit = {
      name: val
    };
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }

  getIdAction(id,idModal) {
    this.idPermissonDelete = id;
    this.openModal({idModal:idModal,confirm : true});
  }

  openModal(params) {
     let idModal = params.idModal;
     let confirm = params.confirm;
     if(confirm)
     {$(idModal).appendTo('body').modal({backdrop: 'static',keyboard: false})}
     else{$(idModal).appendTo('body').modal('show');}
  }

  deletePermission() {
    let id = this.idPermissonDelete;
    this.auth.deletePermission(id).subscribe((data: any) => {
      if (data.status == 200) {
        this.getData();
        // this.dataTable.reload({});
        this.onshowToastSuccess(this.translationsArr['SUCCESS'], this.translationsArr['DELETE_PERMISSION_SUCCESS']);
      }
    },(err: HttpErrorResponse) => {
      this.onshowToastFail(this.translationsArr['FAILED'] , this.translationsArr['DELETE_PERMISSION_FAILED']);
  });
  $('.modal').modal('hide');
  }

  editPermission(item,content) {
    this.isEdit = true;
    this.idPermissonEdit = item.id;
    this.formPermission.get('inherit').setValue(item.isExtend);
    this.auth.getPermissionById(item.id).pipe(
      tap((data: any) => {
        this.dataDefault = [data.result];
        this.nameGroupEdit = {
          name: data.result.groupName
        };
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
    this.open(content);
  }

  loadSelectBootstrap(){
		$('.bootstrap-select').selectpicker();
  }

  convertToObj(data) {
    for (var i = 0; i < data.length; i++) {
      let obj = {
        name: data[i]
      }
      this.listNameGroupObj.push(obj);
    }
  }

  loadListPermissionGroup() {
    this.auth.getListPermissionGroup()
      .pipe(
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((data: any) => {
        if (data.status == 200) {
          this.listNameGroup = data.result;
          this.convertToObj(this.listNameGroup);
          setTimeout(() => {
            this.loadSelectBootstrap();
          }, 100);
        }
      });
  }

  searchPermission(form:any)
  {
    this.filter.name = form.value.name;
    this.filter.groupName = form.value.namegroup;
    this.filter.pageNo = 1;

    this.getData();
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter = option;
        this.getData();
      }),
      
    ).subscribe();
  }

  getData() {
    this.auth.searchPermissions(this.filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          $(function () {
            $('.kt_selectpicker').selectpicker();
          });
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
				selecter:false,
			  }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();

  }

  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '40px' },
        class: '',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Name',
        field: 'name',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'ADMIN.PERMISSION.GENERAL.NAME',
        autoHide: false,
        width: 182,
      },
      {
        title: 'Description',
        field: 'description',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'ADMIN.PERMISSION.GENERAL.DESCRIPTION',
        autoHide: true,
        width: 182,
      },
      {
        title: 'Name group	',
        field: 'groupName',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'ADMIN.PERMISSION.GENERAL.GROUP',
        autoHide: false,
        width: 182,
      },
      {
        title: 'Create At',
        field: 'createdAt',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '182px' },
        class: '',
        translate: 'COMMON.CREATED_DATE',
        autoHide: true,
        width: 182,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 182,
      },
    ]
  }


}
