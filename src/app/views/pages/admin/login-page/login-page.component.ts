
import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { finalize, takeUntil, tap, debounceTime, filter } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { LoginPageService, LoginPage } from '@core/admin';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';
import { UploadFileOptionModel, ImageUploadMode, UploadFileStatus } from '@app/views/partials/content/widgets/upload-file/upload-file.component';

declare var $: any;

@Component({
  selector: 'kt-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  formLoginPage: FormGroup;
  selectdp: ElementRef;
  public isLoading: boolean = false;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idLoginPageEdit: number;
  public dataDefault: any = [{}];
  public idLoginPageDelete: any;
  public filter: any;
  public closeResult: string;
  public loginPages = ["login-v1", "login-v2", "login-v3"];
  public selectedFile: any = [
    {
      logoUrl: File,
    }
  ];
  public listIcon: any = [{
    logoUrl: null,
  }];
  private unsubscribe: Subject<any>;
  public optionsUploadFile: UploadFileOptionModel = {
    crop: false,
    close: true,
    upload: true
  }
  public currentItemSelected;
  private imgsUpload: Array<ImageUploadMode> = [];
  constructor(
    private loginPage: LoginPageService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private sanitizer: DomSanitizer,

  ) {
    this.unsubscribe = new Subject();
  }

  ngOnInit() {
    this.buildPagination();
    this.setColumns();
    this.setPaginationSelect();
    this.setDataTable();
    this.getData();   
    this.updateDataTable();
  }

  closeModal() {
    $('.modal').modal('hide');
  }

  onSubmitLoginPage(form: any) {
    if (this.formLoginPage.invalid) {
      return;
    }
    let params = {
      host: form.value.host,
      website: form.value.website,
      companyName: form.value.company,
      layout: form.value.theme,
      // logo:[""],
      // icon:[""],
      iosUrl: form.value.ios,
      androidUrl: form.value.android,
      description: form.value.description,
      title: form.value.title,
      config: JSON.stringify({
        linkDocument:form.value.linkDocument
      })
    } as LoginPage;
    if (this.isEdit) {
      params.id = this.idLoginPageEdit;
    }

    if (this.imgsUpload && this.imgsUpload.length > 0) {
      params['background'] = this.imgsUpload.filter(f=>f.status == UploadFileStatus.ADD).map(x => x.file);
      params['backgroundRemove'] = this.imgsUpload.filter(f=>f.status == UploadFileStatus.REMOVE).map(x => x.url);
    }
    let formData = this.convertFormData(params);
    
    if (this.isEdit) {

      this.loginPage.update(formData, { notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          this.getData();
          this.dataTable.reload({})
          this.modalService.dismissAll();
        }
      });
      return;
    }
    this.loginPage.create(formData, { notifyGlobal: true })
      .pipe(
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 201) {
          this.getData();
          this.dataTable.reload({})
          this.modalService.dismissAll();
        }
      });
  }
  showFrom(content, action: string, loginPage?: LoginPage) {
    switch (action) {
      case "edit":
        this.createFrom(loginPage);
        this.isEdit = true;
        this.idLoginPageEdit = loginPage.id;
        break;
      case "add":
        this.isEdit = false;
        this.createFrom();
        break;
    }
    this.open(content);
    if(loginPage){
      setTimeout(() => {      
        $("#selectTheme").val(loginPage.layout).selectpicker('refresh');
      });
    }
    
  }

  convertFormData(params) {
    let formData = new FormData();
    Object.keys(params).forEach((x:any) => {
      switch(x){
        case "background":
          params[x].forEach(bg=>{
            formData.append(x,bg);
          })
          break;
        case "backgroundRemove":
          formData.set(x, JSON.stringify(params[x]));
          break;
        default:
          formData.set(x, params[x].toString());
          break;
      }
  
    });

    if (this.selectedFile.logoUrl) formData.append("logo", this.selectedFile.logoUrl);
    if (this.selectedFile.iconUrl) formData.append("icon", this.selectedFile.iconUrl);
    return formData;
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10
    };
  }

  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-800' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    setTimeout(() => {
      $(".kt-selectpicker").selectpicker();
    });

  }
  createFrom(loginPage?: LoginPage) {
    let _this = this;
    this.listIcon = [{}];
    this.imgsUpload = null;
    this.currentItemSelected = null;
    // edit form
    if (loginPage) {
      this.formLoginPage = this.formBuilder.group({
        host: [loginPage.host || "", Validators.required],
        website: [loginPage.website || ""],
        company: [loginPage.companyName],
        theme: [loginPage.layout, Validators.required],
        logo: [""],
        icon: [""],
        ios: [loginPage.iosUrl || ""],
        android: [loginPage.androidUrl || ""],
        description: [loginPage.description || ""],
        title:[loginPage.title || ""],
        linkDocument:[loginPage.config? (loginPage.config['linkDocument'] || ""): ""],
      });
      let icon = {};
      if (loginPage['logoUrl']) icon["logoUrl"] = loginPage['logoUrl'];
      if (loginPage['iconUrl']) icon["iconUrl"] = loginPage['iconUrl'];
      this.listIcon = [icon];
      this.currentItemSelected = loginPage;
     
    }
    else {
      // create form
      this.formLoginPage = this.formBuilder.group({
        host: ['', Validators.required],
        website: [''],
        company: [''],
        theme: ['', Validators.required],
        logo: [""],
        icon: [""],
        ios: [''],
        android: [''],
        description: [''],
        title:[''],
        linkDocument:['']
      });
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }



  get f() {
    if (this.formLoginPage != undefined) return this.formLoginPage.controls;

  }

  onHideModal(id: string) {
    let idModal = '#' + id;
    $(idModal).modal('hide');
  }


  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  getIdAction(id, idModal) {
    this.idLoginPageDelete = id;
    this.openModal({ idModal: idModal, confirm: true });
  }

  onFileChange(event, name: string = '') {

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.selectedFile[name] = event.target.files.item(0);
      var mimeType = event.target.files[0].type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
      this.convertFileToBase64(file, name);
    }
  }
  // upload file change 
  imgChange(value) {
    this.imgsUpload = value;
  }
  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  convertFileToBase64(file, name) {

    var _this = this;
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      _this.cdr.markForCheck();
      _this.listIcon[0][name] = reader.result;
    };
  }

  openModal(params) {
    let idModal = params.idModal;
    let confirm = params.confirm;
    if (confirm) { $(idModal).appendTo('body').modal({ backdrop: 'static', keyboard: false }) }
    else { $(idModal).appendTo('body').modal('show'); }
  }

  deleteLoginPage() {
    let id = this.idLoginPageDelete;
    this.loginPage.delete(id).subscribe((data: any) => {
      if (data.status == 200) {
        this.getData();

      }
    });
    $('.modal').modal('hide');
  }

  loadSelectBootstrap() {
    $('.bootstrap-select').selectpicker();
  }

  convertToObj(data) {
    for (var i = 0; i < data.length; i++) {
      let obj = {
        name: data[i]
      }
    }
  }


  searchLoginPage(form: any) {
    this.filter.name = form.value.name;
    this.filter.groupName = form.value.namegroup;
    this.getData();
  }

  getData() {
    this.loginPage.list(this.filter)
      .pipe(
        takeUntil(this.unsubscribe),
        finalize(() => {
          setTimeout(() => {
            $('.selectpicker ').selectpicker();
          });
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          this.cdr.detectChanges();
        }
      })
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      formSearch:"#formSearch",
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
      }
    });
  }
  onSearch(){
    this.dataTable.search();
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();

  }
  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter = option;
        this.getData();
      }),
      
    ).subscribe();
  }

  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '40px' },
        class: '',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'host',
        field: 'host',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'ADMIN.LOGIN_PAGE.COLUMN.HOST',
        autoHide: false,
        width: 150,
      },
      {
        title: 'title',
        field: 'title',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'COMMON.COLUMN.TITLE',
        autoHide: false,
        width: 110,
      },
      {
        title: 'website',
        field: 'website',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'ADMIN.LOGIN_PAGE.COLUMN.WEBSITE',
        autoHide: false,
        width: 150,
      },
      {
        title: 'company',
        field: 'company',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'ADMIN.LOGIN_PAGE.COLUMN.COMPANY_NAME',
        autoHide: false,
        width: 150,
      },
      {
        title: 'Description',
        field: 'description',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'ADMIN.PERMISSION.GENERAL.DESCRIPTION',
        autoHide: true,
        width: 150,
      },
      {
        title: 'theme',
        field: 'theme',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'ADMIN.LOGIN_PAGE.COLUMN.THEME',
        autoHide: true,
        width: 150,
      },
      {
        title: 'logo',
        field: 'logo',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'ADMIN.LOGIN_PAGE.COLUMN.LOGO',
        autoHide: false,
        width: 150,
      },
      {
        title: 'icon',
        field: 'icon',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'ADMIN.LOGIN_PAGE.COLUMN.ICON',
        autoHide: false,
        width: 150,
      },
      {
        title: 'ios',
        field: 'ios',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'ADMIN.LOGIN_PAGE.COLUMN.IOS',
        autoHide: true,
        width: 150,
      },
      {
        title: 'Adroid',
        field: 'adroid',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'ADMIN.LOGIN_PAGE.COLUMN.ANDROID',
        autoHide: true,
        width: 150,
      },
      {
        title: 'Create At',
        field: 'createdAt',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '110px' },
        class: '',
        translate: 'COMMON.CREATED_DATE',
        autoHide: true,
        width: 150,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 150,
      },
    ]
  }


}
