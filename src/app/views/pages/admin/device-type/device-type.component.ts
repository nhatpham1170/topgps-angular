import { Component, OnInit, ElementRef, ChangeDetectorRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { finalize, takeUntil, tap, debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { Subject, observable, from, Observable, merge } from 'rxjs';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { DeviceType, DeviceTypeService } from '@core/admin';
import { FormGroup, FormBuilder, Validators, FormArray,FormControl  } from '@angular/forms';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { HttpClient } from '@angular/common/http';
import { Command } from '@core/admin/_models/device-type';
import { DomSanitizer} from '@angular/platform-browser';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { SensorService } from '@core/manage';
import { IoSetting } from '@core/manage/_models/sensor';
import { assign } from 'lodash';
import { state } from '@angular/animations';
declare var $: any;
const TITLE_FORM_EDIT: string = "COMMON.ACTIONS.EDIT";
const TITLE_FORM_ADD: string = "COMMON.ACTIONS.ADD";
const TITLE_FORM_DELETE: string = "COMMON.ACTIONS.DELETE";
enum ACTION { edit, add, delete };

const states = [
'RESET','RESET_FACTORY','INFO','TIME_ZONE','SET_TIME_ZONE',
'LOCK_CONFIG','UNLOCK_CONFIG',
'APN','SET_APN',
'STATUS','SET_STATUS', 'LOCATION',
'HEARBEAT_INTERVAL','TRACKING_INTERVAL', 
"MODE","SET_MODE",
"SHOCK_MODE",'SLEEP_MODE',
"SMS_CENTER","SET_SMS_CENTER", "SOS",
"IP_PORT","SET_IP_PORT", "HOST","SET_HOST",
'TURN_OFF_ENGINE', 
'TURN_ON_ENGINE',
'SAVES_BATTERY',
'TURN_OFF',
'TURN_ON',
"CHANGE_PASSWORD", "AUTHORIZATION"];
@Component({
  selector: 'kt-device-type',
  templateUrl: './device-type.component.html',
  styleUrls: ['./device-type.component.scss']
})
export class DeviceTypeComponent implements OnInit {
  imageChangedEvent: any = '';
  croppedImage: any = '';
  
  public focus$ = new Subject<string>();
  public click$ = new Subject<string>();

  public dataTable: DataTable = new DataTable();
  public action: string;
  public currentReason: any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public formEdit: FormGroup;
  public formAdd: FormGroup;
  public titlePopup: string = TITLE_FORM_ADD;
  private tableConfig: any;
  private currentForm: FormGroup;
  public currentModel: DeviceType;
  public commandsForm:FormGroup;   
  public ioSetting: FormGroup;
  public extension: FormGroup;
  public formAddCommand: FormGroup;
  
  public ConfigSensor:any = []; 
  public isAdd : boolean = false;
  public listIcon : any = [{
    icon : null,
  }];
  public selectedFile : any = [
    {
      icon : File,
    }
  ];
  public ConfigExtension:any = [
    {
      id:'hasCamera',
      name:'Camera'
    },
    {
      id:'hasBattery',
      name:'Battery'
    },
    {
      id:'qcvn',
      name:'Qcvn'
    }
  ];
  public command;
  public states;
  @ViewChild('instance', { static: true }) instance: NgbTypeahead;
  constructor(
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private deviceTypeService: DeviceTypeService,
    private currentService: DeviceTypeService,
    private http: HttpClient,
    private sanitizer:DomSanitizer,
    private sensor : SensorService
  ) {
    this.buildFormExtension();
    this.action = "";
    this.tableConfig = {
      pagination: [
        10, 20, 30, 50
      ],
      columns: [      
        {
          title: '#',
          field: 'id',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '20px' },
          width: 20,
          class: 't-datatable__cell--center',
          translate: '#',
          autoHide: false,
        },
        {
          title: 'Name',
          field: 'modelName',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'ADMIN.DEVICETYPE.COLUMN.MODEL_NAME',
          autoHide: false,
        },
        {
          title: 'protocol',
          field: 'protocol',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'ADMIN.DEVICETYPE.COLUMN.PROTOCOL',
          autoHide: false,
        },

        {
          title: 'manufacturer',
          field: 'manufacturer',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'ADMIN.DEVICETYPE.COLUMN.MANUFACTURER',
          autoHide: true,
        },
        {
          title: 'manufacturer',
          field: 'manufacturer',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.DESCRIPTION',
          autoHide: true,
        },
        {
          title: 'Modifield By',
          field: 'modifieldBy',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'ADMIN.DEVICETYPE.COLUMN.IMAGE',
          autoHide: true,
        },
        {
          title: 'Updated',
          field: 'updatedDate',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.UPDATED_DATE',
          autoHide: true,
        },
        {
          title: 'Sort Order',
          field: 'sortOrder',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          class: '',
          width: 120,
          translate: 'COMMON.COLUMN.SORT_ORDER',
          autoHide: true,
        },
        {
          title: 'Actions',
          field: 'action',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '117px' },
          class: '',
          width: 117,
          translate: 'COMMON.ACTIONS.ACTIONS',
          autoHide: false,
        },
      ]
    };
    this.unsubscribe = new Subject();
    this.dataTable.init({
      data: [],
      totalRecod: 0,
      paginationSelect: [1, 2, 5, 10, 20],
      columns: this.tableConfig.columns,
      formSearch: '#formSearch',
      isDebug: false,
      layout:{
        body:{
          scrollable:false,
          maxHeight:600,
        },
        // selecter:true,
      }
    });
    this.currentReason = {};
    this.states = states;
   
    
  }
  ngOnInit() {

    // listening dataTable event
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.getData(option);
      })
    ).subscribe();
    this.dataTable.reload({});
    
    $(function () {
      $('.kt_selectpicker').selectpicker();
    });    
  }
  //crop img
  fileChangeEvent(event: any,content): void {
      this.modalService.open(content, { windowClass: 'kt-mt-50  modal-holder ', backdrop: 'static' }).result.then((result) => {
        this.listIcon[0]['icon'] = this.croppedImage.base64;
        this.selectedFile['icon'] = this.croppedImage.file;
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        this.currentReason = reason;
      });
  
    this.imageChangedEvent = event;
    }
    imageCropped(event: ImageCroppedEvent) {
       this.croppedImage = event;
    }
    imageLoaded() {
        // show cropper
    }
    cropperReady() {
        // cropper ready
    }
    loadImageFailed() {
        // show message
    }

	/**
	 * Create form 
	 * @param DeviceType model
	 */
  createFormAdd(model?: DeviceType) {
    // this.isAdd = true;
    this.formAdd = this.fb.group({
      name: ["", Validators.required],
      description: [""],
      sortOrder: [1],
      link: [""],
      protocol: ["",Validators.required],
      modelName: [""],
      manufacturer:[""]
    });

    let contentForm = [];
    states.forEach(x=>{
      contentForm.push(this.fb.group({
        name:[x],
        commandStr:[""],
        password:[""],
      }));
    }); 

    this.commandsForm = this.fb.group({
      itemRows:this.fb.array(contentForm)
    }); 

    // this.commandsForm = this.fb.group({
    //   itemRows:this.fb.array([])
    // });

    // if(!this.isAdd)
    // {
    //   this.ioSetting = this.fb.group({
    //     itemRows:this.fb.array([])
    //   });
    //   console.log('this.ioSetting: ',this.ioSetting);
    //   this.isAdd = true;
    // }
    this.ConfigExtension.forEach(x => {
      this.extension.controls[x.id].setValue(false);
    }); 
    this.formAddCommand = this.fb.group({
      key: [ "", Validators.required],
      command: [ ""],
      password: [""],
    });
    
    // Filter the selected list in formArr not show in command input
    this.states = states.filter(item => !this.formArr.value.some(x => x.name == item));
     
  } 
  sanitize(url:string){
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  createFormEdit(model: DeviceType) {
    this.formEdit = this.fb.group({
      name: [ model.name, Validators.required],
      description: [ model.description],
      createdBy: [ {value:model.createdBy,disabled:true}],
      createdDate: [ {value:model.createdDate,disabled:true}],
      updatedBy: [ {value:model.updatedBy,disabled:true}],
      updatedDate: [ {value:model.updatedDate,disabled:true}],
      sortOrder: [ model.sortOrder],
      link: [ model.link],
      modelName: [ model.modelName],
      protocol : [model.protocol],
      manufacturer: [ model.manufacturer],
      image : [model.image]
    });
   
    let contentForm = [];
    let contentFormIO = [];
    let _this=this;
    // console.log("model:", model);
    
    //commnad
    let command = model.command;
    if(Array.isArray(model.command)){
     
      command.forEach(x=>{
        contentForm.push(_this.fb.group({
          name: [x['name'], {disabled: true}],
          commandStr:[x['commandStr']],
          password: [x['password']]
        }));
      })
    }   
    else{
        Object.keys(model['command']).forEach((x)=>{
        contentForm.push(_this.fb.group({
          name:[x],
          commandStr:[command[x]],
          password: [command[x]]
        }));
      })
    }
    this.commandsForm = this.fb.group({
      itemRows:this.fb.array(contentForm)
    }); 
    this.formAddCommand = this.fb.group({
      key: [ "", Validators.required],
      command: [ ""],
      password: [""]
    });
    //iosetting
      let ioSetting = model['ioSetting'];
      if(ioSetting == null)
      {
        this.buildFormIoSetting();
      }else{
        this.ConfigSensor = ioSetting;
        ioSetting.forEach(element => {
          contentFormIO.push(_this.fb.group({
            key:element.key,
            value:element.value,
            active:element.active,
            basic:element.basic,
          }));
        });
        this.ioSetting = this.fb.group({
          itemRows:this.fb.array(contentFormIO)
        }); 
      }
    // Extension
    this.ConfigExtension.forEach(x => {
      this.extension.controls[x.id].setValue(this.currentModel.extensions[x.id]);
    }); 

     // Filter the selected list in formArr not show in command input
     this.states = states.filter(item => !this.formArr.value.some(x => x.name == item));
     
  }  

	/**
	 * Get list by data option
	 * @param option option search list
	 */
  private getData(option) {
    this.deviceTypeService.list({ params: option }).pipe(
      tap((data: any) => { 
        this.dataTable.update({
          data: data.result.content,
          totalRecod: data.result.totalRecord
        });
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

	/**
	 * Open popup template
	 * @param content template
	 * @param type action type
	 * @param data data for content 
	 */
  open(content, type, item?: any) {

    this.action = type;

    switch (type) {
      case 'edit':
        this.editFnc(item.id, content);
        break;
      case 'add':
        this.addFnc(content);
        break;
      case 'delete':
        this.deleteFnc(item.id, content);
        break;
    }
  }

 async buildFormIoSetting(){
  await  this.sensor.getAllConfigSensor().pipe(
    tap(data => {
      if (data.status == 200) {
        this.ConfigSensor = [];
        let contentForm = [];
        let _this=this;
        data.result.parameters.forEach(element => {
        this.ConfigSensor.push(
            {
              key:element,
              value:'',
              active:false,
              basic:false,
            }
          );
          contentForm.push(_this.fb.group({
            key:new FormControl(element),
            value:new FormControl(element),
            active:new FormControl(false),
            basic:new FormControl(false),
          }));
        });
        this.ioSetting =  this.fb.group({
          itemRows:this.fb.array(contentForm)
        });

      }
    }),
    takeUntil(this.unsubscribe),
    finalize(() => {
      this.cdr.markForCheck();
    })
  ).toPromise();
 }

 buildFormExtension()
 {
  
   this.extension = this.fb.group({
     qcvn : true,
     hasCamera:true,
     hasBattery:true
   });
 }

  editFnc(id: number, content) {
    this.titlePopup = TITLE_FORM_EDIT;    
    this.currentForm = this.formEdit;
    this.deviceTypeService.detail(id).pipe(
      tap(body => {
      
        
        this.currentModel = body.result as DeviceType;
        this.createFormEdit(this.currentModel);
        this.listIcon = [{
          icon:this.currentModel.image,
        }];
        this.currentForm = this.formEdit;
        this.modalService.open(content, { windowClass: 'kt-mt-50 modal-holder modal-dialog-scrollable modal-lg-800', size: 'lg', backdrop: 'static' }).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          this.currentReason = reason;
        });
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  addFnc(content) {
    this.buildFormIoSetting();
    this.listIcon=[
      {
        icon : null,
      }
    ];
    this.titlePopup = TITLE_FORM_ADD;
    this.createFormAdd();
    this.currentForm = this.formAdd;
    this.currentModel = new DeviceType();
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-holder modal-dialog-scrollable modal-lg-800', size: 'lg', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }

  deleteFnc(id: number, content) {
    this.titlePopup = TITLE_FORM_DELETE;
    this.currentModel = new DeviceType();
    this.currentModel.id = id;
    this.modalService.open(content, { windowClass: 'kt-mt-50  modal-holder modal-delete', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }

  	/**
	 * Submit actions
	 */
  onSubmit() { 

    switch (this.action) {
      case "edit":
        if (!this.currentForm.invalid) {
          this.editAction();
        }
        break;
      case "add":
        if (!this.currentForm.invalid) {
          this.addAction();
        }
        break;
      case "delete":
        this.deleteAction();
        break;
    }
  }

  get f() {
    if(this.formAdd != undefined) return this.formAdd.controls;
   }

  addAction() {
    this.currentModel.name = this.currentForm.get('name').value;
    this.currentModel.sortOrder = this.currentForm.get('sortOrder').value;
    this.currentModel.description = this.currentForm.get('description').value;
    //protocol
    this.currentModel.protocol = this.currentForm.get('protocol').value;
    this.currentModel.modelName = this.currentForm.get('modelName').value;
    this.currentModel.link = this.currentForm.get('link').value;
    this.currentModel.manufacturer = this.currentForm.get('manufacturer').value;
    //extension
    let extension = {
      qcvn:this.extension.value.qcvn,
      hasbattery : this.extension.value.hasbattery,
      hasCamera:this.extension.value.hasCamera
    };
    this.currentModel.extensions = extension;
    if(this.selectedFile['icon'])
    {
      this.currentModel.image = this.selectedFile['icon'];
    }
    // iosetting
    this.currentModel.ioSetting = this.ioSetting.value.itemRows;  
    //command
    
    this.currentModel.commands = this.formArr.value;  
    this.currentService.create(this.currentModel, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 201) {
          this.modalService.dismissAll(this.currentReason);
          this.dataTable.reload({});
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  editAction() {
    this.currentModel.name = this.currentForm.get('name').value;
    this.currentModel.sortOrder = this.currentForm.get('sortOrder').value;
    this.currentModel.description = this.currentForm.get('description').value;
    //protocol
    this.currentModel.protocol = this.currentForm.get('protocol').value;
    this.currentModel.modelName = this.currentForm.get('modelName').value;
    this.currentModel.link = this.currentForm.get('link').value;
    this.currentModel.manufacturer = this.currentForm.get('manufacturer').value;
    this.currentModel.image =  this.currentForm.get('image').value;
    if(this.selectedFile['icon'])
    {
      this.currentModel.image = this.selectedFile['icon'];
    }
    //extension
    let extension = {
      qcvn:this.extension.value.qcvn,
      hasBattery : this.extension.value.hasBattery,
      hasCamera:this.extension.value.hasCamera
    };
    this.currentModel.extensions = extension;
// iosetting
    this.currentModel.ioSetting = this.ioSetting.value.itemRows;  
     //command   
    this.currentModel.commands = this.formArr.value;
    
    this.currentService.update(this.currentModel, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.modalService.dismissAll(this.currentReason);
          this.dataTable.reload({});
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  deleteAction() {
    this.deviceTypeService.delete(this.currentModel.id, { notifyGlobal: true }).pipe(
      tap(data => {
        if (data.status == 200) {
          this.modalService.dismissAll(this.currentReason);
          this.dataTable.reload({});
        }
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  removeCommand(index: number) {    
    // find real index in states
    let position = states.indexOf(this.formArr.value[index].name);
    this.states.splice(position, 0, this.formArr.value[index].name);
    this.formArr.removeAt(index);  
  }

  addCommand() {    
    
    this.formArr.push(this.fb.group({
      name:[this.formAddCommand.controls['key'].value],
      commandStr:[this.formAddCommand.controls['command'].value],
      password:[this.formAddCommand.controls['password'].value ? 1 : 0],
    })); 
    
    //remove added item in the list
    this.states = this.states.filter(item => item != this.formAddCommand.controls['key'].value);

    // scroll view center
    this.formAddCommand.reset(); 
  }
  
  get formArr():FormArray {  
    if(this.commandsForm){
      return this.commandsForm.get('itemRows') as FormArray;  
    }  
  }

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.currentForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
  /**
   * Change size databale
   * @param $elm 
   */
  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    // this.cdr.detectChanges();
  }
  /**
 * Dismiss Reason Popup
 * @param reason 
 */
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }  
  search = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    // const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => {
      
      return (!this.instance || !this.instance.isPopupOpen())
    }));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.states
        : states.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)))
    );
  }
  get fAddCommand() {  
    if(this.formAddCommand){
      return this.formAddCommand.controls;  
    }  
  }

}
