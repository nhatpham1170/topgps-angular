import { Component, OnInit, ElementRef, ChangeDetectorRef, ViewChild, EventEmitter } from '@angular/core';
import { finalize, takeUntil, tap, debounceTime, merge } from 'rxjs/operators';
import { Subject, observable } from 'rxjs';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { Device, DeviceTypeService } from '@core/admin';
import { Device, DeviceService, User, GeofenceModel, PointService, MovePointModel, AddPointModel, EditTransactionModel } from '@core/manage';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { ToastService, UserDatePipe, ValidatorCustomService, UserDateAdvPipe } from '@core/_base/layout';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { DecimalPipe } from '@angular/common';
import { UserManageService } from '@core/manage/_service/user-manage.service';
import { CurrentUserService } from '@core/auth';
import { UserAutocompleteComponent } from '@app/views/partials/content/widgets/user-autocomplete/user-autocomplete.component';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { Lightbox } from 'ngx-lightbox';
import { TranslateService } from '@ngx-translate/core';
import { ExcelService, XLSXModel } from '@core/utils';
import { RequestFileService } from '@core/common/_services/request-file.service';
import { RequestFile } from '@core/common/_models/request-file';
import { UploadFileOptionModel } from '@app/views/partials/content/widgets/upload-file/upload-file.component';
import { FeedbackService, FeedbackModelEdit } from '@core/common';

declare var $: any;
declare var ClipboardJS: any;
const TITLE_FORM_EDIT: string = "POINTS.MANAGE.GENERAL.EDIT_TRANSACTION";

@Component({
  selector: 'kt-feedback-manage',
  templateUrl: './feedback-manage.component.html',
  styleUrls: ['./feedback-manage.component.scss'],
  providers: [UserDateAdvPipe, DecimalPipe]
})

export class FeedbackManageComponent implements OnInit {
  public dataTable: DataTable = new DataTable();
  public dataTableHistory: DataTable = new DataTable();
  public action: string;
  public currentReason: any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public formEdit: FormGroup;
  public formInfo: FormGroup;
  private tableConfig: any;
  public currentModel: GeofenceModel;
  public commandsForm: FormGroup;
  public formEditTransaction: FormGroup;
  public currentItemSelected: any;

  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  private userIdSelecte: number;
  public currentUser: User;
  public userLogin: any;
  public dateTimeServer: string;
  // feature device  
  public showUserTree = true;
  public allowReOrder: boolean = false;
  public titlePopup: string;
  public geofenceEdit: GeofenceModel;
  public processAction: boolean;
    // permisison 
  public permissions: any = {
    sell: "device.action.sell"
  }
  // [options]="{singleDatePicker:true,size: 'md',select: 'from',timePicker:false,format:'date',autoSelect:false,btnClear:true,optionDatePicker:{autoApply:true}}"
  public datePickerOptions: any = {
    size: 'md',
    select: 'from',
    ranges: 'from',
    optionDatePicker: {
      maxSpan: {
        days: 31
      },
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: false,
    selectRange: "LAST_7_DAYS",
  };
  public payAtEditOptions: any = { singleDatePicker: true, size: 'md', select: 'from', timePicker: false, format: 'date', autoSelect: false, btnClear: true, optionDatePicker: { autoApply: true } };
  public optionsUploadFile: UploadFileOptionModel = {
    crop: false,
    close: false,
    upload: false
  }
  public listDevices: Array<Device> = [];
  private dataSearch: any = {};
  @ViewChild('datePicker', { static: true }) datePicker: DateRangePickerComponent;

  constructor(
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private toast: ToastService,
    private userTreeService: UserTreeService,
    private validatorCT: ValidatorCustomService,
    private currentUserService: CurrentUserService,
    private feedbackService: FeedbackService,
    private userDateAdv: UserDateAdvPipe,
    private lightBox: Lightbox,
    private translate: TranslateService ) {
    this.action = "";
    this.tableConfig = {
      pagination: [
        10, 20, 30, 50
      ],
      columns: [
        {
          title: '#',
          field: 'id',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '20px' },
          width: 20,
          class: '',
          translate: '#',
          autoHide: false,
          headerShow: false,
        },
        // {
        //   title: 'date',
        //   field: 'date',
        //   allowSort: true,
        //   isSort: false,
        //   dataSort: '',
        //   style: { 'width': '130px' },
        //   width: 130,
        //   class: '',
        //   translate: 'COMMON.COLUMN.DATE',
        //   autoHide: true,
        //   headerShow:false,
        // },
        {
          title: 'name',
          field: 'name',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.NAME',
          autoHide: true,
          headerShow: true,
        },
        {
          title: 'phone',
          field: 'phone',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.PHONE',
          autoHide: true,
          headerShow: true,
        },
        {
          title: 'email',
          field: 'email',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.EMAIL',
          autoHide: true,
          headerShow: true,
        },
        {
          title: 'content',
          field: 'content',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '200px' },
          width: 200,
          class: '',
          translate: 'COMMON.COLUMN.CONTENT',
          autoHide: true,
          headerShow: true,
        },
        {
          title: 'created date',
          field: 'createdAt',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.CREATED_DATE',
          autoHide: true,
          headerShow: true,
        },
        {
          title: 'user name',
          field: 'username',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.USERNAME',
          autoHide: true,
          headerShow: true,
        },
        {
          title: 'ip address',
          field: 'ipAddress',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.IP_ADDRESS',
          autoHide: true,
          headerShow: true,
        },
        {
          title: 'flatform',
          field: 'flatform',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.FLATFORM',
          autoHide: true,
          headerShow: true,
        },

        {
          title: 'status',
          field: 'status',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.STATUS',
          autoHide: true,
          headerShow: true,
        },
        {
          title: 'description',
          field: 'description',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.DESCRIPTION',
          autoHide: true,
          headerShow: true,
        },

        {
          title: 'updated by',
          field: 'modiffiedBy',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.UPDATED_BY',
          autoHide: true,
          headerShow: true,
        },
        {
          title: 'updated at',
          field: 'updatedAt',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.UPDATED_DATE',
          autoHide: true,
          headerShow: true,
        },
        {
          title: 'Actions',
          field: 'actions',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '117px', 'text-align': 'center' },
          width: 60,
          class: '',
          translate: 'COMMON.COLUMN.ACTIONS',
          autoHide: false,
          headerShow: false,
        },
      ]
    };

    this.unsubscribe = new Subject();
    this.dataTable.init({
      data: [],
      totalRecod: 0,
      paginationSelect: [10, 20, 30, 50],
      columns: this.tableConfig.columns,
      formSearch: '#formSearch',
      pageSize: 10,
      // isDebug: true,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,

        },
        responsive: false
        // selecter: true,
      }
    });
    this.currentReason = {};
    this.parrentEmiter = new EventEmitter();
    this.currentUser = new User();
    this.dateTimeServer = "";
  }
  ngOnInit() {
    this.userLogin = this.currentUserService.currentUser;
    // listening dataTable event
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        // option.parentId = option.userId;
        // delete option.userId;
        this.getData(option);
      })
    ).subscribe();

    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();

    $(function () {
      $('.kt_selectpicker').selectpicker();
    });
    let _this = this;
    setTimeout(() => {
      _this.search();
    })
  }

	/**
	 * Get list by data option
	 * @param option option search list
	 */
  private getData(option) {
    let _this = this;
    if (option['orderBy'] == undefined || option['orderBy'].length == 0) {
      option['orderBy'] = "createdAt";
      option['orderType'] = "desc";
    }

    this.feedbackService.list({ params: option }).pipe(
      tap((data: any) => {
        this.dateTimeServer = data.datetime;

        data.result.content = data.result.content.map(x => {
          x.flatformText = "";
          x.flatformText = x.flatform.cornerstone
          if (x.flatform.type.length > 0) {
            if (x.flatformText.length > 0) x.flatformText += " - " + x.flatform.type;
            else x.flatformText += x.flatform.type;
          }
          if (x.flatform.browser.length > 0) {
            if (x.flatformText.length > 0) x.flatformText += " - " + x.flatform.browser;
            else x.flatformText += x.flatform.browser;
          }
          return x;
        });
        this.dataTable.update({
          data: data.result.content,
          totalRecod: data.result.totalRecord
        });
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.dataTable.isLoading = false;
        this.cdr.markForCheck();
        this.cdr.detectChanges();
      })
    ).subscribe();
  }

  search(data?: any) {
    let listSearch = [];

    if (this.userIdSelecte != undefined) {
      listSearch.push({ key: 'userId', value: this.userIdSelecte });
    }
    listSearch.push({ key: 'timeFrom', value: this.dataSearch['timeFrom'] });
    listSearch.push({ key: 'timeTo', value: this.dataSearch['timeTo'] });
    if (this.dataSearch['userObj']) {
      listSearch.push({ key: 'userIdObj', value: this.dataSearch['userObj']['item']['id'] });
    }
    else if (this.dataSearch['userName']) {
      listSearch.push({ key: 'userNameObj', value: this.dataSearch['userName'] });
    }
    this.dataTable.search(data || listSearch);
  }

  resetFormSearch() {

    this.dataTable.resetSearch();
    this.datePicker.refresh();
    let listSearch = [];
    listSearch.push({ key: 'timeFrom', value: this.dataSearch['timeFrom'] });
    listSearch.push({ key: 'timeTo', value: this.dataSearch['timeTo'] });
    listSearch.push({ key: 'userId', value: this.userIdSelecte });
    listSearch.push({ key: 'orderBy', value: 'createdAt' });
    listSearch.push({ key: 'orderType', value: 'desc' });

    this.search(listSearch);
  }

  dateRouteChange(data) {
    this.dataSearch["timeFrom"] = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dataSearch["timeTo"] = data.endDate.format("YYYY-MM-DD HH:mm:ss");
  }

  createFormEdit(item) {
    this.formEdit = this.fb.group({
      id: [item.id],
      status: [item.status || "", this.validatorCT.select],
      description: [item.description || ""]
    });
    setTimeout(() => {
      $('#feedbackStatus').selectpicker();
      $('#feedbackStatus').val(item.status).selectpicker('refresh');
    })

  }
  createFormInfo(item) {
    this.formInfo = this.fb.group({
      name: [item.name || ""],
      phone: [item.phone || ""],
      email: [item.email || ""],
      content: [item.content || ""],
      createdAt: [this.userDateAdv.transform(item.createdAt, "datetime") || ""],
      username: [item.username || ""],
      ipAddress: [item.ipAddress || ""],
      flatform: [item.flatformText || ""],
      userAgent: [item.userAgent || ""],
      status: [this.getStatus(item.status) || ""],
      description: [item.description || ""],
      modiffiedBy: [item.modiffiedBy || ""],
      updatedAt: [this.userDateAdv.transform(item.updateAt, "datetime") || ""],
    })
  }

  open(content, type, item?: any) {
    this.action = type;
    this.currentItemSelected = item;
    switch (type) {
      case 'edit':
        this.editFnc(content, item);
        break;
      case 'info':
        this.infoFnc(content, item);
        break;
    }
  }
  onSubmit() {
    switch (this.action) {
      case "edit":
        this.editAction();
        break;
    }
  }

  /* Function */
  editFnc(content, item) {
    this.titlePopup = TITLE_FORM_EDIT;
    this.createFormEdit(item);
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal-sm', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }
  infoFnc(content, item) {
    this.titlePopup = TITLE_FORM_EDIT;
    this.createFormInfo(item);
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal-lg', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }

  /* Action */
  editAction() {
    this.formEdit.markAllAsTouched();
    if (this.formEdit.valid) {
      this.processAction = true;
      let model = new FeedbackModelEdit();
      model.id = this.formEdit.get('id').value;
      model.status = this.formEdit.get('status').value;
      model.description = this.formEdit.get('description').value;
      this.feedbackService.update(model, { notifyGlobal: true }).pipe(
        debounceTime(300),
        tap(result => {
          if (result.status === 200) {
            this.modalService.dismissAll(this.currentReason);
            this.dataTable.reload({});
          }
        },
          error => {
          }),
        finalize(() => {
          this.processAction = false;
          this.cdr.markForCheck();
          this.cdr.detectChanges();
        })
      )
        .subscribe();
    }
  }
  onChangeStatus(value) {

  }
  getStatus(status) {
    switch (status) {
      case 0:
        return this.translate.instant('COMMON.FEEDBACK.STATUS.PENDDING');
        break;
      case 1:
        return this.translate.instant('COMMON.FEEDBACK.STATUS.COMPLETE');
        break;
      case 2:
        return this.translate.instant('COMMON.FEEDBACK.STATUS.SKIP');
        break;
      default:
        return this.translate.instant('COMMON.FEEDBACK.STATUS.PENDDING');
        break;
    }
  }

  get fEdit() {
    return this.formEdit.controls;
  }

  get fInfo() {
    return this.formInfo.controls;
  }


  /**
  * Change size dataTabel
  * @param $elm 
  */
  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  copied(val) {
    this.toast.copied(val);
  }

  /**
  * Dismiss Reason Popup
  * @param reason 
  */
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
