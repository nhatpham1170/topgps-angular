import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { finalize, takeUntil, tap ,debounceTime} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { TranslateService } from '@ngx-translate/core';
import { IconType,IconTypeService } from '@core/admin';
import { DomSanitizer} from '@angular/platform-browser';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Content } from '@angular/compiler/src/render3/r3_ast';
import { environment } from '@env/environment.prod';

declare var $: any;
@Component({
  selector: 'kt-icon-type',
  templateUrl: './icon-type.component.html',
  styleUrls: ['./icon-type.component.scss'],
})
export class IconTypeComponent implements OnInit {
  formIcontype: FormGroup;
  searchFormIcontype : FormGroup;
  selectdp: ElementRef;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idIcontypeEdit: number;
  public dataDefault: any = [{}];
  public idIcontypeDelete: any;
  public isIcontypeError: boolean = false;
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public selectedFile : any = [
    {
      icon : File,
      iconMap:File
    }
  ];
  public listIcon : any = [{
    icon : null,
    iconMap:null
  }];
  constructor(
    private icontype: IconTypeService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    private sanitizer:DomSanitizer,
    private modalService: NgbModal,
  ) {
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    this.updateDataTable();
    this.getData();
   }

  private buildForm(): void {
    this.formIcontype = this.formBuilder.group({
      nameIcontype: ['',Validators.required],
      namekey : ['',Validators.required],
      icon:[''],
      iconMap:[''],
      iconSVG:[''],
      iconMapSVG:[''],
      sortOrder:[''],
    });
    this.searchFormIcontype = this.formBuilder.group({
      nameIcontype: [''],
    })
  }

  buttonAddNew(content) {
    this.listIcon=[
      {
        icon : null,
        iconMap: null
      }
    ];
    this.isEdit = false;
    this.formIcontype.reset();
    this.open(content);
  }

  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-800',size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  

  onSubmitIcontype(form: any) {
    if (this.formIcontype.invalid) {
      return;
    }
    let params = {
      name: form.value.nameIcontype,
      nameKey: form.value.namekey,
      iconSVG: form.value.iconSVG,
      iconMapSVG:form.value.iconMapSVG,
      sortOrder: form.value.sortOrder
    } as IconType;
    if (this.isEdit) {
      params.id = this.idIcontypeEdit;
    }
    let formData = this.convertFormData(params);
    if (this.isEdit) {
      params.id = this.idIcontypeEdit;
      this.icontype.update(formData,{ notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
          this.getData();
          this.closeModal();
        }
      });
      return;
    }
    this.icontype.create(formData,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        this.getData();
        this.closeModal();
      }})
   
  }

  convertFormData(params){
    let formData = new FormData();
    Object.keys(params).forEach(x => {
      formData.set(x, params[x]);
    });
    if(this.selectedFile.icon) formData.append("icon",this.selectedFile.icon,this.selectedFile.icon.name);
    if(this.selectedFile.iconMap) formData.append("iconMap",this.selectedFile.iconMap,this.selectedFile.iconMap.name); 
    return formData;
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10
    };
  }

  onHideModal(id: string) {
    let idModal = '#' + id;
    $(idModal).modal('hide');
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }

  getIdAction(id,idModal) {
    this.idIcontypeDelete = id;
    this.openModal({idModal:idModal,confirm : true});
  }

  openModal(params) {
     let idModal = params.idModal;
     let confirm = params.confirm;
     if(confirm)
     {$(idModal).appendTo('body').modal({backdrop: 'static',keyboard: false})}
     else{$(idModal).appendTo('body').modal('show');}
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  deleteIcontype() {
    let id = this.idIcontypeDelete;
    this.icontype.delete(id,{ notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.getData();
      }
    });
    this.onHideModal('modal-delete-icontype');
  }

  editIcontype(id,content) {
    this.isEdit = true;
    this.idIcontypeEdit = id;
    let params = {
      id :id
    };
    this.icontype.list(params).pipe(
      tap((data: any) => {
        this.dataDefault =  data.result.content;
        let iconUrl = data.result.content[0].iconUrl;
        if(iconUrl != null && iconUrl != '' && iconUrl != undefined)
        {
           iconUrl = data.result.content[0].iconUrl;
        }else{iconUrl = null}
        let iconMapUrl = data.result.content[0].iconMapUrl;
        if(iconMapUrl != null && iconMapUrl != '' && iconMapUrl != 'undefined')
        {
          iconMapUrl =  data.result.content[0].iconMapUrl;
        }else{iconMapUrl = null}
        this.listIcon = [{
          icon:iconUrl,
          iconMap : iconMapUrl
        }];

      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe();
    this.open(content);
  }

  loadSelectBootstrap(){
		$('.bootstrap-select').selectpicker();
  }

  searchIcontype(form:any)
  {
    this.filter.name = form.value.nameIcontype;
    this.getData();
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter = option;       
        this.getData();
      }),
      
    ).subscribe();
  }

  get f() {
   if(this.formIcontype != undefined) return this.formIcontype.controls;
  }

  getData() {
    this.icontype.list(this.filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        }) 
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          this.data = result.result.content;
          this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          $(function () {
            $('.kt_selectpicker').selectpicker();
          });
        }
      })
  }

  onFileChange(event,name : string = '') { 

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.selectedFile[name] = event.target.files.item(0);
      var mimeType = event.target.files[0].type;
      if (mimeType.match(/image\/*/) == null)
      {
        return;
      }
     this.convertFileToBase64(file,name);
    }
  }

  convertFileToBase64(file,name){

    var _this = this;
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      _this.cdr.markForCheck();
      _this.listIcon[0][name] = reader.result;
    };
  }

 sanitize(url:string){
      return this.sanitizer.bypassSecurityTrustUrl(url);
    }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout:{
				body:{
				  scrollable:false,
				  maxHeight:600,
				},
				// selecter:true,
			  }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();

  }
  
  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '40px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Name',
        field: 'name',
        allowSort: true,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'COMMON.COLUMN.NAME',
        autoHide: false,
        width: 100,
      },
      {
        title: 'Key name',
        field: 'keyName',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'ADMIN.DEVICE_ICON.GENERAL.NAME_KEY',
        autoHide: false,
        width: 100,
      },
      {
        title: 'ICON',
        field: 'icon',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px','text-align': 'center' },
        class: '',
        translate: 'ADMIN.DEVICE_ICON.GENERAL.ICON',
        autoHide: false,
        width: 100,

      },
      {
        title: 'Icon On Map',
        field: 'iconOnMap',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px','text-align': 'center' },
        class: '',
        translate: 'ADMIN.DEVICE_ICON.GENERAL.ICON_ON_MAP',
        autoHide: true,
        width: 100,

      },
      {
        title: 'Icon - SVG',
        field: 'icon',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px','text-align': 'center' },
        class: '',
        translate: 'ADMIN.DEVICE_ICON.GENERAL.ICON_SVG',
        autoHide: true,
        width: 100,

      },
      {
        title: 'Icon on map SVG',
        field: 'iconOnMapSVG',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px','text-align': 'center' },
        class: '',
        translate: 'ADMIN.DEVICE_ICON.GENERAL.ICON_ON_MAP_SVG',   
        autoHide: true,
        width: 100,

      },
      {
        title: 'Count',
        field: 'count',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px','text-align': 'center' },
        class: '',
        translate: 'ADMIN.DEVICE_ICON.GENERAL.COUNT',
        autoHide: true,
        width: 100,

      },
      {
        title: 'Edit By',
        field: 'editBy',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px','text-align': 'center' },
        class: '',
        translate: 'COMMON.EDIT_BY',
        autoHide: false,
        width: 100,
      },
      {
        title: 'Created update',
        field: 'created',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px','text-align': 'center' },      
        class: '',
        translate: 'COMMON.COLUMN.UPDATED_DATE',
        autoHide: true,
        width: 100,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: false,
        width: 117,
      },
    ]
  }


}
