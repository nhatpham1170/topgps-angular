import { Component, OnInit, Input, ElementRef, ChangeDetectorRef,ViewChild} from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { finalize, takeUntil, tap ,debounceTime,distinctUntilChanged,map,filter, delay} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { DomSanitizer} from '@angular/platform-browser';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { ToastService } from '@core/_base/layout';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { PoiType,PoiTypeService,ListIconPoiType } from '@core/admin';

import * as moment from 'moment';

declare var $: any;
@Component({
  selector: 'kt-poi-type-component',
  templateUrl: './poi-type.component.html',
  styleUrls: ['./poi-type.component.scss'],
  providers: [ListIconPoiType]

})
export class PoiTypeComponent implements OnInit {

  formPoiType: FormGroup;
  searchformPoiType : FormGroup;
  selectdp: ElementRef;
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataEdit: any;
  public isEdit: boolean = false;
  public idPoiTypeEdit: number;
  public dataDefault: any = [{}];
  public idPoiTypeDelete: any;
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public userIdSelected:number;
  public keySearchFilter:string = 'name';
  public showUserTree = true;
  public colorBackgroup:string = '#5d78ff';
  constructor(
    private listIconPoiType :ListIconPoiType,

    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private sanitizer:DomSanitizer,
    private modalService: NgbModal,
    private userTreeService: UserTreeService,
    private toast: ToastService,
    private poiService : PoiTypeService
  ) {
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
  }

  ngOnInit() {
    this.getData();
    this.updateDataTable();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();
   }

  private buildForm(): void {
    this.formPoiType = this.formBuilder.group({
        name: ["",Validators.required],
        type: ["",Validators.required],
        fillColor:[""],
        description: [""],
        createDate: [{value: '', disabled: true}],
        createBy : [{value: '', disabled: true}]
    });
    this.searchformPoiType = this.formBuilder.group({
        name: [''],
    });
  }


  resetFormSearch()
  {
    this.searchformPoiType.reset();
    this.buildPagination();
    this.dataTable.reload({ currentPage: 1 });

  }
  
  buttonAddNew(content) {
    this.colorBackgroup = '#5d78ff';
    this.isEdit = false;
    this.formPoiType.reset();
    this.open(content);
  }

  open(content) {           
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder',backdrop:'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  getIdAction(id, idModal) {
    this.idPoiTypeDelete = id;
    this.modalService.open(idModal, { windowClass: 'kt-mt-50  modal-holder modal-delete',backdrop:'static'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    }); }

  onSubmitPoiType(form: any) {
    if (this.formPoiType.invalid) {
      return;
    }
    let params = {
      name:this.formPoiType.value.name,
      type:this.formPoiType.value.type,
      description : this.formPoiType.value.description,
      fillColor : this.colorBackgroup
    } as PoiType;
    if (this.isEdit) {
      params.id = this.idPoiTypeEdit;
      this.poiService.update(params,{ notifyGlobal: true }).subscribe((result: any) => {
        if (result.status == 200) {
        this.dataTable.reload({ currentPage: 1 });
        //   this.getData(this.userIdSelected);
          this.closeModal();
        }
      });
      return;
    }
    this.poiService.create(params,{ notifyGlobal: true })
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      if (result.status == 201) {
        // this.getData(this.userIdSelected);
        this.dataTable.reload({ currentPage: 1 });

        this.closeModal();
      }})
  }

  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10
    };
  }
    
  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40,50];
  }
 
  ChangeUserTree(value) {
    
  }

  changeColor(value)
  {
    this.colorBackgroup = value.target.value;
    
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  deletePoiType() {
    let id = this.idPoiTypeDelete;
    this.poiService.delete(id,{ notifyGlobal: true }).subscribe((data: any) => {
      if (data.status == 200) {
        this.dataTable.reload({ currentPage: 1 });
        // this.getData(this.userIdSelected);
        this.closeModal();
      }
    });
  }

   editPoiType(item,content){       
    this.isEdit = true;
    this.idPoiTypeEdit = item.id;
    this.open(content);  
    // this.beginDate = item.beginDate;
    // this.expired = item.expireDate;
    this.dataDefault = [item];    
  }

  loadSelectBootstrap(){
    $('.bootstrap-select').selectpicker();
  }

  searchPoiType(form:any)
  {
    if(form.value.name == null) form.value.name = '';
    let params = {
        name : form.value.name,
    } as PoiType;
    this.filter.name = params.name;
    this.dataTable.reload({ currentPage: 1 });

    // this.getData(this.userIdSelected);
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;     
       this.getData();
      }),
    ).subscribe();
  }

  get f() {
   if(this.formPoiType != undefined) return this.formPoiType.controls;
  }

  getData() {    
    this.poiService.list(this.filter)
      .pipe(
        delay(300),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
        //example data
        result.result.content.map(poiType=>{
          // let name = "ADMIN.POI_TYPE.LANGUAGE."+poiType.name;
          // poiType.name = name;
          let iconClass = this.listIconPoiType.getIconByKey(poiType.type);
          poiType.iconClass = iconClass;
          return poiType;
        });
        this.data = result.result.content;      
        this.totalRecod = result.result.totalRecord;
          this.dataTable.update({
            data: this.data,
            totalRecod: this.totalRecod
          });
          // setTimeout(()=>{
          //   $('select').selectpicker();
          // })
          
        }
      })
  }


 sanitize(url:string){
      return this.sanitizer.bypassSecurityTrustUrl(url);
    }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout:{
        body:{
          scrollable:false,
          maxHeight:600,
        },
        selecter:false,
        }
    });
  }

  copied(val) {
    this.toast.copied(val);
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }
  
  setColumns() {
    this.columns = [
      { 
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'Name',
        field: 'name',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '200px' },
        class: '',
        translate: 'ADMIN.POI_TYPE.GENERAL.KEY',
        autoHide: true,
        width: 100,
      },
      {
        title: 'type POI',
        field: 'name',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'COMMON.COLUMN.TYPE',
        autoHide: true,
        width: 150,
      },
    
      {
        title: 'descript',
        field: 'updated',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '200px' },
        class: '',
        translate: 'COMMON.COLUMN.DESCRIPTION',
        autoHide: true,
        width: 200,
      },
      {
        title: 'create by',
        field: 'updated',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '120px'},
        class: '',
        translate: 'COMMON.COLUMN.CREATED_BY',
        autoHide: true,
        width: 120,
      },
      {
        title: 'create date',
        field: 'updated',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '120px'},
        class: '',
        translate: 'COMMON.COLUMN.CREATED_DATE',
        autoHide: true,
        width: 120,
      },
      {
        title: 'Actions',
        field: 'action',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '117px' },
        class: '',
        translate: 'COMMON.ACTIONS.ACTIONS',
        autoHide: true,
        width: 117,
      },
    ]
  }


}




