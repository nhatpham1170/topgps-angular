import { Component, OnInit, ElementRef, ChangeDetectorRef, ViewChild, EventEmitter } from '@angular/core';
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject, observable } from 'rxjs';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { Device, DeviceTypeService } from '@core/admin';
import { Device, DeviceService, User, GeofenceService, GeofenceModel, PointService, MovePointModel, AddPointModel } from '@core/manage';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { HttpClient } from '@angular/common/http';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { TranslateService } from '@ngx-translate/core';
import { ToastService, UserDatePipe, ValidatorCustomService } from '@core/_base/layout';
import { DeviceUtilityService } from '@core/manage/utils/device-utility.service';
import { ExcelService } from '@core/utils';
import { XLSXModel } from '@core/utils/xlsx/excel.service';
import { headersToString } from 'selenium-webdriver/http';
import { RequestFileService } from '@core/common/_services/request-file.service';
import { RequestFile } from '@core/common/_models/request-file';
import { DeviceConfigService } from '@core/common/_services/device-config.service';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { ReportDeviceService } from '@core/report';
import { MapConfig, MapConfigModel } from '@core/utils/map';
import { DecimalPipe } from '@angular/common';
import { TrackingService } from '@core/map';
import { UserManageService } from '@core/manage/_service/user-manage.service';
import { select } from '@ngrx/store';
import { CurrentUserService } from '@core/auth';
import { DomSanitizer } from '@angular/platform-browser';
import { UserAutocompleteComponent } from '@app/views/partials/content/widgets/user-autocomplete/user-autocomplete.component';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';

declare var $: any;
declare var ClipboardJS: any;
const TITLE_FORM_EDIT: string = "COMMON.ACTIONS.EDIT";
const TITLE_FORM_ADD: string = "COMMON.ACTIONS.ADD";
const TITLE_FORM_DELETE: string = "COMMON.ACTIONS.DELETE";
const TITLE_FORM_MOVE_POINT: string = "COMMON.ACTIONS.MOVE";

@Component({
  selector: 'kt-point-manage',
  templateUrl: './point-manage.component.html',
  styleUrls: ['./point-manage.component.scss'],
  providers: [UserDatePipe, DecimalPipe]
})

export class PointManageComponent implements OnInit {
  public dataTable: DataTable = new DataTable();
  public action: string;
  public currentReason: any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public formEdit: FormGroup;
  public formAdd: FormGroup;
  private tableConfig: any;
  private currentForm: FormGroup;
  public currentModel: GeofenceModel;
  public commandsForm: FormGroup;
  public formAddPoint: FormGroup;
  public formMovePoint: FormGroup;
  public isShowSearchAdvanced: boolean;
  public deviceType: any[];
  public simType: any[];
  public groupDevice: any[];

  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  private userIdSelect: number;
  public currentUser: User;
  public userLogin: any;
  public dateTimeServer: string;
  // feature device  
  public showUserTree = true;  
  public titlePopup: string;  
  public payAtOptions: any = { singleDatePicker: true, size: 'md', select: 'from', timePicker: false, format: 'date', autoSelect: false, btnClear: true, optionDatePicker: { autoPlay: true } };
 
  private billImgMove:any;
  private dataSearch:{} = {};
  @ViewChild('payAtCpn', { static: false }) payAtCpn: DateRangePickerComponent;
  @ViewChild('userAutocomplete',{static:true}) userAutocomplete:UserAutocompleteComponent;
  public listDevices: Array<Device> = [];  
  public processAction:boolean;
  constructor(
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private toast: ToastService,
    private userTreeService: UserTreeService,
    private userService:UserManageService,
    private validatorCT:ValidatorCustomService,
    private currentUserService:CurrentUserService,
    private pointService:PointService,    
  
  ) {
    this.action = "";
    this.tableConfig = {
      pagination: [
        10, 20, 30, 50
      ],
      
      columns: [
        {
          title: '#',
          field: 'id',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '20px' },
          width: 20,
          class: '',
          translate: '#',
          autoHide: false,
        },
        {
          title: 'Username',
          field: 'username',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '110px', 'margin': '0px' },
          width: 110,
          class: '',
          translate: 'COMMON.COLUMN.USERNAME',
          autoHide: false,
        },
        {
          title: 'Roles',
          field: 'roleName',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '140px' },
          width: 140,
          class: '',
          translate: 'MANAGE.USER.GENERAL.ROLE_NAME',
          autoHide: true,
        },
        {
          title: 'Full name',
          field: 'fullName',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'MANAGE.USER.GENERAL.FULL_NAME',
          autoHide: true,
        },
        {
          title: 'Phone',
          field: 'phone',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '110px' },
          width: 110,
          class: '',
          translate: 'MANAGE.USER.GENERAL.PHONE',
          autoHide: true,
        },
        {
          title: 'Email',
          field: 'email',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '110px' },
          width: 110,
          class: '',
          translate: 'MANAGE.USER.GENERAL.EMAIL',
          autoHide: true,
        },
        {
          title: 'Point',
          field: 'point',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '80px',},
          width: 80,
          class: '',
          translate: 'COMMON.COLUMN.POINT',
          autoHide: false,
        },
        {
          title: 'Status',
          field: 'status',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '104px', 'text-align':'center' },
          width: 104,
          class: '',
          translate: 'COMMON.COLUMN.STATUS',
          autoHide: true,
        },
        {
          title: 'Create At',
          field: 'createdAt',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '80px' },
          width: 80,
          class: '',
          translate: 'COMMON.COLUMN.CREATED_DATE',
          autoHide: true,
        },
        {
          title: 'Actions',
          field: 'actions',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px' }, 
          width: 100,
          class: '',
          translate: 'COMMON.COLUMN.ACTIONS',
          autoHide: true,
        },
      ]
    };
    this.unsubscribe = new Subject();    
    this.dataTable.init({
      data: [],
      totalRecod: 0,
      paginationSelect: [1, 2, 5, 10, 20],
      columns: this.tableConfig.columns,
      formSearch: '#formSearch',
      pageSize: 10,
      // isDebug: true,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600
        },
        // selecter: true,
      }
    });
    this.currentReason = {};
    this.parrentEmiter = new EventEmitter();
    this.currentUser = new User();
    this.dateTimeServer = "";
  }
  ngOnInit() {
    this.userLogin = this.currentUserService.currentUser;
    // listening dataTable event
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        option.parentId = option.userId;
        delete option.userId;
        this.getData(option);
      })
    ).subscribe();

    this.userTreeService.event.pipe(
      tap(action => {       
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();
    
    $(function () {
      $('select').selectpicker();     
    });
  }

	/**
	 * Get list by data option
	 * @param option option search list
	 */
  private getData(option) {
    if (option['orderBy'] == undefined || option['orderBy'].length == 0) {
      option['orderBy'] = "createdAt";
      option['orderType'] = "desc";
    }
    this.userService.list({ params: option }).pipe(
      tap((data: any) => {
        
        this.dateTimeServer = data.datetime;
        this.dataTable.update({
          data: data.result.content,
          totalRecod: data.result.totalRecord
        });
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
        this.cdr.detectChanges();
      })
    ).subscribe();
  }

  private createFormAddPoint(userReceive){
    this.formAddPoint =  this.fb.group({    
      receiveUserName: new FormControl({ value: userReceive.username, disabled: true }),
      receiveUserId: new FormControl({ value: userReceive.id, disabled: true }),
      transactionPoint: new FormControl({ value: 0, disabled: false },[this.validatorCT.min(6)]),     
      transactionDescription: new FormControl({ value: "", disabled: false },[]),
      transactionAmount: new FormControl({ value: 0, disabled: false },[this.validatorCT.min(0)]),
    });
    setTimeout(()=>{
      $("#formMovePoint .selectpicker").selectpicker();
    });
  }
  
  private createFromMovePoint(userReceive, userSend){
    this.formMovePoint =  this.fb.group({
      userSendUserId: new FormControl({ value: userSend.id, disabled: true }),
      userSendUserName: new FormControl({ value: userSend.username, disabled: true }),
      userSendCurrentPoint: new FormControl({ value: userSend.currentPoint, disabled: true }),
      receiveUserName: new FormControl({ value: userReceive.username, disabled: true }),
      receiveUserId: new FormControl({ value: userReceive.id, disabled: true }),
      transactionPoint: new FormControl({ value: 0, disabled: false },[this.validatorCT.min(6)]),
      transactionIsPay: new FormControl({ value: 2, disabled: false }),
      transactionDescription: new FormControl({ value: "", disabled: false },[]),
      transactionAmount: new FormControl({ value: 0, disabled: false },[this.validatorCT.min(0)]),
      payAt: new FormControl({ value: "", disabled: false }),
    });
    this.billImgMove = undefined;
    setTimeout(()=>{
      $("#formMovePoint .selectpicker").selectpicker();
    });
    this.payAtOptions.autoSelect = false;
    if(this.payAtCpn)this.payAtCpn.refresh();
  }

  get fMove(){
   return this.formMovePoint.controls;
  }

  get fAdd(){
    return this.formAddPoint.controls;
   }

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.currentForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
  /**
   * Change size databale
   * @param $elm 
   */
  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  search(data?: any) {
    let listSearch = [];

    if (this.userIdSelect != undefined) {
      listSearch.push({ key: 'userId', value: this.userIdSelect });
      
    }
    
    listSearch.push({ key: 'name', value: this.dataSearch['userName'] || "" });

    this.dataTable.search(data || listSearch);
  }
  resetFormSearch() {
    $('#formSearch .selectpicker').val('default').selectpicker("refresh");
    $('#formSearch .kt_datepicker').val("").datepicker("update");
    $("#formSearch :input").each(function () {
      switch (this.type) {
        case 'checkbox':
          $(this).prop('checked', false);
          break;
        default:
          $(this).val('');
          break;
      }
    });
    this.userAutocomplete.clear();
    this.search([{ key: 'userId', value: this.userIdSelect }, { key: 'orderBy', value: 'createdAt' }, { key: 'orderType', value: 'desc' }]);
  }

  userTreeChangeUser(value) {

    if (value.id > 0) {
      this.userIdSelect = value.id;
      this.currentUser = value;
      this.search();
    }
  }
  copied(val) {
    this.toast.copied(val);
  }

  // feature export excel file
  open(content, type, item?: any) {
    this.action = type;
    switch (type) {      
      case 'add':
        this.addFnc(content);
        break;
      case 'move':
        this.moveFnc(content,item);
        break;
    }
  }  
  addFnc(content) {
    this.titlePopup = TITLE_FORM_ADD;
    this.createFormAddPoint(this.userLogin);
    // check and get listDevices
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable  kt-modal-sm', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }
  moveFnc(content,item) {
    this.titlePopup = TITLE_FORM_MOVE_POINT;
    this.createFromMovePoint(item,this.userLogin);
    // await this.getListDevice(this.userIdSelect);
    // check and get listDevices
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal-sm', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });

  } 
  onSubmit() {
    switch (this.action) {
    
      case "add":
        this.addAction();
        break;     
      case "move":
        this.moveAction();
        break;
    }
  }
  addAction() {
    this.formAddPoint.markAllAsTouched();
    if(this.formAddPoint.valid){
      this.processAction = true;
      this.cdr.detectChanges();
      let addPointModel = new AddPointModel();            
        addPointModel.point = this.formAddPoint.get('transactionPoint').value;        
        addPointModel.description = this.formAddPoint.get('transactionDescription').value;        
        addPointModel.amount = this.formAddPoint.get('transactionAmount').value;        
        this.pointService.add(addPointModel ,{notifyGlobal:true}).pipe(
          debounceTime(300),
          tap(result=>{
            if(result.status===200|| result.status==201){
              this.modalService.dismissAll(this.currentReason);
              // this.currentUserService.setCurrentUser({currentPoint:result.result.currentPoint});
              this.parrentEmiter.emit({ id: this.currentUser.id, path: this.currentUser.path });
            }
          },
          error=>{
          }),
          finalize(()=>{
            this.processAction = false;
            this.cdr.markForCheck();
            this.cdr.detectChanges();
          })
        )
        .subscribe();
    }
  }
  moveAction(){
    this.formMovePoint.markAllAsTouched();
    if(this.formMovePoint.valid){
      if(this.formMovePoint.get('transactionPoint').value > this.userLogin.currentPoint){
        this.toast.show({type:'error',translate:"POINTS.MANAGE.VALIDATE.NOT_ENOUGH_POINTS"});
      }
      else{
        this.processAction = true;
        let movePointModel = new MovePointModel();

        movePointModel.userId = this.formMovePoint.get('receiveUserId').value;
        movePointModel.point = this.formMovePoint.get('transactionPoint').value;
        movePointModel.isPay = this.formMovePoint.get('transactionIsPay').value;
        movePointModel.description = this.formMovePoint.get('transactionDescription').value;
        movePointModel.amount = this.formMovePoint.get('transactionAmount').value;
        movePointModel.payAt = this.formMovePoint.get('payAt').value;
        if(this.billImgMove!=undefined) movePointModel.billImg = this.billImgMove;
        // if(this.selectedFile['billImg']!=undefined) movePointModel.billImg = this.selectedFile['billImg'];
        this.pointService.move(movePointModel ,{notifyGlobal:true}).pipe(          
          debounceTime(300),
          tap(result=>{
            if(result.status===200){
              this.modalService.dismissAll(this.currentReason);
              // this.currentUserService.setCurrentUser({currentPoint:result.result[0].user.currentPoint});
              this.parrentEmiter.emit({ id: this.currentUser.id, path: this.currentUser.path });
              this.dataTable.reload({});
            }
          },
          error=>{

          }),
          finalize(()=>{            
            this.processAction = false;
            this.cdr.markForCheck();
            this.cdr.detectChanges();
          })
        )
        .subscribe();
      }
    }
  }   
  billImgChange(value){
    this.billImgMove = value;
  }
  payAtMove(value){
    if(value.endDate){
      this.formMovePoint.controls['payAt'].setValue(value.endDate.format("YYYY-MM-DD HH:mm:ss"));      
    }
    else{
      this.formMovePoint.controls['payAt'].setValue("");
    }
  }
  onUserChange(userChange){
    this.dataSearch['userName'] = userChange.text;
    this.dataSearch['userObj'] = userChange.obj;
  }
  
  /**
* Dismiss Reason Popup
* @param reason 
*/
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  onChangePayType(value){
    if(value == 1){
      this.payAtOptions.autoSelect = true;
      this.payAtCpn.refresh();
      this.formMovePoint.get('payAt').setValidators(this.validatorCT.required);
      this.formMovePoint.get('payAt').updateValueAndValidity();
      this.formMovePoint.get('payAt').markAsTouched();
    }
    else{
      this.payAtOptions.autoSelect = false;
      this.payAtCpn.refresh();
      this.formMovePoint.get('payAt').clearValidators();
      this.formMovePoint.get('payAt').updateValueAndValidity();
      this.formMovePoint.get('payAt').markAsTouched();
    }
  }
}


