import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard, ModuleGuard } from '@core/auth';
import { CoreModule } from '@core/core.module';
import { PartialsModule } from '@app/views/partials/partials.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/core/common';
import { NgxMaskModule} from 'ngx-mask';

// libary
import { NgbModalModule, NgbModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ClipboardModule } from 'ngx-clipboard';
import { NgxPermissionsModule } from 'ngx-permissions';
import { LightboxModule } from 'ngx-lightbox';
// component
import { PointsComponent } from './points.component';
import { TransactionHistoryComponent } from './transaction-history/transaction-history.component';
import { RenewsHistoryComponent } from './renews-history/renews-history.component';
import { PointManageComponent } from './point-manage/point-manage.component';
import { SynthesisReportComponent } from './synthesis-report/synthesis-report.component';


const routes: Routes = [{
  path: '',
  component: PointsComponent,
  canActivate: [ModuleGuard, AuthGuard],
  // data: {
  //   // permisison: 'ROLE_map.map',
  //   },
  children: [
    {
      path: 'manage',
      component: PointManageComponent,
      canActivate: [AuthGuard, ModuleGuard],
      data: {
        permisison: 'ROLE_point.manage',
      }
    },
    {
      path: 'transaction-history',
      component: TransactionHistoryComponent,
      canActivate: [AuthGuard, ModuleGuard],
      data: {
        permisison: 'ROLE_point.transaction_history',
      }
    },
    {
      path: 'renews-history',
      component: RenewsHistoryComponent,
      canActivate: [AuthGuard, ModuleGuard],
      data: {
        permisison: 'ROLE_point.renews_history',
      }
    },
    {
      path: 'synthesis-report',
      component: SynthesisReportComponent,
      canActivate: [AuthGuard, ModuleGuard],
      data: {
        permisison: 'ROLE_point.synthesis_report',
      }
    },
  ]
}

]

@NgModule({
  declarations: [
    PointsComponent,
    TransactionHistoryComponent,
    RenewsHistoryComponent,
    PointManageComponent,
    SynthesisReportComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,
    RouterModule.forChild(routes),
    PartialsModule,
    NgbModule,
    NgbModalModule,
    ScrollingModule,
    FormsModule,
    ReactiveFormsModule,
    ClipboardModule,
    NgxPermissionsModule,
    LightboxModule,
    NgxMaskModule.forRoot(),
  ]
})
export class PointsModule { }
