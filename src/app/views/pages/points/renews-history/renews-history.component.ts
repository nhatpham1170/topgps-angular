import { Component, OnInit, ElementRef, ChangeDetectorRef, ViewChild, EventEmitter } from '@angular/core';
import { finalize, takeUntil, tap, debounceTime, merge } from 'rxjs/operators';
import { Subject, observable } from 'rxjs';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { Device, DeviceTypeService } from '@core/admin';
import { Device, DeviceService, User, GeofenceModel, PointService, MovePointModel, AddPointModel, EditTransactionModel, ServerService, UserServerService } from '@core/manage';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { ToastService, UserDatePipe, ValidatorCustomService, UserDateAdvPipe } from '@core/_base/layout';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { DecimalPipe } from '@angular/common';
import { UserManageService } from '@core/manage/_service/user-manage.service';
import { CurrentUserService } from '@core/auth';
import { UserAutocompleteComponent } from '@app/views/partials/content/widgets/user-autocomplete/user-autocomplete.component';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { Lightbox } from 'ngx-lightbox';
import { TranslateService } from '@ngx-translate/core';
import { ExcelService } from '@core/utils';
import { XLSXModel } from '@core/utils/xlsx/excel.service';
import { RequestFileService } from '@core/common/_services/request-file.service';
import { RequestFile } from '@core/common/_models/request-file';
import { environment } from '@env/environment';

declare var $: any;
declare var ClipboardJS: any;
const TITLE_FORM_EDIT: string = "POINTS.MANAGE.GENERAL.EDIT_TRANSACTION";
const TITLE_FORM_MOVE_POINT: string = "COMMON.ACTIONS.MOVE";
const TITLE_FORM_CANCEL_TRANSACTION: string = "POINTS.MANAGE.GENERAL.CANCEL_TRANSACTION";
const TITLE_FORM_REQUEST_CANCEL_TRANSACTION: string = "POINTS.MANAGE.GENERAL.REQUEST_CANCEL_TRANSACTION";
const TITLE_FORM_HISTORY: string = "POINTS.MANAGE.GENERAL.DETAIL_TRANSACTION";
const MAX_RECORD_EXPORT:number = 5000;

@Component({
  selector: 'kt-renews-history',
  templateUrl: './renews-history.component.html',
  styleUrls: ['./renews-history.component.scss'],
  providers: [UserDateAdvPipe, DecimalPipe]
})

export class RenewsHistoryComponent implements OnInit {
  public dataTable: DataTable = new DataTable();
  public dataTableHistory: DataTable = new DataTable();
  public action: string;
  public currentReason: any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public formEdit: FormGroup;
  public formAdd: FormGroup;
  private tableConfig: any;
  private tableConfigHistory: any;
  private currentForm: FormGroup;
  public currentModel: GeofenceModel;
  public commandsForm: FormGroup;
  public formEditTransaction: FormGroup;
  public currentItemSelected: any;
  public isSearchAdvanced: boolean;

  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  private userIdSelecte: number;
  public currentUser: User;
  public userLogin: any;
  public dateTimeServer: string;
  // feature device  
  public showUserTree = true;
  public allowReOrder: boolean = false;
  public titlePopup: string;
  public geofenceEdit: GeofenceModel;
  public processAction: boolean;
  private billImg: any;

  public serversConfig: any = [];
  // permisison 
  public permissions: any = {
    sell: "device.action.sell"
  }
  public datePickerOptions: any = {
    size: 'md',
    select: 'from',
    ranges: 'from',
    optionDatePicker: {
      maxSpan: {
        days: 31
      }
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: true,
    selectRange: "LAST_7_DAYS",
  };
  public payAtEditOptions: any = { singleDatePicker: true, size: 'md', select: 'from', timePicker: false, format: 'date', autoSelect: false, btnClear: true, optionDatePicker: { autoPlay: true } };
  public listDevices: Array<Device> = [];
  private lastUserId: number = 0;
  private dataSearch: any = {};
  @ViewChild('datePicker', { static: true }) datePicker: DateRangePickerComponent;
  @ViewChild('payAtCpn', { static: false }) payAtCpn: DateRangePickerComponent;
  @ViewChild('userAutocomplete', { static: true }) userAutoComplete: UserAutocompleteComponent
  constructor(
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private toast: ToastService,
    private userTreeService: UserTreeService,
    private userService: UserManageService,
    private validatorCT: ValidatorCustomService,
    private currentUserService: CurrentUserService,
    private pointService: PointService,
    private userDateAdv: UserDateAdvPipe,
    private lightBox: Lightbox,
    private serverService: ServerService,
    // private userServerService: UserServerService,
    private translate: TranslateService,
    private xlsx: ExcelService,
    private requestFile: RequestFileService,
    private decimalPipe: DecimalPipe,
  ) {
    this.action = "";
    this.tableConfig = {
      pagination: [
        10, 20, 30, 50
      ],
      columns: [
        {
          title: '#',
          field: 'id',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '20px' },
          width: 20,
          class: '',
          translate: '#',
          autoHide: false,
        },
        {
          title: 'Username',
          field: 'username',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '110px', 'margin': '0px' },
          width: 110,
          class: '',
          translate: 'COMMON.COLUMN.USERNAME',
          autoHide: false,
        },
        {
          title: 'Points',
          field: 'point',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '80px' },
          width: 80,
          class: '',
          translate: 'COMMON.COLUMN.POINT',
          autoHide: true,
        },
        // {
        //   title: 'distributor',
        //   field: 'distributor',
        //   allowSort: false,
        //   isSort: false,
        //   dataSort: '',
        //   style: { 'width': '110px' },
        //   width: 110,
        //   class: '',
        //   translate: 'COMMON.COLUMN.DISTRIBUTOR',
        //   autoHide: false,
        // },
        // {
        //   title: 'server',
        //   field: 'server',
        //   allowSort: false,
        //   isSort: false,
        //   dataSort: '',
        //   style: { 'width': '110px' },
        //   width: 110,
        //   class: '',
        //   translate: 'COMMON.COLUMN.SERVER',
        //   autoHide: false,
        // },
        {
          title: 'Account device',
          field: 'accountDevice',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '110px' },
          width: 110,
          class: '',
          translate: 'COMMON.COLUMN.ACCOUNT_DEVICE',
          autoHide: false,
        },

        {
          title: 'imei',
          field: 'imei',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.IMEI',
          autoHide: true,
        },
        {
          title: 'Number plate',
          field: 'numberPlate',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px' },
          width: 100,
          class: '',
          translate: 'COMMON.COLUMN.NUMBER_PLATE',
          autoHide: true,
        },
        {
          title: 'Device name',
          field: 'deviceName',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.DEVICE_NAME',
          autoHide: true,
        },
        {
          title: 'Service Old',
          field: 'serviceOld',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '90px' },
          width: 90,
          class: '',
          translate: 'COMMON.COLUMN.SERVICE_EXPIRE_OLD',
          autoHide: true,
        },
        {
          title: 'Service new',
          field: 'serviceNew',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '90px' },
          width: 90,
          class: '',
          translate: 'COMMON.COLUMN.SERVICE_EXPIRE_NEW',
          autoHide: true,
        },
        
        // {
        //   title: 'Store name',
        //   field: 'storeName',
        //   allowSort: false,
        //   isSort: false,
        //   dataSort: '',
        //   style: { 'width': '100px' },
        //   width: 100,
        //   class: '',
        //   translate: 'COMMON.COLUMN.STORE_NAME',
        //   autoHide: true,
        // },
        // {
        //   title: 'Model name',
        //   field: 'deviceType',
        //   allowSort: false,
        //   isSort: false,
        //   dataSort: '',
        //   style: { 'width': '120px' },
        //   width: 120,
        //   class: '',
        //   translate: 'COMMON.COLUMN.DEVICE_TYPE',
        //   autoHide: true,
        // },
        {
          title: 'Pay',
          field: 'pay',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.PAY',
          autoHide: true,
        },
        {
          title: 'Created by',
          field: 'createdBy',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '104px' },
          width: 104,
          class: '',
          translate: 'COMMON.COLUMN.IMPLEMENTER',
          autoHide: false,
        },
        {
          title: 'Create At',
          field: 'createdAt',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.CREATED_DATE',
          autoHide: true,
        },
        {
          title: 'Description',
          field: 'description',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '150px', },
          width: 150,
          class: '',
          translate: 'COMMON.COLUMN.NOTE',
          autoHide: true,
        },
        {
          title: 'Amount',
          field: 'amount',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '110px', },
          width: 110,
          class: '',
          translate: 'COMMON.COLUMN.AMOUNT',
          autoHide: true,
        },
        {
          title: 'Actions',
          field: 'actions',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px', },
          width: 100,
          class: '',
          translate: 'COMMON.COLUMN.ACTIONS',
          autoHide: false,
        },
      ]
    };
    this.tableConfigHistory = {
      pagination: [
        10, 20, 30, 50
      ],
      columns: [
        {
          title: '#',
          field: 'id',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '20px' },
          width: 20,
          class: '',
          translate: '#',
          autoHide: false,
        },
        {
          title: 'Username',
          field: 'username',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '110px', 'margin': '0px' },
          width: 110,
          class: '',
          translate: 'COMMON.COLUMN.USERNAME',
          autoHide: false,
        },
        {
          title: 'Points',
          field: 'point',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '80px' },
          width: 80,
          class: '',
          translate: 'COMMON.COLUMN.POINT',
          autoHide: true,
        },
        // {
        //   title: 'distributor',
        //   field: 'distributor',
        //   allowSort: false,
        //   isSort: false,
        //   dataSort: '',
        //   style: { 'width': '110px' },
        //   width: 110,
        //   class: '',
        //   translate: 'COMMON.COLUMN.DISTRIBUTOR',
        //   autoHide: false,
        // },
        // {
        //   title: 'server',
        //   field: 'server',
        //   allowSort: false,
        //   isSort: false,
        //   dataSort: '',
        //   style: { 'width': '110px' },
        //   width: 110,
        //   class: '',
        //   translate: 'COMMON.COLUMN.SERVER',
        //   autoHide: false,
        // },
        {
          title: 'Account device',
          field: 'accountDevice',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '110px' },
          width: 110,
          class: '',
          translate: 'COMMON.COLUMN.ACCOUNT_DEVICE',
          autoHide: false,
        },

        {
          title: 'imei',
          field: 'imei',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.IMEI',
          autoHide: true,
        },
        {
          title: 'Number plate',
          field: 'numberPlate',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '100px' },
          width: 100,
          class: '',
          translate: 'COMMON.COLUMN.NUMBER_PLATE',
          autoHide: true,
        },
        {
          title: 'Device name',
          field: 'deviceName',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.DEVICE_NAME',
          autoHide: true,
        },
        {
          title: 'Service Old',
          field: 'serviceOld',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '90px' },
          width: 90,
          class: '',
          translate: 'COMMON.COLUMN.SERVICE_EXPIRE_OLD',
          autoHide: true,
        },
        {
          title: 'Service new',
          field: 'serviceNew',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '90px' },
          width: 90,
          class: '',
          translate: 'COMMON.COLUMN.SERVICE_EXPIRE_NEW',
          autoHide: true,
        },
        
        
        // {
        //   title: 'Store name',
        //   field: 'storeName',
        //   allowSort: false,
        //   isSort: false,
        //   dataSort: '',
        //   style: { 'width': '100px' },
        //   width: 100,
        //   class: '',
        //   translate: 'COMMON.COLUMN.STORE_NAME',
        //   autoHide: true,
        // },
        // {
        //   title: 'Model name',
        //   field: 'deviceType',
        //   allowSort: false,
        //   isSort: false,
        //   dataSort: '',
        //   style: { 'width': '120px' },
        //   width: 120,
        //   class: '',
        //   translate: 'COMMON.COLUMN.DEVICE_TYPE',
        //   autoHide: true,
        // },
        {
          title: 'Pay',
          field: 'pay',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.PAY',
          autoHide: true,
        },
        {
          title: 'Created by',
          field: 'createdBy',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '104px' },
          width: 104,
          class: '',
          translate: 'COMMON.COLUMN.IMPLEMENTER',
          autoHide: false,
        },
        {
          title: 'Create At',
          field: 'createdAt',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.CREATED_DATE',
          autoHide: true,
        },
        {
          title: 'Description',
          field: 'description',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '150px', },
          width: 150,
          class: '',
          translate: 'COMMON.COLUMN.NOTE',
          autoHide: true,
        },
        {
          title: 'Amount',
          field: 'amount',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '110px', },
          width: 110,
          class: '',
          translate: 'COMMON.COLUMN.AMOUNT',
          autoHide: true,
        },
        {
          title: 'Pay At',
          field: 'payAt',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.PAY_AT',
          autoHide: true,
        },
        {
          title: 'Bill Image',
          field: 'billImg',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.BILL_IMG',
          autoHide: true,
        },
        {
          title: 'Create At',
          field: 'updatedAt',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.UPDATED_DATE',
          autoHide: true,
        },
        {
          title: 'update by',
          field: 'updatedBy',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '104px' },
          width: 104,
          class: '',
          translate: 'COMMON.COLUMN.UPDATED_BY',
          autoHide: false,
        },
      ]
    };
    this.unsubscribe = new Subject();
    this.dataTable.init({
      data: [],
      totalRecod: 0,
      paginationSelect: [10, 20, 30, 50],
      columns: this.tableConfig.columns,
      formSearch: '#formSearch',
      pageSize: 10,
      // isDebug: true,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,

        },
        responsive: false
        // selecter: true,
      }
    });
    this.dataTableHistory.init({
      data: [],
      totalRecod: 0,
      paginationSelect: [10, 20, 30, 50],
      columns: this.tableConfigHistory.columns,
      formSearch: '#formSearch',
      pageSize: 1000,
      // isDebug: true,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,

        },
        responsive: false
      }
    });
    this.currentReason = {};
    this.parrentEmiter = new EventEmitter();
    this.currentUser = new User();
    this.dateTimeServer = "";
    this.isSearchAdvanced = false;
  }
  ngOnInit() {
    this.userLogin = this.currentUserService.currentUser;
    // listening dataTable event
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        // option.parentId = option.userId;
        // delete option.userId;
        this.getData(option);
      })
    ).subscribe();

    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();

    $(function () {
      $('select').selectpicker();
    });
  }

	/**
	 * Get list by data option
	 * @param option option search list
	 */
  private getData(option) {
    if (option['orderBy'] == undefined || option['orderBy'].length == 0) {
      option['orderBy'] = "createdAt";
      option['orderType'] = "desc";
    }
    this.pointService.extendHistory({ params: option }).pipe(
      tap((data: any) => {
        this.dateTimeServer = data.datetime;
        this.dataTable.update({
          data: data.result.content,
          totalRecod: data.result.totalRecord
        });
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
        this.cdr.detectChanges();
      })
    ).subscribe();
  }

  private createFormEditTransaction(item) {
    this.formEditTransaction = this.fb.group({
      transactionId: new FormControl({ value: item.id, disabled: true }),
      userSendUserName: new FormControl({ value: item.userName, disabled: true }),
      receiveUserName: new FormControl({ value: item.userNameObj, disabled: true }),
      transactionType: new FormControl({ value: item.type, disabled: true }),
      deviceIMEI: new FormControl({ value: item.deviceImei, disabled: true }),
      transactionPoint: new FormControl({ value: item.point, disabled: true }),
      transactionIsPay: new FormControl({ value: item.status, disabled: false }),      
      transactionDescription: new FormControl({ value: item.description || "", disabled: false }, []),
      transactionAmount: new FormControl({ value: item.amount || 0, disabled: false }, [this.validatorCT.number,this.validatorCT.min(0)]),
      payAt: new FormControl({ value: this.userDateAdv.transform(item.payAt, 'YYYY-MM-DD HH:mm:ss') || "", disabled: false }),
    });
    this.billImg = undefined;
    if (item.payAt) {
      this.payAtEditOptions['startDate'] = this.userDateAdv.transform(item.payAt, 'YYYY-MM-DD HH:mm:ss');
      this.payAtEditOptions['endDate'] = this.userDateAdv.transform(item.payAt, 'YYYY-MM-DD HH:mm:ss');
      this.payAtEditOptions['autoSelect'] = true;
    }
    else {
      this.payAtEditOptions['autoSelect'] = false;
    }
    setTimeout(() => {
      $("#formEditTransaction .selectpicker").selectpicker();
      $("#formEditTransaction #transactionIsPay").val(item.status).selectpicker('refresh');
      $("#formEditTransaction #transactionType").val(item.type).selectpicker('refresh');
    });

  }
  billImgChange(value) {
    this.billImg = value;
  }
  payAtEdit(value) {
    if (value.endDate) {
      this.formEditTransaction.controls['payAt'].setValue(value.endDate.format("YYYY-MM-DD HH:mm:ss"));
    }
    else {
      this.formEditTransaction.controls['payAt'].setValue("");
    }
  }
  get fEdit() {
    return this.formEditTransaction.controls;
  }

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.currentForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }


  search(data?: any) {
    let listSearch = [];

    if (this.userIdSelecte != undefined) {
      listSearch.push({ key: 'userId', value: this.userIdSelecte });
    }
    listSearch.push({ key: 'timeFrom', value: this.dataSearch['timeFrom'] });
    listSearch.push({ key: 'timeTo', value: this.dataSearch['timeTo'] });
    if (this.dataSearch['userObj']) {
      listSearch.push({ key: 'userIdObj', value: this.dataSearch['userObj']['item']['id'] });
    }
    else if (this.dataSearch['userName']) {
      listSearch.push({ key: 'userNameObj', value: this.dataSearch['userName'] });
    }
    this.dataTable.search(data || listSearch);
  }

  resetFormSearch() {
    $('#formSearch .selectpicker').val('default').selectpicker("refresh");
    // $('#formSearch .kt_datepicker').val("").datepicker("update");
    $("#formSearch :input").each(function () {
      switch (this.type) {
        case 'checkbox':
          $(this).prop('checked', false);
          break;
        default:
          $(this).val('');
          break;
      }
    });
    this.datePicker.refresh();
    this.userAutoComplete.clear();
    let listSearch = [];
    listSearch.push({ key: 'timeFrom', value: this.dataSearch['timeFrom'] });
    listSearch.push({ key: 'timeTo', value: this.dataSearch['timeTo'] });
    listSearch.push({ key: 'userId', value: this.userIdSelecte });
    listSearch.push({ key: 'orderBy', value: 'createdAt' });
    listSearch.push({ key: 'orderType', value: 'desc' });

    this.search(listSearch);
  }

  userTreeChangeUser(value) {

    if (value.id > 0) {
      this.userIdSelecte = value.id;
      this.currentUser = value;
      this.search();
    }
  }
  copied(val) {
    this.toast.copied(val);
  }

  // feature export excel file
  open(content, type, item?: any) {
    this.action = type;
    this.currentItemSelected = item;
    switch (type) {
      case 'history':
        this.historyFnc(content);
        break;
      case 'edit':
        this.editFnc(content, item);
        break;
      case 'cancel':
        this.cancelFnc(content);
        break;
      case 'request_cancel':
        this.requestCancelFnc(content);
        break;
    }
  }
  historyFnc(content) {
    this.titlePopup = TITLE_FORM_HISTORY;
    this.pointService.transactionLog({ params: { transactionId: this.currentItemSelected.id, pageNo: -1 } }).pipe(
      tap(data => {
        // let recordOriginal = {};
        // recordOriginal['objNew'] = this.currentItemSelected;
        if (data.result.length == 0)
          data.result.push({ objNew: this.currentItemSelected });
        else data.result.push({ objNew: data.result[data.result.length - 1].objOld });
        this.dataTableHistory.update({
          data: data.result,
          totalRecod: data.result.length
        });

        this.cdr.detectChanges();
      }),
      finalize(this.cdr.markForCheck)
    ).subscribe();

    // check and get listDevices
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable  kt-modal-xl', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }
  cancelFnc(content) {
    this.titlePopup = TITLE_FORM_CANCEL_TRANSACTION;
    // check and get listDevices
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable  kt-modal-sm', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }
  requestCancelFnc(content) {
    this.titlePopup = TITLE_FORM_REQUEST_CANCEL_TRANSACTION;
    // check and get listDevices
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable  kt-modal-sm', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }
  editFnc(content, item) {

    this.titlePopup = TITLE_FORM_EDIT;
    this.createFormEditTransaction(item);
    // check and get listDevices
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable kt-modal-sm', backdrop: 'static' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.currentReason = reason;
    });
  }

  onSubmit() {
    switch (this.action) {
      case "edit":
        this.editAction();
        break;
      case "cancel":
        this.cancelAction();
        break;
      case "request_cancel":
        this.requestCancelAction();
        break;
    }
  }

  editAction() {
    this.formEditTransaction.markAllAsTouched();
    if (this.formEditTransaction.valid) {
      if (this.currentItemSelected.type == "1") {
        this.toast.show({ type: 'error', translate: "POINTS.MANAGE.VALIDATE.TYPE_INVALID" });
      }
      else {
        this.processAction = true;
        let editTransactionModel = new EditTransactionModel();

        editTransactionModel.id = this.currentItemSelected.id;
        editTransactionModel.isPay = this.formEditTransaction.get('transactionIsPay').value;
        editTransactionModel.description = this.formEditTransaction.get('transactionDescription').value;
        editTransactionModel.payAt = this.formEditTransaction.get('payAt').value;
        editTransactionModel.amount = this.formEditTransaction.get('transactionAmount').value;
        if (this.billImg != undefined) editTransactionModel.billImg = this.billImg;

        this.pointService.edit(editTransactionModel, { notifyGlobal: true }).pipe(
          debounceTime(300),
          tap(result => {
            if (result.status === 200) {
              this.modalService.dismissAll(this.currentReason);
              this.dataTable.reload({});
            }
          },
            error => {
            }),
          finalize(() => {
            this.processAction = false;
            this.cdr.markForCheck();
            this.cdr.detectChanges();
          })
        )
          .subscribe();
      }
    }
  }

  cancelAction() {
    this.processAction = true;
    this.pointService.cancel(this.currentItemSelected.id, { notifyGlobal: true }).pipe(
      tap(result => {
        if (result.status == 200) {
          this.modalService.dismissAll(this.currentReason);
          this.parrentEmiter.emit({ id: this.currentUser.id, path: this.currentUser.path });
          this.dataTable.reload({});
        }
      }),
      finalize(() => {
        this.processAction = false;
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  requestCancelAction() {
    this.processAction = true;
    this.pointService.requestCancel(this.currentItemSelected.id, { notifyGlobal: true }).pipe(
      tap(result => {
        if (result.status == 200) {
          this.modalService.dismissAll(this.currentReason);
          this.parrentEmiter.emit({ id: this.currentUser.id, path: this.currentUser.path });
          this.dataTable.reload({});
        }
      }),
      finalize(() => {
        this.processAction = false;
        this.cdr.markForCheck();
      })
    ).subscribe();
  }

  /**
* Dismiss Reason Popup
* @param reason 
*/
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  onChangeUserSearch(data) {
    this.dataSearch["userName"] = data.text;
    this.dataSearch["userObj"] = data.obj;
  }
  dateRouteChange(data) {
    this.dataSearch["timeFrom"] = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dataSearch["timeTo"] = data.endDate.format("YYYY-MM-DD HH:mm:ss");
  }
  showImg(url) {
    const album = {
      src: url,
      caption: "",
      thumb: url
    };
    this.lightBox.open([album], 0);
  }
  toggleSearchAdvanced() {
    this.isSearchAdvanced = !this.isSearchAdvanced;
    if (this.serversConfig.length == 0) {
      // this.userServerService.list({ params: { userId: this.currentUserService.currentUser.id } }).pipe(
      //   tap(res => {
      //     this.serversConfig = res.result;
      //     setTimeout(() => {
      //       $("#svServers").val('default').selectpicker("refresh");
      //     });
      //   }),
      //   finalize(() => {
      //     this.cdr.markForCheck();
      //     this.cdr.detectChanges();
      //   })
      // ).subscribe();
    }

    setTimeout(() => {
      $('#formSearch .selectpicker').val('default').selectpicker("refresh");
    });

  }
  /**
  * Change size dataTabel
  * @param $elm 
  */
  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  exportXlSX(option?: { all: true }) {

    let data = [];
    let apiSearch = this.dataTable.getDataSearch();
    
    if (option && option.all === true) {

      apiSearch['pageNo'] = -1;
      if (this.dataTable.getPaginations()['total'] > MAX_RECORD_EXPORT) {
        let requestFile = new RequestFile();
        requestFile.type = "POINT__RENEWS_HISTORY";
        requestFile.fileExtention = "XLSX";
        requestFile.params = apiSearch;
        this.requestFile.create(requestFile, { notifyGlobal: true }).subscribe();
      }
      else {
        this.pointService.extendHistory({ params: apiSearch }).pipe(
          tap((data: any) => {
            let summary = {
              startTime: "",
              endTime: "",
              totalCount: 0,
              totalPoint: 0,
              totalAmount:0,
              countPaid: 0,
              pointPaid: 0,
              amountPaid: 0,
              countUnpaid: 0,
              pointUnpaid: 0,
              amountUnpaid: 0,
            };
            summary['startTime'] = this.userDateAdv.transform(apiSearch['timeFrom'], "datetime",{valueOffset:0});
            summary['endTime'] = this.userDateAdv.transform(apiSearch['timeTo'], "datetime", { valueOffset: 0 });
            summary.totalCount = data.result.length;
            let dataTemp = data.result.map(x => {
              summary.totalPoint += x.point;
              switch (x.status) {
                case 0:
                  x.statusStr = this.translate.instant('POINTS.PAY.UNKNOWN');
                  break;
                case 1:
                  x.statusStr = this.translate.instant('POINTS.PAY.PAID');
                  break;
                case 2:
                  x.statusStr = this.translate.instant('POINTS.PAY.UNPAID');
                  break;
                case 3:
                  x.statusStr = this.translate.instant('POINTS.PAY.CANCEL');
                  break;
                case 4:
                  x.statusStr = this.translate.instant('POINTS.PAY.REQUEST_CANCEL');
                  break;
              }
              if (x.status === 1) {
                summary.countPaid++;
                summary.amountPaid += x.amount;
                summary.pointPaid += x.point;
              }
              else {
                summary.countUnpaid++;
                summary.amountUnpaid += x.amount;
                summary.pointUnpaid += x.point;
              }
              summary.totalAmount += x.amount;
              return x;
            })
            this.exportFileXLSX(dataTemp, summary);
          }),
          takeUntil(this.unsubscribe),
          finalize(() => {
            this.cdr.markForCheck();
          })
        ).subscribe();
      }
    }
  }
  private exportFileXLSX(data, summary) {

    let config: XLSXModel = {
      file: {
        title: this.translate.instant('POINTS.MANAGE.GENERAL.RENEWAL_HISTORY'),
        prefixFileName: this.translate.instant('POINTS.MANAGE.GENERAL.RENEWAL_HISTORY')
      },
      header: [
        {
          text: this.translate.instant('POINTS.MANAGE.GENERAL.RENEWAL_HISTORY'),
          type: "header",
          style: {
            fill: {
              patternType: "solid", // none / solid
              fgColor: { rgb: "01B050" },
            },
          },
          hpx: 32
        },
        {
          text: "",
        },
        {
          text: "!timezone",
        },
        {
          text: ""
        },
        {
          text: [this.translate.instant('COMMON.EXCEL.START_TIME') + ": ",, summary['startTime']],
        },
        {
          text: [this.translate.instant('COMMON.EXCEL.END_TIME') + ": ",,summary['endTime']],
          // text: [,,],
        },
        {
          text: "",
        },
        {
          text: this.translate.instant('COMMON.EXCEL.TOTAL') ,
          style: {
            font: { bold: true },
          }
        },
        {
          text: [this.translate.instant('POINTS.MANAGE.GENERAL.COUNT_DEVICE_RENEWS') + ": ",
          ,this.decimalPipe.transform(summary['totalCount'])],
          
        },
        {
          text: [this.translate.instant('POINTS.MANAGE.GENERAL.POINT') + ": ",
          ,this.decimalPipe.transform(summary['totalPoint'])]
        
        },
        {
          text:[this.translate.instant('COMMON.EXCEL.AMOUNT') + ": ",
            , this.decimalPipe.transform(summary['totalAmount']) + " VND"]
        },
        {
          text: this.translate.instant('POINTS.PAY.PAID'),
          style: {
            font: { bold: true },
          }
        },
        {
          text: [this.translate.instant('POINTS.MANAGE.GENERAL.COUNT_DEVICE_RENEWS') + ": ",
          ,this.decimalPipe.transform(summary['countPaid'])],
          
        },
        {
          text: [this.translate.instant('POINTS.MANAGE.GENERAL.POINT') + ": ",
          ,this.decimalPipe.transform(summary['pointPaid'])],
         
        },
        {
          text: [this.translate.instant('COMMON.EXCEL.AMOUNT') + ": ",
            ,this.decimalPipe.transform(summary['amountPaid']) + " VND"],        
        },

        {
         
          text: this.translate.instant('POINTS.PAY.UNPAID'),
          style: {
            font: { bold: true },
          }
        },
        {
          text: [this.translate.instant('POINTS.MANAGE.GENERAL.COUNT_DEVICE_RENEWS') + ": ",
          ,this.decimalPipe.transform(summary['countUnpaid'])],
        },
        {
          text: [this.translate.instant('POINTS.MANAGE.GENERAL.POINT') + ": ",
            ,this.decimalPipe.transform(summary['pointUnpaid'])],
        
        },
        {
          text: [this.translate.instant('COMMON.EXCEL.AMOUNT') + ": ",
          ,this.decimalPipe.transform(summary['amountUnpaid']) + " VND"],
        },

        {
          text: ""
        },
      ],
      columns: [
        {
          name: "#",
          columnData: "auto",
          wch: 5,
        }, {
          name: this.translate.instant('COMMON.COLUMN.USERNAME'),
          columnData: "userName",
          wch: 20,
        },
        {
          name: this.translate.instant('COMMON.COLUMN.POINT'),
          columnData: "point",
          wch: 20,
          type: "number"
        },
        // {
        //   name: this.translate.instant('COMMON.COLUMN.DISTRIBUTOR'),
        //   columnData: "userNameObj",
        //   wch: 20,
        // },
        // {
        //   name: this.translate.instant('COMMON.COLUMN.SERVER'),
        //   columnData: "domainName",
        //   wch: 20,
        // },
        {
          name: this.translate.instant('COMMON.COLUMN.ACCOUNT_DEVICE'),
          columnData: "userNameObj",
          wch: 20,
        },
        {
          name: this.translate.instant('COMMON.COLUMN.IMEI'),
          columnData: "deviceImei",
          wch: 20,
        },
        {
          name: this.translate.instant('COMMON.COLUMN.NUMBER_PLATE'),
          columnData: "vehiclePlate",
          wch: 20,
        },
        {
          name: this.translate.instant('COMMON.COLUMN.DEVICE_NAME'),
          columnData: "deviceName",
          wch: 20,
        },
        // {
        //   name: this.translate.instant('COMMON.COLUMN.STORE_NAME'),
        //   columnData: "storeName",
        //   wch: 20
        // },
        // {
        //   name: this.translate.instant('COMMON.COLUMN.DEVICE_TYPE'),
        //   columnData: "deviceType",
        //   wch: 20
        // },
        {
          name: this.translate.instant('COMMON.COLUMN.SERVICE_EXPIRE_OLD'),
          columnData: "serviceExpireOld",
          wch: 25,
          type: "date"
        },
        {
          name: this.translate.instant('COMMON.COLUMN.SERVICE_EXPIRE_NEW'),
          columnData: "serviceExpireNew",
          wch: 25,
          type: "date"
        },
        
        {
          name: this.translate.instant('COMMON.COLUMN.PAY'),
          columnData: "statusStr",
          wch: 20
        },
        {
          name: this.translate.instant('COMMON.COLUMN.IMPLEMENTER'),
          columnData: "createdByName",
          wch: 15
        },
        {
          name: this.translate.instant('COMMON.COLUMN.CREATED_DATE'),
          columnData: "createdAt",
          wch: 20,
          type: "datetime",
        },
        {
          name: this.translate.instant('COMMON.COLUMN.NOTE'),
          columnData: "description",
          wch: 40,
        },
        {
          name: this.translate.instant('COMMON.COLUMN.AMOUNT'),
          columnData: "amount",
          wch: 15,
          type: "number"
        },
        {
          name: this.translate.instant('COMMON.COLUMN.PAY_AT'),
          columnData: "payAt",
          wch: 20,
          type: "date",
        },
      ],
      styleColumn: {
        fill: {
          patternType: "solid", // none / solid
          fgColor: { rgb: "92D050" },
        },
        font: { bold: true },
        // border: {
        //   top: { style: "thin", color: { "auto": 1 } },
        //   right: { style: "thin", color: { "auto": 1 } },
        //   bottom: { style: "thin", color: { "auto": 1 } },
        //   left: { style: "thin", color: { "auto": 1 } }
        // }
      },
      woorksheet: {
        name: this.translate.instant('POINTS.MANAGE.GENERAL.RENEWAL_HISTORY'),
      }
    }

    this.xlsx.exportFile(data, config, {});
  }
  onChangePayType(value){
    if(value == 1){
      this.payAtEditOptions.autoSelect = true;
      this.payAtCpn.refresh();
      this.formEditTransaction.get('payAt').setValidators(this.validatorCT.required);
      this.formEditTransaction.get('payAt').updateValueAndValidity();
      this.formEditTransaction.get('payAt').markAsTouched();
    }
    else{
      this.payAtEditOptions.autoSelect = false;
      this.payAtCpn.refresh();
      this.formEditTransaction.get('payAt').clearValidators();
      this.formEditTransaction.get('payAt').updateValueAndValidity();
      this.formEditTransaction.get('payAt').markAsTouched();
    }
  }
}


