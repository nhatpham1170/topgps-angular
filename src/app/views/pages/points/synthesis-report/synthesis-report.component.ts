import { Component, OnInit, ElementRef, ChangeDetectorRef, ViewChild, EventEmitter } from '@angular/core';
import { finalize, takeUntil, tap, debounceTime, merge } from 'rxjs/operators';
import { Subject, observable } from 'rxjs';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { Device, DeviceTypeService } from '@core/admin';
import { Device, DeviceService, User, GeofenceModel, PointService, MovePointModel, AddPointModel, EditTransactionModel, UserServerService } from '@core/manage';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { ToastService, UserDatePipe, ValidatorCustomService, UserDateAdvPipe } from '@core/_base/layout';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { DecimalPipe } from '@angular/common';
import { UserManageService } from '@core/manage/_service/user-manage.service';
import { CurrentUserService } from '@core/auth';
import { UserAutocompleteComponent } from '@app/views/partials/content/widgets/user-autocomplete/user-autocomplete.component';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { Lightbox } from 'ngx-lightbox';
import { TranslateService } from '@ngx-translate/core';
import { ExcelService, XLSXModel } from '@core/utils';
import { RequestFileService } from '@core/common/_services/request-file.service';
import { RequestFile } from '@core/common/_models/request-file';

declare var $: any;
declare var ClipboardJS: any;
const TITLE_FORM_EDIT: string = "POINTS.MANAGE.GENERAL.EDIT_TRANSACTION";
const TITLE_FORM_MOVE_POINT: string = "COMMON.ACTIONS.MOVE";
const TITLE_FORM_CANCEL_TRANSACTION: string = "POINTS.MANAGE.GENERAL.CANCEL_TRANSACTION";
const TITLE_FORM_HISTORY: string = "POINTS.MANAGE.GENERAL.DETAIL_TRANSACTION";
const TITLE_FORM_REQUEST_CANCEL_TRANSACTION: string = "POINTS.MANAGE.GENERAL.REQUEST_CANCEL_TRANSACTION";
const MAX_RECORD_EXPORT: number = 5000;

@Component({
  selector: 'kt-synthesis-report',
  templateUrl: './synthesis-report.component.html',
  styleUrls: ['./synthesis-report.component.scss'],
  providers: [UserDateAdvPipe, DecimalPipe]
})

export class SynthesisReportComponent implements OnInit {
  public dataTable: DataTable = new DataTable();
  public dataTableHistory: DataTable = new DataTable();
  public action: string;
  public currentReason: any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public formEdit: FormGroup;
  public formAdd: FormGroup;
  private tableConfig: any;
  private tableConfigHistory: any;
  private currentForm: FormGroup;
  public currentModel: GeofenceModel;
  public commandsForm: FormGroup;
  public formEditTransaction: FormGroup;
  public currentItemSelected: any;

  public parrentEmiter: EventEmitter<boolean | { id: number, path: string }>;
  private userIdSelecte: number;
  public currentUser: User;
  public userLogin: any;
  public dateTimeServer: string;
  // feature device  
  public showUserTree = true;
  public allowReOrder: boolean = false;
  public titlePopup: string;
  public geofenceEdit: GeofenceModel;
  public processAction: boolean;
  private billImg: any;
  // permisison 
  public permissions: any = {
    sell: "device.action.sell"
  }
  // [options]="{singleDatePicker:true,size: 'md',select: 'from',timePicker:false,format:'date',autoSelect:false,btnClear:true,optionDatePicker:{autoApply:true}}"
  public datePickerOptions: any = {
    size: 'md',
    select: 'from',
    ranges: 'from',
    optionDatePicker: {
      maxSpan: {
        days: 31
      },
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: false,
    selectRange: "LAST_7_DAYS",
  };
  public payAtEditOptions: any = { singleDatePicker: true, size: 'md', select: 'from', timePicker: false, format: 'date', autoSelect: false, btnClear: true, optionDatePicker: { autoApply: true } };
  public listDevices: Array<Device> = [];
  private lastUserId: number = 0;
  private dataSearch: any = {};
  private payAtElRef: ElementRef;
  @ViewChild('datePicker', { static: true }) datePicker: DateRangePickerComponent;
  // @ViewChild('payAt', { static: false }) payAt: DateRangePickerComponent;
  @ViewChild('payAt', { static: false }) set content(content: ElementRef) {
    this.payAtElRef = content;
  }
  // @ViewChild('payAt', { static: true }) payAt: DateRangePickerComponent;
  @ViewChild('userAutocomplete', { static: true }) userAutoComplete: UserAutocompleteComponent
  constructor(
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private toast: ToastService,
    private userTreeService: UserTreeService,
    private userService: UserManageService,
    private validatorCT: ValidatorCustomService,
    private currentUserService: CurrentUserService,
    private pointService: PointService,
    private userDateAdv: UserDateAdvPipe,
    private lightBox: Lightbox,
    private userServerService: UserServerService,
    private translate: TranslateService,
    private xlsx: ExcelService,
    private requestFile: RequestFileService,
    private decimalPipe: DecimalPipe,
  ) {
    this.action = "";
    this.tableConfig = {
      pagination: [
        10, 20, 30, 50
      ],
      columns: [
        {
          title: '#',
          field: 'id',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '20px' },
          width: 20,
          class: '',
          translate: '#',
          autoHide: false,
          headerShow:false,
        },
        {
          title: 'date',
          field: 'date',
          allowSort: true,
          isSort: false,
          dataSort: '',
          style: { 'width': '130px' },
          width: 130,
          class: '',
          translate: 'COMMON.COLUMN.DATE',
          autoHide: true,
          headerShow:false,
        },
        {
          title: 'total count',
          field: 'total_count',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '80px', 'margin': '0px' },
          width: 80,
          class: '',
          translate: 'COMMON.COLUMN.COUNT_TRANSACTION',
          autoHide: false,
        },
        {
          title: 'total point',
          field: 'total_point',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '80px' },
          width: 80,
          class: '',
          translate: 'COMMON.COLUMN.TOTAL_POINT',
          autoHide: true,
        },
        {
          title: 'total amount paid',
          field: 'total_amountPaid',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.AMOUNT_PAID',
          autoHide: true,
          unit:"VND",
        },
        {
          title: 'total amount',
          field: 'total_amount',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.TOTAL_AMOUNT',
          autoHide: true,
          unit:"VND",
        },
        {
          title: 'add count',
          field: 'add_count',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '80px', 'margin': '0px' },
          width: 80,
          class: '',
          translate: 'COMMON.COLUMN.COUNT_TRANSACTION',
          autoHide: false,

        },
        {
          title: 'add point',
          field: 'add_point',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '80px' },
          width: 80,
          class: '',
          translate: 'COMMON.COLUMN.POINT',
          autoHide: true,
        },
        {
          title: 'add amount paid',
          field: 'add_amountPaid',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.AMOUNT_PAID',
          autoHide: true,
          unit:"VND",
        },
        {
          title: 'add amount',
          field: 'add_amount',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.AMOUNT',
          autoHide: true,
          unit:"VND",
        },
        
        {
          title: 'move count',
          field: 'move_count',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '80px', 'margin': '0px' },
          width: 80,
          class: '',
          translate: 'COMMON.COLUMN.COUNT_TRANSACTION',
          autoHide: false,
        },
        {
          title: 'move point',
          field: 'move_point',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '80px' },
          width: 80,
          class: '',
          translate: 'COMMON.COLUMN.POINT',
          autoHide: true,
        },
        {
          title: 'move amount paid',
          field: 'move_amountPaid',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.AMOUNT_PAID',
          autoHide: true,
          unit:"VND",
        },
        {
          title: 'move amount',
          field: 'move_amount',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.AMOUNT',
          autoHide: true,
          unit:"VND",
        },
        {
          title: 'renews count',
          field: 'renews_count',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '80px', 'margin': '0px' },
          width: 80,
          class: '',
          translate: 'COMMON.COLUMN.COUNT_TRANSACTION',
          autoHide: false,
        },
        {
          title: 'renews point',
          field: 'renews_point',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '80px' },
          width: 80,
          class: '',
          translate: 'COMMON.COLUMN.POINT',
          autoHide: true,
        },
        {
          title: 'renews amount paid',
          field: 'renews_amountPaid',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.AMOUNT_PAID',
          autoHide: true,
          unit:"VND",
        },
        {
          title: 'renews amount',
          field: 'renews_amount',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '120px' },
          width: 120,
          class: '',
          translate: 'COMMON.COLUMN.AMOUNT',
          autoHide: true,
          unit:"VND",
        },
        
       
        {
          title: 'Actions',
          field: 'actions',
          allowSort: false,
          isSort: false,
          dataSort: '',
          style: { 'width': '60px','text-align':'center' },
          width: 60,
          class: '',
          translate: 'COMMON.COLUMN.ACTIONS',
          autoHide: false,
          headerShow:false,
        },
      ]
    };
   
    this.unsubscribe = new Subject();
    this.dataTable.init({
      data: [],
      totalRecod: 0,
      paginationSelect: [10, 20, 30, 50],
      columns: this.tableConfig.columns,
      formSearch: '#formSearch',
      pageSize: 10,
      // isDebug: true,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,

        },
        responsive: false
        // selecter: true,
      }
    });
    this.currentReason = {};
    this.parrentEmiter = new EventEmitter();
    this.currentUser = new User();
    this.dateTimeServer = "";
  }
  ngOnInit() {
    this.userLogin = this.currentUserService.currentUser;
    // listening dataTable event
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        // option.parentId = option.userId;
        // delete option.userId;
        this.getData(option);
      })
    ).subscribe();

    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();

    $(function () {
      $('select').selectpicker();
    });
  }

	/**
	 * Get list by data option
	 * @param option option search list
	 */
  private getData(option) {
    if (option['orderBy'] == undefined || option['orderBy'].length == 0) {
      option['orderBy'] = "createdAt";
      option['orderType'] = "desc";
    }
    
    this.pointService.synthesisReport({ params: option }).pipe(
      tap((data: any) => {
        this.dateTimeServer = data.datetime;
        data.result.content =  data.result.content.map((day:any) => {
          Object.assign(day,new SynthesisModel());
          day.transactions.map(x=>{
            switch (x.type) {
              case 0:
                day.move_count++;
                day.move_point += x.point;
                day.move_amount += x.amount;
                if (x.status === 1) {
                  day.move_amountPaid += x.amount;
                  day.total_amount += x.amount;
                }
                break;
              case 1:
                day.add_count++;
                day.add_point += x.point;
                day.add_amount += x.amount;
                if (x.status === 1) {
                  day.add_amountPaid += x.amount;
                  day.total_amount += x.amount;
                }
                break;
              case 2:
                day.renews_count++;
                day.renews_point += x.point;
                day.renews_amount += x.amount;
                if (x.status === 1) {
                  day.renews_amountPaid += x.amount;
                  day.total_amount += x.amount;
                }
                break;
            }
            day.total_amount += x.amount;
            day.total_count++;
            
          });
          return day;
        });
        
        this.dataTable.update({
          data: data.result.content,
          totalRecod: data.result.totalRecord
        });
      }),
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
        this.cdr.detectChanges();
      })
    ).subscribe();
  }

  get fEdit() {
    return this.formEditTransaction.controls;
  }

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.currentForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }


  search(data?: any) {
    let listSearch = [];

    if (this.userIdSelecte != undefined) {
      listSearch.push({ key: 'userId', value: this.userIdSelecte });
    }
    listSearch.push({ key: 'timeFrom', value: this.dataSearch['timeFrom'] });
    listSearch.push({ key: 'timeTo', value: this.dataSearch['timeTo'] });
    if (this.dataSearch['userObj']) {
      listSearch.push({ key: 'userIdObj', value: this.dataSearch['userObj']['item']['id'] });
    }
    else if (this.dataSearch['userName']) {
      listSearch.push({ key: 'userNameObj', value: this.dataSearch['userName'] });
    }
    this.dataTable.search(data || listSearch);
  }

  resetFormSearch() {
    $('#formSearch .selectpicker').val('default').selectpicker("refresh");
    // $('#formSearch .kt_datepicker').val("").datepicker("update");
    $("#formSearch :input").each(function () {
      switch (this.type) {
        case 'checkbox':
          $(this).prop('checked', false);
          break;
        default:
          $(this).val('');
          break;
      }
    });
    this.datePicker.refresh();
    this.userAutoComplete.clear();
    let listSearch = [];
    listSearch.push({ key: 'timeFrom', value: this.dataSearch['timeFrom'] });
    listSearch.push({ key: 'timeTo', value: this.dataSearch['timeTo'] });
    listSearch.push({ key: 'userId', value: this.userIdSelecte });
    listSearch.push({ key: 'orderBy', value: 'createdAt' });
    listSearch.push({ key: 'orderType', value: 'desc' });

    this.search(listSearch);
  }

  userTreeChangeUser(value) {

    if (value.id > 0) {
      this.userIdSelecte = value.id;
      this.currentUser = value;
      this.search();
    }
  }
  copied(val) {
    this.toast.copied(val);
  }

  /**
  * Dismiss Reason Popup
  * @param reason 
  */
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  onChangeUserSearch(data) {
    this.dataSearch["userName"] = data.text;
    this.dataSearch["userObj"] = data.obj;
  }
  dateRouteChange(data) {
    this.dataSearch["timeFrom"] = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dataSearch["timeTo"] = data.endDate.format("YYYY-MM-DD HH:mm:ss");
  }
  showImg(url) {
    const album = {
      src: url,
      caption: "",
      thumb: url
    };
    this.lightBox.open([album], 0);
  }

  /**
  * Change size dataTabel
  * @param $elm 
  */
  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }
  exportXlSX(option?: { all: true }) {

    let data = [];
    let apiSearch = this.dataTable.getDataSearch();

    if (option && option.all === true) {

      apiSearch['pageNo'] = -1;
      if (this.dataTable.getPaginations()['total'] > MAX_RECORD_EXPORT) {
        let requestFile = new RequestFile();
        requestFile.type = "POINT__RENEWS_HISTORY";
        requestFile.fileExtention = "XLSX";
        requestFile.params = apiSearch;
        this.requestFile.create(requestFile, { notifyGlobal: true }).subscribe();
      }
      else {
        this.pointService.transactionHistory({ params: apiSearch }).pipe(
          tap((data: any) => {
            let summary = {
              startTime: "",
              endTime: "",
              userName: "",
              typeAddCount: 0,
              typeAddPoint: 0,
              typeAddAmount: 0,
              typeMoveCount: 0,
              typeMovePoint: 0,
              typeMoveAmount: 0,
              typeRenewsCount: 0,
              typeRenewsPoint: 0,
              typeRenewsAmount: 0,
              typeRenewsTotalCount: 0,
              typeRenewsTotalPoint: 0,
              typeRenewsTotalAmount: 0,
              subAccount: false,

            };
            summary['startTime'] = this.userDateAdv.transform(apiSearch['timeFrom'], "datetime", { valueOffset: 0 });
            summary['endTime'] = this.userDateAdv.transform(apiSearch['timeTo'], "datetime", { valueOffset: 0 });
            summary.userName = this.currentUser.username;
            summary.subAccount = apiSearch['subAccount'];
            // summary.totalCount = data.result.length;
            // filter data, remove add and move sub user.
            data.result = data.result.filter(x => {
              return !(x.userId != this.currentUser.id && x.type != 2);
            });

            let dataTemp = data.result.map(x => {
              // summary.totalPoint += x.point;
              switch (x.status) {
                case 0:
                  x.statusStr = this.translate.instant('POINTS.PAY.UNKNOWN');
                  break;
                case 1:
                  x.statusStr = this.translate.instant('POINTS.PAY.PAID');
                  break;
                case 2:
                  x.statusStr = this.translate.instant('POINTS.PAY.UNPAID');
                  break;
                case 3:
                  x.statusStr = this.translate.instant('POINTS.PAY.CANCEL');
                  break;
                case 4:
                  x.statusStr = this.translate.instant('POINTS.PAY.REQUEST_CANCEL');
                  break;
              }
              switch (x.type) {
                case 0:
                  // move
                  summary.typeMoveCount++;
                  summary.typeMoveAmount += x.amount;
                  summary.typeMovePoint += x.point;
                  x.typeStr = this.translate.instant('POINTS.TYPE.MOVE');
                  break;
                case 1:
                  // add
                  summary.typeAddCount++;
                  summary.typeAddAmount += x.amount;
                  summary.typeAddPoint += x.point;
                  x.typeStr = this.translate.instant('POINTS.TYPE.ADD');
                  break;
                case 2:
                  // renews
                  if (x.userId == this.currentUser.id) {
                    summary.typeRenewsCount++;
                    summary.typeRenewsAmount += x.amount;
                    summary.typeRenewsPoint += x.point;
                  }
                  summary.typeRenewsTotalCount++;
                  summary.typeRenewsTotalAmount += x.amount;
                  summary.typeRenewsTotalPoint += x.point;
                  x.typeStr = this.translate.instant('POINTS.TYPE.RENEWS');
                  break;
              }

              return x;
            })
            this.exportFileXLSX(dataTemp, summary);
          }),
          takeUntil(this.unsubscribe),
          finalize(() => {
            this.cdr.markForCheck();
          })
        ).subscribe();
      }
    }
  }
  private exportFileXLSX(data, summary) {

    let config: XLSXModel = {
      file: {
        title: this.translate.instant('POINTS.MANAGE.GENERAL.TRANSACTION_HISTORY'),
        prefixFileName: this.translate.instant('POINTS.MANAGE.GENERAL.TRANSACTION_HISTORY')
      },
      header: [
        {
          text: this.translate.instant('POINTS.MANAGE.GENERAL.TRANSACTION_HISTORY'),
          type: "header",
          style: {
            fill: {
              patternType: "solid", // none / solid
              fgColor: { rgb: "01B050" },
            },
          },
        },
        {
          text: "",
        },
        {
          text: "!timezone",
        },
        {
          text: ""
        },
        {
          text: [this.translate.instant('COMMON.EXCEL.USER_NAME') + ": ", , summary['userName']],
        },
        {
          text: [this.translate.instant('COMMON.EXCEL.START_TIME') + ": ", , summary['startTime']],
        },
        {
          text: [this.translate.instant('COMMON.EXCEL.END_TIME') + ": ", , summary['endTime']],
        },
        {
          text: "",
        },

        // add point
        {
          text: this.translate.instant('POINTS.MANAGE.GENERAL.ADD_POINTS'),
          style: {
            font: { bold: true },
          }
        },
        {
          text: [this.translate.instant('POINTS.MANAGE.GENERAL.COUNT_TRANSACTION') + ": ",
            , this.decimalPipe.transform(summary['typeAddCount'])],

        },
        {
          text: [this.translate.instant('POINTS.MANAGE.GENERAL.POINT') + ": ",
            , this.decimalPipe.transform(summary['typeAddPoint'])],

        },
        {
          text: [this.translate.instant('COMMON.EXCEL.AMOUNT') + ": ",
            , this.decimalPipe.transform(summary['typeAddAmount']) + " VND"],
        },
        // move point
        {
          text: this.translate.instant('POINTS.MANAGE.GENERAL.MOVE_POINTS'),
          style: {
            font: { bold: true },
          }
        },
        {
          text: [this.translate.instant('POINTS.MANAGE.GENERAL.COUNT_TRANSACTION') + ": ",
            , this.decimalPipe.transform(summary['typeMoveCount'])],

        },
        {
          text: [this.translate.instant('POINTS.MANAGE.GENERAL.POINT') + ": ",
            , this.decimalPipe.transform(summary['typeMovePoint'])],

        },
        {
          text: [this.translate.instant('COMMON.EXCEL.AMOUNT') + ": ",
            , this.decimalPipe.transform(summary['typeMoveAmount']) + " VND"],
        },
        // renews
        {
          text: this.translate.instant('POINTS.MANAGE.GENERAL.RENEWS'),
          style: {
            font: { bold: true },
          }
        },
        {
          text: [this.translate.instant('POINTS.MANAGE.GENERAL.COUNT_DEVICE_RENEWS') + ": ",
            , !summary['subAccount'] ? this.decimalPipe.transform(summary['typeRenewsCount']) :
            this.decimalPipe.transform(summary['typeRenewsCount']) + " / " + this.decimalPipe.transform(summary['typeRenewsTotalCount'])],

        },
        {
          text: [this.translate.instant('POINTS.MANAGE.GENERAL.POINT') + ": ",
            , !summary['subAccount'] ? this.decimalPipe.transform(summary['typeRenewsPoint']) :
            this.decimalPipe.transform(summary['typeRenewsPoint']) + " / " + this.decimalPipe.transform(summary['typeRenewsTotalPoint'])],

        },
        {
          text: [this.translate.instant('COMMON.EXCEL.AMOUNT') + ": ",
            , !summary['subAccount'] ? this.decimalPipe.transform(summary['typeRenewsAmount']) :
            this.decimalPipe.transform(summary['typeRenewsAmount']) + " VND" + " / " + this.decimalPipe.transform(summary['typeRenewsTotalAmount']) + " VND"],

        },


        {
          text: ""
        },
      ],
      columns: [
        {
          name: "#",
          columnData: "auto",
          wch: 5,
        }, {
          name: this.translate.instant('COMMON.COLUMN.USERNAME'),
          columnData: "userName",
          wch: 20,
        },
        {
          name: this.translate.instant('COMMON.COLUMN.TYPE'),
          columnData: "typeStr",
          wch: 20,
        },
        {
          name: this.translate.instant('COMMON.COLUMN.POINT'),
          columnData: "point",
          wch: 20,
          type: "number"
        },
        {
          name: this.translate.instant('COMMON.COLUMN.ACCOUNT_TRANSACTION'),
          columnData: "userNameObj",
          wch: 20,
        },
        {
          name: this.translate.instant('COMMON.COLUMN.PAY'),
          columnData: "statusStr",
          wch: 20
        },
        {
          name: this.translate.instant('COMMON.COLUMN.IMPLEMENTER'),
          columnData: "createdByName",
          wch: 15
        },
        {
          name: this.translate.instant('COMMON.COLUMN.CREATED_DATE'),
          columnData: "createdAt",
          wch: 20,
          type: "datetime",
        },
        {
          name: this.translate.instant('COMMON.COLUMN.NOTE'),
          columnData: "description",
          wch: 40,
        },
        {
          name: this.translate.instant('COMMON.COLUMN.AMOUNT'),
          columnData: "amount",
          wch: 15,
          type: "number"
        },
        {
          name: this.translate.instant('COMMON.COLUMN.PAY_AT'),
          columnData: "payAt",
          wch: 20,
          type: "date",

        },
      ],
      styleColumn: {
        fill: {
          patternType: "solid", // none / solid
          fgColor: { rgb: "92D050" },
        },
        font: { bold: true },
      },
      woorksheet: {
        name: this.translate.instant('POINTS.MANAGE.GENERAL.TRANSACTION_HISTORY'),
      }
    }

    this.xlsx.exportFile(data, config, {});
  }
}
export class SynthesisModel {
  add_count: number;
  add_point: number;
  add_amount: number;
  add_amountPaid: number;
  move_count: number;
  move_point: number;
  move_amount: number;
  move_amountPaid: number;
  renews_count: number;
  renews_point: number;
  renews_amount: number;
  renews_amountPaid: number;
  total_amountPaid: number;
  total_count: number;
  total_point: number;
  total_amount: number;
  date: string;
  constructor() {
    this.add_count = 0;
    this.add_point = 0;
    this.add_amount = 0;
    this.add_amountPaid = 0;
    this.move_count = 0;
    this.move_point = 0;
    this.move_amount = 0;
    this.move_amountPaid = 0;
    this.renews_count = 0;
    this.renews_point = 0;
    this.renews_amount = 0;
    this.renews_amountPaid = 0;
    this.total_amount = 0;
    this.total_amountPaid = 0;
    this.total_count = 0;
    this.total_point = 0;
  }
}

