import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef,EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup ,FormArray,FormControl} from "@angular/forms";
import { finalize, takeUntil, tap ,debounceTime} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { DeviceCommandService } from '@core/manage';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { DeviceService} from '@core/manage';
import { UserTreeService } from '@core/common/_service/user-tree.service';
import { select, Store } from '@ngrx/store';
import { AppState } from '@core/reducers';
import { currentUser } from '@core/auth';
import { TranslateService } from '@ngx-translate/core';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { ToastService, ValidatorCustomService, UserDatePipe } from '@core/_base/layout';
import { DatePipe } from '@angular/common';
import { DecimalPipe } from '@angular/common';

declare var $: any;
@Component({
  selector: 'kt-fuel-chart',
  templateUrl: './fuel-chart.component.html',
  styleUrls: ['./fuel-chart.component.scss'],
  providers: [DatePipe, UserDatePipe, DecimalPipe]

})
export class FuelChartComponent implements OnInit {
  @ViewChild('datePicker', { static: true }) 

  datePicker: DateRangePickerComponent;

  searchFormChart : FormGroup;
  selectdp: ElementRef;
  public datePickerOptions: any = {
    size: 'md',
    select: 'from',
    ranges: 'from',
    optionDatePicker: {
      maxSpan: {
        days: 31
      }
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: true,

  };
  public dateStartChart:string;
  public dateEndChart:string;
  public data: any = [];

  public dataEdit: any;
  public filter :any;
  private unsubscribe: Subject<any>;
  public closeResult: string;
  public listDeviced : any = [];
  public listSensors: any = [];
  public isShow:boolean = false;
  public listDevices : any = [];
  public userIdSelected:number;
  public showUserTree:boolean = true;
  public paramsEmiter: EventEmitter<any>;
  public isDownload: EventEmitter<any>;
  public isdownLoadPDF:boolean = false;
  public deviceName:string = '';
  public showData:boolean = true;
  public parentBread:any;

  constructor(
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal,
    private deviceService: DeviceService,
    private userTreeService: UserTreeService,
    private store: Store<AppState>,
    //  private card:CardUtilService,
    private toast: ToastService,
    private translate: TranslateService,

  ) {
    $(function () {
      $('.kt_selectpicker').selectpicker();
    });
    this.paramsEmiter = new EventEmitter();
    this.isDownload = new EventEmitter();

    this.unsubscribe = new Subject();
    this.refreshEventType();
    this.buildForm();
    this.parentBread = [
      {
        name: this.translate.instant('REPORT.REPORTS.GENERAL.REPORTS'),
        translate: "REPORT.REPORTS.GENERAL.REPORTS",
        link: "/reports",
        icon: "flaticon2-graph"
      }
    ]
  }

  ngOnInit() {
   // this.renderDatePickerChart();
   this.userTreeService.event.pipe(
    tap(action => {
      switch (action) {
        case "close":
          this.showUserTree = false;
          break;
        case "open":
          this.showUserTree = true;
          break;
      }
    })
  ).subscribe();
   }

  //  renderDatePickerChart()
  //  {
  //    this.datePickerOptionsSearchChart = {
  //      format:'date',
  //      size: 'md',
  //      select: 'from',
  //      ranges: 'from',
  //      optionDatePicker: {
  //        maxSpan: {
  //          days: 31
  //        }
  //      },
  //      autoSelect: true,
  //      singleDatePicker: false,
  //      timePicker: true,
   
  //    }
  //  }

   private refreshEventType()
   {
      this.translate.onLangChange.subscribe((event) => {
      let device = this.translate.instant('REPORT.ROUTE.GENERAL.CHOISE_DEVICE');
      let sensor = this.translate.instant('COMMON.GENERAL.ALL');
      
      $(function () {
        $('#deviceChart').selectpicker({title: device}).selectpicker('refresh');
        $('#sensorChart').selectpicker({title: sensor}).selectpicker('refresh');

      });
    });
   }

   dateSelectChange(data) {
    this.dateStartChart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEndChart = data.endDate.format("YYYY-MM-DD HH:mm:ss");
   }

   private buildForm(): void {
    this.searchFormChart = this.formBuilder.group({
      deviceIdSearhChart: [''],
      sensorName: [''],
    });
  }

  onChangeDevice(event,deviceName)
  {
    let deviceId = event.target.value;
    this.deviceName = deviceName;

    // this.searchFormChart.value.sensorName = '';
    this.getListSenSorsByDevice(deviceId);
    setTimeout(() => {
      $('#sensorChart').selectpicker('val', ['-1']);
      $('#sensorChart').selectpicker('refresh');
    });   
  }

  showUserTreeFunction(){
    let userModel = {
      type: 0,
    };
    let user$:any = this.store.pipe(select(currentUser));
    if (user$) {
      select(user$.subscribe(function (data) {
        Object.assign(userModel, data);
      }
      ).unsubscribe());
    }
      // code display use-tree
      let type = userModel.type;
      this.showUserTree = false;
      if(type == 0 || type == 1) this.showUserTree = true;
  }

  ChangeUserTree(value) {    
      if (value.id > 0) {
        this.userIdSelected = value.id;
        this.getDevices(value.id);  
      }
      this.listSensors = [];
      this.resetFormSearch();
      setTimeout(() => {
        $('#sensorChart').selectpicker('refresh');
      });  
    }

    getListSenSorsByDevice(deviceId)
    {
     let obj = this.listDevices.find(x => x.id == deviceId);
     if(obj.sensors) this.listSensors = obj.sensors;
    }

    exportPDF(){
      // this.isdownLoadPDF = false;
      this.isDownload.emit({download:true});
    }

    searchChart(form:any)
    {
      let listSensor:any = [];
      if (form.value.deviceIdSearhChart == '' || form.value.deviceIdSearhChart == undefined || form.value.deviceIdSearhChart == null) {
        this.toast.show(
          {
            message: this.translate.instant('MESSEAGE_CODE.SENSOR.DEVICE_NOT_FOUND'),
            type: 'error',
          })
        return false;
      }
      if (form.value.sensorName == '' || form.value.sensorName == undefined || form.value.sensorName == null) {
         this.listSensors.map(sensor => {
          listSensor.push(sensor.id);
          return sensor;
        });
      }else {
        listSensor = form.value.sensorName;
      }
 
      this.dateStartChart = this.dateStartChart;
      this.dateEndChart = this.dateEndChart;
      let parms = {
        dateStartChart:this.dateStartChart,
        dateEndChart:this.dateEndChart,
        sensorIdSearhChart:listSensor.toString(),
        deviceIdSearhChart:form.value.deviceIdSearhChart,
        deviceName:this.deviceName,

        isShow : true
      };
      this.isdownLoadPDF = true;

      this.paramsEmiter.emit(parms);
    }
  

    getDevices(userId) {
      let params = {
        userId: userId, 
        pageNo: -1
      };
      // this.filter.pageNo = -1;
      this.deviceService.list({ params: params }).pipe(
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      ).subscribe((data: any) => {
        data.result = data.result.filter(device => {
          if(device.sensors.length > 0 )
          {
            let foundSensor = false;
            device.sensors = device.sensors.filter(sensor => {
              if(JSON.parse(sensor.parameters).typeSensor == 'fuel')
              {
                foundSensor = true;
                return sensor;
              }
            })
           if(foundSensor)    return device;
          
          }
        });
        this.listDevices = data.result;
        setTimeout( () => {
          $('#deviceChart').val(null).selectpicker('refresh');
        });
      });

    }
    
  open(content) {
    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-holder'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  
  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
      orderBy:'createdAt',
      orderType:'DESC'
    };
  }


  copied(val) {
    this.toast.copied(val);
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  resetFormSearch()
  {
    this.datePicker.refresh();
    this.searchFormChart.reset();
    $('.kt_selectpicker').selectpicker("refresh");
  } 

}




