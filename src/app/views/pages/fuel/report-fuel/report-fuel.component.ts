import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef,EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
// import { ReportFuelService } from '@core/report';
import { DeviceService } from '@core/manage';
import { TranslateService } from '@ngx-translate/core';
import { ToastService, UserDatePipe,UserDateAdvPipe } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { ExcelService,PdfmakeService,ExcelTestService} from '@core/utils';
import { XLSXModel, XLSXModelTest } from '@core/utils/xlsx/excel.service';
import { PdfModel } from '@core/utils/pdfmake/pdfmake.service';

import { DatePipe } from '@angular/common';
import { UserDateService } from '@core/_base/layout';
import { DecimalPipe } from '@angular/common';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { ReportFuelService } from '@core/report/_services/report-fuel.service';
import { UserTreeService } from '@core/common/_service/user-tree.service';

// end chart
declare var $: any;

@Component({
  selector: 'report-fuel',
  templateUrl: './report-fuel.component.html',
  styleUrls: ['./report-fuel.component.scss'],
  providers: [DatePipe, UserDatePipe, DecimalPipe,UserDateAdvPipe]

})
export class ReportFuelComponent implements OnInit {
  @ViewChild('datePicker', { static: true }) 

  datePicker: DateRangePickerComponent;
  @ViewChild('datePickerSearchChart', { static: true }) 

  datePickerSearchChart: DateRangePickerComponent;
  searchFormChart:FormGroup;
  searchFormFuel: FormGroup;
  selectdp: ElementRef;
  public datePickerOptions: any;
  public datePickerOptionsSearchChart:any;

  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public dataExport: any = null;
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataDefault: any = [{}];
  public filter: any;
  private unsubscribe: Subject<any>;
  public listDevices: any = [];
  public listSensors: any = [];
  public userIdSelected: number;
  public userIdSelectedEdit: number;
  public showUserTree: boolean = false;
  public dateStart: string;
  public dateEnd: string;
  public timeStart: string;
  public timeEnd: string;
  //time search chart
  public dateStartChart:string;
  public dateEndChart:string;
  public sensorIdSearhChart:any;
  public paramsEmiter: EventEmitter<any>;
  public deviceId: string;
  public deviceIdSearhChart:any;
  public deviceName: string;
  public dateFormat: string;
  public timeFormat: string;
  public timeZone:string;
  public totalAllRowData : any;
  public parentBread:any;
  public isdownLoad : boolean = false;
  public isdownLoadPDF:boolean = false;
  public closeResult: string;
  // export
  public configExport:PdfModel;
 // configChart
  constructor(
    private userTreeService: UserTreeService,

    private Fuel: ReportFuelService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private deviceService: DeviceService,
    private translate: TranslateService,
    private toast: ToastService,
    private currentUser: CurrentUserService,
    private xlsx: ExcelService,
    public datepipe: DatePipe,
    private userDate: UserDateService,
    private userDatePipe: UserDatePipe,
    private pdf : PdfmakeService,
    private modalService: NgbModal,
    private userDateAdv : UserDateAdvPipe
  ) {
    $(function () {
      $('select').selectpicker();
    });
    this.paramsEmiter = new EventEmitter();

    this.dateFormat = this.currentUser.dateFormatPipe;
    this.timeFormat = this.currentUser.timeFormat;
    this.timeZone = this.currentUser.currentUser.timezone;
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.parentBread = [
      {
        name: this.translate.instant('REPORT.REPORTS.GENERAL.REPORTS'),
        translate: "REPORT.REPORTS.GENERAL.REPORTS",
        link: "/reports",
        icon: "flaticon2-graph"
      }
    ]
  }

  ngOnInit() {

    this.updateDataTable();
    this.refreshEventType();
    this.datePickerOptions = this.objOption();
    // console.log(this.userDatePipe.transform('1574380755', "dateTime", null, "datetime"));
    let temp:any =  1574380755;
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();
  }

  objOption()
  {
    let obj = {
      size: 'md',
      select: 'from',
      ranges: 'from',
      optionDatePicker: {
        maxSpan: {
          days: 31
        }

      },
      autoSelect: true,
      singleDatePicker: false,
      timePicker: true,
    }; 
    return obj;
  }

  renderConfig()
  {
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    this.configExport = {
      info: {
        title: 'ReportFuel_export'
      },
      content:{
        header:{
          title:this.translate.instant('MENU.REPORT_FUEL'),
          params:{
            // deviceName:": "+this.deviceName,
            timeStart:": "+this.dateStart,
            timeEnd:": "+this.dateEnd,
            timeZone:": "+this.timeZone
          }
        },
        table:[
          {
              woorksheet: {
                name: this.deviceName,
              },
              total : {
                group : [
                  { columnData:'deviceName'},
                ],
                totalCol:[
                    { text:this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL'),style:{colSpan:8,alignment:"right",bold:true,fillColor:"#fff"}},
                    { text:''},
                    { text:''},
                    { text:''},
                    { text:''},
                    { text:''},
                    { text:''},
                    { text:''},
                    { columnData:'totalAddFuel',style:{alignment:"right",bold:true,fillColor:"#fff"}},
                    { columnData:'totalRemoveFuel',style:{alignment:"right",bold:true,fillColor:"#fff"}},

                ]
              },
              headerRows: 2,
              widths: ['4%','8%','8%','20%','8%','8%','20%','8%','8%','8%'],
              header :[           
                  [ 
                    { text: '#',rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},
                    { text: this.translate.instant('COMMON.COLUMN.SENSORS'),rowSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,10,0,0]},

                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_START'),colSpan:3,alignment:"center",fillColor:"#EEEEEE",bold:true},
                    { text:''},
                    { text:''},

                    { text: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_END'),colSpan:3,alignment:"center",fillColor:"#EEEEEE",bold:true},
                    { text:''},
                    { text:''},

                    { text: this.translate.instant('REPORT.FUEL.COLUMN.FUEL_CHANGE'),colSpan:2,alignment:"center",fillColor:"#EEEEEE",bold:true},
                    '',
                  
                  ],
                  [
                    '',
                    '',
                    { text: this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('COMMON.COLUMN.ADDRESS'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.FUEL.GENERAL.REPORT_FUEL'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},

                    { text: this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('COMMON.COLUMN.ADDRESS'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},  
                    { text: this.translate.instant('REPORT.FUEL.GENERAL.REPORT_FUEL'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},

                    { text: this.translate.instant('REPORT.FUEL.GENERAL.ADD'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.FUEL.GENERAL.REMOVE'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
      
                  ]
              ],
              body:[
                {auto:true},
                {columnData:'sensorNameExport',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'timeFromUtc',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'address',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'startVolume',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'timeEndUtc',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'address',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'endVolume',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'fuelAdd',style:{alignment:"right",fillColor:"#fff",bold:false}},
                {columnData:'fuelRemove',style:{alignment:"right",fillColor:"#fff",bold:false}},

              ],              
          },
          
        ]
          },
      footer:{
        title: this.userDateAdv.transform(this.dateStart,'YYYY-MM-DD HH:mm:ss')+ ' - ' + this.userDateAdv.transform(this.dateEnd,'YYYY-MM-DD HH:mm:ss')
      }    
    }
  }

  buildDateNow() {
    this.datePicker.refresh();
    this.deviceId = '';
    // setTimeout( () => {
    //   $('select').selectpicker('refresh');
    // });
  }

  private buildForm(): void {
    this.searchFormFuel = this.formBuilder.group({
      dateStart: [''],
      dateEnd: [''],
      deviceId: ['']
    });
    this.searchFormChart = this.formBuilder.group({
      deviceIdSearhChart: ['',],
      sensorName:['']
    });
    
  }

  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
    this.renderConfig();
    // console.log(this.dateStart);
   }


  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
    };
  }

  ChangeUserTree(value) {
    if (value.id > 0) {
      this.userIdSelectedEdit = value.id;
      this.getDevices(value.id);
      this.buildDateNow();
      this.dataTable.update({
        data: [],
        totalRecod: 0
      });
    }
  }

  getNameDevice(from) {
    this.deviceName = from.value.deviceId.toString();
  }

  getDevices(userId) {
    let params = {
      userId: userId, 
      pageNo: -1
    };
    // this.filter.pageNo = -1;
    this.deviceService.list({ params: params }).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      data.result = data.result.filter(device => {
        if(device.sensors.length > 0 )
        {
          
          let foundSensor = false;
          device.sensors = device.sensors.filter(sensor => {
            if(JSON.parse(sensor.parameters).typeSensor == 'fuel')
            {
              foundSensor = true;
              return sensor;
            }
          })
         if(foundSensor)    return device;
        
        }
      });
      this.listDevices = data.result;
      // console.log(this.listDevices);
      setTimeout( () => {
        $('.kt_selectpicker-device').selectpicker('refresh');
      });
    });

  }
  

  private refreshEventType()
  {
   this.translate.onLangChange.subscribe((event) => {
     let titleDevice = this.translate.instant('REPORT.ROUTE.GENERAL.CHOISE_DEVICE');
     $(function () {
       $('.input-device').selectpicker({title: titleDevice}).selectpicker('refresh');
     });
   });
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  validateForm(deviceId: any) {
    if (deviceId == '' || deviceId == undefined || deviceId == null) {
      this.toast.show(
        {
          message: this.translate.instant('MESSEAGE_CODE.SENSOR.DEVICE_NOT_FOUND'),
          type: 'error',
        })
      return false;
    }
    return true;
  }

  searchChart(form:any)
  {

    let listSensor:any = [];
    if (form.value.deviceIdSearhChart == '' || form.value.deviceIdSearhChart == null) {
      this.toast.show(
        {
          message: this.translate.instant('MESSEAGE_CODE.SENSOR.DEVICE_NOT_FOUND'),
          type: 'error',
        })
      return false;
    }
    if (form.value.sensorName == -1 || form.value.sensorName == null) {
      this.listSensors.map(sensor => {
       listSensor.push(sensor.id);
       return sensor;
     });
   }else {
     listSensor = form.value.sensorName;
   }

    this.dateStartChart = this.dateStartChart;
    this.dateEndChart = this.dateEndChart;
    this.sensorIdSearhChart =  listSensor.toString();
    this.deviceIdSearhChart = form.value.deviceIdSearhChart;
    let parms = {
      dateStartChart:this.dateStartChart,
      dateEndChart:this.dateEndChart,
      sensorIdSearhChart:this.sensorIdSearhChart,
      deviceIdSearhChart:this.deviceIdSearhChart
    };
    this.paramsEmiter.emit(parms);
  }

  searchFuel(form: any) {
    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;

    // this.filter.deviceId = form.value.deviceId;
    let listDevices:any = [];
  
    if(form.value.deviceId == '')
    {
      this.listDevices.forEach(device => {
        listDevices.push(device.id);
      });
    }else
    {
      listDevices =  form.value.deviceId;
    }
    if (listDevices.length == 0) {
      this.toast.show(
        {
          message: this.translate.instant('MESSEAGE_CODE.SENSOR.DEVICE_NOT_FOUND'),
          type: 'error',
        })
      return false;
    }
    this.filter.deviceIds = listDevices.toString();
    // this.getData(this.filter);
    this.dataTable.reload({ currentPage: 1 });

  }


  resetFormSearch() {
    this.buildDateNow();
  }

  resetFormSearchChart(){
    console.log("Chán vl");
      $('#deviceChart').val('').selectpicker("refresh");
      $('#sensorChart').val(-1).selectpicker("refresh");
      this.searchFormChart.reset();
    // this.datePickerSearchChart.refresh();
  
  }

  async getData(filter,type?:string) {
    if(type == 'pdf') this.isdownLoadPDF =true;
    if(type == 'excel') this.isdownLoad = true;

    let _this = this;

  await  this.Fuel.list(filter)
      .pipe(
        debounceTime(1000),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.cdr.markForCheck();
        })
      )
      .subscribe((result: any) => {
        if (result.status == 200) {
          if(type == 'pdf') _this.isdownLoadPDF = false;
          if(type == 'excel') _this.isdownLoad = false;

          let pageNo = this.filter.pageNo - 1;
          let pageSize = this.filter.pageSize;
       
          let j = 0;
          result.result = result.result.filter(device => {
            let dataFuelChange = [];
            let totalAddFuel = 0;
            let totalRemoveFuel = 0;

            if(type && device.data.length == 0) return []; 

            if (device.data.length > 0 && device.data) {
                device.data = device.data.map(sensor => {
                sensor.fuelChanges.map(item => {

                  item.timeFromUtc = this.userDateAdv.transform(item.startTime,'YYYY-MM-DD HH:mm:ss');
                  item.timeEndUtc = this.userDateAdv.transform(item.endTime,'YYYY-MM-DD HH:mm:ss');
                  item.totalOffset =  Math.round(item.totalOffset * 100) / 100 ;
                  if(item.add) 
                  {
                    totalAddFuel = totalAddFuel+item.totalOffset;
                    item.fuelAdd = item.totalOffset;
                    item.fuelRemove = 0;
                  }

                  if(!item.add) 
                  {
                    totalRemoveFuel = totalRemoveFuel+item.totalOffset;
                    item.fuelAdd = 0;
                    item.fuelRemove = item.totalOffset;

                  }                
                  item.sensorName = sensor.name;
                  item.sensorVolume = sensor.max+" ("+sensor.unit+")";
                  item.sensorNameExport =  sensor.name+" ("+item.sensorVolume+")";
                  item.sensorId = sensor.id;
                  item.deviceId = device.deviceId;
                  j++;
                  return item;
                });  
                dataFuelChange = dataFuelChange.concat(sensor.fuelChanges)
                let count = pageNo * pageSize + j;
                sensor.count = count;
                return sensor;
              });
              device.dataFuelChange = dataFuelChange;
              device.data = dataFuelChange;
              device.totalAddFuel = Math.round(totalAddFuel * 100) / 100;
              device.totalRemoveFuel = Math.round(totalRemoveFuel * 100) / 100 ;
              return device;
            }
          });
          this.totalRecod = j;
          this.data = result.result;
          if(!type)
          {
            this.dataTable.update({
              data: this.data,
              totalRecod: this.totalRecod
            });
          }
         
          if(type == 'pdf')     this.exportFilePDF(this.data);
          if(type == 'excel')    this.exportFileXLSX(this.data);

          // setTimeout( () => {
          //   $('select').selectpicker('refresh');
          // });

        }
      })
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;
        this.getData(this.filter);
      }),

    ).subscribe();
  }

  convertNumberToTime(number) {
    return this.userDate.convertNumberToTime(number);
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
        responsive: false,
      }
    });
  }

  dateSelectChangeSearchChart(data)
  {
    this.dateStartChart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEndChart = data.endDate.format("YYYY-MM-DD HH:mm:ss");
  }

  durationTime(time,option)
  {
    let timeReturn = '';
    if(option == "start") time = time - 3600;
    if(option == "end") time = time + 3600;
    timeReturn = this.userDateAdv.transform(time.toString(),"YYYY-MM-DD HH:mm:ss");
    return timeReturn;
  }

  getListSenSorsByDevice(deviceId)
  {
   let obj = this.listDevices.find(x => x.id == deviceId);
   if(obj.sensors) this.listSensors = obj.sensors;
  }

  onChangeDevice(event)
  {    
    let deviceId = event.target.value;
    this.getListSenSorsByDevice(deviceId);

    setTimeout(() =>{
      $('#sensorChart').selectpicker('val', ['-1']);
      $('#sensorChart').selectpicker('refresh');
    });   
  }

  renderDatePickerChart(dateStartChart,endDate)
  {
    this.datePickerOptionsSearchChart = {
      size: 'md',
      select: 'from',
      ranges: 'from',
      optionDatePicker: {
        maxSpan: {
          days: 31
        }

      },
      autoSelect: true,
      singleDatePicker: false,
      timePicker: true,
      startDate:dateStartChart,
      endDate:endDate
    }
  }

  showChart(content,item)
  {   
    this.paramsEmiter = new EventEmitter();
    let startTime = item.startTime - 3600;
    let endTime   = item.endTime + 3600;
    this.getListSenSorsByDevice(item.deviceId);
    this.dateStartChart = this.userDateAdv.transform(startTime,'YYYY-MM-DD HH:mm:ss');
    this.dateEndChart = this.userDateAdv.transform(endTime,'YYYY-MM-DD HH:mm:ss');
    this.renderDatePickerChart(this.dateStartChart,this.dateEndChart);
    this.sensorIdSearhChart =  item.sensorId;
    this.deviceIdSearhChart = item.deviceId;
    let parms = {
      dateStartChart:this.dateStartChart,
      dateEndChart:this.dateEndChart,
      sensorIdSearhChart:this.sensorIdSearhChart,
      deviceIdSearhChart:item.deviceId.toString()
    };
    
    setTimeout(()=>{
      $('#deviceChart').val(item.deviceId).selectpicker("refresh");
      $('#sensorChart').val(item.sensorId).selectpicker("refresh");
      this.searchFormChart.value.deviceIdSearhChart = item.deviceId;
      this.searchFormChart.value.sensorName = item.sensorId;

      this.paramsEmiter.emit(parms);
    });  
  
    this.open(content);
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  open(content) {

    this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-1100',backdrop:'static'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  roundNumber(number) {
    return Math.round(number * 100) / 100;
  }

  async getdataExcel(file) {
    if(file =='pdf')
    {this.isdownLoadPDF = true;}else
    {this.isdownLoad = true; } 
    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;

    let listDevices:any = [];

    // console.log(this.deviceName);
    if(this.deviceName == '' ||this.deviceName == undefined )
    {
      this.listDevices.forEach(device => {
        listDevices.push(device.id);
      });
      this.filter.deviceIds = listDevices.toString();
    }else{
      this.filter.deviceIds = this.deviceName;
    }
    this.getData(this.filter,file);

  }

  //pdf
  async exportPDF(){
    this.getdataExcel('pdf')

  }

  exportFilePDF(data){
    this.pdf.ExportPdf([this.data],this.configExport);
  }
  
  //export
  exportXlSX() {
      this.getdataExcel('excel');
  }
  private exportFileXLSX(data) {
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let config: XLSXModelTest = {
      file: {
        title: this.translate.instant('MENU.REPORT_FUEL'),
        prefixFileName: "ReportFuelChange"
      },
      template: [{
        header: [
          {
            text: this.translate.instant('MENU.REPORT_FUEL'),
            type: "header",
          },
          {
            text: ""
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START') + ": " + this.dateStart,
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_END') + ": " + this.dateEnd,
          },
          {
            text: this.translate.instant('MANAGE.USER.PARAMETER.TIMEZONE') + ": " + this.timeZone,
          },
          
          {
            text: ""
          },
        ],
        columns: [
          {
            name: "#",
            columnData: "auto",
            wch: 5,
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            merge:{
              rowSpan:1
            }
          },
          {
            name:this.translate.instant('COMMON.COLUMN.SENSORS'),
            columnData: "sensorNameExport",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            merge:{
              rowSpan:1
            },
            wch:20,
          },
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_START'),
            columnData: "timeFromUtc",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
            merge:{
              colSpan:2
            },
          },
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_START'),
            columnData: "address",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 40,
          },
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_START'),
            columnData: "startVolume",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 10,
          },
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_END'),
            columnData: "timeEndUtc",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
            merge:{
              colSpan:2
            },
          },
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_START'),
            columnData: "address",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 40,
          },
          {
            name: this.translate.instant('REPORT.ROUTE.COLUMN.MOVE_START'),
            columnData: "endVolume",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 10,
          },
          {
            name: this.translate.instant('REPORT.FUEL.COLUMN.FUEL_CHANGE'),
            columnData: "fuelAdd",
            style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },
            wch: 10,
            merge:{
              colSpan:1
            },
          },
          {
            name: this.translate.instant('REPORT.FUEL.COLUMN.FUEL_CHANGE'),
            columnData: "fuelRemove",
            style: { alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true } },
            wch: 10
          },
        ],
        columnsMerge:
        {
          data: [
            "",
            "",
            this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),
            this.translate.instant('COMMON.COLUMN.ADDRESS'),
            this.translate.instant('REPORT.FUEL.GENERAL.REPORT_FUEL'),
            this.translate.instant('MANAGE.ALERT_RULE.NOTIFICATION.TIME'),
            this.translate.instant('COMMON.COLUMN.ADDRESS'),
            this.translate.instant('REPORT.FUEL.GENERAL.REPORT_FUEL')
            ,this.translate.instant('REPORT.FUEL.GENERAL.ADD'), 
            this.translate.instant('REPORT.FUEL.GENERAL.REMOVE')],
          // position: ["A8:A9","B8:B9","C8:E8","F8:H8","I8:J8"]
        },
        woorksheet: {
          name: this.deviceName,
        },
        total: {
          group: "deviceName",
          totalCol: {
            field: [
              {
                text: this.translate.instant('REPORT.ROUTE.COLUMN.TOTAL'),
                merge: {
                  full: false,
                  range: "7"
                },
                wch:5
              },
              {
               text: " "
              },
              {
                text: " "
               },
               {
                text: " "
               },
               {
                text: " "
               },
               {
                text: " "
               },
               {
                text: " "
               },
               {
                text: " "
               },
              {
                columnData: "totalAddFuel",
                style: {
                  font: { name: 'Times New Roman', sz: 14, bold: true },
                  alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                }
              },
              {
                columnData: "totalRemoveFuel",
                style: {
                  font: { name: 'Times New Roman', sz: 14, bold: true },
                  alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                }
              }

            ]
          }

        }
      }]

    };
    this.xlsx.exportFileTest([this.data], config, {});
  }
  setColumns() {
    this.columns = [
      // {
      //   title: '#',
      //   field: 'no',
      //   allowSort: false,
      //   isSort: false,
      //   dataSort: '',
      //   style: { 'width': '20px' },
      //   class: 't-datatable__cell--center',
      //   translate: '#',
      //   autoHide: false,
      //   width: 20,
      // },
      // {
      //   title: 'Sensor name',
      //   field: 'moveStart',
      //   allowSort: false,
      //   isSort: false,
      //   dataSort: '',
      //   style: { 'width': '100px' },
      //   class: '',
      //   translate: 'COMMON.COLUMN.SENSORS',
      //   autoHide: false,
      //   width: 100,
      // },
      {
        title: 'Time',
        field: 'moveStart',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'MANAGE.ALERT_RULE.NOTIFICATION.TIME',
        autoHide: false,
        width: 200,
      },
      {
        title: 'address',
        field: 'fuel_start',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '250px' },
        class: '',
        translate: 'COMMON.COLUMN.ADDRESS',
        autoHide: false,
        width: 200,
      },
      {
        title: 'Fuel',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.FUEL.GENERAL.REPORT_FUEL',
        autoHide: false,
        width: 200,
      },
      {
        title: 'Time',
        field: 'moveStart',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        class: '',
        translate: 'MANAGE.ALERT_RULE.NOTIFICATION.TIME',
        autoHide: false,
        width: 200,
      },
      {
        title: 'address',
        field: 'fuel_start',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '250px' },
        class: '',
        translate: 'COMMON.COLUMN.ADDRESS',
        autoHide: false,
        width: 200,
      },
      {
        title: 'Fuel',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.FUEL.GENERAL.REPORT_FUEL',
        autoHide: false,
        width: 200,
      },
      // {
      //   title: 'Fuel add',
      //   field: 'fuel_end',
      //   allowSort: false,
      //   isSort: false,
      //   dataSort: '',
      //   style: { 'width': '50px' },
      //   class: '',
      //   translate: 'REPORT.FUEL.GENERAL.ADD',
      //   autoHide: false,
      //   width: 200,
      // },
      // {
      //   title: 'Fuel remove ',
      //   field: 'fuel_change',
      //   allowSort: false,
      //   isSort: false,
      //   dataSort: '',
      //   style: { 'width': '50px' },
      //   class: '',
      //   translate: 'REPORT.FUEL.GENERAL.REMOVE',
      //   autoHide: false,
      //   width: 150,
      // },

      
   
      // {
      //   title: 'Fuel graph',
      //   field: 'fuel_graph',
      //   allowSort: false,
      //   isSort: false,
      //   dataSort: '',
      //   style: { 'width': '150px','text-align':'center' },
      //   class: '',
      //   translate: 'REPORT.FUEL.COLUMN.FUEL_GRAPH',
      //   autoHide: false,
      //   width: 150,
      // },

    ]
  }


}




