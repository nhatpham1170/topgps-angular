import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PartialsModule } from '@app/views/partials/partials.module';
import { RouterModule, Routes } from '@angular/router';
import { NgbDropdownModule, NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ClipboardModule } from 'ngx-clipboard';
import { NgxPermissionsModule } from 'ngx-permissions';
import { SharedModule } from '@app/core/common';

//card
import { FuelChartComponent } from './fuel-chart/fuel-chart.component';
import { ReportFuelComponent } from './report-fuel/report-fuel.component';
import { FuelSummaryComponent } from './fuel-summary/fuel-summary.component';

import { ModuleGuard, AuthGuard } from '@core/auth';
import { CoreModule } from '@core/core.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';// Angular Material

import {DragDropModule} from '@angular/cdk/drag-drop';
import { FuelComponent } from './fuel.component';

const routes: Routes = [{
  path: '',
  component:FuelComponent,
  children: [
    {
      path: 'chart',
      component: FuelChartComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data: {
        permisison: 'ROLE_fuel.chart',
      },
    },
    {
      path: 'changes',
      component: ReportFuelComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data: {
        permisison: 'ROLE_report.fuel',
      },
    },
    {
      path: 'summary',
      component: FuelSummaryComponent,
      canActivate: [ModuleGuard, AuthGuard],
      data: {
        permisison: 'ROLE_fuel.summary',
      },
    },
  ]
}]

@NgModule({

  imports: [
    CoreModule,
    CommonModule,
    RouterModule.forChild(routes),
    PartialsModule,
    NgbDropdownModule,
    NgbModalModule,
    NgbModule,
    ClipboardModule,
    NgxPermissionsModule,
    NgSelectModule,
    AutocompleteLibModule,
    FormsModule,
    ReactiveFormsModule,
    DragDropModule,
    SharedModule
  ],
  providers: [ModuleGuard],
  declarations: [
    FuelComponent,
    FuelChartComponent,
    FuelSummaryComponent,
    ReportFuelComponent
    ]
})
export class FuelModule { }
