import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef,EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { finalize, takeUntil, tap, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTable } from '@core/_base/layout/models/datatable.model';
import { ReportRouteService } from '@core/report';
import { DeviceService } from '@core/manage';
import { TranslateService } from '@ngx-translate/core';
import { ToastService, ValidatorCustomService, UserDatePipe } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { ExcelService,PdfmakeService,ExcelTestService} from '@core/utils';
import { XLSXModel, XLSXModelTest } from '@core/utils/xlsx/excel.service';
import { PdfModel } from '@core/utils/pdfmake/pdfmake.service';

import { DatePipe } from '@angular/common';
import { UserDateService ,UserDateAdvPipe} from '@core/_base/layout';
import { DecimalPipe } from '@angular/common';
import { DateRangePickerComponent } from '@app/views/partials/content/general/datepicker/date-range-picker/date-range-picker.component';
import * as moment from 'moment';
import { ReportFuelService } from '@core/report/_services/report-fuel.service';
import { ExportService } from '@core/utils';
import { UserTreeService } from '@core/common/_service/user-tree.service';

declare var $: any;
@Component({
  selector: 'fuel-summary',
  templateUrl: './fuel-summary.component.html',
  styleUrls: ['./fuel-summary.component.scss'],
  providers: [DatePipe, UserDatePipe, DecimalPipe,UserDateAdvPipe]

})
export class FuelSummaryComponent implements OnInit {
  @ViewChild('listDevicePicker', { static: true }) 
  listDevicePicker:any;
  @ViewChild('datePicker', { static: true }) 
  datePicker: DateRangePickerComponent;
  formRoute: FormGroup;
  searchFormFuel: FormGroup;
  formCheckList: FormGroup;
  selectdp: ElementRef;
  public datePickerOptions: any = {
    size: 'md',
    select: 'from',
    ranges: 'from',
    optionDatePicker: {
      maxSpan: {
        days: 31
      }
    },
    autoSelect: true,
    singleDatePicker: false,
    timePicker: true,
    startDate:moment().subtract(1, "days").startOf('day'),
    endDate:moment().subtract(1, "days").endOf('day'),
  };
  public dataTable: DataTable = new DataTable();
  public columns: any = [];
  public data: any = [];
  public dataExport: any = [];
  public totalRecod: number = 0;
  public paginationSelect: any = [];
  public dataDefault: any = [{}];
  public filter: any;
  private unsubscribe: Subject<any>;
  public listDevices: any = [];
  public userIdSelected: number;
  public userIdSelectedEdit: number;
  public showUserTree: boolean = false;
  public dateStart: string;
  public dateEnd: string;
  public timeStart: string;
  public timeEnd: string;
  public deviceId: string;
  public deviceName: string;
  public dateFormat: string;
  public timeFormat: string;
  public timeZone:string;
  public totalAllRowData : any;
  public parentBread:any;
  public isdownLoad : boolean = false;
  public isdownLoadPDF:boolean = false;
  public paramsEmiter: EventEmitter<any>;
  public listDeviced: any = [];
  public dataPagination : any = [];
  public keySearchFilter:string = 'name';
  public showFullData:boolean = true;

  constructor(
    private userTreeService: UserTreeService,
    private Fuel: ReportFuelService,
    private Route: ReportRouteService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private deviceService: DeviceService,
    private translate: TranslateService,
    private toast: ToastService,
    private currentUser: CurrentUserService,
    private xlsx: ExcelService,
    public datepipe: DatePipe,
    private userDate: UserDateService,
    private userDatePipe: UserDatePipe,
    private pdf : PdfmakeService,
    private userDateAdv : UserDateAdvPipe,
    private exportService:ExportService

  ) {
    this.paramsEmiter = new EventEmitter();
    this.dateFormat = this.currentUser.dateFormatPipe;
    this.timeFormat = this.currentUser.timeFormat;
    this.timeZone = this.currentUser.currentUser.timezone;
    this.unsubscribe = new Subject();
    this.buildForm();
    this.setColumns();
    this.buildPagination();
    this.setPaginationSelect();
    this.setDataTable();
    this.parentBread = [
      {
        name: this.translate.instant('REPORT.REPORTS.GENERAL.REPORTS'),
        translate: "REPORT.REPORTS.GENERAL.REPORTS",
        link: "/reports",
        icon: "flaticon2-graph"
      }
    ]
  }

  ngOnInit() {
 
    this.updateDataTable();
    this.userTreeService.event.pipe(
      tap(action => {
        switch (action) {
          case "close":
            this.showUserTree = false;
            break;
          case "open":
            this.showUserTree = true;
            break;
        }
      })
    ).subscribe();
  }

  buildDateNow() {
    this.datePicker.refresh();
    this.deviceId = '';
    setTimeout(function () {
      $('select').selectpicker('refresh');
    }, 100);
  }

  private buildForm(): void {

    this.searchFormFuel = this.formBuilder.group({
      dateStart: [''],
      dateEnd: [''],
      deviceId: ['']
    });
  }

  dateSelectChange(data) {
    this.dateStart = data.startDate.format("YYYY-MM-DD HH:mm:ss");
    this.dateEnd = data.endDate.format("YYYY-MM-DD HH:mm:ss");
   }


  buildPagination() {
    this.filter = {
      pageNo: 1,
      pageSize: 10,
    };
  }

  ChangeUserTree(value) {
    if (value.id > 0) {
      this.userIdSelectedEdit = value.id;
      this.getDevices(value.id);
      this.listDevicePicker.reset();
      // let arrValue = [393];
      // this.listDevicePicker.setValue(arrValue);
      // this.deviceId = '';
      this.buildDateNow();
      this.dataTable.update({
        data: [],
        totalRecod: 0
      });
    }
  }

  getNameDevice(text: string) {
    this.deviceName = text;
  }

  getDevices(userId) {
    let params = {
      userId: userId, 
      pageNo: -1
    };
    // this.filter.pageNo = -1;
    this.deviceService.list({ params: params }).pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    ).subscribe((data: any) => {
      data.result = data.result.filter(device => {
        if(device.sensors.length > 0 )
        {
          let foundSensor = false;
          device.sensors = device.sensors.filter(sensor => {
            if(JSON.parse(sensor.parameters).typeSensor == 'fuel')
            {
              foundSensor = true;
              return sensor;
            }
          })
         if(foundSensor)    return device;
        
        }
      });
      this.listDevices = data.result;
      this.paramsEmiter.emit(this.listDevices);

      // console.log(this.listDevices);
      // setTimeout( () => {
      //   $('select').selectpicker('refresh');
      // });
    });

  }

  changeChecked(event){
    this.listDeviced = event.listChecked;
  }

  getListFilter(){

    let listDevices: any = [];    
    if(this.listDeviced.length == 0)
    {
      this.listDevices.forEach(device => {
        this.listDeviced.push(device.id);
      });
    }
    if(this.listDeviced.length == 0) 
    {
       this.toast.show({message:this.translate.instant('REPORT.FUEL.MESSAGE.NOT_DEVICE'),type:'error'});
       return false;
    } 
    this.filter.pageNo = 1;
    this.filter.timeFrom = this.dateStart;
    this.filter.timeTo = this.dateEnd;

    // this.filter.deviceIds = 130,
    this.filter.deviceIds = this.listDeviced.toString();
    return true;
  }

  setPaginationSelect() {
    this.paginationSelect = [10, 20, 30, 40, 50];
  }

  validateForm(deviceId: string) {
    let timeStart = new Date(this.dateStart).getTime();
    let timeEnd = new Date(this.dateEnd).getTime();
    if (timeStart > timeEnd) {
      this.toast.show(
        {
          message: this.translate.instant('REPORT.ROUTE.MESSAGE.ERROR_TIME'),
          type: 'error',
        })
      return false;
    }
    if (deviceId == '' || deviceId == undefined || deviceId == null) {
      this.toast.show(
        {
          message: this.translate.instant('MESSEAGE_CODE.SENSOR.DEVICE_NOT_FOUND'),
          type: 'error',
        })
      return false;
    }
    return true;
  }

  searchRoute(form: any) {
    this.showFullData = true;
    if(this.getListFilter())    this.dataTable.reload({ currentPage: 1 });


  }

  resetFormSearch() {
    this.listDevicePicker.reset();
    this.buildDateNow();
  }

  async getData(filter,type?:string) {
    if(type == 'pdf') this.isdownLoadPDF =true;
    if(type == 'excel') this.isdownLoad = true;
    let _this = this;
    await  this.Fuel.summary(filter)
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.cdr.markForCheck();
      })
    )
    .subscribe((result: any) => {
      let j = 1;
      if(type == 'pdf') _this.isdownLoadPDF = false;
      if(type == 'excel') _this.isdownLoad = false;
      let pageNo = this.filter.pageNo - 1;
      let pageSize = this.filter.pageSize;
      let test=[];
      result.result.filter(item=>{
        
        item.deviceName = item.name;
        item.fuelByDays.filter(fuel=>{
        
             let count = pageNo * pageSize + j;
              fuel.timeFromUtc =  fuel.date;
              fuel.engine = this.convertNumberToTime(fuel.totalEngineOnTime);
              fuel.distance = this.roundNumber(fuel.distance);
              fuel.fuel = fuel.fuel.map(itemFuel => {
                itemFuel.nameSensor = itemFuel.name;
                itemFuel.name = itemFuel.name+" (lit)";
                itemFuel.add = this.roundNumber(itemFuel.add);
                itemFuel.remove = this.roundNumber(itemFuel.remove);
                itemFuel.consume = this.roundNumber(itemFuel.consume);
                itemFuel.consume = Math.abs(itemFuel.consume);
                // item excel
                fuel.name = itemFuel.name;
                fuel.add = itemFuel.add;
                fuel.remove = itemFuel.remove;
                fuel.consume = itemFuel.consume;
                fuel.start = itemFuel.start;
                fuel.end = itemFuel.end;
                // end item excel
                //total
                return  itemFuel;
              });
              if(fuel.fuel.length == 0) fuel.fuel.push({nametext:''});
              fuel.count = count;
              test.push(fuel);
         
         j++;
          return fuel;
        });
        item.data = item.fuelByDays;
        return item;
      })

      this.dataPagination = result.result;
      this.getDataShowPage();
      
      if(!type)
      {
        this.dataTable.update({
          data: this.data,
          totalRecod: j-1
        });
      }
      if(type == 'pdf')     this.exportFilePDF(this.data);
      if(type == 'excel')    this.exportFileXLSX(this.data);
      this.showFullData = false;

    });

  }

  getDataShowPage()
  {
    this.data = [];
    for(let i=0;i<this.dataPagination.length;i++)
    {
    
        this.data.push({
          deviceName:this.dataPagination[i].name,
          data:[]
        }); 
        let data = this.dataPagination[i].data;
        for(let j=0;j<data.length;j++)
        {
          let count = this.dataPagination[i].data[j].count;
          if(count > (this.filter.pageNo-1)*this.filter.pageSize && count <= (this.filter.pageNo)*this.filter.pageSize)
          {
            this.data[i].data.push(this.dataPagination[i].data[j]);
          }
        }
    
    }
  }

  convertTextData(text)
  {    
    return this.exportService.convertTextData(text);
  }

  updateDataTable() {
    this.dataTable.eventUpdate.pipe(
      tap(option => {
        this.filter.pageNo = option.pageNo;
        this.filter.pageSize = option.pageSize;
        if(this.showFullData)
        {
          this.getData(this.filter);
        }else{
          this.dataTable.isLoading = true;
          setTimeout(() => {
            this.getDataShowPage();
            this.dataTable.update({
              data: this.data
            });
            this.cdr.markForCheck();
            this.cdr.detectChanges();

            this.dataTable.isLoading = false;

          },300);
       
        }
      }),

    ).subscribe();
  }

  convertNumberToTime(number) {
    return this.userDate.convertNumberToTime(number);
  }

  setDataTable() {
    this.dataTable.init({
      data: this.data,
      totalRecod: this.totalRecod,
      paginationSelect: this.paginationSelect,
      columns: this.columns,
      layout: {
        body: {
          scrollable: false,
          maxHeight: 600,
        },
        selecter: false,
        responsive: false,

      }
    });
  }

  onResize($elm) {
    this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    this.cdr.detectChanges();
  }

  roundNumber(number) {
    return Math.round(number * 100) / 100;
  }

  

  //pdf
  async exportPDF(){
     if(this.getListFilter()) this.getData(this.filter,'pdf');
  }
  
  exportFilePDF(data){
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let config:PdfModel = {
      info: {
        title: 'SummaryFuel',
        columnParent:'fuel',
      },
      content:{
        header:{
          title: this.translate.instant('REPORT.FUEL.GENERAL.EXPORT_FUEL'),
          params:{
            timeStart:": "+this.dateStart,
            timeEnd:": "+this.dateEnd,
            timeZone:": "+this.timeZone
          }
        },
        table:[
          {
              woorksheet: {
                name: this.deviceName,
              },
              total : {
                group : [
                  { columnData:'deviceName'},
                ],
          
              },
              headerRows: 1,
              widths: ['4%','13%','13%','13%','13%','8%','8%','8%','8%','8%'],
              header :[           
                  [ 
                    { text: '#',alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.DATE'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('DASHBOARD.STATIC.DISTANCE_LABEL'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.ENGINE'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('COMMON.COLUMN.SENSORS'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.START_DAY'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.END_DAY'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.CONSUME'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.FUEL.GENERAL.ADD'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]},
                    { text: this.translate.instant('REPORT.FUEL.GENERAL.REMOVE'),alignment:"center",fillColor:"#EEEEEE",bold:true,margin:[0,4,0,0]}
                  ]
              ],
              body:[
                {auto:true},
                {columnData:'timeFromUtc',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnData:'distance',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnData:'engine',style:{alignment:"center",fillColor:"#fff",bold:false}},
                {columnParent:'fuel',columnData:'name',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnParent:'fuel',columnData:'start',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnParent:'fuel',columnData:'end',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnParent:'fuel',columnData:'consume',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnParent:'fuel',columnData:'add',style:{alignment:"left",fillColor:"#fff",bold:false}},
                {columnParent:'fuel',columnData:'remove',style:{alignment:"left",fillColor:"#fff",bold:false}},
              ],              
          },
          
        ]
          },
      footer:{
        title: this.datepipe.transform(this.dateStart, dateTimeFormat)+ ' - ' + this.datepipe.transform(this.dateEnd, dateTimeFormat)
      }    
    }

    this.pdf.ExportPdf([this.dataPagination],config);
    // this.xlsx.exportFileTest([data], config, {});
 
  }
  
  //export
  exportXlSX() {
    if(this.getListFilter()) this.getData(this.filter,'excel');

  }
  private exportFileXLSX(data) {
    let dateTimeFormat = this.dateFormat + ' ' + this.timeFormat;
    let config: XLSXModelTest = {
      file: {
        title: this.translate.instant('REPORT.FUEL.GENERAL.EXPORT_FUEL'),
        prefixFileName: "SummaryFuel",
        columnParent:'fuel',
      },
      template: [{
        header: [
          {
            text: this.translate.instant('REPORT.FUEL.GENERAL.EXPORT_FUEL'),
            type: "header",
          },
          {
            text: ""
          },
          {
            text: '!timezone',
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START') + ": " + this.datepipe.transform(this.dateStart, dateTimeFormat),
          },
          {
            text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_END') + ": " + this.datepipe.transform(this.dateEnd, dateTimeFormat),
          },
          {
            text: ""
          },
        ],
        columns: [
          {
            name: "#",
            columnData: "count",
            wch: 5,
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
          },
          {
            name: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.DATE'),
            columnData: "timeFromUtc",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
          },
          {
            name: this.translate.instant('DASHBOARD.STATIC.DISTANCE_LABEL'),
            columnData: "distance",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 20,
          },
          {
            name: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.ENGINE'),
            columnData: "engine",
            style: { alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 10,
          },
          {
            name: this.translate.instant('COMMON.COLUMN.SENSORS'),
            columnData: "name",
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
            type: "mergeTime",
            wch: 15,
            columnParent:'fuel',
          },
          {   
            name: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.START_DAY'),
            wch: 15,
            columnData: "start",
            columnParent:'fuel',
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
          },
          {   
            name: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.END_DAY'), 
            wch: 15,
            columnData: "end",
            columnParent:'fuel',
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
          },
          {  
            name: this.translate.instant('REPORT.SUMMARY.DAY.COLUMN.CONSUME'), 
            wch: 15,
            columnData: "consume",
            columnParent:'fuel',
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
          },
          {   
            name: this.translate.instant('REPORT.FUEL.GENERAL.ADD'),
            wch: 15,
            columnData: "add",
            columnParent:'fuel',
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
          },
          {   
            name: this.translate.instant('REPORT.FUEL.GENERAL.REMOVE'),
            wch: 15,
            columnData: "remove",
            columnParent:'fuel',
            style: { alignment: { vertical: "center", horizontal: "left", indent: 0, wrapText: true } },
          },

        ],
        woorksheet: {
          name: this.deviceName,
        },
        total: {
          group: "deviceName",
        
        }
      }]

    };
    this.xlsx.exportFileTest([this.data], config, {});
  }
  setColumns() {
    this.columns = [
      {
        title: '#',
        field: 'no',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '20px' },
        class: 't-datatable__cell--center',
        translate: '#',
        autoHide: false,
        width: 20,
      },
      {
        title: 'date',
        field: 'Engine',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px','text-align':'center' },
        styleNoFuel :{'width': '200px','text-align':'center'},

        class: '',
        translate: 'REPORT.SUMMARY.DAY.COLUMN.DATE',     
        autoHide: false,
        width: 200,
      },
      {
        title: 'distance',
        field: 'Engine',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px' },
        styleNoFuel :{'width': '200px','text-align':'center'},

        class: '',
        translate: 'DASHBOARD.STATIC.DISTANCE_LABEL',     
        autoHide: false,
        width: 200,
      },
      {
        title: 'Engine',
        field: 'Engine',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '150px','text-align':'center' },
        styleNoFuel :{'width': '200px','text-align':'center'},

        class: '',
        translate: 'REPORT.SUMMARY.DAY.COLUMN.ENGINE',     
        autoHide: false,
        width: 200,
      },
      {
        title: 'Sensor',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'COMMON.COLUMN.SENSORS',     
        autoHide: false,
        width: 200,
        fuel:true

      },
      {
        title: 'Start day',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.SUMMARY.DAY.COLUMN.START_DAY',     
        autoHide: false,
        width: 200,
        fuel:true

      },
      {
        title: 'Speed avg',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.SUMMARY.DAY.COLUMN.END_DAY',     
        autoHide: false,
        width: 200,
        fuel:true

      },
      {
        title: 'Comsume',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.SUMMARY.DAY.COLUMN.CONSUME',     
        autoHide: false,
        width: 200,
        fuel:true

      },
      {
        title: 'Comsume',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.FUEL.GENERAL.ADD',     
        autoHide: false,
        width: 200,
        fuel:true

      },
      {
        title: 'Comsume',
        field: 'moveEnd',
        allowSort: false,
        isSort: false,
        dataSort: '',
        style: { 'width': '100px' },
        class: '',
        translate: 'REPORT.FUEL.GENERAL.REMOVE',     
        autoHide: false,
        width: 200,
        fuel:true
      },
    ]
  }


}




