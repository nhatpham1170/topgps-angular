import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PartialsModule } from '@app/views/partials/partials.module';
import { RouterModule, Routes } from '@angular/router';
import { NgbDropdownModule, NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ClipboardModule } from 'ngx-clipboard';
import { NgxPermissionsModule } from 'ngx-permissions';

//
import { ModuleGuard, AuthGuard } from '@core/auth';


import { CoreModule } from '@core/core.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';// Angular Material
import { ChartsModule } from 'ng2-charts';

import {DragDropModule} from '@angular/cdk/drag-drop';

const routes: Routes = [{

}]

@NgModule({

  imports: [
    CoreModule,
    CommonModule,
    RouterModule.forChild(routes),
    PartialsModule,
    NgbDropdownModule,
    NgbModalModule,
    NgbModule,
    ClipboardModule,
    NgxPermissionsModule,
    NgSelectModule,
    AutocompleteLibModule,
    FormsModule,
    ReactiveFormsModule,
    DragDropModule,
    ChartsModule
  ],
  providers: [ModuleGuard],
  declarations: [
    //  ReportGeneralComponent,
    // FuelChartServiceComponent
  ]
})
export class UtilitiesModule { }
