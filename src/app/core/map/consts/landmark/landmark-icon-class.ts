export const landmarkIconClass = {
    HOME: 'icon-home',
    RESTAURANT: 'icon-restaurant',
    SCHOOL: 'icon-graduted-hat',
    STADIUM: 'icon-stadium2',
    COMPANY: 'icon-company',
    HOSPITAL: 'icon-hospital',
    SUPERMARKET: 'icon-supermarket'
}