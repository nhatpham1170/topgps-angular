export function createLandmarkIcon(icon: string){
    return `<svg xmlns="http://www.w3.org/2000/svg" style="animation: jump 0.9s" width="40" height="38" viewBox="0 0 42 36" fill="{color}">
                  <rect x="5" stroke="#fff" stroke-width="1" y="1" width="32" height="32" rx="5" ry="5"></rect>.
                  <svg  width="24" height="24" x="9" y="4.5">${icon}</svg>
	                <svg width="10" x="16" y="30" height="10" viewBox="0 0 10 8">
                      <polyline points="0,0 5,7 10,0" stroke="#fff" stroke-width="1"/>
                      <polygon points="0,0 5,7 10,0" fill="{color}" stroke="{color}" stroke-width="0.5"/>
                  </svg>
            </svg>`
}