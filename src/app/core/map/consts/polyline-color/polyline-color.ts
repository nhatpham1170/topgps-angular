export function getSpeedColor(speed){
    if(speed ==0){
        speed = 1;
    }
    let max = 252;
    let color: string;
   if(speed<=5){
    //    color = `rgb(${max},${Math.floor(max/speed)},0)`
       color = '#f22105'
   } else if(speed>5 && speed <= 30){
    //    color = `rgb(0,${Math.floor(max * (speed-20)/10) },${max})`
       color = '#eb8e02'
   } else if (speed > 30) { color = '#0ac720'}
   return color
}