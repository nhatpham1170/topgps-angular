export interface TollFee {
    datetime: string;
    message: string;
    messageCode: string;
    result: TollFeeDetail[];
    status: number
}

export interface TollFeeDetail {
    amount: number;
    tollStations: {
        address: string;
        encodedPoints: string;
        id: number;
        name: string;
        redius: number;
        timestamp: number;
        type: number;
        typeGeofence: string;
    }[]
    unit: string;
}