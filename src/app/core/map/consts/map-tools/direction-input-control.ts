export class DirectionInputControl {
    id: number;
    name: string;
    data;
    type: string;
    value: string;
    active?:number;
    constructor(id: number, name: string) {
        this.name = name;
        this.id = id
    }
}