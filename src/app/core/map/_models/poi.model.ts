export interface Poi {
    status: number;
    datetime: string;
    message: string;
    messageCode: string,
    result: {
        content: PoiResult[],
        totalRecord: number;
    }
}

export interface PoiResult {
    id: number;
    name: string;
    userId: number;
    poiTypeName: string;
    typeName: string;
    poiType: number;
    latitude: number;
    longitude: number;
    description: string;
    active: number;
    createdBy: number;
    updatedBy: number;
    createdAt: string;
    updatedAt: string;
    createdByName: string;
    updatedByName: string;
    fillColor: string
}