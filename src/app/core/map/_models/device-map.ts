import { Driver, DeviceGroup, GeofenceModel } from '@core/manage';
import { Icon } from '@core/manage/_models/icon';
import { BaseModelCT } from '@core/_base/crud/models/_base.model.ct';
import { IconType, StatusType } from '@core/utils/map';

export class DeviceMap extends BaseModelCT {
    id: number;
    name: string;
    imei: string;
    address: string;
    direction: string;
    driver: Driver;
    drivingTime: string;
    icon: Icon;
    lat: string;
    lng: string;
    status: string;
    time: string;
    dayDistance: string;
    dayDrivingTime: string;
    VIN: string;
    speed: string;
    trktime: string;
    updatetime: string;
    iconType: IconType;
    iconTypeProcess: IconType;
    statusType: StatusType;
    groups: Array<DeviceGroup>;
    groupsStr: string;
    updatetimeUTC: string;
    timestampUTC: string;
    powerVoltage:number;
    type:number;
    typpeName:string;
    numberPlate:string;
    timestampProcess:string;
    battery: {
        isCharged: false,
        level: any,
        percent: any,
        voltage: any,
    };
    batterySvg:string;
    gsm:{
        level:any,
        signal:any,
        translate:string;
    };
    statusDuration:number;
    statusDurationStr:string;
    geofence:string;
    simno:string;
    feature?:{
        follow?:boolean,
        pin?:boolean,
        hideOnMap?:boolean
    };
    geofenceObj:GeofenceModel;
    favorite:number = 0;
    clear() {

    }
}
