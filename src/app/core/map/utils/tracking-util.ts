import { Injectable, Pipe, NgModule } from '@angular/core';
import { MapConfigModel, MapConfig, SVGUtil } from '@core/utils/map';
import { UserDatePipe, AsciiPipe } from '@core/_base/layout';
import { DomSanitizer } from '@angular/platform-browser';
import moment from 'moment';

@Injectable({
    providedIn: 'root'
})

export class TrackingUtil {
    private configMap: MapConfigModel;
    constructor(
        private sanitizer: DomSanitizer,
        private userDate: UserDatePipe,
        private svgUtil: SVGUtil) {
        this.configMap = new MapConfig().configs();
    }
    processItems(data, datetimeNow) {
        let _this = this;
        const icons = _this.configMap.icons;
        let dataNew = data.map(x => {
            let indexIcon = 0;
            if (x['icon']['name']) {
                let indexIconSearch = _this.configMap.icons.findIndex(i => i.name == x['icon']['name']);
                if (indexIconSearch >= 0) {
                    indexIcon = indexIconSearch;
                }
            }
            x.iconType = Object.assign({}, icons[indexIcon]);
            x.iconTypeProcess = Object.assign({}, icons[indexIcon]);

            if (x['lat'] == "null") x['lat'] = null;
            if (x['lng'] == "null") x['lng'] = null;
            x.groupsStr = x.groups.map(g => {
                return g.name;
            }).join(', ');
            if (x['timestampUTC']) {
                x['timestampProcess'] = _this.userDate.transform(x['timestampUTC'], 'fromNowDateTimeShort3', datetimeNow);
            }
            if (x['battery']['isAvailable'] && x['battery']['percent'] != undefined) {
                x.batterySvg = _this.sanitizer.bypassSecurityTrustHtml(_this.svgUtil.renderBattery(x['battery']['percent'], x['battery']['isCharged']));
            }
            // status_duration
            if (x['statusDuration'] != undefined) {
                x['statusDuration'] = Math.abs(x['statusDuration']);
                x['statusTimestamp'] = moment().utc().unix() - x['statusDuration'];
                x['statusDurationStr'] = _this.userDate.transform(datetimeNow, 'fromNowDateTimeShort', null, "datetime", x['statusDuration']);
            }
            // signal
            if (x.gsm) {
                if (x.gsm.level != null) {
                    let gsmTranslate = "COMMON.LIST_DEVICE.GSM.LOST_GSM";
                    if (x.gsm.level > 0 && x.gsm.level <= 25) {
                        gsmTranslate = "COMMON.LIST_DEVICE.GSM.WEAK";
                    } else if (x.gsm.level > 25 && x.gsm.level <= 50) {
                        gsmTranslate = "COMMON.LIST_DEVICE.GSM.MEDIUM";
                    } else if (x.gsm.level > 50 && x.gsm.level <= 75) {
                        gsmTranslate = "COMMON.LIST_DEVICE.GSM.GOOD";
                    } else if (x.gsm.level > 75) {
                        gsmTranslate = "COMMON.LIST_DEVICE.GSM.EXCELLENT";
                    }
                    x.gsm.translate = gsmTranslate;
                }
            }
            // allow show on map

            x.iconTypeProcess.icon = x.iconTypeProcess.icon
                .replace('viewBox="0 0 34 34"', 'viewBox="0 0 24 24"')
                .replace('viewBox="0 0 48 48"', 'viewBox="0 0 36 36"')
                .replace('transform="rotate({iconRotate})"', "")
                .replace('{iconFill}', "#fff")
                .replace(/_1/g, "_" + Date.now() + "_" + (Math.random() * 100).toFixed(3))
                .replace(/_2/g, "_" + Date.now() + "_" + (Math.random() * 100).toFixed(3));
            x.iconTypeProcess.icon =
                _this.sanitizer.bypassSecurityTrustHtml(x.iconTypeProcess.icon);
            x.feature = {};
            x.otherData = {
                air: "-",
                door: "-",
                engine: "-",
            };
            // if (x.statusAdvance != undefined) {
            //     if (x.statusAdvance.air != undefined) x.otherData.air = "COMMON.LIST_DEVICE.GENERAL." + (x.statusAdvance.air === 1 ? "ON" : "OFF");
            //     if (x.statusAdvance.door != undefined) x.otherData.door = "COMMON.LIST_DEVICE.GENERAL." + (x.statusAdvance.door === 1 ? "OPEN" : "CLOSED");
            //     if (x.statusAdvance.engine != undefined) x.otherData.engine = "COMMON.LIST_DEVICE.GENERAL." + (x.statusAdvance.engine === 1 ? "ON" : "OFF");
            // }
            
            if (x.basic != undefined) {
                x.basic.forEach(status=>{
                    switch(status.type){
                    case "door":
                        x.otherData.door = "COMMON.LIST_DEVICE.GENERAL." + ( status.value === 1 ? "OPEN" : "CLOSED");
                         break;
                    case "air":
                        x.otherData.air = "COMMON.LIST_DEVICE.GENERAL." + ( status.value  === 1 ? "ON" : "OFF");
                        break;
                    case "engine":
                        x.otherData.engine = "COMMON.LIST_DEVICE.GENERAL." + ( status.value  === 1 ? "ON" : "OFF");
                        break;
                    }
                });
            }
            // process geofence

            x = this.processStatus(x);
            return x;
        });
        return dataNew;
    }
    private processStatus(item) {
        switch (item.status) {
            case "lost_gps":
                item.statusType = this.configMap.status.lostGPS;
                break;
            case "lost_gprs":
                item.statusType = this.configMap.status.lostGPRS;
                break;
            case "history_transfer":
                item.statusType = this.configMap.status.historyTransfer;
                break;
            case "expired":
                item.statusType = this.configMap.status.expired;
                break;
            case "stop":
                item.statusType = this.configMap.status.stop;
                break;
            case "run":
                item.statusType = this.configMap.status.run;
                break;
            case "inactive":
                item.statusType = this.configMap.status.inactive;
                break;
            case "nodata":
                item.statusType = this.configMap.status.nodata;
                break;
            default:
                item.statusType = this.configMap.status.lostSignal;
                break;
        }
        return item;

    }

}
