import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment.prod';
import { tap } from 'rxjs/operators';
import { DeviceMap } from '../_models/device-map';

const API_URL: string = environment.api.host + "/map";
@Injectable({
  providedIn: 'root'
})

export class MapService {
  public eventChange: EventEmitter<EventMapService>;
  constructor(private http: HttpClient, private utils: TypesUtilsService) {
    this.eventChange = new EventEmitter();
  }
  list(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result) {
          body.result = body.result.map(function (x) {
            let tranfrom = new DeviceMap().deserialize(x, {
              tranform: {
                'id': 'deviceId',
                'name': 'deviceName',
                'imei': 'imei',
                'diretion': 'diretion',
                'battery': 'battery',
                'dayDistance': 'dayDistance',
                'dayDrivingTime': 'dayDrivingTime',
                'lat': 'latitude',
                'lng': 'longitude',
                'speed': 'speed',
                'status': 'status',
                'time': 'time',
                'powerVoltage': 'powerVoltage',
                'updatetimeUTC': 'updatetimeUTC',
                'timestampUTC': 'timestampUTC',
                'groups': 'group',
                'numberPlate': 'numberPlate'
              }
            });
            return tranfrom;
          });
        }
      }),
    );
  }
  playback(option?: BaseOptionAPI): Observable<ResponseBody> {

    return this.http.get<ResponseBody>(API_URL + "/playback", {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
      }),
    );
  }
}

export class EventMapService {
  type: string;
  data?: any;
  options?: any;
}