import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment.prod';
import { tap } from 'rxjs/operators';
import { DeviceMap } from '../_models/device-map';

const API_URL: string = environment.api.host + "/map/tracking";
@Injectable({
  providedIn: 'root'
})
export class TrackingService {

  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result) {
          body.result = body.result.map(function (x) {
            let tranfrom = new DeviceMap().deserialize(x, {
              tranform: {
                'id': 'deviceId',
                'name': 'deviceName',
                'imei': 'imei',
                'diretion': 'diretion',
                'battery': 'battery',
                'dayDistance': 'dayDistance',
                'dayDrivingTime': 'dayDrivingTime',
                'lat': 'latitude',
                'lng': 'longitude',
                'speed': 'speed',
                'status': 'status',
                'time': 'time',
                'powerVoltage': 'powerVoltage',
                'updatetimeUTC': 'updatetimeUTC',
                'timestampUTC': 'timestampUTC',
                'groups': 'group',
                'numberPlate': 'numberPlate'
              }
            });
            return tranfrom;
          });
        }
      }),
    );
  }
  
  listRefresh(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL + "/items", {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result) {
          body.result = body.result.map(function (x) {
            let tranfrom = new DeviceMap().deserialize(x, {
              tranform: {
                'id': 'deviceId',
                'name': 'deviceName',
                'imei': 'imei',
                'diretion': 'diretion',
                'battery': 'battery',
                'dayDistance': 'dayDistance',
                'dayDrivingTime': 'dayDrivingTime',
                'lat': 'latitude',
                'lng': 'longitude',
                'speed': 'speed',
                'status': 'status',
                'time': 'time',
                'powerVoltage': 'powerVoltage',
                'updatetimeUTC': 'updatetimeUTC',
                'timestampUTC': 'timestampUTC',
                'groups': 'group',
                'numberPlate': 'numberPlate'
              }
            });
            // tranfrom.lat += Math.random()/100;
            // tranfrom.lng += Math.random()/100;
            return tranfrom;
          });
        }
      }),
    );
  }
  detail(option?: BaseOptionAPI): Observable<ResponseBody> {
    let deviceId = option.params.deviceId;

    return this.http.get<ResponseBody>(API_URL + "/" + deviceId, {
      // params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result) {
          body.result = new DeviceMap().deserialize(body.result, {
            tranform: {
              'id': 'deviceId',
              'name': 'deviceName',
              'imei': 'imei',
              'diretion': 'diretion',
              'battery': 'battery',
              'dayDistance': 'dayDistance',
              'dayDrivingTime': 'dayDrivingTime',
              'lat': 'latitude',
              'lng': 'longitude',
              'speed': 'speed',
              'status': 'status',
              'time': 'time',
              'powerVoltage': 'powerVoltage',
              'updatetimeUTC': 'updatetimeUTC',
              'timestampUTC': 'timestampUTC',
              'groups': 'group',
              'numberPlate': 'numberPlate'
            }
          });
          // body.result.lat += Math.random() / 100;
          // body.result.lng += Math.random() / 100;
        }
      }),
    );
  }
}