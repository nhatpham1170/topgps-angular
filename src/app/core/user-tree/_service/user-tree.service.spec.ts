import { TestBed } from '@angular/core/testing';

import { UserTreeService } from './user-tree.service';

describe('UserTreeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserTreeService = TestBed.get(UserTreeService);
    expect(service).toBeTruthy();
  });
});
