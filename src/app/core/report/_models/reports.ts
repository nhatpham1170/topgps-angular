export class Reports {
 id:string;
 name:string;
 img:string;
 icon:string;
 status:number
 favoriteReport:FavoriteReport[];
 userId : string

}

export class FavoriteReport{
    key : string;
    sortOrder : string
}
   