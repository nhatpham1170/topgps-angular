import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment';

const API_URL:string = environment.api.host + "/report/fuel-change";
const API_URL_DETAIL:string = environment.api.host + "/report/fuel-detailed";
const API_URL_SUMMARY:string = environment.api.host + "/report/fuel-summary";

@Injectable({
  providedIn: 'root'
})
export class ReportFuelService {
 
  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(params:any): Observable<ResponseBody> 

  {return this.http.get<ResponseBody>(API_URL, {params:params})}

  detail(params:any,option?: BaseOptionAPI): Observable<ResponseBody> 
  {return this.http.get<ResponseBody>(API_URL_DETAIL, {params:params})}

  summary(params:any,option?: BaseOptionAPI): Observable<ResponseBody> 
  {return this.http.get<ResponseBody>(API_URL_SUMMARY, {params:params})}
}