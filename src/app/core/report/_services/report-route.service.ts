import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment';
import { tap } from 'rxjs/operators';

const API_URL:string = environment.api.host + "/report/route";
const API_URL_map: string = environment.api.host + "/utility/maps";

@Injectable({
  providedIn: 'root'
})
export class ReportRouteService {
  
  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(params:any): Observable<ResponseBody> 
  {return this.http.get<ResponseBody>(API_URL, {params:params})}

  geocode(option?: BaseOptionAPI): Observable<ResponseBody> {
    option.sessionStore = true;
    return this.http.get<ResponseBody>(API_URL_map + "/geocode", {
      params: this.utils.processBassOptionApi(option),
    });
  }
}