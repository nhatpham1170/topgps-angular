import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment';
import { tap } from 'rxjs/operators';
import { Reports } from '../_models/reports';


const API_URL_REPORT:string = environment.api.host + "/settings/fav-report";
const API_URL:string = environment.api.host + "/settings";
const API_REPORT:string = environment.api.host + "/report";

@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  
  constructor(private http: HttpClient, private utils: TypesUtilsService) {}

  listReport(params:Reports): Observable<ResponseBody> 
  {
    return this.http.get<ResponseBody>(API_URL+"/"+params.id);
  }

  addReport(params:any, option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.post<ResponseBody>(API_URL, params,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  updateReport(params:any, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    formData.append("userId", params.userId);
    formData.append("favoriteReport", JSON.stringify(params.favoriteReport));
    return this.http.post<ResponseBody>(API_URL_REPORT, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  removeReport(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`;
    return this.http.delete<ResponseBody>(url,{
      params:this.utils.processBassOptionApi(option),
    });
  }
  dataLogs(options:BaseOptionAPI): Observable<ResponseBody> 
  {
    return this.http.get<ResponseBody>(`${API_REPORT}/data-log`,{
      params:this.utils.processBassOptionApi(options)
    });
  }



}