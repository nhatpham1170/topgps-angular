import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment';
import { tap } from 'rxjs/operators';

const API_URL:string = environment.api.host + "/report/gov/stop";

@Injectable({
  providedIn: 'root'
})
export class QCVNStopService {
  
  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
//   list(params:any): Observable<ResponseBody> 
//   {return this.http.get<ResponseBody>(API_URL, {params:params})}
  list(option?: BaseOptionAPI): Observable<ResponseBody> {
    option.sessionStore = true;
    let params = {
        params: option
    };
    return this.http.get<ResponseBody>(API_URL, {
      params: this.utils.processBassOptionApi(params),
    });
  }
}