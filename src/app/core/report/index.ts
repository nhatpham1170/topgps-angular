//service
export { ReportDeviceService } from "./_services/report-device.service";
export { ReportRouteService } from "./_services/report-route.service";
export { ReportsService } from "./_services/reports.service";
export { ReportEngineService } from "./_services/report-engine.service";
export { ReportFuelService } from "./_services/report-fuel.service";
export { ReportSummaryDayService } from "./_services/report-summary-day.service";
export { ReportSummaryDevicesService } from "./_services/report-summary-devices.service";
export { ReportGeofenceSummaryService } from "./_services/report-geofence-summary.service";
export { ReportGeofenceDetailService } from "./_services/report-geofence-detail.service";
export { ReportTemperatureService } from "./_services/report-temperature.service";
export { ReportTollStationService } from "./_services/report-toll-station.service";

export { ReportStatusHistoryService } from "./_services/report-status-history.service";
export { ReportSummaryTripService } from "./_services/report-summary-trip.service";


//model
export { Reports,FavoriteReport } from "./_models/reports";
export { ReportFuel } from "./_models/report-fuel";

//qcvn

export { QCVNRouteService } from "./_services/qcvn-route.service";
export { QCVNSpeedService } from "./_services/qcvn-speed.service";
export { QCVNOverSpeedService } from "./_services/qcvn-over-speed.service";
export { QCVNDrivingService } from "./_services/qcvn-driving.service";
export { QCVNStopService } from "./_services/qcvn-stop.service";
export { QCVNSummaryByVehicleService } from "./_services/qcvn-summary-by-vehicle.service";

export { QCVNSummaryByDriverService } from "./_services/qcvn-summary-by-driver.service";
export { QCVNDataTransmissionService } from "./_services/qcvn-data-transmission.service";
export { QCVNDrivingOnDayService } from "./_services/qcvn-driving-on-day.service";

