
import { EventEmitter, ChangeDetectorRef } from '@angular/core';
import { max } from 'moment';
import { NgxPermissionsService } from 'ngx-permissions';

declare var $: any;

const className: string = "DataTable";

/**
 * DataTable manage page
 * 1. Manage pagination
 * 2. Sort order by field name
 * 3. Search by data option and formSearch
 */
export class DataTable {
    private isDebug: boolean;
    public data: any;
    public dataLocal: any;
    private lastColumnSort: string;
    private formSearch: string;// id or class 
    private formBuilderSearch: any; // form Builder angular
    private lastWidth: number = 0;
    private lastChecked: number = 0;
    private columns: [
        {
            field?: string,
            title?: string,
            sortable?: string,
            width?: number,
            type?: string,
            selector?: boolean,
            textAlign?: string,
            format?: string,
            class?: string,
            style?: string,
            template?: string,
            allowSort?: boolean,
            isSort?: boolean,
            autoHide?: boolean,
            isShow?: boolean,
            translate?: string,
            permisison?: string,
            display?: boolean,
        }
    ];
    private paginations: {
        total: number,
        pageSize: number,
        currentPage: number,
        maxPageDisplay: number,
        pageDisplays: any,
        firstPage: number,
        lastPage: number,
        form: number,
        to: number,
    };
    private api: {
        pagination?: {
            currentPage: number,
            pageSize: number,
        },
        query?: {
            key: any,
            value: any
        }[],
        sorts?: {
            sort: string,
            field: string
        }
    };
    private config: {
        paginationSelect: any[],
    };
    private layout: {
        body: {
            scrollable: boolean,
            maxHeight: number,
        }
        selecter?: boolean,
        responsive?: boolean
    }
    public lastRowShow = -1;
    public isShowSubRow = false;
    public isCheckBoxAll: boolean;
    public eventUpdate: EventEmitter<any>;
    public isLoading: boolean;
    public isNotFound: boolean;
    public showFull: boolean;
    public isDataLocal: boolean;
    public crd: ChangeDetectorRef;
    private dataSearch: any;
    private isClickSubRow: boolean = false;

    /**
     * Creates an instance of this class that can
     * Set default value
     */
    constructor(
    ) {
        this.eventUpdate = new EventEmitter<any>();
        this.isLoading = false;
        this.isDebug = false;
        this.lastColumnSort = '';
        this.api = {
            pagination: {
                currentPage: 1,
                pageSize: 10,
            },
            query: [],
            sorts: {
                sort: "",
                field: ""
            }
        };
        this.paginations = {
            total: 0,
            pageSize: 10,
            currentPage: 1,
            maxPageDisplay: 5,
            pageDisplays: [1, 2, 3, 4, 5],
            firstPage: 1,
            lastPage: 1,
            form: 0,
            to: 0,
        };
        this.config = {
            paginationSelect: [10, 20, 30, 50, 100]
        };
        this.isNotFound = false;
        this.layout = {
            body: {
                scrollable: true,
                maxHeight: 500,
            },
            selecter: false,
            responsive: true,
        }
        this.isCheckBoxAll = false;
    }

    /**
     * @returns all page display
     */
    getPageDisplay() {
        return this.paginations.pageDisplays;
    }

    /**
     * @returns first page
     */
    getFirstPage() {
        return this.paginations.firstPage;
    }

    /**
     * @returns last page
     */
    getLastPage() {
        return this.paginations.lastPage;
    }

    /**
     * @returns current page select
     */
    getCurrentPage() {
        return this.paginations.currentPage;
    }
    /**
     * @returns pagination select
     */
    getPaginationSelect() {
        return this.config.paginationSelect;
    }

    /**
     * @returns paginations config
     */
    getPaginations() {
        return this.paginations;
    }

    /**
     * @returns columns config
     */
    getColumns() {
        return this.columns;
    }
    getColumn(field: string) {
        let column = this.columns.find(x => x.field == field);
        return column;
    }
    get isResponsive() {
        return this.layout.responsive;
    }

    setColumns(columns) {
        let columnDefault = {
            field: '',
            title: "",
            sortable: "",
            width: "0",
            type: "",
            selector: false,
            textAlign: "",
            format: "",
            class: "",
            style: "",
            template: "",
            allowSort: false,
            isSort: false,
            autoHide: true,
            isShow: true
        }
        let columnNew;
        columnNew = [];
        columns.forEach(element => {
            columnNew.push(Object.assign({}, columnDefault, element));
        });
        this.columns = columnNew;
    }

    /**
     * Go to page number
     * @param page 
     */
    gotoPage(page: number) {
        if (this.isDebug) {
            console.info("%s call gotoPage()", className);
        }
        if (page >= this.paginations.firstPage && page <= this.paginations.lastPage && page != this.paginations.currentPage) {
            this.reload({ currentPage: page });
        }
    }

    /**
       * Set page size 
       * Reload page and set default currentPage = 1
       * @param pageSize number pagesize
       */
    setPageSize(pageSize: number) {
        if (this.isDebug) {
            console.info("%s call setPageSize()", className);
        }
        if (pageSize > 0 && pageSize != this.paginations.pageSize) {
            this.reload({ pageSize: pageSize, currentPage: 1 });
        }
    }

    getDataSearch() {
        return Object.assign({}, this.dataSearch);
    }

    /**
     * Init this class
     * @param option data setting
     */
    init(option: {
        data?: any,
        currentPage?: number,
        totalRecod?: number,
        pageSize?: number,
        maxPageDisplay?: number,
        paginationSelect?: any,
        columns: any,
        formSearch?: string,
        isDebug?: boolean,
        layout?: {
            body?: {
                scrollable: boolean,
                maxHeight: number,

            },
            selecter?: boolean,
            responsive?: boolean,
        },
        showFull?:boolean,
        isDataLocal?:boolean
    }) {
        if (option.isDebug) {
            console.info("%s call init()", className);
        }
        // set value with data option
        if (option.data) this.setData(option.data);
        if (option.currentPage) this.paginations.currentPage = option.currentPage;
        if (option.totalRecod) this.paginations.total = option.totalRecod;
        if (option.pageSize) this.paginations.pageSize = option.pageSize;
        if (option.paginationSelect) this.config.paginationSelect = option.paginationSelect;
        if (option.maxPageDisplay) {
            if (option.maxPageDisplay > 0 && option.maxPageDisplay < 10)
                this.paginations.maxPageDisplay = option.maxPageDisplay
        }
        if (option.columns) this.setColumns(option.columns);
        if (option.formSearch) this.formSearch = option.formSearch;
        if (option.isDebug) this.isDebug = option.isDebug;
        if (option.layout) this.layout = Object.assign({}, this.layout, option.layout);
        if(option.showFull) this.showFull = option.showFull;
        if(option.isDataLocal) this.isDataLocal = option.isDataLocal;
        // const
        // //update data
        // console.log(document.querySelector('.datatable'));
        // resizeObserver.observe(document.querySelector('.datatable'));    

        this.update(option);
        $(function () {
            $('#dataTableSelect select').selectpicker();
            $('.kt-datatable__pager-info .selectpicker').selectpicker();
        });
    };

    /**
     * Process and update data table by option
     * @param option data config
     */
    update(option: {
        data?: any,
        currentPage?: number,
        totalRecod?: number,
        pageSize?: number,
        maxPageDisplay?: number,
        paginationSelect?: any,
    }) {
        if (this.isDebug) {
            console.info("%s call update()", className);
        }
        if (option.data) {
            this.setData(option.data);
            // this.data = option.data;
        }
        if (option.totalRecod != undefined) {
            this.paginations.total = option.totalRecod;
        }
        if (option.pageSize) {
            this.paginations.pageSize = option.pageSize
        }

        // set first page and last page
        this.paginations.firstPage = 1;
        if (this.paginations.total % this.paginations.pageSize == 0) {
            this.paginations.lastPage = Math.trunc(this.paginations.total / this.paginations.pageSize);
        }
        else {
            this.paginations.lastPage = Math.trunc(this.paginations.total / this.paginations.pageSize) + 1;
        }
        // check end set current page
        if (option.currentPage) {
            if (option.currentPage < this.paginations.lastPage && option.currentPage > 0)
                this.paginations.currentPage = option.currentPage;
        }
        if (this.paginations.currentPage > this.paginations.lastPage && this.paginations.lastPage > 0)
            this.paginations.currentPage = this.paginations.lastPage;
        else if (this.paginations.currentPage < 1)
            this.paginations.currentPage = 1;

        // set from to.
        if (this.paginations.total == 0) {
            this.paginations.form = 0;
            this.paginations.to = 0;
        }
        else {
            this.paginations.form = 1 + (this.paginations.currentPage - 1) * this.paginations.pageSize;
            let to = this.paginations.pageSize * this.paginations.currentPage;
            if (this.paginations.total < to)
                this.paginations.to = this.paginations.total;
            else this.paginations.to = to;
        }
        
        // splipt array if data > page size.
        if (this.data.length > this.paginations.pageSize && !this.showFull) {
            this.data = this.data.slice(0, this.paginations.pageSize);
        }
        // else{
        //     let checkSize = this.paginations.form-this.paginations.to;
        //     if(this.data.length>checkSize)
        //     {
        //         this.data = this.data.slice(0,checkSize+1); 
        //     }
        // }

        // set pagination display
        if (this.data.length >= 0) {
            this.paginations.pageDisplays = [1];
            if (this.paginations.lastPage < this.paginations.maxPageDisplay) {
                this.paginations.pageDisplays = [];
                for (let i = 1; i <= this.paginations.lastPage; i++) {
                    this.paginations.pageDisplays.push(i);
                }
            }
            else {
                let pageStart = 1;
                this.paginations.pageDisplays = [];
                if (this.paginations.currentPage % this.paginations.maxPageDisplay == 0) {
                    pageStart = (Math.trunc(this.paginations.currentPage / this.paginations.maxPageDisplay) - 1) * this.paginations.maxPageDisplay + 1;
                } else {
                    pageStart = (Math.trunc(this.paginations.currentPage / this.paginations.maxPageDisplay)) * this.paginations.maxPageDisplay + 1;
                }
                for (let i = 0; i < this.paginations.maxPageDisplay; i++) {
                    if ((pageStart + i) <= this.paginations.lastPage) {
                        this.paginations.pageDisplays.push(pageStart + i);
                    }
                }
            }
        }
        else this.paginations.pageDisplays = [1];

        // off loading datatable  
        this.isLoading = false;
        if (this.data.length == 0) {
            this.isNotFound = true;
        }
        else this.isNotFound = false;

        $(function () {
            $('bootstrap-select').selectpicker();
            // auto format text imei
            $(".auto-format-text").change(function () {
                var val = $(this).val().replace(/,/g, "\n").replace(/ /g, "");
                $(this).val(val);
            });
            $('.kt-datatable__pager-info .selectpicker').selectpicker();
        });
        // $(function () {
        //     $('#dataTableSelect select').selectpicker();
        //     $('.kt-datatable__pager-info .selectpicker').selectpicker();
        // });
    };
    /**
     * Reload data table after change page
     * @param option setting
     */
    reload(option: {
        data?: any,
        currentPage?: number,
        totalRecod?: number,
        pageSize?: number,
        maxPageDisplay?: number
        paginationSelect?: []
    }) {

        if (this.isDebug) {
            console.info("%s call reload()", className);
        }
        // on loading datatable     
        this.isLoading = true;
        this.isCheckBoxAll = false;
        // set value by value option
        if (option.data) {
            this.setData(option.data);
            // this.data = option.data;
        }
        if (option.currentPage) {
            this.paginations.currentPage = option.currentPage;
        }
        if (option.totalRecod) {
            this.paginations.total = option.totalRecod;
        }
        if (option.paginationSelect) {
            this.config.paginationSelect = option.paginationSelect;
        }
        if (option.pageSize) {
            this.paginations.pageSize = option.pageSize
        }
        if (option.maxPageDisplay) {
            if (option.maxPageDisplay > 0 && option.maxPageDisplay < 10)
                this.paginations.maxPageDisplay = option.maxPageDisplay
        }
        // set value for api
        this.api.pagination.currentPage = this.paginations.currentPage;
        this.api.pagination.pageSize = this.paginations.pageSize;

        let dataSearch = {};
        dataSearch['pageNo'] = this.api.pagination.currentPage;
        dataSearch['pageSize'] = this.api.pagination.pageSize;

        if (this.api.query) {
            this.api.query.forEach(function (value) {
                dataSearch[value.key] = value.value;
            })
        }
        if (this.api.sorts.field.length > 0) {
            dataSearch['orderBy'] = this.api.sorts.field;
            dataSearch['orderType'] = this.api.sorts.sort;
        }

        this.dataSearch = dataSearch;

        // send emit
        this.eventUpdate.emit(dataSearch);
    }
    resetSearch() {
        if (this.formSearch) {
            $(`${this.formSearch} .selectpicker`).val('default').selectpicker("refresh");            
            $(`${this.formSearch} .kt_datepicker`).val("").datepicker("update");            
            $(`${this.formSearch} :input`).each(function () {
                if ($(this).attr('data-key') != undefined) {
                    switch (this.type) {
                        case 'checkbox':
                            $(this).prop('checked', false);
                            break;
                        default:
                            $(this).val('');
                            break;
                    }
                }
            });
        }
    }

    /**
     * Search option 
     * 1. Important searh with query
     * 2. If search param is undefined => search with formSearch
     * @param query  query search 
     */
    search(query?: { key: any, value: any }[], searchByform?: boolean) {
        let _this = this;        
        if (this.isDebug) {
            console.info("%s call search()", className);
        }
        let newQuery: { key: string, value: any }[] = [];

        // check search with query
        if (query) {
            let orderBy = query.find(x => x.key == "orderBy");
            let sort = query.find(x => x.key == "orderType");
            if (orderBy != undefined) {
                this.lastColumnSort = orderBy.value;
                for (let i = 0; i < this.columns.length; i++) {
                    this.columns[i]['isSort'] = false;
                }
                let index = this.columns.findIndex(x => x.field == orderBy.value);
                if (index >= 0) {
                    this.columns[index].isSort = true;
                }
                this.api.sorts.field = orderBy.value;
                this.api.sorts.sort = sort ? sort.value : "asc";

            }
            query.forEach(function (item) {
                if (typeof item.value == "string" && item.value.trim().length > 0) {
                    newQuery.push({
                        key: item.key,
                        value: item.value.trim()
                    });
                }
                else {
                    newQuery.push({
                        key: item.key,
                        value: item.value
                    });
                }

            });
        }
        
        if (searchByform == undefined || searchByform === true) {
            // search  with formSearch
            if (this.formSearch) {
                $(this.formSearch + " :input").each(function () {
                    if ($(this).attr('data-key') != undefined) { 
                        switch (this.type) {

                            case "checkbox":
                                newQuery.push({
                                    key: $(this).attr('data-key'),
                                    value: $(this).prop("checked")
                                });
                                break;
                            case "textarea":
                                if ($(this).val().trim().length > 0) {
                                    newQuery.push({
                                        key: $(this).attr('data-key'),
                                        value: $(this).val().trim().replace(/\n/g, ",")
                                    });
                                }
                                break;
                            default:
                                switch (typeof $(this).val()) {
                                    case "string":
                                        if ($(this).attr('data-date-format') != undefined) {
                                            let format = $(this).attr('data-date-format');
                                            let str = _this.stringToISODateStr($(this).val(), format);

                                            if (str.length > 0) {
                                                newQuery.push({
                                                    key: $(this).attr('data-key'),
                                                    value: str
                                                });
                                            }
                                        }
                                        else {
                                            if ($(this).val().trim().length > 0) {
                                                newQuery.push({
                                                    key: $(this).attr('data-key'),
                                                    value: $(this).val().trim()
                                                });
                                            }
                                        }

                                        break;
                                    case "object":
                                        if (Array.isArray($(this).val()) && $(this).val().length > 0) {
                                            newQuery.push({
                                                key: $(this).attr('data-key'),
                                                value: $(this).val().toString()
                                            });
                                        }
                                        else if (Object.keys($(this).val()).length > 0) {
                                            newQuery.push({
                                                key: $(this).attr('data-key'),
                                                value: JSON.stringify($(this).val())
                                            });
                                        }

                                        break;
                                    case "number":
                                        newQuery.push({
                                            key: $(this).attr('data-key'),
                                            value: $(this).val()
                                        });
                                        break;
                                    default:
                                        newQuery.push({
                                            key: $(this).attr('data-key'),
                                            value: $(this).val()
                                        });
                                        break;
                                }
                                break;
                        }
                    }
                });
            }
        }

        // compare newQuery and api.query, set api query and reload
        if (JSON.stringify(newQuery) != JSON.stringify(this.api.query)) {
            this.api.query = newQuery;
            this.reload({ currentPage: 1 });
        }
    }

    /**
     * Sort data table
     * @param columnName column name order
     */
    sort(columnName) {
        if (this.isDebug) {
            console.info("%s call sort()", className);
        }
        let lastColumn = this.lastColumnSort;

        // compare with lastcolumn
        if (columnName == lastColumn) {
            let index = this.columns.findIndex(x => x.field == lastColumn);

            if (index != -1) {
                if (this.columns[index].allowSort) {
                    this.columns[index].isSort = true;
                    switch (this.columns[index].sortable) {
                        case 'asc':
                            this.columns[index].sortable = 'desc';
                            break;
                        case 'desc':
                            this.columns[index].sortable = 'asc';
                            break;
                        default:
                            this.columns[index].sortable = 'asc';
                            break;
                    }
                    this.api.sorts.field = this.columns[index].field;
                    this.api.sorts.sort = this.columns[index].sortable;
                    this.reload({});
                }
            }
        }
        else {
            let indexNew = this.columns.findIndex(x => x.field == columnName);
            if (indexNew != -1) {
                if (this.columns[indexNew].allowSort) {
                    if (lastColumn.length > 0) {
                        let index = this.columns.findIndex(x => x.field == lastColumn);
                        if (index != -1) {
                            this.columns[index].sortable = '';
                            this.columns[index].isSort = false;
                        }
                    }

                    this.columns[indexNew].sortable = 'asc';
                    this.columns[indexNew].isSort = true;

                    this.lastColumnSort = columnName;
                    this.api.sorts.field = this.columns[indexNew].field;
                    this.api.sorts.sort = this.columns[indexNew].sortable;
                    this.reload({});
                }
            }
        }
    }
    /**
     * 
     * @param option option width height
     */
    updateLayout(option: { width: number, height: number }) {
        if (!this.isLoading && this.lastWidth != option.width && this.layout.responsive && !this.isClickSubRow) {
            this.lastWidth = option.width;
            this.isShowSubRow = false;
            this.isLoading = true;
            let columnsNew;
            columnsNew = this.columns.map(x => x);
            // check column
            let width = option.width;
            let widthMin = 0;
            let offset = 40;
            let checkAutoHide = true;
            for (let i = 0; i < columnsNew.length; i++) {
                if (columnsNew[i].autoHide === false) {
                    let calculator = widthMin + columnsNew[i].width + offset;
                    if (calculator <= width && checkAutoHide) {
                        widthMin = calculator;
                        columnsNew[i].isShow = true;

                    }
                    else {
                        checkAutoHide = false;
                        columnsNew[i].isShow = false;
                        this.isShowSubRow = true;

                    }
                }
            }
            for (let i = 0; i < columnsNew.length; i++) {
                if (columnsNew[i].autoHide) {
                    let calculator = widthMin + columnsNew[i].width + offset;
                    if (calculator <= width && checkAutoHide) {
                        widthMin = widthMin + columnsNew[i].width + offset;
                        columnsNew[i].isShow = true;
                    }
                    else {
                        checkAutoHide = false;
                        columnsNew[i].isShow = false;
                        this.isShowSubRow = true;
                    }
                }
            }


            this.columns = columnsNew;
            this.isLoading = false;
            this.lastRowShow = -1;

            // this.crd.detectChanges();
            // this.update({});
        }

    }

    /**
     * 
     * @param i 
     */
    showSubRow(i) {
        if (i == this.lastRowShow) this.lastRowShow = -1;
        else this.lastRowShow = i;
        this.isClickSubRow = true;
        let _this = this;
        setTimeout(() => {
            this.isClickSubRow = false;
        }, 1000);
    }
    /**
     * set style body table by layout.body.scrolable config
     */
    get bodyStyle() {
        let style = {};
        if (this.layout.body.scrollable) {
            style = { 'max-height': this.layout.body.maxHeight + 'px', 'overflow-y': 'auto', 'overflow-x': 'hidden', 'right': '1px' };
        }
        return style;
    }

    // ------ Checkbox process ------

    /**
     * Checkbox all
     */
    checkCheckBoxAll() {
        let selected = this.data.filter(function (val) {
            return val['dt_selecter'] === true
        });
        if (selected && selected.length == this.data.length) this.isCheckBoxAll = true;
        else this.isCheckBoxAll = false;
    }

    setCheckBox(index: number) {
        this.data[index].dt_selecter = !this.data[index].dt_selecter;
        this.checkCheckBoxAll();
    }
    setCheckBoxAll() {
        this.isCheckBoxAll = !this.isCheckBoxAll;
        let checked = this.isCheckBoxAll;
        this.data = this.data.map(function (val) {
            val.dt_selecter = checked;
            return val;
        });
    }
    get dataSelected() {
        return this.data.filter(function (val) {
            return val.dt_selecter === true;
        });
    }
    get selecter() {
        return this.layout.selecter;
    }
    // ------ End checkbox process ------

    /**
     * Custom set data
     * @param data 
     */
    private setData(data: any[]) {
        if (this.layout.selecter) {
            
            this.data = data.map(function (val) {
                return Object.assign({}, val, { dt_selecter: false });
            });
            this.dataLocal =  data.map(function (val) {
                return Object.assign({}, val, { dt_selecter: false });
            });
        }
        else this.data = data;
    }
    private stringToISODateStr(str: string, format: string) {
        let strResult = "";
        let temp;
        switch (format) {
            case "dd/mm/yyyy":
                strResult = str.split("/").reverse().join('-');
                break;
            case "mm/dd/yyyy":
                temp = str.split("/");
                if (temp.length == 3) {
                    strResult = temp[2] + "/" + temp[0] + "/" + temp[1];
                }
                break;
            case "yyyy/mm/dd":
                strResult = str;
                break;
            default:
                break;
        }
        return strResult;
    }


}