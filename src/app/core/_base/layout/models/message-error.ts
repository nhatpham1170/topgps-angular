import { RuleError } from './rule-error';

export class MessageError {
    identifier: string;
    rules: RuleError[]
}