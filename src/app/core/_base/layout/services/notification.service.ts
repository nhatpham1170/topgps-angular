import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '@core/_base/crud/models/_response-body.model';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const API_ALERT = environment.api.host + "/alert/alert-rule";

@Injectable({
  providedIn:'root'
})
export class NotificationService {

  constructor(private http: HttpClient,private utils:TypesUtilsService) { }
  list(params:  any | undefined): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(API_ALERT, {params:params});
  }

  count(params:  any | undefined): Observable<ResponseBody>{
    return this.http.get<ResponseBody>(API_ALERT+'/unread/count', {params:params});
  }
 
  detail(id:number): Observable<ResponseBody>{

    return this.http.get<ResponseBody>(API_ALERT + '/'+id);
  }

}
