import { AfterViewInit, Directive, ElementRef, Input, ChangeDetectorRef, OnChanges, OnDestroy, Renderer2 } from '@angular/core';
import { Subject, timer } from 'rxjs';
import { UserDateAdvPipe } from '../pipes/user-date-adv.pipe';
import { TranslateService } from '@ngx-translate/core';
import { CurrentUserService } from '@core/auth';

export interface ToggleOptions {
	target?: string;
	targetState?: string;
	togglerState?: string;
}

/**
 * Toggle
 */
@Directive({
	selector: '[userDate]',
    exportAs: 'userDate',
    providers:[UserDateAdvPipe]
})
export class UserDateDirective implements OnChanges, OnDestroy,AfterViewInit  {
	// Public properties

    stateChanges = new Subject<any>();
    @Input() options?:{
        formatInput?: string,
        now?: string,
        nowOffset?: number,
        valueOffset?: number,
        duration?:number // s
      };
    @Input() value: any;
    @Input() format?: string ;
    @Input() interval?:number = 1000;

    private timer;
	/**
	 * Directive constructor
	 * @param el: ElementRef
	 */
    constructor(private el: ElementRef, private cd: ChangeDetectorRef,
        private render: Renderer2,
        private translate: TranslateService,
        private currentUser: CurrentUserService,
        private userDateAdv:UserDateAdvPipe
        ) { 
        // this.setContent(el.nativeElement, "abc");
        this.stateChanges.subscribe(() => {
            this.setContent(this.el.nativeElement, "abc");
            this.cd.markForCheck();
          });
          this.stateChanges.next();
        // if(this.interval == undefined) this.interval = 1000;
    }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

    ngOnChanges() {

    }
    ngOnDestroy() {
        if(this.timer){
            this.timer.unsubscribe();
            this.timer = undefined;
        }
    }
    ngAfterViewInit(): void {
        this.timer = timer(this.interval,0).subscribe(x=>{
            let strValue =  this.userDateAdv.transform(this.value,this.format,this.options);
            this.render.setProperty(this.el.nativeElement, 'innerHTML', strValue);
        });
	}
    setContent(node: any, content: string) {
        if ((node.textContent)) {
          node.textContent = content;
        } else {
          node.data = content;
        }
      }
}