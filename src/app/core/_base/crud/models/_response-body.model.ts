
export class ResponseBody {
	message: string = '';
	status: number = 0;
	result: any;   
	path:string;
	timestamp:Date;	
	error:string;	
	datetime:string;
}
