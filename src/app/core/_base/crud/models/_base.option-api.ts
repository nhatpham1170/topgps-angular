export class BaseOptionAPI {
    notifyGlobal?: boolean = false;
    module?: string = 'default';
    message?: string = '';
    params?: any = {};
    toastConfig?: any = {};
    sessionStore?: boolean = false;
    localStore?:boolean = false;
}