import { supportsPassiveEventListeners } from '@angular/cdk/platform';

// //service
export { SupportsService } from './_services/supports.service';

// //model
export { Supports } from './_model/supports';
