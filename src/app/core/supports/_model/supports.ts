import { BaseModelCT } from '@core/_base/crud/models/_base.model.ct';

export class Supports extends BaseModelCT {
    imei: string;
    raw: string;
    timestamp: number;
    timestampUpdate: number;

    constructor() {
        super();
        this.imei = '';
        this.raw = '';
        this.timestamp = 0;
        this.timestampUpdate = 0;
    }

    clear() {
        this.imei = '';
        this.raw = '';
        this.timestamp = 0;
        this.timestampUpdate = 0;
    }
}