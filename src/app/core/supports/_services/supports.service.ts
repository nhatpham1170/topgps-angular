import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';

@Injectable({
    providedIn: 'root'
  })

export class SupportsService {
    constructor(private http: HttpClient, private utils: TypesUtilsService) {}

    getRawLogs(options:BaseOptionAPI): Observable<ResponseBody> 
    {
      return this.http.get<ResponseBody>(environment.api.host + "/report/debug-log",{
        params:this.utils.processBassOptionApi(options)
      });
    }

    getDeviceLogin(options:BaseOptionAPI): Observable<ResponseBody> 
    {
      return this.http.get<ResponseBody>(environment.api.host + "/report/session-login",{
        params:this.utils.processBassOptionApi(options)
      });
    }

    getSimInfoLogs(options:BaseOptionAPI): Observable<ResponseBody> 
    {
      return this.http.get<ResponseBody>(environment.api.host + "/report/sim-log",{
        params:this.utils.processBassOptionApi(options)
      });
    }
}

