import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { UserAlias } from '../_models/user-alias';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const API_USER_ALIAS = environment.api.host + "/users/user-alias";

@Injectable({
  providedIn:'root'
})
export class UserAliasService {

  constructor(private http: HttpClient,private utils:TypesUtilsService) { }
  list(params:  any | undefined): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(API_USER_ALIAS, {params:params});
  }
 
  create(user: UserAlias, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(user).forEach(x => {
      formData.set(x, user[x]);
    });
    return this.http.post<ResponseBody>(API_USER_ALIAS, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  update(user: UserAlias, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(user).forEach(x => {
      formData.set(x, user[x]);
    });
    return this.http.put<ResponseBody>(API_USER_ALIAS + "/" + user.id, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  resetPassword(params: any | undefined,option?: BaseOptionAPI){
    let formData = new FormData();
    Object.keys(params).forEach(x => {
      formData.set(x, params[x]);
    });
    return  this.http.put(API_USER_ALIAS +'/reset-password-default/'+params.id,formData,{
      params:this.utils.processBassOptionApi(option)})
  }

  delete(user:UserAlias, option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.delete<ResponseBody>(API_USER_ALIAS + "/" + user.id + "?userId="+user.userId,{
      params:this.utils.processBassOptionApi(option),
    });
  }
}
