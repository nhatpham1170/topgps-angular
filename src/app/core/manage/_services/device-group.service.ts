import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment.prod';
import { tap } from 'rxjs/operators';
import { Device } from '../_models/device';
import { DeviceGroup } from '../_models/device-group';

const API_URL: string = environment.api.host + "/settings/device-group";
@Injectable({
  providedIn: 'root'
})
export class DeviceGroupService {

  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result.content) {
          body.result.content = body.result.content.map(function (x) {
            let tranfrom = new Device().deserialize(x, {});
            return tranfrom;
          });
        }
      }),
    );
  }
  // detail(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
  //   return this.http.get<ResponseBody>(API_URL + "/search", {
  //     params: this.utils.processBassOptionApi(option),
  //   }).pipe(
  //     tap(body => {
  //       if (body.result.content) {
  //         body.result.content = body.result.content.map(function (x) {
  //           let tranfrom = new Device().deserialize(x, {
  //             tranform: {
  //               'id': 'id',
  //               'name': 'name',
  //               description: 'description',
  //               updatedBy: '',
  //               updatedDate: 'createdAt',
  //               createdBy: '',
  //               createdDate: 'createdAt',
  //               sortOrder: '',
  //             }
  //           });
  //           return tranfrom;
  //         });

  //         body.result = body.result.content.find(x => x.id == id);

  //       }
  //     }),
  //   );
  // }
  listDeviceGroup(params: any = undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, { params: params });

  }
  detail(params: any = undefined, id): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(params).forEach(x => {
      formData.set(x, params[x]);
    });
    return this.http.get<ResponseBody>(API_URL + '/' + id, { params: params })
  }

  update(deviceGroup: DeviceGroup, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(deviceGroup).forEach(x => {
      formData.set(x, deviceGroup[x]);
    });
    return this.http.put<ResponseBody>(API_URL + "/" + deviceGroup.id, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  create(model: DeviceGroup, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("name", model.name);
    formData.append("userId", model.userId.toString());
    formData.append("deviceIds", model.deviceIds.toString());

    
    return this.http.post<ResponseBody>(API_URL, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }


  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
}
