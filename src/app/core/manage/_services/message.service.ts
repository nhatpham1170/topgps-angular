import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';
import { tap } from 'rxjs/operators';
import { message } from '../_models/message';


const API_MESSAGES_SEND = environment.api.host + "/messages/send";
const API_MESSAGES_RECEIVE = environment.api.host + "/messages/receive";

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  /** send message */
  public eventChange: EventEmitter<{ type: string, value?: any, data: any }> = new EventEmitter();
  constructor(private http: HttpClient, private utils: TypesUtilsService) { }

  listSend(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_MESSAGES_SEND, { params: params });
  }

  createSend(message: message, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(message).forEach(x => {
      formData.set(x, message[x]);
    });
    return this.http.post<ResponseBody>(API_MESSAGES_SEND, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  deleteSend(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_MESSAGES_SEND}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  // receive message
  listReceive(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_MESSAGES_RECEIVE, { params: params });
  }

  markAsRead(params: any | undefined, option?: BaseOptionAPI): Observable<ResponseBody> {
    // let formData = new FormData();
    // Object.keys(params).forEach(x => {
    //   formData.set(x, params[x]);
    // });
    // return this.http.put<ResponseBody>(environment.api.host + "/messages/mark_read/" + params.id, formData, {
    //   params: this.utils.processBassOptionApi(option),
    // }).pipe(
    //   tap(event => {
    //     this.eventChange.emit({ type: "mark_read", data: params });
    //   })
    // );
    
  
   let formData = new FormData();
    Object.keys(params).forEach(x => {
      formData.set(x, params[x]);
    });
    return this.http.put<ResponseBody>(API_MESSAGES_RECEIVE + "/mark_read/" + params.id, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  
  }

  markAllRead(params: any | undefined, option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.put<ResponseBody>(API_MESSAGES_RECEIVE + '/mark_read_all', params, {
      params: this.utils.processBassOptionApi(option),
    })
  }

  deleteReceive(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_MESSAGES_RECEIVE}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  deleteReceiveAll(option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = API_MESSAGES_RECEIVE+'/delete_all';
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  countReceiveNew(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(environment.api.host + "/messages/receive/count", { params: params });
  }

  getCountUnread(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(environment.api.host + "/notifications/unread/count", { params: params });
  }

  getListAlert(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_MESSAGES_RECEIVE, { params: params });
  }


  getCount(params:  any | undefined): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(environment.api.host + "/counts", {params:params});
  }

  alertDetail(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_MESSAGES_RECEIVE}/${id}`;
    return this.http.get<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(result => {
        if (result.result.lat) result.result.lat = Number.parseFloat(result.result.lat);
        if (result.result.lng) result.result.lng = Number.parseFloat(result.result.lng);
        if (result.result.geofenceo && result.result.geofenceo.points) result.result.geofenceo.latlngs = result.result.geofenceo.points;
      })
    );
  }
}
