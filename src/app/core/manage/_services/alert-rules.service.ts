import { AlertRule } from '@core/manage/_models/alert-rules';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { Alert } from '../_models/alert-rules';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const API_ALERT = environment.api.host + "/alert/alert-rule";
const API_EVENT_TYPE = environment.api.host + "/alert/event-groups";
const API_ALERT_HISTORY = environment.api.host + "/notifications/history";

@Injectable({
  providedIn: 'root'
})
export class AlertRulesService {

  private eventTypeConfigs: Array<{ name: string,id:number, template: any,group:string }> = [
    {      
      id:3,
      name: "battery_low",
      group:"device",
      template: {
        percent: 20,
        interval: "000000",
      },
    },
    {
      id:20,
      name: "battery_hight",
      group:"device",
      template: {
        percent: 80,
        interval: "000000",
      }
    },
   
    {
      id:22,
      name: "external_power_hight",
      group:"device",
      template: {
        powerVol: 14,
        interval: "000000",
      }
    },
    {
      id:21,
      name: "external_power_low",
      group:"device",
      template: {
        powerVol: 12,
        interval: "000000",
      }
    },
   
    {
      id:2,
      name: "external_power_cut",
      group:"device",
      template: {
        // percent: 20,
        // interval: "000000",
      }
    },
    {
      id:11,
      name: "external_power_connected",
      group:"device",
      template: {
        // percent: 20,
        // interval: "000000",
      }
    },
    {
      id:4,
      name:"sos",
      group:"action",
      template:{}
    },
    {
      id:5,
      name:"geofence_in",
      group:"in_out",
      template:{}
    },
    {
      id:6,
      name:"geofence_out",
      group:"in_out",
      template:{}
    },
    {
      id:12,
      name:"moving",
      group:"action",
      template:{}
    },
    {
      id:13,
      name:"door_open",
      group:"action",
      template:{}
    },
    {
      id:14,
      name:"door_close",
      group:"action",
      template:{}
    },    
    {
      id:100,
      name:"trip_deviation",
      group:"action",
      template:{
        interval: "000500",
      }
    },  
    {
      id:101,
      name:"thieves_fuel",
      group:"action",
      template:{
        lostFuel:30,
        interval: "000500",
      }
    },   
    {
      id:101,
      name:"comming_geofence",
      group:"action",
      template:{
        distance:500,
        interval: "000500",

      }
    },  
    {
      id:7,
      name: "overspeed",
      group:"action",
      template: {
        speed: 70,
        interval: "000500",
      }
    },
    
    {
      id:19,
      name: "stop_overtime",
      group:"action",
      template: {
        time: "010000",
        interval: "010000",
      }
    },
    {
      id:23,
      group:"OBD",
      name:"vin_number_change",
      template:{}
    },
  ];
  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_ALERT, { params: params });
  }

  getEventType(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_EVENT_TYPE, { params: params });
  }
  eventTypes(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_EVENT_TYPE, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  create(alert: Alert, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("name", alert.nameAlert);
    formData.append("description", alert.description);
    formData.append("eventType", alert.eventType.toString());
    formData.append("textPrimary", alert.textPrimary);
    formData.append("suspended", alert.suspended.toString());
    formData.append("geofenceIds", alert.geofenceIds.toString());
    formData.append("deviceIds", alert.deviceIds.toString());
    formData.append("extendedParams", JSON.stringify(alert.extendedParams));
    formData.append("parameterValue", JSON.stringify(alert.parameterValue));
    formData.append("pushSetting", JSON.stringify(alert.pushSetting));
    formData.append("schedule", JSON.stringify(alert.schedule));
    return this.http.post<ResponseBody>(API_ALERT, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  createAlertRule(alert: AlertRule, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("name", alert.name || "");
    formData.append("description", alert.description || "");
    formData.append("eventType", alert.eventType.toString());
    formData.append("textPrimary", alert.textPrimary || "");
    formData.append("suspended",  alert.suspended?"1":"0");
    formData.append("geofenceIds",  alert.geofenceIds.join(","));
    formData.append("deviceIds", alert.deviceIds.join(","));
    formData.append("extendedParams", JSON.stringify(alert.extendedParams));
    formData.append("parameterValue", JSON.stringify(alert.parameterValue));
    formData.append("pushSetting", JSON.stringify(alert.pushSetting));
    formData.append("schedule", JSON.stringify(alert.schedule));
    formData.append("userId", alert.userId.toString());

    return this.http.post<ResponseBody>(API_ALERT, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  updateAlertRule(alert: AlertRule, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("name", alert.name || "");
    formData.append("description", alert.description || "");
    formData.append("eventType", alert.eventType.toString());
    formData.append("textPrimary", alert.textPrimary || "");
    formData.append("suspended",  alert.suspended?"1":"0");
    formData.append("geofenceIds",  alert.geofenceIds.join(","));
    formData.append("deviceIds", alert.deviceIds.join(","));
    formData.append("extendedParams", JSON.stringify(alert.extendedParams));
    formData.append("parameterValue", JSON.stringify(alert.parameterValue));
    formData.append("pushSetting", JSON.stringify(alert.pushSetting));
    formData.append("schedule", JSON.stringify(alert.schedule));
    formData.append("userId", alert.userId.toString());

    return this.http.put<ResponseBody>(`${API_ALERT}/${alert.id}`, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  changeLock(alert: Alert, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    formData.append("suspended", alert.suspended.toString());
    return this.http.put<ResponseBody>(API_ALERT + "/" + alert.id, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  detail(id: number): Observable<ResponseBody> {

    return this.http.get<ResponseBody>(API_ALERT + '/' + id);
  }

  getCountUnread(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(environment.api.host + "/notifications/unread/count", { params: params });
  }

  getListAlert(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_ALERT_HISTORY, { params: params });
  }

  markAsRead(params: any | undefined, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(params).forEach(x => {
      formData.set(x, params[x]);
    });
    return this.http.put<ResponseBody>(API_ALERT_HISTORY + '/mark_read/' + params.id, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  markAllRead(params: any | undefined, option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.put<ResponseBody>(API_ALERT_HISTORY + '/mark_read_all', params, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  update(alert: Alert, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    formData.append("name", alert.nameAlert);
    formData.append("description", alert.description);
    formData.append("eventType", alert.eventType.toString());
    formData.append("textPrimary", alert.textPrimary);
    formData.append("suspended", alert.suspended.toString());
    formData.append("geofenceIds", alert.geofenceIds.toString());
    formData.append("deviceIds", alert.deviceIds.toString());
    formData.append("extendedParams", JSON.stringify(alert.extendedParams));
    formData.append("parameterValue", JSON.stringify(alert.parameterValue));
    formData.append("pushSetting", JSON.stringify(alert.pushSetting));
    formData.append("schedule", JSON.stringify(alert.schedule));
    return this.http.put<ResponseBody>(API_ALERT + "/" + alert.id, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_ALERT}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  get configTemplate(){
    return JSON.parse(JSON.stringify( this.eventTypeConfigs));
  } 
}
