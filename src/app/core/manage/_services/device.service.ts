import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment.prod';
import { tap } from 'rxjs/operators';
import { Device } from '../_models/device';

const API_URL = environment.api.host + "/settings/devices";
const API_URL_FAVORITE = environment.api.host + "/settings/devices/favorite";
const API_SETTING = environment.api.host + "/settings";

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result.content) {
          body.result.content = body.result.content.map(function (x) {
            let tranfrom = new Device().deserialize(x, {});
            return tranfrom;
          });
        }
      }),
    );
  }
  detail(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL + "/search", {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result.content) {
          body.result.content = body.result.content.map(function (x) {
            let tranfrom = new Device().deserialize(x, {
              tranform: {
                'id': 'id',
                'name': 'name',
                description: 'description',
                updatedBy: '',
                updatedDate: 'createdAt',
                createdBy: '',
                createdDate: 'createdAt',
                sortOrder: '',
              }
            });
            return tranfrom;
          });

          body.result = body.result.content.find(x => x.id == id);

        }
      }),
    );
  }

  create(model: Device, option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.post<ResponseBody>(API_URL, model, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  update(device: Device, option?: BaseOptionAPI): Observable<ResponseBody> {
    let model = new Device();
    model = this.utils.transformObject(device, model);
    // model = Object.assign({}, model, device);
    const formData = new FormData();
    // formData.append("id", model.id.toString());
    formData.append("imei", model.imei);
    // formData.append("userId", model.userId.toString());
    formData.append("deviceName", model.name);
    formData.append("simNumber", model.simno);
    formData.append("simType", model.simType.toString());
    formData.append("icon", model.icon);
    if (model.groupIds != []) formData.append("groupIds", model.groupIds.toString());
    formData.append("activeStatus", model.active.toString());
    formData.append("activeCustomer", model.activeWarranty.toString());
    formData.append("description", model.description);
    formData.append("descriptionAdmin", model.descAdmin);
    formData.append("isSell", model.isSell.toString());
    formData.append("sellAt", model.sellAt);
    if(model.serviceExpire)
      formData.append("serviceExpire", model.serviceExpire);

    formData.append("deviceType", model.typeDevice.toString());
    if (model.driver != 0) formData.append("driverId", model.driver.toString());
    formData.append("transportType", model.transportType.toString());
    formData.append("numberPlate", model.numberPlate.toString());
    formData.append("frameNumber", model.frameNumber.toString());
    formData.append("vinNumber", model.vinNumber.toString());
    formData.append("machineCode", model.machineCode);
    formData.append("isGovernment", model.sendToGovernment.toString());
    formData.append("transportTypeQcvn", model.transportTypeQcvn.toString());
    formData.append("extensions", JSON.stringify(model.extensions));
    formData.append("isDebug", model.isDebug.toString());
    formData.append("usingTollFee", model.usingTollFee.toString());
    if (model.governmentManage != '') formData.append("governmentId", model.governmentManage.toString());
    formData.append("coefficient", model.coefficient.toString());
    const url = `${API_URL}/${model.id}`;
    return this.http.put<ResponseBody>(url, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  getAllConfig(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL + "/config", {
      params: this.utils.processBassOptionApi(option),
    });
  }
  getGroupDevice(userId: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    // /settings/device-group?userid={xx}
    if (!option) option = new BaseOptionAPI();
    option.params = { user_id: userId };
    return this.http.get<ResponseBody>(API_SETTING + "/device-group", {
      params: this.utils.processBassOptionApi(option),
    });
  }
  changeStatus(params: { ids: number[], status: number }, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("deviceIds", params.ids.join(','));
    formData.append("status", params.status.toString());

    return this.http.put<ResponseBody>(API_URL + "/change-status", formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  moves(params: { ids: number[], userId: number }, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("deviceIds", params.ids.join(','));
    formData.append("userId", params.userId.toString());

    return this.http.put<ResponseBody>(API_URL + "/moves", formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  sell(params: { id: number }, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("id", params.id.toString());

    return this.http.put<ResponseBody>(API_URL + "/sell", formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  sendCommand(params: { ids: number[], commandStr: string, commandName?:string }, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("ids", params.ids.join(','));
    formData.append("commandText", params.commandStr);
    formData.append("commandName", params.commandName);

    return this.http.post<ResponseBody>(API_SETTING + "/commands", formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  renews(params: { ids: number[], dateRenews: string, userId: number }, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("dateExtension", params.dateRenews);
    formData.append("deviceIds", params.ids.join(','));
    formData.append("userIdExtend", params.userId.toString());

    return this.http.put<ResponseBody>(API_URL + "/extension", formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  imporDevice(params: { devices: Array<any>, deviceTypeId?: number, iconTypeId?: number, simTypeId?: number, userId?: number }, option?: BaseOptionAPI): Observable<ResponseBody> {
    params.devices = params.devices.map(x => { return JSON.stringify(x); })
    const formData = new FormData();
    formData.append("devices", "[" + params.devices.join(';') + "]");
    if (params.deviceTypeId) formData.append("devType", params.deviceTypeId.toString());
    if (params.iconTypeId) formData.append("icon", params.iconTypeId.toString());
    if (params.simTypeId) formData.append("simType", params.simTypeId.toString());
    if (params.userId) formData.append("userId", params.userId.toString());

    return this.http.post<ResponseBody>(API_URL, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  sortOrder(params: { id: number, sortOrder: number }[], option?: BaseOptionAPI): Observable<ResponseBody> {
    let data = params.map(x => JSON.stringify(x)).join(';');
    const formData = new FormData();
    formData.append("data", data);

    return this.http.put<ResponseBody>(API_URL + "/sort-order", formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  favoriteList(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL_FAVORITE, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result.content) {
          body.result.content = body.result.content.map(function (x) {
            let tranfrom = new Device().deserialize(x, {});
            return tranfrom;
          });
        }
      }),
    );
  }

  favoriteCreate(favorite:{deviceId:number},option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("deviceId", favorite.deviceId.toString());    
    return this.http.post<ResponseBody>(API_URL_FAVORITE, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  favoriteDelete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL_FAVORITE}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  favoriteDeleteByDeviceId(option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL_FAVORITE}`;
    
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  searchGlobal(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(`${API_URL}/search-global`, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result.content) {
          body.result.content = body.result.content.map(function (x) {
            let tranfrom = new Device().deserialize(x, {});
            return tranfrom;
          });
        }
      }),
    );
  }

  // searchGlobal(option?: BaseOptionAPI): Observable<ResponseBody> {
  //   return this.http.get<ResponseBody>(`${API_URL}/search-global`, {
  //     params: this.utils.processBassOptionApi(option),
  //   }).pipe(
  //     tap(body => {
  //       if (body.result.content) {
  //         body.result.content = body.result.content.map(function (x) {
  //           let tranfrom = new Device().deserialize(x, {});
  //           return tranfrom;
  //         });
  //       }
  //     }),
  //   );
  // }

}

