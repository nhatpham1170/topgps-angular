import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { TypesUtilsService } from '@core/_base/crud';
import { Role } from '@core/auth/_models/role.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ResponseBody } from '@core/_base/crud/models/_response-body.model';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';

const API_ROLES_URL = environment.api.host + "/roles-alias";

@Injectable({
  providedIn:'root'
})
export class RoleAliasService {
    constructor(
        private http: HttpClient, 
       private utils:TypesUtilsService
       ) { }
        // api role
        searchRoles(params:  any | undefined):Observable<ResponseBody>{
            return this.http.get<ResponseBody>(environment.api.host+'/roles/search', { params: params});
        }

        
        getRoleById(roleId: number):Observable<ResponseBody> {
            return this.http.get<ResponseBody>(API_ROLES_URL+ `/${roleId}`);
        }

        getListRoles(params:  any | undefined):Observable<ResponseBody>{
            return this.http.get<ResponseBody>(API_ROLES_URL, { params: params});
        }

        getListPermisson( option?: BaseOptionAPI):Observable<ResponseBody>{
            return this.http.get<ResponseBody>(environment.api.host+'/roles-alias/0',{
                params:this.utils.processBassOptionApi(option),
            });
        }

        addRole(Role : Role, option?: BaseOptionAPI):Observable<ResponseBody>{
            let formData = new FormData();
            Object.keys(Role).forEach(x => {
              formData.set(x, Role[x]);
            });
            return this.http.post<ResponseBody>(API_ROLES_URL, formData,{
                params:this.utils.processBassOptionApi(option),
            });
        } 

        updateRoleById(Role : Role, option?: BaseOptionAPI):Observable<ResponseBody>{
            let formData = new FormData();
            Object.keys(Role).forEach(x => {
              formData.set(x, Role[x]);
            });
        return this.http.put<ResponseBody>(API_ROLES_URL+'/'+Role.id, formData,{
            params:this.utils.processBassOptionApi(option),
        });
        }

        deleteRoleById(roleId: number, option?: BaseOptionAPI):Observable<ResponseBody>{
        return  this.http.delete<ResponseBody>(API_ROLES_URL+'/'+roleId,{
            params:this.utils.processBassOptionApi(option),
        });
        }  

}