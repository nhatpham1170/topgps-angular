import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment.prod';
import { tap } from 'rxjs/operators';
import { Device } from '../_models/device';
import { SensorParameter } from '../_models/sensor-parameter';
import { Sensor } from '../_models/sensor';

const API_URL = environment.api.host + "/settings/sensors";
// fuel, temperature, humid, pressure, other 
// ['icon-tag', 'icon-thermometer-half', 'icon-gas-station', 'icon-tank', 'icon-tachometer-alt', "icon-manometer", "icon-odometer", "icon-route"]
const TYPE_SENSORS: Array<TypeSensor> = [
  {
    type: "fuel",
    icon: "icon-gas-station",
    translate: "SENSOR_TYPE.FUEL"
  },
  {
    type: "temperature",
    icon: "icon-thermometer-half",
    translate: "SENSOR_TYPE.TEMPERATURE"
  },
  {
    type: "humid",
    icon: "icon-cloud-rain",
    translate: "SENSOR_TYPE.HUMID"
  },
  {
    type: "pressure",
    icon: "icon-manometer",
    translate: "SENSOR_TYPE.PRESSURE"
  },
  {
    type: "other",
    icon: "icon-tag",
    translate: "SENSOR_TYPE.OTHER"
  },
]

@Injectable({
  providedIn: 'root'
})
export class SensorService {

  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result.content) {
          body.result.content = body.result.content.map(function (x) {
            let tranfrom = new Device().deserialize(x, {});
            return tranfrom;
          });
        }
      }),
    );
  }
  detail(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL + "/search", {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result.content) {
          body.result.content = body.result.content.map(function (x) {
            let tranfrom = new Device().deserialize(x, {
              tranform: {
                'id': 'id',
                'name': 'name',
                description: 'description',
                updatedBy: '',
                updatedDate: 'createdAt',
                createdBy: '',
                createdDate: 'createdAt',
                sortOrder: '',
              }
            });
            return tranfrom;
          });

          body.result = body.result.content.find(x => x.id == id);

        }
      }),
    );
  }

  create(model: Sensor, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("id", model.id.toString());
    formData.append("name", model.name);
    formData.append("sortOrder", model.sortOrder.toString());
    formData.append("calculationType", model.calculationType);
    formData.append("deviceId", model.deviceId.toString());
    formData.append("description", model.description);
    formData.append("parameters", JSON.stringify(model.parameters));

    return this.http.post<ResponseBody>(API_URL, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  update(model: Sensor, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("id", model.id.toString());
    formData.append("name", model.name);
    formData.append("sortOrder", model.sortOrder.toString());
    formData.append("calculationType", model.calculationType);
    formData.append("deviceId", model.deviceId.toString());
    formData.append("description", model.description);
    formData.append("parameters", JSON.stringify(model.parameters));

    return this.http.put<ResponseBody>(API_URL + "/" + model.id, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  // sensors
  getSensors(deviceId: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    if (!option) option = new BaseOptionAPI();
    option.params = { deviceId: deviceId };
    return this.http.get<ResponseBody>(API_URL, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(data => {
        data.result.content = data.result.content.map(x => {

          return x;
        })
      })
    );
  }
  // sensors
  getAllConfigSensor(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL + "/all-sensor-template", {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(data => {
        data.result.templates = data.result.templates.map(x => {
          return x;
        })
      })
    );
  }

  getAllTypeSensor() {
    return [...TYPE_SENSORS];
  }

  sortOrder(params: { id: number, sortOrder: number }[], option?: BaseOptionAPI): Observable<ResponseBody> {
    let data = params.map(x => JSON.stringify(x)).join(';');
    const formData = new FormData();
    formData.append("data", data);

    return this.http.put<ResponseBody>(API_URL + "/sort-order", formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
}
export class TypeSensor {
  type: string;
  icon: string;
  translate: string;
}
