import { string } from '@amcharts/amcharts4/core';
import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { Alert } from '../_models/alert-rules';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';
import { tap, map } from 'rxjs/operators';

const API_ALERT = environment.api.host + "/alert/alert-rule";
const API_ALERT_HISTORY = environment.api.host + "/notifications/history";

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  /** seen_device | seen_all */
  public eventChange: EventEmitter<{ type: string, value?: any, data: any }> = new EventEmitter();
  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_ALERT, { params: params });
  }

  changeLock(alert: Alert, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    formData.append("suspended", alert.suspended.toString());
    return this.http.put<ResponseBody>(API_ALERT + "/" + alert.id, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  detail(id: number): Observable<ResponseBody> {

    return this.http.get<ResponseBody>(API_ALERT_HISTORY + '/' + id);
  }

  countNew(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(environment.api.host + "/notifications/unseen/count", { params: params });
  }

  getCountUnread(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(environment.api.host + "/notifications/unread/count", { params: params });
  }

  getListAlert(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_ALERT_HISTORY, { params: params }).pipe(
      tap(data=>{
        if(data.result["content"]){
          data.result.content = data.result.content.map(x=>{
            x["eventKey"] = "MANAGE.ALERT_RULE.TYPE_ALERT." + (x["eventType"] || "").toString().toUpperCase();
            return x;
          });
        }        
      })
    );
  }
  markAsRead(params: any | undefined, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(params).forEach(x => {
      formData.set(x, params[x]);
    });
    return this.http.put<ResponseBody>(API_ALERT_HISTORY + '/mark_read/' + params.id, formData, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(event => {
        this.eventChange.emit({ type: "mark_read", data: params });
      })
    );
  }

  markAllRead(params: any | undefined, option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.put<ResponseBody>(API_ALERT_HISTORY + '/mark_read_all', params, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(event => {
        if (params.isRead == undefined) params.isRead = 1;
        this.eventChange.emit({ type: "mark_read_all", data: params });
      })
    );
  }
  // markAsRead(params: any | undefined, option?: BaseOptionAPI): Observable<ResponseBody> {
  //   let formData = new FormData();
  //   Object.keys(params).forEach(x => {
  //     formData.set(x, params[x]);
  //   });
  //   return this.http.put<ResponseBody>(API_ALERT_HISTORY + '/mark_read/' + params.id, formData, {
  //     params: this.utils.processBassOptionApi(option),
  //   }).pipe(
  //     tap(event => {
  //       this.eventChange.emit({ type: "mark_read", data: params });
  //     })
  //   );
  // }

  // markAllRead(params: any | undefined, option?: BaseOptionAPI): Observable<ResponseBody> {
  //   return this.http.put<ResponseBody>(API_ALERT_HISTORY + '/mark_read_all', params, {
  //     params: this.utils.processBassOptionApi(option),
  //   }).pipe(
  //     tap(event => {
  //       this.eventChange.emit({ type: "mark_read_all", data: params });
  //     })
  //   );
  // }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_ALERT}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  alertDetail(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_ALERT_HISTORY}/${id}`;
    return this.http.get<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(result => {
        if (result.result.lat) result.result.lat = Number.parseFloat(result.result.lat);
        if (result.result.lng) result.result.lng = Number.parseFloat(result.result.lng);
        if (result.result.geofenceo && result.result.geofenceo.points) result.result.geofenceo.latlngs = result.result.geofenceo.points;
      })
    );
  }
}
