import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment.prod';
import { tap } from 'rxjs/operators';
import { Device } from '../_models/device';
import { DeviceGroup } from '../_models/device-group';
import { options } from '@amcharts/amcharts4/core';

const API_URL: string = environment.api.host + "/points";
@Injectable({
  providedIn: 'root'
})
export class PointService {

  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  add(addPointModel: AddPointModel, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    formData.append("point", addPointModel.point.toString());
    formData.append("description", addPointModel.description || "");
    formData.append("amount", addPointModel.amount.toString());
    return this.http.put<ResponseBody>(API_URL + "/add", formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  move(movePointModel: MovePointModel, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    formData.append("point", movePointModel.point.toString());
    formData.append("userId", movePointModel.userId.toString());
    formData.append("status", movePointModel.isPay.toString());
    formData.append("payAt", movePointModel.payAt || "");
    formData.append("amount", movePointModel.amount.toString() || "0");
    formData.append("description", movePointModel.description || "");
    if (movePointModel.billImg) formData.append("billImg", movePointModel.billImg);

    return this.http.put<ResponseBody>(API_URL + "/move", formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  edit(editTransactionModel: EditTransactionModel, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    formData.append("status", editTransactionModel.isPay.toString());
    formData.append("payAt", editTransactionModel.payAt || "");
    formData.append("description", editTransactionModel.description || "");
    formData.append("amount", editTransactionModel.amount ? editTransactionModel.amount.toString() : '0');
    if (editTransactionModel.billImg) formData.append("billImg", editTransactionModel.billImg);
    if (editTransactionModel.billImgRemove != undefined && editTransactionModel.billImgRemove.length>0) {
      formData.append('billImgRemove',JSON.stringify(editTransactionModel.billImgRemove));
    }
    
    return this.http.put<ResponseBody>(`${API_URL}/${'transaction'}/${editTransactionModel.id}`, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  extend(extend: ExtendModel, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    formData.append("month", extend.month.toString());
    formData.append("deviceIds", extend.deviceIds.join(","));
    formData.append("userIdExtend", extend.userIdExtend.toString());
    formData.append("isPay", extend.isPay.toString());
    formData.append("description", extend.description || "");
    formData.append("amount", extend.amount.toString() || "0");     
    formData.append("isDateNow", (!extend.usedOldService).toString() || 'true');
    if (extend.billImg) formData.append("billImg", extend.billImg);
    if (extend.payAt) formData.append("payAt", extend.payAt);
    return this.http.put<ResponseBody>(API_URL + "/extend", formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  list(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result.content) {
          body.result.content = body.result.content.map(function (x) {
            let tranfrom = new Device().deserialize(x, {});
            return tranfrom;
          });
        }
      }),
    );
  }
  cancel(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    return this.http.put<ResponseBody>(`${API_URL}/${'transaction/cancel'}/${id}`, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  requestCancel(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    return this.http.put<ResponseBody>(`${API_URL}/${'transaction/request-cancel'}/${id}`, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  transactionLog(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(`${API_URL}/${'transaction-history'}`, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  transactionHistory(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(`${API_URL}/${'transaction'}`, {
      params: this.utils.processBassOptionApi(option),
    });
  } 
  extendHistory(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(`${API_URL}/${'extend-history'}`, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  synthesisReport(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(`${API_URL}/${'transaction/synthesis-report'}`, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  // listDeviceGroup(params: any = undefined): Observable<ResponseBody> {
  //   return this.http.get<ResponseBody>(API_URL, { params: params });

  // }
  // detail(params: any = undefined, id): Observable<ResponseBody> {
  //   let formData = new FormData();
  //   Object.keys(params).forEach(x => {
  //     formData.set(x, params[x]);
  //   });
  //   return this.http.get<ResponseBody>(API_URL + '/' + id, { params: params })
  // }

  // update(deviceGroup: DeviceGroup, option?: BaseOptionAPI): Observable<ResponseBody> {
  //   let formData = new FormData();
  //   Object.keys(deviceGroup).forEach(x => {
  //     formData.set(x, deviceGroup[x]);
  //   });
  //   return this.http.put<ResponseBody>(API_URL + "/" + deviceGroup.id, formData, {
  //     params: this.utils.processBassOptionApi(option),
  //   });
  // }

  // create(model: DeviceGroup, option?: BaseOptionAPI): Observable<ResponseBody> {
  //   const formData = new FormData();
  //   formData.append("name", model.name);
  //   formData.append("userId", model.userId.toString());
  //   return this.http.post<ResponseBody>(API_URL, formData, {
  //     params: this.utils.processBassOptionApi(option),
  //   });
  // }


  // delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
  //   const url = `${API_URL}/${id}`;
  //   return this.http.delete<ResponseBody>(url, {
  //     params: this.utils.processBassOptionApi(option),
  //   });
  // }
}
export class ExtendModel {
  month: number;
  deviceIds: Array<number>;
  userIdExtend: number;
  isPay: number;
  description: string;
  userObjId?: number;
  amount?: number;
  data?: any;
  billImg?: any;
  usedOldService?:boolean;
  payAt?: string;
}
export class MovePointModel {
  userId: number;
  point: number;
  isPay: number;
  description: string;
  payAt?: string;
  amount?: number
  billImg?: any;
}

export class EditTransactionModel {
  id: number;
  isPay: number;
  description: string;
  payAt?: string;
  amount?: number
  billImg?: any;
  billImgRemove?:any;
}

export class AddPointModel {
  point: number;
  description: string;
  amount?: number
}
