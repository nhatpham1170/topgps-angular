import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { DeviceCommand } from '../_models/device-command';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const API_DEVICE_COMMAND = environment.api.host + "/settings/commands";

@Injectable({
  providedIn:'root'
})
export class DeviceCommandService {

  constructor(private http: HttpClient,private utils:TypesUtilsService) { }
  list(params:  any | undefined): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(API_DEVICE_COMMAND, {params:params});
  }
 
  create(deviceCommand: DeviceCommand, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();   
    formData.append("commandName", deviceCommand.commandName);
    formData.append("commandText", deviceCommand.commandText);
    formData.append("devices", JSON.stringify(deviceCommand.devices));
    return this.http.post<ResponseBody>(API_DEVICE_COMMAND, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  update(deviceCommand: DeviceCommand, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(deviceCommand).forEach(x => {
      formData.set(x, deviceCommand[x]);
    });
    return this.http.put<ResponseBody>(API_DEVICE_COMMAND + "/" + deviceCommand.id, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_DEVICE_COMMAND}/${id}`;
    return this.http.delete<ResponseBody>(url,{
      params:this.utils.processBassOptionApi(option),
    });
  }
}
