import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { UserAlias } from '../_models/user-alias';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const URL = 'http://192.168.1.67:7200/wp-json/wp/v2/posts';
const API_ALERT = environment.api.host + "/alert/alert-rule";

@Injectable({
  providedIn:'root'
})
export class HelpService {

  constructor(private http: HttpClient,private utils:TypesUtilsService) { }
  listVersion() {    
      return this.http.get('http://192.168.1.67:7200/wp-json/wp/v2/posts?categories=7');
  }

}
