import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment.prod';
import { tap } from 'rxjs/operators';
import { Device } from '../_models/device';
import { DeviceGroup } from '../_models/device-group';
import { Driver } from '../_models/driver';

const API_URL:string = environment.api.host + "/settings/drivers";
@Injectable({
  providedIn: 'root'
})
export class DriverService {
  
  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result.content) {
          body.result.content = body.result.content.map(function (x) {
            let tranfrom = new Driver().deserialize(x, {});
            return tranfrom;
          });
        }
      }),
    );
  }

  listDriver(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, { params: params });
  }
  detail(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL + "/search", {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result.content) {
          body.result.content = body.result.content.map(function (x) {
            let tranfrom = new Device().deserialize(x, {
              tranform: {
                'id': 'id',
                'name': 'name',
                description: 'description',
                updatedBy: '',
                updatedDate: 'createdAt',
                createdBy: '',
                createdDate: 'createdAt',
                sortOrder: '',
              }
            });
            return tranfrom;
          });

          body.result = body.result.content.find(x => x.id == id);

        }
      }),
    );
  }

  create(driver: Driver, option?: BaseOptionAPI): Observable<ResponseBody> {
    let model = new Driver();
    model = this.utils.transformObject(driver,model);
    const formData = new FormData();
    formData.append("name", model.name);
    formData.append("phone", model.phone);
    formData.append("email", model.email);
    formData.append("driverCode", model.driverCode);
    formData.append("licenseNumber", model.licenseNumber);
    formData.append("beginDate", model.beginDate);
    formData.append("expireDate", model.expireDate);
    formData.append("address", model.address);
    formData.append("description", model.description);
    formData.append("userId", model.userId.toString());

    return this.http.post<ResponseBody>(API_URL, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  update(driver: Driver, option?: BaseOptionAPI): Observable<ResponseBody> {
    let model = new Driver();
    const url = `${API_URL}/${driver.id}`;
    model = this.utils.transformObject(driver,model);
    const formData = new FormData();
    formData.append("name", model.name);
    formData.append("phone", model.phone);
    formData.append("email", model.email);
    formData.append("driverCode", model.driverCode);
    formData.append("licenseNumber", model.licenseNumber);
    formData.append("beginDate", model.beginDate);
    formData.append("expireDate", model.expireDate);
    formData.append("address", model.address);
    formData.append("description", model.description);
    formData.append("userId", model.userId.toString());

    return this.http.put<ResponseBody>(url, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
}