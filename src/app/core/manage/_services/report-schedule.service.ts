import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { ReportSchedule } from '../_models/report-schedule';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const API_SCHEDULE_REPORT = environment.api.host + "/scheduled-report";

@Injectable({
  providedIn:'root'
})
export class ReportScheduleService {

  constructor(private http: HttpClient,private utils:TypesUtilsService) { }
  list(params:  any | undefined): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(API_SCHEDULE_REPORT, {params:params});
  }

  detail(params:any): Observable<ResponseBody> {  

    return this.http.get<ResponseBody>(API_SCHEDULE_REPORT+'/'+params.id, {params:params});
  }
 
  create(scheduleReport: ReportSchedule, option?: BaseOptionAPI): Observable<ResponseBody> {

    const formData = new FormData();   
    formData.append("userId", scheduleReport.userId.toString());
    formData.append("name", scheduleReport.name);
    formData.append("pushSetting", JSON.stringify(scheduleReport.pushSetting));
    formData.append("typeReport", scheduleReport.typeReport.toString());
    formData.append("formatReport", scheduleReport.formatReport);
    formData.append("schedule",  JSON.stringify(scheduleReport.schedule));
    formData.append("deviceIds", scheduleReport.deviceIds.toString());
    return this.http.post<ResponseBody>(API_SCHEDULE_REPORT, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  update(scheduleReport: ReportSchedule, option?: BaseOptionAPI): Observable<ResponseBody> {
    
    const formData = new FormData();   
    formData.append("userId", scheduleReport.userId.toString());
    formData.append("name", scheduleReport.name);
    formData.append("pushSetting", JSON.stringify(scheduleReport.pushSetting));
    formData.append("typeReport", scheduleReport.typeReport.toString());
    formData.append("formatReport", scheduleReport.formatReport);
    formData.append("schedule",  JSON.stringify(scheduleReport.schedule));
    formData.append("deviceIds", scheduleReport.deviceIds.toString());

    return this.http.put<ResponseBody>(API_SCHEDULE_REPORT + "/" + scheduleReport.id, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_SCHEDULE_REPORT}/${id}`;
    return this.http.delete<ResponseBody>(url,{
      params:this.utils.processBassOptionApi(option),
    });
  }
}
