import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment';
import { tap } from 'rxjs/operators';
import { Device } from '../_models/device';
import { DeviceGroup } from '../_models/device-group';
import { Driver } from '../_models/driver';
import { GeofenceModel } from '../_models/geofence';

const API_URL: string = environment.api.host + "/geofences";
@Injectable({
  providedIn: 'root'
})
export class GeofenceService {

  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      // tap(body => {
      //   if (body.result.content) {
      //     body.result.content = body.result.content.map(function (x) {
      //       let tranfrom = new Driver().deserialize(x, {});
      //       return tranfrom;
      //     });
      //   }
      // }),
    );
  }
  detail(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`;
    return this.http.get<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        body.result["latlngs"] = [...body.result.points];
        body.result["points"] = body.result.points.map(x => [x.lat, x.lng]);

      })
    );
  }

  create(geofence: GeofenceModel, option?: BaseOptionAPI): Observable<ResponseBody> {
    let model = new GeofenceModel();
    model = this.utils.transformObject(geofence, model);
    const formData = new FormData();
    formData.append("userId", model.userId.toString());
    formData.append("name", model.name);
    formData.append("type", model.type);
    formData.append("description", model.description);
    formData.append("active", model.active.toString());
    formData.append("radius", model.radius.toString());
    formData.append("color", model.color);
    formData.append("encodedPoints", model.encodedPoints);
    formData.append("points", "[" + model.latlngs.map(x => { return JSON.stringify(x) }).join(",") + "]");
    formData.append("fill", model.fill.toString());
    formData.append("fillColor", model.fillColor);
    formData.append("fillOpacity", model.fillOpacity.toString());
    formData.append("opacity", model.opacity.toString());
    formData.append("stroke", model.stroke.toString());
    formData.append("weight", model.weight.toString());
    formData.append("groupIds", model.groupIds.toString());

    formData.append("neLat", model.neLat.toString());
    formData.append("neLng", model.neLng.toString());
    formData.append("swLat", model.swLat.toString());
    formData.append("swLng", model.swLng.toString());
    return this.http.post<ResponseBody>(API_URL, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  update(geofence: GeofenceModel, option?: BaseOptionAPI): Observable<ResponseBody> {
    let model = new GeofenceModel();
    model = this.utils.transformObject(geofence, model);
    const formData = new FormData();
    const url = `${API_URL}/${model.id}`;
    formData.append("userId", model.userId.toString());
    formData.append("name", model.name);
    formData.append("type", model.type);
    formData.append("description", model.description);
    formData.append("active", model.active.toString());
    formData.append("radius", model.radius.toString());
    formData.append("color", model.color);
    formData.append("encodedPoints", model.encodedPoints);
    formData.append("points", "[" + model.latlngs.map(x => { return JSON.stringify(x) }).join(",") + "]");
    formData.append("fill", model.fill.toString());
    formData.append("fillColor", model.fillColor);
    formData.append("fillOpacity", model.fillOpacity.toString());
    formData.append("opacity", model.opacity.toString());
    formData.append("stroke", model.stroke.toString());
    formData.append("weight", model.weight.toString());
    formData.append("groupIds",model.groupIds.toString());

    formData.append("neLat", model.neLat.toString());
    formData.append("neLng", model.neLng.toString());
    formData.append("swLat", model.swLat.toString());
    formData.append("swLng", model.swLng.toString());
    return this.http.put<ResponseBody>(url, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  changeActive(params: { id: number, active: number }, option?: BaseOptionAPI) {

    const formData = new FormData();
    const url = API_URL+'/change-status/'+params.id;
    formData.append("active", params.active.toString());

    return this.http.put<ResponseBody>(url, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }


  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
}