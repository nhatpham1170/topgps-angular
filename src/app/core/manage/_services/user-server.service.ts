import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment.prod';
import { tap } from 'rxjs/operators';
import { Device } from '../_models/device';
import { DeviceGroup } from '../_models/device-group';
import { Driver } from '../_models/driver';
import { UserServerModel } from '../_models/user-server.model';

const API_URL:string = environment.api.host + "/settings/user-server";
@Injectable({
  providedIn: 'root'
})
export class UserServerService {
  
  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, {
      params: this.utils.processBassOptionApi(option),
    });    
  }

  detail(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL + "/search", {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result.content) {
          body.result.content = body.result.content.map(function (x) {
            let tranfrom = new Device().deserialize(x, {
              tranform: {
                'id': 'id',
                'name': 'name',
                description: 'description',
                updatedBy: '',
                updatedDate: 'createdAt',
                createdBy: '',
                createdDate: 'createdAt',
                sortOrder: '',
              }
            });
            return tranfrom;
          });

          body.result = body.result.content.find(x => x.id == id);

        }
      }),
    );
  }

  edit(model: UserServerModel, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url =`${API_URL}/${model.id}`
    const formData = new FormData();
    formData.append("serverId", model.serverId.toString());
    formData.append("userServer", model.userServer);
    formData.append("userId", model.userId.toString());
    // formData.append("id", model.id.toString());    

    return this.http.put<ResponseBody>(url, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  create(model: UserServerModel, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("serverId", model.serverId.toString());
    formData.append("userServer", model.userServer);
    formData.append("userId", model.userId.toString());    

    return this.http.post<ResponseBody>(API_URL, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }


  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
}