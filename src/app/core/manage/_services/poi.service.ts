import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment';
import { tap } from 'rxjs/operators';;
import { PoiModel } from '../_models/poi';

const API_URL: string = environment.api.host + "/pois";
@Injectable({
  providedIn: 'root'
})
export class PoiService {

  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      // tap(body => {
      //   if (body.result.content) {
      //     body.result.content = body.result.content.map(function (x) {
      //       let tranfrom = new Driver().deserialize(x, {});
      //       return tranfrom;
      //     });
      //   }
      // }),
    );
  }
  detail(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`;
    return this.http.get<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        body.result["latlngs"] = [...body.result.points];
        body.result["points"] = body.result.points.map(x => [x.lat, x.lng]);

      })
    );
  }

  create(poi: PoiModel, option?: BaseOptionAPI): Observable<ResponseBody> {
    let model = new PoiModel();
    model = this.utils.transformObject(poi, model);
    const formData = new FormData();
    formData.append("userId", model.userId.toString());
    formData.append("name", model.name);
    formData.append("poiType", model.poiType);
    formData.append("description", model.description);
    formData.append("active", model.active.toString());
    formData.append("latitude", model.latitude.toString());
    formData.append("longitude", model.longitude.toString());

    return this.http.post<ResponseBody>(API_URL, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  update(geofence: PoiModel, option?: BaseOptionAPI): Observable<ResponseBody> {
    let model = new PoiModel();
    model = this.utils.transformObject(geofence, model);
    const formData = new FormData();
    const url = `${API_URL}/${model.id}`;
    formData.append("userId", model.userId.toString());
    formData.append("name", model.name);
    formData.append("poiType", model.poiType);
    formData.append("description", model.description);
    formData.append("active", model.active.toString());
    formData.append("latitude", model.latitude.toString());
    formData.append("longitude", model.longitude.toString());

    return this.http.put<ResponseBody>(url, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  changeActive(params: { id: number, active: number,description:string }, option?: BaseOptionAPI) {

    const formData = new FormData();
    const url = `${API_URL}/${params.id}`;
    formData.append("active", params.active.toString());
    formData.append("description", params.description.toString());

    return this.http.put<ResponseBody>(url, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }


  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
}