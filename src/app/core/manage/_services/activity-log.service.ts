import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { TypesUtilsService } from '@core/_base/crud';
import { Role } from '@core/auth/_models/role.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ResponseBody } from '@core/_base/crud/models/_response-body.model';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';

const API_LOG_URL = environment.api.host + "/activity-log";

@Injectable({
  providedIn:'root'
})
export class ActivityLogService {
    constructor(
        private http: HttpClient, 
       private utils:TypesUtilsService
       ) { }
        // api role
        search(params:  any | undefined):Observable<ResponseBody>{
            return this.http.get<ResponseBody>(API_LOG_URL, { params: params});
        }

}