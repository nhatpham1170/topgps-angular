import { AlertRule } from '@core/manage/_models/alert-rules';
import { TaskRemind } from '@core/manage/_models/task-remind';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { Alert } from '../_models/alert-rules';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const API_TASK_REMIND = environment.api.host + "/remind";
const API_EVENT_TYPE = environment.api.host + "/alert/event-groups";
const API_TASK_REMIND_HISTORY = environment.api.host + "/notifications/history";

@Injectable({
  providedIn: 'root'
})
export class TaskRemindService {

  private eventTypeConfigs: Array<{ name: string,id:number, template: any,group:string }> = [
    {
      id:5,
      name:"task_normal",
      group:"normal",
      template:{}
    },
    {
      id:21,
      name: "maintenance_by_distance",
      group:"device",
      template: {
        odometer: 0,
        odometer_before: 0,
        odometer_after: 0,
        day_next:0,
        day_pass:0
      }
    },
    {
      id:22,
      name: "maintenance_by_hours",
      group:"device",
      template: {
        total_hours: 0,
        pass_hours:0,
        next_hours:0,
        day_pass:0
      }
    },

    // {
    //   id:6,
    //   name:"geofence_out",
    //   group:"in_out",
    //   template:{}
    // },


    // {
    //   id:14,
    //   name:"door_close",
    //   group:"action",
    //   template:{}
    // },    
    // {
    //   id:100,
    //   name:"trip_deviation",
    //   group:"action",
    //   template:{
    //     interval: "000500",
    //   }
    // },  
    // {
    //   id:101,
    //   name:"thieves_fuel",
    //   group:"action",
    //   template:{
    //     lostFuel:30,
    //     interval: "000500",
    //   }
    // },   
    // {
    //   id:101,
    //   name:"comming_geofence",
    //   group:"action",
    //   template:{
    //     distance:500,
    //     interval: "000500",

    //   }
    // },  

  ];
  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_TASK_REMIND, { params: params });
  }

  getEventType(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_EVENT_TYPE, { params: params });
  }
  eventTypes(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_EVENT_TYPE, {
      params: this.utils.processBassOptionApi(option),
    });
  }


  createTaskRemind(alert: TaskRemind, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("name", alert.name || "");
    formData.append("description", alert.description || "");
    formData.append("eventType", alert.eventType.toString());
    formData.append("textPrimary", alert.textPrimary || "");
    formData.append("suspended",  alert.suspended?"1":"0");
    formData.append("geofenceIds",  alert.geofenceIds.join(","));
    formData.append("deviceIds", alert.deviceIds.join(","));
    formData.append("extendedParams", JSON.stringify(alert.extendedParams));
    formData.append("parameterValue", JSON.stringify(alert.parameterValue));
    formData.append("pushSetting", JSON.stringify(alert.pushSetting));
    formData.append("schedule", JSON.stringify(alert.schedule));
    formData.append("userId", alert.userId.toString());

    return this.http.post<ResponseBody>(API_TASK_REMIND, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  updateRemind(alert: TaskRemind, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("name", alert.name || "");
    formData.append("description", alert.description || "");
    formData.append("eventType", alert.eventType.toString());
    formData.append("textPrimary", alert.textPrimary || "");
    formData.append("suspended",  alert.suspended?"1":"0");
    formData.append("geofenceIds",  alert.geofenceIds.join(","));
    formData.append("deviceIds", alert.deviceIds.join(","));
    formData.append("extendedParams", JSON.stringify(alert.extendedParams));
    formData.append("parameterValue", JSON.stringify(alert.parameterValue));
    formData.append("pushSetting", JSON.stringify(alert.pushSetting));
    formData.append("schedule", JSON.stringify(alert.schedule));
    formData.append("userId", alert.userId.toString());

    return this.http.put<ResponseBody>(`${API_TASK_REMIND}/${alert.id}`, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  changeLock(alert: TaskRemind, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    formData.append("suspended", alert.suspended.toString());
    return this.http.put<ResponseBody>(API_TASK_REMIND + "/" + alert.id, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  detail(id: number): Observable<ResponseBody> {

    return this.http.get<ResponseBody>(API_TASK_REMIND + '/' + id);
  }


  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_TASK_REMIND}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  get configTemplate(){
    return JSON.parse(JSON.stringify( this.eventTypeConfigs));
  } 
}
