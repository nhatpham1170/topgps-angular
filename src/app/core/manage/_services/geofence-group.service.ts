import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment.prod';
import { tap } from 'rxjs/operators';
// import { Geofence } from '../_models/Geofence';
import { GeofenceGroup } from '../_models/geofence-group';

const API_URL: string = environment.api.host + "/geofence-groups";
@Injectable({
  providedIn: 'root'
})
export class GeofenceGroupService {

  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, {
      params: this.utils.processBassOptionApi(option),
    })
  }
 
  listGeofenceGroup(params: any = undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, { params: params });

  }
  detail(params: any = undefined, id): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(params).forEach(x => {
      formData.set(x, params[x]);
    });
    return this.http.get<ResponseBody>(API_URL + '/' + id, { params: params })
  }

  update(GeofenceGroup: GeofenceGroup, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(GeofenceGroup).forEach(x => {
      formData.set(x, GeofenceGroup[x]);
    });
    return this.http.put<ResponseBody>(API_URL + "/" + GeofenceGroup.id, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  create(GeofenceGroup: GeofenceGroup, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(GeofenceGroup).forEach(x => {
      formData.set(x, GeofenceGroup[x]);
    });

    return this.http.post<ResponseBody>(API_URL, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }


  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
}
