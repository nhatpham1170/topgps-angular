import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment';
import { tap } from 'rxjs/operators';;
import { TripsModel } from '../_models/trips';

const API_URL: string = environment.api.host + "/trips";
@Injectable({
  providedIn: 'root'
})
export class TripService {

  constructor(private http: HttpClient, private utils: TypesUtilsService) { }

  list(params:  any | undefined): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(API_URL, {params:params});
  }

  detail(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`; 
    return this.http.get<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        body.result["latlngs"] = [...body.result.points];
        body.result["points"] = body.result.points.map(x => [x.lat, x.lng]);

      })
    );
  }

  create(trip: TripsModel, option?: BaseOptionAPI): Observable<ResponseBody> {
    let model = new TripsModel();
    model = this.utils.transformObject(trip, model);
    const formData = new FormData();
    formData.append("userId", model.userId.toString());
    formData.append("name", model.name);
    formData.append("status", model.status.toString());
    formData.append("stations", model.stations.toString());
    formData.append("deviceIds", model.deviceIds.toString());
    formData.append("groupIds", model.groupIds.toString());
    return this.http.post<ResponseBody>(API_URL, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  update(trip: TripsModel, option?: BaseOptionAPI): Observable<ResponseBody> {
    let model = new TripsModel();
    model = this.utils.transformObject(trip, model);
    const formData = new FormData();
    const url = `${API_URL}/${model.id}`;
    formData.append("userId", model.userId.toString());
    formData.append("name", model.name);
    formData.append("status", model.status.toString());
    formData.append("stations", model.stations.toString());
    formData.append("deviceIds", model.deviceIds.toString());
    formData.append("groupIds", model.groupIds.toString());
    return this.http.put<ResponseBody>(url, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  changeActive(trip: TripsModel, option?: BaseOptionAPI) {
    let model = new TripsModel();
    model = this.utils.transformObject(trip, model);
    const formData = new FormData();
    const url = `${API_URL}/${model.id}`;
    formData.append("status", model.status.toString());
    return this.http.put<ResponseBody>(url, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

}