import { Injectable } from '@angular/core';
import { Device } from '../_models/device';
import * as moment from 'moment'; // add this 1 of 4
import { CurrentUserService } from '@core/auth';

@Injectable({
  providedIn: 'root'
})
export class DeviceUtilityService {
  
  constructor(private currentUser:CurrentUserService){

  }
  /**
   * 
   * @param data List devices
   * @param dateNow 
   */
  processDevices(data: Device[],dateTimeNow:string) {

    // let dateTimeNow = moment(new Date()).format("YYYY/MM/DD");
    // // if(dateNow)
    let now =  moment(dateTimeNow);
    now.add(this.currentUser.timeZone,"minutes");
    data = data.map(x => {     
      let duration = moment.duration(now.diff(x.serviceExpire));
      let day = duration.asDays();
      if (day > -7 && day <= 0) {
        x.serviceExpireStatus = "warning";
      }
      else if (day > 0) {
        x.serviceExpireStatus = "danger";
      }
      else if (day <= -7) {
        x.serviceExpireStatus = "success";
      }

      // warrantyExpire
      duration = moment.duration(now.diff(x.warrantyExpiredDate));
      day = duration.asDays();
      if (day > -7 && day <= 0) {
        x.warrantyExpireStatus = "warning";
      }
      else if (day > 0) {
        x.warrantyExpireStatus = "danger";
      }
      else if (day <= -7) {
        x.warrantyExpireStatus = "success";
      }     
      return x;
    })
    return data;
  }
}
