import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';

import { environment } from '@env/environment';
import { BaseOptionAPI } from '../../_base/crud/models/_base.option-api';
import { Observable } from 'rxjs';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { TypesUtilsService } from '@core/_base/crud';
import { User, MapApiKey } from '@core/manage';
import { tap, retry } from 'rxjs/operators';
import { JsonPipe } from '@angular/common';

const API_URL: string = environment.api.host + '/users';
const API_URL_SETTING: string = environment.api.host + '/settings';
const API_URL_NOTIFICATION: string = environment.api.host + '/settings/notification';

@Injectable({
	providedIn: 'root',
})
export class UserManageService {
	public hostApi;
	constructor(private http: HttpClient, private utils: TypesUtilsService) {
		this.hostApi = environment.api.host;
	}

	post(User: User, option?: BaseOptionAPI): Observable<ResponseBody> {
		return this.http.post<ResponseBody>(
			environment.api.host + '/users',
			User,
			{
				params: this.utils.processBassOptionApi(option),
			}
		);
	}

	put(User: User, option?: BaseOptionAPI) {
		return this.http.put(environment.api.host + '/users/' + User.id, User, {
			params: this.utils.processBassOptionApi(option),
		});
	}

	searchChildUsers(option: BaseOptionAPI) {
		return this.http.get<ResponseBody>(API_URL + '/search-child-users', {
			params: this.utils.processBassOptionApi(option),
		});
	}

	changePassWord(params,id, option?: BaseOptionAPI)
	{
		let formData = new FormData();
		Object.keys(params).forEach(x => {
		  formData.set(x, params[x]);
		});
		return this.http.put(environment.api.host + '/users/change-password/' + id, formData, {
			params: this.utils.processBassOptionApi(option),
		});
	}

	updateProfile(user: User, option?: BaseOptionAPI) {
		let formData = new FormData();
		Object.keys(user).forEach((x) => {
			formData.set(x, user[x]);
		});
		return this.http.put(
			environment.api.host + '/users/profile/update',
			formData,
			{
				params: this.utils.processBassOptionApi(option),
			}
		);
	}

	getInfoParent(params, option?: BaseOptionAPI): Observable<ResponseBody> {
		return this.http.get<ResponseBody>(
			environment.api.host + '/dashboard',
			{ params: params }
		);
	}

	get(id: any | undefined) {
		return this.http.get(environment.api.host + '/users/' + id);
	}

	list(option?: BaseOptionAPI): Observable<ResponseBody> {
		return this.http.get<ResponseBody>(API_URL + '/search-users', {
			params: this.utils.processBassOptionApi(option),
		});
	}

	search(filter: any | undefined) {
		return this.http.get(environment.api.host + '/users/search-users', {
			params: filter,
		});
	}

	delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
		return this.http.delete<ResponseBody>(
			environment.api.host + '/users/' + id,
			{
				params: this.utils.processBassOptionApi(option),
			}
		);
	}

	resetPassword(params: any | undefined, option?: BaseOptionAPI) {
		return this.http.put(
			environment.api.host + '/users/reset-password-default/' + params.id,
			params,
			{
				params: this.utils.processBassOptionApi(option),
			}
		);
	}

	// MAP API KEY
	postKey(mapApiKey: MapApiKey, option?: BaseOptionAPI): Observable<ResponseBody> {
		let model = new MapApiKey();
		model = this.utils.transformObject(mapApiKey, model);
		const formData = new FormData();
		formData.append('userId', model.userId.toString());
		formData.append('type', model.type);
		formData.append('name', model.name);
		formData.append('apiKey', model.apiKey);
		formData.append('description', model.description);
		formData.append('mapApiKeyUser', model.mapApiKeyUser );

		return this.http.post<ResponseBody>(
			environment.api.host + '/users/map-api-key',
			formData,
			{
				params: this.utils.processBassOptionApi(option),
			}
		);
	}

	getKey(params: any | undefined): Observable<ResponseBody> {
		return this.http.get<ResponseBody>(
			environment.api.host + '/users/map-api-key',
			{
				params: params 
			}
		);
	}

	searchKey(filter: any | undefined) {
		return this.http.get(environment.api.host + '/users/map-api-key', {
			params: filter,
		});
	}

	changeActive(
		params: { id: number; active: number },
		option: BaseOptionAPI
	) {
		const formData = new FormData();
		formData.append('active', params.active.toString());
		return this.http.put(
			environment.api.host + '/users/map-api-key/change-status/' + params.id,
			formData,
			{
				params: this.utils.processBassOptionApi(option),
			}
		);
	}



	getUserUses(params: any | undefined): Observable<ResponseBody> {
		return this.http.get<ResponseBody>(
			environment.api.host + '/users/map-api-key-user',
			{ params },
		);
	}

	createUserUseApiKey(params: any, option?: BaseOptionAPI): Observable<ResponseBody> {
		const formData = new FormData();
		formData.append('userIds', params.userIds.toString());
		formData.append('apiKeyId', params.apiKeyId.toString());
		return this.http.post<ResponseBody>(
			environment.api.host + '/users/map-api-key-user',
			formData,
			{
				params: this.utils.processBassOptionApi(option),
			}
		);
	}

	deleteUserUseApiKey(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
		return this.http.delete<ResponseBody>(
			environment.api.host + '/users/map-api-key-user/' + id,
			{
				params: this.utils.processBassOptionApi(option),
			}
		);
	}

	updateKey(mapApiKey: MapApiKey, option?: BaseOptionAPI) {
		let model = new MapApiKey();
		model = this.utils.transformObject(mapApiKey, model);
		const formData = new FormData();
		formData.append('userId', model.userId.toString());
		formData.append('type', model.type);
		formData.append('apiKey', model.apiKey);
		formData.append('name', model.name);
		formData.append('description', model.description);
		formData.append('mapApiKeyUser', model.mapApiKeyUser );

		return this.http.put<ResponseBody>(
			environment.api.host + '/users/map-api-key/' + mapApiKey.id,
			formData,
			{
				params: this.utils.processBassOptionApi(option),
			}
		);
	}

	deleteKey(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
		return this.http.delete<ResponseBody>(
			environment.api.host + '/users/map-api-key/' + id,
			{
				params: this.utils.processBassOptionApi(option),
			}
		);
	}

	// USER SETTINGS - NOTIFICATIONS
	putNotification(params: any, option?: BaseOptionAPI): Observable<ResponseBody> {
		const formData = new FormData();
		formData.append('userId', params.userId)
		formData.append('notification', JSON.stringify(params.notification));
		return this.http.post<ResponseBody>(
			API_URL_NOTIFICATION,
			formData,
			{ params:this.utils.processBassOptionApi(option), }
		);
	}

	listNotifications(userId): Observable<ResponseBody> 
	{
	  return this.http.get<ResponseBody>(API_URL_SETTING + "/" + userId);
	}
}
