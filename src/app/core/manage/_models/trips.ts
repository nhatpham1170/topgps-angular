export class TripsModel {
    id: number;
    userId: number;
    name: string;
    status: number;
    createdAt: string;
    stations:Array<number>
    deviceIds:Array<number>;
    groupIds:Array<number>;
    constructor(obj?: any) {
        this.id = undefined;
        this.userId = undefined;
        this.status = 1;
        this.stations = [];
        this.deviceIds = [];
        this.groupIds = [];
        Object.assign(this, obj);
        return Object.assign({}, this, obj);
    }
    clear() {
     
    }
}
