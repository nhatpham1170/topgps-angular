export class message {
    id: number;
    title: string;
    message:string;
    isRead:boolean;
    userIds:any;
    createdBy : string; 
    createdAt : string;
    constructor(obj?:any){
        return Object.assign({},this,obj);
    }
    clear() {

    }
}
