export class UserAlias {
    id: number;
    userId:number;
    username: string;
    name:string;
    email:string;
    phone: number;
    timeSend:string;
    roleId:number;
    deviceIds?:any;
    deviceGroups?:any;
    startTime?:string;
    endTime?:string;
    constructor(obj?:any){
        return Object.assign({},this,obj);
    }
    clear() {
      this.username = '';
      this.name = '';
      this.email = '';
      this.phone = 0;
    }
}
