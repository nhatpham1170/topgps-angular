import { BaseModelCT } from '@core/_base/crud/models/_base.model.ct';

export class Icon extends BaseModelCT {
    id :number;
    name : string;
    nameKey  : string;
    iconSvg : File;
    iconMapSvg : string;
    sortOrder : number;
    status : number;
    iconUrl : File;
    iconMapUrl :string;
    createdDate : string;
    createdBy : string;
    modifiedBy  : string;
    modifiedDate : string;
    constructor() {
        super();
        this.id = 0;
        this.name = "";
        this.nameKey = "";
        this.iconSvg = null;
        this.iconMapSvg = '';
        this.sortOrder = 0;
        this.status = 0;
        this.iconUrl = null
        this.iconMapUrl = "";
        this.createdDate = "";
        this.createdBy = "";
        this.modifiedBy = "";
        this.modifiedDate = "";
    }
    clear(){
        this.id = 0;
        this.name = "";
        this.nameKey = "";
        this.iconSvg = null;
        this.iconMapSvg = '';
        this.sortOrder = 0;
        this.status = 0;
        this.iconUrl = null
        this.iconMapUrl = "";
        this.createdDate = "";
        this.createdBy = "";
        this.modifiedBy = "";
        this.modifiedDate = "";
    }
}
