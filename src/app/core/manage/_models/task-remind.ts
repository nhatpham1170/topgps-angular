
export class TaskRemind{
    id:number;
    userId:number;
    name:string;
    description:string;
    eventTypeId:number;
    eventType:string;
    type:string;
    suspended:number;//active
    textPrimary:string;
    textSecondary:string;
    pushSetting:{
        emails:Array<string>,
        phones:Array<string>,
        smsPhones:Array<string>,
        pushEnabled:boolean,   
        sos:boolean,     
    };
    deviceIds:Array<number>;
    geofenceIds:Array<number>;
    extendedParams:{        
        geofenceStatus:number,
        isAllDevices:boolean,
        isAllGeofence:boolean,
    };
    schedule:{
        weekly:{
            day:Array<number>,
            timeStart:number,
            timeEnd:number,    
        },
        periodActive:boolean,
        periodForm:number,
        periodTo:number,
        repeatSelect:string,
        dateTime:number;
    };
    parameterValue:{
        odometer?:number,
        odometer_after?:number,
        odometer_before?:number,
        total_hours?:number,
        pass_hours?:number,
        next_hours?:number,
        value?:number,
        interval?:number,
        day_next?:number,
        day_pass?:number
    };
    createdAt:string;
    constructor(){
        this.id = 0;
        this.userId=0;
        this.eventTypeId = 0;
        this.name = "";
        this.description="";
        this.eventType= "";
        this.suspended= 1;//active
        this.textPrimary = "";
        this.textSecondary = "";
        this.pushSetting = {
            emails:[],
            phones:[],
            smsPhones:[],
            pushEnabled:true, 
            sos:false       
        };
        this.deviceIds = [];
        this.geofenceIds = [];
        this.extendedParams ={            
            geofenceStatus:0,
            isAllDevices:true,
            isAllGeofence:true,
        };
        this.schedule = {
            weekly:{
                day:[],
                timeStart:0,
                timeEnd:0,     
               
            },
            periodActive:false,
            periodForm:0,
            repeatSelect:'no',
            periodTo:0,
            dateTime:0
        };
       this.parameterValue = {
            odometer:0,
            odometer_after:0,
            odometer_before:0
        };
        this.createdAt = null;
    }
} 
