
export class Alert {
    id: number;
    nameAlert: string;
    userId:number;
    description:string;
    eventType:number;
    pushSetting:object;
    deviceIds:any;
    geofenceIds:any;
    geofenceAppendTitle:string;
    extendedParams:object;
    schedule:object;
    textPrimary:string;
    paramValues:any;
    parameterValue:object;
    textNotification:string;
    suspended: any;
    createdBy : string; 
    createdAt : string;
    constructor(obj?:any){
        return Object.assign({},this,obj);
    }
    clear() {
        this.id = undefined;
        this.nameAlert = "";
        this.userId = undefined;
        this.description = "";
        this.eventType = undefined;
        this.pushSetting = {
            push_enabled : true,
            emergency: false,
            sms_phones: '',
            emails : '',
            phones:''
        };
        this.deviceIds = [];
        this.geofenceIds = [];
        this.geofenceAppendTitle ='';
        this.extendedParams = {
            emergency:false,
            is_inside_genfence:true
        };
        this.schedule = {
            period_active : true, 
            period_from: null,
            period_to: null,
            weekly: 
              {
                  day : [2,3,4,5,6],
                  timestart : '',
                  timeend :'' 
              }
        };
        this.parameterValue = {
            eventType: undefined,
            value : ''
        };
        this.textNotification = '';
        this.suspended = '1';
        this.createdAt = null;
        this.createdBy = '';
    }
}
export class AlertRule{
    id:number;
    userId:number;
    name:string;
    description:string;
    eventTypeId:number;
    eventType:string;
    suspended:number;//active
    textPrimary:string;
    textSecondary:string;
    pushSetting:{
        emails:Array<string>,
        phones:Array<string>,
        smsPhones:Array<string>,
        pushEnabled:boolean,   
        sos:boolean,     
    };
    deviceIds:Array<number>;
    geofenceIds:Array<number>;
    extendedParams:{        
        geofenceStatus:number,
        isAllDevices:boolean,
        isAllGeofence:boolean,
    };
    schedule:{
        weekly:{
            day:Array<number>,
            timeStart:number,
            timeEnd:number,            
        },
        periodActive:boolean,
        periodForm:number,
        periodTo:number,
        dateTime:number,
    };
    parameterValue:{
        value:number,
        interval:number,
    };
    createdAt:string;
    constructor(){
        this.id = 0;
        this.userId=0;
        this.eventTypeId = 0;
        this.name = "";
        this.description="";
        this.eventType= "";
        this.suspended= 1;//active
        this.textPrimary = "";
        this.textSecondary = "";
        this.pushSetting = {
            emails:[],
            phones:[],
            smsPhones:[],
            pushEnabled:true, 
            sos:false       
        };
        this.deviceIds = [];
        this.geofenceIds = [];
        this.extendedParams ={            
            geofenceStatus:0,
            isAllDevices:true,
            isAllGeofence:true,
        };
        this.schedule = {
            weekly:{
                day:[],
                timeStart:0,
                timeEnd:0,            
            },
            periodActive:false,
            periodForm:0,
            periodTo:0,
            dateTime:0
        };
       this.parameterValue = {
            value:0,
            interval:0,
        };
        this.createdAt = null;
    }
} 
