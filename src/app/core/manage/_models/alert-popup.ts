import { GeofenceModel } from './geofence';
import { Device } from './device';

export class AlertPopup {
    id:string;
    event:{
        name:string,
        id:number,
        key:string,
    };
    alertRule:{
        name:string,
        id:number,
    }
    userId:number
    eventKey:string;
    extendedInfo:any;
    isPushedEmail:string;
    isPushedSms:string;
    nameAlert:string;
    isRead:string;
    message:string;
    geofence?:GeofenceModel;
    points?:any;
    lat:number;
    lng:number;
    createdAt:string; 
    timeIn:string; 
    timeOut:string; 
    totalTime:string; 
    device:Device;
    address:string;
}
