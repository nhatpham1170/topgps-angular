import { BaseModelCT } from '@core/_base/crud/models/_base.model.ct';

export class GeofenceGroup extends BaseModelCT {
    id: number;
    name: string;
    userId: number;
    createBy: string;
    createdAt: string;    
    modifiedBy: string;
    updatedAt: string;
    countGeofence : number;
    geofenceIds: any [];
    constructor() {
        super();
        this.id = 0;
        this.name = "";
        this.userId = 0;
        this.createBy = "";
        this.createdAt = "";
        this.modifiedBy = "";
        this.updatedAt = "";
        this.geofenceIds = [];
        this.countGeofence = 0;
    }
    clear() {
        this.id = 0;
        this.name = "";    
        this.userId = 0;    
        this.createBy = "";
        this.createdAt = "";
        this.modifiedBy = "";
        this.updatedAt = "";
        this.geofenceIds = [];
        this.countGeofence = 0;
    }
}
