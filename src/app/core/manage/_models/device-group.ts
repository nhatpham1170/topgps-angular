import { BaseModelCT } from '@core/_base/crud/models/_base.model.ct';

export class DeviceGroup extends BaseModelCT {
    id: number;
    name: string;
    userId: number;
    createBy: string;
    createdAt: string;    
    modifiedBy: string;
    updatedAt: string;
    countDevice : number;
    deviceIds: any [];
    constructor() {
        super();
        this.id = 0;
        this.name = "";
        this.userId = 0;
        this.createBy = "";
        this.createdAt = "";
        this.modifiedBy = "";
        this.updatedAt = "";
        this.deviceIds = [];
        this.countDevice = 0;
    }
    clear() {
        this.id = 0;
        this.name = "";    
        this.userId = 0;    
        this.createBy = "";
        this.createdAt = "";
        this.modifiedBy = "";
        this.updatedAt = "";
        this.deviceIds = [];
        this.countDevice = 0;
    }
}
