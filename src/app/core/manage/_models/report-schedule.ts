import { stringify } from 'querystring';

export class ReportSchedule {
    id: number;
    name: string;
    userId: number;
    typeReport:number;
    formatReport:string;
    schedule:{
        dateStart:string,
        dateEnd:string,
        daily?:{
            isDaily:boolean,
            time:any
        },
        weekly?:{
            isWeekly:boolean,
            time:any
        }
    };
    pushSetting:{
        emails?:string
    };
    deviceIds:any;
    createdBy : string; 
    createdAt : string;
    constructor(obj?:any){
        return Object.assign({},this,obj);
    }
    clear() {
        this.id = undefined;
        this.name = '',
        this.typeReport = undefined;
        this.formatReport = "Excel";
        this.schedule = {
            dateStart:'',
            dateEnd:'',
            daily:{
                isDaily: false,
                time:''
            },
            weekly:{
                isWeekly:false,
                time: ''
            }
        };
        this.deviceIds = [];
        this.createdAt = null;
        this.createdBy = '';
    }
}
