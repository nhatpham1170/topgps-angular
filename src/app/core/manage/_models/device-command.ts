export class DeviceCommand {
    id: number;
    imei: string;
    commandName:string;
    commandText:string;
    sended: boolean;
    timeSend:string;
    timeReturn:string;
    contentReturn:string;
    devices:any;
    returned : boolean;
    status : number;
    createdBy : string; 
    createdAt : string;
    constructor(obj?:any){
        return Object.assign({},this,obj);
    }
    clear() {
        this.id = undefined;
        this.imei = "";
        this.devices = [];
        this.commandName = "";
        this.commandText = "";
        this.sended = true;
        this.timeSend = "";
        this.timeReturn = "";
        this.returned = true;
        this.status = 0;
        this.createdAt = null;
        this.createdBy = '';
    }
}
