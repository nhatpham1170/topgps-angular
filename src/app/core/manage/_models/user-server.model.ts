export class UserServerModel{
    id:number;
    serverId:number;
    serverName:string;
    domainName:string;
    userServer:string;
    userId:number;
    status?:number;
    constructor(){
        this.id = undefined;
        this.serverId = undefined;
        this.serverName = "";
        this.domainName = "";
        this.userServer = "";
        this.userId = 0;
        this.serverName = "";
        this.status = 0;
    }
}