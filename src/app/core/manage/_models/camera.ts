import { BaseModelCT } from '@core/_base/crud/models/_base.model.ct';

export class Camera extends BaseModelCT {
    id: number;
    name: string;
    deviceId:number;
    cameraId:string;
    description: string;
    constructor() {
        super();
        this.id = 0;
        this.name = "";
        this.description = "";
        this.cameraId = "";
        this.deviceId = 0;
    }
    clear() {
        this.id = 0;
        this.name = "";
        this.description = "";
        this.cameraId = "";
        this.deviceId = 0;
    }
}