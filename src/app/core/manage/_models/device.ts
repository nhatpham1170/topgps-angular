import { BaseModelCT } from '@core/_base/crud/models/_base.model.ct';
import { number } from '@amcharts/amcharts4/core';
import { MatMenuModule } from '@angular/material';

export class Device extends BaseModelCT {
    id: number;
    imei: string;
    userName: string;
    typeDevice: number;
    groupIds: [];
    groupDevices: [];
    simno: string;
    simType: number;
    userId: number;
    active: number;
    status: number;
    inputFlag: number;
    icon: string;
    serviceExpire: string;
    serviceExpireStatus: string;
    machineCode: string;
    machineCodeSearch: string;
    name: string;
    description: string;
    createdAt: string;
    createByUser: string;
    deviceGroupData: number;
    descAdmin: string;
    activeWarranty: number;
    warrantyActiveDate: string;
    warrantyExpiredDate: string;
    warrantyExpireStatus: string;
    sortOrder: number;
    iconAdvancedJson: any;
    updatedAt: string;
    modifiedBy: string;
    groupName: string;
    driverId: number;
    typeName: string;
    // advanced
    numberPlate: string;
    frameNumber: string;
    vinNumber: string;
    engineNumber:string;
    transportType: number;
    transportTypeQcvn: number;
    driver: number;
    coefficient: number;
    sendToGovernment: number;
    governmentManage: string;
    isSell:number;
    sellAt:string;
    sellUpdated:string;
    governmentId:number;
    isDebug: number;
    corridor: number;
    usingTollFee: number;
    qcvn: boolean;
    // extentions
    extensions: {
        timeoutGprs: number;
        consumptionRate: number;
        minSpeed: number;
        timeZone: number;
        minStopDuration: number;
    };


    constructor() {
        super();
        this.id = 0;
        this.imei = "";
        this.userName = "";
        this.typeDevice = 0;
        this.groupIds = [];
        this.simno = "";
        this.simType = 0;
        this.userId = 0;
        this.active = 0;
        this.status = 0;
        this.inputFlag = 0;
        this.icon = "";
        this.serviceExpire = "";
        this.serviceExpireStatus = "";
        this.machineCode = "";
        this.machineCodeSearch = "";
        this.name = "";
        this.description = "";
        this.createdAt = "";
        this.createByUser = "";
        this.deviceGroupData = 0;
        this.descAdmin = "";
        this.activeWarranty = 0;
        this.warrantyActiveDate = "";
        this.warrantyExpiredDate = "";
        this.warrantyExpireStatus = "";
        this.sortOrder = 0;
        this.updatedAt = "";
        this.modifiedBy = "";
        this.groupName = "";
        this.typeName = "";
        this.numberPlate = "";
        this.frameNumber = "";
        this.vinNumber = "";
        this.engineNumber = "";
        this.transportType = 0;
        this.transportTypeQcvn = 0;
        this.driver = 0;
        this.coefficient = 0;
        this.sendToGovernment = 0;
        this.governmentManage = "";
        this.isSell = 0;
        this.sellAt = "";
        this.sellUpdated = "";
        this.governmentId = 0;
        this.isDebug = 0;
        this.usingTollFee = 0;
        this.extensions = {
            timeoutGprs: 20,
            consumptionRate: 0,
            minSpeed: 3,
            timeZone: 0,
            minStopDuration: 600
        };
    }
  
    clear() {
        this.id = 0;
        this.imei = "";
        this.userName = "";
        this.typeDevice = 0;
        this.groupIds = [];
        this.simno = "";
        this.simType = 0;
        this.userId = 0;
        this.active = 0;
        this.status = 0;
        this.inputFlag = 0;
        this.icon = "";
        this.serviceExpire = "";
        this.serviceExpireStatus = "";
        this.machineCode = "";
        this.machineCodeSearch = "";
        this.name = "";
        this.description = "";
        this.createdAt = "";
        this.createByUser = "";
        this.deviceGroupData = 0;
        this.descAdmin = "";
        this.activeWarranty = 0;
        this.warrantyActiveDate = "";
        this.warrantyExpiredDate = "";
        this.warrantyExpireStatus = "";
        this.sortOrder = 0;
        this.updatedAt = "";
        this.modifiedBy = "";
        this.groupName = "";
        this.typeName = "";
        this.numberPlate = "";
        this.frameNumber = "";
        this.vinNumber = "";
        this.engineNumber = "";
        this.transportType = 0;
        this.transportTypeQcvn = 0;
        this.driver = 0;
        this.coefficient = 0;
        this.sendToGovernment = 0;
        this.governmentManage = "";
        this.isSell = 0;
         this.sellAt = "";
        this.sellUpdated = "";
    }
}