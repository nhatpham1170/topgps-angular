export class PoiModel {
    id: number;
    userId: number;
    name: string;
    description: string;
    latitude: number;
    longitude:number;
    type: string;
    poiType:string;
    active: number;
    createdAt: string;
    radius: number;
    encodedPoints: string;
    points: Array<[number,number]>;
    color: string;
    fill: boolean;
    fillColor: string;
    fillOpacity: number;
    opacity: number;
    stroke: true;
    typePoi: string;
    weight: number;
    latlngs:Array<{lat:number,lng:number}>
    constructor(obj?: any) {
        this.id = undefined;
        this.userId = undefined;
        this.latitude = 0;
        this.longitude = 0;
        this.typePoi = '';
        this.name = "";
        this.description = "";
        this.type = "";
        this.active = 1;
        this.createdAt = "";
        this.radius = 0;
        this.encodedPoints = "";
        this.points = [];
        this.color = "#3388ff";
        this.fill = true;
        this.fillColor = "#3388ff";
        this.fillOpacity = 0.2;
        this.opacity = 0.5;
        this.stroke = true;
        this.weight = 4;
        this.latlngs = [];
        Object.assign(this, obj);
        if(this.latlngs.length==0 && this.points && this.points.length>0){
            
        }
        return Object.assign({}, this, obj);
    }
    clear() {
        this.id = undefined;
        this.userId = 0;
        this.name = "";
        this.description = "";
        this.type = "";
        this.active = 1;
        this.createdAt = "";
        this.radius = 0;
        this.encodedPoints = "";
        this.points = [];
        this.color = "#3388ff";
        this.fill = true;
        this.fillColor = "#3388ff";
        this.fillOpacity = 0.2;
        this.opacity = 0.5;
        this.stroke = true;
        this.weight = 4;
    }
}
