import { BaseModelCT } from '@core/_base/crud/models/_base.model.ct';
import { array } from '@amcharts/amcharts4/core';

export class User extends BaseModelCT {
    active: number;
    aliasId: number;
    password: string;
    carSorting: string;
    createdAt: string;
    dateFormat: string;
    decimalSeprerator: string;
    description: string;
    editedAt: string;
    email: string;
    id: number;
    isDeleted: number;
    isRenewed: number;
    language: string;
    markerStyle: string;
    name: string;
    parentId: number;
    path: string;
    phone: string;
    point: number;
    roleId: number;
    roleName: string;
    status: number;
    stockDevice: number;
    surname: string;
    timeFormat: string;
    timezone: string;
    totalDevice: number;
    totalPoint: number;
    type: number;
    unitDistance: string;
    unitTemperature: string;
    unitVolume: string;
    unitWeight: string;
    userSettingJson: any;
    username: string;
    weekFirstDay: number;
    pageMain:string;
    notifications: Notification[];

    constructor() {
        super();
        this.active= 0;
        this.aliasId= 0;
        this.carSorting= "";
        this.createdAt= "";
        this.dateFormat= "";
        this.decimalSeprerator= "";
        this.description= "";
        this.editedAt= "";
        this.email= "";
        this.id= 0;
        this.isDeleted= 0;
        this.isRenewed= 0;
        this.language= "";
        this.markerStyle= "";
        this.name= "";
        this.parentId= 0;
        this.path= "";
        this.phone= "";
        this.point= 0;
        this.roleId= 0;
        this.roleName= "";
        this.status= 0;
        this.stockDevice= 0;
        this.surname= "";
        this.timeFormat= "";
        this.timezone= "";
        this.totalDevice= 0;
        this.totalPoint= 0;
        this.type= 0;
        this.unitDistance= "";
        this.unitTemperature= "";
        this.unitVolume= "";
        this.unitWeight= "";
        this.userSettingJson= null;
        this.username= "";
        this.weekFirstDay= 0;
        this.password = '';
        this.pageMain = "";
        this.notifications = []
    }
    clear() {
        this.id = 0;
        this.active= 0;
        this.aliasId= 0;
        this.carSorting= "";
        this.createdAt= "";
        this.dateFormat= "";
        this.decimalSeprerator= "";
        this.description= "";
        this.editedAt= "";
        this.email= "";
        this.id= 0;
        this.isDeleted= 0;
        this.isRenewed= 0;
        this.language= "";
        this.markerStyle= "";
        this.name= "";
        this.parentId= 0;
        this.path= "";
        this.phone= "";
        this.point= 0;
        this.roleId= 0;
        this.roleName= "";
        this.status= 0;
        this.stockDevice= 0;
        this.surname= "";
        this.timeFormat= "";
        this.timezone= "";
        this.totalDevice= 0;
        this.totalPoint= 0;
        this.type= 0;
        this.unitDistance= "";
        this.unitTemperature= "";
        this.unitVolume= "";
        this.unitWeight= "";
        this.userSettingJson= null;
        this.username= "";
        this.weekFirstDay= 0;
        this.notifications = []
    }
}

// MAP API KEY 
export class MapApiKey extends BaseModelCT {
    id: number;
    userId: number;
    mapApiKeyUser: any;
    type: string;
    name: string;
    apiKey: string;
    active: number;
    description?: string;
    createdAt: string;

    constructor() {
        super();
        this.id = undefined;
        this.userId = 0;
        this.mapApiKeyUser = [];
        this.name = '';
        this.type = '';
        this.apiKey = '';
        this.active = 1;
        this.description = '';
        this.createdAt = "";

    }

    clear() {
        this.id = undefined;
        this.userId = 0;
        this.mapApiKeyUser = [];
        this.type = '';
        this.name = '';
        this.apiKey = '';
        this.active = 1;
        this.description = '';
        this.createdAt = "";
    }
}

// USER SETTINGS - NOTIFICATION 
export class Notification {
    type: string;
    active: number;
}