export class SensorParameter {
    max?: number;
    min?: number;
    type: string;
    unit?: string;
    round?: number;
    offset?: number;
    formula?: string;
    parameter_name?: string;
    calibration?: string;
    show_on_map?: boolean;
    increase?: number;
    decrease?: number;
    icon?:string;
    typeSensor:string;
    constructor() {
        this.max = 0;
        this.min = 0;
        this.type = "";
        this.unit = "";
        this.round = 0;
        this.offset = 0;
        this.formula = "[value]";
        this.parameter_name = "";
        this.calibration = "";
        this.show_on_map = false;
        this.max = 20;
        this.min = 20;
        this.typeSensor = "other";
    }
    clear() {
        this.max = 0;
        this.min = 0;
        this.type = "";
        this.unit = "";
        this.round = 0;
        this.offset = 0;
        this.formula = "[value]";
        this.parameter_name = "";
        this.calibration = "";
        this.show_on_map = false;
        this.max = 20;
        this.min = 20;
    }
}