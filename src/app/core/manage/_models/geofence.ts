export class GeofenceModel {
    id: number;
    userId: number;
    name: string;
    description: string;
    type: string;
    active: number;
    createdAt: string;
    radius: number;
    encodedPoints: string;
    points: Array<[number,number]>;
    color: string;
    fill: boolean;
    fillColor: string;
    fillOpacity: number;
    opacity: number;
    stroke: true;
    weight: number;
    groupIds:any;
    geofenceGroups:any;
    neLat:number;
    neLng:number;
    swLat:number; 
    swLng:number;
    latLngBounds:{
        _northEast:{
            lat:number,
            lng:number
        },
        _southWest:{
            lat:number,
            lng:number
        }    
    };

    latlngs:Array<{lat:number,lng:number}>
    constructor(obj?: any) {
        this.id = undefined;
        this.userId = undefined;
        this.name = "";
        this.description = "";
        this.type = "";
        this.active = 1;
        this.createdAt = "";
        this.radius = 0;
        this.encodedPoints = "";
        this.points = [];
        this.color = "#3388ff";
        this.fill = true;
        this.fillColor = "#3388ff";
        this.fillOpacity = 0.2;
        this.opacity = 0.5;
        this.stroke = true;
        this.weight = 4;
        this.latlngs = [];
        this.groupIds = [];
        this.neLat= 0;
        this.neLng= 0;
        this.swLat= 0; 
        this.swLng= 0;
        this.radius= 0;
        this.latLngBounds = {
            _northEast:{
                lat:0,
                lng:0
            },
            _southWest:{
                lat:0,
                lng:0
            }    
        };
        Object.assign(this, obj);

        
        if(this.latlngs.length==0 && this.points && this.points.length>0){
            
        }
        return Object.assign({}, this, obj);
    }
    clear() {
        this.id = undefined;
        this.userId = 0;
        this.name = "";
        this.description = "";
        this.type = "";
        this.active = 1;
        this.createdAt = "";
        this.radius = 0;
        this.encodedPoints = "";
        this.points = [];
        this.color = "#3388ff";
        this.fill = true;
        this.fillColor = "#3388ff";
        this.fillOpacity = 0.2;
        this.opacity = 0.5;
        this.stroke = true;
        this.weight = 4;
        this.groupIds = [];
        this.neLat= 0;
        this.neLng= 0;
        this.swLat= 0; 
        this.swLng= 0;
        this.radius= 0;
        this.latLngBounds = {
            _northEast:{
                lat:0,
                lng:0
            },
            _southWest:{
                lat:0,
                lng:0
            }    
        };
    }
}
