import { BaseModelCT } from '@core/_base/crud/models/_base.model.ct';

export class Driver extends BaseModelCT {
    id: number;
    name: string;
    description: string;
    phone: string;
    email: string;
    licenseNumber: string;
    createdBy: string;
    createdAt: string;
    updatedAt:string;
    updatedBy:string;
    userId: number;
    driverCode:string;
    beginDate:string;
    expireDate:string;
    address:string;

    constructor() {
        super();
        this.id = 0;
        this.name = "";
        this.description = "";
        this.createdBy = "";
        this.createdAt = "";
        this.updatedAt = "";
        this.updatedBy = "";
        this.phone = "";
        this.licenseNumber = "";
        this.email = "";
        this.userId = 0;
        this.driverCode = "";
        this.beginDate = "";
        this.expireDate = "";
        this.address = "";
    }
    clear() {
        this.id = 0;
        this.name = "";
        this.description = "";
        this.createdBy = "";
        this.createdAt = "";
        this.updatedAt = "";
        this.updatedBy = "";
        this.phone = "";
        this.licenseNumber = "";
        this.email = "";
        this.userId = 0;
        this.driverCode = "";
        this.beginDate = "";
        this.expireDate = "";
        this.address = "";
    }
}