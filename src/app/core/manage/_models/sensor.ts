import { BaseModelCT } from '@core/_base/crud/models/_base.model.ct';
import { SensorParameter } from './sensor-parameter';

export class Sensor extends BaseModelCT {
    id: number;
    name: string;
    calculationType: string;
    keyLanguage: string;
    sortOrder: number;
    parameters: SensorParameter;
    description: string;
    createBy: string;
    createdAt: string;
    deviceId: number;
    modifiedBy: string;
    updatedAt: string;
    constructor() {
        super();
        this.id = 0;
        this.name = "";
        this.calculationType = "";
        this.keyLanguage = "";
        this.createBy = "";
        this.sortOrder = 0;
        this.parameters = new SensorParameter();
        this.description = "";
        this.createdAt = "";
        this.deviceId = 0;
        this.modifiedBy = "";
        this.updatedAt = "";
    }
    clear() {
        this.id = 0;
        this.name = "";
        this.calculationType = "";
        this.createBy = "";
        this.sortOrder = 0;
        this.parameters = new SensorParameter();
        this.description = "";
        this.createdAt = "";
        this.deviceId = 0;
        this.modifiedBy = "";
        this.updatedAt = "";
    }
}

export class IoSetting {
    key:string;
    value:string;
    active:boolean;
    basic:boolean;
  }