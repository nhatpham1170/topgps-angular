import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment.prod';
import { DeviceType } from '../_models/device-type';
import { tap } from 'rxjs/operators';
import { IoSetting } from '@core/manage/_models/sensor';

const API_URL = environment.api.host + "/admin/device-type";

@Injectable({
  providedIn: 'root'
})
export class DeviceTypeService {

  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result.content) {
          body.result.content = body.result.content.map(function (x) {
            let tranfrom = new DeviceType().deserialize(x, {
              tranform: {
                'id': 'id',
                'name': 'name',
                description: 'description',
                updatedBy: 'modifiedBy',
                updatedDate: 'updatedAt',
                createdBy: 'createBy',
                createdDate: 'createdAt',
                sortOrder: 'sortOrder',
              }
            });
            return tranfrom;
          });
        }
      }),
    );
  }
  detail(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    if (option == undefined) option = new BaseOptionAPI();
    option.params = { id: id };
    return this.http.get<ResponseBody>(`${API_URL}`, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result.content) {
          body.result.content = body.result.content.map(function (x) {
            let tranfrom = new DeviceType().deserialize(x, {
              tranform: {
                'id': 'id',
                'name': 'name',
                description: 'description',
                updatedBy: 'modifiedBy',
                updatedDate: 'updatedAt',
                createdBy: 'createBy',
                createdDate: 'createdAt',
                sortOrder: 'sortOrder',
              }
            });
            return tranfrom;
          });
          body.result = body.result.content.find(x => x.id == id);
        }
      }),
    );
  }

  create(model: DeviceType, option?: BaseOptionAPI): Observable<ResponseBody> {   
    model = this.processDeviceType(model);
    let IoSetting = model.ioSetting;
    let extensions = model.extensions;
    let formData = new FormData();
    Object.keys(model).forEach(x => {
      formData.set(x, model[x]);
    });
    formData.set('ioSetting', JSON.stringify(IoSetting));
    formData.set('extensions', JSON.stringify(extensions));

    return this.http.post<ResponseBody>(API_URL, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  update(model: DeviceType, option?: BaseOptionAPI): Observable<ResponseBody> {    
    model = this.processDeviceType(model);
    let extensions = model.extensions;
    let formData = new FormData();
    Object.keys(model).forEach(x => {
      formData.set(x, model[x]);
    });
    formData.set('ioSetting', JSON.stringify(model.ioSetting));
    formData.set('extensions', JSON.stringify(extensions));

    return this.http.put<ResponseBody>(API_URL + "/" + model.id, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  private processDeviceType(model: DeviceType) {
    if (model.commands.length > 0) {
      // let command = {};
      // model.commands.forEach(x => {
      //   command[x.name] = x.commandStr;
      // });
      model.commands = model.commands.map((x,i)=>{
        x.index = i+1;
        return x;
      });
      model['command'] = JSON.stringify(model.commands);
    }
    else{
      model['command'] =  JSON.stringify(new Array());
    }
    if (typeof model.command == "object") model.command = JSON.stringify(model.command);
    delete model['commands'];
    return model;
  }
}

