import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../../../../src/environments/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { SimType } from '../_models/sim-type';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';
import { tap } from 'rxjs/operators';

const API_SIM_TYPE = environment.api.host + "/admin/sim";

@Injectable({
  providedIn: 'root'
})
export class SimTypeService {
 

  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(option?: BaseOptionAPI): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(API_SIM_TYPE, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        if (body.result.content) {
          body.result.content = body.result.content.map(function (x) {
            let tranfrom = new SimType().deserialize(x, {
              tranform: {
                'id': 'id',
                createdDate: 'createdAt',
                name: 'nameKey',
              }
            });
            return tranfrom;
          });
        }
      }));
  }

  create(simType: SimType, option: BaseOptionAPI): Observable<ResponseBody> {
    const formData = this.convertFormData(simType);
    return this.http.post<ResponseBody>(API_SIM_TYPE, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  update(simType: SimType, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = this.convertFormData(simType);
    return this.http.put<ResponseBody>(API_SIM_TYPE + "/" + simType.id, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_SIM_TYPE}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  private convertFormData(model:SimType){
    let formData = new FormData();
    Object.keys(model).forEach(x => {
      formData.set(x, model[x]);
    });
    return formData;
  }
}
