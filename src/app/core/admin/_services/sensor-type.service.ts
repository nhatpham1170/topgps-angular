import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { SensorType } from '../_models/sensor-type';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const API_SENSOR_TYPE = environment.api.host + "/admin/sensor-template";

@Injectable({
  providedIn:'root'
})
export class SensorTypeService {

  constructor(private http: HttpClient,private utils:TypesUtilsService) { }
  list(params:  any | undefined): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(API_SENSOR_TYPE, {params:params});
  }
 
  create(sensorType: SensorType, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(sensorType).forEach(x => {
      formData.set(x, sensorType[x]);
    });
    return this.http.post<ResponseBody>(API_SENSOR_TYPE, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  update(sensorType: SensorType, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(sensorType).forEach(x => {
      formData.set(x, sensorType[x]);
    });
    return this.http.put<ResponseBody>(API_SENSOR_TYPE + "/" + sensorType.id, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_SENSOR_TYPE}/${id}`;
    return this.http.delete<ResponseBody>(url,{
      params:this.utils.processBassOptionApi(option),
    });
  }
}
