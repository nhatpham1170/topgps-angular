import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment';
import { tap } from 'rxjs/operators';;

const API_URL_COUNTRIES: string = environment.api.host + "/geo/countries";
const API_URL_PROVINCES: string = environment.api.host + "/geo/provinces";

@Injectable({
  providedIn: 'root'
})
export class GeoService {
  constructor(private http: HttpClient, private utils: TypesUtilsService) {;
}
  listCountry(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL_COUNTRIES, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  listProvince(params:  any | undefined): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(API_URL_PROVINCES ,  {params:params});
  }
 
}