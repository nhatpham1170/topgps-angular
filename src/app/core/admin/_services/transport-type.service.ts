import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { TransportType } from '../_models/transport-type';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const API_TRANSPORT_TYPE = environment.api.host + "/admin/transport-type";

@Injectable({
  providedIn:'root'
})
export class TransportTypeService {

  constructor(private http: HttpClient,private utils:TypesUtilsService) { }
  list(params:  any | undefined): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(API_TRANSPORT_TYPE, {params:params});
  }
 
  create(transportType: TransportType, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(transportType).forEach(x => {
      formData.set(x, transportType[x]);
    });
    return this.http.post<ResponseBody>(API_TRANSPORT_TYPE, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  update(transportType: TransportType, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(transportType).forEach(x => {
      formData.set(x, transportType[x]);
    });
    return this.http.put<ResponseBody>(API_TRANSPORT_TYPE + "/" + transportType.id, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  test(encodedPoint, selectedType) {
    // let formData = new FormData();
    // Object.keys(transportType).forEach(x => {
    //   formData.set(x, transportType[x]);
    // });
    return this.http.get(environment.api.host + '/report/toll-estimated', {
      params: {
        points: encodedPoint,
        transportType: selectedType
      }
    })
  }
  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_TRANSPORT_TYPE}/${id}`;
    return this.http.delete<ResponseBody>(url,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  sortOrder(params: { id: number, sortOrder: number }[], option?: BaseOptionAPI): Observable<ResponseBody> {
    let data = params.map(x => JSON.stringify(x)).join(';');
    const formData = new FormData();
    formData.append("data", data);

    return this.http.put<ResponseBody>(API_TRANSPORT_TYPE + "/sort-order", formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
}
