import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { PoiType } from '../_models/poi-type';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const API_POI_TYPE = environment.api.host + "/admin/poi-types";

@Injectable({
  providedIn:'root'
})
export class PoiTypeService {

  constructor(private http: HttpClient,private utils:TypesUtilsService) { }
  list(params:  any | undefined): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(API_POI_TYPE ,  {params:params});
  }
 
  create(params : PoiType, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(params).forEach(x => {
      formData.set(x, params[x]);
    });
    return this.http.post<ResponseBody>(API_POI_TYPE, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  update(params : PoiType, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(params).forEach(x => {
      formData.set(x, params[x]);
    });
    return this.http.put<ResponseBody>(API_POI_TYPE + "/" + params.id, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_POI_TYPE}/${id}`;
    return this.http.delete<ResponseBody>(url,{
      params:this.utils.processBassOptionApi(option),
    });
  }
}

export class ListIconPoiType {
  getIconByKey(key)
  {
    let iconClass = '';
    
    let list = [
      {
        id:'hospital',
        icon:'icon icon-hospital'
      },
      {
        id:'school',
        icon:'icon icon-graduted-hat'
      },
      {
        id:'other',
        icon:'icon icon-place-marker-2'
      },
      {
        id:'home',
        icon:'icon icon-home'
      },
      {
        id:'supermarket',
        icon:'icon icon-supermarket'

      }, 
      {
        id:'company',
        icon:'icon icon-company'
      },
      {
        id:'restaurant',
        icon:'icon icon-restaurant'
      },
      {
        id:'stadium',
        icon:'icon icon-stadium2'
      }
    ];
    let typePoi = list.find(x =>x.id == key);
    if(typePoi)  iconClass = typePoi.icon;
    if(!typePoi) iconClass = list.find(x =>x.id == 'other').icon;
    return iconClass;
  }
}
