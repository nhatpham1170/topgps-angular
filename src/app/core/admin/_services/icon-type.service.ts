import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { IconType } from '../_models/icon-type';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const API_ICON_TYPE = environment.api.host + "/admin/device-icon";

@Injectable({
  providedIn:'root'
})
export class IconTypeService {

  constructor(private http: HttpClient,private utils:TypesUtilsService) { }
  list(params:  any | undefined): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(API_ICON_TYPE ,  {params:params});
  }
 
  create(formData, option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.post<ResponseBody>(API_ICON_TYPE, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  update(formData, option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.put<ResponseBody>(API_ICON_TYPE+'/'+formData.get('id'), formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_ICON_TYPE}/${id}`;
    return this.http.delete<ResponseBody>(url,{
      params:this.utils.processBassOptionApi(option),
    });
  }
}
