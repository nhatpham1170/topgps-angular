import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { TransportType } from '../_models/transport-type';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const API_TRANSPORT_TYPE = environment.api.host + "/admin/transport-type-qcvn";

@Injectable({
  providedIn:'root'
})
export class TransportTypeServiceQCVN {

  constructor(private http: HttpClient,private utils:TypesUtilsService) { }
  list(params:  any | undefined): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(API_TRANSPORT_TYPE, {params:params});
  }
 
  create(transportType: TransportType, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(transportType).forEach(x => {
      formData.set(x, transportType[x]);
    });
    return this.http.post<ResponseBody>(API_TRANSPORT_TYPE, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  update(transportType: TransportType, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(transportType).forEach(x => {
      formData.set(x, transportType[x]);
    });
    return this.http.put<ResponseBody>(API_TRANSPORT_TYPE + "/" + transportType.id, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_TRANSPORT_TYPE}/${id}`;
    return this.http.delete<ResponseBody>(url,{
      params:this.utils.processBassOptionApi(option),
    });
  }
}
