import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment';
import { tap } from 'rxjs/operators';;
import { TollStationModel } from '../_models/toll-station';

const API_URL: string = environment.api.host + "/admin/toll-stations";
@Injectable({
  providedIn: 'root'
})
export class TollStationService {

  constructor(private http: HttpClient, private utils: TypesUtilsService) {;
}
  list(option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_URL, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
    );
  }
  detail(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`;
    return this.http.get<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      tap(body => {
        body.result["latlngs"] = [...body.result.points];
        body.result["points"] = body.result.points.map(x => [x.lat, x.lng]);

      })
    );
  }

  changeCurrency(oldCurrency,newCurrency)
  {
    let currency = oldCurrency+"_"+newCurrency;
    let option = {
      q:currency,
      compact:'ultra',
      apiKey:'88d46662bed1c0184259'
    };
    let reqHeader = new HttpHeaders()
    .set('Auth', 'False');
    return this.http.get('https://free.currconv.com/api/v7/convert',{
      headers:reqHeader,
      params:option
    }).pipe(
      tap(body => {
        body['value'] = body[currency];
      })
    );;
  }

  listCurrency()
  {
    let option = {
      apiKey:'88d46662bed1c0184259'
    };
    let reqHeader = new HttpHeaders()
    .set('Auth', 'False');
    return this.http.get('https://free.currconv.com/api/v7/currencies',{
      headers:reqHeader,
      params:option
    });

  }

  create(toll: TollStationModel, option?: BaseOptionAPI): Observable<ResponseBody> {
  

    const formData = new FormData();
    formData.append("name", toll.name);
    // formData.append("poiType", model.poiType);
    if(toll.active) toll.active = 1;
    if(!toll.active) toll.active = 0;
    if(toll.endTime != null) formData.append("endTime", toll.endTime.toString());
    if(toll.startTime != null) formData.append("startTime", toll.startTime.toString());
    
    if(toll.info.length > 0) formData.append("info", JSON.stringify(toll.info));

    formData.append("description", toll.description);
    formData.append("active", toll.active.toString());
    formData.append("countryId", toll.countryId.toString());
    formData.append("provinceId", toll.provinceId.toString());
    formData.append("roadId", toll.roadId.toString());

    formData.append("radius", toll.radius.toString());

    formData.append("type", toll.type.toString());

    formData.append("neLat", toll.neLat.toString());
    formData.append("neLng", toll.neLng.toString());
    formData.append("swLat", toll.swLat.toString());
    formData.append("swLng", toll.swLng.toString());
    formData.append("currencyUnit", toll.currencyUnit.toString());

    formData.append("encodedPoints", toll.encodedPoints.toString());
    formData.append("typeGeofence", toll.typeGeofence.toString());


    // formData.append("latitude", model.latitude.toString());
    // formData.append("longitude", model.longitude.toString());

    return this.http.post<ResponseBody>(API_URL, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
  update(toll: TollStationModel, option?: BaseOptionAPI): Observable<ResponseBody> {
    let model = new TollStationModel();
    model = this.utils.transformObject(toll, model);
    const formData = new FormData();
    formData.append("name", model.name);
    // formData.append("poiType", model.poiType);
    if(model.active) model.active = 1;
    if(!model.active) model.active = 0;
    if(model.endTime != null) formData.append("endTime", model.endTime.toString());
    if(model.startTime != null) formData.append("startTime", model.startTime.toString());
    if(model.info.length > 0) formData.append("info", JSON.stringify(model.info));

    formData.append("description", model.description);
    formData.append("active", model.active.toString());
    formData.append("countryId", model.countryId.toString());
    formData.append("provinceId", model.provinceId.toString());
    formData.append("roadId", model.roadId.toString());

    formData.append("radius", model.radius.toString());

    formData.append("type", model.type.toString());
    formData.append("currencyUnit", model.currencyUnit.toString());

    formData.append("neLat", model.neLat.toString());
    formData.append("neLng", model.neLng.toString());
    formData.append("swLat", model.swLat.toString());
    formData.append("swLng", model.swLng.toString());
    formData.append("encodedPoints", model.encodedPoints.toString());
    formData.append("typeGeofence", model.typeGeofence.toString());

    return this.http.put<ResponseBody>(API_URL+'/'+model.id, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  updateTest(toll, option?: BaseOptionAPI): Observable<ResponseBody> {
    let model = new TollStationModel();
    model = this.utils.transformObject(toll, model);
    const formData = new FormData();
    // formData.append("poiType", model.poiType);
    formData.append("neLat", model.neLat.toString());
    formData.append("neLng", model.neLng.toString());
    formData.append("swLat", model.swLat.toString());
    formData.append("swLng", model.swLng.toString());
    return this.http.put<ResponseBody>(API_URL+'/'+model.id, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  changeActive(params: { id: number, active: number }, option?: BaseOptionAPI) {

    const formData = new FormData();
    const url = `${API_URL}/${params.id}`;
    formData.append("active", params.active.toString());

    return this.http.put<ResponseBody>(url, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_URL}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }
}