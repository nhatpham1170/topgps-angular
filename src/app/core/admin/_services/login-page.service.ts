import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { LoginPage } from '../_models/login-page';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';
import { tap } from 'rxjs/operators';

const API_LOGIN_TYPE = environment.api.host + "/admin/login-pages";
const KEY_LOGIN_PAGE: string = "KEY_LOGIN_PAGE";
const KEY_CHECK_LOGIN_PAGE: string = "KEY_CHECK_LOGIN_PAGE";

@Injectable({
  providedIn: 'root'
})
export class LoginPageService {

  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
  list(params: any | undefined): Observable<ResponseBody> {
    return this.http.get<ResponseBody>(API_LOGIN_TYPE, { params: params });
  }

  create(params: any, option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.post<ResponseBody>(API_LOGIN_TYPE, params, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  update(params: any, option?: BaseOptionAPI): Observable<ResponseBody> {
    return this.http.put<ResponseBody>(API_LOGIN_TYPE + "/" + params.get('id'), params, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_LOGIN_TYPE}/${id}`;
    return this.http.delete<ResponseBody>(url, {
      params: this.utils.processBassOptionApi(option),
    });
  }

  host(name: string): Observable<ResponseBody> {
    let reqHeader = new HttpHeaders()
      .set('Auth', 'False')
    return this.http.get<ResponseBody>(API_LOGIN_TYPE + '/host', 
      {
        params:{name:name},
        headers:reqHeader
      }).pipe(
        tap(result=>{
          localStorage.setItem(KEY_LOGIN_PAGE,JSON.stringify(result.result));
        })
      );
  }
  getCacheHost(){
    let host = localStorage.getItem(KEY_LOGIN_PAGE);
    if(host) return JSON.parse(host);
    else return {};
  }
}
