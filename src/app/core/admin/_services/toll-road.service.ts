import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { tollRoad } from '../_models/toll-road';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const API_TOLL_ROAD = environment.api.host + "/admin/toll-roads";

@Injectable({
  providedIn:'root'
})
export class TollRoadService {

  constructor(private http: HttpClient,private utils:TypesUtilsService) { }
  list(params?:  any | undefined): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(API_TOLL_ROAD, {params:params});
  }
 
  create(tollRoad: tollRoad, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(tollRoad).forEach(x => {
      formData.set(x, tollRoad[x]);
    });
    return this.http.post<ResponseBody>(API_TOLL_ROAD, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  update(tollRoad: tollRoad, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(tollRoad).forEach(x => {
      formData.set(x, tollRoad[x]);
    });
    return this.http.put<ResponseBody>(API_TOLL_ROAD + "/" + tollRoad.id, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_TOLL_ROAD}/${id}`;
    return this.http.delete<ResponseBody>(url,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  sortOrder(params: { id: number, sortOrder: number }[], option?: BaseOptionAPI): Observable<ResponseBody> {
    let data = params.map(x => JSON.stringify(x)).join(';');
    const formData = new FormData();
    formData.append("data", data);

    return this.http.put<ResponseBody>(API_TOLL_ROAD + "/sort-order", formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
}
