import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { LoginPage } from '../_models/login-page';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const SYSTEM_LOG = environment.api.host + "/system-log";

@Injectable({
  providedIn:'root'
})
export class SystemChangeLogService {

  constructor(private http: HttpClient,private utils:TypesUtilsService) { }
  list(params:  any | undefined ,option?: BaseOptionAPI): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(SYSTEM_LOG, {params:params});
  }
 
  create(params:any, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(params).forEach(x => {
        formData.set(x, params[x]);
      });  
    return this.http.post<ResponseBody>(SYSTEM_LOG, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  update(params:any, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(params).forEach(x => {
        formData.set(x, params[x]);
      });  
    return this.http.put<ResponseBody>(SYSTEM_LOG + "/" + params.id, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${SYSTEM_LOG}/${id}`;
    return this.http.delete<ResponseBody>(url,{
      params:this.utils.processBassOptionApi(option),
    });
  }
}
