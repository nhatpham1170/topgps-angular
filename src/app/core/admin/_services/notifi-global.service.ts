import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { Notifi } from '../_models/notifi';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const API_NOTIFI = environment.api.host + "/admin/notices";

@Injectable({
  providedIn:'root'
})
export class NotifiGlobalService {

  constructor(private http: HttpClient,private utils:TypesUtilsService) { }
  list(params:  any | undefined, option?: BaseOptionAPI): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(API_NOTIFI, {params:params});
  }
  
  listActive(params:  any | undefined, option?: BaseOptionAPI): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(environment.api.host + "/admin/notices-active", {params:params});
  }
 
  create(notification: Notifi, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(notification).forEach(x => {
      formData.set(x, notification[x]);
    });
    return this.http.post<ResponseBody>(API_NOTIFI, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  update(notification: Notifi, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(notification).forEach(x => {
      formData.set(x, notification[x]);
    });
    return this.http.put<ResponseBody>(API_NOTIFI + "/" + notification.id, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_NOTIFI}/${id}`;
    return this.http.delete<ResponseBody>(url,{
      params:this.utils.processBassOptionApi(option),
    });
  }
}
