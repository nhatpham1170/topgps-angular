import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment.prod';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { tollSegment } from '../_models/toll-segment';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService } from '@core/_base/crud';

const API_TOLL_SEGMENT = environment.api.host + "/admin/toll-segments";

@Injectable({
  providedIn:'root'
})
export class TollsegmentService {

  constructor(private http: HttpClient,private utils:TypesUtilsService) { }
  list(params:  any | undefined): Observable<ResponseBody> {    
    return this.http.get<ResponseBody>(API_TOLL_SEGMENT, {params:params});
  }
 
  create(tollSegment: tollSegment, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(tollSegment).forEach(x => {
      formData.set(x, tollSegment[x]);
    });
    return this.http.post<ResponseBody>(API_TOLL_SEGMENT, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  update(tollSegment: tollSegment, option?: BaseOptionAPI): Observable<ResponseBody> {
    let formData = new FormData();
    Object.keys(tollSegment).forEach(x => {
      formData.set(x, tollSegment[x]);
    });
    return this.http.put<ResponseBody>(API_TOLL_SEGMENT + "/" + tollSegment.id, formData,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  delete(id: number, option?: BaseOptionAPI): Observable<ResponseBody> {
    const url = `${API_TOLL_SEGMENT}/${id}`;
    return this.http.delete<ResponseBody>(url,{
      params:this.utils.processBassOptionApi(option),
    });
  }

  sortOrder(params: { id: number, sortOrder: number }[], option?: BaseOptionAPI): Observable<ResponseBody> {
    let data = params.map(x => JSON.stringify(x)).join(';');
    const formData = new FormData();
    formData.append("data", data);

    return this.http.put<ResponseBody>(API_TOLL_SEGMENT + "/sort-order", formData, {
      params: this.utils.processBassOptionApi(option),
    });
  }
}
