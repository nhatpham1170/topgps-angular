export class LoginPage {
    id: number;
    host: string;
    website:string;
    companyName:string;
    description:string;
    layout:string;
    logo:string;
    icon?:string;
    background?:any;
    iosUrl:string;
    androidUrl:string;
    createdAt : string;
    createdUpdate: string;
    title?:string;
    config?:any;
    constructor(obj?:any){
        return Object.assign({},this,obj);
    }
    clear() {
       this.id = undefined;
       this.host = '';
       this.website = '';
       this.companyName = '';
       this.description = '';
       this.logo = '';
       this.iosUrl = '';
       this.androidUrl = '';
       this.createdAt = '';
       this.createdUpdate = '';
       this.title = "";
       this.config;
    }
}

