export class IconType {
    id: number;
    name: string;
    nameKey: string;
    icon: string;
    iconMap: string;
    iconSVG: string;
    iconMapSVG:string;
    sortOrder:number;
    createBy : string;
    modifiedBy: string;
    createdAt : string;
    updateAt: string;
    constructor(obj?:any){
        return Object.assign({},this,obj);
    }
    clear() {
        this.id = undefined;
        this.name = "";
        this.nameKey = "";
        this.icon = "";
        this.iconMap = "";
        this.iconSVG = "";
        this.iconMapSVG = "";
        this.modifiedBy = "";
        this.createBy = "";
        this.createdAt = null;
        this.updateAt = null;
        this.sortOrder = 0;
    }
}

