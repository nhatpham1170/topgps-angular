import { BaseModelCT } from '@core/_base/crud/models/_base.model.ct';

export class Deparment extends BaseModelCT {
    id: number;
    name: string;
    description: string;
    createBy: string;
    createAt: string;
    constructor() {
        super();
        this.id = 0;
        this.name = "";
        this.description = "";
        this.createBy = "";
        this.createAt = "";
    }
    clear() {
        this.id = 0;
        this.name = "";
        this.description = "";
        this.createBy = "";
        this.createAt = "";
    }
}
