import { BaseModelCT } from '../../_base/crud/models/_base.model.ct';

export class SimType extends BaseModelCT {
    id: number;
    nameKey: string;
    sortOrder: number;
    createdDate: string;
    name: string;
    clear() {
        this.id = undefined;
        this.nameKey = "";
        this.name = "";
        this.sortOrder = 1;
        this.createdDate = null;
    }

}
