export class tollSegment{
    id: number;
    name: string;
    roadId:number;
    entryStationId:number;
    exitStationId:number;
    distance:number;
    unit:string;
    isTwoWay:boolean;
    info:any;
    stationIds:any;
    createdBy : string; 
    createdAt : string;
    constructor(obj?:any){
        return Object.assign({},this,obj);
    }
    clear() {
        this.id = undefined;
        this.name = "";
        this.createdAt = null;
        this.createdBy = '';
    }
}
