export class Notifi {
    id: number;
    title: string;
    titleVi: string;
    content: string;
    contentVi:string;
    timeStart:string;
    timeEnd:string;
    createdAt : string;
    createdUpdate: string;
    constructor(obj?:any){
        return Object.assign({},this,obj);
    }
    clear() {
      
    }
}
