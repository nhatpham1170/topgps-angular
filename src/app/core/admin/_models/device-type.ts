import { BaseModelCT } from '@core/_base/crud/models/_base.model.ct';
import { IoSetting } from '@core/manage/_models/sensor';

export class DeviceType extends BaseModelCT {
    id: number;
    name: string;
    description: string;
    protocol:string;
    modelName:string;
    manufacturer:string;
    link:string;
    image:string;
    updatedBy: string;
    updatedDate: any;
    createdBy: string;
    createdDate: any;
    sortOrder: number;
    commands: Command[];
    command: any;
    ioSetting: IoSetting[];
    extensions:any;

    constructor() {
        super();
        this.id = 0;
        this.name = "";
        this.description = "";
        this.updatedBy = "";
        this.updatedDate = "";
        this.createdBy = "";
        this.createdDate = "";
        this.sortOrder = 1;
        this.commands = [];
        this.command = "";
        this.extensions = ''
    }
    clear() {
        this.id = 0;
        this.name = "";
        this.description = "";
        this.updatedBy = "";
        this.updatedDate = "";
        this.createdBy = "";
        this.createdDate = "";
        this.sortOrder = 1;
        this.commands = [];
        this.command = "";
        this.extensions = ''
    }
}
export class Command {
    name: string;
    commandStr: string;
    onPasswork: number;
    index?:number;
    constructor() {
        this.name = "";
        this.commandStr = "";
        this.onPasswork = 0;
    }

}
