export class TollStationModel {
    id: number;
    name:string;
    type:number;
    active:number;
    neLat:number;
    neLng:number;
    swLat:number; 
    swLng:number;
    radius:number;
    countryId:number;
    roadId:number;
    provinceId:string;
    description:string;
    startTime:string;
    endTime:string;
    encodedPoints: string;
    info:any;
    typeGeofence:string;
    color: string;
    fill: boolean;
    fillColor: string;
    fillOpacity: number;
    opacity: number;
    stroke: true;
    weight: number;
    currencyUnit:string;
    
    points: Array<[number,number]>;
    latLngBounds:{
        _northEast:{
            lat:number,
            lng:number
        },
        _southWest:{
            lat:number,
            lng:number
        }    
    };
    latlngs:Array<{lat:number,lng:number}>
    constructor(obj?: any) {
        this.id = undefined;
        this.name= '';
        this.type= 0;
        this.active= 1;
        this.neLat= 0;
        this.neLng= 0;
        this.swLat= 0; 
        this.swLng= 0;
        this.radius= 0;  
      this.color = "#3388ff";
        this.fill = true;
        this.fillColor = "#3388ff";
        this.fillOpacity = 0.2;
        this.opacity = 0.5;
        this.stroke = true;
        this.weight = 4;
        this.countryId= 237;
        this.provinceId= '';
        this.description= '';
        this.startTime= null;
        this.endTime= null;
        this.encodedPoints= '';
        this.typeGeofence = '';
        this.currencyUnit = "VND";
        this.latlngs = [];
        this.info = '';
        this.latLngBounds = {
            _northEast:{
                lat:0,
                lng:0
            },
            _southWest:{
                lat:0,
                lng:0
            }    
        };
        Object.assign(this, obj);
        if(this.latlngs.length==0 && this.points && this.points.length>0){
            
        }
        return Object.assign({}, this, obj);
    }
    clear() {
        this.id = undefined;
        this.name= '';
        this.typeGeofence = '';
        this.type= 0;
        this.active= 1;
        this.neLng= 0;
        this.neLat= 0;
        this.swLng= 0; 
        this.swLat= 0;
        this.radius= 0;
        this.countryId= 237;
        this.opacity = 0.5;
        this.stroke = true;
        this.weight = 4;
        this.provinceId= '';
        this.description= '';
        this.startTime= '';
        this.endTime= '';
        this.encodedPoints= '';
        this.color = "#3388ff";
        this.fill = true;
        this.fillColor = "#3388ff";
        this.fillOpacity = 0.2;
        this.latlngs = [];
        this.currencyUnit = "VND";
        this.info = [
            {
                
            }
        ];
        this.latLngBounds = {
            _northEast:{
                lat:0,
                lng:0
            },
            _southWest:{
                lat:0,
                lng:0
            }    
        };
    }
}
