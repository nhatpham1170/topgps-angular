export class PoiType {
    id: number;
    name: string;
    type: string;
    description: string;
    createBy : string;
    modifiedBy: string;
    createdAt : string;
    updateAt: string;
    fillColor:string;
    constructor(obj?:any){
        return Object.assign({},this,obj);
    }
    clear() {
        this.id = undefined;
        this.name = "";
        this.type = "";
        this.fillColor = '';
        this.modifiedBy = "";
        this.createBy = "";
        this.createdAt = null;
        this.updateAt = null;
    }
}

