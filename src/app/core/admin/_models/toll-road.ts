export class tollRoad{
    id: number;
    name: string;
    type: number;
    countryId: number;
    createdBy : string; 
    createdAt : string;
    constructor(obj?:any){
        return Object.assign({},this,obj);
    }
    clear() {
        this.id = undefined;
        this.name = "";
        this.type = 0;
        this.countryId = 0;
        this.createdAt = null;
        this.createdBy = '';
    }
}
