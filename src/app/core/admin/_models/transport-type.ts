export class TransportType{
    id: number;
    name: string;
    nameKey: string;
    limitSpeed: number;
    qcvnCode: number;
    description:string;
    createdBy : string; 
    createdAt : string;
    constructor(obj?:any){
        return Object.assign({},this,obj);
    }
    clear() {
        this.id = undefined;
        this.name = "";
        this.nameKey = "";
        this.limitSpeed = 0;
        this.qcvnCode = 100;
        this.description = '';
        this.createdAt = null;
        this.createdBy = '';
    }
}
