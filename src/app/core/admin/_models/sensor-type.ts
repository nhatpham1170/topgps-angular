export class SensorType {
    id: number;
    name: string;
    typeSensor: string;
    description: string;
    editBy:string;
    parameters : any;
    createdAt : string;
    createdUpdate: string;
    constructor(obj?:any){
        return Object.assign({},this,obj);
    }
    clear() {
        this.id = undefined;
        this.name = "";
        this.typeSensor = "";
        this.description = "";
        this.editBy = "";
        this.parameters = [{
            key : '',
            value : ''
        }];
        this.createdAt = null;
        this.createdUpdate = null;
    }
}
