import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { AsciiPipe } from '@core/_base/layout';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Item } from '@core/utils/map/_model/item';

@Component({
  selector: 'kt-cluster-popup',
  templateUrl: './cluster-popup.component.html',
  styleUrls: ['./cluster-popup.component.scss'],
  providers: [AsciiPipe]
})
export class ClusterPopupComponent implements OnInit {

  public data:any;
  public dataView: any;
  public keySearch: string;
  public clusterOption:any;
  public infoBoxConfig:any;
  // public inputSearch;
  @ViewChild('inputSearch', { static: false }) inputSearch: ElementRef;
  // @ViewChild('svgContent', { static: true }) svgContent: ElementRef;
  constructor(private cdr: ChangeDetectorRef,
    private asci: AsciiPipe,
    private sanitizer: DomSanitizer) {
    this.data = [];
    this.keySearch = "";
    this.dataView = [];
  }
  ngOnInit() {
    let _this = this;    
    this.data.map(item=>{
      if(typeof item.iconType.icon =='string'){
        item.iconType.icon = 
        _this.sanitizer.bypassSecurityTrustHtml(item.iconType.icon
          .replace('viewBox="0 0 34 34"','viewBox="0 0 24 24"')
          .replace('viewBox="0 0 48 48"','viewBox="0 0 38 38"')
          .replace('transform="rotate({iconRotate})"',"")
          .replace('fill="{iconFill}"',""));
      }    
      return item;
    });
    this.data.sort(function(a, b){
      var x = a.name.toLowerCase();
      var y = b.name.toLowerCase();
      if (x < y) {return -1;}
      if (x > y) {return 1;}
      return 0;
    });    
    this.dataView.map(item=>{
      if(typeof item.iconType.icon =='string'){
        item.iconType.icon = 
        _this.sanitizer.bypassSecurityTrustHtml(item.iconType.icon
          .replace('viewBox="0 0 34 34"','viewBox="0 0 24 24"')
          .replace('transform="rotate({iconRotate})"',"")
          .replace('fill="{iconFill}"',""));
      }
      return item;
    });
    this.dataView.sort(function(a, b){
      var x = a.name.toLowerCase();
      var y = b.name.toLowerCase();
      if (x < y) {return -1;}
      if (x > y) {return 1;}
      return 0;
    });  
  }
  selectItem(item) {
    // console.log(item);

  }
  searchChange($event) {
    let _this = this;
    this.keySearch = $event.target.value;
    this.searchData();
  }
  clearSearch() {
    this.keySearch = "";
    this.inputSearch.nativeElement.value = "";
    this.searchData();
  }
  private searchData() {
    let _this = this;      
    if (this.keySearch.length == 0) this.dataView = [...this.data];  
    this.dataView = this.data.filter(function (x) {
      return _this.asci.transform(x['name']).toLowerCase().includes(_this.asci.transform(_this.keySearch).toLowerCase());
    });
    this.cdr.detectChanges();
  }
}
