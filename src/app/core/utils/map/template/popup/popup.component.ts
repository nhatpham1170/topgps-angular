
import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Item } from '@core/utils/map/_model/item';
import { DeviceMap } from '@core/map/_models/device-map';

@Component({
  selector: 'kt-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {
  public iconOption:Array<number> = [];
  public data:any;
  public class:string;
  public type:string;
  public isShow:boolean;
  constructor(private cdr:ChangeDetectorRef) {
    this.class = "map-unit-tooltip";
    this.isShow = true;
   }
  ngOnInit() {
  } 
  closePopup(data){
    this.isShow = false;
    this.cdr.detectChanges();
  }
}
