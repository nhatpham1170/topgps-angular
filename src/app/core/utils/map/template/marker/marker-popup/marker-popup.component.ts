import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, EventEmitter } from '@angular/core';
import { Item } from '@core/utils/map/_model/item';
import { DeviceMap } from '@core/map/_models/device-map';
import { ResultPopupMarkerEvent } from '@core/utils/map/_model/result-popup-marker-event';
import { ToastService } from '@core/_base/layout';
import { MapUtilityService } from '@core/utils/map/_services/map-util.service';
import { tap, finalize } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { error } from '@angular/compiler/src/util';
import { ToastrService } from 'ngx-toastr';
import objectPath from 'object-path';
// import { ToastService } from '@core/_base/layout';

@Component({
  selector: 'kt-marker-popup',
  templateUrl: './marker-popup.component.html',
  styleUrls: ['./marker-popup.component.scss']
})
export class MarkerPopupComponent implements OnInit {
  public iconOption: Array<number> = [];
  public data: DeviceMap = new DeviceMap();
  public class: string;
  public pinActive: boolean;
  public lockActive: boolean;
  public follow: boolean;
  public type: string;
  public loadingAddress: boolean;
  public geocoderReverse: boolean;
  public markerPopupEvent: EventEmitter<ResultPopupMarkerEvent> = new EventEmitter();
  public infoBoxConfig:any;

  @ViewChild('dataContainer', { static: true }) dataContainer: ElementRef;
  constructor(private cdr: ChangeDetectorRef, private mapUtilService: MapUtilityService,
    // private translateService: TranslateService,
    private toast: ToastService, ) {
    this.class = "map-unit-tooltip";
    this.pinActive = false;
    this.lockActive = false;
    this.follow = false;
    this.loadingAddress = false;

  }

  ngOnInit() {
    this.dataContainer.nativeElement.innerHTML = this.data.iconType.icon
    .replace('viewBox="0 0 34 34"', 'viewBox="0 0 24 24"')
    .replace('viewBox="0 0 48 48"', 'viewBox="0 0 38 38"')
    .replace('transform="rotate({iconRotate})"', "")
    .replace('fill="{iconFill}"', "");
  
  
    this.getAddress(this.data);
  }
  public pinPopupMarker(data) {
    this.pinActive = !this.pinActive;
    let dataResult = new ResultPopupMarkerEvent();
    dataResult.action = "pin";
    dataResult.data.value = Object.assign({}, data);
    dataResult.data.pinActive = this.pinActive;
    this.markerPopupEvent.emit(dataResult);
    this.cdr.detectChanges();
  }
  public clockPopupMarker(data) {
    this.lockActive = !this.lockActive;
    let dataResult = new ResultPopupMarkerEvent();
    dataResult.action = "lock";
    dataResult.data.value = Object.assign({}, data);
    dataResult.data.lockActive = this.lockActive;
    this.markerPopupEvent.emit(dataResult);
    this.cdr.detectChanges();
  }
  public followItem(data) {
    this.follow = !this.follow;
    let dataResult = new ResultPopupMarkerEvent();
    dataResult.action = "follow";
    dataResult.data.value = Object.assign({}, data);
    this.markerPopupEvent.emit(dataResult);
    this.cdr.detectChanges();
  }
  public selectMarker(data){
    // this.follow = !this.follow;
    let dataResult = new ResultPopupMarkerEvent();
    dataResult.action = "select_marker";
    dataResult.data.value = Object.assign({}, data);
    this.markerPopupEvent.emit(dataResult);
    this.cdr.detectChanges();
  }
  private getAddress(item: any) {
    if (item.lat != null && item.lng != null && (!item.address || item.address.length == 0)) {
      let address = item.lat.toFixed(5) + ", " + item.lng.toFixed(5);

      this.loadingAddress = true;
      this.cdr.detectChanges();
      this.mapUtilService.geocode({ params: { lat: item.lat, lng: item.lng } }).pipe(
        tap(data => {
          this.geocoderReverse = true;
          address = data.result.address;
        }, error => {
          this.geocoderReverse = false;
        }),
        finalize(() => {
          this.data.address = address;
          let dataResult = new ResultPopupMarkerEvent();
          dataResult.action = "geocode";
          dataResult.data.value = Object.assign({}, this.data);
          this.markerPopupEvent.emit(dataResult);
          this.loadingAddress = false;
          this.cdr.markForCheck();
          this.cdr.detectChanges();
        })
      ).subscribe();

    }
  }
  copied(val) {
    this.toast.copied(val);
    this.cdr.markForCheck();
    this.cdr.detectChanges();
  }
  checkShow(path:string,deep?:boolean){
    let obj =  objectPath.get(this.infoBoxConfig,path);
    if(deep && obj){
      if(Object.values(obj).some(x=>x===true)) return true;
      else return false
    }
    return objectPath.get(this.infoBoxConfig,path);
  }
  // show popup 

}

