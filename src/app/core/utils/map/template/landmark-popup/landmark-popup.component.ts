import { Component, OnInit, ChangeDetectorRef } from '@angular/core';


@Component({
  selector: 'kt-landmark-popup',
  templateUrl: './landmark-popup.component.html',
  styleUrls: ['./landmark-popup.component.scss']
})
export class LandmarkPopupComponent implements OnInit {
  public iconOption:Array<number> = [];
  public data:any;
  public class:string;
  public type:string;
  public isShow:boolean;
  constructor(private cdr:ChangeDetectorRef) {
    this.class = "map-unit-tooltip";
    this.isShow = true;
   }

  ngOnInit() {
  } 

  closePopup(){
    this.isShow = false;
    this.cdr.detectChanges();
  }

}
