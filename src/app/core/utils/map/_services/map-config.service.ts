import { Injectable } from '@angular/core';
import { StatusType } from '../_model/status-type';
import { MapConfig } from '../_config/map.config';
import { TranslateService } from '@ngx-translate/core';
import { LayoutMapConfigModel } from '../_model/layout-map-config.model';
import {LayoutMapConfig} from '../_config/layout.map.config'
import { Subject } from 'rxjs';

const  KEY_LAYOUT_CONFIG:string = "LAYOUT_MAP_CONFIG";
@Injectable({
  providedIn: 'root'
})
export class MapConfigService {

  public layoutConfigUpdate$: Subject<LayoutMapConfigModel>;

  private layoutConfigModel:LayoutMapConfigModel;
  constructor(private translate: TranslateService) {
    this.layoutConfigUpdate$ = new Subject();
  }
  //
  getConfig() {
    let config = new MapConfig().configs();
    let _this = this;
    Object.keys(config.status).forEach(x => {
      config.status[x].text = this.translate.instant(config.status[x].text);
    })
    return config;
  }
  getLayoutConfig():LayoutMapConfigModel{
    let config = localStorage.getItem(KEY_LAYOUT_CONFIG);
    if(config){    
      this.layoutConfigModel =  Object.assign({},new LayoutMapConfig().default,JSON.parse(config))  as LayoutMapConfigModel      
    }
    else{
      this.layoutConfigModel = new LayoutMapConfig().default;
    }
    return this.layoutConfigModel;
  }
  setLayoutConfig(config:LayoutMapConfigModel):void{
    let temp = Object.assign({},new LayoutMapConfig().default,config);
    this.layoutConfigUpdate$.next(temp);
    localStorage.setItem(KEY_LAYOUT_CONFIG,JSON.stringify(temp));
  }
}
