import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment.prod';
import { tap, delay } from 'rxjs/operators';

const API_URL: string = environment.api.host + "/utility/maps";

@Injectable({
  providedIn: 'root'
})
export class MapUtilityService {

  constructor(private http: HttpClient, private utils: TypesUtilsService) {

  }
  geocode(option?: BaseOptionAPI): Observable<ResponseBody> {
    option.sessionStore = true;
    return this.http.get<ResponseBody>(API_URL + "/geocode", {
      params: this.utils.processBassOptionApi(option),
    }).pipe(
      delay(100)
    );
  }
  poi(){
    return this.http.get<ResponseBody>(API_URL + "/geocode", {
      params: {
        pageNo: '1',
        pageSize: '2',
        userId: '235'
      },
    }).pipe(
      delay(100)
    );
  }
}