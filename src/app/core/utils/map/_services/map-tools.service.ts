import { TollFeePopupComponent } from './../template/toll-fee-popup/toll-fee-popup.component';
import { MapService } from './map.service';
import { SearchResults } from './../_model/search-address';
import { environment } from './../../../../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Injectable, ComponentFactoryResolver, Injector } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import * as L from 'leaflet';
import { MapUtil } from '..';
import { getDistancePoint } from '@core/map/consts/map-tools/distance-point';
import { MarkerOptionCT } from '../utils/map-util';

const API_URL = environment.api.host
@Injectable({
  providedIn: 'root'
})
export class MapToolsService {
  private mapService: MapService;
  private distanceLayer = new L.LayerGroup();
  private poupLayer = new L.LayerGroup();
  private totalDistance = 0;
  public distanceChange = new BehaviorSubject('0m');
  private point: L.LatLngExpression[] = []
  constructor(private http: HttpClient, private mapUtil: MapUtil, private resolver: ComponentFactoryResolver, private injector: Injector) { }

  searchAddress(address: string): Observable<SearchResults> {
    return (this.http.get<SearchResults>(API_URL + '/utility/maps/places', {
      params: {
        textSearch: address
      }
    }))
  }

  setMapService(mapService) {
    this.mapService = mapService
  }

  calculateDistance(isOnRunning) {
    if (isOnRunning) {
      this.mapService.map.on('click', (e) => {
        let distance = '0m';
        this.point.push([e['latlng'].lat, e['latlng'].lng])
        let polyline = this.mapService.createPolyline(
          this.point,
          {
            color: '#03cafc',
            weight: 5,
            opacity: 1,
          },
          {
            isShow: false,
            options: {
              offset: 3,
            },
          });

        if (this.point.length > 1) {
          let latlng1 = this.point[this.point.length - 2]
          let latlng2 = this.point[this.point.length - 1]
          let numDistance = this.mapService.map.distance(latlng1, latlng2);
          let partDistance;
          this.totalDistance += numDistance;
          if (this.totalDistance > 1000) {
            distance = this.formatNumber((this.totalDistance / 1000).toFixed(2)) + ' km'
          } else {
            distance = this.formatNumber(this.totalDistance.toFixed(2)) + ' m'
          }
          if (numDistance > 1000) {
            partDistance = (numDistance / 1000).toFixed(2) + ' km'
          } else {
            partDistance = numDistance.toFixed(2) + ' m'
          }
          let polylinePopup;
          polyline.on('mousemove', (e) => {
            this.poupLayer.clearLayers();
            polylinePopup = this.mapUtil.createMarker(
              [e['latlng'].lat, e['latlng'].lng], { zIndexOffset: -9999 },
              {
                width: 12,
                heigth: 20,
                mapIconUrl: `<p class="polyline-popup">${partDistance}</p>`
              }
            )
            this.poupLayer.addLayer(polylinePopup);
            this.poupLayer.addTo(this.mapService.map)
          });

          polyline.on('mouseout', () => {
            this.poupLayer.clearLayers();
            this.poupLayer.addTo(this.mapService.map)
          })
        }

        let place = this.mapUtil.createMarker(
          [e['latlng'].lat, e['latlng'].lng], { zIndexOffset: -9999 },
          {
            width: 14,
            heigth: 10,
            mapIconUrl: getDistancePoint()
          }
        );
        let distanceMaker = this.mapUtil.createMarker(
          [e['latlng'].lat, e['latlng'].lng], { zIndexOffset: -9999 },
          {
            width: 12,
            heigth: 20,
            mapIconUrl: `<p style="width: max-content; font-weight: bold; color: black">${distance}</p>`
          }
        );
        this.distanceChange.next(distance);
        this.distanceLayer.addLayer(polyline);
        this.distanceLayer.addLayer(place);
        this.distanceLayer.addLayer(distanceMaker);
        this.distanceLayer.addTo(this.mapService.map);
        this.point = [this.point[this.point.length - 1]];
      });
    } else {
      this.mapService.map.removeEventListener('click');
      this.distanceLayer.clearLayers();
      this.distanceLayer.addTo(this.mapService.map);
      this.point = [];
      this.totalDistance = 0;
    }
  }

  formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  calculateTollFee(point, type, neLat, neLng, swLat, swLng) {
    let formData = new FormData();
    formData.set('points', point);
    formData.set('transportType', type);
    formData.set('neLat', neLat);
    formData.set('neLng', neLng);
    formData.set('swLat', swLat);
    formData.set('swLng', swLng);

    return this.http.post(API_URL + '/report/toll-estimated', formData)
  }

  createTollFeePopup(latlng: L.LatLngExpression, options?: L.MarkerOptions, optionCT?: MarkerOptionCT, item?: any): L.Marker {
    let classTooltip = "map-unit-tooltip map-popup-tooltip";
    let classNameMarker = "marker-tooltip";
    let optionCTDefault = {
      width: 30,
      heigth: 100,
      color: "#ff2b2bde",//hex  
      mapIconUrl: `
      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
          width="{width}px" height="{height}px"
          viewBox="0 0 24 24"
          style=" fill:{color};">    
          <path d="M 12 1 C 8.686 1 6 3.686 6 7 C 6 10.323 9.6065625 15.105891 11.226562 17.087891 C 11.627562 17.577891 12.370484 17.577891 12.771484 17.087891 C 14.392484 15.105891 18 10.323 18 7 C 18 3.686 15.314 1 12 1 z M 12 4.8574219 C 13.184 4.8574219 14.142578 5.816 14.142578 7 C 14.142578 8.183 13.183 9.1425781 12 9.1425781 C 10.817 9.1425781 9.8574219 8.184 9.8574219 7 C 9.8574219 5.816 10.816 4.8574219 12 4.8574219 z M 6.9941406 15.564453 C 6.9172363 15.56473 6.8383906 15.574375 6.7597656 15.59375 C 3.9047656 16.29875 2 17.56 2 19 C 2 21.209 6.477 23 12 23 C 17.523 23 22 21.209 22 19 C 22 17.56 20.095234 16.299703 17.240234 15.595703 C 16.611234 15.440703 16 15.926219 16 16.574219 C 16 17.028219 16.302187 17.433969 16.742188 17.542969 C 18.547187 17.990969 19.644219 18.612047 19.949219 18.998047 C 19.434219 19.653047 16.694 20.998047 12 20.998047 C 7.306 20.998047 4.5657813 19.653047 4.0507812 18.998047 C 4.3547813 18.611047 5.4528125 17.990969 7.2578125 17.542969 C 7.6978125 17.434969 8 17.028219 8 16.574219 C 8 16.006344 7.5324707 15.562512 6.9941406 15.564453 z">
          </path>
      </svg>`,
    }
    const factory = this.resolver.resolveComponentFactory(TollFeePopupComponent);
    const component = factory.create(this.injector);
    component.instance.data = item;
    component.instance.class = classTooltip;
    component.changeDetectorRef.detectChanges();
    const landmarkPopupContent = component.location.nativeElement;
    let object: any = {
      icon: L.divIcon({
        html: landmarkPopupContent,
        iconAnchor: [optionCTDefault.width / 2, optionCTDefault.heigth],
        iconSize: [optionCTDefault.width, optionCTDefault.heigth],
        popupAnchor: [0, 0],
        className: classNameMarker,
      }),
      data: item,
    };;
    return L.marker(latlng, object)
  }
}
