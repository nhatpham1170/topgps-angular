import { lankmarkIcon } from '../../../map/consts/landmark/landmark-icon';
import { Injectable, ComponentFactoryResolver, Injector, DoCheck, EventEmitter, ChangeDetectorRef } from '@angular/core';
import * as L from 'leaflet';
import 'leaflet-textpath';
import 'leaflet.gridlayer.googlemutant';
import { MapConfigService } from './map-config.service';
import { MapConfigModel } from '../_model/map-config';
import { MapConfig } from '../_config/map.config';
import { Item } from '../_model/item';
import { MapUtil, MarkerOptionCT } from '../utils/map-util';
import { MarkerPopupComponent } from '../template/marker/marker-popup/marker-popup.component';
import { ClusterPopupComponent } from '../template/cluster/cluster-popup/cluster-popup.component';
import { PopupComponent } from '../template/popup/popup.component';
import { MapType } from '../_model/map-type';
import { GeofenceModel } from '@core/manage';
import { DeviceMap } from '@core/map/_models/device-map';
import { tap, finalize, filter } from 'rxjs/operators';
import { ResultPopupMarkerEvent } from '../_model/result-popup-marker-event';
import { ToastService } from '@core/_base/layout';
import { MapChangeEvent } from '../_model/map-change-event';
import { EventMapService } from '@core/map/_services/map.service';
import objectPath from 'object-path';
import { LandmarkPopupComponent } from '../template/landmark-popup/landmark-popup.component';
import { createLandmarkIcon } from '@core/map/consts/landmark/landmark';
import { Poi, PoiResult } from '@core/map/_models/poi.model';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';

// import * as Polyline from 'polyline-encoded/test/testcases';
// import from 'polyline-encoded';
// var polyline = require("polyline-encoded");

declare var google;
const KEY_CACHE_DEFAULT: string = "KEY_MAP_CONFIG_CONTROLS";

// @Injectable({
//   providedIn: 'root'
// })
export class MapService implements DoCheck {

  public map: L.Map;
  public config: MapConfigModel;
  // data
  private groupMarkers: L.LayerGroup;
  private cluster: L.MarkerClusterGroup;
  private markers: Array<L.Marker>;
  private popup: L.Marker;
  private items: Array<Item>;
  private currentMapType: any;
  private locationMarker: L.Marker;
  public geofences: Array<{ key: any, layer: L.Layer }>;
  public layerGeofence: L.FeatureGroup;
  private itemPin: Item;
  private itemOnTop: Item;
  public lastZoom: number;
  private mapUtil: MapUtil;
  private toastService: ToastService;
  public eventChange: EventEmitter<MapChangeEvent>;
  public eventChangeConfig: EventEmitter<{ config: MapConfigModel, change: any }>;
  public followItems: Array<{ item: any, polyline: L.Polyline }> = [];
  public renderMarker: boolean = true;
  private layerPoi: L.FeatureGroup = new L.FeatureGroup([]);
  private pointLayer: L.FeatureGroup = new L.FeatureGroup([]);
  public lastCluster: boolean;
  public poi: Poi;
  private checkZoom = false;
  private landmarkOnShow = [];
  public listTypePoi;
  savedLandmarks: PoiResult[];
  private lastMaptype: any;
  private savedPoints = {}
  // public key:string = "MAPS_SERVICE";
  ngDoCheck() {

  }
  constructor(private resolver: ComponentFactoryResolver, private injector: Injector, private key?: string) {
    this.mapUtil = new MapUtil();
    this.config = new MapConfig().configs();
    this.markers = [];
    this.items = [];
    this.geofences = [];
    this.eventChangeConfig = new EventEmitter();
    this.eventChange = new EventEmitter();
    if (!this.key) this.key = KEY_CACHE_DEFAULT;

    this.loadConfigFromCache();
  }
  private loadConfigFromCache() {
    let mapConfigControls = localStorage.getItem(this.key);

    if (mapConfigControls) {
      let cacheConfigControls = JSON.parse(mapConfigControls);

      if (this.config.version == cacheConfigControls.version) {
        this.config.infoBox = Object.assign(this.config.infoBox, cacheConfigControls.infoBox);
        if (cacheConfigControls.features) {
          this.lastMaptype = JSON.parse(JSON.stringify(cacheConfigControls.features.mapType));
          Object.keys(cacheConfigControls.features).forEach((x, i, arr) => {
            if (cacheConfigControls.features[x].cache && this.config.features[x] != undefined) {
              this.config.features[x].value = cacheConfigControls.features[x].value;
            }
          });
        }
      }
      // console.log(cacheConfigControls);
      // console.log(this.config.features);
    }
  }

  saveConfigControls() {
    let object = { features: this.config.features, infoBox: this.config.infoBox, version: this.config.version };
    // check maptype google 
    if (this.config.features.mapType.value.includes("google") && this.lastMaptype != undefined) {
      object.features.mapType = JSON.parse(JSON.stringify(this.lastMaptype));
    }
    else {
      this.lastMaptype = JSON.parse(JSON.stringify(object.features.mapType));
    }
    localStorage.setItem(this.key, JSON.stringify(object));
    this.eventChange.emit({ type: "feature__change_config_controls", data: {} });
  }

  getConfigs(): MapConfigModel {
    return this.config;
  }

  getCacheConfigs() {
    // this.configOld = localStorage.getItem(this.key);
    return localStorage.getItem(this.key);
  }

  /**
   * Init map
   * @param config 
   */
  init(config?: MapConfigModel) {
    if (!this.config) {
      this.config = new MapConfig().configs();
    }
    if (config) {
      Object.assign(this.config, config);
    }
    // this.config.options.elementProcessText = `${this.key}_text_${new Date().getMilliseconds().toString()}`;

    let mapType: MapType = this.config.options.mapType;

    let mapTypeKey = objectPath.get(this.config, "features.mapType.value");
    if (mapTypeKey && this.config.mapType[mapTypeKey]) {
      mapType = this.config.mapType[mapTypeKey];
      this.config.options.mapType = mapType;
      this.config.options.maxZoom = objectPath.get(mapType.layer, "option.maxZoom");
    }
    this.currentMapType = L.tileLayer(mapType.layer['link'], mapType.layer['option']);
    this.config.features.mapType.value = mapType.type;

    return {
      layers: [
        this.currentMapType,
      ],
      zoom: this.config.options.zoom,
      center: this.config.options.center,
      zoomControl: false,
    }
  }

  update(items: [Item]) {

  }
  reload(items: [Item]) {

  }
  refresh() {
    let items = [...this.items];

    this.updateMarkers(items);
  }
  resize() {
    if (this.map)
      this.map.invalidateSize();
  }

  // Set map
  setMap(map: L.Map) {
    let _this = this;
    this.map = map;
    this.setBrightness(this.config.features.brightness.value);
    // this.currentMapType.getContainer().style.filter = 'brightness(80%)';
    this.map.setMinZoom(this.config.options.minZoom);
    this.map.setMaxZoom(this.config.options.maxZoom);
    this.lastZoom = this.map.getZoom();
    this.map.on('zoomend', function () {
      _this.changeZoom();
    });
  }
  setBrightness(brightness?: number) {
    if (brightness > 0 && brightness <= 100) {
      this.config.features.brightness.value = brightness;
    }
    this.currentMapType.getContainer().style.filter = 'brightness(' + this.config.features.brightness.value + '%)';
    this.eventChange.emit({ type: "feature__brightness", data: this.config.features.brightness.value });
    // this.saveConfigControls();
  }
  getMap() {
    return this.map;
  }
  fitbound(items?: Array<Item>, options?: L.FitBoundsOptions) {
    let bounds = [];
    if (!items || items.length == 0) {
      this.markers.forEach(x => {
        bounds.push(x.getLatLng());
      });
    }
    else {
      items.forEach(x => {
        bounds.push({ lat: x.lat, lng: x.lng });
      })
    }
    if (bounds.length > 0) {
      // 
      let optionDefault: any = { padding: [20, 20] };
      if (options) Object.assign(optionDefault, options);
      this.map.fitBounds(bounds, optionDefault);
    }
    this.eventChange.emit({ type: "feature__fitbound", data: items });
  }
  fitboundByIds(ids: Array<number>) {
    let bounds = [];
    ids.forEach(x => {
      let marker = this.markers.find(m => m.options['data'].id == x);
      if (marker) {
        bounds.push(marker.getLatLng());
      }
    });
    if (bounds.length > 0) this.map.fitBounds(bounds, { padding: [20, 20], maxZoom: 18 });
    this.eventChange.emit({ type: "feature__fitbound_ids", data: ids });
  }

  // Control action
  zoomIn() {
    let newZoom = this.map.getZoom() - 1;
    if (newZoom >= this.config.options.minZoom && newZoom <= this.config.options.maxZoom) {
      this.map.setZoom(newZoom);
    }
    this.eventChange.emit({ type: "feature__zoom_in", data: {} });
  }

  zoomOut() {
    let newZoom = this.map.getZoom() + 1;
    if (newZoom >= this.config.options.minZoom && newZoom <= this.config.options.maxZoom) {
      this.map.setZoom(newZoom);
    }
    this.eventChange.emit({ type: "feature__zoom_out", data: {} });
  }

  changeZoom() {
    if (this.lastZoom != this.map.getZoom()) {
      this.lastZoom = this.map.getZoom();
      if (this.lastZoom > 10 && this.poi) {
        if (this.checkZoom) {
          this.addLandmark(this.savedLandmarks);
          this.checkZoom = false;
        }
      }
      if (this.lastZoom < 11 && this.poi) {
        this.clearLandmark();
        this.checkZoom = true;
      }
      if (this.lastZoom == this.map.options.maxZoom && this.config.features.cluster.value) {
        this.config.features.cluster.value = false;
        this.lastCluster = true;
        this.groupChange(this.config.features.cluster.value, true);
        this.eventChangeConfig.emit({ config: this.config, change: { cluster: false } });
      }
      if (this.lastZoom < this.map.options.maxZoom && this.lastCluster) {
        this.config.features.cluster.value = true;
        this.lastCluster = false;
        this.eventChangeConfig.emit({ config: this.config, change: { cluster: true } });
        this.groupChange(this.config.features.cluster.value, true);
      }
      this.currentPopupChange("hide");
    }
  }
  onChangeMapConfig() {

  }
  changeMapType(mapType: string) {
    try {
      if (this.config.mapType[mapType]) {
        if (mapType != this.config.features.mapType.value) {
          this.config.features.traffic.show = false;
          this.config.features.traffic.value = false;
        }
        if (mapType.includes('google')) {


          if (google != undefined) {
            this.map.removeLayer(this.currentMapType);
            let params: any = {
              type: mapType.replace("google", "").toLowerCase(),	// valid values are 'roadmap', 'satellite', 'terrain' and 'hybrid'
            };
            let map: any = L.gridLayer.googleMutant(params);
            if (mapType.replace("google", "").toLowerCase() == "roadmap") {
              this.config.features.traffic.show = true;
              if (this.config.features.traffic.value) {
                // map.addGoogleLayer('TrafficLayer');
              }
            }
            this.currentMapType = map;
            this.map.addLayer(map);
            this.setBrightness(this.currentMapType);
          }

        }
        else {
          this.map.removeLayer(this.currentMapType);
          let layer = L.tileLayer(this.config.mapType[mapType].layer['link'], this.config.mapType[mapType].layer['option']);
          this.currentMapType = layer;
          this.map.addLayer(this.currentMapType);
          this.setBrightness(this.currentMapType);
          if (this.map.getZoom() > layer.options.maxZoom) {
            this.map.setZoom(layer.options.maxZoom);
          }
          this.map.setMaxZoom(layer.options.maxZoom);
        }
        this.config.features.mapType.value = mapType;
        this.config.options.maxZoom = this.map.getMaxZoom();
        this.eventChange.emit({ type: "feature__change_map_type", data: mapType });
        this.saveConfigControls();

      }
    }
    catch (ex) {
      console.log("error");
    }


  }
  groupChange(value, auto?: boolean) {
    let _this = this;

    // remove markers layer  
    this.markers.forEach(x => {
      x.remove();
    });

    if (!auto) {
      this.lastCluster = false;
    }
    // add cluster     
    if (value === true) {
      _this.cluster = L.markerClusterGroup({
        iconCreateFunction: function (cluster) {
          let markers = cluster.getAllChildMarkers();
          let html = _this.createClusterLayer(markers);
          return L.divIcon({ html: html, className: 'mapIconCluster', iconSize: L.point(32, 32) });
        },
        spiderfyOnMaxZoom: false,
        showCoverageOnHover: false,
        // zoomToBoundsOnClick: false
      });
      _this.cluster.addLayers(_this.markers);
      _this.cluster.addTo(_this.map);

      //cluster mouseover show cluster tooltip      
      _this.cluster.on('clustermouseover', function (cluster) {
        let popup = _this.createClusterTooltip(cluster);
        _this.currentPopupChange("show", popup);
        _this.popup.on('mouseout', function (a) {
          _this.currentPopupChange("hide");
        });
        cluster['layer'].on('click', function (event) {
          _this.currentPopupChange("hide");
        });
      });
      // fit bound
      // if (this.markers.length > 0 && !auto)
      //   this.map.fitBounds(this.cluster.getBounds(), { padding: [20, 20] });
    }
    else {
      if (this.cluster) this.cluster.remove();

      this.markers.forEach(x => {
        x.addTo(_this.map);
      });
    }
    this.eventChange.emit({ type: "feature__group_change", data: value });
    // this.saveConfigControls();
  }
  location() {
    let _this = this;

    if (this.config.features.location.value) {
      _this.locationMarker.remove();
      _this.config.features.location.value = !_this.config.features.location.value;
      return;
    }
    else {
      this.map.locate({
        setView: true,
        maxZoom: _this.config.options.maxZoom
      });
      this.map.on('locationfound', function (e: any) {
        if (_this.config.features.location.value) return;
        _this.config.features.location.value = !_this.config.features.location.value;
        let object: any = {
          icon: L.divIcon({
            html: `<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 58 58" style="enable-background:new 0 0 58 58;" xml:space="preserve">
            <ellipse style="fill:#23A24D;" cx="29" cy="50" rx="29" ry="8"/>
            <path style="fill:#EBBA16;" d="M41.676,5.324L41.676,5.324c-7.098-7.098-18.607-7.098-25.706,0h0
              C9.574,11.72,8.855,23.763,14.282,31l14.541,21l14.541-21C48.792,23.763,48.072,11.72,41.676,5.324z M29,24c-3.314,0-6-2.686-6-6
              s2.686-6,6-6s6,2.686,6,6S32.314,24,29,24z"/>
            </svg>`,
            iconSize: [25, 41],
            // iconAnchor: [13, 41],
            // popupAnchor: [0, -28],
            className: 'location',

          }),
        };
        let radius = e.accuracy / 2;
        _this.locationMarker = L.marker([e.latlng.lat, e.latlng.lng], object);
        _this.locationMarker.addTo(_this.map);
        return;
      });
      this.map.on('locationerror', function (e: any) {
        alert(e.message);
      });
    }

  }

  getLocation() {
    let _this = this;
    let latlng;
    if (this.config.features.location.value) {
      _this.locationMarker.remove();
      _this.config.features.location.value = !_this.config.features.location.value;
      return;
    }
    else {
      // this.map.locate({
      //   setView: true,
      //   maxZoom: _this.config.options.maxZoom
      // });
      this.map.on('locationfound', function (e: any) {
        if (_this.config.features.location.value) return;
        _this.config.features.location.value = !_this.config.features.location.value;
        latlng = ([e.latlng.lat, e.latlng.lng]);
        console.log('service', latlng);

      });
      this.map.on('locationerror', function (e: any) {
        alert(e.message);
      });
    }
    return latlng;
  }

  traffic(value) {
    if (this.config.features.traffic.show) {
      this.config.features.traffic.value = value;

      if (value) {
        this.currentMapType.addGoogleLayer('TrafficLayer');
      }
      else {
        this.currentMapType.removeGoogleLayer('TrafficLayer');
      }
    }
  }
  changStyleMarker(value: string) {
    this.clearMarkers();
    let items = [...this.items];

    this.updateMarkers(items);
    this.eventChange.emit({ type: "feature__change_style_marker", data: value });
    this.saveConfigControls();
  }
  clearMarkers() {
    let _this = this;
    if (this.config.features.cluster.value) {
      if (this.cluster) {
        this.cluster.remove();
        this.cluster = null;
      }

      this.markers = [];
    }
    else {
      this.markers.forEach(x => {
        x.remove();
      });
      this.markers = [];
    }

  }
  markerPopupChange(value) {
    this.updateMarkers(this.items);
  }

  centerPointChange() {
    this.config.features.centerPoint.value = !this.config.features.centerPoint.value
  }

  followChange(value) {
    this.follows(this.items, value);
  }
  fullScreen(value) {
    this.eventChange.emit({ type: "fullScreen", data: value });
  }

  // ========== Change option addvanced ==========

  /**
   * Change setting cluster on/off
   * @param status 
   */
  setCluster(status: boolean) {

  }
  public clearMap() {
    this.items = [];
    this.config.features.follow.value = false;
    // this.followChange(false);
    this.followItems.forEach(x => {
      x.polyline.remove();
    });
    this.followItems = [];
    this.clearMarkers();
    this.itemPin = undefined;
    this.itemOnTop = undefined;
  }
  public removeMarkers(items: Array<Item>) {
    // 1 remove item
    // 2 remover marker in cluster, marker    
    // 3 remover polyline
    items.forEach(item => {
      let marker = this.markers.find(m => m['options']['data']['id'] == item.id);
      if (marker) {
        marker.remove()
      }
      let followItem = this.followItems.find(fi => fi.item.id == item.id);
      if (followItem) followItem.polyline.remove();
    });
    this.items = this.items.filter(x => items.some(i => i.id != x.id));
    this.eventChange.emit({ type: "action__remove_items", data: items });
  }
  // ========== End change option addvanced ==========

  // ========== Render ==========

  public removeItems(items: Array<Item>) {
    // 1 remove item
    // 2 remover marker in cluster, marker    
    // 3 remover polyline
    items.forEach(item => {
      let marker = this.markers.find(m => m['options']['data']['id'] == item.id);
      if (marker) {
        marker.remove()
      }
    });
    this.items = this.items.filter(x => items.some(i => i.id != x.id));
    this.eventChange.emit({ type: "action__remove_items", data: items });
  }
  public addItems(items: Array<Item>, fitbound?: boolean) {
    this.items = [];
    this.followItems.forEach(x => x.polyline.remove());
    this.followItems = [];
    let _this = this;
    this.clearMarkers();
    this.itemPin = undefined;
    this.itemOnTop = undefined;
    items.forEach(i => {
      let index = _this.items.findIndex(x => x.id == i.id)
      if (index >= 0) {
        _this.items[index] = items[index];
      }
      else {
        _this.items.push(i);
      }
    });
    this.eventChange.emit({ type: "action__add_items", data: items });
  }
  public updateItems(items: Array<Item>, fitbound?: boolean) {

    let _this = this;

    items.forEach(i => {
      let index = _this.items.findIndex(x => x.id == i.id);
      if (index >= 0) {
        let address = _this.items[index].address;
        if (i.lat == _this.items[index].lat && i.lng == _this.items[index].lng && address && address.length > 0) {
          i.address = address;
        }
        _this.items[index] = i;
      }
      else {
        _this.items.push(i);
      }
    });
    this.eventChange.emit({ type: "action__update_items", data: items });
  }

  public updateItemsTracking(items: Array<Item>, fitbound?: boolean) {

    let _this = this;

    items.forEach(i => {
      let index = _this.items.findIndex(x => x.id == i.id);
      if (index >= 0) {
        let address = _this.items[index].address;
        if (i.lat == _this.items[index].lat && i.lng == _this.items[index].lng && address && address.length > 0) {
          i.address = address;
        }
        _this.items[index] = i;
      }
      else {
        _this.items.push(i);
      }
    });
    this.eventChange.emit({ type: "action__update_items_tracking", data: items });
  }

  public addMarkers(items: Array<Item>, fitbound?: boolean) {
    if (!this.renderMarker) return;
    this.items = [];
    this.followItems.forEach(x => x.polyline.remove());
    this.followItems = [];
    let _this = this;
    this.clearMarkers();
    this.itemPin = undefined;
    this.itemOnTop = undefined;
    items.forEach(i => {
      let index = _this.items.findIndex(x => x.id == i.id)
      if (index >= 0) {
        _this.items[index] = items[index];
      }
      else {
        _this.items.push(i);
      }
    });
    items.forEach((item) => {
      if (item.lat != null && item.lng != null && item.statusType.name != this.config.status.expired.name
        && item.statusType.name != this.config.status.inactive.name
        && item.statusType.name != this.config.status.nodata.name
        && (item.lat != 0 && item.lng != 0)) {
        let styleMarker = this.config.features.markerStyle.value;
        let marker;
        let popupNew;
        switch (styleMarker) {
          case "style-two":
            marker = this.createMarker2(item);
            _this.markers.push(marker);
            if (this.config.features.markerPopup.value) {

              marker.on('mouseover', function (a) {
                popupNew = _this.createMarkerToolTip(marker, "style-two");
                _this.currentPopupChange("show", popupNew);
                popupNew.on('mouseout', function (a) {
                  _this.currentPopupChange("hide");
                })
              });

            }
            marker.on('click', function (a) {
              // _this.follow(a.target.options.data);
              _this.focusItem(a.target.options.data);
            });

            break;
          default:
            marker = this.createMarker(item);
            _this.markers.push(marker);
            if (this.config.features.markerPopup.value) {

              marker.on('mouseover', function (a) {
                popupNew = _this.createMarkerToolTip(marker, "style-one");
                _this.currentPopupChange("show", popupNew);
                popupNew.on('mouseout', function (a) {
                  _this.currentPopupChange("hide");
                })
              });

            }
            marker.on('click', function (a) {
              // _this.follow(a.target.options.data);
              _this.focusItem(a.target.options.data);
            });
            break;
        }
      }

    });

    if (this.config.features.cluster.value) {
      _this.cluster = L.markerClusterGroup({
        iconCreateFunction: function (cluster) {
          let markers = cluster.getAllChildMarkers();
          let html = _this.createClusterLayer(markers);
          return L.divIcon({ html: html, className: 'mapIconCluster', iconSize: L.point(32, 32) });
        },
        spiderfyOnMaxZoom: false,
        showCoverageOnHover: false,
      });
      _this.cluster.addLayers(_this.markers);
      _this.cluster.addTo(_this.map);

      //cluster mouseover show cluster tooltip      
      _this.cluster.on('clustermouseover', function (cluster) {
        let popup = _this.createClusterTooltip(cluster);
        _this.currentPopupChange("show", popup);
        popup.on('mouseout', function (a) {
          _this.currentPopupChange("hide");
        });
        cluster['layer'].on('click', function (event) {
          _this.currentPopupChange("hide");
        });
      });
      // fit bound
      if (fitbound && this.markers.length > 0) this.map.fitBounds(this.cluster.getBounds(), { padding: [20, 20], maxZoom: 18 });
    }
    else {
      let bounds = [];
      if (this.groupMarkers) {
        this.groupMarkers.remove();
      }
      this.groupMarkers = new L.LayerGroup(this.markers);
      this.map.addLayer(this.groupMarkers);
      _this.markers.forEach((marker) => {
        // _this.map.addLayer(marker);
        // this.groupMarkers.(this.markers);
        bounds.push(marker.getLatLng());
      });

      if (fitbound && bounds.length > 0) {
        this.map.fitBounds(bounds, { padding: [20, 20], maxZoom: 18 });
      }
    }
    this.eventChange.emit({ type: "action__add_items", data: items });
  }

  updateMarker(item: Item, typeShowPopup?: string) {
    if (!this.renderMarker) return;

    let _this = this;
    let lock = false;

    if (_this.itemPin && item.id == _this.itemPin.id) {
      typeShowPopup = "pin";
      if (_this.itemPin.markerOption && _this.itemPin.markerOption.lock) {
        lock = _this.itemPin.markerOption.lock;
      }
    }

    this.markers.forEach((m, index) => {
      if (m.options['data'].id == item.id) {
        if (_this.config.features.cluster.value) {
          if (_this.cluster) _this.cluster.removeLayer(m);
        }
        else {
          m.remove();
        }
      }

      if (item.feature.hideOnMap === true) {
        let indexFollow = this.followItems.findIndex(x => x.item.id == item.id);
        if (indexFollow >= 0) {
          this.followItems[indexFollow].polyline.remove();
        }
      }

    });
    _this.markers = _this.markers.filter(m => m.options['data'].id != item.id);

    // add marker 
    if (item.lat != null && item.lng != null && item.statusType.name != this.config.status.expired.name
      && item.statusType.name != this.config.status.inactive.name
      && item.statusType.name != this.config.status.nodata.name
      && !item.feature.hideOnMap
      && (item.lat != 0 && item.lng != 0)) {
      let styleMarker = _this.config.features.markerStyle.value;
      let marker;
      let popup;
      let follow = this.followItems.find(x => x.item.id == item.id) ? true : false;
      switch (styleMarker) {
        case "style-two":
          marker = _this.createMarker2(item);
          _this.markers.push(marker);
          if (this.config.features.markerPopup.value) {
            _this.markerPopupTrigger(typeShowPopup, marker, {
              style: "style-two", feature: {
                pinActive: (typeShowPopup == "pin" ? true : false), lockActive: lock, follow: follow
              }
            });
          }
          marker.on('click', function (a) {
            // _this.follow(a.target.options.data);
            _this.focusItem(a.target.options.data);
          });

          break;
        default:
          marker = _this.createMarker(item);
          _this.markers.push(marker);
          if (this.config.features.markerPopup.value) {
            _this.markerPopupTrigger(typeShowPopup, marker,
              {
                style: "style-one", feature: {
                  pinActive: (typeShowPopup == "pin" ? true : false), lockActive: lock, follow: follow
                }
              });
          }
          marker.on('click', function (a) {
            // _this.follow(a.target.options.data);
            _this.focusItem(a.target.options.data);
          });

          break;
      }
      if (_this.config.features.cluster.value) {
        if (!_this.cluster) {
          _this.cluster = L.markerClusterGroup({
            iconCreateFunction: function (cluster) {
              let markers = cluster.getAllChildMarkers();
              let html = _this.createClusterLayer(markers);
              return L.divIcon({ html: html, className: 'mapIconCluster', iconSize: L.point(32, 32) });
            },
            spiderfyOnMaxZoom: false,
            showCoverageOnHover: false,
          });
          // set show on hover
          _this.cluster.addTo(_this.map);
          _this.cluster.on('clustermouseover', function (cluster) {
            let popup = _this.createClusterTooltip(cluster);
            _this.currentPopupChange("show", popup);
            _this.popup.on('mouseout', function (a) {
              _this.currentPopupChange("hide");
            });
            cluster['layer'].on('click', function (event) {
              _this.currentPopupChange("hide");
            });
          });
        }
        _this.cluster.addLayer(marker);
      }
      else {
        this.map.addLayer(marker);
      }

    }
  }
  markerPopupTrigger(typeShowPopup: string, marker, popupOption) {

    // "pin" | "click" | "mouse"    
    let _this = this;

    let popup;
    switch (typeShowPopup) {
      case "pin":
        _this.currentPopupChange("show", popup, undefined, marker.options.data);
        // check lock
        if (_this.itemPin.markerOption && _this.itemPin.markerOption.lock === true) {
          _this.map.panTo([marker.options.data.lat, marker.options.data.lng], { duration: 2000 });
        }
        break;
      case "click":
        break;
      default:
        marker.on('mouseover', function (mrk) {
          popup = _this.createMarkerToolTip(marker, popupOption.style, popupOption.feature);

          _this.currentPopupChange("show", popup);
          popup.on('mouseout', function (a) {
            _this.currentPopupChange("hide");
          })
        });
        break;
    }
  }

  currentPopupChange(type: "show" | "hide", popup?: any, map?: L.Map, item?: any) {

    let _this = this;
    if (item == undefined || (_this.itemPin != undefined && item.id != _this.itemPin.id)) {
      if (_this.itemPin) _this.unPinMarker(_this.itemPin);
    }
    switch (type) {
      case "hide":
        if (_this.popup) _this.popup.remove();
        break;
      case "show":
        if (map) {
          if (popup) map.addLayer(popup);
        }
        else {

          if (_this.popup) _this.popup.remove();
          if (popup) {
            _this.popup = popup;
            _this.map.addLayer(_this.popup);
          }
        }
        break;
    }
  }
  createDevice(item: Item, popup: L.Layer, map: L.Map, style?: "style-one" | "style-two", showPopup?: true | false) {
    let _this = this;
    let marker: L.Layer;
    if (item.lat != null && item.lng != null && item.statusType.name != this.config.status.expired.name
      && (item.lat != 0 && item.lng != 0)) {
      switch (style) {
        case "style-two":
          marker = _this.createMarker2(item);
          if (showPopup) {
            popup = _this.createMarkerToolTip(marker, "style-two");
            marker.on('mouseover', function (marker) {
              if (_this.popup) popup.remove();
              map.addLayer(popup);
              popup.on('mouseout', function (a) {
                popup.remove();
              })
            });
          }
          break;
        default:
          marker = _this.createMarker(item);
          if (showPopup) {
            popup = _this.createMarkerToolTip(marker, "style-one");
            marker.on('mouseover', function (marker) {
              if (popup) popup.remove();
              map.addLayer(popup);
              popup.on('mouseout', function (a) {
                popup.remove();
              })
            });
          }
          break;
      }
    }

    return marker;
  }
  // ========== Render ==========
  public updateMarkers(items: Array<Item>, fitbound?: boolean) {
    if (!this.renderMarker) return;
    let _this = this;

    items.forEach(i => {
      let index = _this.items.findIndex(x => x.id == i.id);
      if (index >= 0) {
        let address = _this.items[index].address;
        if (i.lat == _this.items[index].lat && i.lng == _this.items[index].lng && address && address.length > 0) {
          i.address = address;
        }
        _this.items[index] = i;
      }
      else {
        _this.items.push(i);
      }
      _this.updateMarker(i);
    });

    this.updateFollow(items);
    if (fitbound) {
      let bounds = [];
      _this.markers.forEach((marker) => {
        _this.map.addLayer(marker);
        bounds.push(marker.getLatLng());
      });
      if (fitbound && bounds.length > 0) {
        this.map.fitBounds(bounds, { padding: [20, 20], maxZoom: this.lastZoom });
      }
    }
    this.eventChange.emit({ type: "action__update_items", data: items });

  }

  public addMarker(item: Item) {
    if (!this.renderMarker) return;
    let _this = this;
    let marker = this.createMarker(item);
    this.markers.push(marker);
    if (this.config.features.cluster.show == true) {
      // this.cluster.addLayer(marker);
    }
    else {
      marker.addTo(this.map);
      if (this.config.features.markerPopup.value) {
        let popupNew = _this.createMarkerToolTip(marker, "style-one");
        marker.on('mouseover', function (marker) {
          _this.currentPopupChange("show", popupNew);
          popupNew.on('mouseout', function (a) {
            _this.currentPopupChange("hide");
          })
        });
      }

    }
    if (this.config.features.cluster.show == true) {

      this.cluster = L.markerClusterGroup({
        iconCreateFunction: function (cluster) {
          let markers = cluster.getAllChildMarkers();
          let html = _this.createClusterLayer(markers);
          return L.divIcon({ html: html, className: 'mapIconCluster', iconSize: L.point(32, 32) });
        },
      });
      this.cluster.addLayer(marker);
    }
  }
  private createMarker(item: Item): L.Marker {
    let width = 0;
    let widthText = 0;
    let widthStatus = 0;
    let statusShow = "";
    let elementProcess = document.getElementById(this.config.options.elementProcessText);
    let svgStatus = "";
    let label = item[this.config.infoBox.base.label.value] ? item[this.config.infoBox.base.label.value] : "---";
    let direction = "";
    let icon, svgActive = "";
    let xLabel = 8;
    let offSet = 2;

    // activce 
    // if (this.itemOnTop && item.id == this.itemOnTop.id) {
    //   offSet = 10;
    // }

    elementProcess.getElementsByTagName('text')[0].innerHTML = label;
    width = elementProcess.getElementsByTagName('text')[0].getBoundingClientRect().width + 16 + offSet;

    // check show speed
    if (
      item.statusType.name == this.config.status.run.name
      && item['speed']
      && this.config.infoBox.base.speed) {
      statusShow = item['speed'] + " km/h";
    }

    if (item.statusType.name != this.config.status.run.name && item['statusDurationStr']
      && this.config.infoBox.base.duration) {
      statusShow = item['statusDurationStr'];
    }

    let rect = ` <rect width="{width}" height="22" fill="{mapIconColor}" stroke="#fff" stroke-width="1" rx="3" ry="3"></rect>`;
    let bolderArrow = `<polygon points="0,1 5,6 10,1" fill="#fff"></polygon>`;

    // icon
    if (this.config.infoBox.base.icon) {
      icon = item.iconType.icon;
      xLabel = 25;
      width += 20;
    }
    // activce 
    // if (this.itemOnTop && item.id == this.itemOnTop.id) {
    //   rect = ` <rect width="{width}" height="22" fill="{mapIconColor}" stroke="#5476e5" stroke-width="2" rx="3" ry="3"></rect>`;
    //   bolderArrow = `<polygon points="0,2 5,7 10,2" fill="#5476e5"></polygon>`;
    //   // <polygon points="0,1 4,5  0,9" fill="#7495a6" />
    //   // / <polygon points="  3,1 1,1  1,9 3,9" fill="#ffffff" />
    //   svgActive = `
    //   <svg width="20" x="1" y="1" height="20" stroke="#5476e5" stroke-width="0.9" viewBox="0 0 10 10">
    //     <polygon points="  0,1 4,5 0,9" fill="#5476e5" />
    //   </svg>`;

    // }
    if (icon && offSet > 0) icon = icon.replace('x="3"', 'x="' + (3 + offSet) + '"');
    // status
    if (statusShow.length > 0) {
      svgStatus = `<rect x="{xRectStatus}" width="{wRectStatus}" height="13" fill="#e8e8e8" rx="3" ry="3" y="4"></rect>
      <text x="{xStatus}" y="15" fill="#000" font-size="11" font-weight="bold">{statusShow}</text>`;
      elementProcess.getElementsByTagName('text')[1].innerHTML = statusShow;
      width = elementProcess.getElementsByTagName('text')[0].getBoundingClientRect().width
        + elementProcess.getElementsByTagName('text')[1].getBoundingClientRect().width + xLabel + 27 + offSet;
      widthText = elementProcess.getElementsByTagName('text')[0].getBoundingClientRect().width + xLabel + 7;
      widthStatus = elementProcess.getElementsByTagName('text')[1].getBoundingClientRect().width;
    }
    // direction    
    if (this.config.infoBox.base.direction && item.statusType.name == this.config.status.run.name) {
      direction = `<svg  xmlns="http://www.w3.org/2000/svg" width="13" x="{xDirection}" height="13" y="5" viewBox="0 0 13 13">
          <path transform="rotate({mapIconDirection} 6 6)" fill="#FFF" fill-rule="evenodd"
              d="M1.55 5.086L6.5.136 7.914 1.55 3.964 5.5h7.9v2h-7.9l3.95 3.95L6.5 12.864.136 6.5 1.55 5.086z"></path>
      </svg>`;
      width += 15;
    }

    let iconSettings = {
      mapIconUrl: `<svg x="116.56048094444441" y="185.82438400133492" width="50" height="22" class="mapCarIcon"  style="cursor: pointer;">
          <svg>
             ${rect}
             ${svgActive}
             ${icon}
              <text x="{xLabel}" y="15" fill="#fff" font-size="11" font-weight="bold">{mapIconName}</text>
              ${svgStatus}
          </svg>
         
          ${direction}
          <svg width="10" x="{xPoint}" y="20" height="10" viewBox="0 0 8 8">
           ${bolderArrow}
            <polygon points="0,0 5,5 10,0" fill="{mapIconColor}" />
          </svg>
      </svg>
      `,
      mapIconColor: item.statusType.colorCode,
      mapIconColorInnerCircle: '#fff',
      pinInnerCircleRadius: 48,
      mapIconName: label,
      mapIconDirection: 90 + item.direction,
      width: width,
      xDirection: width - 17,
      xPoint: width / 2,
      iconRotate: item.direction,
      iconFill: "",
      statusShow: statusShow,
      xStatus: widthText + 5 + offSet,
      xRectStatus: widthText + offSet,
      wRectStatus: widthStatus + 10,
      xLabel: xLabel + offSet,
    };
    let object: any = {
      icon: L.divIcon({
        html: L.Util.template(iconSettings.mapIconUrl, iconSettings),
        iconAnchor: [(width / 2) + 6, 25],
        iconSize: [25, 30],
        // popupAnchor: [0, -28],
        className: 'mapCarIcon',

      }),
      data: item,
    };
    if (this.itemOnTop && item.id === this.itemOnTop.id) {
      object["zIndexOffset"] = 999;
    }
    return L.marker([item.lat, item.lng], object);
  }
  private createMarker2(item: Item): L.Marker {
    // let _this = this;
    let width = 0;
    let widthText = 0;
    let widthStatus = 0;
    let statusShow = "";
    let elementProcess = document.getElementById(this.config.options.elementProcessText);
    let svgStatus = "";
    let offset = 0;
    let xLabel = 8;
    let svgActive = "";

    // if (this.itemOnTop && item.id == this.itemOnTop.id){
    //  offset = 10; 
    // }
    let label = item[this.config.infoBox.base.label.value] ? item[this.config.infoBox.base.label.value] : "---";
    elementProcess.getElementsByTagName('text')[0].innerHTML = label;
    width = elementProcess.getElementsByTagName('text')[0].getBoundingClientRect().width + 20 + offset;
    // check show speed
    if (
      // this.config.features.markerStyle.options.showSpeed && 
      this.config.infoBox.base.speed
      && item.statusType.name == this.config.status.run.name && item['speed']) {
      statusShow = item['speed'] + " km/h";
    }
    if (item.statusType.name != this.config.status.run.name
      && item['statusDurationStr']
      // && this.config.features.markerStyle.options.showDuration
      && this.config.infoBox.base.duration
    ) {
      statusShow = item['statusDurationStr'];
    }
    if (statusShow.length > 0) {
      svgStatus = `<rect x="{xRectStatus}" width="{wRectStatus}" height="13" fill="#e8e8e8" rx="3" ry="3" y="4"></rect>
      <text x="{xStatus}" y="15" fill="#000" font-size="11" font-weight="bold">{statusShow}</text>`;
      elementProcess.getElementsByTagName('text')[1].innerHTML = statusShow;
      width = elementProcess.getElementsByTagName('text')[0].getBoundingClientRect().width
        + elementProcess.getElementsByTagName('text')[1].getBoundingClientRect().width + 35 + offset;
      widthText = elementProcess.getElementsByTagName('text')[0].getBoundingClientRect().width + 10;
      widthStatus = elementProcess.getElementsByTagName('text')[1].getBoundingClientRect().width;
    }

    let rect = ` <rect width="{width}" height="22" fill="{mapIconColor}" stroke="#fff" stroke-width="1" rx="3" ry="3"></rect>`;
    let bolderArrow = `<polygon points="0,1 5,6 10,1" fill="#fff"></polygon>`;
    // activce 
    // if (this.itemOnTop && item.id == this.itemOnTop.id) {
    //   rect = ` <rect width="{width}" height="22" fill="{mapIconColor}" stroke="#5476e5" stroke-width="2" rx="3" ry="3"></rect>`;
    //   bolderArrow = `<polygon points="0,2 5,7 10,2" fill="#5476e5"></polygon>`;
    //   // <polygon points="0,1 4,5  0,9" fill="#7495a6" />
    //   // / <polygon points="  3,1 1,1  1,9 3,9" fill="#ffffff" />
    //   svgActive = `
    //   <svg width="20" x="1" y="1" height="20" stroke="#5476e5" stroke-width="0.9" viewBox="0 0 10 10">
    //     <polygon points="  0,1 4,5 0,9" fill="#5476e5" />
    //   </svg>`;

    // }

    let iconSettings = {
      mapIconUrl: `<svg x="116.56048094444441" y="185.82438400133492" width="50" height="22" class="mapCarIcon2"  style="cursor: pointer;">
          <svg y="0">
              ${rect}    
              ${svgActive}          
              <text x="{xLabel}" y="15" fill="#fff" font-size="11" font-weight="bold">{mapIconName}</text>
              ${svgStatus}      
          </svg>
          <svg width="10" x="{xPoint}" y="20" height="10" viewBox="0 0 8 8">
              ${bolderArrow}
              <polygon points="0,0 5,5 10,0" fill="{mapIconColor}" />
          </svg>
          `+ item.iconType.iconExtend + `
      </svg>`,
      mapIconColor: item.statusType.colorCode,
      mapIconColorInnerCircle: '#fff',
      pinInnerCircleRadius: 48,
      mapIconName: label,
      mapIconDirection: 90 + item.direction,
      width: width,
      xDirection: width - 17,
      xPoint: width / 2 - 4,
      iconRotate: item.direction,
      iconFill: "",
      xCar: width / 2 - 22,
      bgCarSub: "black",
      statusShow: statusShow,
      xStatus: widthText + 10 + 5 + offset,
      xRectStatus: widthText + 10 + offset,
      wRectStatus: widthStatus + 10,
      xLabel: xLabel + offset
    };
    let object: any = {
      icon: L.divIcon({
        html: L.Util.template(iconSettings.mapIconUrl, iconSettings),
        iconAnchor: [width / 2 + 6, 57],
        iconSize: [25, 30],
        popupAnchor: [0, -28],
        className: 'mapCarIcon2',
      }),
      data: item,
    };
    if (this.itemOnTop && item.id === this.itemOnTop.id) {
      object["zIndexOffset"] = 100;
    }
    return L.marker([item.lat, item.lng], object);
  }
  private createMarkerToolTip(marker: any, style: string, option?: { pinActive?: boolean, lockActive?: boolean, follow?: boolean }): L.Marker {

    let _this = this;
    let iconOption = [];
    let data = {};
    let classTooltip = "map-unit-tooltip";
    let classNameMarker = "marker-tooltip";
    if (style == "style-two") {
      classTooltip = "map-unit-tooltip2";
      classNameMarker = "marker-tooltip2";
    }
    data = marker['options']['data'];
    iconOption = marker['options']['icon']['options'];
    const factory = this.resolver.resolveComponentFactory(MarkerPopupComponent);
    const component = factory.create(this.injector);
    component.instance.data = JSON.parse(JSON.stringify(data));
    component.instance.class = classTooltip;
    component.instance.iconOption = JSON.parse(JSON.stringify(iconOption));
    component.instance.infoBoxConfig = this.config.infoBox;
    if (option) {
      if (option.pinActive != undefined) component.instance.pinActive = option.pinActive;
      if (option.lockActive != undefined && option.pinActive != undefined && option.pinActive === true)
        component.instance.lockActive = option.lockActive;
      else component.instance.lockActive = false;
      if (option.follow != undefined) component.instance.follow = option.follow;
    }

    component.instance.markerPopupEvent.pipe(
      tap(result => {
        this.popupMarkerAction(result);
      }),
      finalize(() => {
        component.changeDetectorRef.markForCheck();
        component.changeDetectorRef.detectChanges();
      })
    ).subscribe();
    component.changeDetectorRef.detectChanges();
    const popupContent = component.location.nativeElement;

    let object: any = {
      icon: L.divIcon({
        html: popupContent,
        iconAnchor: [18, 32],
        iconSize: [1, 1],
        popupAnchor: [0, -28],
        className: classNameMarker,
      }),
      zIndexOffset: 1000,
      data: {},
    };
    return L.marker([marker.getLatLng().lat, marker.getLatLng().lng], object);
  }
  private createClusterLayer(markers) {

    let _this = this;
    let listStatus = {};
    let status = [];
    Object.keys(this.config.status).forEach((x) => {
      status.push(_this.config.status[x]);
      listStatus[x] = 0;
    });

    markers.map(x => {
      let index = status.findIndex(s => s.name == x.options['data'].statusType.name);
      if (index >= 0) {
        listStatus[status[index].name] += 1;
      }
    });

    let paths = [];

    let o = 0;
    Object.keys(listStatus).forEach(x => {
      let statusColor = status.find(s => s.name == x);


      if (listStatus[x] > 0 && statusColor) {
        let n = listStatus[x] / markers.length;
        let s = 360 * n;
        let path = this.mapUtil.createArc(0, 0, 20, o + 3, o + s - 3);
        let pathStr = `<path id="arc1" fill="none" stroke="` + statusColor.colorCode + `" stroke-width="6"
        // d="`+ path + `"></path>`;
        paths.push(pathStr)
        o += s;
      }
    });

    let iconSettings = {
      mapIconUrl: `<svg height="20" width="20" viewBox="0 0 20 20" style="cursor: pointer;">
          <circle r="22" stroke="#fff" stroke-width="2" fill="#fff"></circle><text fill="#000" font-size="14" y="5"
              text-anchor="middle" style="font-weight: bold; color: rgb(56, 64, 69);">{count}</text>
              {paths}
      </svg>`,
      mapIconColor: '#cc756b',
      mapIconColorInnerCircle: '#fff',
      pinInnerCircleRadius: 48,
      count: markers.length,
      mapIconDirection: "0",
      status: "",
      paths: paths.join(''),

    };

    return L.Util.template(iconSettings.mapIconUrl, iconSettings);
  }
  public createPopupCustom(latlng: L.LatLngExpression, options?: L.MarkerOptions, optionCT?: MarkerOptionCT, item?: { type: string, data: any }): L.Marker {
    let iconOption = [];
    let classTooltip = "map-unit-tooltip map-popup-tooltip";
    let classNameMarker = "marker-tooltip";
    let optionCTDefault = {
      width: 40,
      heigth: 40,
      color: "#ff2b2bde",//hex  
      mapIconUrl: `
      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
          width="{width}px" height="{height}px"
          viewBox="0 0 24 24"
          style=" fill:{color};">    
          <path d="M 12 1 C 8.686 1 6 3.686 6 7 C 6 10.323 9.6065625 15.105891 11.226562 17.087891 C 11.627562 17.577891 12.370484 17.577891 12.771484 17.087891 C 14.392484 15.105891 18 10.323 18 7 C 18 3.686 15.314 1 12 1 z M 12 4.8574219 C 13.184 4.8574219 14.142578 5.816 14.142578 7 C 14.142578 8.183 13.183 9.1425781 12 9.1425781 C 10.817 9.1425781 9.8574219 8.184 9.8574219 7 C 9.8574219 5.816 10.816 4.8574219 12 4.8574219 z M 6.9941406 15.564453 C 6.9172363 15.56473 6.8383906 15.574375 6.7597656 15.59375 C 3.9047656 16.29875 2 17.56 2 19 C 2 21.209 6.477 23 12 23 C 17.523 23 22 21.209 22 19 C 22 17.56 20.095234 16.299703 17.240234 15.595703 C 16.611234 15.440703 16 15.926219 16 16.574219 C 16 17.028219 16.302187 17.433969 16.742188 17.542969 C 18.547187 17.990969 19.644219 18.612047 19.949219 18.998047 C 19.434219 19.653047 16.694 20.998047 12 20.998047 C 7.306 20.998047 4.5657813 19.653047 4.0507812 18.998047 C 4.3547813 18.611047 5.4528125 17.990969 7.2578125 17.542969 C 7.6978125 17.434969 8 17.028219 8 16.574219 C 8 16.006344 7.5324707 15.562512 6.9941406 15.564453 z">
          </path>
      </svg>`,
    }
    const factory = this.resolver.resolveComponentFactory(PopupComponent);
    const component = factory.create(this.injector);
    component.instance.data = item.data;
    component.instance.class = classTooltip;
    component.instance.type = item.type;
    // component.instance.closePopup = function (data) {
    //   // _this.currentPopupChange("hide");
    //   // if (_this.popup) _this.popup.remove();
    // }
    component.changeDetectorRef.detectChanges();
    const popupContent = component.location.nativeElement;
    if (optionCT) Object.assign(optionCTDefault, optionCT);

    try {
      // let iconSettings = {
      //   mapIconUrl: optionCTDefault.mapIconUrl,
      //   width: optionCTDefault.width,
      //   height: optionCTDefault.heigth,
      //   color: optionCTDefault.color,
      // };
      let object: any = {
        icon: L.divIcon({
          html: popupContent,
          iconAnchor: [optionCTDefault.width / 2, optionCTDefault.heigth],
          iconSize: [optionCTDefault.width, optionCTDefault.heigth],
          popupAnchor: [0, 0],
          className: classNameMarker,
        }),
        data: item,
      };
      if (options) object = Object.assign(object, options);

      return L.marker(latlng, object);
    }
    catch (ex) {
      console.log(ex);
      return null;
    }
  }

  createLandmarkPopup(latlng: L.LatLngExpression, options?: L.MarkerOptions, optionCT?: MarkerOptionCT, item?: any): L.Marker {
    let classTooltip = "map-unit-tooltip map-popup-tooltip";
    let classNameMarker = "marker-tooltip";
    let optionCTDefault = {
      width: 30,
      heigth: 30,
      color: "#ff2b2bde",//hex  
      mapIconUrl: `
      <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
          width="{width}px" height="{height}px"
          viewBox="0 0 24 24"
          style=" fill:{color};">    
          <path d="M 12 1 C 8.686 1 6 3.686 6 7 C 6 10.323 9.6065625 15.105891 11.226562 17.087891 C 11.627562 17.577891 12.370484 17.577891 12.771484 17.087891 C 14.392484 15.105891 18 10.323 18 7 C 18 3.686 15.314 1 12 1 z M 12 4.8574219 C 13.184 4.8574219 14.142578 5.816 14.142578 7 C 14.142578 8.183 13.183 9.1425781 12 9.1425781 C 10.817 9.1425781 9.8574219 8.184 9.8574219 7 C 9.8574219 5.816 10.816 4.8574219 12 4.8574219 z M 6.9941406 15.564453 C 6.9172363 15.56473 6.8383906 15.574375 6.7597656 15.59375 C 3.9047656 16.29875 2 17.56 2 19 C 2 21.209 6.477 23 12 23 C 17.523 23 22 21.209 22 19 C 22 17.56 20.095234 16.299703 17.240234 15.595703 C 16.611234 15.440703 16 15.926219 16 16.574219 C 16 17.028219 16.302187 17.433969 16.742188 17.542969 C 18.547187 17.990969 19.644219 18.612047 19.949219 18.998047 C 19.434219 19.653047 16.694 20.998047 12 20.998047 C 7.306 20.998047 4.5657813 19.653047 4.0507812 18.998047 C 4.3547813 18.611047 5.4528125 17.990969 7.2578125 17.542969 C 7.6978125 17.434969 8 17.028219 8 16.574219 C 8 16.006344 7.5324707 15.562512 6.9941406 15.564453 z">
          </path>
      </svg>`,
    }
    const factory = this.resolver.resolveComponentFactory(LandmarkPopupComponent);
    const component = factory.create(this.injector);
    component.instance.data = item;
    component.instance.class = classTooltip;
    component.changeDetectorRef.detectChanges();
    const landmarkPopupContent = component.location.nativeElement;
    let object: any = {
      icon: L.divIcon({
        html: landmarkPopupContent,
        iconAnchor: [optionCTDefault.width / 2, optionCTDefault.heigth],
        iconSize: [optionCTDefault.width, optionCTDefault.heigth],
        popupAnchor: [0, 0],
        className: classNameMarker,
      }),
      data: item,
    };;
    return L.marker(latlng, object)
  }

  private createGeofencePopup(latLng: L.LatLngExpression | [number, number], data): L.Marker {
    let _this = this;
    let iconOption = [];
    let classTooltip = "map-unit-tooltip map-popup-tooltip";
    let classNameMarker = "marker-tooltip";
    const factory = this.resolver.resolveComponentFactory(PopupComponent);
    const component = factory.create(this.injector);
    component.instance.data = JSON.parse(JSON.stringify(data));
    component.instance.class = classTooltip;
    component.instance.closePopup = function (data) {
      _this.currentPopupChange("hide");
      // if (_this.popup) _this.popup.remove();
    }

    component.changeDetectorRef.detectChanges();
    const popupContent = component.location.nativeElement;

    let object: any = {
      icon: L.divIcon({
        html: popupContent,
        iconAnchor: [16, 0],
        iconSize: [32, 32],
        popupAnchor: [0, 0],
        className: classNameMarker,

      }),
      data: {},
    };
    return L.marker(latLng, object);
  }
  private createClusterTooltip(cluster) {
    let clusterItems = '';

    cluster.layer.getAllChildMarkers().forEach(function (x) {
      data = x.options.data;
      clusterItems += `<div class="cluster-item">
      <div class="mp-inline-block"><span class="unit-state nodata"><svg x="0" y="0"
                  xmlns="http://www.w3.org/2000/svg"
                  xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24"
                  viewBox="0 0 24 24">
                  <defs>
                      <path id="0.5250787507392536"
                          d="M11 9.833a2 2 0 1 1 .001 4 2 2 0 0 1-.001-4zm-6 0a2 2 0 1 1 .001 4 2 2 0 0 1-.001-4zm13-9a2 2 0 0 1 2 2v9h-2v2h-1v-2h-3c0-.772-1-3-3-3a3 3 0 0 0-3 3c0-1.654-1.346-3-3-3s-3 1.346-3 3a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h16z">
                      </path>
                  </defs>
                  <g fill="none" fill-rule="evenodd" transform="translate(2 5)">
                      <mask id="0.7966610412884654" fill="#fff">
                          <use xlink:href="#0.5250787507392536"></use>
                      </mask>
                      <use fill="#595959" xlink:href="#0.5250787507392536"></use>
                      <g fill="#495156" mask="url(#0.7966610412884654)">
                          <path d="M-16-19h52v52h-52z"></path>
                      </g>
                  </g>
              </svg></span>
              <div class="name">`+ data['name'] + `</div>

              </div>
  </div>`;
    });

    let iconOption = cluster['layer']['options']['icon']['options'];
    let data = cluster['layer']['options']['data'] || {};
    let html = `<div class="custom-tile" style="width: 100%; height: 100%; position: relative;">
    <div class="cluster-block" >
        <div class="custom-scroll"  >
            <div class="outer-container">
                <div class="inner-container">
                    `+ clusterItems + `
                </div>
            </div>
        </div>
        <div class="click-holder"></div>
      </div>
    </div>`;
    let _this = this;
    const factory = this.resolver.resolveComponentFactory(ClusterPopupComponent);
    const component = factory.create(this.injector);
    const listData = [];
    for (let i = 0; i < cluster.layer.getAllChildMarkers().length; i++) {
      listData.push(JSON.parse(JSON.stringify(cluster.layer.getAllChildMarkers()[i].options.data)));
    }
    component.instance.data = listData;
    component.instance.dataView = listData;
    component.instance.clusterOption = this.config.features.cluster.options;
    component.instance.infoBoxConfig = this.config.infoBox;
    component.changeDetectorRef.detectChanges();
    component.instance.selectItem = function (item) {
      _this.map.fitBounds([[Number.parseFloat(item['lat']), Number.parseFloat(item['lng'])]], { padding: [20, 20] });
      _this.focusItem(item);
    }
    const popupContent = component.location.nativeElement;

    let object: any = {
      icon: L.divIcon({
        html: popupContent,
        iconAnchor: [12, 32],
        iconSize: [1, 1],
        popupAnchor: [0, -28],
        className: 'cluster-tooltip',

      }),
      zIndexOffset: 1000,
      data: {},

    };

    return L.marker([cluster['latlng'].lat, cluster['latlng'].lng], object);
  }

  addGeofences(geofences: Array<GeofenceModel>, fitBound?: true | false) {
    this.layerGeofence = new L.FeatureGroup([]);
    let geofenceOnMap;
    geofences.forEach(x => {
      geofenceOnMap = this.createGeofence(x);
    });
    if (this.geofences.length > 0) {
      this.layerGeofence.addTo(this.map);
    }
    if (this.layerGeofence.getLayers().length > 0 && fitBound) {
      this.map.fitBounds(this.layerGeofence.getBounds(), { padding: [20, 20] });
    }
    return geofenceOnMap
  }

  fitBoundGeofences(geofences: Array<GeofenceModel>) {
    let layerGeofence = new L.FeatureGroup([]);
    geofences.forEach(x => {
      let geofenceFind = this.geofences.find(g => g.key.id == x.id);
      if (geofenceFind) {
        layerGeofence.addLayer(geofenceFind.layer);
      }
    });
    if (layerGeofence.getLayers().length > 0) {
      this.map.fitBounds(layerGeofence.getBounds(), { padding: [20, 20] });
    }
  }

  private createGeofence(geofence: GeofenceModel, fitBound?: true | false,) {
    let _this = this;
    let geofenceOnMap;
    if (geofence.active == 1) {
      switch (geofence.type) {
        case "polygon":
          let polygon = L.polygon(geofence.points, {
            fillColor: geofence.fillColor,
            fillOpacity: geofence.fillOpacity,
            color: geofence.color,
            opacity: geofence.opacity,
          });
          polygon.on('click', function (p) {
            let popup = _this.createGeofencePopup([p['latlng']['lat'], p['latlng']['lng']], geofence);
            _this.currentPopupChange("show", popup);
          });
          this.geofences.push({ key: geofence, layer: polygon });
          this.layerGeofence.addLayer(polygon);
          geofenceOnMap = polygon;
          break;
        case "circle":
          let circle = L.circle(geofence.points[0], {
            radius: geofence.radius,
            fillColor: geofence.fillColor,
            fillOpacity: geofence.fillOpacity,
            color: geofence.color,
            opacity: geofence.opacity,
          });
          circle.on('click', function (p) {
            let popup = _this.createGeofencePopup([p['latlng']['lat'], p['latlng']['lng']], geofence);
            _this.currentPopupChange("show", popup);
          });
          this.geofences.push({ key: geofence, layer: circle });
          this.layerGeofence.addLayer(circle);
          geofenceOnMap = circle
          break;
      }
    }
    return geofenceOnMap
  }

  clearGeofences() {
    if (this.layerGeofence) {
      this.layerGeofence.remove();
    }
    this.geofences = [];
  }

  setPoi(poi: Poi) {
    this.poi = poi;
  }

  addLandmark(landmarks: PoiResult[]) {
    this.savedLandmarks = landmarks;
    if (this.lastZoom < 11 && this.poi) {
      this.clearLandmark();
      this.checkZoom = true;
    }
    if (this.map.getZoom() < 11) return;
    this.createLandmark(landmarks);
    if (this.landmarkOnShow.length > 0) {
      this.layerPoi.addTo(this.map);
    }
  }

  createLandmark(landmarks: PoiResult[]) {
    let show = this.filterNewLandmark(landmarks);
    this.filterOldLandmark(landmarks);

    if (show.length == 0) return;
    show.forEach(result => {
      if (result.active === 0) return;
      let color = result.fillColor;
      let isShowPopup = false;
      let popup: L.Marker;
      let icon = lankmarkIcon[result.poiTypeName];
      let landmark = this.mapUtil.createMarker(
        [result.latitude, result.longitude], { zIndexOffset: -9999 },
        {
          width: 40,
          heigth: 38,
          color: color,
          mapIconUrl: createLandmarkIcon(icon),
        }
      );
      landmark.on('click', () => {
        if (isShowPopup) return;
        popup = this.createLandmarkPopup(
          [result.latitude, result.longitude], { zIndexOffset: 9999 },
          { width: 32, heigth: 38 },
          result
        );
        popup.on('mouseout', () => {
          this.layerPoi.removeLayer(popup);
          this.layerPoi.addTo(this.map);
        });
        this.layerPoi.addLayer(popup);
        this.layerPoi.addTo(this.map);
      });
      this.landmarkOnShow.push({ key: result, layer: landmark })
      this.layerPoi.addLayer(landmark);
    })
  }
  createLandmarkTracking(landmarks: PoiResult[]) {
    let returnValue;
    landmarks.forEach(result => {
      if (result.active === 0) return;
      this.map.setView([result.latitude, result.longitude], 16)
      let color = result.fillColor;
      let isShowPopup = false;
      let popup: L.Marker;
      let icon = lankmarkIcon[result.poiTypeName];
      let landmark = this.mapUtil.createMarker(
        [result.latitude, result.longitude], { zIndexOffset: -9999 },
        {
          width: 40,
          heigth: 38,
          color: color,
          mapIconUrl: createLandmarkIcon(icon),
        }
      );
      landmark.on('mouseover', () => {
        if (isShowPopup) return;
        popup = this.createLandmarkPopup(
          [result.latitude, result.longitude], { zIndexOffset: 9999 },
          { width: 32, heigth: 38 },
          result
        );
        popup.on('mouseout', () => {
          this.layerPoi.removeLayer(popup);
          this.layerPoi.addTo(this.map);
        });
        this.layerPoi.addLayer(popup);
      });
      this.landmarkOnShow.push({ key: result, layer: landmark })
      this.layerPoi.addLayer(landmark);
      this.layerPoi.addTo(this.map);
      returnValue = landmark
    })
    return returnValue
  }

  fitBoundLandmark(landmark: Array<PoiResult>) {
    landmark.forEach(mark => {
      if (this.lastZoom < 11) {
        this.map.setView([mark.latitude, mark.longitude], 12);
        this.addLandmark(this.savedLandmarks);
      }
      else {
        this.map.panTo([mark.latitude, mark.longitude])
      }
    })
  }

  filterOldLandmark(landmarks: PoiResult[]) {
    if (this.landmarkOnShow.length == 0) return;
    if (landmarks.length == 0) {
      this.clearLandmark();
      return;
    };
    this.landmarkOnShow.forEach((landmarkOS, j) => {
      let check = false;
      landmarks.forEach((landmark) => {
        if (landmarkOS.key.id == landmark.id) {
          check = true;
        }
      })
      if (check == false) {
        this.layerPoi.removeLayer(landmarkOS.layer);
        this.landmarkOnShow.splice(j, 1)
      }
    });
  }

  filterNewLandmark(landmarks: PoiResult[]) {
    if (this.landmarkOnShow.length == 0) return landmarks;
    let arr = [];
    landmarks.forEach(landmark => {
      let check = true;
      this.landmarkOnShow.forEach(landmarkOS => {
        if (landmark.id == landmarkOS.key.id) {
          check = false;
        }
      })
      if (check) {
        arr.push(landmark);
      }
    })
    return arr
  }

  clearLandmark() {
    if (this.layerPoi) {
      this.landmarkOnShow.forEach((e) => {
        this.layerPoi.removeLayer(e.layer);
      });
      this.landmarkOnShow = [];
    }
  }

  panToLandmark(data) {
    this.map.setView(data, 18)
  }

  checkBound(point) {
    let p1 = L.point(this.map.getBounds().getNorthEast().lat, this.map.getBounds().getNorthEast().lng);
    let p2 = L.point(this.map.getBounds().getSouthWest().lat, this.map.getBounds().getSouthWest().lng);
    let bound = L.bounds(p1, p2);
    let check = L.point(point.lat, point.lng);
    return bound.contains(check);
  }

  getBounds() {
    let p1 = L.point(this.map.getBounds().getNorthEast().lat, this.map.getBounds().getNorthEast().lng);
    let p2 = L.point(this.map.getBounds().getSouthWest().lat, this.map.getBounds().getSouthWest().lng);
    let bound = L.bounds(p1, p2);
    return bound;
  }

  drawTollsOnMap(listTolls) {
    this.map.on('dragend', () => {
      this.showTolls(listTolls);
    })
    this.map.on('zoomend', () => {
      this.showTolls(listTolls);
    })
  }

  showTolls(listTolls) {
    this.pointLayer.clearLayers();
    if (this.map.getZoom() < 15) return;
    let currentBounds = this.getBounds();
    listTolls.forEach((toll, i) => {
      let point = L.point((toll.neLat + toll.swLat) / 2, (toll.neLng + toll.swLng) / 2)
      if (currentBounds.contains(point)) {
        if (this.savedPoints[i] == undefined) {
          let isShowPopup = false;
          let popup;
          let latLng: L.LatLngExpression = [(toll.neLat + toll.swLat) / 2, (toll.neLng + toll.swLng) / 2];
          let tollOnMap = L.circle(latLng, { radius: toll.radius })
          tollOnMap.on('mouseover', () => {
            if (isShowPopup) return;
            popup = this.createLandmarkPopup(latLng, { zIndexOffset: 9999 },
              { width: 32, heigth: 38 },
              toll.name);
            popup.on('mouseout', () => {
              this.pointLayer.removeLayer(popup);
            });
            this.pointLayer.addLayer(popup);
          });
          this.savedPoints[i] = tollOnMap;
          this.pointLayer.addLayer(tollOnMap);
        } else {
          let tollOnMap = this.savedPoints[i];
          this.pointLayer.addLayer(tollOnMap);
        }
      }
    })
    this.pointLayer.addTo(this.map);
  }


  private popupMarkerAction(result: ResultPopupMarkerEvent) {
    switch (result.action) {
      case "pin":
        if (result.data.pinActive) this.pinMarker(result.data.value);
        else this.unPinMarker(result.data.value);
        break;
      case "lock":
        if (result.data.lockActive) this.lockMarker(result.data.value);
        else this.unlockMarker(result.data.value);
        break;
      case "follow":
        this.follow(result.data.value);
        // this.focusItem(result.data.value);
        break;
      case "select_marker":
        this.focusItem(result.data.value);
        break;
      case "geocode":
        let itemIndex = this.items.findIndex(x => x.id == result.data.value.id);
        if (itemIndex >= 0) {
          this.items[itemIndex].address = result.data.value.address;
        }

        break;
    }
  }

  lockMarker(item) {
    let _this = this;
    if (_this.itemPin) {
      if (!_this.itemPin.markerOption) {
        _this.itemPin.markerOption = { lock: true }
      }
      else _this.itemPin.markerOption.lock = true;
      _this.updateMarker(_this.itemPin);
    }
  }

  unlockMarker(item) {
    let _this = this;
    if (_this.itemPin) {
      if (!_this.itemPin.markerOption) {
        _this.itemPin.markerOption = { lock: false }
      }
      else _this.itemPin.markerOption.lock = false;
    }
  }

  pinMarker(item) {
    let _this = this;
    let itemConver = new Item(item);
    if (itemConver.markerOption) itemConver.markerOption.lock = false;
    else itemConver.markerOption = { lock: false };
    _this.itemPin = itemConver;

    _this.updateMarker(itemConver, "pin");
  }

  unPinMarker(item) {
    this.itemPin = undefined;
    if (item.markerOption) item.markerOption.lock = false;
    else item.markerOption = { lock: false };

    this.currentPopupChange("hide");
    this.updateMarker(item, "mouse");
  }

  updateFollow(items) {
    items.forEach(item => {
      if (item.lat != null && item.lng != null) {
        let itemFind = this.followItems.find(x => x.item.id == item.id);
        if (itemFind) {
          let latlngs = itemFind.polyline.getLatLngs();
          // check last latlng 
          let lastLatlng = latlngs[latlngs.length - 1];
          if (lastLatlng["lat"] != item.lat || lastLatlng["lng"] != item.lng) {
            let offset = latlngs.length - this.config.features.follow.maxTail;
            if (offset >= 0 && latlngs.length > 0) {
              let newLatLngs = [];
              latlngs.forEach((value, index) => {
                if (index > offset) {
                  newLatLngs.push(value);
                }
              });
              newLatLngs.push({ lat: item.lat, lng: item.lng });
              itemFind.polyline = itemFind.polyline.setLatLngs(newLatLngs);
            }
            else {
              itemFind.polyline.addLatLng({ lat: item.lat, lng: item.lng });
            }
          }

        }
      }
    })
  }

  follows(items, isFollow: boolean) {
    if (isFollow) {
      items.forEach(item => {
        this.eventChange.emit({ type: "follow", data: item });
        let polyline: any = this.createPolyline([{ lat: item.lat, lng: item.lng }], null, { isShow: true });
        if (polyline) {
          this.followItems.push({ item: item, polyline: polyline });
          polyline.addTo(this.map);
        }
      });
    } else {
      this.followItems.forEach(item => {
        this.eventChange.emit({ type: "unfollow", data: item.item });
        item.polyline.remove();
      });
      this.followItems = [];
    }

    this.updateMarkers(this.items);
  }

  follow(item, isFollow?: boolean) {
    let index = this.followItems.findIndex(x => x.item.id == item.id);

    if (index >= 0) {
      this.eventChange.emit({ type: "unfollow", data: item });
      this.followItems[index].polyline.remove();
      this.followItems.splice(index, 1);
    }
    else {
      this.eventChange.emit({ type: "follow", data: item });
      let polyline: any = this.createPolyline([{ lat: item.lat, lng: item.lng }], null, { isShow: true });
      if (polyline) {
        this.followItems.push({ item: item, polyline: polyline });
        polyline.addTo(this.map);
      }
    }
    // update marker
    let itemFind = this.items.find(x => x.id == item.id);
    if (itemFind) this.updateMarker(itemFind);
  }

  createPolyline(latlngs: L.LatLngExpression[] | L.LatLngExpression[][], options?: L.PolylineOptions, showText?: { isShow: boolean, options?: any, text?: string }): L.Polyline {
    try {
      let optionsDefault = {
        color: "#0095F6",
        weight: 5,
        opacity: 1,
      };
      if (options) Object.assign(optionsDefault, options);
      let polyline: any = L.polyline(latlngs, optionsDefault);
      if (showText && showText.isShow) {
        let optionsText = {
          repeat: true,
          offset: 3,
          attributes: { fill: '#384045', 'font-size': '9', 'opacity': '0.7' }
        };
        if (showText.options) Object.assign(optionsText, showText.options);
        polyline.setText(showText.text || '\u25BA                  ', optionsText);
      }
      return polyline;
    }
    catch (ex) {
      console.log(ex);
      return null;
    }


  }

  getItem(id?: number) {
    if (id > 0) {
      return this.items.find((x) => x.id == id) || null;
    }
    else return this.items.length > 0 ? this.items[0] : null;
  }

  selectItem(item?: Item, zoomNumber?:number) {

    // remove on top 
    if (this.itemOnTop && this.itemOnTop.id !== item.id) {
      let itemOld = this.items.find(x => x.id == this.itemOnTop.id);

      this.itemOnTop = undefined;
      if (itemOld) this.updateMarker(itemOld);
    }
    // update on top 
    let itemNew = this.items.find(x => x.id == item.id);
    if (itemNew) {
      this.itemOnTop = itemNew;
      this.updateMarker(this.itemOnTop);
      this.mapUtil.setView(new L.LatLng(this.itemOnTop.lat, this.itemOnTop.lng), this.map, zoomNumber?zoomNumber:this.map.getZoom());
      // this.fitbound([this.itemOnTop]);
    }

    this.eventChange.emit({ type: "action__select_device", data: item });
  }

  focusItem(item?: Item) {
    // remove on top 
    if (this.itemOnTop && this.itemOnTop.id !== item.id) {
      let itemOld = this.items.find(x => x.id == this.itemOnTop.id);

      this.itemOnTop = undefined;
      if (itemOld) this.updateMarker(itemOld);
    }
    // update on top 
    let itemNew = this.items.find(x => x.id == item.id);
    if (itemNew) {
      this.itemOnTop = itemNew;
      this.updateMarker(this.itemOnTop);
    }

    this.eventChange.emit({ type: "action__set_view_list_device", data: item });
    this.eventChange.emit({ type: "action__select_device", data: item });
  }
  sendEmitter(emit: { type: string, data: any }) {
    this.eventChange.emit({ type: emit.type, data: emit.data });
  }
  getItems() {
    return this.items;
  }
  // itemHideOnMap(item:Item,isHide:boolean){
  //   let index =  this.items.findIndex(x=>x.id == item.id);
  //   if(index>=0){
  //     this.items[index].feature.hideOnMap = isHide;
  //     this.updateMarker(item);
  //   }
  // }
  // unFollow(item) {
  //   let index = this.followItems.findIndex(x => x.item.id == item.id);
  //   if (index >= 0) {
  //     this.followItems[index].polyline.remove();
  //     this.followItems.splice(index, 1);
  //   }
  // }


  // ========== End Render ==========

}
