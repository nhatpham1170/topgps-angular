import { Injectable } from '@angular/core';
import { Item } from '../_model/item';
import { GeofenceModel } from '@core/manage';
import * as L from 'leaflet';
const PRECISION = 1e5;
const CHARCODE_OFFSET = 63;
const CHARMAP = {};
@Injectable({
    providedIn: 'root'
})
export class MapUtil {
    public setView(center: L.LatLngExpression, map: L.Map, zoom?: number, options?: L.ZoomPanOptions) {
        let optionDefault: any = { padding: [50, 50], maxZoom: 18 };
        if (zoom == undefined) zoom = 18;
        map.setView(center, zoom, optionDefault);
    }
    public flyTo(latlng: L.LatLngExpression, map: L.Map, zoom?: number, options?: L.ZoomPanOptions) {
        let optionDefault: any = { padding: [50, 50], maxZoom: 18 };
        if (options) Object.assign(optionDefault, options);
        map.flyTo(latlng, zoom, optionDefault);
    }
    public panTo(latlng: L.LatLngExpression, map: L.Map, options?: L.PanOptions) {
        map.panTo(latlng)
    }
    public fitBounds(bounds: L.LatLngBoundsExpression, map: L.Map, options?: L.FitBoundsOptions) {

        let optionDefault: any = { padding: [50, 50], maxZoom: 18 };
        if (options) Object.assign(optionDefault, options);
        if (bounds) {
            map.fitBounds(bounds, optionDefault);
        }
    }
    public fitBound(layerGroup: L.FeatureGroup, map: L.Map, options?: L.FitBoundsOptions) {

        let optionDefault: any = { padding: [50, 50], maxZoom: 18 };
        if (options) Object.assign(optionDefault, options);
        if (layerGroup.getLayers().length > 0) {
            map.fitBounds(layerGroup.getBounds(), optionDefault);
        }
    }
    public createMarker(latlng: L.LatLngExpression, options?: L.MarkerOptions, optionCT?: MarkerOptionCT, data?: any): L.Marker {
        let optionCTDefault = {
            width: 32,
            heigth: 32,
            color: "#ff2b2bde",//hex  
            // mapIconUrl: `
            // <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
            //     width="{width}px" height="{height}px"
            //     viewBox="0 0 24 24"
            //     style=" fill:{color};">    
            //     <path d="M 12 1 C 8.686 1 6 3.686 6 7 C 6 10.323 9.6065625 15.105891 11.226562 17.087891 C 11.627562 17.577891 12.370484 17.577891 12.771484 17.087891 C 14.392484 15.105891 18 10.323 18 7 C 18 3.686 15.314 1 12 1 z M 12 4.8574219 C 13.184 4.8574219 14.142578 5.816 14.142578 7 C 14.142578 8.183 13.183 9.1425781 12 9.1425781 C 10.817 9.1425781 9.8574219 8.184 9.8574219 7 C 9.8574219 5.816 10.816 4.8574219 12 4.8574219 z M 6.9941406 15.564453 C 6.9172363 15.56473 6.8383906 15.574375 6.7597656 15.59375 C 3.9047656 16.29875 2 17.56 2 19 C 2 21.209 6.477 23 12 23 C 17.523 23 22 21.209 22 19 C 22 17.56 20.095234 16.299703 17.240234 15.595703 C 16.611234 15.440703 16 15.926219 16 16.574219 C 16 17.028219 16.302187 17.433969 16.742188 17.542969 C 18.547187 17.990969 19.644219 18.612047 19.949219 18.998047 C 19.434219 19.653047 16.694 20.998047 12 20.998047 C 7.306 20.998047 4.5657813 19.653047 4.0507812 18.998047 C 4.3547813 18.611047 5.4528125 17.990969 7.2578125 17.542969 C 7.6978125 17.434969 8 17.028219 8 16.574219 C 8 16.006344 7.5324707 15.562512 6.9941406 15.564453 z">
            //     </path>
            // </svg>`,
            mapIconUrl:  `<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
            width="{width}px" height="{height}px"
            viewBox="0 0 48 48"
            style=" fill:{color};"><path stroke="#fff" stroke-width="1"  d="M24,1C15.2,1,6.015,7.988,6,18C5.982,29.981,24,48,24,48s18.019-17.994,18-30 C41.984,8.003,32.8,1,24,1z M24,26c-4.418,0-8-3.582-8-8s3.582-8,8-8s8,3.582,8,8S28.418,26,24,26z"></path></svg>`,
        }
        if (optionCT) Object.assign(optionCTDefault, optionCT);

        try {
            let iconSettings = {
                mapIconUrl: optionCTDefault.mapIconUrl,
                width: optionCTDefault.width,
                height: optionCTDefault.heigth,
                color: optionCTDefault.color,
            };
            let object: any = {
                icon: L.divIcon({
                    html: L.Util.template(iconSettings.mapIconUrl, iconSettings),
                    iconAnchor: [optionCTDefault.width / 2, optionCTDefault.heigth],
                    iconSize: [optionCTDefault.width, optionCTDefault.heigth],
                    // iconSize: [optionCTDefault.width, optionCTDefault.heigth],
                    // popupAnchor: [0, -28],
                    className: 'map-icon-marker',
                }),
                data: data,
            };
            if (options) object = Object.assign(object, options);

            return L.marker(latlng, object);
        }
        catch (ex) {
            console.log(ex);
            return null;
        }

    }

    // Geofence process
    private getOptionsGeofence(geofence: GeofenceModel) {
        return {
            color: geofence.color,
            fill: geofence.fill,
            fillColor: geofence.fillColor,
            fillOpacity: geofence.fillOpacity,
            opacity: geofence.opacity,
            stroke: geofence.stroke,
            weight: geofence.weight,
        };
    }
    createGeofence(geofence: GeofenceModel): L.Layer {
        try {
            if (geofence) {
                switch (geofence.type) {
                    case "polygon":
                        let polygon = new L.Polygon(geofence.points);
                        polygon.options = this.getOptionsGeofence(geofence);
                        return polygon;
                        break;
                    case "circle":
                        let circle = new L.Circle(geofence.points[0], geofence.radius);
                        circle.options = this.getOptionsGeofence(geofence);
                        return circle;
                        break;
                }
            }
        }
        catch (ex) {
            console.log(ex);
            return null;
        }
        return null;
    }
    // end geofence process 
    createPolyline(latlngs: L.LatLngExpression[] | L.LatLngExpression[][], options?: L.PolylineOptions, showText?: { isShow: boolean, options?: any, text?: string }): L.Polyline {
        try {
          let optionsDefault = {
            color: "#0095F6",
            weight: 5,
            opacity: 1,
          };
          if (options) Object.assign(optionsDefault, options);
          let polyline: any = L.polyline(latlngs, optionsDefault);
          if (showText && showText.isShow) {
            let optionsText = {
              repeat: true,
              offset:10,
              attributes: { fill: '#384045', 'font-size': '9', 'opacity': '0.7' }
            };
            if (showText.options) Object.assign(optionsText, showText.options);
            polyline.setText(showText.text || '\u25BA                  ', optionsText);
          }
          return polyline;
        }
        catch (ex) {
          return null;
        }
    
    
      }
    // create Arc
    r(e, t, n, r) {
        let a = (r - 90) * Math.PI / 180;
        return {
            x: e + n * Math.cos(a),
            y: t + n * Math.sin(a)
        }
    };
    createArc(e, t, n, a, o) {
        var i = this.r(e, t, n, o)
            , s = this.r(e, t, n, a)
            , l = o - a <= 180 ? "0" : "1";
        return ["M", i.x, i.y, "A", n, n, 0, l, 0, s.x, s.y].join(" ")
    }
    // end process Arc
    // process item
    processItems(items: Array<any>): Array<any> {
        let item = new Item();
        return [item];
    }

    polylineDecode(encoded) {
        let points = []
        let index = 0, len = encoded.length;
        let lat = 0, lng = 0;
        while (index < len) {
            let b, shift = 0, result = 0;
            do {

                b = encoded.charAt(index++).charCodeAt(0) - 63;//finds ascii                                                                                    //and substract it by 63
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);


            let dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++).charCodeAt(0) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            let dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            points.push({ lat: (lat / PRECISION), lng: (lng / PRECISION) })
            // console.log(points);

        }
        return points;
    }

    polylineEncodings(coords) {
        let i = 0;

        let plat = 0;
        let plng = 0;

        var encoded_points = "";

        for (i = 0; i < coords.length; ++i) {
            var lat = coords[i][0];
            var lng = coords[i][1];

            encoded_points += this.encodePoint(plat, plng, lat, lng);

            plat = lat;
            plng = lng;
        }

        // close polyline
        encoded_points += this.encodePoint(plat, plng, coords[0][0], coords[0][1]);

        return encoded_points;
    }

    private encodePoint(plat, plng, lat, lng) {
        let late5 = Math.round(lat * PRECISION);
        let plate5 = Math.round(plat * PRECISION)

        let lnge5 = Math.round(lng * PRECISION);
        let plnge5 = Math.round(plng * PRECISION)

        let dlng = lnge5 - plnge5;
        let dlat = late5 - plate5;

        return this.encodeSignedNumber(dlat) + this.encodeSignedNumber(dlng);
    }

    private encodeSignedNumber(num) {
        let sgn_num = num << 1;

        if (num < 0) {
            sgn_num = ~(sgn_num);
        }

        return (this.encodeNumber(sgn_num));
    }

    private encodeNumber(num) {
        let encodeString = "";

        while (num >= 0x20) {
            encodeString += (String.fromCharCode((0x20 | (num & 0x1f)) + 63));
            num >>= 5;
        }

        encodeString += (String.fromCharCode(num + 63));
        return encodeString;
    }



    public decode(value) {

        let points = []
        let lat = 0
        let lon = 0

        let values = this.integers(value, function (x, y) {
            lat += x
            lon += y
            points.push([lat / PRECISION, lon / PRECISION])
        })

        return points;

    }

    private sign(value) {
        return value & 1 ? ~(value >>> 1) : (value >>> 1)
    }

    private integers(value, callback) {

        let values = 0
        let x = 0
        let y = 0

        let byte = 0
        let current = 0
        let bits = 0

        for (let i = 0; i < value.length; i++) {

            byte = value.charCodeAt(i) - 63
            current = current | ((byte & 0x1F) << bits)
            bits = bits + 5

            if (byte < 0x20) {
                if (++values & 1) {
                    x = this.sign(current)
                } else {
                    y = this.sign(current)
                    callback(x, y)
                }
                current = 0
                bits = 0
            }

        }

        return values;
    }

    // endcoder    


    public encode(points) {
        for (var i = 0x20; i < 0x7F; i++) {
            CHARMAP[i] = String.fromCharCode(i)
        }
        // px, py, x and y store rounded exponentiated versions of the values
        // they represent to compute the actual desired differences. This helps
        // with finer than 5 decimals floating point numbers.
        let px = 0, py = 0;
        let _this = this;
        return this.reduceEndcode(points, function (str, lat, lon) {

            let x = Math.round(lat * PRECISION);
            let y = Math.round(lon * PRECISION);

            str += _this.chars(_this.signEncode((x - px))) + _this.chars(_this.signEncode((y - py)));

            px = x;
            py = y;

            return str;

        })

    }

    private reduceEndcode(points, callback) {

        let point = null;

        let lat = 0;
        let lon = 0;
        let str = '';

        for (let i = 0; i < points.length; i++) {
            point = points[i]
            lat = point.lat || point.x || point[0]
            lon = point.lng || point.y || point[1]
            str = callback(str, lat, lon)
        }

        return str;

    }

    private signEncode(value) {
        return (value < 0) ? ~(value << 1) : (value << 1)
    }

    private charCode(value) {
        return ((value & 0x1F) | 0x20) + 63
    }

    private chars(value) {

        let str = ''

        while (value >= 0x20) {
            str += CHARMAP[this.charCode(value)];
            value = value >> 5;
        }

        str += CHARMAP[value + 63];

        return str;

    }

}
export class MarkerOptionCT {
    width?: number;
    heigth?: number;
    color?: string;//hex  
    mapIconUrl?: string;
}