export class ResultPopupMarkerEvent {
    type?: string;
    action: string;
    data: {
      pinActive?: boolean,
      value?: any
      lockActive?:boolean,
      follow?:boolean
    }
    constructor() {
      this.data = { value: {} };
    }
  }