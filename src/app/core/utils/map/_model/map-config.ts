import { StatusType } from './status-type';
import { IconType } from './icon-type';
import { MapOption } from './map-option';
import { MapFeature } from './map-feature';
import { MapType } from './map-type';

export class MapConfigModel {
    icons2:{
        car:IconType,
        truck:IconType,
        container:IconType,
        motorbike:IconType,
        motorcycles:IconType,
        other:IconType,
        direction:IconType,
        horse: IconType
    };
    icons:IconType[];
    status:{
        online:StatusType,
        sell:StatusType,
        noSell:StatusType,
        active:StatusType,
        run:StatusType,
        stop:StatusType,
        stopIgnitionOn:StatusType,
        lostSignal:StatusType,
        expired:StatusType,
        historyTransfer:StatusType,
        lostGPS:StatusType,
        lostGPRS:StatusType,
        inactive:StatusType,
        nodata:StatusType,
        stopIgnition: StatusType
    };
    options:MapOption;
    features:MapFeature;
    mapType:{
        // hybrid:MapType,
        roadmap:MapType,
        satellite:MapType,
        roadmapTopgps?:MapType,
        googleRoadmap:MapType,
        googleSatellite:MapType,
        // terrain:MapType,
    };
    infoBox?:{
        collapse?:boolean,
        base?:{
            label:{
                value:string;
                list:Array<{translate:string,value:string}>
            };
            icon:boolean;
            title:string;
            speed:boolean;
            duration:boolean;
            direction:boolean;
        },
        advance?:{
            icon:boolean;
            duration:boolean;
            speed:boolean;
            battery:boolean;
            gsm:boolean;
            numberPlate:boolean;
            imei:boolean;
            simno:boolean;
            vin:boolean;
            powerVoltage:boolean;
            distanceToday:boolean;
            driver:boolean;
            drivingToday:boolean;
            updateTime:boolean;
            group:boolean;
            geofence:boolean;
            address:boolean;
        }
    };
    version:string;
}
