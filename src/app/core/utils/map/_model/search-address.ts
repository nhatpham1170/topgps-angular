export interface SearchResults {
    datetime?: string;
    message?: string;
    messageCode?: string;
    result?: SearchResultDetail[];
    status?: number;
    active?: number;
}

export interface SearchResultDetail {
   address: string;
   lat: number;
   lng: number;
   isActive?:boolean
}
