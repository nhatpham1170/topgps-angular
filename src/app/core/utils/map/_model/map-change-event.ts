export class MapChangeEvent {
  type: "follow" | "unfollow" | string;
  data: any;
  option?: any;
}