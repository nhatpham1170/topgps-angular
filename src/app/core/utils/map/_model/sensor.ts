export class Sensor {
    type: string;
    icon: string;
    name: string;
    text: string;
    value: string;
    unit: string;
    scaleValue: string;

}
