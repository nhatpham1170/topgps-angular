import { StatusType } from './status-type';
import { Sensor } from './sensor';
import { IconType } from './icon-type';
import { MapConfig } from '../_config/map.config';

export class Item {
    id?: number;
    name?: string;
    imei?: string;
    time?: string;
    timestamp?: string;
    statusType?: StatusType;
    active?: number;
    address?: string;
    isACK?: boolean;
    isUpdated?: boolean;
    timeLocal?: string;
    sensors?: Sensor[];
    follow?: boolean;
    timeACK?: string;
    timeTitle?: string;
    direction?: number;
    iconType?: IconType;
    lat?: number;
    lng?: number;
    markerOption?: any;
    focus?:boolean;
    statusTimeStamp?:number;
    feature?: {
        hideOnMap: boolean;
    }
    constructor(item?: any) {
        this.id = 0;
        this.name = "";
        this.imei = "";
        this.time = "";
        this.timestamp = "";
        this.statusType = new MapConfig().defaults.status.stop;
        this.active = 0;
        this.address = "";
        this.isACK = false;
        this.isUpdated = false;
        this.timeLocal = "";
        this.sensors = [];
        this.follow = false;
        this.timeACK = "";
        this.timeTitle = "";
        this.direction = 0;
        this.iconType = new MapConfig().defaults.icons[0];
        this.lng = null;
        this.lat = null;
        this.focus = false;
        this.feature = { hideOnMap: false };
        this.statusTimeStamp  = 0;
        if (item) {
            let config = new MapConfig().configs();

            if (item['status']) {
                let statusTypeCheck = Object.keys(config.status).find(status => {
                    return config.status[status].key == item['status'];
                });
                if (statusTypeCheck) this.statusType = config.status[statusTypeCheck];
            }
            return Object.assign(this, item);
        }
    }
}
