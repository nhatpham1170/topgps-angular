
import { Component, OnInit,  } from '@angular/core';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { group } from '@angular/animations';
import { CurrentUserService } from '@core/auth';

@Injectable({
    providedIn: 'root'
})
export class ExportService {

  constructor(
    private translate: TranslateService,
    private currentUserService:CurrentUserService,

  ) {}

  getTimeZone()
  {
   let timezone =  ': '+this.currentUserService.currentTimeZone.timeZone +' '+ this.currentUserService.currentTimeZone.GMTOffset;
   return timezone;
  }

  renderDate(date)
  {
    let text = '';
    let d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    // console.log(d);
    let day = '' + d.getDate();
    // let year = d.getFullYear();
    if (month.length < 2)  month = '0' + month;
    if (day.length < 2)  day = '0' + day;
   

    var hours = d.getHours().toString();
    var minutes = d.getMinutes().toString();
    var seconds = d.getSeconds().toString();

    if (parseInt(hours) < 10)
        hours = "0" + hours;

    if (parseInt(minutes) < 10)
        minutes = "0" + minutes;

    if (parseInt(seconds) < 10)
        seconds = "0" + seconds;

    text = text+d.getFullYear();
    text = text+month;
    text = text+day;
    text = text+'.';
    text = text+hours;
    text = text+minutes;
    text = text+seconds;
    return text;
  }

  convertDeviceExport(deviceName)
  {
    let str = deviceName;
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    str = str.replace(': ', "");
    str = str.replace('.', "");

    // str = str.replace(' ', "_");
    str = str.split(" ").join("_")
    
    return str;
  }
  convertDateTimeExport(date)
  {
    let time = '';
    time = date.split('-').join('');
    time = time.split(' ').join('_');
    time = time.split(':').join('');
    return time;
  }

 convertTextData(text)
{
if(text == undefined || text == null) return '_';
return text;
}
}

