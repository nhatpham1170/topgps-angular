import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';


@Injectable({
  providedIn: 'root'
})
export class XlsxService {

  constructor() { }
  readXLSXToJson(files, fncCallBack) {
    let _this=this;  
    // let rABS = typeof FileReader !== "undefined" && (FileReader.prototype || {}).readAsBinaryString;
    let f = files[0];
    let reader = new FileReader();
    reader.onload = function(e) {
        try {
            let data = e.target['result'];
            // if(!rABS) data = new Uint8Array(data);
            let v = XLSX.read(data, {type:'binary'});
            // data = new Uint8Array(data);
            // let v = XLSX.read(data, {type:'binary'});
            let output = _this.toJson(v);
            fncCallBack(JSON.parse(output));
        }
        catch (e) {

        }

    };
    reader.readAsBinaryString(f);
  }
  private toJson(workbook) {
    let result = {};
    workbook.SheetNames.forEach(function (sheetName) {
      var roa = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], { header: 1 });
      // if(roa.length)
      result[sheetName] = roa;
    });
    return JSON.stringify(result);
  }
}
