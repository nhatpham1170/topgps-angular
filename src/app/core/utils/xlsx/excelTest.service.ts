import * as FileSaver from 'file-saver';
import * as XLSXJS from 'xlsx';
import * as moment from 'moment';
import { UserDateService } from '@core/_base/layout';
import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
// import { start } from 'repl';
// import {XLSX} from 'xlsx-style';
import { TranslateService } from '@ngx-translate/core';


declare var XLSX: any;
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
const CLOUMNS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
  'Y', 'Z'];
@Injectable({
  providedIn: 'root'
})
export class ExcelTestService {
  private defaultCellStyle = {
    font: { name: 'Times New Roman', sz: 12, color: { rgb: "#FF000000" }, bold: false, italic: false, underline: false },
    alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true },
    border: {
      top: { style: "thin", color: { "auto": 1 } }, right: {
        style: "thin",
        color: { "auto": 1 }
      }, bottom: { style: "thin", color: { "auto": 1 } }, left: { style: "thin", color: { "auto": 1 } }
    }
  };

  private styleHeaderFile = {
    font: { name: 'Times New Roman', sz: 14, bold: true },
    alignment: { vertical: "center", horizontal: "center", wrapText: true }
    };

  private styleHeader = { 
    font: { bold: true },
    alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true },
    fill: {
      patternType: "solid", // none / solid
      fgColor: { rgb: "EEEEEE" },
    },
    border: {
      top: { style: "thin", color: { "auto": 1 } },
      right: { style: "thin",color: { "auto": 1 }}, 
      bottom: { style: "thin", color: { "auto": 1}},
      left: { style: "thin", color: { "auto": 1 } }
    }
   };
   
   private styleTitleCol = {
    font: { name: 'Times New Roman', sz: 14, bold: true },
    alignment: { vertical: "center", horizontal: "left", wrapText: '1' },
    fill: {
      patternType: "solid", // none / solid
      fgColor: { rgb: "CCCCCC" },
      // bgColor: { rgb: "ccc" }
    },
    // border: {
    //   top: { style: "thin", color: { "auto": 1 }}, 
    //   right: {style: "thin",color: { "auto": 1 }},
    //   bottom: { style: "thin", color: { "auto": 1 }},
    //   left: { style: "thin", color: { "auto": 1 } }
    // }
  };

  constructor(
    private userDate: UserDateService,
    private translate: TranslateService,
    ) {
  }
 
  public exportFileTest(dataExecel: any[], config: any, option: { dateTimeNow?: string }) {
    let wb: XLSXJS.WorkBook = XLSXJS.utils.book_new();
    
    let rowFlag = {};
    rowFlag['headerStart'] = 1;
    // wb.Props = {
    //   Title: config.file.title || "",
    //   Subject: config.file.subject || "",
    //   Author: config.file.author || "",
    //   CreatedDate: config.file.createdDate || new Date(moment.now())
    // };
// begin tempalte sheet
config.content.table.forEach((sheet, sheetcount) => {
    let data = dataExecel[sheetcount];
    let config1 = sheet;
    let rowNumber = 1;
    let header;
    let table;
    let lenghtCol = sheet.widths.length;
    wb.SheetNames.push(config1.woorksheet.name || "Sheet 1");
    // add header
    let ws_data = [];
    let arrMerges = [];
    if(config.content.header)
    {
        header = config.content.header;
    }
    // add data to excel
    // add title report
    ws_data.push([header.title] || "");
    rowNumber++;
    rowFlag['headerTitle'] = rowNumber;
    rowFlag['params'] = [];
    // add params to excel
    ws_data.push("");
    rowNumber++;
    if(header.params.timeStart)
    {
        ws_data.push([this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START') + header.params.timeStart] || "");
        rowNumber++;
        rowFlag['params'].push(rowNumber);
    }
    if(header.params.timeEnd)
    {
        ws_data.push([this.translate.instant('REPORT.ROUTE.GENERAL.DATE_END') + header.params.timeEnd] || "");
        rowNumber++;
        rowFlag['params'].push(rowNumber);
    }
    if(header.params.deviceName)
    {
        ws_data.push([this.translate.instant('REPORT.DEVICE.GENERAL.DEVICE_NAME') + header.params.deviceName] || "");
        rowNumber++;
        rowFlag['params'].push(rowNumber);
    }
    if(header.params.timeZone)
    {
        ws_data.push([this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START') + header.params.timeZone] || "");
        rowNumber++;
        rowFlag['params'].push(rowNumber);
    }
    ws_data.push("");
    rowNumber++;
    // add table to excel
    //header table
    sheet.header.forEach((row, i) => {
        row.forEach((col, j) => {
// codeing here
        })
        
    })
    // add style to excel
    let ws = XLSXJS.utils.aoa_to_sheet(ws_data);

    if (rowFlag['headerTitle']) {
        let colName = "A1";
        ws[colName].s = this.styleHeaderFile;
        arrMerges.push(XLSXJS.utils.decode_range('A1' + ":" + CLOUMNS[lenghtCol - 1] + '1'));
      }
    
    ws['!merges'] = arrMerges;
    wb.Sheets[config1.woorksheet.name || "Sheet 1"] = ws;
});
    // end add multil sheet
    const excelBuffer: any = this.s2ab(XLSX.write(wb, { bookType: 'xlsx', type: 'binary', cellStyles: true, defaultCellStyle: this.defaultCellStyle }));
    // console.log(excelBuffer);
    this.saveAsExcelFile(excelBuffer, config.info.title + '_');
  }

  private s2ab(s) {
    if (typeof ArrayBuffer !== 'undefined') {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    } else {
      let buf = new Array(s.length);
      for (let i = 0; i != s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    // FileSaver.saveAs(new Blob([data], { type: 'application/octet-stream' }), 'exported.xlsx');
    let today = new Date();
    let day = today.getDate();
    let month = today.getMonth()+1;
    let year = today.getFullYear();
    let date = year+''+month+''+day;
  
    FileSaver.saveAs(data, fileName + '_export_' + date + EXCEL_EXTENSION);
  }

}
export class XLSXModel {
  file: {
    title: string,
    subject?: string,
    author?: string,
    createdDate?: Date,
    prefixFileName: string,
  };
  woorksheet: {
    name: string,
    defaultCellStyle?: any;
  };
  header: XLSXTitleModel[];
  columns: XLSXCellModel[];
  columnsMerge?:{
    data:any[],
    position?:any[],
    style?:any
  };
  total?: {
    group?:string,
    totalCol?: {
      field:XLSXTotalCol[],
    };
    totalAll?:{
      position? : {
        top: number,
        left: number
      }
      data: XLSXTotal[]
    }
  }
}

export class XLSXModelTest {
  file: {
    title: string,
    subject?: string,
    author?: string,
    createdDate?: Date,
    prefixFileName: string,
  };
  template: XLSXTemplate[];
}
export class XLSXTemplate{
  header: XLSXTitleModel[];
  woorksheet: {
    name: string,
    defaultCellStyle?: any;
  };
  columns: XLSXCellModel[];
  columnsMerge?:{   // Tiêu đề bảng có nhiều dòng
    data:any[],
    position?:any[],
    style?:any
  };
  total?: { // tính tổng
    group?:string, // title từng ngày (20-11-2019)
    totalCol?: {   // Tổng từng ngày
      field:XLSXTotalCol[],
    };
    totalAll?:{ // Tổng nhiều ngày
      position? : {
        top: number,
        left: number
      }
      data: XLSXTotal[]
    }
  }
}

export class XLSXTotalCol {
  text?: any;
  columnData?: string;
  style?: any;
  merge?: {
    full: boolean,
    range: string,
  };
}

export class XLSXCellModel {
  text?: any;
  name: string;
  columnData: string;
  type?: string;// set default to string
  format?: string;
  style?: any;
  link?: {
    target : string
  };
  merge?: {
    full: boolean,
    range: string,
  };
  wch: number;
}

export class XLSXTitleModel {
  text: any;
  style?: any;
  merge?: {
    full: boolean,
    range: string,
  };
  type?: string; // header: style for header default
}

export class XLSXTotal {
  key: any;
  value: any;
  style?: any;
  merge?: {
    full: boolean,
    range: string,
  };
  type?: string; // header: style for header default
}