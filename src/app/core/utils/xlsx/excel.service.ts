import * as FileSaver from 'file-saver';
import * as XLSXJS from 'xlsx';
import * as moment from 'moment';
import { UserDateService, UserDateAdvPipe } from '@core/_base/layout';
import { DatePipe, DecimalPipe } from '@angular/common';
import { Injectable } from '@angular/core';
// import { start } from 'repl';
// import {XLSX} from 'xlsx-style';
import { TranslateService } from '@ngx-translate/core';
import { CurrentUserService } from '@core/auth';
import { string } from '@amcharts/amcharts4/core';
import { isArray } from 'util';
import { PdfmakeService } from '../pdfmake/pdfmake.service';
import { ExportService } from '../export/export.service';


declare var XLSX: any;
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
const CLOUMNS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
  'Y', 'Z'];
@Injectable({
  providedIn: 'root',
})
export class ExcelService {
  private defaultCellStyle = {
    font: { name: 'Times New Roman', sz: 12, color: { rgb: "#FF000000" }, bold: false, italic: false, underline: false },
    alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true },
    border: {
      top: { style: "thin", color: { "auto": 1 } }, right: {
        style: "thin",
        color: { "auto": 1 }
      }, bottom: { style: "thin", color: { "auto": 1 } }, left: { style: "thin", color: { "auto": 1 } }
    }
  };

  private styleHeaderFile = {
    font: { name: 'Times New Roman', sz: 14, bold: true },
    alignment: { vertical: "center", horizontal: "center", wrapText: true }
  };

  private styleHeader = {
    font: { bold: true },
    alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true },
    fill: {
      patternType: "solid", // none / solid
      fgColor: { rgb: "EEEEEE" },
    },
    border: {
      top: { style: "thin", color: { "auto": 1 } },
      right: { style: "thin", color: { "auto": 1 } },
      bottom: { style: "thin", color: { "auto": 1 } },
      left: { style: "thin", color: { "auto": 1 } }
    }
  };

  private styleTitleCol = {
    font: { name: 'Times New Roman', sz: 14, bold: true },
    alignment: { vertical: "center", horizontal: "left", wrapText: '1' },
    fill: {
      patternType: "solid", // none / solid
      fgColor: { rgb: "CCCCCC" },
      // bgColor: { rgb: "ccc" }
    },
    // border: {
    //   top: { style: "thin", color: { "auto": 1 }}, 
    //   right: {style: "thin",color: { "auto": 1 }},
    //   bottom: { style: "thin", color: { "auto": 1 }},
    //   left: { style: "thin", color: { "auto": 1 } }
    // }
  };
  
  constructor(
    private userDate: UserDateService,
    private translate: TranslateService,
    private currentUserService:CurrentUserService,
    private pdf:PdfmakeService,
    private exportService:ExportService

  ) {
  }
  public exportFile(data: any[], config: XLSXModel, option: { dateTimeNow?: string }) {
    let wb: XLSXJS.WorkBook = XLSXJS.utils.book_new();
    let rowNumber = 1;
    let rowFlag = {};
    rowFlag['headerStart'] = rowNumber;
    wb.Props = {
      Title: config.file.title || "",
      Subject: config.file.subject || "",
      Author: config.file.author || "",
      CreatedDate: config.file.createdDate || new Date(moment.now())
    };

    wb.SheetNames.push(config.woorksheet.name || "Sheet 1");
    // wb.SheetNames.push("Sheet 2");
    // add header
    let ws_data = [];
    let arrMerges = [];
    config.header.forEach(x => {
      if (isArray(x.text)) {
        ws_data.push(x.text);
      }
      else {
        switch (x.text) {
          case "!timezone":
            ws_data.push([this.translate.instant('COMMON.EXCEL.TIMEZONE') + ": "
              + this.currentUserService.currentTimeZone.timeZone + " "
              + this.currentUserService.currentTimeZone.GMTOffset]);
            break;
          case "!userLogin":
            ws_data.push([this.translate.instant('COMMON.EXCEL.USER_LOGIN') + ": "
              + this.currentUserService.currentUser.username]);
            break;
          default:
            ws_data.push([x.text]);
            break;
        }
      }
      rowNumber++;
    });
    rowFlag['headerEnd'] = rowNumber;
    // sub header
    // if(config.subHeader){
    //   if(config.subHeader.userLogin){
    //     ws_data.push([this.translate.instant('COMMON.EXCEL.USER_LOGIN') + ": "
    //       + this.currentUserService.currentUser.username]);
    //       rowNumber++;
    //   }
    //   if(config.subHeader.timezone){
    //     ws_data.push([this.translate.instant('COMMON.EXCEL.TIMEZONE') + ": " 
    //       + this.currentUserService.currentTimeZone.timeZone + " " 
    //       + this.currentUserService.currentTimeZone.GMTOffset]);
    //       rowNumber++;
    //   }
    //   ws_data.push([""]);
    //   rowNumber++;
    // }
    // add column name
    let row = [];
    config.columns.forEach(x => {
      row.push(x.name);
    });
    ws_data.push(row || "");


    rowFlag['columnName'] = rowNumber;
    rowNumber++;
    rowFlag['rowDataStart'] = rowNumber++;
    // add data row
    let _this = this;

    data.forEach((d, i) => {
      let row = [];
      config.columns.forEach(c => {
        if (c.columnData == "auto") {
          row.push((i + 1).toString());
        }
        else {
          let value = "";
          switch (c.type) {

            case "date":
              value = new UserDateAdvPipe(this.translate, this.currentUserService).transform(d[c.columnData], c.format || "date");
              row.push(value || "");
              break;
            case "datetime":
              value = new UserDateAdvPipe(this.translate, this.currentUserService).transform(d[c.columnData], "datetime");
              row.push(value || "");
              break;
            case "number":
              // value = new DecimalPipe("en-US").transform( d[c.columnData] || 0);
              // row.push(value || "");
              row.push(d[c.columnData] || 0);
              break;
            default:
              row.push(d[c.columnData] || "");
              break;
          }
        }
      });
      ws_data.push(row || "");
      rowNumber++;
    });
    rowFlag['rowDataEnd'] = rowNumber;
    
    // set style and ranger
    let ws = XLSXJS.utils.aoa_to_sheet(ws_data);

    // set width column
    ws["!cols"] = config.columns.map(x => {
      return { wch: x.wch };
    });
  
    // style and merge for header
    config.header.forEach((v, i) => {
      let colName = "A" + (i + 1);
      if (v.type == "header") {
        ws[colName].s = {
          // fill: {
          //   patternType: "solid", // none / solid
          //   fgColor: { rgb: "CCCCCC" },
          //   bgColor: { rgb: "ccc" }
          // },
          font: { name: 'Times New Roman', sz: 14, bold: true },
          alignment: { vertical: "center", horizontal: "center", wrapText: true },
          // border: {
          //   top: { style: "solid", color: { auto: 1 } },
          //   right: { style: "solid", color: { auto: 1 } },
          //   bottom: { style: "solid", color: { auto: 1 } },
          //   left: { style: "solid", color: { auto: 1 } }
          // }
        };
        if (v.style) {
          ws[colName].s = Object.assign({}, ws[colName].s, v.style);
        }
        arrMerges.push(XLSXJS.utils.decode_range(CLOUMNS[i] + '1' + ":" + CLOUMNS[config.columns.length - 1] + '1'));
      }
      else {
        ws[colName].s = v.style || {};
        if (v.merge) {
          if (v.merge.full) {
            arrMerges.push(XLSXJS.utils.decode_range(CLOUMNS[i] + '1' + ":" + CLOUMNS[config.columns.length - 1] + '1'));
          }
          else arrMerges.push(XLSXJS.utils.decode_range(v.merge.range));
        }
      }
    });

    // style and merge for cell
    let rowDataStart = rowFlag['rowDataStart'];
    data.forEach((d, di) => {
      config.columns.forEach((c, ci) => {
        let colName = CLOUMNS[ci] + (rowDataStart + di);
        if (c.style) {
          ws[colName].s =  c.style || {};
        }
      });
    });

    ws['!merges'] = arrMerges;

    // style for column name 
    config.columns.forEach((v, i) => {
      let cellName = CLOUMNS[i] + rowFlag['columnName'];
      ws[cellName].s = v.styleColumn || config.styleColumn || { font: { bold: true } };
    });
    let defaultCellStyle = {
      font: { name: 'Times New Roman', sz: 13, color: { rgb: "#FF000000" }, bold: false, italic: false, underline: false },
      alignment: { vertical: "center", horizontal: "center", indent: 0, wrapText: true },
      border: {
        top: { style: "thin", color: { "auto": 1 } }, right: {
          style: "thin",
          color: { "auto": 1 }
        }, bottom: { style: "thin", color: { "auto": 1 } }, left: { style: "thin", color: { "auto": 1 } }
      }
    };
    if (config.woorksheet.defaultCellStyle != undefined) {
      this.defaultCellStyle = Object.assign({}, this.defaultCellStyle, config.woorksheet.defaultCellStyle);
    }
    wb.Sheets[config.woorksheet.name || "Sheet 1"] = ws;

    const excelBuffer: any = this.s2ab(XLSX.write(wb, { bookType: 'xlsx', type: 'binary', cellStyles: true, defaultCellStyle: this.defaultCellStyle }));
    this.saveAsExcelFileXLSX(excelBuffer, config);
  }
  
  /**
   * Render sheet
   * @param data 
   * @param config 
   * @param option 
   */
  public renderSheet(data: any[], config: XLSXModel, option: { dateTimeNow?: string,rowNumber?:number,woorkSheet?:any }) {
    let rowNumber = 1;
    let rowStart = 1;
    if(option.rowNumber){
      rowNumber = option.rowNumber;
      rowStart = option.rowNumber;
    }
    let rowFlag = {};
    rowFlag['headerStart'] = rowNumber;

    // add header
    let ws_data = [];
    let arrMerges = [];
    config.header.forEach(x => {
      if (isArray(x.text)) {
        ws_data.push(x.text);
      }
      else {
        switch (x.text) {
          case "!timezone":
            ws_data.push([this.translate.instant('COMMON.EXCEL.TIMEZONE') + ": "
              + this.currentUserService.currentTimeZone.timeZone + " "
              + this.currentUserService.currentTimeZone.GMTOffset]);
            break;
          case "!userLogin":
            ws_data.push([this.translate.instant('COMMON.EXCEL.USER_LOGIN') + ": "
              + this.currentUserService.currentUser.username]);
            break;
          default:
            ws_data.push([x.text]);
            break;
        }
      }
      rowNumber++;
    });
    rowFlag['headerEnd'] = rowNumber;

    // add column name
    let row = [];
    if(config.showColumn!==false){
      config.columns.forEach(x => {
        row.push(x.name);
      });
      ws_data.push(row || "");
      rowFlag['columnName'] = rowNumber;
      rowNumber++;
    }
    //add sub column 
    if(config.subColumn && config.subColumn.length>0){
      rowFlag['subColumnStart'] = rowNumber;

      config.subColumn.forEach(x => {
        if (isArray(x.text)) {
          ws_data.push(x.text);
        }
        else {
          switch (x.text) {
            case "!timezone":
              ws_data.push([this.translate.instant('COMMON.EXCEL.TIMEZONE') + ": "
                + this.currentUserService.currentTimeZone.timeZone + " "
                + this.currentUserService.currentTimeZone.GMTOffset]);
              break;
            case "!userLogin":
              ws_data.push([this.translate.instant('COMMON.EXCEL.USER_LOGIN') + ": "
                + this.currentUserService.currentUser.username]);
              break;
            default:
              ws_data.push([x.text]);
              break;
          }
        }
        rowNumber++;
      });
      rowFlag['subColumnEnd'] =  ws_data.length + 1;
    }
  
    rowFlag['rowDataStart'] = rowNumber++;
    // add data row - body
    data.forEach((d, i) => {
      let row = [];
      config.columns.forEach(c => {
        if (c.columnData == "auto") {
          row.push((i + 1).toString());
        }
        else {
          let value = "";
          switch (c.type) {

            case "date":
              value = new UserDateAdvPipe(this.translate, this.currentUserService).transform(d[c.columnData], c.format || "date");
              row.push(value || "");
              break;
            case "datetime":
              value = new UserDateAdvPipe(this.translate, this.currentUserService).transform(d[c.columnData], "datetime");
              row.push(value || "");
              break;
            case "number":
              // value = new DecimalPipe("en-US").transform( d[c.columnData] || 0);
              // row.push(value || "");
              row.push(d[c.columnData] || 0);
              break;
            default:
              row.push(d[c.columnData] || "");
              break;
          }
        }
      });
      ws_data.push(row || "");
      rowNumber++;
    });
    rowFlag['rowDataEnd'] = rowNumber;
    
    // add footer
    if(config.footer && config.footer.length>0){
      // rowNumber++;
      rowFlag['footerStart'] = ws_data.length + 1;

      config.footer.forEach(x => {
        if (isArray(x.text)) {
          ws_data.push(x.text);
        }
        else {
          switch (x.text) {
            case "!timezone":
              ws_data.push([this.translate.instant('COMMON.EXCEL.TIMEZONE') + ": "
                + this.currentUserService.currentTimeZone.timeZone + " "
                + this.currentUserService.currentTimeZone.GMTOffset]);
              break;
            case "!userLogin":
              ws_data.push([this.translate.instant('COMMON.EXCEL.USER_LOGIN') + ": "
                + this.currentUserService.currentUser.username]);
              break;
            default:
              ws_data.push([x.text]);
              break;
          }
        }
        rowNumber++;
      });
      rowFlag['footerEnd'] =  ws_data.length + 1;
    }
  
    if(option.woorkSheet){
      option.woorkSheet["!ws_data"] = option.woorkSheet["!ws_data"].concat(ws_data);
    }

    // set style and ranger
    let ws = XLSXJS.utils.aoa_to_sheet(ws_data);
    ws["!ws_data"] = ws_data;
    // ws["!data"] = data;
    if(option.woorkSheet){
      ws = option.woorkSheet;
      ws = XLSXJS.utils.aoa_to_sheet(option.woorkSheet["!ws_data"]);
      let woorkSheetOld = JSON.parse(JSON.stringify(option.woorkSheet));
      delete woorkSheetOld['!ref'];
      delete woorkSheetOld['!ws_data'];
      delete woorkSheetOld['!cols'];
      delete woorkSheetOld['!merges'];
      delete woorkSheetOld['!rowNumber'];
      Object.assign(ws,woorkSheetOld);
      ws["!ws_data"] = option.woorkSheet["!ws_data"];
    }

    // set width column
    ws["!cols"] = config.columns.map(x => {
      return { wch: x.wch};
    });

    // style and merge for header
    config.header.forEach((v, i) => {
      let colName = "A" + (i + rowStart);
      if (v.type == "header") {
        ws[colName].s = { 
          font: { name: 'Times New Roman', sz: 14, bold: true },
          alignment: { vertical: "center", horizontal: "center", wrapText: true },
        };
        if (v.style) {
          ws[colName].s = Object.assign({}, ws[colName].s, v.style);
        }
        
        arrMerges.push(XLSXJS.utils.decode_range(CLOUMNS[i] + (i +  rowStart) + ":" + CLOUMNS[config.columns.length - 1] + (i +  rowStart)));
      }
      else {
        ws[colName].s = v.style || {};
   
        if (v.merge) {
          if (v.merge.full) {
            arrMerges.push(XLSXJS.utils.decode_range(CLOUMNS[i] + (i +  rowStart) + ":" + CLOUMNS[config.columns.length - 1] + (i +  rowStart)));
          }
          else arrMerges.push(XLSXJS.utils.decode_range(v.merge.range));
        }
      }
    });

    // style and merge for cell
    let rowDataStart = rowFlag['rowDataStart'];
      data.forEach((d, di) => {
      config.columns.forEach((c, ci) => {
        let colName = CLOUMNS[ci] + (rowDataStart + di);
        if (c.style) {
          ws[colName].s = c.style || {};
        }
      });
    });
  // style and merge for subColumn
  if(config.subColumn && config.subColumn.length > 0){
    config.subColumn.forEach((v, i) => {
      let colName = "A" + (i +  rowFlag['subColumnStart']);
      if (v.type == "title") {
        ws[colName].s = { 
          font: { name: 'Times New Roman', sz: 14, bold: true },
          alignment: { vertical: "center", horizontal: "center", wrapText: true },
        };
        if (v.style) {
          ws[colName].s = Object.assign({}, ws[colName].s, v.style);
        }
        arrMerges.push(XLSXJS.utils.decode_range(CLOUMNS[i] +  (i +  rowFlag['subColumnStart']) + ":" + CLOUMNS[config.columns.length - 1] +  (i +  rowFlag['subColumnStart']))); 
        // arrMerges.push(XLSXJS.utils.decode_range(CLOUMNS[i] +  rowFlag['subColumnStart'] + ":" + CLOUMNS[config.columns.length - 1] +  rowFlag['subColumnStart']));
      }
      else {
        ws[colName].s = v.style || {};
  
        if (v.merge) {
          if (v.merge.full) {
            arrMerges.push(XLSXJS.utils.decode_range(CLOUMNS[i] + (i +  rowFlag['subColumnStart']) + ":" + CLOUMNS[config.columns.length - 1] + (i +  rowFlag['subColumnStart'])));
          }
          else arrMerges.push(XLSXJS.utils.decode_range(v.merge.range));
        }
      }
    });
  }

    // style and merge for footer
    if(config.footer && config.footer.length > 0){
      config.footer.forEach((v, i) => {
        let colName = "A" + (i +  rowFlag['footerStart']);
        if (v.type == "footer") {
          ws[colName].s = { 
            font: { name: 'Times New Roman', sz: 14, bold: true },
            alignment: { vertical: "center", horizontal: "center", wrapText: true },
          };
          if (v.style) {
            ws[colName].s = Object.assign({}, ws[colName].s, v.style);
          }
          
          arrMerges.push(XLSXJS.utils.decode_range(CLOUMNS[i] +  (i +  rowFlag['footerStart']) + ":" + CLOUMNS[config.columns.length - 1] +  (i +  rowFlag['footerStart'])));
        }
        else {
          ws[colName].s = v.style || {};
     
          if (v.merge) {
            if (v.merge.full) {
              arrMerges.push(XLSXJS.utils.decode_range(CLOUMNS[i] + (i +  rowFlag['footerStart']) + ":" + CLOUMNS[config.columns.length - 1] + (i +  rowFlag['footerStart'])));
            }
            else arrMerges.push(XLSXJS.utils.decode_range(v.merge.range));
          }
        }
      });
    }
   

    if(option.woorkSheet){
      ws['!merges'] = option.woorkSheet['!merges'];
      arrMerges.forEach(x=>{
        ws['!merges'].push(x);
      })
    }
    else{
      ws['!merges'] = arrMerges;
    }

    // style for column name 
    if(config.showColumn!==false){
      config.columns.forEach((v, i) => {
        let cellName = CLOUMNS[i] + (rowFlag['columnName']);
        ws[cellName]['s'] = v.styleColumn || config.styleColumn || { font: { bold: true } };
      });
    }
    
    if (config.woorksheet.defaultCellStyle != undefined) {
      this.defaultCellStyle = Object.assign({}, this.defaultCellStyle, config.woorksheet.defaultCellStyle);
    }
  
    return {woorksheet:ws,rowNumber:rowNumber};
  }
  public exportFileAdvanced(configAdv: {
    sheets: Array<{
      data: any[], config: XLSXModel, option: { dateTimeNow?: string}
    }>,
    mergeSheet?: boolean
  }) 
  {
    
    let wb: XLSXJS.WorkBook = XLSXJS.utils.book_new();
    let rowNumber = 1;
    let rowFlag = {};
    rowFlag['headerStart'] = rowNumber;
    let config;
    if(configAdv.mergeSheet === undefined) configAdv.mergeSheet = false;
    if(configAdv.sheets.length>0){
      config = configAdv.sheets[0].config;
      wb.Props = {
        Title: config.file.title || "",
        Subject: config.file.subject || "",
        Author: config.file.author || "",
        CreatedDate: config.file.createdDate || new Date(moment.now())
      };

      if(configAdv.mergeSheet){
        let ws = null;
        wb.SheetNames.push(config.woorksheet.name || "Sheet 1");
        configAdv.sheets.forEach(x=>{
          x.option['rowNumber'] = rowNumber;
          if(ws!=null){
            x.option['woorkSheet'] = ws;
          }
          
          let wsResult = this.renderSheet(x.data,x.config,x.option);
          ws  = wsResult.woorksheet; 
          rowNumber = wsResult.rowNumber-1;
        });
        wb.Sheets[config.woorksheet.name || "Sheet 1"] = ws;
        const excelBuffer: any = this.s2ab(XLSX.write(wb, { bookType: 'xlsx', type: 'binary', cellStyles: true, defaultCellStyle: this.defaultCellStyle }));
        this.saveAsExcelFileXLSX(excelBuffer, config);

      }else{

        configAdv.sheets.forEach((sheet,index)=>{
          let wsResult = this.renderSheet(sheet.data,sheet.config,sheet.option);
          rowNumber += (wsResult.rowNumber + 1);
          wb.SheetNames.push(sheet.config.woorksheet.name || ("Sheet "+index+1));
          wb.Sheets[sheet.config.woorksheet.name || ("Sheet "+index+1)] = wsResult.woorksheet;
        });
        const excelBuffer: any = this.s2ab(XLSX.write(wb, { bookType: 'xlsx', type: 'binary', cellStyles: true, defaultCellStyle: this.defaultCellStyle }));
        this.saveAsExcelFileXLSX(excelBuffer, config);
      }
    }
  }

  convertParamsHeader(text)
  {
    let result:any = [];
    let start = text.indexOf(":");
    let key = text.slice(0,start+1);
    let value = text.slice(start+1,text.length);
    result = [key,null,value];    
    if(start == -1) result = [text];
    return result;
  }

  public exportFileTest(dataExecel: any[], config: XLSXModelTest, option: { dateTimeNow?: string }) {
    let wb: XLSXJS.WorkBook = XLSXJS.utils.book_new();
    let fuel = 'fuel';
    if(config.file.columnParent) fuel = config.file.columnParent;
    let rowFlag = {};
    rowFlag['headerStart'] = 1;
    wb.Props = {
      Title: config.file.title || "",
      Subject: config.file.subject || "",
      Author: config.file.author || "",
      CreatedDate: config.file.createdDate || new Date(moment.now())
    };
    // begin tempalte sheet
    config.template.forEach((sheet, sheetcount) => {
      let data = dataExecel[sheetcount];
      let config1 = sheet;
      let rowNumber = 1;
      wb.SheetNames.push(config1.woorksheet.name || "Sheet 1");
      // add header

      let ws_data = [];
      let arrMerges = [];

      config1.header.forEach(x => {
        if(isArray(x.text)){
          ws_data.push(x.text);
        }
        else{
          switch(x.text){
            case "!timezone":
              ws_data.push(this.convertParamsHeader(this.translate.instant('MANAGE.USER.PARAMETER.TIMEZONE') + ": " 
              + this.currentUserService.currentTimeZone.timeZone + " " 
              + this.currentUserService.currentTimeZone.GMTOffset));
              break;
            case "!userLogin":
              ws_data.push(this.convertParamsHeader(this.translate.instant('COMMON.EXCEL.USER_LOGIN') + ": "
              + this.currentUserService.currentUser.username));
              break;
            default:
              ws_data.push(this.convertParamsHeader(x.text));
              
              break;
          }
        }
        rowNumber++;
      });

      // config1.header.forEach(x => {
      //   ws_data.push([x.text] || "");
      //   rowNumber++;
      // });
      rowFlag['headerEnd'] = rowNumber;

      // add column name . begin loop template
      // let template = config.template;
      // template.forEach((item, j) => {
      //   config = item;
      // });
      let row = [];
      config1.columns.forEach((x, i) => {
        row.push(x.name);
        //merge col header table
        if (x.merge && x.merge.colSpan) {
          let range = CLOUMNS[i] + rowNumber + ":" + CLOUMNS[i + x.merge.colSpan] + rowNumber;
          arrMerges.push(XLSXJS.utils.decode_range(range));
        }
        //merge row header table
        if (x.merge && x.merge.rowSpan) {
          let rowNumberEnd = rowNumber + x.merge.rowSpan;
          let range = CLOUMNS[i] + rowNumber + ":" + CLOUMNS[i] + rowNumberEnd;
          // console.log(range);
          arrMerges.push(XLSXJS.utils.decode_range(range));
        }
      });
      ws_data.push(row || "");
      rowFlag['columnName'] = rowNumber;
      rowNumber++;
      //let row test
      if (config1.columnsMerge) {
        ws_data.push(config1.columnsMerge.data || "");
        if (config1.columnsMerge.position) {
          config1.columnsMerge.position.forEach((d, i) => {
            arrMerges.push(XLSXJS.utils.decode_range(d));
          })
        }

        rowNumber++;
      }

      // rowNumber++;
      // rowFlag['rowDataStart'] = rowNumber++;
      // // add data array

      let rowTitleData = [];
      let rowTotalData = [];

      data.forEach((item, j) => {
        let dateItem = item.data;
        let date;
        let _this = this;
        rowTitleData.push(rowNumber);

        if (config1.total && config1.total.group) {

          date = item[config1.total.group];
          let rowTitle = [date];
          ws_data.push(rowTitle || "");
          rowNumber++;
          // add  title date group(2010-10-19)

        };
        //add array data to array
        // console.log(dateItem);
     
        
        if (dateItem != null) {
           
          dateItem.forEach((d, i) => {
            
            let dataFuel:any;
            
            let startRow = rowNumber;
            
            let endRow   = rowNumber+1;
            let lenghtFuel = 0;
            let checkColumnParent:boolean = false;
       

            if(!d.hasOwnProperty(fuel) || d[fuel].length == 0) d[fuel] =  [{nametext:''}];
            if(d[fuel]) dataFuel = d.fuel;
            lenghtFuel = dataFuel.length;
            if(lenghtFuel > 1 ) checkColumnParent = true;
            dataFuel.forEach((itemdataFuel, countFuel) => {
              let row:any = [];
              config1.columns.forEach((c,countConfig)=> {
                if(c.hasOwnProperty("columnParent"))
                {                  
                  row.push(this.exportService.convertTextData(itemdataFuel[c.columnData]));
                }
                 else 
                {
                  // console.log(CLOUMNS[countConfig] + startRow);
                  
                  if(countFuel == 0 && checkColumnParent) arrMerges.push(XLSXJS.utils.decode_range(CLOUMNS[countConfig] + startRow + ":" + CLOUMNS[countConfig] + endRow));
                  if (c.columnData == "auto") {
                    row.push((i + 1).toString());
                  }else{                    
                    row.push(this.exportService.convertTextData(d[c.columnData]));
                  }
                }

              });
              // console.log(row); 
              
              ws_data.push(row);
              rowNumber++;
            });
          })

        }
        // get all total

        //add total row group
        if (config1.total && config1.total.totalCol) {
          let totalCol_field = config1.total.totalCol.field;
          let rowTotal = [];
          let style = [];
          let merge;

          totalCol_field.forEach((v, i) => {
            if (v.text) rowTotal.push(this.exportService.convertTextData(v.text));
            if (v.columnData) {
              let field = v.columnData;
              let itemData = item[field];
              rowTotal.push(this.exportService.convertTextData(itemData));
            }
           
              style.push(v.style || '');
            
            if (v.merge) {
              merge = v.merge.range;
            }
          })
          ws_data.push(this.exportService.convertTextData(rowTotal));

          rowTotalData.push({
            rowNumber: rowNumber,
            style: style,
            merge: merge
          });
          rowNumber++;
        }
        return;
      });
      rowFlag['rowDataEnd'] = rowNumber;
      
      // add all total data
      let numberAllStart = rowNumber;
      let allTotalRow = [];
      if (config1.total && config1.total.totalAll) {
        let top = 0;
        let left = 0;
        let data = [];
        let rowTotal = [];
        if (config1.total.totalAll.position.top) top = config1.total.totalAll.position.top;
        if (config1.total.totalAll.position.left) left = config1.total.totalAll.position.left;
        if (config1.total.totalAll.data) data = config1.total.totalAll.data;
        for (var j = 0; j < top; j++) {
          ws_data.push("");
        }
        data.forEach((c, ci) => {
          rowTotal = [];
          for (var j = 0; j < left; j++) {
            rowTotal.push("");
          }
          rowTotal.push(c.key);
          rowTotal.push(c.value);
          ws_data.push(this.exportService.convertTextData(rowTotal));

        })
      };

      // set style and ranger

      let ws = XLSXJS.utils.aoa_to_sheet(ws_data);

      // set width column
      ws["!cols"] = config1.columns.map(x => {
        return { wch: x.wch };
      });
      // set height column
      ws["!rows"] = [
        { hpt:30},
        { hpt:30},
        { hpt:30}
      ];
      

      // end loop template  data

      //begin loop teplate style
      // style and merge for header
      config1.header.forEach((v, i) => {
        let colName = "A" + (i + 1);
        if (v.type == "header" && ws[colName]) {
          ws[colName].s = this.styleHeaderFile;
          if (v.style && ws[colName]) {
            ws[colName].s = Object.assign({}, ws[colName].s, v.style);
          }
          arrMerges.push(XLSXJS.utils.decode_range(CLOUMNS[i] + '1' + ":" + CLOUMNS[config1.columns.length - 1] + '1'));
        }
        else {
          ws[colName].s = v.style || {};
          if (v.merge) {
            if (v.merge.full) {
              arrMerges.push(XLSXJS.utils.decode_range(CLOUMNS[i] + '1' + ":" + CLOUMNS[config1.columns.length - 1] + '1'));
            }
            else arrMerges.push(XLSXJS.utils.decode_range(v.merge.range));
          }
        }
      });

      // add first row title style
      if (config1.total && config1.total.group) {
        rowTitleData.forEach((v, i) => {
          arrMerges.push(XLSXJS.utils.decode_range('A' + v + ":" + CLOUMNS[config1.columns.length - 1] + v));
          if (ws["A" + (v)]) ws["A" + (v)].s = this.styleTitleCol;
          // config.columns.forEach((c, ci) => {
          //   let colName = CLOUMNS[ci] + (v);
          //   ws[colName].s = this.styleTitleCol;
          // });
        });
      }
      // add end row total style
      rowTotalData.forEach((v, i) => {
        let numberLast = v.rowNumber;

        config1.columns.forEach((c, ci) => {
          let colName = CLOUMNS[ci] + (numberLast);
          if (v.style && ws[colName]) {
            if(config1.total && config1.total.totalCol )
            {
              ws[colName].s = v.style[ci] || {};
            }else
            {
              ws[colName].s = {
                
                  font: { name: 'Times New Roman', sz: 14, bold: true },
                  alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true }
                
              }
            }
        
          }
        });
        if (v.merge) {
          let merge = parseInt(v.merge);
          let mergeToCol = CLOUMNS[merge] + (numberLast);
          arrMerges.push(XLSXJS.utils.decode_range('A' + (numberLast) + ":" + mergeToCol));
        }
      });
      rowNumber++;

      //add row all total data style
      if (config1.total && config1.total.totalAll) {
        numberAllStart = numberAllStart + config1.total.totalAll.position.top;
        allTotalRow = config1.total.totalAll.data;
        allTotalRow.forEach((v, i) => {
          config1.columns.forEach((c, ci) => {
            if (ci > config1.total.totalAll.position.left - 1) {
              let colName = CLOUMNS[ci] + (numberAllStart + i);
              if (v.style && ws[colName]) ws[colName].s = v.style;
            }
          });
        });
      }

      rowNumber++;
      // style and merge for cell style
      data.forEach((item, j) => {
        let dateItem = item.data;
        let lengthFuel = 0;
        // sensor fuel
        if (dateItem != null)
        {
          dateItem = dateItem.filter(data1=>{
            if(data1.hasOwnProperty(fuel) && data1[fuel].length > 1)
            {
              lengthFuel = lengthFuel+data1[fuel].length-1;
            }
            return data1;
          })
        }
    

        for (let i = 0; i < lengthFuel; i++) {
          dateItem.push({name:''})
        }
          // end sensor fuel
        let rowDataStart = rowTitleData[j];
        if (config1.total && config1.total.group) {
          rowDataStart = rowTitleData[j] + 1;
        }
        if (dateItem != null && dateItem.length > 0 && dateItem != undefined) {
          dateItem.forEach((d, di) => {
      
            config1.columns.forEach((c, ci) => {
              let colName = CLOUMNS[ci] + (rowDataStart + di);

              if (c.style && ws[colName]) {
                ws[colName].s = c.style || {};
              }
            });
          });
        }
      });
      

      ws['!merges'] = arrMerges;
      // style for column name table
      config1.columns.forEach((v, i) => {
        let cellName = CLOUMNS[i] + rowFlag['columnName'];
        if (ws[cellName]) ws[cellName].s = this.styleHeader;

      });
      //set style column merge
      if(config1.columnsMerge)
      {
      rowFlag['columnName'] = rowFlag['columnName'] + 1;
      config1.columns.forEach((v, i) => {
        let cellName = CLOUMNS[i] + rowFlag['columnName'];
        if (ws[cellName]) ws[cellName].s = this.styleHeader;
      });
      }
      // end style column name
      let defaultCellStyle = {
        font: { name: 'Times New Roman', sz: 12, color: { rgb: "#FF000000" }, bold: false, italic: false, underline: false },
        alignment: { vertical: "center", horizontal: "right", indent: 0, wrapText: true },
        border: {
          top: { style: "thin", color: { "auto": 1 } }, right: {
            style: "thin",
            color: { "auto": 1 }
          }, bottom: { style: "thin", color: { "auto": 1 } }, left: { style: "thin", color: { "auto": 1 } }
        }
      };
      if (config1.woorksheet.defaultCellStyle != undefined) {
        defaultCellStyle = Object.assign({}, defaultCellStyle, config1.woorksheet.defaultCellStyle);
      }
      // end loop template style
      // add multil sheet
      // console.log(ws);
      // ws = { '!type': 'chart',
      // A1: { t: 'n', v: 1, z: 'General' },
      // A2: { t: 'n', v: 2, z: 'General' },
      // A3: { t: 'n', v: 3, z: 'General' },
      // A4: { t: 'n', v: 4, z: 'General' },
      // B1: { t: 'n', v: 1, z: 'General' },
      // B2: { t: 'n', v: 4, z: 'General' },
      // B3: { t: 'n', v: 9, z: 'General' },
      // B4: { t: 'n', v: 16, z: 'General' },
      // '!ref': 'A1:B4' };
      wb.Sheets[config1.woorksheet.name || "Sheet 1"] = ws;
    });
    // end add multil sheet
    const excelBuffer: any = this.s2ab(XLSX.write(wb, {compression:true, bookType: 'xlsx', type: 'binary', cellStyles: true, defaultCellStyle: this.defaultCellStyle }));
    // console.log(excelBuffer);
    let dateNow   =  moment().format('YYYY-MM-DD HH:mm:ss');
    let timeStart = config.file.timeStart || dateNow;
    let timeEnd   = config.file.timeEnd || dateNow;
    let deviceName   = config.file.devices || '-1';

    timeStart = this.exportService.convertDateTimeExport(timeStart);
    timeEnd   =  this.exportService.convertDateTimeExport(timeEnd);
    dateNow =  this.exportService.convertDateTimeExport(dateNow);
    
    let file_name = config.file.prefixFileName;
    if(deviceName != '-1') 
    {
      deviceName =  this.exportService.convertDeviceExport(deviceName);
      file_name = file_name+'__'+deviceName+'_';
    }
    
    file_name = file_name+'__'+timeStart+'__'+timeEnd+'__'+'export'+'__'+dateNow;
    // console.log(file_name);
    // return;
    this.saveAsExcelFile(excelBuffer, file_name);
  }
// Code config same pdf vandinh

  private s2ab(s) {
    if (typeof ArrayBuffer !== 'undefined') {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    } else {
      let buf = new Array(s.length);
      for (let i = 0; i != s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }
  }
  private saveAsExcelFileXLSX(buffer: any, fileModel: XLSXModel): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    // FileSaver.saveAs(new Blob([data], { type: 'application/octet-stream' }), 'exported.xlsx');
    let suffix:string = `export_${ moment().format('YYYYMMDD_HHmmss')}`;
    let modelName:string = `${fileModel.file.typeName?fileModel.file.typeName + '__':""}${fileModel.file.objName?fileModel.file.objName+"__":""}`;
    let time:string = `${fileModel.file.timeStart?fileModel.file.timeStart+"__":""}${fileModel.file.timeEnd?fileModel.file.timeEnd+"__":""}`;
    let fileName = `${modelName}${time}${suffix}.xlsx`;
    FileSaver.saveAs(data, fileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    // FileSaver.saveAs(new Blob([data], { type: 'application/octet-stream' }), 'exported.xlsx');
    let today = new Date();
    let day = today.getDate();
    let month = today.getMonth() + 1;
    let year = today.getFullYear();
    let date = year + '' + month + '' + day;
    fileName = fileName+EXCEL_EXTENSION;
    FileSaver.saveAs(data, fileName);
  }

  convertTextParams(x) {
    let text = '';
    switch (x) {
      case 'timeStart':
        text = this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START');
        break;
      case 'timeEnd':
        text = this.translate.instant('REPORT.ROUTE.GENERAL.DATE_END');
        break;
      case 'deviceName':
        text = this.translate.instant('MANAGE.DEVICE.GENERAL.DEVICE_NAME');
        break;
      case 'timeZone':
        text = this.translate.instant('MANAGE.USER.PARAMETER.TIMEZONE');
        break;
      // code block
    }
    return text;
    ;
  }

}
export class XLSXModel {
  file: {
    title: string,
    subject?: string,
    author?: string,
    createdDate?: Date,
    prefixFileName: string,
    timeStart?:string,
    timeEnd?:string,
    typeName?:string,
    objName?:string,
  };
  woorksheet: {
    name: string,
    defaultCellStyle?: any;
  };
  subColumn?:XLSXTitleModel[];
  header: XLSXTitleModel[];
  subHeader?: {
    timezone?: boolean;
    userLogin?: boolean;
  };
  footer?: XLSXTitleModel[];
  styleColumn?: any;
  columns: XLSXCellModel[];
  columnsMerge?: {
    data: any[],
    position?: any[],
    style?: any
  };
  showColumn?:boolean;
  total?: {
    group?: string,
    totalCol?: {
      field: XLSXTotalCol[],
    };
    totalAll?: {
      position?: {
        top: number,
        left: number
      }
      data: XLSXTotal[]
    }
  }
}

export class XLSXModelTest {
  file: {
    title: string,
    subject?: string,
    author?: string,
    createdDate?: Date,
    prefixFileName: string,
    timeStart?:string,
    timeEnd?:string,
    devices?:string,
    columnParent?:string,

  };
  template: XLSXTemplate[];
}
export class XLSXTemplate {
  header: XLSXTitleModel[];
  woorksheet: {
    name: string,
    defaultCellStyle?: any;
  };
  columns: XLSXCellModel[];
  columnsMerge?: {   // Tiêu đề bảng có nhiều dòng
    data: any[],
    position?: any[],
    style?: any
  };
  total?: { // tính tổng
    group?: string, // title từng ngày (20-11-2019)
    totalCol?: {   // Tổng từng ngày
      field: XLSXTotalCol[],
      style?: boolean
    };
    totalAll?: { // Tổng nhiều ngày
      position?: {
        top: number,
        left: number
      }
      data: XLSXTotal[]
    }
  }
}

export class XLSXTotalCol {
  text?: any;
  columnData?: string;
  style?: any;
  merge?: {
    full: boolean,
    range: string,
  };
  wch?: number;
}

export class XLSXCellModel {
  text?: any;
  name?: string;
  columnData?: string;
  columnParent?: string;
  type?: string;// set default to string
  format?: string;
  style?: any;
  styleColumn?: any;
  merge?: {
    full?: boolean,
    range?: string,
    rowSpan?: number,
    colSpan?: number
  };
  wch?: number;
  link?: any;
  hpx?: number;
}

export class XLSXTitleModel {
  text: any;
  style?: any;
  merge?: {
    full?: boolean,
    range?: string,
  };
  type?: string; // header: style for header default
  hpx?: number;
}

export class XLSXTotal {
  key: any;
  value: any;
  style?: any;
  merge?: {
    full: boolean,
    range: string,
  };
  type?: string; // header: style for header default
}