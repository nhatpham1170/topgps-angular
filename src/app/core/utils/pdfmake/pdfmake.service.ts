import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { Component, OnInit,  } from '@angular/core';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { group } from '@angular/animations';
import { ContentAnimateDirective } from '@core/_base/layout';
import { CurrentUserService } from '@core/auth';
import { ExportService } from '../export/export.service';
import { Triangle } from '@amcharts/amcharts4/core';

pdfMake.vfs = pdfFonts.pdfMake.vfs;
@Injectable({
    providedIn: 'root'
})
export class PdfmakeService {
private config:any;
private dataPDF:any;

  constructor(
    private translate: TranslateService,
    private currentUserService:CurrentUserService,
    private exportService:ExportService
  ) {}

  ExportPdf(dataPDF,config) {
  this.config = config;
  this.dataPDF = dataPDF;
  let obj;
  // let testTable_ObjectAssign = this.objectAssign(dataTotal);
  let today = new Date();
  let day = today.getDate();
  let month = today.getMonth()+1;
  let year = today.getFullYear();
  let date = year+''+month+''+day;
  let timeEnd = config.content.header.params.timeEnd || new Date();
  let timeStart = config.content.header.params.timeStart || new Date();
  let deviceName = config.content.header.params.deviceName || '-1';
  let timeStop = config.content.header.params.timeStop || 1;

  obj = {
    pageOrientation: 'landscape',
    info: {
      title: config.info.title+'_export_' + date
      },
    content: [
      this.getHeader(config.content.header),
    ],
    footer:function(currentPage, pageCount) { 
            return {
              columns: [
                {
                  text:config.footer.title,
                  alignment : 'left',
                  margin:[35,10,20,0],
                },
                {
                  text:'Page '+currentPage.toString() + '/' + pageCount,
                  alignment : 'right',
                  margin:[0,10,30,0],
                }
              ]
            }           
            },
    defaultStyle: {
      fontSize: 8,  
    }
  };
  // header table content
  let table = config.content.table;
  let countTable = -1;
  table.forEach(itemTable => {
    countTable++;
    let bodyTable;
    let titleDay;
    let totalDay;
    var group;
    let nameTable;
    let dataTable = dataPDF[countTable];
    let length = itemTable.widths.length;
    //name table
    if(itemTable.name) obj.content.push(itemTable.name);
    if(itemTable.header) bodyTable = itemTable.header;
     
 
    //content table
    dataTable.forEach(element => {
        // title day 
        if(itemTable.total && itemTable.total.group)
        {
          group = itemTable.total.group;
          titleDay = this.convertTitleDay(group,element,length);
          bodyTable.push(titleDay);
        }

        // content data
        let data = element.data;
        if(data)
        {
          let count = 0;
          data.forEach(item => {
            count ++;
            let params = {
              config : itemTable.body,
              data : item,
              count : count
            };
            let itemDay = this.convertTotalColDay(params)
            bodyTable =   bodyTable.concat(itemDay);
          });
        }
        // total day
        if(itemTable.total && itemTable.total.totalCol) {
          let params = {
            config : itemTable.total.totalCol,
            data : element,
          };
          totalDay = this.convertTotalColDay(params);
          bodyTable =   bodyTable.concat(totalDay);
          // bodyTable.push(totalDay);
        }
  
    });
   // end table
   
   let table = {
     table : {
      headerRows: itemTable.headerRows,
      widths: itemTable.widths,
      body: bodyTable
     }
   }
   if(!this.config.info.chart)   obj.content.push(table);
   //total all
   if(itemTable.total.totalAll && itemTable.total.totalAll.title)
   {
    obj.content.push(itemTable.total.totalAll.title);
   }

   if(itemTable.total.totalAll && itemTable.total.totalAll.content)
   {
    let totalAll = itemTable.total.totalAll.content;
    let testTable_ObjectAssign = this.objectAssign(totalAll);
    obj.content.push(testTable_ObjectAssign);
   }

  });
    //export pdf
    // pdfMake.createPdf(obj).open();
    // return;
    let file_name = '';
    // timeStart = this.exportService.convertDateTimeExport(timeStart);
    // timeEnd   = this.exportService.convertDateTimeExport(timeEnd);
    // let dateNow =  moment().format('YYYY-MM-DD HH:mm:ss');
    // dateNow = this.exportService.convertDateTimeExport(dateNow);
    
    // let file_name = config.info.title;
    // if(deviceName != '-1') 
    // {
    //   if(this.config.info.devices && this.config.info.devices.length  == 1)  deviceName =  this.config.info.devices.toString();
    //   deviceName = this.exportService.convertDeviceExport(deviceName);
    //   file_name = file_name+'__'+deviceName+'_';
    // }
    // file_name = file_name+timeStart+'_'+timeEnd+'__'+'export'+'__'+dateNow;
    file_name = this.getFileName(this.config.content.header,config.info.title);
    pdfMake.createPdf(obj).download(file_name);
  }

  getFileName(config,prefix)
  {
    let timeEnd = config.params.timeEnd || new Date();
    let timeStart = config.params.timeStart || new Date();
    let deviceName = config.params.deviceName || '-1';
    timeStart = this.exportService.convertDateTimeExport(timeStart);
    timeEnd   = this.exportService.convertDateTimeExport(timeEnd);
    let dateNow =  moment().format('YYYY-MM-DD HH:mm:ss');
    dateNow = this.exportService.convertDateTimeExport(dateNow);
    
    let file_name = prefix;
    if(deviceName != '-1') 
    {
      if(this.config.info.devices && this.config.info.devices.length  == 1)  deviceName =  this.config.info.devices.toString();
      deviceName = this.exportService.convertDeviceExport(deviceName);
      file_name = file_name+'__'+deviceName+'_';
    }
    file_name = file_name+timeStart+'_'+timeEnd+'__'+'export'+'__'+dateNow;
    return file_name;
  }

  convertTitleDay(group,data,length){
    let columns_content = {
     text:'',colSpan:length,alignment:"left",fillColor:"#CCCCCC",bold:true,color:"#fff",fontSize:14
    };
    group.forEach(element => {
      if(element.hasOwnProperty("columnData"))
      {
        columns_content.text = this.exportService.convertTextData(data[element.columnData]);
        // delete element.columnData;
      }
    });
    return [columns_content];
  }

  getProfilePicObject() {
    return {
      image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPkAAAArCAYAAAC6uS60AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjVBNDU0N0IyQzY2MTExRTM5NTlEQjRBMTc5MjlEMzQ3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjVBNDU0N0IzQzY2MTExRTM5NTlEQjRBMTc5MjlEMzQ3Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NUE0NTQ3QjBDNjYxMTFFMzk1OURCNEExNzkyOUQzNDciIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NUE0NTQ3QjFDNjYxMTFFMzk1OURCNEExNzkyOUQzNDciLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5hwLfAAAAjmklEQVR42uxdB4AU5fV/M1tu9yrH3VFFBKQrVpqIFVFRQoLYiBoTS9TElr/Y0FhiNBpj7zWKYsAWmihqNCpVRaRIFQSkXQMOru7uzP+9md/czc3NbLlbwCR88G52p3zzlfd77Sur6LpO+9P+tD/99ya//ctBU0v2yEsURSHFdiT5a/6X1JqpFVOIqZKpjGm3IXoggHTjo05aHIEkeddGorRt924r32RSBlMf6xVWVkwxpmVMWopVzWfqbMvLyq+caQO+d8Z99vfVMK1ItjmZejMFHXmskXZzub8nU9hRpmZ3JfpnI75Lvj1wPl3aQvLawfRDCs8cyHQ0U1+0bwbOS5lKmb5nWsz0jUcbeaV2ID3JcmvIvwp9XteCdjiEqT9TL6b2VA8XqmVaz7SU6UumH70yiF50cFOQ7+V0JNMpTAOZOoFpq8Csc5g+xOekkggBn6qQymAXYZAk0EWwPArQNWofprNTeT/SL5mucDCFyvQI0/P4/jumEQ6ACmOcxbQpiXcEkF8HRx6/YVrgcv89EArpAvnbTHfgu/TbBBsDpgvks5j+kMS9oiDGMo1mOtylH60Ug5CdzfQqeCuZJDzw2xTaTgcPi5BaBz6ehu/Jpk7oy58D4CGP+7Yzfc30JtPEeMJrX4B8GLfFzdwcJ3u03CCmC5giTJOZ7mX6LpmMAz4f5YYyaHtVtaHZk0g7IXGHulwbxfS3FOolYL4E2sSZvrZ97uZxz31MFyUJgsOY2jjO53jc3xcgT1ey1yWT6dA9wCPfJ3HPZUzjobkTJR9TF5Dw1ntM1zGtTvBcB4++SjaJwF/LdCvTpCTuvxT8XpSk1TgMdDWUx2dejLm3kphQjzCwP2QSgH/P9IAuYFKUU5jOFA3H38cyPaWbJvsvdVM7/S6ZF4gGz8nIoHAgENe0d6SXPc6PTlFDHeLB8HNgJlrJy4S7EJZNMqnGrfoe99amuR8jjnfuiaBOPDM3m+kNpueSBLhbEkvqC6YhCe6LpqEuXZn+ASDGSzfD2itqxjuE92YynbovQZ4lZh4D71qmYjatr+fvRzGCbmKaCv/3bgb626yBr2B6je85lO99lI9ZTE/w9b8ko53lnvzMMPnVpKv2Lw8zuT/89WTTKGgMZ5qUAhAegp/7U05BB/8oe/gdToC/y3ReGt4hltA/mbrvpXZ7jGm4x7Wh0OAtSZlwnTrtC3NdYZC+xFx+hkLKBsbgDwxEAYRo6Tw+LmGHWvzG85hbBvK5PnwM8T2b+LnrGOhigrzCdBOCKA8m8s2DbLbnwGxXEwuGXfCbrnDxfUdCACVjqo9yOS/+2ZQUJfL1aejwRKmCaVUznvM5npNA6VdxAm8B1Mkp/LbZgndu7/AKQj4N89QricU0H8Eo8cMLIKyP9RDAhchzOCUfZNXAEzUQRlk2HmiFOIFXeoDpE4c1JG13p4ew3Aje/Aq8JPf0AF8e7XJ/EXDye0+Q76HRtMuZzjEigor+Kwb6FQzEMbZ3d4dp/Ay//kq7tkZx3tEFiLphjtynKwJ6ZUEioIeDQdpdW0dRTUtG1UxyAbmkXzDdn4QmPgSBH2f6lMxIaCrpFsQi1uxBkM/zMu1STKsBIq/Ulmm5S0DsNaYbUnzX+fCnveozHu2tecR5HsbRmU5GW8xMshxVANl6m1Cyjq3BB39kGuzy7GE4b/edD/aICb0JN9VtyOte0DiXa2PQFjtdzXUNw1RppEKd9LuZ5N+VjJR/8/F8Pv+ky71XMF0dY1DGNJ3kKGDVzYb7lK/dzt/8fOJ+vqAYEsmDNH7Wz8IiHPBTkvMA5nhotiOSDLx4mepvNAM42YmslTSkvTU5Il3vyTTcOfckYDgJbpcWRwgMRz+7pStbUK8YSOIIW5nex7u+jGOa21NPWDzOOMqNHgC3YgU3QsO7CdbDPX1yPf3/LmRwtmF6l2mmZoBX9/Hnwz2Ewni5P6ZrxnfdpIuZpMFe5nOrmE5gOiaecJEeECGR4feTL7koex18PTdzc3QSpvrPXc6XpaAd3ITGWXvShdpLIFfT9P6zoPGc6SsELKuTyEPcsstcgmm1MOsz0lAvK+2O43J1TTL+kIwrPdHlnAicDp4ZCQjTykiKYaZLetJoGcVQtOfyx+PjmHcD+bZptpLkw8wZwgCW4MUTMP9nJ3QgVZUCfh/VRmKUBNYnw/xxdqAA+J44WuIQlM+ZZgLozU3iv80Cc/6vpws8tOnNlNrogQzFPs50EJlDgYthwW2glk1ccUsrkwSv2/i2MRJF5nh5cZx3vIr4RQ3iI3UQYj96g7zFqG4kJDrw1yMQZJlny7+Nx+Miga5WFWWmpYkNXCrK2wi4dWeNPhX3DqFkZlkxsn2qD/VOiPJFCNwc5Tjfj8yhsW9TNNX/kULTRV06XyT+bQikpDtp/0EAF34Z4HJ+GYJYqaY/7KVy5yTpwiwD0LMd589gWoj4xVTwZnVzrMVGjBXTAkm7IroLxFUlwn/r+ceaXrgCUoYUvT5Y45beZkw+rcGisMa5WfuLKbWJ3yjj63JyhwEAnXKt4EKEq1HVZORJoSCXxyyRziisSwR0Df7dUS7R3p97gNwrqr4hRQacCF+ti+P8tbj2bZoZUBqrfTOeq7IHdPZSkmHMVi7n//UTF1ZjPc5vdXyX4dtpCCw6U0cIeSGZIDQfFuwcWCVJWR+NQH6kf4xhY5Mxx1yxNKlpeVvnFNWCNL7LpxhV6x3o65rb+VPAAroVUS12aHoxkWRooJPZSQpLIz3PMKN0uwAx4ux/03Ttejz+qW42XAnMLZF8O2v1IHVVN9K5vuksGKx4HPv+/LbetJw6+DbR9b4b6GvtKEZrVaL2eIfpLhf/THzCP7v4c15R9SkARLJJrIgPmF53Mdtk7HxYmoNlx1Li2V5u6Vmm/9vLYPEax168j8CrU+L57zKi5DWBa77LOZkmfLqHMLNSN9BYCLeVcOfeInNiT3IgL1A/A8gBYICYAG7F7ch+r3zyK3W0NTqYvq87iwLKLrIBwueItgjAJXosc8YXcJsdD59rs97oPj2gm0NYVjrBMNlNs16HgKAa3U83+Z+j830T+EyIr8TEFDCOYg2oSg09Gaikk2onsq0TMARSnCSMPxfvcgOzM5r5szSY6pZpJ0M815A5l9+eJHIs011fSSOjSpmzmmkB7O3U1uN8cYLnWqOOyS4uKU0ygBdATGgbLLlsHDOhfI4h96E6SVs8LLzVwMBk5JNM4K83SKy9z8mcFj0zsbnO+WO9WKN/5ncL+I6jrpLhPSt+6pkxgTbUnUgxPYPPxIptZnt9S2IByRO6aYYNgynyDnfFWiuiLgEzvq1ON4egbrH5HzUwM0s5r50VeiaN9H9K5/rfoxqtjWGWiw7XDUEXw+cwDVQX0jj/k3RXVIYPKxI14BsuIFdgsjtBfqbL80vJfaFIItBZ5vkXLv651YHFtG/TvjCPg3HiGPGSKJKzUwC5CNJ3k7hXFow82cy63Evei1VmkDn0JsK+f4r5DqWGWXPj3SRCWpIAu9C3lHqFXqdaLVPAuo7xWs7Um8Hbzj68BWaRSTGn8rlaGQNngP5dCirXIzJWbgL+ab72FZ79RCazMeXx+ZURXa0KUi39ITiBVAPe3v52JQvbG/wv0iB1NsM+oQKTxt7lEWCzg0+k6JEu971FzZ/zLKbc0y7n28ON2NdpX2hyPQ4w46VsUE4SlE1Nx6rTnUR5PJXgntlwpWQ140dJWhb2JAthbo+ryVuYTolQ1sreodc3rK89gcqjPXb4lerPAQ6JFL5oLNqOacaSUCRzkYqmd4AZ/yspKIP4PpkQwxp9I4P6OP7+CWv21xj21tjxrN16mMaFXqWh/gVUrYUHs5gRP2mJqwBiRRnm9nrIdzedrE1icyDIgsEThxII+Ziajn33hYSdi+8jXRgjApC3JN2JdzvnIF8Gk31eGvpqC+qYavpsH4C8Ko6LE1fvpEmYpCMJuK9P0hKqQ7BVSKawnkjmkLPw3sFJPH8HFNXCdINcfOe3NN23KKjuOn1I9j3FM3c8zdo48DKb7aNQwQlGBXTR0uzD+1RjZhui6AfZrAoxORby6Q9iMaNNqvmOKxSj8sprCunVVXp4Un//Ehofepmiekh8oPdhh59MHuOTEn0frC6icb7H6e7Y7YnM9n+4gFxBPefaPjuTmOnLWtiW5XBhJrqY9A+j01u6OkqGYy6k/4zktSlC15+g1WEXGKVwvZ6CVm5OWgV6Fm5CX2j64QB+lofrdy14aGu6QD4apoiA9EgG3fS2gcVn9M9+rOSzivEzAkrlAgbmAPjWdxmRbzbHHXPKnfN8qx022SL+NIXbLjtCvidzade657IeoDylYnCNnjGVhbZldonfKssIXRc47OJ+v8H3Ar2vnUgL9MHcGpVedZJNBSSKX+Tig98ICXtUGgJu8Uy7CxFxtadBCM5VtjB/P/3nJK/FKon8Vmsike6IKXSk+ItcEqUa+L3FDnfX2uFnB65toJZNhnJ779egR8GD4sK5rcg7HgHAd9LR2WI+v+4IjvSv07Jm9A6/e2ZZtHvx0sox1zLQP4cZsYZBbgwT6TI3vWEamt00na/Y5xib9zzADvrPYqT+UKcH7pqUdzcd7l8+uFYLT+d+s6/6kXHm9wCOlU3jBiqLvip62HcHDYu+za3mGW0vh8lzseO8+OF9oE0DTVz/1FacJUoyaeM4F2l9G/1vpRXUMOTqDDa1Rl95mchuPvCQFoJcXLJXoalbmg6E9dkRcRcZSZDtpmT4LdHcCNHwMrbe2UVJygSiXukIvI2GSZnR1FZR+mt6YNpxuX9tc3Bo1ryaWM41DG5ZcjoBprt5X8P89GeZqvFZ5qhHsbAkB9fGRXWlsk4LXXBP9kslPw99dkydnjmN3Jf1dYFG7+kehAvRYGUpjVMf43LGDcJ57eQhu3f8zOX8J+S9fLK5zH2/hy+a8z8EchGen7qcL2ymyxFqYXkUajo7rblJhmVfYvoT01VQmiKETkshj088TPZQS0FuAdxreIPtIt8AnXzThuY9WtQ+uPjpWi3nOt2c9PYQTKnhloDgc8sZyM9gIYqYvDI55iI+P59hfzn78GURPTT6npyXZo/PfmVQVMuYwoKkIE75LI3ey+1iBSvhm9Vn6DRlGutxT7z8m9w3E7yc3Ofev7EHGFy2n/qO9ievOQJi1XRLMa+6n1C9viX3XX4uouSj/R08YgLRloDcU4M3NY8DA8Jq+fQzCsYXdQl98WidFh7BWnoFg/kU3ZzhJbOWXlbEx1GUOiYB+TNiavP1V/i+3nWa7+OoFh56T+7fZ43PmTA4pmdOZ2++MIlydgXQezYVQDLGH6O/qndRPm01Zum5JIkL/NPlfJZL3UsR/Et3qrJbPmkODP0nJdFWcz20uYxtH5xkPn3IZRx5H6aNZryp2eWUdRVuAWDhx9XNBfkvoLGSXpYX1cMDMn3l00cV3lzUL2vKzKiWOzBGyrWsob9h6sF0MdM9zHc3MV3Fn89jKmD6KKJnnJ2tRIY91frx5eNzJg7W9PBULb4GT9p0r2YjpA+38V3KvYZl48H1byb5nplxfMOWJrF6Xktznj5YYc0lZS+DQQJmt3pck8VDEvO5wsN9k7LKLioygUXmIZwaxwynfVCviR7X7oC75jbjT3Arw9JTYfU60xdkm+qaSuAtoYnupTREo/Nx+kn5j52ZF9hSMmfHxY9F9eATPqXO2vywEzSkbDO7jgH3raZlre0e3EhPt36GTg4tGBSLZU7VSStshhKyTPczyBGprWBev1R9k2Zop9AH+kj+VuHsaxkSkxlshyR4x8Q9zAw3w08rTFN+4vetbIEV8DO0y95M4pfLzL9bXK5JsEomEcnY6JfQkAKgfPBXP9qLm5ZKQFlT/aRoMf7Mr+XPaowNVd11mFziVBJkPcjlmozk/BpWzEaY4Hnw5Q+PU4TH7fEhf4oAz2i+yBIfXZ8+MHfiyHbBVcWzyq7TdkY6L1bVqsW2lWsiEEg2gDk7+3N6pPVL1MG3bXAslgOANztZpnujqLtuLJnR6UHlTvpS78+qOB+r1RoMETL3GY8H8h/gv+/JJBN0ZEuhp9KUX9iDqZJNGXuysjGfaSyomgCjkVAXE1aGNS+N45+OasYrZ3gE91Lnc8VHfi53xx0/UGl2O8qrKqeDylfTN50GU20wh2+Ikk9rNM1BhtxkT7bpHlkWkXug1yvJmPpsp9pPBuBvpKNjdVIH1GpZ0zqHFhWd324cHZYzxYBZTAszuH18zKIegW30apsnaHKbhxjgOwbFtMypadJgXdx89Gp+bx/G0B2G2Z7hNj32TYo/+eRdckw/jKlcFzXtQ9GyBfGcn4Afqe8Jn17a3Wg3X4AOLl5CvbZ8bQBcQON4t8z8k008Yml47VZoUQFRccsBrjLAIzRi6WQatWgCjVn4Ip227E0auO4T4/uxq9+j/KoyivlDTv6YQY7NF5uZZpLLenk1CYA3w0SPmwbUaeHpYd/ONqcVPkxj2v6ROoW/oSJfJY3Ln0JzO95BF+Z+zN0ZHKTpgelpNFHtGr2X3RGr4L+XKZPZHp7GNkWTaLtEtxfGyXNyI0b1h6l1ZSm1rdjEHRlIVgsGndpAmN0hcISpr40DMF8crZ3OpDj5RkxSi1zuzUpUXwG3zIfIqamgtjs30okrpxngOHnFFGNdggDCzLu+PcQsl9V5HzazDquQh0xoepi8p5u68X2W06cz+kvKyODtue1b6ly2iur8GZRVW0GhaDVVBzKpoHIbHbFhNo1m4B/H9WslYGeLRWtoM4kZyFTp5c2oj0zp/guZszSbTAP270kTPR7QNd0/tVYPjDwwtLikddsVNErvQKdmlHOLcWNpmYPZjGYNrhfsgXc3Md01mO1/Ve+mhdoRLNLbM3OZO+CaK9SNPbN/65KXzGr6qoHRFeq+dREd8/0s4/NbR15KVaF8Y9mryiYafLJvXQTXaotZdAZ3blUp5VZvp815B7LQyDD2zMLz8q7byH0ctawBNOwD8jth6s6m9K5e0wlzgk1tpFBGpKqe62sCYeOcTzN2HZatmWRKp1NyrrDXN7N2Jw1c+y/W4MsM+Kjsy1YFsxkw5pLxz7uPMFBYxyDSDV83wvXTZB69DMPKhCEZXz4Bfes2hi0z3+SXTOZBa8qGE5X1AsaQWbq9zezC4HNHXlWW5SZCWOPy53F/DV3zPvljUWpVXWqU06qfvU+ERNP3+3Ee9dz6LX1z4BBafMBAQyCY/atPh9vwC+DvaATeAi4Bux3g3w9gaXtusa3YdzM9d2qOtWnEaD5OZMpIuJ680VHF5hKq8d3c89w8yrmGzSis+5T5fPLMWkUvvcgXokEKd6KuDWKUT+NyFco8d12AUX+MGUddizX+bqwhb/isa9a1mO15nCcrL30tnxghK9rMLSyFEzWaT33pPO0l2sJtq1LE9A/5fgGoaoBUgfTFjzEaWkg1gDhk9Uw6cuNsihrP6LQ9s4i2ZxUxWDvRynaHUV0gywjAyLXGWk83mDeLmf2QTV9Rny3fUGbdLtrYuhvVMsNsyutsPB/hzz5+XjPaWq9nSM3SogI6rmf+7mLaFWrF5fAz2GKN7jfiELLRR30QSOpj/940iKSTtVGIBhNaNcBZsGsLHcYM23H7OuSp0xau66JOx1BJTgejrHZT1tqxy5jpyFZOZs1O6ruF67t5IWWz1otCaNhmQhr+a2VGjlGGneF8Wt32UFpX0JNqWAgofE3uhY+rwifv4Ig470BMYyuXQTM2LuH/Gvo1v6qEQiykBGgVLJAjIqQsTAD0qt7goZjA9htfpc1ya7bT6UsnUeHurRRjUGvMD5qS2AtWwVNSt9VtDqF5XYeZvGTsehLDdV12P+qIwKLfBnAZItvCdS+WdtHlh0TM+xsZGdqFXTxBPpoRaAI8mU0jWgZyyXt+jUIjL1AzS4aogUEMwOlc4AKjXHsW5HJ+Lb9nBH9YaamofJ9Gz64bTu9vHUJhtZo25nel79ofaUjnKu4QyTe7dle9zdy6soR8nNeB5auo36YFFGHmscxsAZhPjxoZl2W3oSUdB9Cqtv0oysyQGdld3yEi8XNqdtCw5e+wWVdiMJzh38Uihrkqz5fmtKM5XU+hDYW9KMTCQDRQhIWGBHIy63ZTFguFbsXL+VhB3UqWU0l2e/qwz2janVlAmWwViLAQX1CN1RpMXZVh4kAiwBlsUtYEcwzQiDZrYOYgl6GGQnWmBVgTzKQovzODATl47UfUfdsSfrbWECb1piFrswgLgE97jqSV3G4kGl3xU5jLJWAUphRtVrh7G/Vf/29qU7GZ6xt0+t5NACElkvYURt6eVchtMcwQJCIMi3MPMAUtX6cmAGtg/DDfG1GDLLj91Is1abeS79it+pEC3M6S7/qCg7mvj+LPZhuUs4AWgV3JAoWM8kkwMEJtdm2mQ3+cT/nVZZxnldHmUbV5K1WlThKoW9qhP60t6m1o9O2ZhQb4I/5Ms/zCq/WCD/UR4RatoXCkmvKqy7mMOQbv2fdZ3HVJP1eQn8vAe50z8CW9M0zLQU4M8vkM8gcZ5M8zAFuZoNwrIJf3yP5xp1lDQtlBjd5Ydgz9e0M/CvvrDKav4cYWxl3LAAszQA6A5hIgyHe5Rz5HfYE4nRk1OrQkp73RGa2YQSxhIHkFWOsJqL0CdgIMYaQVrNHl/fKuZR2Opnbswx5Yvsa4HuIOF60cZUYOMJi3srm/uVVn6sFg3M5gFwHTlQVAW2bSFfx5Y0F31qJfGYy+ot3hnN9RVBkyf1VZtHCXkhWGpm5VbU4BKGfmW9H+COpevJQOKl3FgiPDzQ83BQU37796jaJNbI30ZeEnoJIyykQkuS7tZvqyvmYAI2oIQRGGwWgdLecy1bAAa1+xkUqz2hmgqWE/eGe4NR3E/vHawt7Gu49e/xm7ATkGaLuVrjCnhUl7Kw1C2T7MJb60lLEsuy2D8ChDuEs9WrN/HeD8RHNL/ZPR3ImS9L0FUBHIuzNy2ZQfZPBdL3YBfyjowcKtDfXl/pJr23I6GkpFfPsg97XwlJTHDvKXnnrcFeRnA+SBFEEe4eOvGbwn8f2/aQbI545Vww8OVTMY5LHWzQD5NQzyIv58ezNAzn61Lv75d0HW4mt2tKO/Lz2NKiOs9RTNaiSj8YRRzCiw32GGpsagxg+gG+3WOLqsJ9g3WsogwiCm+Ot9SEJ53J63BIsIBNPf1AxNKialBRTRkgI00WYVbOKLtVHBZrH4je1ZgFhRbyM/wySM1QeaEpmjwnQCxBw2y0Xb6+hxHTGMlkX/GjScH5rYqA9iaGaPKdhQRK0XygS3JZ5AbvBlzZWSPrgFhrujNLT3nkqmi4D4gEKubpbxfoWMNjaElMvPdT/93POugTcZLhJHalIKATeJsFyIZ2TeuUQdxqZQpzkY2yzFAL4EH9qk8Pz/YfCf8O5xKTz7PQJwq30M6IraML26bDjtrMukoGr4eTKWfB43rsZNuLPOlzEBPtHleJe062fUMOVSlv+dCz5+E4ERWV10EZ7zMYNMhv94jm62tfSBbDSxBhFjoR8xSaISPuavDYyRMonLIHnKqqNyNvRWYhz114j6SrT5C3w+m9/1OpP0zykMgCUYMhI/7xw2xWVhhBi4EhQ7rtbvmxGOVBpmuMVAEWO82thWW6K+SlRR3+HiLsE7f4N6ViNAK4E/We8sWy5V8/teYYbcGoa/i3bpqpvtYP3OuQ8Bpij4TlYoWr8achwCTvbNLUag7uu4jBOYueX9p3M5p2EU4XyUpS+CbevBi6+g/eVXYxfhHRJZP8NWFufkni4siC6U8nJbTaCGn0U6AX0kwce/g2+PRpDR2rRE2nQoArySZKWZrOE/ETxitcE7uJ6LAK7022+Y39qyIJ+LIKHwiuz/JnXcHjMnMhUjWCvtcYwRVFSU13WPPejdxOkUdEhNigC3hnl+Rckv1phrA7ikL8FQJSkA/CHHDKEHUwD4aVZkO8jsNmdzHyqpzrMATmCErmC0VhiDzEMnzwKgZChGRgE6Yfx2PkgG3mUp4cFgug/wTDk6OxffP0D95b7rwOitABZJ92EYT+ZvWz9/K8zaD0M8D0IoSD5XIW8NIwLW/WdTw1LNkzEUZ21dlWcJZdHQAmzRcvAx5UcjbsFkH6nrrSh3Nzz/MXjgNgire8GYAoirjJlfDRp7KYT48cirDPUIoezS6HfZ+PJSjIlbKvcc8MZ0zHW4GPU8BxH15yA0hNFlKenhyOcZCGUrD2sW3E2IZM/GJJvWjuHIv0ABCfiuwflhKNcHEJiPonzHUuMfIMx3KLqzwQvfYUTGagPp9yHUsODpPtRpGup6Ab7LFNc/4R5RSrKDrSx7voTMKc8SYTvT0xXwOD8FUvEN8l6a5wS4laLQXHoCjT4XFXEurF8ACZtIozsBbiVLk8f7Qb01kILGjwr6WCGU14Ro9ua+DPCIc2xapOZmCJ6uqF8E0jqAjqqFYHwfwCVo8DPAKAGAbDeivREw5gF4hwwp7YT2GY48vrMNNx2LvO+0tf1uaNmt1DCtthZtvxBaYiA0R5lt8sjRYNQRYGCNvPcSk6GplwHyLDCopS1K0C5bIAxiAPdwWDfvOfKyfkRgADXMq44BUNm2qLFVxtmo5yAMY50KIWLtP56PdvXhXQKM5/F8HZ4Nwbo8CcAsh/KSjRZeQDmzUO5qR7+vgcD8CMKbMA79IKyB2RC0/aDVnb/bbs+vBpp7OayzgbY20HFOouiZ1LBJpJTpfliEH0GRHIt211GXKATP5HhDaPEco3+iMdw0etQD4E6gv5EiwMmh0b3Gd2/wALgd6A/G0eAjyLZKJ8iG8FwGeBlrcb/aaCipFg37e5juj0B7ivbrj054AcybQ413bKmAy6OBkYpsQkvHuXxoED8Y82Z04jXQVNI/L8KcHIL3BZGnBqaocowH+5CfrAP4M7RvFu7rAe15EiR/PpjTa4JNJjXskyVt8AC0SAWsnKvBsA/AXL4PAsb6ZU2fy0QgnwNMHdEfY6jh98PGwCwfZLNodGpsjip4/iCA1QKJc4JQGdrtdoBeQ1mtH4m4GuXv7SjnY3B/hqMdfaBKxySUEMoWc5RNd4T4vdoA24sb5+31q7TVQYO1dAWUhQ5c3oi+uAltljLI7Rq9xqHBL4gDcHsHugF9HqYRJtoaZwGAXuoC8L8lYY67AX2N5YPXcwGDuqw6k0111uK+JrNXc6FBhWH/iLLkQso+BAax/PNZqNcBYLjzYGILkBbDf3vTBh459y4slgjM39vgn0+CEFFhElqM2h7PWtpPtgLqA00qwuJ30DA6Jtwshp94GUB+AczaWejbC9C3rTAW294Ri/kQwqYDhI21QCKM99wKWof3PYL+/RtcFL8Lv9ktQ6nDTLSv5PdL1EN2O3kKmrkNJoR8CfO0Fco0DvVcj3b7GmBUoeH9AFsBrq1GHCECzXgJ6vUCeDXPMUvwcfDL/TCH/RAmV6KMh4EWox7tUM5CWGsFcMHaoz7bPNogYLMmcmA95MGlWop+y4dyehfnKyBcrV/YmUHuPyVl8vidd95Z/+Wtlfc5fkHFiK6zuags4+MY45uijOXj5IZfVIlzVCQ8rUzhY3f+diifmMfHM5nK7NH1KB8OVQLUGVHjBsFnDG+xqaj/gomZW2cTXX+I6icn2Eh3/fyh7AvHdAzT92SOia+25U8ZPo3mb+3B1Js/R9xAXkuNV68FbYGSHwHq3TDhqsA8Q9Ehn6HDFGq8k2wWNOqx8JErcG8nCMYuECKlmMhxJeIHbyGA0wam8vdg3ktgti1BkCkTYPgSDN4axwMA8k1wBw6FMB0MS2EoZoaV2KyeIOIsw2AmT0f5A9R4i6KdANBVyO9Z5OWcJmoJJ6KG7ZvWo17HQpBsgCDaDABmQigNgAtRAEFSDXAtRPsfiWc1WIFbce+XoCIIk8UA2kWo1xyY+zGbeV2Gush164cFl8K3Hguf/xG8rxXAOQDA/gRteDmspkm2OjvbIA8W2Epq2GDzFLTLIxAKVh2Xoi2+hTDti7IUYXpuo91JR44cGXfGm9svqMjwWpCPrzdjnDzA527lk8/x1S0uQ2g0Vg3TUDXDHPNuOoR2HB8PIz32eDPHyW/mzzP4/BLnEFqItfesHw6jt1cPNcbF96effEr8Q5f762ikZ599tinI96f9aX/670vq/ibYn/an/+70/wIMAIXbFV7cijvDAAAAAElFTkSuQmCC' ,
      width: 100,
      alignment : 'left'
    };
  
  }

  getHeader(configHeader,config?){
    if(config) this.config = config;
    let timezone = this.exportService.getTimeZone();
    let custom = [];
    if(configHeader.custom)  custom = configHeader.custom;
    let body =  {
      layout: 'noBorders', // optional
      table: {
          headerRows: 1,
          widths: [ 'auto', 'auto'],  
          body: [
            [
              {
                text: this.translate.instant('MANAGE.USER.PARAMETER.TIMEZONE'),
                fontSize: 12,    
              },
              {
                text:timezone ,
                fontSize: 12,
              }
            ],
            [
              {
                text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_START'),
                fontSize: 12,
              },
              {
                text:configHeader.params.timeStart ,
                fontSize: 12,
              }
            ],
            [
              {
                text: this.translate.instant('REPORT.ROUTE.GENERAL.DATE_END'),
                fontSize: 12,
              },
              {
                text:configHeader.params.timeEnd ,
                fontSize: 12,
              }
            ],
          ]
        }
  };

  let devices:any = [];
  let devicesText = configHeader.params.deviceName;
  let point = '';
  if(this.config.info.devices) devices = this.config.info.devices;

  if(devices.length > 3)
  {
    devices = devices.slice(0,3);
    point = '...';
  }
  if(this.config.info.devices)  devicesText = ': '+ devices.toString()+point;
  
  
  let arrDevice =    
  [
    {
      text: this.translate.instant('MANAGE.DEVICE.GENERAL.DEVICE_NAME'),
      fontSize: 12,
    },
    {
      text: devicesText ,
      fontSize: 12,
    }
  ];

  let arrUnit =    
  [
    {
      text: this.translate.instant('REPORT.QCVN.ROUTE.UNIT'),
      fontSize: 12,
    },
    {
      text:configHeader.params.unit,
      fontSize: 12,
    }
  ];

  let arrNumberPlate =    
  [
    {
      text: this.translate.instant('REPORT.QCVN.ROUTE.NUMBER_PLATE'),
      fontSize: 12,
    },
    {
      text:configHeader.params.numberPlate,
      fontSize: 12,
    }
  ];

  let arrUserName =    
  [
    {
      text: this.translate.instant('COMMON.COLUMN.USERNAME'),
      fontSize: 12,
    },
    {
      text:configHeader.params.userName,
      fontSize: 12,
    }
  ];

  let arrTimeStop = [
    {
      text: this.translate.instant('REPORT.GEOFENCE.SUMMARY.TIME_STOP'),
      fontSize: 12,
    },
    {
      text:configHeader.params.timeStop,
      fontSize: 12,
    }
  ];

  let arrStatus = [
    {
      text: this.translate.instant('DASHBOARD.STATUS'),
      fontSize: 12,
    },
    {
      text:configHeader.params.status,
      fontSize: 12,
    }
  ]

  
  let arrDuration = [
    {
      text: this.translate.instant('REPORT.QCVN.DRIVING_4H.DURATION'),
      fontSize: 12,
    },
    {
      text:configHeader.params.duration,
      fontSize: 12,
    }
  ];

  let arrDriver = [
    {
      text: this.translate.instant('REPORT.QCVN.DRIVER.NAME_EXPORT'),
      fontSize: 12,
    },
    {
      text:configHeader.params.driver,
      fontSize: 12,
    }
  ];


  if(configHeader.params.unit) body.table.body.push(arrUnit);
  if(configHeader.params.numberPlate) body.table.body.push(arrNumberPlate);

  if(configHeader.params.userName) body.table.body.push(arrUserName);
  if(configHeader.params.timeStop) body.table.body.push(arrTimeStop);
  if(configHeader.params.status) body.table.body.push(arrStatus);
  if(configHeader.params.duration) body.table.body.push(arrDuration);
  if(configHeader.params.driver) body.table.body.push(arrDriver);

  if(configHeader.params.deviceName || this.config.info.devices) body.table.body.push(arrDevice);  
  body.table.body[body.table.body.length - 1][0]['margin'] = [0,0,0,20];

  let header = [
   
      {
        text: configHeader.title, bold: true,fontSize: 20,alignment: 'center',margin: [0, 0, 0, 20]},
      {
        text : ""
      },
      body,
      custom
  
    ];
    return header;
  }

  convertTotalColDay(params){
    let total = params.config;
    let data = params.data;
    let fuel = 'fuel';
    let checkColumnParent:boolean = false;

    if(this.config.info.columnParent) fuel = this.config.info.columnParent;

    if(!data.hasOwnProperty(fuel)) data[fuel] =  [{text:''}];

    let lenghtFuel = 0;
    if(data[fuel]) lenghtFuel = data[fuel].length;
    if(lenghtFuel > 1 ) checkColumnParent = true;

    let count;
    if(params.count) count = params.count;

    let resutl:any = [];
    let columns_content:any;
      let countFuel = 0;
      if(lenghtFuel == 0) data[fuel] = [{text:''}];
      
      data[fuel].forEach(itemFuel =>{
        let objTotal:any = [];
        total.forEach(element => {
        
          columns_content = {};
          if(element.hasOwnProperty("style"))
          {
            let style = element.style;
            Object.keys(style).forEach(x => {
              columns_content[x] =  style[x];
            });
          }
          // fuel
          if(element.hasOwnProperty("columnParent") && element.hasOwnProperty("columnData") ) 
          {
            columns_content.text = this.exportService.convertTextData(itemFuel[element.columnData]);
            // if(itemFuel.hasOwnProperty(element.columnData))
            // {
            //   columns_content.text = this.exportService.convertTextData(itemFuel[element.columnData]);
            // }else
            // {
            //   columns_content.text = '';
            // }
          }
         else  if(element.hasOwnProperty("columnData"))
          {
            if(countFuel == 0 && checkColumnParent) this.StyleFuel(columns_content,lenghtFuel);

            columns_content.text = this.exportService.convertTextData(data[element.columnData]);
          }
          else
          {
            columns_content.text = this.exportService.convertTextData(element.text);
            if(countFuel == 0 && checkColumnParent) this.StyleFuel(columns_content,lenghtFuel);
          }
          if(element.hasOwnProperty("auto"))
          {
            columns_content.text = count;
            columns_content.style = {alignment:"center"};
            if(countFuel == 0 && checkColumnParent) this.StyleFuel(columns_content,lenghtFuel);
          } 
      
         objTotal.push(columns_content);
  
       });
       resutl.push(objTotal);
       countFuel++;
      //  console.log(resutl);
       
      })
    
  
     return resutl;
  }

  StyleFuel(content,lenghtFuel)
  {
    content.rowSpan = lenghtFuel;
    content.margin  = [0,6,0,0];
    return content;
  }

  objectAssign(dataTotal){
    let testTable_ObjectAssign = {table: {body: []}};  
    dataTotal.forEach(element => {
      let objectAssignRow = []
      element.forEach(item => {
        let itemObjectAssignRow =  Object.assign({}, item);
        objectAssignRow.push(itemObjectAssignRow);
      });
      testTable_ObjectAssign.table.body.push(objectAssignRow);  
    });
   return testTable_ObjectAssign;
  }


}
export class itemBody {
    text?:any;
    colSpan?:number;
    rowSpan?:number;
    alignment?:string;
    fillColor?:string;
    bold?:boolean;
    margin?:any;
    style?:any;
}

export class PdfModel{
  info? : {
    columnParent?:string,
    devices?:any,
    chart?:boolean,
    title: string;
  };
  file?: {
    title: string,
    subject?: string,
    author?: string,
    createdDate?: Date,
    prefixFileName?: string,
  };
  content:{
    header :{
      title:string,
      params:{
        timeStart:string,
        timeEnd:string,
        unit?:string,
        numberPlate?:string,
        deviceName?:string,
        userName?:string;
        timeStop?:string;
        status?:string;
        timeZone?:string;
        duration?:string;
        driver?:string
      },
      custom?:PdfContentModel[];
    };
    table?:tableModel[];
  };
  footer?:{
   title:string 
  }  

}

export class tableModel{
  woorksheet?: {
    name: string
  };
  name?:PdfContentModel[];
  header?:any;
  headerRows?: number;
  widths?:any;
  body?: PdfContentModel[];
  total?: { // tính tổng
    group?:PdfContentModel[], // title từng ngày (20-11-2019)
    totalCol?:PdfContentModel[],// total day
    totalAll?:any,
  }
}
export class PdfContentModel{
  image?:any;
  width?:any;
  text?:any;
  columns?:any;
  colSpan?:number;
  rowSpan?:number;
  alignment?:string;
  fillColor?:string;
  bold?:boolean;
  color?:string;
  margin?:any;
  columnData?:string;
  columnParent?:string;
  fontSize?:number;
  style?:object;
  auto?:boolean
}
