import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';// Angular Material
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

//angular user tree
import { TreeModule } from 'angular-tree-component';
import { UsersTreeComponent } from '@app/core/user-tree/_view/users-tree.component';
import { ControlMessagesComponent } from './_components/control-messages/control-messages.component';


@NgModule({
  declarations: [
    ControlMessagesComponent,
    // SpinnerComponent,
    UsersTreeComponent
    // PaginationComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    NgbModule,
    HttpClientModule,
    TreeModule.forRoot(),
    // ToastrModule.forRoot(),
    AutocompleteLibModule

  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    // Component
    ControlMessagesComponent,
    // SpinnerComponent,
    UsersTreeComponent

  ],

  providers: [

  ],
})
export class SharedModule { }
