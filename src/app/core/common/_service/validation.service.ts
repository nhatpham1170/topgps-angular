import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
	providedIn: 'root'
})
export class ValidationService {
	constructor(private translate: TranslateService) { }

	public  getValidationErrorMessage(validatorName: string, validatorValue?: any, labelName?: string): any {
		
		const validate = [{
			required: this.translate.instant('COMMON.VALIDATION.REQUIRED_FIELD'),
			email : this.translate.instant('COMMON.VALIDATION.EMAIL'),
			pattern: 'Invalid password. Password must be at least 6 characters long, and contain a number.',
			minLength:  this.translate.instant('COMMON.VALIDATION.MAX_LENGTH',{ max: validatorValue.requiredLength }), 
			max:  this.translate.instant('COMMON.VALIDATION.MIN_LENGTH',{ min: validatorValue.requiredLength }),
			listemail : this.translate.instant('COMMON.VALIDATION.ELE_EMAIl',{ email: validatorValue.email })
			
		}];
		return validate[0][validatorName];
	}

	public static passwordValidator(control: AbstractControl): any {
		if (!control.value) { return; }

		// {6,100}           - Assert password is between 6 and 100 characters
		// (?=.*[0-9])       - Assert a string has at least one number
		// (?!.*\s)          - Spaces are not allowed
		return (control.value.match(/^(?=.*\d)(?=.*[a-zA-Z!@#$%^&*])(?!.*\s).{6,100}$/)) ? '' : { invalidPassword: true };
	}

	
	public  phoneValidator(control: AbstractControl): any {
		if (!control.value) { return; }

		// {6,100}           - Assert password is between 6 and 100 characters
		// (?=.*[0-9])       - Assert a string has at least one number
		// (?!.*\s)          - Spaces are not allowed
		return (control.value.match(/^(?=.*\d)(?=.*[a-zA-Z!@#$%^&*])(?!.*\s).{6,100}$/)) ? '' : { invalidPassword: true };
	}
}
