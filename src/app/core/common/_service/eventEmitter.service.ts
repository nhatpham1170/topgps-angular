import { Injectable, EventEmitter } from '@angular/core';    
import { Subscription } from 'rxjs/internal/Subscription';    
    
@Injectable({    
  providedIn: 'root'    
})    
export class EventEmitterService {    
    
  invokeIdSelectedUserTree = new EventEmitter(name);
  resetLocalStorage = new EventEmitter();    
  invokeIdloadDataDefault = new EventEmitter(name);    
  loadUpdateTree = new EventEmitter();    
  alertChanged = new EventEmitter(name);
  paramsGeofence = new EventEmitter();

  subsVar: Subscription;    
  geofenceDetail:any;

  
  constructor() { }    


  userTreeIdSelect(id) {    
    this.invokeIdSelectedUserTree.emit(id);   
  }    

  resetLocalStorageName(){
    this.resetLocalStorage.emit();   
  }
  
  loadDataDefault(id)
  {
    this.invokeIdloadDataDefault.emit(id);
  }

  updateTree(params)
  {
    this.loadUpdateTree.emit(params);
  }

  getAlertChanged(number)
  {
    this.alertChanged.emit(number);
  }

  detailGeofence(params)
  {
    this.paramsGeofence.emit(params);
  }



}    