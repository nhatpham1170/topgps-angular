import { Injectable } from '@angular/core';

import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment.prod';
import { tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

const API_URL = environment.api.host + "/utilities/feedback";
@Injectable({
  providedIn: 'root'
})
export class FeedbackService {
  constructor(private http: HttpClient, private utils: TypesUtilsService) { }

  create( model:FeedbackModel, option?:BaseOptionAPI){
    let formData = new FormData();
    formData.append("name", model.name);
    formData.append("phone", model.phone);
    formData.append("email", model.email);
    formData.append("content", model.content);
    if(model.images != undefined){
      model.images.forEach(x=>{
      formData.append("images", x);
      })
    }
    if(model.imagesRemove != undefined){
      formData.append("imagesRemove", JSON.stringify(model.imagesRemove));
    }
  
    return this.http.post<ResponseBody>(API_URL ,formData, {
      params: this.utils.processBassOptionApi(option),
    })
  }
  list(option:BaseOptionAPI):Observable<ResponseBody>{
    return this.http.get<ResponseBody>(API_URL,  {
      params: this.utils.processBassOptionApi(option)
    });
  }
  update(model:FeedbackModelEdit,option:BaseOptionAPI):Observable<ResponseBody>{
    let params = new FormData();
    params.append('id',model.id);
    params.append('status',model.status);
    params.append('description',model.description);

    return this.http.put<ResponseBody>(`${API_URL}/${model.id}`,params,{
      params:this.utils.processBassOptionApi(option)
    });
  }
}
export class FeedbackModel {
  name:string;
  phone:string;
  email:string;
  content:string;
  images:Array<any>;// [file:bold]
  imagesRemove:Array<any>;//[{url:string}]
}
export class FeedbackModelEdit{
  id:string;
  status:string;
  description:string;
}
