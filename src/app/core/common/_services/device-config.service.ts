import { Injectable } from '@angular/core';

import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment.prod';
import { tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

const API_URL = environment.api.host + "/settings/devices";
@Injectable({
  providedIn: 'root'
})
export class DeviceConfigService {
  configDevice: any;
  constructor(private http: HttpClient, private utils: TypesUtilsService,
    private translate:TranslateService) { }

  async get(option?: BaseOptionAPI) {
    if (this.configDevice!=undefined)
    {
      return this.configDevice;}
    else {
     let result =  await this.http.get<ResponseBody>(API_URL + "/config", {
        params: this.utils.processBassOptionApi(option),
      }).toPromise();
      if(result.result.data.deviceIcons){
        result.result.data.deviceIcons = result.result.data.deviceIcons.map((icon:any)=>{
          const temp = `ICON_TYPE.${icon.name}`.toUpperCase();
          const tempTranslate = this.translate.instant(temp);
          icon.nameTranslate = tempTranslate==temp?icon.name:tempTranslate;
          return icon;
        })
      }
      this.configDevice = result.result.data;
    }
    return this.configDevice;
  }
  remove() {
    this.configDevice = undefined;
  }
}
