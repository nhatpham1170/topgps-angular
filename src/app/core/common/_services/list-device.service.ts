import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ListDeviceService {

  public eventEmitterAction: EventEmitter<string>;
  public showOnMobile:boolean = false;
  constructor() {
    this.eventEmitterAction = new EventEmitter();
  }

  close() {
    this.eventEmitterAction.emit("close");
  }
  open() {
    this.showOnMobile = true;
    this.eventEmitterAction.emit("open");
  }
}
