import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseOptionAPI } from '@core/_base/crud/models/_base.option-api';
import { TypesUtilsService, ResponseBody } from '@core/_base/crud';
import { environment } from '@env/environment.prod';
import { tap } from 'rxjs/operators';
import { RequestFile } from '../_models/request-file';

const API_URL = environment.api.host + "/download/request";

@Injectable({
  providedIn: 'root'
})
export class RequestFileService {

  constructor(private http: HttpClient, private utils: TypesUtilsService) { }
 
  create(model: RequestFile, option?: BaseOptionAPI): Observable<ResponseBody> {
    const formData = new FormData();
    formData.append("type", model.type);
    formData.append("fileExentetion", model.fileExtention);
    formData.append("params", JSON.stringify(model.params));    

    return this.http.post<ResponseBody>(API_URL, formData, {
      params: this.utils.processBassOptionApi(option),
    });
  } 
}

