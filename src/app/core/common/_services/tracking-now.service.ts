import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TrackingNowService {

  public eventEmitterAction: EventEmitter<string>;
  constructor() {
    this.eventEmitterAction = new EventEmitter();
  }

  close() {
    this.eventEmitterAction.emit("close");
  }
  open() {
    this.eventEmitterAction.emit("open");
  }
}
