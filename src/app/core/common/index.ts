import { from } from 'rxjs';

export * from './shared.module';

// sevice
export { ListDeviceService } from './_services/list-device.service';
export { TrackingNowService } from './_services/tracking-now.service';
export { DeviceConfigService } from './_services/device-config.service';
export { RequestFileService } from './_services/request-file.service';
export { UserTreeService } from './_service/user-tree.service';
export {FeedbackService, FeedbackModel,FeedbackModelEdit} from './_services/feedback.service'
