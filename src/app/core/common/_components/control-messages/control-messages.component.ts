import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ValidationService } from './../../_service/validation.service';

@Component({
	selector: 'app-control-messages',
	templateUrl: './control-messages.component.html',
	styleUrls: ['./control-messages.component.scss']
})
export class ControlMessagesComponent implements OnInit {

	@Input()
	public control: FormControl;
	@Input()
	public labelName?: string;

	constructor(
		public validate : ValidationService
	) { }

	ngOnInit() {
	}

 
	get errorMessage(): boolean {
		for (const propertyName in this.control.errors) {
			if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
				return this.validate.getValidationErrorMessage(
					propertyName,
					this.control.errors[propertyName],
					this.labelName,
				);
			}
		}

		return undefined;
	}

}
