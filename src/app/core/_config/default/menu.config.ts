import { transcode } from 'buffer';

export class MenuConfig {
	public defaults: any = {
		header: {
			self: {},
			items: [
				{
					title: 'Dashboards',
					root: true,
					alignment: 'left',
					page: 'dashboard',					
					translate: 'MENU.DASHBOARD',
				},
				
				{
					title: 'Components',
					root: true,
					alignment: 'left',
					toggle: 'click',
					submenu: [
						{
							title: 'Google Material',
							bullet: 'dot',
							icon: 'flaticon-interface-7',
							submenu: [
								{
									title: 'Form Controls',
									bullet: 'dot',
									submenu: [
										{
											title: 'Auto Complete',
											page: 'material/form-controls/autocomplete',											
										},
										{
											title: 'Checkbox',
											page: 'material/form-controls/checkbox'
										},
										{
											title: 'Radio Button',
											page: 'material/form-controls/radiobutton'
										},
										{
											title: 'Datepicker',
											page: 'material/form-controls/datepicker'
										},
										{
											title: 'Form Field',
											page: 'material/form-controls/formfield'
										},
										{
											title: 'Input',
											page: 'material/form-controls/input'
										},
										{
											title: 'Select',
											page: 'material/form-controls/select'
										},
										{
											title: 'Slider',
											page: 'material/form-controls/slider'
										},
										{
											title: 'Slider Toggle',
											page: 'material/form-controls/slidertoggle'
										}
									]
								},
								{
									title: 'Navigation',
									bullet: 'dot',
									submenu: [
										{
											title: 'Menu',
											page: 'material/navigation/menu'
										},
										{
											title: 'Sidenav',
											page: 'material/navigation/sidenav'
										},
										{
											title: 'Toolbar',
											page: 'material/navigation/toolbar'
										}
									]
								},
								{
									title: 'Layout',
									bullet: 'dot',
									submenu: [
										{
											title: 'Card',
											page: 'material/layout/card'
										},
										{
											title: 'Divider',
											page: 'material/layout/divider'
										},
										{
											title: 'Expansion panel',
											page: 'material/layout/expansion-panel'
										},
										{
											title: 'Grid list',
											page: 'material/layout/grid-list'
										},
										{
											title: 'List',
											page: 'material/layout/list'
										},
										{
											title: 'Tabs',
											page: 'material/layout/tabs'
										},
										{
											title: 'Stepper',
											page: 'material/layout/stepper'
										},
										{
											title: 'Default Forms',
											page: 'material/layout/default-forms'
										},
										{
											title: 'Tree',
											page: 'material/layout/tree'
										}
									]
								},
								{
									title: 'Buttons & Indicators',
									bullet: 'dot',
									submenu: [
										{
											title: 'Button',
											page: 'material/buttons-and-indicators/button'
										},
										{
											title: 'Button toggle',
											page: 'material/buttons-and-indicators/button-toggle'
										},
										{
											title: 'Chips',
											page: 'material/buttons-and-indicators/chips'
										},
										{
											title: 'Icon',
											page: 'material/buttons-and-indicators/icon'
										},
										{
											title: 'Progress bar',
											page: 'material/buttons-and-indicators/progress-bar'
										},
										{
											title: 'Progress spinner',
											page: 'material/buttons-and-indicators/progress-spinner'
										},
										{
											title: 'Ripples',
											page: 'material/buttons-and-indicators/ripples'
										}
									]
								},
								{
									title: 'Popups & Modals',
									bullet: 'dot',
									submenu: [
										{
											title: 'Bottom sheet',
											page: 'material/popups-and-modals/bottom-sheet'
										},
										{
											title: 'Dialog',
											page: 'material/popups-and-modals/dialog'
										},
										{
											title: 'Snackbar',
											page: 'material/popups-and-modals/snackbar'
										},
										{
											title: 'Tooltip',
											page: 'material/popups-and-modals/tooltip'
										}
									]
								},
								{
									title: 'Data table',
									bullet: 'dot',
									submenu: [
										{
											title: 'Paginator',
											page: 'material/data-table/paginator'
										},
										{
											title: 'Sort header',
											page: 'material/data-table/sort-header'
										},
										{
											title: 'Table',
											page: 'material/data-table/table'
										}
									]
								}
							]
						},
						{
							title: 'Ng-Bootstrap',
							bullet: 'dot',
							icon: 'flaticon-web',
							submenu: [
								{
									title: 'Accordion',
									page: 'ngbootstrap/accordion'
								},
								{
									title: 'Alert',
									page: 'ngbootstrap/alert'
								},
								{
									title: 'Buttons',
									page: 'ngbootstrap/buttons'
								},
								{
									title: 'Carousel',
									page: 'ngbootstrap/carousel'
								},
								{
									title: 'Collapse',
									page: 'ngbootstrap/collapse'
								},
								{
									title: 'Datepicker',
									page: 'ngbootstrap/datepicker'
								},
								{
									title: 'Dropdown',
									page: 'ngbootstrap/dropdown'
								},
								{
									title: 'Modal',
									page: 'ngbootstrap/modal'
								},
								{
									title: 'Pagination',
									page: 'ngbootstrap/pagination'
								},
								{
									title: 'Popover',
									page: 'ngbootstrap/popover'
								},
								{
									title: 'Progressbar',
									page: 'ngbootstrap/progressbar'
								},
								{
									title: 'Rating',
									page: 'ngbootstrap/rating'
								},
								{
									title: 'Tabs',
									page: 'ngbootstrap/tabs'
								},
								{
									title: 'Timepicker',
									page: 'ngbootstrap/timepicker'
								},
								{
									title: 'Tooltips',
									page: 'ngbootstrap/tooltip'
								},
								{
									title: 'Typehead',
									page: 'ngbootstrap/typehead'
								}
							]
						},
					]
				},
				{
					title: 'Applications',
					root: true,
					alignment: 'left',
					toggle: 'click',
					submenu: [
						{
							title: 'eCommerce',
							bullet: 'dot',
							icon: 'flaticon-business',
							permission: 'accessToECommerceModule',
							submenu: [
								{
									title: 'Customers',
									page: 'ecommerce/customers'
								},
								{
									title: 'Products',
									page: 'ecommerce/products'
								},
							]
						},
						{
							title: 'User Management',
							bullet: 'dot',
							icon: 'flaticon-user',
							submenu: [
								{
									title: 'Users',
									page: 'user-management/users'
								},
								{
									title: 'Roles',
									page: 'user-management/roles'
								}
							]
						},
					]
				},
				{
					title: 'Custom',
					root: true,
					alignment: 'left',
					toggle: 'click',
					submenu: [
						{
							title: 'Error Pages',
							bullet: 'dot',
							icon: 'flaticon2-attention',
							submenu: [
								{
									title: 'Error 1',
									page: 'error/error-v1'
								},
								{
									title: 'Error 2',
									page: 'error/error-v2'
								},
								{
									title: 'Error 3',
									page: 'error/error-v3'
								},
								{
									title: 'Error 4',
									page: 'error/error-v4'
								},
								{
									title: 'Error 5',
									page: 'error/error-v5'
								},
								{
									title: 'Error 6',
									page: 'error/error-v6'
								},
							]
						},
						{
							title: 'Wizard',
							bullet: 'dot',
							icon: 'flaticon2-mail-1',
							submenu: [
								{
									title: 'Wizard 1',
									page: 'wizard/wizard-1'
								},
								{
									title: 'Wizard 2',
									page: 'wizard/wizard-2'
								},
								{
									title: 'Wizard 3',
									page: 'wizard/wizard-3'
								},
								{
									title: 'Wizard 4',
									page: 'wizard/wizard-4'
								},
							]
						},
					]
				},
			]
		},
		aside: {
			self: {},
			items: [
				{
					title: 'Dashboard',
					root: true,
					icon: 'icon-home',
					page: '/dashboard',
					// permission:'ROLE_device.action.command',		
					translate: 'MENU.DASHBOARD',
					bullet: 'dot',
					main:true
				},
				{
					title: 'Map',
					root: true,
					icon: 'flaticon2-pin',
					page: '/map',
					permission:'ROLE_map.map',
					translate: 'MENU.MAP',
					bullet: 'dot',
					main:true,
					mainDefault:true
				},				
				// {
				// 	title: 'Report',
				// 	root: true,
				// 	icon: 'flaticon2-graph',
				// 	page: '/report/reports',
				// 	permission:'ROLE_report.report',
				// 	translate: 'MENU.REPORT',
			
				// },	
				{
					title: 'List report',
					root: true,
					icon: 'icon-report',
					page: '/reports',
					permission:'ROLE_report.report',
					translate: 'MENU.REPORT',
			
				},
				{
					title: 'Notifications',
					root: true,
					icon: 'icon-bell2',
					page: '/manage/notifications',
					permission:'ROLE_manage.alert',
					translate: 'MENU.MANAGE_ALERTS',
					bullet: 'dot',
				},				
				{
					title: 'Manage',
					root: true,
					// bullet: 'dot',
					icon: 'flaticon2-settings',
					permission:'ROLE_manage.manage',
					translate: 'MENU.MANAGE',
					submenu: [
						{
							title: 'Manage User',
							page: '/manage/users',	
							icon:'icon-user',
							permission:'ROLE_manage.user',
							translate: 'MENU.MANAGER_USER',
						},
						{
							title: 'Manage Device',
							page: '/manage/devices',
							icon:'icon-device',
							permission: 'ROLE_manage.device',
							translate: 'MENU.MANAGER_DEVICE',
						},
						{
							title: 'Manage Group Device',
							page: '/manage/device-group',
							icon:'icon-user-alias1',
							permission: 'ROLE_manage.device_group',
							translate: 'MENU.GROUP_DEVICE',
						},	
						{
							title: 'Manage Driver',
							page: '/manage/driver',
							icon:'icon-driver1',
							permission: 'ROLE_manage.driver',
							translate: 'MENU.MANAGER_DRIVER',
						},	
						{
							title: 'Manage Command',
							page: '/manage/command',
							icon:'icon-command',
							permission: 'ROLE_manage.command',
							translate: 'MENU.COMMAND',
						},
						// {
						// 	title: 'User Alias',
						// 	page: '/manage/user-alias',
						// 	permission: 'ROLE_manage.alias',
						// 	translate: 'MENU.USER_ALIAS',
						// },
						{
							title: 'Manage Alert',
							page: '/manage/alert-rules',
							icon:'icon-alert',
							permission: 'ROLE_manage.alert_rules',
							translate: 'MENU.ALERT',
						},
						{
							title: 'Manage geofence',
							page: '/manage/geofences',
							icon:'icon-vector-square',
							permission: 'ROLE_manage.geofence',
							translate: 'MENU.MANAGE_GEOFENCE',
						},
						{
							title: 'Manage geofence group',
							page: '/manage/geofence-group',
							icon:'icon-geofence-group',
							permission: 'ROLE_manage.geofence_group',
							translate: 'MENU.MANAGE_GEOFENCE_GROUP',
						},
						{
							title: 'Manage poi',
							page: '/manage/pois',
							icon:'icon-map-marked-alt',
							// permission: 'ROLE_manage.poi',
							translate: 'MENU.MANAGER_POI',
						},
						{
							title: 'Schedule report',
							page: '/manage/report-schedule',
							icon:'flaticon2-calendar-2',
							permission: 'ROLE_manage.schedule_report',
							translate: 'MENU.SCHEDULE_REPORT',
						},

						{
							title: 'Trip manage',
							page: '/manage/trip',
							icon:'icon-deviantart',
							permission: 'ROLE_manage.trip',
							translate: 'MENU.TRIP',
						},
						{
							title: 'Task remind manage',
							page: '/manage/task-remind',
							icon:'fas fa-clock',
							permission: 'ROLE_manage.remind',
							translate: 'MENU.REPORT_TASK_REMIND',
						}


						
											
					]
				},
				{
					title : "Balance",
					root: true,
					// bullet: 'dot',
					icon: 'icon-pastel',
					permission:'ROLE_point.points',
					translate: 'MENU.BALANCE',
					submenu: [
						{
							title: 'Point manage',
							page: '/points/manage',	
							icon:'icon-money-bag',
							permission:'ROLE_point.manage',
							translate: 'MENU.POINT_MANAGE',
						},
						{
							title: 'Transaction history',
							page: '/points/transaction-history',	
							icon:'icon-comments-dollar',
							permission:'ROLE_point.transaction_history',
							translate: 'MENU.TRANSACTION_HISTORY',
						},
						{
							title: 'Renews history',
							page: '/points/renews-history',
							icon:'icon-calendar-alt',
							permission: 'ROLE_point.renews_history',
							translate: 'MENU.RENEWS_HISTORY',
						},
						{
							title: 'Synthesis report',
							page: '/points/synthesis-report',
							icon:'icon-file-alt1',
							permission: 'ROLE_point.synthesis_report',
							translate: 'MENU.SYNTHESIS_REPORT',
						},
					]
				},
				
				{
					title : "Utilities",
					root: true,
					// bullet: 'dot',
					icon: 'icon-tools',
					permission:'ROLE_utilities.utilities',
					translate: 'MENU.UTILITIES',
					submenu: [
						{
							title: 'Map tracking',
							// root: true,
							icon: 'flaticon2-map',
							page: '/map/trackings',
							permission:'ROLE_map.trackings',
							translate: 'MENU.TRACKINGS',
							// bullet: 'dot',
						},
						// {
						// 	title: "Debug",
						// 	icon: 'flaticon2-map',
						// 	page: '/utilities/debug',
						// 	// permission:'ROLE_report.debug',
						// 	translate: 'MENU.DEBUG',
						// 	// bullet: 'dot',
						// },
						{
							title: 'Tracking Geofences',
							// root: true,
							icon: 'icon-geofence-group',
							page: '/map/tracking-geofences',
							// permission: 'ROLE_map',
							translate: 'MENU.TRACKING_GEOFENCES',
							bullet: 'dot',
						},
						{
							title: 'Tracking Landmark',
							// root: true,
							icon: 'icon-vector-square',
							page: '/map/tracking-landmarks',
							// permission: 'ROLE_map',
							translate: 'MENU.TRACKING_LANDMARKS',
							bullet: 'dot',
						},
						{
							title: 'Toll Fee Calculation',
							// root: true,
							icon: 'icon-money',
							page: '/map/toll-fee',
							// permission: 'ROLE_map',
							translate: 'MENU.TOLL_FEE',
							bullet: 'dot',
						}
						// {
						// 	title: 'Card service',
						// 	page: '/utilities/card-service',
						// 	icon:'icon-credit-card',
						// 	permission: 'ROLE_utilities.card_service',
						// 	translate: 'MENU.CARD_SERVICE',
						// },
						// {
						// 	title: 'Insert Card',
						// 	page: '/utilities/insert-card',
						// 	icon:'icon-credit-card',
						// 	permission: 'ROLE_utilities.insert_card',
						// 	translate: 'MENU.INSERT_CARD',
						// }
					]
				},
				{
				title : "fuel",
					root: true,
					// bullet: 'dot',
					icon: 'icon-gas-pump',
					permission:'ROLE_fuel.fuel',
					translate: 'MENU.FUEL',
					submenu: [
						{
							title: 'Fuel chart',
							page: '/fuel/chart',
							icon:'icon-chart-bar1',
							permission: 'ROLE_fuel.chart',
							translate: 'REPORT.DATA_LOG.GENERAL.FUEL_GRAPH',
						},

						{
							title: 'report fuel',
							page: '/fuel/changes',
							icon:'icon-up-down',
							permission: 'ROLE_report.fuel',
							translate: 'MENU.REPORT_FUEL',
						},

						{
							title: 'fuel summary',
							page: '/fuel/summary',
							icon: 'icon-eco-fuel',
							permission: 'ROLE_fuel.summary',
							translate: 'MAP.TRACKING.ROUTE.SUMMARY',
						},
				
					]
				},
				{
					title: 'Technical Support',
					root: true,
					icon: 'icon-computer-support',
					permission:'ROLE_report.supports',
					translate: 'MENU.TECHNICAL_SUPPORT',
					submenu: [
						{
							title: 'Raw log',
							page: '/support/debug',
							icon:'icon-data',
							permission: 'ROLE_report.debug',
							translate: 'MENU.DEBUG',
						}, 
						{
							title: 'Devive login',
							page: '/support/device-login',
							icon:'icon-sync-settings',
							permission: 'ROLE_report.device_session_login',
							translate: 'MENU.DEVICE_LOGIN'
						},
						 {
							 title: 'SIM info',
							 page: '/support/sim-info',
							 icon: 'icon-sim-card',
							 permission: 'ROLE_report.sim',
							 translate: 'MENU.SIM_INFO'
						 }
					]
				},

				{
					section:{
						title:'Admin',
						permission: 'ROLE_admin.admin',
						translate:'MENU.ADMIN'
					}
				},
				{
					title: 'Setting System',
					root: true,
					// bullet: 'dot',
					icon: 'icon-shield',
					permission: 'ROLE_admin.admin',
					translate: 'MENU.SETTING_SYSTEM',
					submenu: [
						{
							title: 'Manage Permissions',
							page: '/admin/permission',
							icon:'icon-shield-alt',
							permission: 'ROLE_admin.permission',
							translate: 'MENU.MANAGE_PERMISSIONS',
						},
						{
							title: 'Manage Roles',
							page: '/admin/role',
							icon:'icon-user-secret',
							permission: 'ROLE_admin.role',
							translate: 'MENU.MANAGE_ROLES',
						},
						{
							title: 'Manage Login Page',
							page: '/admin/login-page',
							icon:'icon-hips1',
							permission: 'ROLE_admin.login_page',
							translate: 'MENU.LOGIN_PAGE',
						},			
						{
							title: 'Transport Type',
							page: '/admin/transport-type',
							icon:'icon-semi-truck',
							permission: 'ROLE_admin.transport_type',
							translate: 'MENU.TRANSPORT_TYPE',
						},	
						{
							title: 'Transport Type QCVN',
							page: '/admin/transport-type-qcvn',
							icon:'icon-tag',
							permission: 'ROLE_admin.transport_type_qcvn',
							translate: 'MENU.TRANSPORT_TYPE_QCVN',
						},	
						{
							title: 'System change log',
							page: '/admin/system-change-log',
							icon:'icon-history',
							permission: 'ROLE_admin.system_log',
							translate: 'MENU.SYSTEM_LOG',
						},	
								
						{
							title: 'Notifi global',
							page: '/admin/notices',
							icon:'icon-envelope-regular',
							permission: 'ROLE_admin.notices',
							translate: 'MENU.NOTIFI_GLOBAL',
						},	
						{
							title: 'Manage POI type',
							page: '/admin/poi-type',
							icon:'icon-place-marker-2',
							permission: 'ROLE_admin.poi_type',
							translate: 'MENU.POI_TYPE',
						},
						{
							title: 'Manage toll station',
							page: '/admin/toll-station',
							icon:'icon-tollbooth',
							permission: 'ROLE_admin.toll_station',
							translate: 'MENU.TOLL_STATION',
						},
						{
							title: 'Manage toll road',
							page: '/admin/toll-road',
							icon:'icon-road',
							permission: 'ROLE_admin.toll_road',
							translate: 'MENU.TOLL_ROAD',
						},
						{
							title: 'Manage toll segment',
							page: '/admin/toll-segment',
							icon:'icon-compare-git',
							permission: 'ROLE_admin.toll_segment',
							translate: 'MENU.TOLL_SEGMENT',
						},
					]
				},
				{
					title: 'Setting Device',
					root: true,
					// bullet: 'dot',
					icon: 'flaticon2-settings',
					permission: 'ROLE_admin.admin',
					translate: 'MENU.SETTING_DEVICE',
					submenu: [												
						{
							title: 'Manage Sim Type',
							page: '/admin/sim-type',
							icon: 'icon-sim-card',
							permission: 'ROLE_admin.sim_type',
							translate: 'MENU.SIM_TYPE',
						},
						// {
						// 	title: 'Page Template',
						// 	page: '/admin/page-template',
						// 	// permission: 'accessToECommerceModule',
						// 	translate: 'MENU.PAGE_TEMPLATE',
						// },
						{
							title: 'Manage Device Type',
							page: '/admin/device-type',
							icon:'icon-multiple-devices',
							permission: 'ROLE_admin.device_type',
							translate: 'MENU.DEVICE_TYPE',
						},					
						{
							title: 'Manage Sensor Model',
							page: '/admin/sensor-type',
							icon:'icon-sensor',
							permission: 'ROLE_admin.sensor_template',
							translate: 'MENU.SENSOR_MODEL',
						},
						{
							title: 'Manage Icon Model',
							page: '/admin/icon-type',
							icon: 'icon-create',
							permission: 'ROLE_admin.icon',
							translate: 'MENU.ICON',
						},
					
					]
				},
				{
					title : "Utilities",
					root: true,
					// bullet: 'dot',
					icon: 'icon-tools',
					permission:'ROLE_admin.utilities',
					translate: 'MENU.UTILITIES',
					submenu: [
						{
							title: 'Feedback',
							// root: true,
							icon: 'flaticon-feed',
							page: '/admin/feedback',
							permission:'ROLE_admin.feedback',
							translate: 'MENU.FEEDBACK',
							bullet: 'dot',
						},
						
					]
				},
				// {
				// 	title: 'Layout Builder',
				// 	root: true,
				// 	icon: 'flaticon2-expand',
				// 	page: 'builder'
				// },
				// {section: 'Components'},
				// {
				// 	title: 'Google Material',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'flaticon2-browser-2',
				// 	submenu: [
				// 		{
				// 			title: 'Form Controls',
				// 			bullet: 'dot',
				// 			submenu: [
				// 				{
				// 					title: 'Auto Complete',
				// 					page: 'material/form-controls/autocomplete',
				// 					permission: 'accessToECommerceModule'
				// 				},
				// 				{
				// 					title: 'Checkbox',
				// 					page: 'material/form-controls/checkbox'
				// 				},
				// 				{
				// 					title: 'Radio Button',
				// 					page: 'material/form-controls/radiobutton'
				// 				},
				// 				{
				// 					title: 'Datepicker',
				// 					page: 'material/form-controls/datepicker'
				// 				},
				// 				{
				// 					title: 'Form Field',
				// 					page: 'material/form-controls/formfield'
				// 				},
				// 				{
				// 					title: 'Input',
				// 					page: 'material/form-controls/input'
				// 				},
				// 				{
				// 					title: 'Select',
				// 					page: 'material/form-controls/select'
				// 				},
				// 				{
				// 					title: 'Slider',
				// 					page: 'material/form-controls/slider'
				// 				},
				// 				{
				// 					title: 'Slider Toggle',
				// 					page: 'material/form-controls/slidertoggle'
				// 				}
				// 			]
				// 		},
				// 		{
				// 			title: 'Navigation',
				// 			bullet: 'dot',
				// 			submenu: [
				// 				{
				// 					title: 'Menu',
				// 					page: 'material/navigation/menu'
				// 				},
				// 				{
				// 					title: 'Sidenav',
				// 					page: 'material/navigation/sidenav'
				// 				},
				// 				{
				// 					title: 'Toolbar',
				// 					page: 'material/navigation/toolbar'
				// 				}
				// 			]
				// 		},
				// 		{
				// 			title: 'Layout',
				// 			bullet: 'dot',
				// 			submenu: [
				// 				{
				// 					title: 'Card',
				// 					page: 'material/layout/card'
				// 				},
				// 				{
				// 					title: 'Divider',
				// 					page: 'material/layout/divider'
				// 				},
				// 				{
				// 					title: 'Expansion panel',
				// 					page: 'material/layout/expansion-panel'
				// 				},
				// 				{
				// 					title: 'Grid list',
				// 					page: 'material/layout/grid-list'
				// 				},
				// 				{
				// 					title: 'List',
				// 					page: 'material/layout/list'
				// 				},
				// 				{
				// 					title: 'Tabs',
				// 					page: 'material/layout/tabs'
				// 				},
				// 				{
				// 					title: 'Stepper',
				// 					page: 'material/layout/stepper'
				// 				},
				// 				{
				// 					title: 'Default Forms',
				// 					page: 'material/layout/default-forms'
				// 				},
				// 				{
				// 					title: 'Tree',
				// 					page: 'material/layout/tree'
				// 				}
				// 			]
				// 		},
				// 		{
				// 			title: 'Buttons & Indicators',
				// 			bullet: 'dot',
				// 			submenu: [
				// 				{
				// 					title: 'Button',
				// 					page: 'material/buttons-and-indicators/button'
				// 				},
				// 				{
				// 					title: 'Button toggle',
				// 					page: 'material/buttons-and-indicators/button-toggle'
				// 				},
				// 				{
				// 					title: 'Chips',
				// 					page: 'material/buttons-and-indicators/chips'
				// 				},
				// 				{
				// 					title: 'Icon',
				// 					page: 'material/buttons-and-indicators/icon'
				// 				},
				// 				{
				// 					title: 'Progress bar',
				// 					page: 'material/buttons-and-indicators/progress-bar'
				// 				},
				// 				{
				// 					title: 'Progress spinner',
				// 					page: 'material/buttons-and-indicators/progress-spinner'
				// 				},
				// 				{
				// 					title: 'Ripples',
				// 					page: 'material/buttons-and-indicators/ripples'
				// 				}
				// 			]
				// 		},
				// 		{
				// 			title: 'Popups & Modals',
				// 			bullet: 'dot',
				// 			submenu: [
				// 				{
				// 					title: 'Bottom sheet',
				// 					page: 'material/popups-and-modals/bottom-sheet'
				// 				},
				// 				{
				// 					title: 'Dialog',
				// 					page: 'material/popups-and-modals/dialog'
				// 				},
				// 				{
				// 					title: 'Snackbar',
				// 					page: 'material/popups-and-modals/snackbar'
				// 				},
				// 				{
				// 					title: 'Tooltip',
				// 					page: 'material/popups-and-modals/tooltip'
				// 				}
				// 			]
				// 		},
				// 		{
				// 			title: 'Data table',
				// 			bullet: 'dot',
				// 			submenu: [
				// 				{
				// 					title: 'Paginator',
				// 					page: 'material/data-table/paginator'
				// 				},
				// 				{
				// 					title: 'Sort header',
				// 					page: 'material/data-table/sort-header'
				// 				},
				// 				{
				// 					title: 'Table',
				// 					page: 'material/data-table/table'
				// 				}
				// 			]
				// 		}
				// 	]
				// },
				// {
				// 	title: 'Ng-Bootstrap',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'flaticon2-digital-marketing',
				// 	submenu: [
				// 		{
				// 			title: 'Accordion',
				// 			page: 'ngbootstrap/accordion'
				// 		},
				// 		{
				// 			title: 'Alert',
				// 			page: 'ngbootstrap/alert'
				// 		},
				// 		{
				// 			title: 'Buttons',
				// 			page: 'ngbootstrap/buttons'
				// 		},
				// 		{
				// 			title: 'Carousel',
				// 			page: 'ngbootstrap/carousel'
				// 		},
				// 		{
				// 			title: 'Collapse',
				// 			page: 'ngbootstrap/collapse'
				// 		},
				// 		{
				// 			title: 'Datepicker',
				// 			page: 'ngbootstrap/datepicker'
				// 		},
				// 		{
				// 			title: 'Dropdown',
				// 			page: 'ngbootstrap/dropdown'
				// 		},
				// 		{
				// 			title: 'Modal',
				// 			page: 'ngbootstrap/modal'
				// 		},
				// 		{
				// 			title: 'Pagination',
				// 			page: 'ngbootstrap/pagination'
				// 		},
				// 		{
				// 			title: 'Popover',
				// 			page: 'ngbootstrap/popover'
				// 		},
				// 		{
				// 			title: 'Progressbar',
				// 			page: 'ngbootstrap/progressbar'
				// 		},
				// 		{
				// 			title: 'Rating',
				// 			page: 'ngbootstrap/rating'
				// 		},
				// 		{
				// 			title: 'Tabs',
				// 			page: 'ngbootstrap/tabs'
				// 		},
				// 		{
				// 			title: 'Timepicker',
				// 			page: 'ngbootstrap/timepicker'
				// 		},
				// 		{
				// 			title: 'Tooltips',
				// 			page: 'ngbootstrap/tooltip'
				// 		},
				// 		{
				// 			title: 'Typehead',
				// 			page: 'ngbootstrap/typehead'
				// 		}
				// 	]
				// },
				// {section: 'Applications'},
				// {
				// 	title: 'eCommerce',
				// 	bullet: 'dot',
				// 	icon: 'flaticon2-list-2',
				// 	root: true,
				// 	permission: 'accessToECommerceModule',
				// 	submenu: [
				// 		{
				// 			title: 'Customers',
				// 			page: 'ecommerce/customers'
				// 		},
				// 		{
				// 			title: 'Products',
				// 			page: 'ecommerce/products'
				// 		},
				// 	]
				// },
				// {
				// 	title: 'User Management',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'flaticon2-user-outline-symbol',
				// 	submenu: [
				// 		{
				// 			title: 'Users',
				// 			page: 'user-management/users'
				// 		},
				// 		{
				// 			title: 'Roles',
				// 			page: 'user-management/roles'
				// 		}
				// 	]
				// },
				// {section: 'Custom'},
				// {
				// 	title: 'Error Pages',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'flaticon2-attention',
				// 	submenu: [
				// 		{
				// 			title: 'Error 1',
				// 			page: 'error/error-v1'
				// 		},
				// 		{
				// 			title: 'Error 2',
				// 			page: 'error/error-v2'
				// 		},
				// 		{
				// 			title: 'Error 3',
				// 			page: 'error/error-v3'
				// 		},
				// 		{
				// 			title: 'Error 4',
				// 			page: 'error/error-v4'
				// 		},
				// 		{
				// 			title: 'Error 5',
				// 			page: 'error/error-v5'
				// 		},
				// 		{
				// 			title: 'Error 6',
				// 			page: 'error/error-v6'
				// 		},
				// 	]
				// },
				// {
				// 	title: 'Wizard',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'flaticon2-mail-1',
				// 	submenu: [
				// 		{
				// 			title: 'Wizard 1',
				// 			page: 'wizard/wizard-1'
				// 		},
				// 		{
				// 			title: 'Wizard 2',
				// 			page: 'wizard/wizard-2'
				// 		},
				// 		{
				// 			title: 'Wizard 3',
				// 			page: 'wizard/wizard-3'
				// 		},
				// 		{
				// 			title: 'Wizard 4',
				// 			page: 'wizard/wizard-4'
				// 		},
				// 	]
				// },
			]
		},
		
	};

	public get configs(): any {
		return this.defaults;
	}
}
