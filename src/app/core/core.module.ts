import { TollFeePopupComponent } from './utils/map/template/toll-fee-popup/toll-fee-popup.component';
import { LandmarkPopupComponent } from './utils/map/template/landmark-popup/landmark-popup.component';
// Anglar
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// Layout Directives
// Services

import {
	ContentAnimateDirective,
	FirstLetterPipe,
	GetObjectPipe,
	HeaderDirective,
	JoinPipe,
	MenuDirective,
	OffcanvasDirective,
	SafePipe,
	ScrollTopDirective,
	SparklineChartDirective,
	StickyDirective,
	TabClickEventDirective,
	TimeElapsedPipe,
	TimeFormat,
	ToggleDirective,
	ResizeObserverDirective,
	UserDateDirective,
	AsciiPipe
} from './_base/layout';
import { ToastsDirective } from './_base/layout/directives/toasts.directive';
import { UserDatePipe } from './_base/layout/pipes/user-date.pipe';
import { ClusterPopupComponent } from './utils/map/template/cluster/cluster-popup/cluster-popup.component';
import { MarkerPopupComponent } from './utils/map/template/marker/marker-popup/marker-popup.component';
import { TranslateModule, TranslatePipe } from '@ngx-translate/core';
import { FilterPipe } from './_base/layout/pipes/filter.pipe';
import { GroupByPipe } from './_base/layout/pipes/group.pipe';

import { CurrentUserService } from './auth';
import { PopupComponent } from './utils/map/template/popup/popup.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserDateAdvPipe } from './_base/layout/pipes/user-date-adv.pipe';
import { ClipboardModule } from 'ngx-clipboard';

@NgModule({
	imports: [CommonModule,TranslateModule,NgbModule, ClipboardModule],
	declarations: [
		// directives
		ScrollTopDirective,
		HeaderDirective,
		OffcanvasDirective,
		ToggleDirective,
		MenuDirective,
		TabClickEventDirective,
		SparklineChartDirective,
		ContentAnimateDirective,
		StickyDirective,
		ResizeObserverDirective,
		UserDateDirective,
		// pipes
		TimeElapsedPipe,
		TimeFormat,
		JoinPipe,
		GetObjectPipe,
		SafePipe,
		FirstLetterPipe,
		ToastsDirective,
		AsciiPipe,
		UserDatePipe,				
		UserDateAdvPipe,				
		FilterPipe,
		GroupByPipe,
		ClusterPopupComponent,
		MarkerPopupComponent,
		PopupComponent,
		LandmarkPopupComponent,
		TollFeePopupComponent
				
	],
	exports: [
		
		// directives
		ScrollTopDirective,
		HeaderDirective,
		OffcanvasDirective,
		ToggleDirective,
		MenuDirective,
		TabClickEventDirective,
		SparklineChartDirective,
		ContentAnimateDirective,
		StickyDirective,
		ResizeObserverDirective,
		UserDateDirective,
		// pipes
		TimeElapsedPipe,
		TimeFormat,
		JoinPipe,
		GetObjectPipe,
		SafePipe,
		FirstLetterPipe,
		AsciiPipe,
		UserDatePipe,
		UserDateAdvPipe,
		FilterPipe,
		GroupByPipe,
		TranslatePipe
	],
	providers: [],
	entryComponents: [MarkerPopupComponent,ClusterPopupComponent,PopupComponent,LandmarkPopupComponent,TollFeePopupComponent ],
})
export class CoreModule {
}
