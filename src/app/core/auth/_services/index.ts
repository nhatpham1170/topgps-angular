export { AuthService } from './auth.service'; // You have to comment this, when your real back-end is done
export { TestService } from './test.service'; // You have to comment this, when your real back-end is done
export { CurrentUserService } from './current-user.service'; // You have to comment this, when your real back-end is done
// export { AuthService } from './auth.service'; // You have to uncomment this, when your real back-end is done
