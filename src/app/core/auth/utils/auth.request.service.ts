import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ResponseBody } from '../../_base/crud/models/_response-body.model';
import { tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
  })
export class AuthRequest {
    constructor(
        private http: HttpClient, 
        private store: Store<AppState>, 
        private router:Router
    ){

    }
    // request(response:Observable<ResponseBody>,model,tranform:Object={}){
    //     response.pipe(
    //         tap(body=>{
    //             body.result = model.deserialize(body.result,{
    //                 tranform: tranform
    //             })
    //         })
    //     );
    //     return response;
    // }
    request(response:Observable<ResponseBody>){        
        // let status = status.SUCCESS;
        return response.pipe(
            tap(body => {                                        
            })
        );;
    }
}
