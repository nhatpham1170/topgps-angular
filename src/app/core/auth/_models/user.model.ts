import { BaseModel } from '../../_base/crud';
import { Address } from './address.model';
import { SocialNetworks } from './social-networks.model';
import { BaseModelCT } from '../../_base/crud/models/_base.model.ct';

export class User extends  BaseModelCT  {
    id: number;
    username: string;
    password: string;
    email: string;
    accessToken: string;
    refreshToken: string;
    roles: number[];
    pic: string;
    fullname: string;
    occupation: string;
	companyName: string;
    phone: string;
    timezone:string;
    address: Address;
    socialNetworks: SocialNetworks;
    pageMain?:string;

    clear(): void {
        // this.id = undefined;
        // this.username = '';
        // this.password = '';
        // this.email = '';
        // this.roles = [];
        // this.fullname = '';
        // this.accessToken = null;
        // // this.accessToken = 'access-token-' + Math.random();
        // this.refreshToken = 'access-token-' + Math.random();
        // this.pic = './assets/media/users/default.jpg';
        // this.occupation = '';
        // this.companyName = '';
        // this.phone = '';
        // this.address = new Address();
        // this.address.clear();
        // this.socialNetworks = new SocialNetworks();
        // this.socialNetworks.clear();
        this.id = undefined;
        this.username = 'admin';
        this.password = 'demo';
        this.email = 'admin@demo.com';
        this.accessToken = null;
        this.refreshToken = 'access-token-f8c137a2c98743f48b643e71161d90aa';
        this.roles = [1]; // Administrator
        this.pic = './assets/media/users/300_25.jpg';
        this.fullname = 'Sean';
        this.occupation= 'CEO';
        this.companyName= 'Keenthemes';
        this.phone= '456669067890';
        this.address = {
            addressLine: 'L-12-20 Vertex, Cybersquare',
            city: 'San Francisco',
            state: 'California',
            postCode: '45000'
        } as Address;
        this.socialNetworks = {
            linkedIn: 'https://linkedin.com/admin',
            facebook: 'https://facebook.com/admin',
            twitter: 'https://twitter.com/admin',
            instagram: 'https://instagram.com/admin'
        } as SocialNetworks;
        this.pageMain = "";
    }    
    // deserialize(input:any):User{
    //     Object.assign(this,input);        
    //     return this;
    // }
}
