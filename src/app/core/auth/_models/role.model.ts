import { BaseModel } from '../../_base/crud';

export class Role extends BaseModel {
    id: number;
    name: string;
    parentId : number;
    description : string;
    listPermission : [];
    permissions :[];
    isCoreRole: boolean = false;

    clear(): void { 
        this.id = undefined;
        this.name = '';
        this.parentId = 1;
        this.description = '';
        this.listPermission = [];
        this.permissions = [];
        this.isCoreRole = false;
	}
}
